
package FI.realitymodeler;

import java.io.*;
import java.util.zip.*;

public class GZIPFlushOutputStream extends GZIPOutputStream {

    public GZIPFlushOutputStream(OutputStream out) throws IOException {
        super(out);
    }

    public void flush() throws IOException {
        finish();
        super.flush();
        def.reset();
    }

}
