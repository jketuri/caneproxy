
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;

class FilePipingThread extends CacheFillingThread {
    File file;
    String list[], path, target = null;

    FilePipingThread(W3FileURLConnection uc) {
        super(uc);
    }

    public void run() {
        try {
            String s = MessageFormat.format(uc.messages.getString("directoryOfFile"), new Object[] {Support.htmlString(path)});
            writeBytes("<html><head>\n");
            writeBytes("<style type=\"text/css\">\n");
            writeBytes("span.listcolumn0 { width: 15em; clear: left; float: left }\n");
            writeBytes("span.listcolumn { width: 15em; float: left }\n");
            writeBytes("</style>\n");
            writeBytes("<title>" + s + "</title></head><body>\n");
            writeBytes("<h1>" + s + "</h1>\n");
            if (!path.equals("/")) writeBytes("<a href=\"" + Support.encodePath(path.substring(0, path.lastIndexOf('/', path.length() - 2) + 1)) + (target != null ? ";target=" + target : "") + "\">Parent Directory</a><p>\n");
            for (int i = 0; i < list.length; i++) {
                File file1 = new File(file, list[i]);
                String size;
                if (file.isDirectory()) {
                    size = "Directory";
                    list[i] += "/";
                } else size = String.valueOf(file.length());
                writeBytes("<span class=\"listcolumn0\">");
                uc.writeLine(out, new Date(file.lastModified()), size, path + list[i], target, list[i], uc.charsetName, false);
            }
            writeBytes(MessageFormat.format(uc.messages.getString("numberFiles"), new Object[] {new Long(list.length)}));
            writeBytes("</body></html>");
            out.close();
        } catch (Exception ex) {
            try {
                if (in != null)
                    synchronized (in) {
                        in.close();
                        in.notifyAll();
                    }
            } catch (IOException ex1) {} finally {
                uc.log(getClass().getName(), ex);
            }
        }
    }

}

/** Implements URL connection to local file system. URL must be of form:<br>
    file:path
*/
public class W3FileURLConnection extends W3FtpURLConnection {

    public W3FileURLConnection(URL url) {
        super(url);
    }

    public void doConnect() {
    }

    public InputStream getInputStream() throws IOException {
        if (!url.getHost().equals("")) return super.getInputStream();
        if (inputDone) return in;
        mayKeepAlive = true;
        inputDone = true;
        if (doOutput) {
            headerFields = new HeaderList();
            headerFields.append(new Header("", protocol_1_1 + " " + HTTP_NO_CONTENT));
            return in = null;
        }
        File file = new File(path.startsWith(":") ? path.substring(1) : path);
        if (!file.exists()) throw new FileNotFoundException(file.getPath());
        headerFields = new HeaderList();
        headerFields.append(new Header("", protocol_1_1 + " " + HTTP_OK + " OK"));
        if (file.isFile()) {
            String ct = guessContentType(file.getName());
            if (ct == null) ct = Support.unknownType;
            headerFields.append(new Header("Content-Type", ct));
            headerFields.append(new Header("Content-Length", String.valueOf(file.length())));
            headerFields.append(new Header("Last-Modified", Support.format(new Date(file.lastModified()))));
            Support.copyHeaderList(headerFields, headerList = new HeaderList());
            return in = origin = new BufferedInputStream(new FileInputStream(file));
        }
        if (!path.endsWith("/")) path += "/";
        headerFields.append(new Header("Content-Type", Support.htmlType));
        headerFields.append(new Header("Cache-Control", "no-cache"));
        headerFields.append(new Header("Pragma", "no-cache"));
        Support.copyHeaderList(headerFields, headerList = new HeaderList());
        FilePipingThread piping = new FilePipingThread(this);
        filling = piping;
        if (params != null) piping.target = params.get("target");
        piping.file = file;
        piping.list = file.list();
        piping.path = path;
        piping.setup(this);
        in = origin = new BufferedInputStream(piping.in = new PipedInputStream(piping.out = new PipedOutputStream()));
        piping.start();
        return in;
    }

    public OutputStream getOutputStream() throws IOException {
        if (!url.getHost().equals("")) return super.getOutputStream();
        doOutput = true;
        return sink = new FileOutputStream(path);
    }

}
