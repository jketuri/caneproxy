
package FI.realitymodeler.sasl;

import FI.realitymodeler.common.*;
import java.io.*;
import javax.security.auth.callback.*;
import javax.security.sasl.*;

public class Login implements SaslClient
{
	CallbackHandler callbackHandler;
	String username;
	int state = 0;

public Login(String username, CallbackHandler callbackHandler)
{
	this.username = username;
	this.callbackHandler = callbackHandler;
}

public void dispose()
{
}

public byte[] evaluateChallenge(byte challenge[]) throws SaslException
{
	Exception ex = null;
	try
	{
		if (challenge == null || challenge.length == 0) return new byte[0];
		String credential = null;
		switch (state) {
		case 0:
		{
			credential = username;
			state = 1;
			break;
		}
		case 1:
		{
			PasswordCallback passwordCallback = new PasswordCallback("LOGIN password: ", false);
			callbackHandler.handle(new Callback[] {passwordCallback});
			credential = new String(passwordCallback.getPassword());
			state = 2;
			break;
		}
		case 2:
		{
			return null;
		}
		}
		return credential.getBytes("8859_1");
	}
	catch (IOException ex1)
	{
		ex = ex1;
	}
	catch (UnsupportedCallbackException ex1)
	{
		ex = ex1;
	}
	if (ex != null) throw new SaslException(Support.stackTrace(ex), ex);
	return null;
}

public String getMechanismName()
{
	return "LOGIN";
}

public Object getNegotiatedProperty(String propertyName)
{
	return null;
}

public boolean hasInitialResponse()
{
	return true;
}

public boolean isComplete()
{
	return state == 2;
}

public byte[] unwrap(byte incoming[], int offset, int len)
{
	return incoming;
}

public byte[] wrap(byte outgoing[], int offset, int len)
{
	return outgoing;
}

}
