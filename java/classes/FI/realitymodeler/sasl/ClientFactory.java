
package FI.realitymodeler.sasl;

import java.security.*;
import java.util.*;
import javax.security.sasl.*;
import javax.security.auth.callback.*;

public class ClientFactory extends Provider implements SaslClientFactory
{
    static final long serialVersionUID = 0L;

	private static final String mechanismNames[] = {"LOGIN"};
	static
	{
		Security.addProvider(new ClientFactory());
	}

public ClientFactory()
{
	super("FI.realitymodeler.sasl.ClientFactory", 20040320.0, "");
	put("SaslClientFactory.LOGIN", getClass().getName());
}

public static void initialize()
{
}

public SaslClient createSaslClient(String mechanisms[], String authorizationId, String protocol, String serverName, Map<String,?> properties, CallbackHandler callbackHandler)
{
	for (int i = 0; i < mechanisms.length; i++)
	if (mechanisms[i].equals("LOGIN")) return new Login(authorizationId, callbackHandler);
	return null;
}

public String[] getMechanismNames(Map<String,?> properties)
{
	return mechanismNames;
}

}
