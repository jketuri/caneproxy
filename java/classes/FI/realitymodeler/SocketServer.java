
package FI.realitymodeler;

import java.io.*;
import java.net.*;
import java.util.*;

/** General socket server using thread pool. */
public class SocketServer extends Thread {
    public ServerSocket serverSocket;
    public Request request;
    public ThreadPool pool;
    public long timeout;

    public SocketServer() {
    }

    public SocketServer(ServerSocket serverSocket, Request request, long timeout) {
        this.serverSocket = serverSocket;
        this.request = request;
        this.timeout = timeout;
    }

    public void handle(Throwable throwable) {
        throwable.printStackTrace();
    }

    public void run() {
        request.pool = pool = new ThreadPool(timeout);
        while (!isInterrupted())
            try {
                request.socket = serverSocket.accept();
                request = (Request)pool.reserve(request);
            } catch (Exception ex) {
                if (ex instanceof InterruptedException) break;
                handle(ex);
            } catch (Error er) {
                handle(er);
                throw er;
            }
    }

    public final boolean release(Request request) throws InterruptedException {
        return pool.release(request);
    }

}
