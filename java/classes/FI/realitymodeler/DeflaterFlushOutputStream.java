
package FI.realitymodeler;

import java.io.*;
import java.util.zip.*;

public class DeflaterFlushOutputStream extends DeflaterOutputStream {

    public DeflaterFlushOutputStream(OutputStream out, Deflater def) {
        super(out, def);
    }

    public void flush() throws IOException {
        finish();
        super.flush();
        def.reset();
    }

}
