
package FI.realitymodeler.calbean;

import FI.realitymodeler.common.*;
import FI.realitymodeler.msgbean.*;
import FI.realitymodeler.portalbean.*;
import FI.realitymodeler.xml.*;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;
import javax.naming.*;
import javax.naming.directory.*;
import javax.naming.ldap.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;
import org.w3c.dom.*;
import org.xml.sax.*;
import org.xml.sax.helpers.*;

public class CalBean implements ErrorHandler, EntityResolver {
    static DocumentBuilderFactory documentBuilderFactory = null;
    static Map<ServletConfig, CalBean> calBeans = new HashMap<ServletConfig, CalBean>();
    static Map<String, String> contentClasses = new HashMap<String, String>();
    static boolean DEBUG = true;
    static {
        try {
            contentClasses.put("contact", "S:person");
            contentClasses.put("event", "S:appointment");
            contentClasses.put("journal", "S:message");
            contentClasses.put("todo", "S:message");
            documentBuilderFactory = DocumentBuilderFactory.newInstance();
            documentBuilderFactory.setNamespaceAware(true);
            documentBuilderFactory.setIgnoringElementContentWhitespace(false);
        } catch (Exception ex) {
            throw new Error(Support.stackTrace(ex));
        }
    }

    Boolean activeDirectory = null;
    CalBean calBean = null;
    Map<String, String> parameterAliases = new HashMap<String, String>();
    Map<String, Object> properties = new HashMap<String, Object>();
    Map<String, String> userParameters = null;
    ServletConfig servletConfig = null;
    ServletContext servletContext = null;
    String result = "failed";
    String state = "started";
    URL baseUrl = null;
    boolean verbose = false;

    public void warning(SAXParseException exception) {
        if (verbose) servletContext.log(Support.stackTrace(exception));
    }

    public void error(SAXParseException exception) {
        if (verbose) servletContext.log(Support.stackTrace(exception));
    }

    public void fatalError(SAXParseException exception)
        throws SAXParseException {
        throw exception;
    }

    public InputSource resolveEntity(String publicId, String systemId) {
        return new InputSource(new ByteArrayInputStream(new byte[0]));
    }

    public boolean getActiveDirectory() {
        return activeDirectory != null ? activeDirectory.booleanValue() : calBean != null ? calBean.getActiveDirectory() : false;
    }

    public void setActiveDirectory(boolean activeDirectory) {
        this.activeDirectory = new Boolean(activeDirectory);
    }

    public String getBaseUrl() {
        return baseUrl != null ? baseUrl.toString() : calBean != null ? calBean.getBaseUrl() : null;
    }

    public void setBaseUrl(String url)
        throws MalformedURLException {
        baseUrl = new URL(url);
    }

    public void setParameterAlias(String parameterAlias, String parameterName) {
        parameterAliases.put(parameterName, parameterAlias);
    }

    public String getParameterAlias(String parameterName) {
        String alias = parameterAliases.get(parameterName);
        return alias != null ? alias : calBean != null ? calBean.getParameterAlias(parameterName) : parameterName;
    }

    public String getProperty(String propertyName) {
        Object value = properties.get(propertyName);
        if (value instanceof String) return (String)value;
        if (value instanceof Vector) return (String)((Vector)value).firstElement();
        return null;
    }

    public String getIndexedProperty(String propertyName, int index) {
        Object value = properties.get(propertyName);
        if (value instanceof String) return index == 0 ? (String)value : null;
        if (value instanceof Vector) return index >= 0 && index < ((Vector)value).size() ? (String)((Vector)value).elementAt(index) : null;
        return null;
    }

    public void setServlet(String property, String value)
        throws ServletException, ParseException, MalformedURLException {
        StringTokenizer st = new StringTokenizer(property, ":");
        if (!st.hasMoreTokens() || !st.nextToken().equalsIgnoreCase("cal")) return;
        value = Support.decode(value);
        if (verbose) servletContext.log("setServlet " + property + " to " + value);
        String name = st.nextToken().trim();
        if (name.equals("activeDirectory")) setActiveDirectory(Support.isTrue(value, name));
        else if (name.equals("baseUrl")) setBaseUrl(value);
        else if (name.equals("parameterAlias")) setParameterAlias(st.nextToken(), value);
        else throw new ServletException("Unknown cal bean servlet parameter " + name);
    }

    public void setServletConfig(ServletConfig servletConfig)
        throws ServletException, ParseException, MalformedURLException {
        if (servletConfig == null) return;
        this.servletConfig = servletConfig;
        servletContext = servletConfig.getServletContext();
        if (Support.isTrue(servletConfig.getInitParameter("verbose"), "verbose") ||
            servletContext.getAttribute("FI.realitymodeler.server.W3Server/verbose") == Boolean.TRUE) setVerbose(true);
        if ((calBean = calBeans.get(servletConfig)) != null) return;
        calBean = new CalBean();
        calBean.servletContext = servletContext;
        if (getVerbose()) calBean.setVerbose(true);
        Enumeration initParameterNames = servletConfig.getInitParameterNames();
        while (initParameterNames.hasMoreElements()) {
            String name = (String)initParameterNames.nextElement();
            calBean.setServlet(name, servletConfig.getInitParameter(name));
        }
        calBeans.put(servletConfig, calBean);
    }

    /** Returns parameter value which was given in request with name
        which is mapped to actual parameter name.

        @param name name which is mapped to parameter name.

        @return parameter value or null if not found. */

    public String getUserParameter(String name) {
        return userParameters.get(name);
    }

    public boolean getVerbose() {
        return verbose;
    }

    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    private Element makeSubPropElement(Document doc, Object subPropertyFields[], StringTokenizer valueList) {
        Element propElement = doc.createElement("D:prop");
        Element contentClassElement = doc.createElement("D:contentclass");
        contentClassElement.appendChild(doc.createTextNode((String)subPropertyFields[0]));
        for (int i = 1; i < subPropertyFields.length; i++) {
            Object propertyField = subPropertyFields[i];
            String propertyName, value = null;
            if (propertyField instanceof Object[])
                if (((String)((Object[])propertyField)[0]).startsWith("S:")) {
                    propElement.appendChild(makeSubPropElement(doc, (Object[])propertyField, valueList));
                    continue;
                } else {
                    propertyName = (String)((Object[])propertyField)[0];
                    value = (String)((Object[])propertyField)[1];
                } else propertyName = (String)propertyField;
            if (value == null)
                if (valueList != null)
                    if (valueList.hasMoreTokens()) value = valueList.nextToken();
                    else valueList = null;
            if (value == null) continue;
            Element property = doc.createElement(propertyName);
            property.appendChild(doc.createTextNode(value));
            propElement.appendChild(property);
        }
        return propElement;
    }

    private void takeValue(Map<String, Object> properties, String propertyName, Object value) {
        Object oldValue = properties.get(propertyName);
        if (oldValue instanceof Vector) ((Vector<Object>)oldValue).addElement(value);
        else if (oldValue != null) {
            Vector<Object> vector = new Vector<Object>();
            vector.addElement(oldValue);
            vector.addElement(value);
            properties.put(propertyName, vector);
        } else properties.put(propertyName, value);
    }

    private void takeProperties(Element element, Map<String, Object> properties) {
        NodeList nodeList = element.getChildNodes();
        for (int i = nodeList.getLength() - 1; i >= 0; i--) {
            Node node = nodeList.item(i);
            if (node instanceof Text) takeValue(properties, element.getTagName(), node.getNodeValue());
            else if (node instanceof Element) {
                Map<String, Object> properties1 = new HashMap<String, Object>();
                takeProperties((Element)node, properties1);
                takeValue(properties, ((Element)node).getTagName(), properties1);
            }
        }
    }

    public void processRequest(HttpServletRequest req)
        throws IOException, NamingException, ParserConfigurationException, RedirectException, SAXException, ServletException, TransformerException {
        userParameters = new HashMap<String, String>();
        String name = req.getPathInfo();
        int i = name.lastIndexOf('/'), j = name.lastIndexOf('.');
        String type = name.substring(j + 1);
        name = name.substring(i + 1, j != -1 ? j : name.length()).trim().toLowerCase();
        String contentClass = contentClasses.get(name);
        if (contentClass == null) throw new ServletException("Invalid template page");
        String credentials[] = new String[3];
        PortalEntry portalEntry = PortalBean.searchPortalEntry(req, servletConfig);
        if (portalEntry == null) return;
        String action = Support.getParameterValue(req.getParameterValues(getParameterAlias("a")));
        if (action == null) return;
        URL calUrl = new URL(baseUrl != null ? baseUrl : calBean != null ? calBean.baseUrl : null,
                             credentials[1] + (name.equalsIgnoreCase("contact") ? "/contacts/" : "/calendar/"));
        String method = action.equals("Save") ? "PUT" : "FINDPROP";
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document doc = documentBuilder.newDocument();
        Element propFindElement = doc.createElement("D:" + method);
        propFindElement.setAttribute("xmlns:D", "DAV");
        propFindElement.setAttribute("xmlns:S", "urn:content-classes:");
        propFindElement.setAttribute("xmlns:C", "urn:schemas:calendar:");
        propFindElement.setAttribute("xmlns:H", "urn:schemas:httpmail:");
        propFindElement.setAttribute("xmlns:T", "urn:schemas:contacts:");
        Element propElement = doc.createElement("D:prop");
        Element uriElement = doc.createElement("D:uri");
        uriElement.appendChild(doc.createTextNode(calUrl.toString()));
        propElement.appendChild(uriElement);
        Element contentClassElement = doc.createElement("D:contentclass");
        contentClassElement.appendChild(doc.createTextNode(contentClass));
        propElement.appendChild(contentClassElement);
        Enumeration parameterNames = req.getParameterNames();
        while (parameterNames.hasMoreElements()) {
            name = (String)parameterNames.nextElement();
            Object propertyField = MsgBean.propertyFields.get(name);
            if (propertyField == null) continue;
            String propertyName, value = Support.getParameterValue(req.getParameterValues(name));
            if (propertyField instanceof Object[])
                if (((String)((Object[])propertyField)[0]).startsWith("S:")) {
                    propElement.appendChild(makeSubPropElement(doc, (Object[])propertyField, new StringTokenizer(value)));
                    continue;
                } else {
                    propertyName = ((String[])propertyField)[0];
                    if (value == null || value.trim().equals("")) value = (String)((Object[])propertyField)[1];
                } else propertyName = (String)propertyField;
            if (value == null || value.equals("")) continue;
            Element property = doc.createElement(propertyName);
            property.appendChild(doc.createTextNode(value));
            propElement.appendChild(property);
        }
        propFindElement.appendChild(propElement);
        doc.appendChild(propFindElement);
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        transformer.transform(new DOMSource(doc), new StreamResult(bout));
        HttpURLConnection calUc = (HttpURLConnection)calUrl.openConnection();
        calUc.setRequestProperty("Authorization", credentials[0]);
        try {
            calUc.setRequestMethod(method);
            calUc.setRequestProperty("Content-Type", "text/xml");
            calUc.setRequestProperty("Content-Length", String.valueOf(bout.size()));
            calUc.setRequestProperty("Depth", "1");
            OutputStream out = calUc.getOutputStream();
            if (DEBUG) {
                bout.writeTo(System.out);
                System.out.flush();
            }
            bout.writeTo(out);
            out.flush();
            InputStream in = calUc.getInputStream();
            int responseCode = calUc.getResponseCode();
            if (responseCode == HttpServletResponse.SC_UNAUTHORIZED) PortalBean.requestLogin(req);
            if (responseCode != HttpServletResponse.SC_OK) throw new ServletException("Invalid response code " + calUc.getResponseCode() + ", " + calUc.getResponseMessage());
            //          InputSource inputSource = Resolver.createInputSource(calUc.getContentType(), in, false, calUrl.getProtocol());
            InputSource inputSource = new InputSource(in);
            inputSource.setSystemId(calUrl.toString());
            documentBuilder = documentBuilderFactory.newDocumentBuilder();
            documentBuilder.setErrorHandler(this);
            documentBuilder.setEntityResolver(this);
            doc = documentBuilder.parse(inputSource);
            if (DEBUG) {
                transformerFactory = TransformerFactory.newInstance();
                transformer = transformerFactory.newTransformer();
                transformer.transform(new DOMSource(doc), new StreamResult(System.out));
            }
        } finally {
            calUc.disconnect();
        }
        takeProperties(doc.getDocumentElement(), properties);
    }

}
