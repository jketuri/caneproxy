
package FI.realitymodeler;

import java.io.*;
import java.util.jar.*;

public class W3JarRequester extends W3Requester {
    JarFile jarFile = null;

    public W3JarRequester() {
    }

    public W3JarRequester(String name)
        throws IOException {
        open(name);
    }

    public Object clone() {
        W3JarRequester jarRequester = new W3JarRequester();
        jarRequester.jarFile = jarFile;
        return jarRequester;
    }

    public void open(String name)
        throws IOException {
        try {
            close();
        } catch (IOException ex) {} finally {
            jarFile = new JarFile(name);
        }
    }

    public void close()
        throws IOException {
        if (jarFile == null) return;
        jarFile.close();
    }

}
