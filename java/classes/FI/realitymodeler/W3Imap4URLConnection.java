
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.mail.search.*;
import javax.servlet.*;

/** Implements URL connection to Internet Message Access Protocol 4 -servers. URL must be of form:<br>
    When getting input stream of message list:<br>
    imap4://host[/from]<br>
    [?auto= : quote original automatically<br>
    &amp;contents= : show in list also message contents<br>
    &amp;date=messages newer than date are listed<br>
    &amp;from=reply email address<br>
    &amp;only=only messages newer than cookie value or date are listed]<br>
    When getting input stream of specified message:<br>
    imap4://host/message number/message id<br>
    [?delete= : message is only deleted<br>
    &amp;forward= : get message as raw text<br>
    &amp;header= : show message header<br>
    &amp;plain= : only plain text is returned from message<br>
    &amp;remove= : removes message after reading]<br>
    When getting output stream of unique id list of messages to be moved to some category:<br>
    imap4://host<br>
    Basic credentials must be in request property Authorization.<br>
*/
public class W3Imap4URLConnection extends W3MsgURLConnection implements Cloneable {
    public static long cacheCleaningInterval = 24L * 60L * 60L * 1000L, cacheCleaningTime = 0L;
    public static String cacheRoot = "cache/imap4/";
    public static boolean defaultUseCaches = false, useProxy = false;
    public static String proxyHost = null;
    public static int proxyPort = 80;
    public static String imap4ServerHost = null;

    boolean secure = false;

    public Object clone() {
        W3Imap4URLConnection uc = new W3Imap4URLConnection(url);
        uc.requester = requester;
        uc.msgs = msgs;
        uc.cachePath = cachePath;
        uc.host = host;
        uc.msgVec = msgVec;
        uc.loggedIn = loggedIn;
        return uc;
    }

    public String getCacheRoot() {
        return cacheRoot;
    }

    public synchronized HeaderList getHeader(W3Requester requester, MsgItem msgItem)
        throws IOException, java.text.ParseException {
        return (HeaderList)msgItem.headerList.clone();
    }

    public synchronized InputStream getEntity(W3Requester requester, MsgItem msgItem)
        throws IOException, java.text.ParseException {
        try {
            /*
              SearchTerm messageIDTerm = new MessageIDTerm(msgItem.msgId);
              Message messages[] = ((W3Imap4Requester)requester).folder.search(messageIDTerm);
              if (messages == null || messages.length == 0) throw new IOException("Message " + msgItem.msgId + " not found");
              Message message = messages[0];
            */
            Message message = ((W3Imap4Requester)requester).folder.getMessageByUID(msgItem.uid);
            message.setFlag(Flags.Flag.SEEN, true);
            msgItem.headerList.remove("X-Unread");
            return ((MimeMessage)message).getRawInputStream();
        } catch (MessagingException ex) {
            throw new IOException(Support.stackTrace(ex));
        }
    }

    void delete(MsgPipingThread piping)
        throws IOException {
        MsgItem msgItem = piping.msgs.get(piping.uniqueID);
        if (msgItem == null) return;
        try {
            Message message = ((W3Imap4Requester)requester).folder.getMessage(msgItem.index);
            message.setFlag(Flags.Flag.DELETED, true);
        } catch (MessagingException ex) {
            throw new IOException(Support.stackTrace(ex));
        }
        piping.msgs.remove(piping.uniqueID);
        new EntityCache(piping.cachePath + Support.encode(piping.uniqueID, null)).reset();
    }

    Object openCategoryObject(String category, String from)
        throws IOException {
        try {
            if (category.equalsIgnoreCase("destruction")) return null;
            Folder folder = ((W3Imap4Requester)requester).store.getFolder(category);
            if (!folder.exists()) folder.create(folder.HOLDS_FOLDERS | folder.HOLDS_MESSAGES);
            folder.open(folder.READ_WRITE);
            return folder;
        } catch (MessagingException ex) {
            throw new IOException(Support.stackTrace(ex));
        }
    }

    void move(MsgPipingThread piping, Object categoryObject)
        throws IOException {
        MsgItem msgItem = piping.msgs.get(piping.uniqueID);
        if (msgItem == null) return;
        try {
            Message message = ((W3Imap4Requester)requester).folder.getMessage(msgItem.index);
            ((W3Imap4Requester)requester).folder.copyMessages(new Message[] {message}, (Folder)categoryObject);
            message.setFlag(Flags.Flag.DELETED, true);
        } catch (MessagingException ex) {
            throw new IOException(Support.stackTrace(ex));
        }
        piping.msgs.remove(piping.uniqueID);
        new EntityCache(piping.cachePath + Support.encode(piping.uniqueID, null)).reset();
    }

    void closeCategoryObject(Object categoryObject) {
        if (categoryObject != null)
            try {
                ((Folder)categoryObject).close(true);
            } catch (MessagingException ex) {}
    }

    public static void cleanCache(ServletContext servletContext) {
        cacheCleaningTime = cleanCache(servletContext, cacheRoot, cacheCleaningInterval);
    }

    public W3Imap4URLConnection(URL url) {
        super(url);
        secure = url.getProtocol().equalsIgnoreCase("imap4s");
    }

    public void checkCacheCleaning() {
        if (cacheCleaningInterval >= 0L &&
            System.currentTimeMillis() - cacheCleaningTime > cacheCleaningInterval) cleanCache(servletContext);
    }

    public void open()
        throws IOException {
        if (requester != null || username.equals("") || password.equals("")) return;
        if (useProxy) {
            proxyConnect(proxyHost, proxyPort);
            return;
        }
        try {
            String category = getRequestProperty("x-category");
            requester = new W3Imap4Requester(host, url.getPort() != -1 ? url.getPort() : -1, username, password, category, secure);
            if (timeout > 0) requester.setTimeout(timeout);
            loggedIn = true;
            if (list()) return;
            first = Integer.MAX_VALUE;
            last = 0;
            Message messages[] = ((W3Imap4Requester)requester).folder.getMessages();
            for (int i = 0; i < messages.length; i++) {
                MimeMessage message = (MimeMessage)messages[i];
                if (msgs.containsKey(message.getMessageID())) continue;
                int n = message.getMessageNumber();
                HeaderList headerList = new HeaderList();
                Enumeration allHeaders = message.getAllHeaders();
                while (allHeaders.hasMoreElements()) {
                    javax.mail.Header header = (javax.mail.Header)allHeaders.nextElement();
                    headerList.append(new FI.realitymodeler.common.Header(header.getName(), header.getValue()));
                }
                long uid = ((W3Imap4Requester)requester).folder.getUID(message);
                headerList.append(new FI.realitymodeler.common.Header("-", String.valueOf(uid)));
                MsgItem msgItem = new MsgItem(headerList, null, null, n);
                if (useCaches && msgItem.msgId != null) {
                    String idEncoded = Support.encode(msgItem.msgId, null);
                    W3File file = new W3File(cachePath + idEncoded + "_");
                    if (!file.exists()) {
                        BufferedOutputStream fout = new BufferedOutputStream(new FileOutputStream(file));
                        Support.sendHeaderList(fout, headerList);
                        fout.close();
                    }
                }
                if (!message.isSet(Flags.Flag.SEEN)) headerList.replace(new FI.realitymodeler.common.Header("X-Unread", "true"));
                msgVec.addElement(msgItem);
                if (msgItem.msgId != null) msgs.put(msgItem.msgId, msgItem);
                first = Math.min(first, n);
                last = Math.max(last, n);
            }
            Collections.sort(msgVec, this);
            saveSession();
        } catch (MessagingException ex) {
            if (ex instanceof AuthenticationFailedException) return;
            throw new IOException(Support.stackTrace(ex));
        }
    }

    public void doConnect()
        throws IOException {
        if ((host = url.getHost()).equals("") && (host = imap4ServerHost) == null &&
            (host = System.getProperty("imap4.host")) == null) throw new IOException("No host");
        getBasicCredentials();
        if (username == null || username.equals("")) {
            in = origin = getUnauthorizedStream("Messaging authorization required");
            inputDone = true;
            return;
        }
        String category = getRequestProperty("x-category");
        cachePath = cacheRoot + host.toLowerCase() + "/" + username + "/" + (category != null && !(category = category.trim()).equals("") ? category + "/" : "");
        W3File dir = new W3File(cachePath.replace('/', File.separatorChar));
        if (!dir.exists()) dir.mkdirs();
        StringTokenizer st = new StringTokenizer(path, "/");
        int tc = st.countTokens();
        if (tc > 2) {
            // With attachments cache must be used always
            st.nextToken();
            setCheckCaches(true);
            if (checkCacheFile(cachePath + st.nextToken(), st.nextToken())) return;
        }
        open();
    }

    public void setDefaultUseCaches(boolean useCaches) {
        W3Imap4URLConnection.defaultUseCaches = useCaches;
    }

    public boolean getDefaultUseCaches() {
        return W3Imap4URLConnection.defaultUseCaches;
    }

    public int getDefaultPort() {
        return W3Imap4Requester.IMAP4_PORT;
    }

    public boolean usingProxy() {
        return useProxy;
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public int getProxyPort() {
        return proxyPort;
    }

    public void setCacheCleaningInterval(long cacheCleaningInterval) {
        W3Imap4URLConnection.cacheCleaningInterval = cacheCleaningInterval;
    }

}
