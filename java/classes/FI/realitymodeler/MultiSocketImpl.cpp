
/* Interface to WinSock2 Service Providers and Unix sockets */

#include <errno.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#ifdef _WIN32
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#ifdef __GNUC__
#include <winsock.h>
#else
#include <winsock2.h>
#include <WS2tcpip.h>
#endif
//#include <gsmsms.h>
//#include <ws2nbs.h>
#else
#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <unistd.h>
#endif
#include "FI_realitymodeler_MultiSocketImpl.h"
#ifndef ULONG
#define ULONG unsigned long
#endif
#ifdef _WIN32
#define in_addr_t ULONG
#define socklen_t int
#define SHUT_RD SD_RECEIVE
#define SHUT_WR SD_SEND
#else
#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#define WSAEADDRINUSE EADDRINUSE
#define WSAEADDRNOTAVAIL ENETUNREACH
#define WSAECONNREFUSED ECONNREFUSED
#define WSAEINPROGRESS EINPROGRESS
#define WSAHOST_NOT_FOUND HOST_NOT_FOUND
#define WSAEWOULDBLOCK EWOULDBLOCK
#define SOCKADDR struct sockaddr
#define SOCKADDR_IN struct sockaddr_in
#define SOCKADDR_IN6 struct sockaddr_in6
#define IN_ADDR struct sockaddr_in
#define LPSOCKADDR struct sockaddr *
#define LPSOCKADDR_IN struct sockaddr_in *
#define LPSOCKADDR_IN6 struct sockaddr_in6 *
#define WSAGetLastError() errno
#define WSAGetLastHostError() h_errno
#define closesocket(socket) close(socket)
#define ioctlsocket ioctl
#define SOCKET int
#endif

#define VALUE_IP_MULTICAST_IF 16
#define VALUE_IP_MULTICAST_IF2 31
#define VALUE_IP_MULTICAST_LOOP 18
#define VALUE_IP_TOS 3
#define VALUE_SO_BROADCAST 32
#define VALUE_SO_KEEPALIVE 8
#define VALUE_SO_LINGER 128
#define VALUE_SO_OOBINLINE 4099
#define VALUE_SO_RCVBUF 4098
#define VALUE_SO_REUSEADDR 4
#define VALUE_SO_SNDBUF 4097
#define VALUE_SO_TIMEOUT 4102
#define VALUE_TCP_NODELAY 1
#define LENGTH(array) (sizeof(array) / sizeof(*array))

static jclass inetAddressClass, inet6AddressClass, arrayIndexOutOfBoundsException, interruptedIOException, nullPointerException, bindException, connectException, noRouteToHostException, protocolException, socketException, unknownHostException, runtimeException,
    object, string, byteArray, w3SocketImpl;
static jfieldID addrID, afID, mdpDataID, mdpLengthID, mdpAddressID, mdpScopeIdID, mdpPortID, portID;
static jmethodID getByAddressID, getByAddress6ID, getDeclaredFieldID, setAccessibleID, setID, setIntID;
static jobject addressField, portField;
#ifdef _WIN32
#ifndef __GNUC__
static int numOfProtocols;
static LPWSAPROTOCOL_INFO protocolBuffer;
#endif
static LPSTR msgs[WSAEDISCON - WSABASEERR + 1],
  msgs1[WSANO_DATA - WSABASEERR - 1000 + 1];
#else
static struct addrinfo hints_alphabetic;
static struct addrinfo hints_numeric;
static struct addrinfo hints6_alphabetic;
static struct addrinfo hints6_numeric;
#endif
static struct timeval timeval_zero;

#ifdef _WIN32
static void throwNew(JNIEnv *env, jclass throwable, DWORD n)
{
    LPSTR msg = NULL;
    if (n >= WSABASEERR && n < WSABASEERR + LENGTH(msgs)) msg = msgs[n - WSABASEERR];
    else if (n >= WSABASEERR + 1000 && n < WSABASEERR + 1000 + LENGTH(msgs1)) msg = msgs1[n - WSABASEERR - 1000];
    size_t msgLen = msg ? strlen(msg) + 1 : 0;
    char str[32];
    _itoa(n, str, 10);
    LPSTR err = "Socket error ", msgBuf = NULL;
    size_t errLen = strlen(err);
    if (!(msgBuf = new char[errLen + strlen(str) + msgLen + 1])) {
        env->ThrowNew(throwable, "error");
        return;
    }
    strcpy(msgBuf, err);
    strcpy(msgBuf + errLen, str);
    if (msg) {
        strcpy(msgBuf + errLen + strlen(str), " ");
        strcpy(msgBuf + errLen + strlen(str) + 1, msg);
    }
    env->ThrowNew(throwable, msgBuf);
    delete msgBuf;
}
#else
static void throwNew(JNIEnv *env, jclass throwable, int n)
{
    env->ThrowNew(throwable, strerror(n));
}

static void throwNewHostError(JNIEnv *env, jclass throwable, int n)
{
    env->ThrowNew(throwable, gai_strerror(n));
}
#endif

static jclass getClass(JNIEnv *env, char *name)
{
    jclass clazz;
    if ((clazz = env->FindClass(name))) clazz = (jclass)env->NewGlobalRef(clazz);
    return clazz;
}

extern "C" JNIEXPORT jstring JNICALL Java_FI_realitymodeler_MultiSocketImpl_getHostName(JNIEnv *env, jclass clazz)
{
    char hostname[256];
    if (gethostname(hostname, sizeof hostname) == SOCKET_ERROR) {
        throwNew(env, socketException, WSAGetLastError());
        return NULL;
    }
    return env->NewStringUTF(hostname);
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_MultiSocketImpl_initialize(JNIEnv *env, jclass clazz)
{
    if (!(addrID = env->GetFieldID(clazz, "addr", "[B")) ||
        !(afID = env->GetFieldID(clazz, "af", "I")) ||
        !(inetAddressClass = getClass(env, (char *)"java/net/InetAddress")) ||
        !(inet6AddressClass = getClass(env, (char *)"java/net/Inet6Address")) ||
        !(getByAddressID = env->GetStaticMethodID(inetAddressClass, "getByAddress", "([B)Ljava/net/InetAddress;")) ||
        !(getByAddress6ID = env->GetStaticMethodID(inet6AddressClass, "getByAddress", "(Ljava/lang/String;[BI)Ljava/net/Inet6Address;")) ||
        !(clazz = getClass(env, (char *)"java/lang/Class")) ||
        !(getDeclaredFieldID = env->GetMethodID(clazz, "getDeclaredField", "(Ljava/lang/String;)Ljava/lang/reflect/Field;")) ||
        !(clazz = getClass(env, (char *)"java/lang/reflect/Field")) ||
        !(setAccessibleID = env->GetMethodID(clazz, "setAccessible", "(Z)V")) ||
        !(setID = env->GetMethodID(clazz, "set", "(Ljava/lang/Object;Ljava/lang/Object;)V")) ||
        !(setIntID = env->GetMethodID(clazz, "setInt", "(Ljava/lang/Object;I)V")) ||
        !(clazz = getClass(env, (char *)"FI/realitymodeler/MultiDatagramPacket")) ||
        !(mdpDataID = env->GetFieldID(clazz, "data", "[B")) ||
        !(mdpLengthID = env->GetFieldID(clazz, "length", "I")) ||
        !(mdpAddressID = env->GetFieldID(clazz, "address", "[B")) ||
        !(mdpScopeIdID = env->GetFieldID(clazz, "scopeId", "I")) ||
        !(mdpPortID = env->GetFieldID(clazz, "port", "I")) ||
        !(arrayIndexOutOfBoundsException = getClass(env, (char *)"java/lang/ArrayIndexOutOfBoundsException")) ||
        !(interruptedIOException = getClass(env, (char *)"java/io/InterruptedIOException")) ||
        !(nullPointerException = getClass(env, (char *)"java/lang/NullPointerException")) ||
        !(bindException = getClass(env, (char *)"java/net/BindException")) ||
        !(connectException = getClass(env, (char *)"java/net/ConnectException")) ||
        !(noRouteToHostException = getClass(env, (char *)"java/net/NoRouteToHostException")) ||
        !(protocolException = getClass(env, (char *)"java/net/ProtocolException")) ||
        !(socketException = getClass(env, (char *)"java/net/SocketException")) ||
        !(unknownHostException = getClass(env, (char *)"java/net/UnknownHostException")) ||
        !(runtimeException = getClass(env, (char *)"java/lang/RuntimeException")) ||
        !(object = getClass(env, (char *)"java/lang/Object")) ||
        !(string = getClass(env, (char *)"java/lang/String")) ||
        !(byteArray = getClass(env, (char *)"[B")) ||
        !(w3SocketImpl = getClass(env, (char *)"FI/realitymodeler/W3SocketImpl")) ||
        !(clazz = getClass(env, (char *)"java/net/SocketImpl"))) return;
    jstring fieldName = env->NewStringUTF("address");
    if (!fieldName) return;
    addressField = env->CallObjectMethod(clazz, getDeclaredFieldID, fieldName);
    if (env->ExceptionOccurred() || !(addressField = env->NewGlobalRef(addressField))) return;
    fieldName = env->NewStringUTF("port");
    if (!fieldName) return;
    portField = env->CallObjectMethod(clazz, getDeclaredFieldID, fieldName);
    if (env->ExceptionOccurred() || !(portField = env->NewGlobalRef(portField))) return;
    env->CallVoidMethod(addressField, setAccessibleID, JNI_TRUE);
    if (env->ExceptionOccurred()) return;
    env->CallVoidMethod(portField, setAccessibleID, JNI_TRUE);
    if (env->ExceptionOccurred()) return;
#ifdef _WIN32
    msgs[WSAEINTR - WSABASEERR] = "Interrupted function call";
    msgs[WSAEACCES - WSABASEERR] = "Permission denied";
    msgs[WSAEFAULT - WSABASEERR] = "Bad address";
    msgs[WSAEINVAL - WSABASEERR] = "Invalid argument";
    msgs[WSAEMFILE - WSABASEERR] = "Too many open files";
    msgs[WSAEWOULDBLOCK - WSABASEERR] = "Resource temporarily unavailable";
    msgs[WSAEINPROGRESS - WSABASEERR] = "Operation now in progress";
    msgs[WSAEALREADY - WSABASEERR] = "Operation already in progress";
    msgs[WSAENOTSOCK - WSABASEERR] = "Socket operation on non-socket",
        msgs[WSAEDESTADDRREQ - WSABASEERR] = "Destination address required";
    msgs[WSAEPROTOTYPE - WSABASEERR] = "Protocol wrong type for socket";
    msgs[WSAENOPROTOOPT - WSABASEERR] = "Bad protocol option";
    msgs[WSAEPROTONOSUPPORT - WSABASEERR] = "Protocol not supported";
    msgs[WSAESOCKTNOSUPPORT - WSABASEERR] = "Socket type not supported";
    msgs[WSAEOPNOTSUPP - WSABASEERR] = "Operation not supported";
    msgs[WSAEPFNOSUPPORT - WSABASEERR] = "Protocol family not supported";
    msgs[WSAEAFNOSUPPORT - WSABASEERR] = "Address family not supported by protocol family";
    msgs[WSAEADDRINUSE - WSABASEERR] = "Address already in use";
    msgs[WSAEADDRNOTAVAIL - WSABASEERR] = "Cannot assign requested address";
    msgs[WSAENETDOWN - WSABASEERR] = "Network is down";
    msgs[WSAENETUNREACH - WSABASEERR] = "Network is unreachable";
    msgs[WSAENETRESET - WSABASEERR] = "Network dropped connection on reset";
    msgs[WSAECONNABORTED - WSABASEERR] = "Software caused connection abort";
    msgs[WSAECONNRESET - WSABASEERR] = "Connection reset by peer";
    msgs[WSAENOBUFS - WSABASEERR] = "No buffer space available";
    msgs[WSAEISCONN - WSABASEERR] = "Socket is already connected";
    msgs[WSAENOTCONN - WSABASEERR] = "Socket is not connected";
    msgs[WSAESHUTDOWN - WSABASEERR] = "Cannot send after socket shutdown";
    msgs[WSAETIMEDOUT - WSABASEERR] = "Connection timed out";
    msgs[WSAECONNREFUSED - WSABASEERR] = "Connection refused";
    msgs[WSAEHOSTDOWN - WSABASEERR] = "Host is down";
    msgs[WSAEHOSTUNREACH - WSABASEERR] = "No route to host";
    msgs[WSAEPROCLIM - WSABASEERR] = "Too many processes";
    msgs[WSASYSNOTREADY - WSABASEERR] = "Network subsystem is unavailable";
    msgs[WSAVERNOTSUPPORTED - WSABASEERR] = "WINSOCK.DLL version out of range";
    msgs[WSANOTINITIALISED - WSABASEERR] = "Successfull WSAStartup not yet performed";
    msgs[WSAEDISCON - WSABASEERR] = "Graceful shutdown in progress";
    msgs1[WSAHOST_NOT_FOUND - WSABASEERR - 1000] = "Host not found";
    msgs1[WSATRY_AGAIN - WSABASEERR - 1000] = "Non-authorative host not found";
    msgs1[WSANO_RECOVERY - WSABASEERR - 1000] = "This is a non-recoverable error";
    msgs1[WSANO_DATA - WSABASEERR - 1000] = "Valid name, no data record of requested type";
    int rv;
    WSADATA data;
    WORD version = MAKEWORD(2, 2);
    if (rv = WSAStartup(version, &data)) {
        throwNew(env, runtimeException, rv);
        return;
    }
#ifndef __GNUC__
    DWORD bufferLength = 0;
    if (WSAEnumProtocols(NULL, NULL, &bufferLength) == SOCKET_ERROR &&
        (rv = WSAGetLastError()) != WSAENOBUFS) {
        throwNew(env, runtimeException, rv);
        return;
    }
    if (!(protocolBuffer = (LPWSAPROTOCOL_INFO)new char[bufferLength])) {
        env->ThrowNew(runtimeException, "Out of memory");
        return;
    }
    if ((numOfProtocols = WSAEnumProtocols(NULL, protocolBuffer, &bufferLength)) == SOCKET_ERROR)
        throwNew(env, runtimeException, WSAGetLastError());
#endif
#else
    hints_alphabetic.ai_family = AF_INET;
    //hints_alphabetic.ai_socktype = SOCK_STREAM;
    hints_alphabetic.ai_protocol = IPPROTO_TCP;
    hints_alphabetic.ai_flags = AI_CANONNAME;
    hints_numeric.ai_family = AF_INET;
    //hints_numeric.ai_socktype = SOCK_STREAM;
    hints_numeric.ai_protocol = IPPROTO_TCP;
    hints_numeric.ai_flags = AI_CANONNAME | AI_NUMERICHOST;
    hints6_alphabetic.ai_family = AF_INET6;
    //hints6_alphabetic.ai_socktype = SOCK_STREAM;
    hints6_alphabetic.ai_protocol = IPPROTO_TCP;
    hints6_alphabetic.ai_flags = AI_CANONNAME | AI_V4MAPPED;
    hints6_numeric.ai_family = AF_INET6;
    //hints6_numeric.ai_socktype = SOCK_STREAM;
    hints6_numeric.ai_protocol = IPPROTO_TCP;
    hints6_numeric.ai_flags = AI_CANONNAME | AI_V4MAPPED | AI_NUMERICHOST;
#endif
}

extern "C" JNIEXPORT jlong JNICALL Java_FI_realitymodeler_MultiSocketImpl_getTimeval(JNIEnv *env, jclass clazz, jlong timeval, jint millis)
{
    if (timeval) delete (struct timeval *)timeval;
    if (millis == 0) return 0;
    struct timeval *tv = (struct timeval *)new char[sizeof(struct timeval)];
    if (!tv) {
        env->ThrowNew(runtimeException, "Out of memory");
        return 0;
    }
    tv->tv_sec = millis / 1000;
    tv->tv_usec = millis % 1000 * 1000;
    return (jlong)tv;
}

extern "C" JNIEXPORT jlong JNICALL Java_FI_realitymodeler_MultiSocketImpl_getProtocolInfo0(JNIEnv *env, jclass clazz, jstring protocolName)
{
#if defined(_WIN32) && !defined(__GNUC__)
    if (!protocolName) {
        env->ThrowNew(nullPointerException, "");
        return NULL;
    }
    const char *s = env->GetStringUTFChars(protocolName, NULL);
    if (!s) return 0;
    int n;
    for (n = 0; n < numOfProtocols && strcmp(s, protocolBuffer[n].szProtocol); n++);
    env->ReleaseStringUTFChars(protocolName, s);
    if (n == numOfProtocols) {
        env->ThrowNew(protocolException, "Unknown protocol name");
        return 0;
    }
    return (jlong)(protocolBuffer + n);
#else
    return 0;
#endif
}

#ifndef _WIN32
static jobjectArray makeArrayFromAddrinfos(JNIEnv *env, struct addrinfo *ai)
{
    int n;
    struct addrinfo *p;
    jbyteArray addr;
    jobjectArray array, nameArray, addrArray;
    jintArray scopeArray;
    jstring name;
    if (!(array = env->NewObjectArray(3, object, NULL))) return NULL;
    for (n = 0, p = ai; p; n++, p = p->ai_next);
    if (!(nameArray = env->NewObjectArray(1, string, NULL))) return NULL;
    if (!(addrArray = env->NewObjectArray(n, byteArray, NULL))) return NULL;
    if (!(scopeArray = env->NewIntArray(n))) return NULL;
    for (n = 0, p = ai; p; n++, p = p->ai_next) {
        if (!n) {
            char *canonname = p->ai_canonname;
            if (!canonname) canonname = (char *)"";
            if (!(name = env->NewStringUTF(canonname))) return NULL;
            else env->SetObjectArrayElement(nameArray, n, name);
        }
        switch (p->ai_family) {
        case AF_INET: {
            int size = sizeof(((LPSOCKADDR_IN)p->ai_addr)->sin_addr);
            if (!(addr = env->NewByteArray(size))) return NULL;
            env->SetByteArrayRegion(addr, 0, size, (jbyte *)&((LPSOCKADDR_IN)p->ai_addr)->sin_addr);
            break;
        }
        case AF_INET6: {
            int size = sizeof(((LPSOCKADDR_IN6)p->ai_addr)->sin6_addr);
            if (!(addr = env->NewByteArray(size))) return NULL;
            env->SetByteArrayRegion(addr, 0, size, (jbyte *)&((LPSOCKADDR_IN6)p->ai_addr)->sin6_addr);
            env->SetIntArrayRegion(scopeArray, n, 1, (jint *)&((LPSOCKADDR_IN6)p->ai_addr)->sin6_scope_id);
            break;
        }
        default:
            return NULL;
        }
        env->SetObjectArrayElement(addrArray, n, addr);
    }
    env->SetObjectArrayElement(array, 0, nameArray);
    env->SetObjectArrayElement(array, 1, addrArray);
    env->SetObjectArrayElement(array, 2, scopeArray);
    return array;
}
#endif

static jobjectArray makeArrayFromHostent(JNIEnv *env, struct hostent *he)
{
    int n;
    char **p;
    jbyteArray addr;
    jobjectArray array, nameArray, addrArray;
    if (!(array = env->NewObjectArray(2, object, NULL))) return NULL;
    for (n = 0, p = he->h_aliases; *p; n++, p++);
    if (!(nameArray = env->NewObjectArray(1 + n, string, NULL))) return NULL;
    jstring name = env->NewStringUTF(he->h_name);
    if (!name) return NULL;
    env->SetObjectArrayElement(nameArray, 0, name);
    for (n = 1, p = he->h_aliases; *p; n++, p++)
        if (!(name = env->NewStringUTF(*p))) return NULL;
        else env->SetObjectArrayElement(nameArray, n, name);
    for (n = 0, p = he->h_addr_list; *p; n++, p++);
    if (!(addrArray = env->NewObjectArray(n, byteArray, NULL))) return NULL;
    for (n = 0, p = he->h_addr_list; *p; n++, p++) {
        if (!(addr = env->NewByteArray(4))) return NULL;
        env->SetByteArrayRegion(addr, 0, 4, (jbyte *)*p);
        env->SetObjectArrayElement(addrArray, n, addr);
    }
    env->SetObjectArrayElement(array, 0, nameArray);
    env->SetObjectArrayElement(array, 1, addrArray);
    return array;
}

extern "C" JNIEXPORT jobjectArray JNICALL Java_FI_realitymodeler_MultiSocketImpl_getHostByAddr0(JNIEnv *env, jclass clazz, jbyteArray addr)
{
    if (!addr) {
        env->ThrowNew(nullPointerException, "");
        return NULL;
    }
    jbyte *ba = env->GetByteArrayElements(addr, NULL);
    if (!ba) return NULL;
    int rv;
    struct hostent *he = gethostbyaddr((char *)ba, 4, PF_INET);
    if (!he) rv = WSAGetLastError();
    env->ReleaseByteArrayElements(addr, ba, JNI_ABORT);
    if (!he) {
        throwNew(env, rv == WSAHOST_NOT_FOUND ? unknownHostException : socketException, rv);
        return NULL;
    }
    return makeArrayFromHostent(env, he);
}

extern "C" JNIEXPORT jobjectArray JNICALL Java_FI_realitymodeler_MultiSocketImpl_getHostByName0(JNIEnv *env, jclass clazz, jint af, jstring name)
{
    if (!name) {
        env->ThrowNew(nullPointerException, "");
        return NULL;
    }
    const char *s = env->GetStringUTFChars(name, NULL);
    if (!s) return 0;
    int rv;
    size_t length = strlen(s);
#ifdef _WIN32
    struct hostent *he;
    /*
    if (length && isdigit(s[length - 1])) {
        in_addr_t ia = inet_addr(s);
        he = gethostbyaddr((char *)&ia, sizeof(SOCKADDR_IN), AF_INET);
    }
    else */
    he = gethostbyname(s);
    if (!he) rv = WSAGetLastError();
    env->ReleaseStringUTFChars(name, s);
    if (!he) {
        throwNew(env, rv == WSAHOST_NOT_FOUND ? unknownHostException : socketException, rv);
        return NULL;
    }
    return makeArrayFromHostent(env, he);
#else
    struct addrinfo *hints, *res;
    if (af == AF_INET)
        if (length && isdigit(s[length - 1])) hints = &hints_numeric;
        else hints = &hints_alphabetic;
    else if (strchr(s, ':')) hints = &hints6_numeric;
    else hints = &hints6_alphabetic;
    rv = getaddrinfo(s, NULL, hints, &res);
    env->ReleaseStringUTFChars(name, s);
    if (rv) {
        throwNewHostError(env, rv == WSAHOST_NOT_FOUND ? unknownHostException : socketException, rv);
        return NULL;
    }
    jobjectArray array = makeArrayFromAddrinfos(env, res);
    freeaddrinfo(res);
    if (!array) env->ThrowNew(protocolException, "Unknown protocol");
    return array;
#endif
}

extern "C" JNIEXPORT jint JNICALL Java_FI_realitymodeler_MultiSocketImpl_getDefaultAddressFamily0(JNIEnv *env, jclass clazz)
{
#ifdef __APPLE__
    return AF_INET6;
#else
    return AF_INET;
#endif
}

extern "C" JNIEXPORT jbyteArray JNICALL Java_FI_realitymodeler_MultiSocketImpl_getLoopbackAddress0(JNIEnv *env, jclass clazz, jint af)
{
    jbyteArray addr = NULL;
    switch (af) {
    case AF_INET: {
        in_addr_t loopbackInAddr = INADDR_LOOPBACK;
        int size = sizeof(INADDR_LOOPBACK);
        if (!(addr = env->NewByteArray(size))) return NULL;
        env->SetByteArrayRegion(addr, 0, size, (jbyte *)&loopbackInAddr);
        break;
    }
    case AF_INET6: {
        int size = sizeof(in6addr_loopback);
        if (!(addr = env->NewByteArray(size))) return NULL;
        env->SetByteArrayRegion(addr, 0, size, (jbyte *)&in6addr_loopback);
        break;
    }
    }
    return addr;
}

extern "C" JNIEXPORT jbyteArray JNICALL Java_FI_realitymodeler_MultiSocketImpl_getAnyLocalAddress0(JNIEnv *env, jclass clazz, jint af)
{
    jbyteArray addr = NULL;
    switch (af) {
    case AF_INET: {
        in_addr_t anyInAddr = INADDR_ANY;
        int size = sizeof(INADDR_ANY);
        if (!(addr = env->NewByteArray(size))) return NULL;
        env->SetByteArrayRegion(addr, 0, size, (jbyte *)&anyInAddr);
        break;
    }
    case AF_INET6: {
        int size = sizeof(in6addr_any);
        if (!(addr = env->NewByteArray(size))) return NULL;
        env->SetByteArrayRegion(addr, 0, size, (jbyte *)&in6addr_any);
        break;
    }
    }
    return addr;
}

extern "C" JNIEXPORT jint JNICALL Java_FI_realitymodeler_MultiSocketImpl_select0(JNIEnv *env, jclass clazz, jlong readFds, jlong writeFds, jlong exceptFds, jint sec, jint usec)
{
    struct timeval *tv = NULL;
    if (sec || usec) {
        tv = (struct timeval *)new char[sizeof(struct timeval)];
        tv->tv_sec = sec;
        tv->tv_usec = usec;
    }
    int n;
    if ((n = select(FD_SETSIZE, (fd_set *)readFds, (fd_set *)writeFds, (fd_set *)exceptFds, tv)) == SOCKET_ERROR)
        throwNew(env, socketException, WSAGetLastError());
    if (tv) delete tv;
    return n;
}

extern "C" JNIEXPORT jint JNICALL Java_FI_realitymodeler_MultiSocketImpl_create0(JNIEnv *env, jobject obj, jint af, jint type, jint protocol, jlong protocolInfo, jint group, jint flags)
{
    SOCKET fd;
#if defined(_WIN32) && !defined(__GNUC__)
    if ((fd = WSASocket(af, type, protocol, (LPWSAPROTOCOL_INFO)protocolInfo, group, flags)) == INVALID_SOCKET) {
        throwNew(env, socketException, WSAGetLastError());
        return 0;
    }
    setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, "yes", 4);
    if (af == FROM_PROTOCOL_INFO && protocolInfo)
        env->SetIntField(obj, afID, ((LPWSAPROTOCOL_INFO)protocolInfo)->iAddressFamily);
#else
    if ((fd = socket(af, type, protocol)) < 0) {
        throwNew(env, socketException, WSAGetLastError());
        return 0;
    }
    setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, "yes", 4);
#endif
    return (jint)fd;
}

static LPSOCKADDR getSockAddr(JNIEnv *env, jint af, jbyteArray addr, int scopeId, int port, int *size)
{
    jbyte *ba;
    jsize n = 0;
    LPSOCKADDR_IN sin = NULL;
    if (addr) {
        ba = env->GetByteArrayElements(addr, NULL);
        if (!ba) return NULL;
        n = env->GetArrayLength(addr);
    }
    switch (af) {
    case AF_INET:
    case AF_INET6: {
#ifdef _WIN32
        *size = sizeof(SOCKADDR_IN);
#else
        if (af == AF_INET)
            *size = sizeof(SOCKADDR_IN);
        else *size = sizeof(SOCKADDR_IN6);
#endif
        sin = (LPSOCKADDR_IN)new char[*size];
        if (!sin) break;
        if (addr) {
            if (af == AF_INET) {
                if (n > sizeof(sin->sin_addr)) {
                    delete sin;
                    n = -1;
                    break;
                }
            } else {
                if (n > sizeof(((LPSOCKADDR_IN6)sin)->sin6_addr)) {
                    delete sin;
                    n = -1;
                    break;
                }
            }
        }
        memset(sin, 0, *size);
#ifdef _WIN32
        if (addr) memcpy(&sin->sin_addr, ba, n);
#else
        if (addr)
            if (af == AF_INET)
                memcpy(&sin->sin_addr, ba, n);
            else memcpy(&((LPSOCKADDR_IN6)sin)->sin6_addr, ba, n);
#endif
        else if (af == AF_INET)
            sin->sin_addr.s_addr = INADDR_ANY;
        else ((LPSOCKADDR_IN6)sin)->sin6_addr = in6addr_any;
        if (af == AF_INET) {
#ifdef _WIN32
            sin->sin_family = (ADDRESS_FAMILY)af;
#else
            sin->sin_family = af;
#endif
            sin->sin_port = htons((short)port);
#ifdef __APPLE__
            sin->sin_len = *size;
#endif
        } else {
#ifdef _WIN32
            ((LPSOCKADDR_IN6)sin)->sin6_family = (ADDRESS_FAMILY)af;
#else
            ((LPSOCKADDR_IN6)sin)->sin6_family = af;
#endif
            ((LPSOCKADDR_IN6)sin)->sin6_port = htons((short)port);
            ((LPSOCKADDR_IN6)sin)->sin6_scope_id = scopeId;
#ifdef __APPLE__
            ((LPSOCKADDR_IN6)sin)->sin6_len = *size;
#endif
        }
        break;
    }
        /*
          case AF_NBS: {
          if (n > MAX_MSISDN_LEN) {
          n = -1;
          break;
          }
          *size = sizeof(struct sockaddr_nbs);
          sin = (LPSOCKADDR_IN)new char[*size];
          if (!sin) break;
          memset(sin, 0, *size);
          ((sockaddr_nbs *)sin)->s_nbs_family = AF_NBS;
          ((sockaddr_nbs *)sin)->s_nbs_port = htons((short)port);
          memcpy(((sockaddr_nbs *)sin)->s_nbs_addr.s_un.s_isdn.s_isdn, ba, n);
          break;
          }
#ifndef __GNUC__
    case AF_GSMSMS: {
        if (n > MAX_DEST_ADDR_LEN) {
            n = -1;
            break;
        }
        *size = sizeof(struct sockaddr_gsm);
        sin = (LPSOCKADDR_IN)new char[*size];
        if (!sin) break;
        memset(sin, 0, *size);
        ((struct sockaddr_gsm *)sin)->sgsm_family = AF_GSMSMS;
        memcpy(((struct sockaddr_gsm *)sin)->sgsm_msisdn, ba, n);
        break;
    }
#endif
        */
    default:
        *size = 0;
        break;
    }
    if (addr) env->ReleaseByteArrayElements(addr, ba, JNI_ABORT);
    if (!*size) {
        env->ThrowNew(protocolException, "Unknown protocol");
        return NULL;
    }
    if (n == -1) {
        env->ThrowNew(protocolException, "Too long address");
        return NULL;
    }
    if (!sin) {
        env->ThrowNew(runtimeException, "Out of memory");
        return NULL;
    }
    return (SOCKADDR *)sin;
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_MultiSocketImpl_connect0(JNIEnv *env, jobject obj, jint fd, jint af, jbyteArray addr, jint scopeId, jint port, jint timeout)
{
    if (!addr) {
        env->ThrowNew(nullPointerException, "");
        return;
    }
    int size;
    LPSOCKADDR sa = getSockAddr(env, af, addr, scopeId, port, &size);
    if (!sa) return;
    if (timeout > 0) {
#ifdef _WIN32
        u_long iMode = 1;
        if (ioctlsocket(fd, FIONBIO, &iMode) == SOCKET_ERROR) {
            int rv = WSAGetLastError();
            throwNew(env, socketException, rv);
        }
#else
        int flags = fcntl(fd, F_GETFL, NULL);
        if (flags == SOCKET_ERROR) {
            int rv = WSAGetLastError();
            throwNew(env, socketException, rv);
        }
        flags |= O_NONBLOCK;
        if (fcntl(fd, F_SETFL, flags) == SOCKET_ERROR) {
            int rv = WSAGetLastError();
            throwNew(env, socketException, rv);
        }
#endif
    }
    if (connect(fd, sa, size) == SOCKET_ERROR) {
        int rv = WSAGetLastError();
        if (timeout > 0 && (rv == WSAEINPROGRESS || rv == WSAEWOULDBLOCK)) {
            struct timeval *tv = NULL;
            tv = (struct timeval *)new char[sizeof(struct timeval)];
            tv->tv_sec = timeout / 1000;
            tv->tv_usec = timeout % 1000 * 1000;
            int n = 0;
            fd_set fds;
            FD_ZERO(&fds);
            FD_SET(fd, &fds);
            if ((n = select(FD_SETSIZE, NULL, &fds, NULL, tv)) == SOCKET_ERROR)
                throwNew(env, socketException, WSAGetLastError());
            if (n == 0) env->ThrowNew(interruptedIOException, "connect timeout");
            delete tv;
        } else throwNew(env, rv == WSAEADDRNOTAVAIL ? noRouteToHostException :
                        rv == WSAECONNREFUSED ? connectException : socketException, rv);
    }
    if (timeout > 0) {
#ifdef _WIN32
        u_long iMode = 0;
        ioctlsocket(fd, FIONBIO, &iMode);
#else
        int flags = fcntl(fd, F_GETFL, NULL);
        flags &= (~O_NONBLOCK);
        fcntl(fd, F_SETFL, flags);
#endif
    }
    delete sa;
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_MultiSocketImpl_bind0(JNIEnv *env, jobject obj, jint fd, jint af, jbyteArray addr, jint scopeId, jint port)
{
    int size;
    LPSOCKADDR sa = getSockAddr(env, af, addr, scopeId, port, &size);
    if (!sa) return;
    if (bind(fd, sa, size) == SOCKET_ERROR) {
        int rv = WSAGetLastError();
        throwNew(env, rv == WSAEADDRINUSE ? bindException : socketException, rv);
    }
    delete sa;
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_MultiSocketImpl_listen0(JNIEnv *env, jobject obj, jint fd, jint backlog)
{
    if (listen(fd, backlog) == SOCKET_ERROR) throwNew(env, socketException, WSAGetLastError());
}

static LPSOCKADDR_IN makeSockAddr(JNIEnv *env, jint af, socklen_t *size)
{
    switch (af) {
    case AF_INET:
        *size = sizeof(SOCKADDR_IN);
        break;
    case AF_INET6:
        *size = sizeof(SOCKADDR_IN6);
        break;
        /*
          case AF_NBS:
          *size = sizeof(struct sockaddr_nbs);
          break;
#ifndef __GNUC__
    case AF_GSMSMS:
        *size = sizeof(struct sockaddr_gsm);
        break;
#endif
        */
    default:
        *size = 0;
        env->ThrowNew(protocolException, "Unknown protocol");
        return NULL;
    }
    LPSOCKADDR_IN sin = (LPSOCKADDR_IN)new char[*size];
    if (!sin) env->ThrowNew(runtimeException, "Out of memory");
    else memset(sin, 0, *size);
    return sin;
}

static jbyteArray takeSockAddr(JNIEnv *env, jint af, LPSOCKADDR_IN sin, jint &scopeId, jint &port)
{
    jbyteArray addr;
    switch (af) {
    case AF_INET: {
        port = sin->sin_port;
        int size = sizeof(sin->sin_addr);
        if (!(addr = env->NewByteArray(size))) return NULL;
        env->SetByteArrayRegion(addr, 0, size, (jbyte *)&sin->sin_addr);
        break;
    }
    case AF_INET6: {
        port = sin->sin_port;
        int size = sizeof(((LPSOCKADDR_IN6)sin)->sin6_addr);
        if (!(addr = env->NewByteArray(size))) return NULL;
        env->SetByteArrayRegion(addr, 0, size, (jbyte *)&((LPSOCKADDR_IN6)sin)->sin6_addr);
        scopeId = ((LPSOCKADDR_IN6)sin)->sin6_scope_id;
        break;
    }
        /*
          case AF_NBS: {
          port = ((struct sockaddr_nbs *)sin)->s_nbs_port;
          int len = strlen((char *)((struct sockaddr_nbs *)sin)->s_nbs_addr.s_un.s_isdn.s_isdn);
          if (!(addr = env->NewByteArray(len))) return NULL;
          env->SetByteArrayRegion(addr, 0, len, (jbyte *)((struct sockaddr_nbs *)sin)->s_nbs_addr.s_un.s_isdn.s_isdn);
          break;
          }
#ifndef __GNUC__
    case AF_GSMSMS: {
        port = 0;
        int len = strlen((char *)((struct sockaddr_gsm *)sin)->sgsm_msisdn);
        if (!(addr = env->NewByteArray(len))) return NULL;
        env->SetByteArrayRegion(addr, 0, len, (jbyte *)((struct sockaddr_gsm *)sin)->sgsm_msisdn);
        break;
    }
#endif
        */
    default:
        env->ThrowNew(protocolException, "Unknown protocol");
        return NULL;
    }
    return addr;
}

extern "C" JNIEXPORT jint JNICALL Java_FI_realitymodeler_MultiSocketImpl_accept0(JNIEnv *env, jobject obj, jint fd, jint af, jlong timeval, jobject impl)
{
    if (!impl) {
        env->ThrowNew(nullPointerException, "");
        return -1;
    }
    SOCKET fd1;
    socklen_t size;
    LPSOCKADDR_IN sin = makeSockAddr(env, af, &size);
    if (!sin) return -1;
    int n = 0;
    if (timeval) {
        fd_set fds;
        FD_ZERO(&fds);
        FD_SET(fd, &fds);
        if ((n = select(FD_SETSIZE, &fds, &fds, &fds, (struct timeval *)timeval)) == 0)
            env->ThrowNew(interruptedIOException, "accept timeout");
    }
    if (n != SOCKET_ERROR && (fd1 = accept(fd, (SOCKADDR *)sin, &size)) != INVALID_SOCKET) {
        jint port, scopeId;
        jbyteArray addr;
        if ((addr = takeSockAddr(env, af, sin, scopeId, port)))
            for (;;) {
                if (env->IsInstanceOf(impl, w3SocketImpl)) env->SetObjectField(impl, addrID, addr);
                env->CallVoidMethod(portField, setIntID, impl, port);
                if (env->ExceptionOccurred()) break;
                if (af == AF_INET || af == AF_INET6) {
                    jobject address = NULL;
                    if (af == AF_INET6)
                        address = env->CallStaticObjectMethod(inet6AddressClass, getByAddress6ID, NULL, addr, scopeId);
                    else address = env->CallStaticObjectMethod(inetAddressClass, getByAddressID, addr);
                    if (env->ExceptionOccurred()) break;
                    env->CallVoidMethod(addressField, setID, impl, address);
                    if (env->ExceptionOccurred()) break;
                }
                delete sin;
                return (jint)fd1;
            }
        closesocket(fd1);
        fd1 = -1;
    }
    else throwNew(env, socketException, WSAGetLastError());
    delete sin;
    return (jint)fd1;
}

extern "C" JNIEXPORT jint JNICALL Java_FI_realitymodeler_MultiSocketImpl_available0(JNIEnv *env, jobject obj, jint fd)
{
    ULONG ret;
    if (ioctlsocket(fd, FIONREAD, &ret) == SOCKET_ERROR) throwNew(env, socketException, WSAGetLastError());
    return ret;
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_MultiSocketImpl_close0(JNIEnv *env, jobject obj, jint fd, jlong timeval)
{
    if (fd <= 0) return;
    if (closesocket(fd) == SOCKET_ERROR) throwNew(env, socketException, WSAGetLastError());
    if (timeval) delete (struct timeval *)timeval;
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_MultiSocketImpl_shutdownInput0(JNIEnv *env, jobject obj, jint fd)
{
    if (fd <= 0) return;
    if (shutdown(fd, SHUT_RD) == SOCKET_ERROR) throwNew(env, socketException, WSAGetLastError());
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_MultiSocketImpl_shutdownOutput0(JNIEnv *env, jobject obj, jint fd)
{
    if (fd <= 0) return;
    if (shutdown(fd, SHUT_WR) == SOCKET_ERROR) throwNew(env, socketException, WSAGetLastError());
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_MultiSocketImpl_setOption0(JNIEnv *env, jobject obj, jint fd, jint option, jboolean flag, jint value)
{
    char *pval;
    int len, val;
    struct linger ling;
    switch (option) {
    case VALUE_SO_LINGER:
        option = SO_LINGER;
        ling.l_onoff = flag;
        ling.l_linger = (unsigned short)value;
        pval = (char *)&ling;
        len = sizeof ling;
        break;
    case VALUE_SO_REUSEADDR:
    case VALUE_SO_KEEPALIVE:
    case VALUE_SO_RCVBUF:
    case VALUE_SO_SNDBUF:
    case VALUE_TCP_NODELAY:
        option = option == VALUE_TCP_NODELAY ? TCP_NODELAY : option == VALUE_SO_KEEPALIVE ? SO_KEEPALIVE : option == VALUE_SO_REUSEADDR ? SO_REUSEADDR : option == VALUE_SO_RCVBUF ? SO_RCVBUF : SO_SNDBUF;
        val = (int)value;
        pval = (char *)&val;
        len = sizeof val;
        break;
        /*
          case VALUE_TCP_NODELAY:
          option = TCP_NODELAY;
          b = flag;
          pval = &b;
          len = 1;
          break;
        */
    default:
        env->ThrowNew(socketException, "Illegal option");
        return;
    }
    if (setsockopt(fd, SOL_SOCKET, option, pval, len) == SOCKET_ERROR) throwNew(env, socketException, WSAGetLastError());
}

extern "C" JNIEXPORT jint JNICALL Java_FI_realitymodeler_MultiSocketImpl_getOption0(JNIEnv *env, jobject obj, jint fd, jint option)
{
    char b, *val;
    int i;
    socklen_t len;
    struct linger ling;
    switch (option) {
    case VALUE_SO_KEEPALIVE:
    case VALUE_SO_RCVBUF:
    case VALUE_SO_SNDBUF:
        option = option == VALUE_SO_KEEPALIVE ? SO_KEEPALIVE : option == VALUE_SO_RCVBUF ? SO_RCVBUF : SO_SNDBUF;
        val = (char *)&i;
        len = sizeof i;
        break;
    case VALUE_SO_LINGER:
        option = SO_LINGER;
        val = (char *)&ling;
        len = sizeof ling;
        break;
    case VALUE_TCP_NODELAY:
        option = TCP_NODELAY;
        val = (char *)&b;
        len = sizeof b;
        break;
    default:
        env->ThrowNew(socketException, "Illegal option");
        return 0;
    }
    if (getsockopt(fd, SOL_SOCKET, option, val, &len) == SOCKET_ERROR)
        throwNew(env, socketException, WSAGetLastError());
    switch (option) {
    case SO_KEEPALIVE:
    case SO_RCVBUF:
    case SO_SNDBUF:
        return i;
    case SO_LINGER:
        return ling.l_onoff ? ling.l_linger : -1;
    case TCP_NODELAY:
        return b;
    }
    return 0;
}

extern "C" JNIEXPORT jint JNICALL Java_FI_realitymodeler_MultiSocketImpl_read0(JNIEnv *env, jobject obj, jint fd, jlong timeval, jbyteArray buf, jint off, jint len)
{
    if (len <= 0) return 0;
    if (!buf) {
        env->ThrowNew(nullPointerException, "");
        return 0;
    }
    if (off < 0 || off + len > env->GetArrayLength(buf)) {
        env->ThrowNew(arrayIndexOutOfBoundsException, "");
        return 0;
    }
    jbyte *ba = env->GetByteArrayElements(buf, NULL);
    if (!ba) return 0;
    int n = 0;
    if (timeval) {
        fd_set fds;
        FD_ZERO(&fds);
        FD_SET(fd, &fds);
        if ((n = select(FD_SETSIZE, &fds, NULL, &fds, (struct timeval *)timeval)) == 0)
            env->ThrowNew(interruptedIOException, "read timeout");
    }
    if (n == SOCKET_ERROR || (n = recv(fd, (char *)ba + off, len, 0)) == SOCKET_ERROR) {
        throwNew(env, socketException, WSAGetLastError());
        n = 0;
    }
    env->ReleaseByteArrayElements(buf, ba, 0);
    return n;
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_MultiSocketImpl_write0(JNIEnv *env, jobject obj, jint fd, jbyteArray buf, jint off, jint len)
{
    if (len <= 0) return;
    if (!buf) {
        env->ThrowNew(nullPointerException, "");
        return;
    }
    if (off < 0 || off + len > env->GetArrayLength(buf)) {
        env->ThrowNew(arrayIndexOutOfBoundsException, "");
        return;
    }
    jbyte *ba = env->GetByteArrayElements(buf, NULL);
    if (!ba) return;
    for (int n; len > 0; off +=n, len -= n)
        if ((n = send(fd, (char *)ba + off, len, 0)) == SOCKET_ERROR) {
            throwNew(env, socketException, WSAGetLastError());
            break;
        }
    env->ReleaseByteArrayElements(buf, ba, JNI_ABORT);
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_MultiSocketImpl_receive0(JNIEnv *env, jobject obj, jint fd, jint af, jlong timeval, jobject mdp)
{
    if (!mdp) {
        env->ThrowNew(nullPointerException, "");
        return;
    }
    socklen_t size;
    LPSOCKADDR_IN sin = makeSockAddr(env, af, &size);
    if (!sin) return;
    jbyteArray data = (jbyteArray)env->GetObjectField(mdp, mdpDataID);
    jbyte *ba = env->GetByteArrayElements(data, NULL);
    if (!ba) {
        delete sin;
        return;
    }
    jint length = env->GetIntField(mdp, mdpLengthID);
    int n = 0;
    if (timeval) {
        fd_set fds;
        FD_ZERO(&fds);
        FD_SET(fd, &fds);
        if ((n = select(FD_SETSIZE, &fds, NULL, &fds, (struct timeval *)timeval)) == 0)
            env->ThrowNew(interruptedIOException, "receive timeout");
    }
    if (n != SOCKET_ERROR && (n = recvfrom(fd, (char *)ba, length, 0, (LPSOCKADDR)sin, &size)) != SOCKET_ERROR) {
        jbyteArray addr;
        jint port, scopeId;
        if ((addr = takeSockAddr(env, af, sin, scopeId, port))) {
            env->SetObjectField(mdp, mdpAddressID, addr);
            env->SetIntField(mdp, mdpScopeIdID, scopeId);
            env->SetIntField(mdp, mdpPortID, port);
            env->SetIntField(mdp, mdpLengthID, n);
        }
    }
    else {
        throwNew(env, socketException, WSAGetLastError());
        n = 0;
    }
    env->ReleaseByteArrayElements(data, ba, 0);
    delete sin;
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_MultiSocketImpl_send0(JNIEnv *env, jobject obj, jint fd, jint af, jobject mdp)
{
    if (!mdp) {
        env->ThrowNew(nullPointerException, "");
        return;
    }
    jbyteArray addr = (jbyteArray)env->GetObjectField(mdp, mdpAddressID);
    jint scopeId = env->GetIntField(mdp, mdpScopeIdID),
        port = env->GetIntField(mdp, mdpPortID);
    int size;
    LPSOCKADDR sa = getSockAddr(env, af, addr, scopeId, port, &size);
    if (!sa) return;
    jbyteArray data = (jbyteArray)env->GetObjectField(mdp, mdpDataID);
    jbyte *ba = env->GetByteArrayElements(data, NULL);
    if (!ba) {
        delete sa;
        return;
    }
    jint length = env->GetIntField(mdp, mdpLengthID);
    int n;
    if ((n = sendto(fd, (char *)ba, length, 0, sa, size)) != SOCKET_ERROR)
        env->SetIntField(mdp, mdpLengthID, n);
    else throwNew(env, socketException, WSAGetLastError());
    env->ReleaseByteArrayElements(data, ba, JNI_ABORT);
    delete sa;
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_MultiSocketImpl_setIOControl0(JNIEnv *env, jobject obj, jint fd, jint af, jint code, jint value)
{
    switch ((unsigned)code) {
    case FIONBIO:
        break;
        /*
#ifndef __GNUC__
    case NBS_NOTIFY_MODE:
    case NBS_OPERATIONSTATUS:
    case NBS_ORDERED:
    case NBS_DATACODING:
    case NBS_VALIDITY:
    case NBS_SET_PARAMETERS:
    case NBS_CONCATINFO:
    case NBS_SETPARTIALRECV:
    case NBS_SETHEADERCODING:
    case NBS_SET_PROTSPECIFIC:
        if (af == AF_NBS) break;
#endif
        */
    default:
        env->ThrowNew(socketException, "Illegal option");
    }
    if (ioctlsocket(fd, code, (ULONG *)&value) == SOCKET_ERROR) throwNew(env, socketException, WSAGetLastError());
}

extern "C" JNIEXPORT jint JNICALL Java_FI_realitymodeler_MultiSocketImpl_getIOControl0(JNIEnv *env, jobject obj, jint fd, jint af, jint code)
{
#ifdef _WIN32
      /*
    switch (code) {
#ifndef __GNUC__
    case NBS_OPERATIONSTATUS:
    case NBS_VALIDITY:
    case NBS_GET_PARAMETERS:
    case NBS_GET_PROTSPECIFIC:
        break;
    case NBS_CONCATINFO: {
        if (af != AF_NBS) break;
        NBSSTRUCT_CONCAT concat;
        if (ioctlsocket(fd, code, (ULONG *)&concat) == SOCKET_ERROR) throwNew(env, socketException, WSAGetLastError());
        return (jint)concat.totalConcat;
    }
    case NBS_REFNUM:
    case NBS_GETHEADERCODING: {
        if (af != AF_NBS) break;
        ULONG value;
        if (ioctlsocket(fd, code, &value) == SOCKET_ERROR) throwNew(env, socketException, WSAGetLastError());
        return (jint)value;
    }
#endif
    default:
        env->ThrowNew(socketException, "Illegal option");
    }
      */
    env->ThrowNew(socketException, "Illegal option");
    return 0;
#else
    return 0;
#endif
}

extern "C" JNIEXPORT jint JNICALL Java_FI_realitymodeler_MultiSocketImpl_getLocalPort(JNIEnv *env, jobject obj, jint fd)
{
    SOCKADDR_IN sin;
    socklen_t len = sizeof(sin);
    if (getsockname(fd, (LPSOCKADDR)&sin, &len) == SOCKET_ERROR) {
        throwNew(env, socketException, WSAGetLastError());
        return 0;
    }
    return ntohs(sin.sin_port);
}

extern "C" JNIEXPORT jboolean JNICALL Java_FI_realitymodeler_MultiSocketImpl_isConnected0(JNIEnv *env, jobject obj, jint fd)
{
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(fd, &fds);
    return select(FD_SETSIZE, &fds, &fds, NULL, &timeval_zero) > 0
        && select(FD_SETSIZE, NULL, NULL, &fds, &timeval_zero) == 0;
}
