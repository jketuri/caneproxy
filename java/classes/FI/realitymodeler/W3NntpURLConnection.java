
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;
import javax.servlet.*;

class NntpPipingThread extends MsgPipingThread {
    Decoder decoder;
    W3NntpRequester nntpRequester;
    String articleID = null, dateValue = null, groupName = null, to = null;
    boolean list = false, only = false;

    NntpPipingThread(W3NntpURLConnection uc) {
        super(uc);
        this.cachePath = uc.cachePath;
        this.host = uc.host;
        this.nntpRequester = (W3NntpRequester)uc.nntpRequester.clone();
        this.query = uc.query;
    }

    public void run() {
        try {
            if (list) {
                if (groupName == null) {
                    String hostEncoded = URLEncoder.encode(host, uc.charsetName);
                    writeBytes("<html><head>\n");
                    writeBytes("<style type=\"text/css\">\n");
                    writeBytes("span.listcolumn0 { width: 10em; clear: left; float: left }\n");
                    writeBytes("span.listcolumn1 { width: 15em; float: left }\n");
                    writeBytes("span.listcolumn { width: 15em; float: left }\n");
                    writeBytes("</style>\n");
                    writeBytes("<title>" + host + "</title>\n");
                    writeBytes("<base target=\"_blank\">\n");
                    writeBytes("</head><body>\n");
                    writeBytes("<h1>" + MessageFormat.format(uc.messages.getString("groupsInHost"), new Object[] {host}) + "</h1>\n");
                    query = (query != null ? query + "&" : "") + "new=";
                    if (mobile) {
                        writeBytes("<form action=\"" + path + "\" method=get>\n");
                        writeBytes("<input name=ask type=hidden value=\"\">\n");
                        if (dateValue != null) writeBytes("<input name=date type=hidden value=\"" + dateValue + "\">\n");
                        if (from != null) writeBytes("<input name=from type=hidden value=\"" + from + "\">\n");
                        if (only) writeBytes("<input name=only type=hidden value=on>\n");
                        if (to != null) writeBytes("<input name=to type=hidden value=\"" + to + "\">\n");
                        writeBytes("<select name=\"group\">\n");
                    } else if (servlet) writeBytes("<a href=\"" + path.substring(0, path.lastIndexOf('/', path.length() - 1) + 1) + "?" + query + "\" target=_top>" + uc.messages.getString("main") + "</a><p>\n");
                    String s;
                    int n = 1;
                    while ((s = Support.readLine(source)) != null) {
                        StringTokenizer st = new StringTokenizer(s);
                        groupName = st.nextToken();
                        writeBytes("<!--start-->");
                        if (!mobile) {
                            writeBytes("<span class=\"listcolumn0\">" + String.valueOf(n++) + "." + "</span>\n");
                            try {
                                s = String.valueOf(Integer.parseInt(st.nextToken()) - Integer.parseInt(st.nextToken()) + 1);
                            } catch (NumberFormatException ex) {
                                s = "-";
                            }
                            writeBytes("<span class=\"listcolumn1\">" + s + "</span>\n");
                            writeBytes("<span class=\"listcolumn\"><a href=\"" + path + "?host=" + hostEncoded + "&group=" + URLEncoder.encode(groupName, uc.charsetName) + "&" + query + "\">" + Support.htmlString(groupName) + "</a></span>");
                        } else writeBytes("<option>" + groupName);
                        writeBytes("<br>\n<!--end-->");
                    }
                    if (mobile) writeBytes("</select></form>");
                    writeBytes("</body></html>");
                } else {
                    OutputStream htmlOut = Support.getHtmlOutputStream(out, from != null ? uc.servletPathTable : null, uc.charsetName);
                    String address[] = new String[2], others[] = new String[2],
                        hostEncoded = URLEncoder.encode(host, uc.charsetName),
                        groupNameEncoded = URLEncoder.encode(groupName, uc.charsetName),
                        groupNameString = Support.htmlString(groupName),
                        fromEncoded = from != null ? URLEncoder.encode(from, uc.charsetName) : "",
                        fromAddress = from != null ? uc.getAddress(address, from, host)[0] : null;
                    writeBytes("<html><head>\n");
                    writeBytes("<style type=\"text/css\">\n");
                    writeBytes("span.listcolumn0 { width: 20em; clear: left; float: left }\n");
                    writeBytes("span.listcolumn { width: 10em; float: left }\n");
                    writeBytes("</style>\n");
                    writeBytes("<title>" + groupNameString + "</title></head><body>\n");
                    writeBytes("<base target=\"message\">\n");
                    writeBytes("<h1>" + MessageFormat.format(uc.messages.getString("articlesInGroup"), new Object[] {groupNameString}) + "</h1>\n");
                    if (!contents)
                        if (mobile) {
                            writeBytes("<form action=\"" + path + "\" method=get>\n");
                            writeBytes("<select name=item>\n");
                        } else if (servlet) writeBytes("<a href=\"" + path.substring(0, path.lastIndexOf('/', path.length() - 2) + 1) +
                                                       (query != null ? "?" + query + "&" : "?") + "new=\" target=_top>" + uc.messages.getString("main") + "</a><br>\n" +
                                                       "<a href=\"" + path +
                                                       (query != null ? "?" + query + "&" : "?") + "contents=\" target=_top>" +
                                                       uc.messages.getString("getAllContents") + "</a><br>\n<hr>\n");
                    int listIndex = msgVec.size() - 1;
                    long listTime = 0L;
                    if (!msgVec.isEmpty()) {
                        MsgItem msgItem = msgVec.lastElement();
                        Date date = uc.parse(msgItem.headerList.getHeaderValue("date"));
                        listTime = date != null ? date.getTime() : 0L;
                    }
                    MsgItem msgItem = new MsgItem();
                    boolean fromList = false;
                    int total = 0;
                    for (uc.index0 = uc.last; uc.index0 >= uc.first && (!fromList || listIndex >= 0); uc.index0--) {
                        HeaderList headerList = null, headerFields = new HeaderList();
                        if (fromList) {
                            msgItem = msgVec.elementAt(listIndex--);
                            headerList = msgItem.headerList;
                        } else {
                            msgItem.index = uc.index0;
                            headerList = uc.getHeader(nntpRequester, msgItem);
                        }
                        if (headerList == null || !uc.getHeader(null, headerList, headerFields, false) && !forward) continue;
                        String articleID = msgItem.msgId,
                            articleIDEncoded = articleID != null ? Support.encode(articleID, null) : null;
                        String s;
                        Date date;
                        if ((s = headerList.getHeaderValue("date")) == null) date = null;
                        else if ((date = uc.parse(s)) != null && time != 0L && date.getTime() < time) break;
                        if (!fromList && useCaches) {
                            if (msgItem.time > 0L && msgItem.time < listTime) fromList = true;
                            else if (articleIDEncoded != null) {
                                W3File file = new W3File(cachePath + articleIDEncoded + "_");
                                if (!file.exists()) {
                                    BufferedOutputStream fout = new BufferedOutputStream(new FileOutputStream(file));
                                    Support.sendHeaderList(fout, headerList);
                                    fout.close();
                                }
                            }
                        }
                        writeBytes("<!--start-->");
                        if (contents) {
                            try {
                                source = uc.getEntity(uc.requester, msgItem);
                            } catch (IOException ex) {
                                uc.log(getClass().getName(), ex);
                                continue;
                            }
                            if (header) Support.writeHeader(out, htmlOut, headerList, uc.charsetName, true);
                            if (articleIDEncoded != null)
                                Support.decodeBody(uc.requester instanceof W3JarRequester ? source : new FeederInputStream(uc, source, uc.requester), out, htmlOut, headerList, headerFields, uc.decoder, uc.charsetName, msgItem.index + "/" + articleIDEncoded + "/", uc.requester instanceof W3JarRequester ? (EntityMemory)new EntityArchive(cachePath + articleIDEncoded) : (EntityMemory)new EntityCache(cachePath + articleIDEncoded), true, true, false, null);
                            htmlOut.write('\n');
                            htmlOut.flush();
                        } else if (!mobile) {
                            String to = uc.getAddress(headerList, address, host);
                            uc.getOthers(headerList, fromAddress, address[1], host, others);
                            if ((s = headerList.getHeaderValue("newsgroups")) != null) {
                                StringBuffer sb = new StringBuffer();
                                StringTokenizer st = new StringTokenizer(s, ",");
                                while (st.hasMoreTokens()) {
                                    if ((s = st.nextToken().trim()).equals("") || s.equals(groupName)) continue;
                                    if (sb.length() > 0) sb.append(',');
                                    sb.append(s);
                                }
                                if (sb.length() > 0) others[0] = others[0] != null ? sb.toString() + "," + others[0] : sb.toString();
                            }
                            to = to != null ? groupName + "," + to : groupName;
                            String href = path + uc.index0 + (articleIDEncoded != null ? "/" + articleIDEncoded : "") +
                                "?host=" + hostEncoded + "&from=" + fromEncoded +
                                "&group=" + groupNameEncoded + "&to=" + URLEncoder.encode(to, uc.charsetName) +
                                "&subject=" + (uc.subject != null ? URLEncoder.encode(uc.subject, uc.charsetName) : "") +
                                (others[0] != null ? "&alsoto=" + URLEncoder.encode(others[0], uc.charsetName) : "") +
                                (others[1] != null ? "&alsocc=" + URLEncoder.encode(others[1], uc.charsetName) : "") +
                                (signEncoded != null ? "&sign=" + signEncoded : "") +
                                (auto ? "&auto=" : "") + (header ? "&header=" : "");
                            writeBytes("<span class=\"listcolumn0\">");
                            uc.writeLine(out, date, (s = headerList.getHeaderValue("lines")) != null ? s : null, articleIDEncoded != null ? href + (forward ? "&forward=" : "") : null, target, uc.subject != null ? Support.htmlString(uc.subject) : "(Untitled)", uc.charsetName, mobile);
                            writeBytes(servlet ? "<a href=\"" + (from != null ? href + "&form=\" target=composition>" : "news:" + groupName + "\">") + (address[1] != null ? Support.htmlString(address[1]) : groupNameString) + "</a>" :
                                       address[1] != null ? Support.htmlString(address[1]) : groupNameString);
                            if ((s = headerList.getHeaderValue("organization")) != null && !(s = s.trim()).equals("")) Support.writeBytes(htmlOut, ", " + Support.decodeWords(s) + "\0", uc.charsetName);
                            writeBytes("<br>\n<hr>\n");
                        } else writeBytes("<option value=\"" + (uc.subject != null ? (uc.subject = Support.htmlString(uc.subject)) : "(Untitled)") + "-" + Integer.toString(uc.index0, Character.MAX_RADIX) + "\">" + (address[1] != null ? Support.htmlString(address[1]) : address[0] != null ? Support.htmlString(address[0]) + " " : "") + (uc.subject != null ? uc.subject : ""));
                        writeBytes("\n<!--end-->");
                        total++;
                    }
                    if (!contents)
                        if (mobile) writeBytes("</select></form>");
                    writeBytes(MessageFormat.format(uc.messages.getString("numberArticles"), new Object[] {new Long(total)}));
                    writeBytes("</body></html>");
                }
            } else uc.decode(source, out, nntpRequester, headerList, headerFields, decoder, cachePath + Support.encode(articleID, null), servlet ? path + "?" + query : null, path, header, false, from != null, !plain);
            out.close();
            if (entity != null) entity.setReadOnly();
        } catch (Exception ex) {
            try {
                if (in != null)
                    synchronized (in) {
                        in.close();
                        in.notifyAll();
                    }
            } catch (IOException ex1) {} finally {
                if (entity != null)
                    try {
                        entityOut.close();
                        entity.delete("");
                    } catch (IOException ex1) {}
                uc.log(getClass().getName(), ex);
            }
        }
    }

}

/** Implements URL connection to Network News Transport Protocol -servers. URL must be of form:<br>
    When getting input stream of message list in specific group or names of groups:<br>
    nntp://host[/group[/from]]<br>
    [?auto= : quote original automatically<br>
    from=reply email address]<br>
    When getting input stream of specific message:<br>
    nntp://host/group/article-number/article id<br>
    [?forward= : get message as raw text<br>
    &amp;header= : show message header<br>
    &amp;plain= : only plain text is returned from message]<br>
    When getting output stream for posting to specific groups:<br>
    nntp://host[/groups[/subject[/from]]]<br>
*/
public class W3NntpURLConnection extends W3MsgURLConnection {
    public static long cacheCleaningInterval = 24L * 60L * 60L * 1000L, cacheCleaningTime = 0L;
    public static String cacheRoot = "cache/nntp/";
    public static boolean defaultUseCaches = false, useProxy = false;
    public static String proxyHost = null;
    public static int proxyPort = 80;
    public static String nntpServerHost = null;
    public static String fromAddr = null;
    public static String version = "FI.realitymodeler.W3NntpURLConnection/2000-5-10";

    W3NntpRequester nntpRequester;
    NntpPipingThread piping = null;
    String host;
    boolean forward = false;

    public synchronized HeaderList getHeader(W3Requester requester, MsgItem msgItem) throws IOException {
        HeaderList headerList = new HeaderList();
        msgItem.headerList = headerList;
        try {
            ((W3NntpRequester)requester).getHeader(msgItem.index, headerList);
        } catch (IOException ex) {
            if (ex instanceof SocketException) throw ex;
            return null;
        }
        msgItem.msgId = headerList.getHeaderValue("message-id");
        Date date = parse(headerList.getHeaderValue("date"));
        msgItem.time = date != null ? date.getTime() : 0L;
        if (msgItem.index == last) last++;
        return headerList;
    }

    public synchronized InputStream getEntity(W3Requester requester, MsgItem msgItem) throws IOException {
        HeaderList headerList = new HeaderList();
        msgItem.headerList = headerList;
        if (requester == null) throw new IOException("closed");
        return ((W3NntpRequester)requester).getArticle(msgItem.msgId, headerList);
    }

    public static void cleanCache(ServletContext servletContext) {
        cacheCleaningTime = cleanCache(servletContext, cacheRoot, cacheCleaningInterval);
    }

    public W3NntpURLConnection(URL url) {
        super(url);
    }

    public String getCacheRoot() {
        return cacheRoot;
    }

    public void checkCacheCleaning() {
        if (cacheCleaningInterval >= 0L &&
            System.currentTimeMillis() - cacheCleaningTime > cacheCleaningInterval) cleanCache(servletContext);
    }

    public String getVersion() {
        return version;
    }

    public void open() throws IOException {
        if (requester != null) return;
        if (useProxy) {
            proxyConnect(proxyHost, proxyPort);
            return;
        }
        requester = nntpRequester = new W3NntpRequester(host, url.getPort() != -1 ? url.getPort() : W3NntpRequester.NNTP_PORT, localAddress, localPort);
        if (timeout > 0) nntpRequester.setTimeout(timeout);
    }

    public void doConnect()
        throws IOException {
        if ((host = url.getHost()).equals("") && (host = nntpServerHost) == null &&
            (host = System.getProperty("nntp.host")) == null) throw new IOException("No host");
        getBasicCredentials();
        String groupName = "";
        StringTokenizer st = new StringTokenizer(path, "/");
        int tc = st.countTokens();
        if (tc > 0) groupName = st.nextToken().trim().toLowerCase();
        cachePath = cacheRoot + host.toLowerCase() + "/" + groupName + "/";
        W3File dir = new W3File(cachePath.replace('/', File.separatorChar));
        if (!dir.exists()) dir.mkdirs();
        if (tc > 3) {
            st.nextToken();
            setCheckCaches(true);
            if (checkCacheFile(cachePath + st.nextToken(), st.nextToken())) return;
        }
        open();
    }

    public InputStream getInputStream() throws IOException {
        if (inputDone) return in;
        if (doOutput) {
            closeStreams();
            inputDone = true;
            useCaches = false;
            if (requester == null) throw new IOException("Not connected");
            if (useProxy && mayKeepAlive) {
                in = requester.input;
                checkInput();
                return in;
            }
            mayKeepAlive = true;
            headerFields = new HeaderList();
            headerFields.append(new Header("", protocol_1_1 + " " + HTTP_NO_CONTENT));
            return in = null;
        }
        connect();
        if (inputDone) return in;
        inputDone = true;
        setRequestMethod();
        setRequestFields();
        if (useProxy) return getInput(false);
        mayKeepAlive = true;
        filling = piping = new NntpPipingThread(this);
        String item = null;
        piping.forward = false;
        piping.mobile = isMobile();
        if (query != null) {
            HashMap<String,Object> queryPars = new HashMap<String,Object>();
            parseQuery(query, queryPars);
            piping.auto = Support.getParameter(queryPars, "auto") != null;
            piping.contents = Support.getParameter(queryPars, "contents") != null;
            piping.dateValue = Support.getParameter(queryPars, "date");
            piping.forward = Support.getParameter(queryPars, "forward") != null;
            piping.from = Support.getParameter(queryPars, "from");
            piping.groupName = Support.getParameter(queryPars, "group");
            piping.header = Support.getParameter(queryPars, "header") != null;
            item = Support.getParameter(queryPars, "item");
            piping.only = Support.getParameter(queryPars, "only") != null;
            piping.plain = Support.getParameter(queryPars, "plain") != null;
            if ((piping.signEncoded = Support.getParameter(queryPars, "sign")) != null)
                piping.signEncoded = URLEncoder.encode(piping.signEncoded, charsetName);
            piping.to = Support.getParameter(queryPars, "to");
        }
        piping.time = piping.only ? getTime(piping.dateValue, getCookies(getRequestHeaderFields())) : 0L;
        StringTokenizer st = new StringTokenizer(path, "/");
        if (st.hasMoreTokens()) {
            piping.groupName = st.nextToken().trim();
            if (st.hasMoreTokens()) {
                String s = st.nextToken().trim();
                if (s.indexOf('@') == -1) {
                    index = Integer.parseInt(s);
                    if (st.hasMoreTokens()) piping.articleID = Support.decode(st.nextToken());
                } else piping.from = s;
            }
        }
        if (piping.from != null && piping.from.equals("")) piping.from = null;
        checkCacheCleaning();
        piping.headerFields = new HeaderList();
        piping.headerFields.append(new Header("", protocol_1_1 + " " + HTTP_OK + " OK"));
        headerFields = piping.headerFields;
        if (piping.groupName != null && !piping.groupName.equals("")) {
            W3Newsgroup newsgroup = nntpRequester.getGroup(piping.groupName);
            first = newsgroup.firstArticle;
            last = newsgroup.lastArticle;
        }
        if (piping.articleID != null || item != null) {
            if (piping.mobile) piping.plain = true;
            if (item != null) index = Integer.parseInt(item.substring(item.lastIndexOf('-') + 1), Character.MAX_RADIX);
            MsgItem msgItem = new MsgItem(null, piping.articleID, piping.articleID, 0);
            in = getEntity(nntpRequester, msgItem);
            headerList = piping.headerList = msgItem.headerList;
            if (forward = piping.forward) {
                Support.copyHeaderList(piping.headerList, piping.headerFields);
                return checkMessage(true);
            }
            try {
                getHeader(in, piping.headerList, piping.headerFields, !piping.plain);
            } catch (ParseException ex) {
                throw new IOException(ex.toString());
            }
            piping.decoder = decoder;
            piping.list = false;
        } else {
            if (piping.groupName == null || piping.groupName.equals("")) {
                in = nntpRequester.getList();
                piping.groupName = null;
            } else list();
            piping.headerFields.replace(new Header("Content-Type", Support.htmlType));
            piping.headerFields.append(new Header("Cache-Control", "no-cache"));
            piping.headerFields.append(new Header("Pragma", "no-cache"));
            if (piping.only) setTime();
            piping.list = true;
            forward = false;
            Support.copyHeaderList(piping.headerFields, headerList = piping.headerList = new HeaderList());
        }
        if (params != null) piping.target = params.get("target");
        piping.servlet = requestRoot != null;
        piping.path = piping.servlet ? requestRoot : path.endsWith("/") ? "" : path.substring(path.lastIndexOf('/') + 1) + "/";
        piping.msgVec = msgVec;
        piping.setup(this);
        in = new BufferedInputStream(piping.in = new PipedInputStream(piping.out = new PipedOutputStream()));
        piping.start();
        return checkMessage(true);
    }

    public OutputStream getOutputStream() throws IOException {
        doOutput = true;
        connect();
        setRequestMethod();
        setDefaultRequestFields();
        if (useProxy) {
            Support.sendMessage(requester.output, requestHeaderFields);
            return requester.output;
        }
        String groupNames = null, from = null, subject = null;
        if (query != null) {
            HashMap<String,Object> queryPars = new HashMap<String,Object>();
            parseQuery(query, queryPars);
            groupNames = (String)queryPars.get("groupNames");
            from = (String)queryPars.get("from");
            subject = (String)queryPars.get("subject");
        }
        StringTokenizer st = new StringTokenizer(path, "/");
        if (st.hasMoreTokens()) {
            groupNames = st.nextToken().trim();
            if (st.hasMoreTokens()) {
                subject = st.nextToken().trim();
                if (st.hasMoreTokens()) from = st.nextToken().trim();
            }
        }
        if (from == null)
            if ((from = getRequestProperty("from")) != null) from = Support.decodeWords(from);
            else if ((from = fromAddr) == null) from = System.getProperty("user.fromaddr");
        if (getRequestProperty("from") == null) setRequestProperty("From", from != null ? Support.encodeWords(from) : "(Unknown)");
        if (from != null && getRequestProperty("reply-to") == null) {
            String address[] = new String[2];
            getAddress(address, from, host);
            if (address[0] != null) setRequestProperty("Reply-To", address[0]);
        }
        String s;
        if (groupNames != null) setRequestProperty("Newsgroups", groupNames);
        if ((s = getRequestProperty("subject")) == null || s.trim().equals(""))
            setRequestProperty("Subject", subject != null && !subject.trim().equals("") ? Support.encodeWords(subject) : "(Untitled)");
        if (getRequestProperty("date") == null) setRequestProperty("Date", Support.format(new Date()));
        if ((s = getRequestProperty("organization")) == null || s.trim().equals("")) setRequestProperty("Organization", "(None)");
        if (getRequestProperty("x-mailer") == null) setRequestProperty("X-Mailer", getVersion());
        OutputStream out = nntpRequester.startArticle();
        Support.sendHeaderList(out, requestHeaderFields, -1);
        return out;
    }

    public void setDefaultUseCaches(boolean useCaches) {
        W3NntpURLConnection.defaultUseCaches = useCaches;
    }

    public boolean getDefaultUseCaches() {
        return W3NntpURLConnection.defaultUseCaches;
    }

    public int getDefaultPort() {
        return W3NntpRequester.NNTP_PORT;
    }

    public boolean usingProxy() {
        return useProxy;
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public int getProxyPort() {
        return proxyPort;
    }

    public void setCacheCleaningInterval(long cacheCleaningInterval) {
        W3NntpURLConnection.cacheCleaningInterval = cacheCleaningInterval;
    }

}
