
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import java.io.*;
import java.lang.reflect.*;
import java.net.*;
import java.util.zip.*;

public class W3Socket extends Socket {
    public static final int NORMAL = 0, SECURE = 1, COMPRESSED = 2;

    static Field implField;
    static {
        try {
            implField = Socket.class.getDeclaredField("impl");
            implField.setAccessible(true);
        } catch (Exception ex) {
            throw new Error(ex.toString());
        }
    }

    public int type = NORMAL;

    protected InputStream in = null;
    protected OutputStream out = null;
    protected W3SocketImpl impl = null;

    protected W3Socket(SocketImpl impl)
        throws SocketException {
        super(impl);
    }

    public W3Socket()
        throws SocketException {
    }

    public W3Socket(int type)
        throws SocketException {
        this.type = type;
    }

    public W3Socket(String host, int port)
        throws UnknownHostException, IOException {
        this(host, port, null, 0, true, 0);
    }

    public W3Socket(InetAddress address, int port)
        throws IOException {
        this(address, port, null, 0, true, 0);
    }

    public W3Socket(String host, int port, InetAddress localAddress, int localPort)
        throws IOException {
        this(host, port, localAddress, localPort, true, 0);
    }

    public W3Socket(InetAddress address, int port, InetAddress localAddress, int localPort)
        throws IOException {
        this(address, port, localAddress, localPort, true, 0);
    }

    public W3Socket(String host, int port, boolean stream)
        throws IOException {
        this(host, port, null, 0, stream, 0);
    }

    public W3Socket(InetAddress address, int port, boolean stream)
        throws IOException {
        this(address, port, null, 0, stream, 0);
    }

    public W3Socket(String host, int port, int type)
        throws UnknownHostException, IOException {
        this(host, port, null, 0, true, type);
    }

    public W3Socket(InetAddress address, int port, int type)
        throws IOException {
        this(address, port, null, 0, true, type);
    }

    public W3Socket(String host, int port, InetAddress localAddress, int localPort, int type)
        throws IOException {
        this(host, port, localAddress, localPort, true, type);
    }

    public W3Socket(InetAddress address, int port, InetAddress localAddress, int localPort, int type)
        throws IOException {
        this(address, port, localAddress, localPort, true, type);
    }

    public W3Socket(String host, int port, boolean stream, int type)
        throws IOException {
        this(host, port, null, 0, stream, type);
    }

    public W3Socket(InetAddress address, int port, boolean stream, int type)
        throws IOException {
        this(address, port, null, 0, stream, type);
    }

    public W3Socket(String host, int port, InetAddress localAddress, int localPort, boolean stream, int type)
        throws IOException {
        this();
        connect(new InetSocketAddress(host, port), 0, localAddress, localPort, true, type);
    }

    public W3Socket(InetAddress address, int port, InetAddress localAddress, int localPort, boolean stream, int type)
        throws IOException {
        this();
        connect(new InetSocketAddress(address, port), 0, localAddress, localPort, stream, type);
    }

    public W3SocketImpl getSocketImpl() {
        try {
            SocketImpl socketImpl = (SocketImpl)W3Socket.implField.get(this);
            return socketImpl instanceof W3SocketImpl ? (W3SocketImpl)socketImpl : null;
        } catch (IllegalAccessException ex) {
            throw new RuntimeException(ex);
        }
    }

    protected void connect(InetSocketAddress inetSocketAddress, int timeout, InetAddress localAddress, int localPort,
                           boolean stream, int type)
        throws IOException {
        impl = getSocketImpl();
        if (impl == null) {
            if (localAddress != null || localPort > 0) super.bind(new InetSocketAddress(localAddress, localPort));
            super.connect(inetSocketAddress, timeout);
            return;
        }
        this.type = type;
        if ((type & SECURE) != 0) impl.secure = true;
        int port = inetSocketAddress.getPort();
        if (port < 0 || port > 65535) throw new IllegalArgumentException("port out range: " + port);
        if (localPort < 0 || localPort > 65535) throw new IllegalArgumentException("port out range: " + localPort);
        SecurityManager securityManager = System.getSecurityManager();
        if (securityManager != null) securityManager.checkConnect(inetSocketAddress.getHostName(), inetSocketAddress.getPort());
        int af = inetSocketAddress.getAddress() instanceof Inet6Address ? MultiSocketImpl.AF_INET6 : MultiSocketImpl.AF_INET;
        try {
            impl.create(stream, af);
            if (localAddress != null || localPort > 0) impl.bind(localAddress, localPort);
            impl.connect(inetSocketAddress, timeout);
        } catch (IOException ex) {
            impl.close();
            throw ex;
        }
    }

    public void connect(SocketAddress endpoint)
        throws IOException {
        connect((InetSocketAddress)endpoint, 0, null, 0, true, type);
    }

    public void connect(SocketAddress endpoint, int timeout)
        throws IOException {
        connect((InetSocketAddress)endpoint, timeout, null, 0, true, type);
    }

    public byte[] getAddress() {
        if (impl == null) return null;
        return impl.getAddress();
    }

    public byte[] getLocalByteAddress() {
        if (impl == null) return null;
        return impl.getLocalByteAddress();
    }

    public InetAddress getInetAddress() {
        if (impl == null) return super.getInetAddress();
        return impl.getInetAddress();
    }

    public InetAddress getLocalAddress() {
        if (impl == null) return super.getLocalAddress();
        try {
            return (InetAddress)impl.getOption(SocketOptions.SO_BINDADDR);
        } catch (Exception e) {
            try {
                return InetAddress.getLocalHost();
            } catch (UnknownHostException e1) {}
        }
        return null;
    }

    public int getPort() {
        if (impl == null) return super.getPort();
        return impl.getPort();
    }

    public int getLocalPort() {
        if (impl == null) return super.getLocalPort();
        return impl.getLocalPort();
    }

    protected InputStream getInputStream(InputStream in)
        throws IOException {
        if (in == null) in = impl.getInputStream();
        if ((type & COMPRESSED) != 0) in = new InflaterFillInputStream(in, new Inflater(true));
        return in;
    }

    public InputStream getInputStream()
        throws IOException {
        if (impl == null) return super.getInputStream();
        return in != null ? in : (in = getInputStream(in));
    }

    protected OutputStream getOutputStream(OutputStream out)
        throws IOException {
        if (out == null) out = impl.getOutputStream();
        if ((type & COMPRESSED) != 0) out = new DeflaterFlushOutputStream(out, new Deflater(Deflater.BEST_SPEED, true));
        return out;
    }

    public OutputStream getOutputStream()
        throws IOException {
        if (impl == null) return super.getOutputStream();
        return out != null ? out : (out = getOutputStream(out));
    }

    public synchronized void receive(MultiDatagramPacket pack)
        throws IOException {
        if (impl == null) throw new SocketException("Not supported");
        impl.receive(pack);
    }

    public void send(MultiDatagramPacket pack)
        throws IOException {
        if (impl == null) throw new SocketException("Not supported");
        impl.send(pack);
    }

    public void setTcpNoDelay(boolean on)
        throws SocketException {
        if (impl == null) {
            super.setTcpNoDelay(on);
            return;
        }
        impl.setOption(SocketOptions.TCP_NODELAY, new Boolean(on));
    }

    public boolean getTcpNoDelay()
        throws SocketException {
        if (impl == null) return super.getTcpNoDelay();
        return ((Boolean)impl.getOption(SocketOptions.TCP_NODELAY)).booleanValue();
    }

    public void setSoLinger(boolean on, int val)
        throws SocketException {
        if (impl == null) {
            super.setSoLinger(on, val);
            return;
        }
        if (!on) {
            impl.setOption(SocketOptions.SO_LINGER, new Boolean(on));
            return;
        }
        impl.setOption(SocketOptions.SO_LINGER, new Integer(val));
    }

    public int getSoLinger()
        throws SocketException {
        if (impl == null) return super.getSoLinger();
        Object object = impl.getOption(SocketOptions.SO_LINGER);
        if (object instanceof Integer) return ((Integer)object).intValue();
        else return -1;
    }

    public synchronized void setSoTimeout(int timeout)
        throws SocketException {
        if (impl == null) {
            super.setSoTimeout(timeout);
            return;
        }
        impl.setOption(SocketOptions.SO_TIMEOUT, new Integer(timeout));
    }

    public synchronized int getSoTimeout()
        throws SocketException {
        if (impl == null) return super.getSoTimeout();
        Object object = impl.getOption(SocketOptions.SO_TIMEOUT);
        if (object instanceof Integer) return ((Integer)object).intValue();
        return 0;
    }

    public synchronized void setIOControl(int code, int value)
        throws SocketException {
        if (impl == null) throw new SocketException("Not supported");
        impl.setIOControl(code, value);
    }

    public synchronized int getIOControl(int code)
        throws SocketException {
        if (impl == null) return -1;
        return impl.getIOControl(code);
    }

    public synchronized void close()
        throws IOException {
        if (impl == null) {
            super.close();
            return;
        }
        impl.close();
    }

    public boolean isConnected() {
        if (impl == null) return super.isConnected();
        return impl.isConnected();
    }

    public boolean isClosed() {
        if (impl == null) return super.isClosed();
        return impl.isClosed();
    }

    public String toString() {
        if (impl == null) return super.toString();
        return "W3Socket[address=" + Support.toHexString(impl.getAddress()) + ", port=" + impl.getPort() + ", localport=" + impl.getLocalPort() + "]";
    }

    protected void finalize()
        throws Throwable {
        if (impl == null) {
            super.finalize();
            return;
        }
        try {
            close();
        } catch (IOException ex) {}
    }

}
