Installation needs JRun 2.2 or backwards
compatible version of servlet engine from
www.livesoftware.com. JRun and connected
web-server must be stopped before installation.
FAXmaker for SMTP5.0 server from www.gfifax.com
and Mercury mail-server from www.pegasus.usa.com
(if no other SMTP/POP3 mail server is available)
must have been already installed. SMTP Server and
Client and POP3 Server modules must be installed
in Mercury mail-server. Users 'admin', 'fax',
'fax_recv' and 'fax_send' must have been added
with Mercury's local user manager. Passwords must
be same as usernames. Corresponding users must be
added to any mail server which is used. User
'admin' should have administrator privileges.
FAXmaker printer must have been set as default.
Users 'fax_recv' and 'fax_send' must have been
added with FAXmaker User Manager. In their
properties Fax Reports Outgoing faxes should not
include files in fax-report after transmissions.
User 'fax_recv' should receive all incoming faxes.
Default fax cover page file 'fax_cover.rtf' must
be added from cover page directory for 'fax_send'
user's default cover page after installation.
Personal cover pages must be added for user
'fax_send' using owner's full phone number (with
+) as name.  They must be of rtf-format and copies
of them must also be in separate cover page
directory.  Installation must be finished setting
destination directory used in installation as
current and running setup.bat from there.
Servlets must be configured in NAMP2 as follows:

http://server:port/servlet/fax
and it's parameters:
mobile=&receivers=[receivers]

Fax servlet also needs MSISDN-number to be sended
in query parameter 'MSISDN'.

Weather servlet's URL:
http://server:port/servlet/wea
Golf servlet's URL:
http://server:port/servlet/wea/golf
and their parameters:
mobile=&area=[area]
