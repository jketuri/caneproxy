
#include <process.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <windows.h>

#define LENGTH(array) (sizeof(array) / sizeof(*array))

typedef struct
{
    HKEY key;
    LPSTR name;
    LPBYTE data;
    DWORD type;
    DWORD size;
} REGVALUE;

LPSTR message(DWORD n)
{
    LPSTR msg = NULL;
    FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, NULL, n,
                  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&msg, 0, NULL);
    return msg;
}

void error(LPSTR msg)
{
    MessageBox(NULL, msg, "Install Error", MB_OK | MB_APPLMODAL);
}

/* Makes a new string concatenating all strings given as parameters
   (list must end with NULL). */
LPSTR makeString(LPSTR str, ...)
{
    va_list list;
    va_start(list, str);
    LPSTR s = str;
    int l = 0;
    do l += strlen(s);
    while (s = va_arg(list, LPSTR));
    va_end(list);
    va_start(list, str);
    s = str;
    str = new char[l + 1];
    str[0] = '\0';
    do strcat(str, s);
    while (s = va_arg(list, LPSTR));
    va_end(list);
    return str;
}

/* Gets value from registry under key and with name of value given in structure parameter.
   Checks type if it's non-zero. */
void getRegValue(REGVALUE &value, DWORD type, BOOL check)
{
    if (RegQueryValueEx(value.key, value.name, NULL, &value.type, NULL, &value.size) != ERROR_SUCCESS) {
        value.data = NULL;
        if (check) throw makeString("Failed to query name ", value.name, ". ", message(GetLastError()), NULL);
        return;
    }
    if (type && value.type != type) throw makeString("Type of value ", value.name, " is wrong", NULL);
    value.data = new unsigned char[(int)value.size];
    if (RegQueryValueEx(value.key, value.name, NULL, NULL, value.data, &value.size) != ERROR_SUCCESS)
        throw makeString("Failed to query value ", value.name, ". ", message(GetLastError()), NULL);
}

/* Sets registry value. */
void setRegValue(HKEY key, LPCTSTR name, DWORD type, CONST BYTE *data, DWORD size)
{
    if (RegSetValueEx(key, name, 0, type, data, size) != ERROR_SUCCESS)
        throw makeString("Failed to set value ", name, ". ", message(GetLastError()), NULL);
}

void setRegValue(REGVALUE &value)
{
    if (value.data) setRegValue(value.key, value.name, value.type, value.data, value.size);
}

void copyValues(HKEY fromKey, HKEY toKey)
{
    DWORD valuesCount, maxValueNameLen, maxValueLen;
    if (RegQueryInfoKey(fromKey, NULL, NULL, NULL, NULL, NULL, NULL,
                        &valuesCount, &maxValueNameLen, &maxValueLen, NULL, NULL) != ERROR_SUCCESS)
        throw makeString("Failed to query key info. ", message(GetLastError()), NULL);
    REGVALUE *regValues = new REGVALUE[valuesCount];
    DWORD valueIndex;
    for (valueIndex = 0; valueIndex < valuesCount; valueIndex++) {
        regValues[valueIndex].key = toKey;
        DWORD valueNameLen = maxValueNameLen + 1;
        regValues[valueIndex].name = new CHAR[valueNameLen];
        regValues[valueIndex].data = new UCHAR[maxValueLen];
        regValues[valueIndex].size = maxValueLen;
        if (RegEnumValue(fromKey, valueIndex, regValues[valueIndex].name,
                         &valueNameLen, NULL, &regValues[valueIndex].type,
                         regValues[valueIndex].data, &regValues[valueIndex].size) != ERROR_SUCCESS)
            throw makeString("Failed to enumerate value. ", message(GetLastError()), NULL);
    }
    for (valueIndex = 0; valueIndex < valuesCount; valueIndex++) setRegValue(regValues[valueIndex]);
}

int main()
{
    try {
        LPSTR s;
        HKEY currentUserDevicesKey, currentUserPrinterPortsKey, currentUserWindowsKey,
            usersDevicesKey, usersPrinterPortsKey, usersWindowsKey, jsmDefaultKey;
        // JRunNSAPIKey, aliasesKey;
        // opening various registry keys
        if (RegOpenKeyEx(HKEY_CURRENT_USER, s = "Software\\Microsoft\\Windows NT\\CurrentVersion\\Devices", 0, KEY_ALL_ACCESS, &currentUserDevicesKey) != ERROR_SUCCESS ||
            RegOpenKeyEx(HKEY_CURRENT_USER, s = "Software\\Microsoft\\Windows NT\\CurrentVersion\\PrinterPorts", 0, KEY_ALL_ACCESS, &currentUserPrinterPortsKey) != ERROR_SUCCESS ||
            RegOpenKeyEx(HKEY_CURRENT_USER, s = "Software\\Microsoft\\Windows NT\\CurrentVersion\\Windows", 0, KEY_ALL_ACCESS, &currentUserWindowsKey) != ERROR_SUCCESS ||
            RegOpenKeyEx(HKEY_USERS, s = ".DEFAULT\\Software\\Microsoft\\Windows NT\\CurrentVersion\\Devices", 0, KEY_ALL_ACCESS, &usersDevicesKey) != ERROR_SUCCESS ||
            RegOpenKeyEx(HKEY_USERS, s = ".DEFAULT\\Software\\Microsoft\\Windows NT\\CurrentVersion\\PrinterPorts", 0, KEY_ALL_ACCESS, &usersPrinterPortsKey) != ERROR_SUCCESS ||
            RegOpenKeyEx(HKEY_USERS, s = ".DEFAULT\\Software\\Microsoft\\Windows NT\\CurrentVersion\\Windows", 0, KEY_ALL_ACCESS, &usersWindowsKey) != ERROR_SUCCESS ||
            RegOpenKeyEx(HKEY_LOCAL_MACHINE, s = "Software\\Live Software\\JRunServiceManager\\jsm-default", 0, KEY_ALL_ACCESS, &jsmDefaultKey) != ERROR_SUCCESS)
            throw makeString("Failed to open key ", s, ". ", message(GetLastError()), NULL);
        // copying values from current user key to default user key
        copyValues(currentUserDevicesKey, usersDevicesKey);
        copyValues(currentUserPrinterPortsKey, usersPrinterPortsKey);
        copyValues(currentUserWindowsKey, usersWindowsKey);
        // storing JSMDirectory for install.java-program
        REGVALUE JSMDirectory = {jsmDefaultKey, "JSMDirectory"};
        getRegValue(JSMDirectory, REG_SZ, TRUE);
        FILE *JSMDirectoryFile = fopen("JSMDirectory", "wt");
        fputs((LPSTR)JSMDirectory.data, JSMDirectoryFile);
        fclose(JSMDirectoryFile);
    }
    catch (LPSTR msg) {
        error(msg);
        return 1;
    }
    return 0;
}
