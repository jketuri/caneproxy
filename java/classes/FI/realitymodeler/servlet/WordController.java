
package FI.realitymodeler.servlet;

import FI.realitymodeler.common.*;

public class WordController
{
    static
    {
        try {
            Support.loadLibrary("FI_realitymodeler_servlet");
            initialize();
        } catch (Exception ex) {
            throw new Error(ex.toString());
        }
    }

    long instance = 0;

    private static native void initialize();

    private native long construct();
    private native void quit0(long instance);
    private native void open0(long instance, String name);
    private native void close0(long instance);
    private native void printSetup0(long instance, String printer);
    private native void print0(long instance);
    private native void replace0(long instance, String from, String to);
    private native void save0(long instance);
    private native void show0(long instance);
    private native void hide0(long instance);
    private native int initiate0(long instance, String app, String topic);
    private native void poke0(long instance, int channel, String item, String data);
    private native void terminate0(long instance, int channel);

    public WordController()
    {
        instance = construct();
    }

    public void quit()
    {
        quit0(instance);
        instance = 0;
    }

    public void open(String name)
    {
        open0(instance, name);
    }

    public void close()
    {
        close0(instance);
    }

    public void printSetup(String printer)
    {
        printSetup0(instance, printer);
    }

    public void print()
    {
        print0(instance);
    }

    public void replace(String from, String to)
    {
        replace0(instance, from, to);
    }

    public void save()
    {
        save0(instance);
    }

    public void show()
    {
        show0(instance);
    }

    public void hide()
    {
        hide0(instance);
    }

    public int initiate(String app, String topic)
    {
        return initiate0(instance, app, topic);
    }

    public void poke(int channel, String item, String data)
    {
        poke0(instance, channel, item, data);
    }

    public void terminate(int channel)
    {
        terminate0(instance, channel);
    }

    protected void finalize()
    {
        quit0(instance);
    }

}
