
package FI.realitymodeler.servlet;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Weather report servlet. Initialization parameters are:<br>

    directory=directory where area index files are kept or null if they are
    read through class loader from resources directory under current
    package<p>

    source=source URL like 'ftp://ftp.smhi.se/ftp/Europol/'<p>

    username=username to be used when logging in to the source URL<p>

    password=password to be used when logging in to the source URL<p>

    cleaningMins=minimum interval in minutes when local cache where weather
    reports are stored is cleaned (defaults to one week) meaning that files
    not accessed during this interval are removed.<p>

    delayMins=minimum delay in minutes when new report is tried to read
    from source URL (defaults 5 minutes).<p>

    areas=name base for weather report file;area index file name;...<p>

    Actual file name for weather report is constructed from given name base
    as follows:<p>

    &lt;name base&gt;_YYMMDDHH00.txt_original<p>

    Golf weather reports are supposed to start with lowercase word 'golf'.
    Area index files can be of the following format:<p>

    [&lt;golf number&gt;] &lt;index&gt; [&lt;latitude&gt; &lt;longitude&gt;] &lt;area name&gt;<p>

    If line in index file doesn't start with a number it is skipped.<p>

    hours=hh;hh...<p>

    Hours when new weather reports are supposed to come under source URL.
    They default to 5, 10 and 15.<p>

    When calling servlet and path info starts with /golf, golf weathers are
    reported. Virtual path to weather servlet can be configured as
    follows:<p>

    &lt;servlet path&gt;?mobile=&amp;area=[area] */

public class Weather extends HttpServlet implements Runnable
{
    static String messagesBaseName = "FI.realitymodeler.servlet.resources.Messages";

    private AreaGroup areaGroups[];
    private ResourceBundle defaultMessages;
    private SimpleTimeZone gmt0;
    private String source = null, username = null, password = null;
    private Thread daemon = null;
    private boolean verbose = false;
    private int delayMins = 5;
    private int hours[];
    private long checkDelay;

    class AreaGroup
    {
        String nameBase;
        TreeMap areas;
        long updateTime = 0L;
    }

    class Area
    {
        int index;
        float latitude, longitude;
        String name;
    }

    void respond(HttpServletResponse res, String message) throws IOException
    {
        PrintWriter writer = res.getWriter();
        writer.println("<html><head><title>" + message + "</title></head><body>\n");
        writer.println("<h1>" + Support.htmlString(message) + "</h1></body></html>");
    }

    boolean readReport(int areaGroupIndex, int day, Area area, HttpServletResponse res, ResourceBundle messages, Locale locale, boolean mobile) throws ServletException, MalformedURLException, IOException, InterruptedException
    {
        Calendar cal = Calendar.getInstance(gmt0);
        int year = cal.get(cal.YEAR), month = cal.get(cal.MONTH), dayOfMonth = cal.get(cal.DAY_OF_MONTH),
            hour = cal.get(cal.HOUR_OF_DAY), minute = cal.get(cal.MINUTE);
        int hourIndex;
        for (hourIndex = hours.length - 1; hourIndex >= 0 &&
                 (hour < hours[hourIndex] || hour == hours[hourIndex] && minute < delayMins); hourIndex--);
        int firstState, lastState, firstAreaGroupIndex, lastAreaGroupIndex;
        if (res != null) {
            firstState = 0;
            lastState = 2;
            firstAreaGroupIndex = lastAreaGroupIndex = areaGroupIndex;
        }
        else {
            firstState = lastState = 1;
            firstAreaGroupIndex = 0;
            lastAreaGroupIndex = areaGroups.length - 1;
        }
        cal.set(year, month, dayOfMonth, hours[hourIndex < 0 ? 0 : hourIndex], 0, 0);
        long offsetTime = cal.getTime().getTime();
        if (verbose) log("Offset time is " + new Date(offsetTime));
        W3URLConnection uc = null;
        try {
            for (;;) {
                if (hourIndex < 0) {
                    hourIndex = hours.length - 1;
                    cal.add(cal.DAY_OF_MONTH, -1);
                    year = cal.get(cal.YEAR);
                    month = cal.get(cal.MONTH);
                    dayOfMonth = cal.get(cal.DAY_OF_MONTH);
                    day++;
                }
                cal.set(cal.HOUR_OF_DAY, hours[hourIndex]);
                for (; hourIndex >= 0; hourIndex--) {
                    int doneCount = 0;
                    cal.set(cal.HOUR_OF_DAY, hours[hourIndex]);
                    for (areaGroupIndex = firstAreaGroupIndex; areaGroupIndex <= lastAreaGroupIndex; areaGroupIndex++) {
                        URL url = new URL(source + areaGroups[areaGroupIndex].nameBase + "_" + pad(year % 100, 2) + pad(month + 1, 2) + pad(dayOfMonth, 2) + pad(hours[hourIndex], 2) + "00.txt_original");
                        for (int state = firstState; state <= lastState; state++) {
                            if (daemon.isInterrupted()) {
                                if (res == null) return true;
                                throw new ServletException("Servlet daemon is interrupted");
                            }
                            long ctm = System.currentTimeMillis();
                            if (res == null)
                                if (areaGroups[areaGroupIndex].updateTime > ctm) {
                                    doneCount++;
                                    break;
                                }
                                else if (verbose) log("Reading report of " + areaGroups[areaGroupIndex].nameBase + " of " + cal.getTime() + " for day " + day + " in state " + state);
                            boolean forceClose = false;
                            try {
                                if (uc == null || !uc.check(url)) {
                                    if (uc != null) uc.disconnect();
                                    uc = W3URLConnection.openConnection(url);
                                    uc.setServletContext(getServletConfig().getServletContext());
                                }
                                if (username != null) uc.setBasicCredentials(username, password);
                                switch (state) {
                                case 0:
                                case 2:
                                    uc.setCheckCaches(true);
                                    break;
                                case 1:
                                    uc.setFillCaches(true);
                                }
                                InputStream in, in1;
                                try {
                                    in = uc.getInputStream();
                                }
                                catch (IOException ex) {
                                    if (state == 1)
                                        if (!(ex instanceof FileNotFoundException)) {
                                            areaGroups[areaGroupIndex].updateTime = ctm + checkDelay;
                                            forceClose = true;
                                            getServletContext().log(ex.toString(), ex);
                                        }
                                        else if (res == null && verbose) log(url + " not found");
                                    if (state > 0) break;
                                    continue;
                                }
                                if (uc.getResponseCode() != HttpURLConnection.HTTP_OK) {
                                    if (state == 1) areaGroups[areaGroupIndex].updateTime = ctm + checkDelay;
                                    log("Received status " + uc.getResponseCode());
                                    if (res == null) continue;
                                    res.setStatus(HttpURLConnection.HTTP_INTERNAL_ERROR);
                                    PrintWriter writer = res.getWriter();
                                    if (in == null) {
                                        writer.println("<html><head><title>Status " + uc.getResponseCode() + "</title>");
                                        if (uc.getResponseMessage() != null) writer.println("<h1>" + Support.htmlString(uc.getResponseMessage()) + "</h1>");
                                    }
                                    else for (int c; (c = in.read()) != -1;) writer.write(c);
                                    return false;
                                }
                                areaGroups[areaGroupIndex].updateTime = (cal.getTime().getTime() >= offsetTime ?
                                                                         offsetTime + (long)(hourIndex < hours.length - 1 ? hours[hourIndex + 1] - hours[hourIndex] :
                                                                                             24 - hours[hourIndex] + hours[0]) * 60L * 60L * 1000L : ctm) + checkDelay;
                                if (verbose) log("Set update time to " + new Date(areaGroups[areaGroupIndex].updateTime));
                                if (uc.fillingCaches()) {
                                    if (verbose) log("Filling caches with " + uc.getURL());
                                    uc.waitForFillingCaches();
                                    continue;
                                }
                                if (res == null) break;
                                in1 = new ConvertInputStream(in, "%" + pad(area.index, 4), true, true);
                                while (in1.read() != -1);
                                int c = 0, firstChar = 0;
                                for (int day1 = 0; day1 <= day; day1++) {
                                    while ((c = in.read()) != -1 && c != '\n');
                                    if ((c = in.read()) == -1 || c == '%') {
                                        try {
                                            in.reset();
                                            c = firstChar;
                                            day = day1 - 1;
                                            break;
                                        }
                                        catch (IOException ex) {
                                            respond(res, messages.getString("weatherReportDataIsInvalid"));
                                            return false;
                                        }
                                    }
                                    firstChar = c;
                                    in.mark(200);
                                }
                                PrintWriter writer = res.getWriter();
                                writer.println("<html><head><title>" + Support.htmlString(area.name) + "</title></head><body>");
                                Date date = cal.getTime();
                                cal.add(cal.DAY_OF_MONTH, day);
                                MessageFormat messageFormat = new MessageFormat(messages.getString(mobile ? "weatherReportInfoMobile" : "weatherReportInfo"));
                                messageFormat.setLocale(locale);
                                writer.println(messageFormat.format(new Object[] {area.name, cal.getTime(), date}) + "<br>");
                                boolean wasWhite = false, written = false;
                                do {
                                    if (Character.isWhitespace((char)c)) {
                                        wasWhite = true;
                                        continue;
                                    }
                                    if (wasWhite) {
                                        if (written) writer.write(' ');
                                        wasWhite = false;
                                    }
                                    writer.write(c);
                                    written = true;
                                }
                                while ((c = in.read()) != -1 && c != '\n');
                                writer.println("<br></body></html>");
                                return false;
                            }
                            catch (IOException ex) {
                                forceClose = true;
                                getServletContext().log(ex.toString(), ex);
                            }
                            finally {
                                if (uc != null)
                                    if (!uc.mayKeepAlive() || forceClose) {
                                        uc.disconnect();
                                        uc = null;
                                    }
                                    else uc.closeStreams();
                            }
                        }
                    }
                    if (doneCount == areaGroups.length) return false;
                }
                if (day > 4) break;
            }
        }
        finally {
            if (uc != null) uc.disconnect();
        }
        if (res != null) respond(res, messages.getString("weatherReportNotFound"));
        return false;
    }

    public void run()
    {
        try {
            while (!daemon.isInterrupted()) {
                if (verbose) log("Reading reports in daemon");
                if (readReport(0, 0, null, null, defaultMessages, null, false)) break;
                long minUpdateTime = Long.MAX_VALUE;
                for (int areaGroupIndex = 0; areaGroupIndex < areaGroups.length; areaGroupIndex++)
                    if (areaGroups[areaGroupIndex].updateTime < minUpdateTime) minUpdateTime = areaGroups[areaGroupIndex].updateTime;
                long delay = minUpdateTime - System.currentTimeMillis();
                if (delay <= 0L) delay = checkDelay;
                if (verbose) log("Sleeping for " + (delay / 1000L) + " seconds");
                Thread.sleep(delay);
            }
        }
        catch (Exception ex) {
            if (!(ex instanceof InterruptedException)) getServletConfig().getServletContext().log(ex.toString(), ex);
        }
    }

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        defaultMessages = ResourceBundle.getBundle("FI.realitymodeler.servlet.resources.Messages");
        W3URLConnection uc = null;
        StreamTokenizer sTok = null;
        String path = null;
        try {
            gmt0 = new SimpleTimeZone(0, "GMT");
            String directory = config.getInitParameter("directory");
            if ((source = config.getInitParameter("source")) == null) throw new ServletException(defaultMessages.getString("parameterSourceIsNotSpecified"));
            if (!source.endsWith("/")) source += "/";
            username = config.getInitParameter("username");
            password = config.getInitParameter("password");
            if (Support.isTrue(config.getInitParameter("verbose"), "verbose") ||
                config.getServletContext().getAttribute("FI.realitymodeler.server.W3Server/verbose") == Boolean.TRUE) verbose = true;
            String s;
            if ((s = config.getInitParameter("cleaningMins")) != null) {
                uc = W3URLConnection.openConnection(new URL(source));
                uc.setCacheCleaningInterval(Long.parseLong(s) * 60L * 1000L);
                uc.disconnect();
                uc = null;
            }
            if ((s = config.getInitParameter("delayMins")) != null) delayMins = Integer.parseInt(s);
            checkDelay = (long)delayMins * 60L * 1000L;
            if ((s = config.getInitParameter("hours")) != null) {
                StringTokenizer st = new StringTokenizer(s, ";");
                hours = new int[st.countTokens()];
                for (int i = 0; i < hours.length; i++) hours[i] = Integer.parseInt(st.nextToken().trim());
            }
            else {
                hours = new int[3];
                hours[0] = 5;
                hours[1] = 10;
                hours[2] = 15;
            }
            if ((s = config.getInitParameter("areas")) == null) throw new ServletException(defaultMessages.getString("parameterAreasIsNotSpecified"));
            StringTokenizer st = new StringTokenizer(s, ";");
            areaGroups = new AreaGroup[st.countTokens() / 2];
            if (areaGroups.length == 0) throw new ServletException(defaultMessages.getString("parameterAreasIsInvalid"));
            String resourcePath = "FI/realitymodeler/servlet/resources/";
            for (int areaGroupIndex = 0; areaGroupIndex < areaGroups.length; areaGroupIndex++) {
                areaGroups[areaGroupIndex] = new AreaGroup();
                areaGroups[areaGroupIndex].nameBase = st.nextToken().trim();
                boolean golf = areaGroups[areaGroupIndex].nameBase.startsWith("golf");
                String filename = st.nextToken().trim();
                Reader reader;
                if (directory == null) {
                    InputStream in = ClassLoader.getSystemResourceAsStream(path = resourcePath + filename);
                    if (in == null) throw new MissingResourceException(path + " not found from resources", resourcePath, filename);
                    reader = new InputStreamReader(in);
                }
                else if (path.indexOf("://") != -1) {
                    URL url = new URL(path);
                    if (uc == null || !uc.check(url)) {
                        if (uc != null) uc.disconnect();
                        uc = W3URLConnection.openConnection(url);
                        uc.setServletContext(config.getServletContext());
                    }
                    else if (uc != null) uc.closeStreams();
                    if (username != null) uc.setBasicCredentials(username, password);
                    InputStream in = uc.getInputStream();
                    if (uc.getResponseCode() != HttpURLConnection.HTTP_OK) throw new ServletException("Received status " + uc.getResponseCode());
                    reader = new BufferedReader(new InputStreamReader(in));
                }
                else reader = new FileReader(new File(directory, path = filename));
                sTok = new StreamTokenizer(new BufferedReader(reader));
                sTok.resetSyntax();
                sTok.wordChars(33, 255);
                sTok.whitespaceChars(0, 32);
                sTok.parseNumbers();
                sTok.eolIsSignificant(true);
                areaGroups[areaGroupIndex].areas = new TreeMap();
                while (sTok.nextToken() != sTok.TT_EOF) {
                    if (sTok.ttype != sTok.TT_NUMBER) continue;
                    if (golf && sTok.nextToken() != sTok.TT_NUMBER) {
                        sTok.pushBack();
                        continue;
                    }
                    Area area = new Area();
                    area.index = (int)sTok.nval;
                    sTok.nextToken();
                    for (;;) {
                        if (sTok.ttype != sTok.TT_NUMBER) break;
                        area.latitude = (float)sTok.nval;
                        if (sTok.nextToken() != sTok.TT_NUMBER) break;
                        area.longitude = (float)sTok.nval;
                    }
                    StringBuffer sb = new StringBuffer();
                    while (sTok.ttype == sTok.TT_WORD) {
                        if (sb.length() > 0) sb.append(' ');
                        sb.append(sTok.sval);
                        sTok.nextToken();
                    }
                    if (sb.length() > 0) areaGroups[areaGroupIndex].areas.put((area.name = sb.toString()).toLowerCase(), area);
                    while (sTok.ttype != sTok.TT_EOL && sTok.ttype != sTok.TT_EOF) sTok.nextToken();
                    if (sTok.ttype == sTok.TT_EOF) break;
                }
            }
            daemon = new Thread(this);
            daemon.setDaemon(true);
            daemon.setPriority(daemon.MIN_PRIORITY);
            daemon.start();
        }
        catch (Exception ex) {
            if (path != null && sTok != null) throw new ServletException(MessageFormat.format(defaultMessages.getString("errorInFileInLine"),
                                                                                              new Object[] {path, new Integer(sTok.lineno())}) + "\n" + Support.stackTrace(ex));
            throw new ServletException(Support.stackTrace(ex));
        }
        finally {
            if (uc != null) uc.disconnect();
        }
    }

    public void destroy()
    {
        super.destroy();
        daemon.interrupt();
        try {
            daemon.join();
        }
        catch (InterruptedException ex) {}
    }

    final String pad(int i, int n)
    {
        String s = String.valueOf(i);
        if (s.length() >= n) return s;
        StringBuffer sb = new StringBuffer(s);
        while (sb.length() < n) sb.insert(0, '0');
        return sb.toString();
    }

    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
    {
        ResourceBundle messages = W3URLConnection.getLanguage(req, res, defaultMessages, messagesBaseName);
        if (messages == null) return;
        boolean golf = req.getPathInfo() != null && req.getPathInfo().startsWith("/golf");
        Locale locale;
        try {
            locale = new Locale(messages.getString("LANGUAGE"), "");
        }
        catch (MissingResourceException ex) {
            locale = Locale.getDefault();
        }
        SimpleDateFormat dayFormat = new SimpleDateFormat("E", locale);
        HashMap values = new HashMap();
        if (req.getQueryString() != null) W3URLConnection.parseQuery(req.getQueryString(), values);
        String s;
        boolean mobile = (s = req.getHeader("user-agent")) != null && s.trim().equalsIgnoreCase("mobile") ||
            values.containsKey("mobile");
        String areaName = W3URLConnection.getValue(values, "area"), dayValue = W3URLConnection.getValue(values, "day"), lastValue = null;
        if (mobile && areaName.length() == 0) {
            respond(res, messages.getString("areaIsMissing"));
            return;
        }
        Object value = values.get("");
        boolean foundSlash = false;
        if (value != null)
            if (value instanceof Vector) {
                StringBuffer sb = new StringBuffer(areaName);
                Enumeration elements = ((Vector)value).elements();
                if (elements.hasMoreElements())
                    for (;;) {
                        lastValue = ((String)elements.nextElement()).trim();
                        if (!elements.hasMoreElements() || foundSlash) break;
                        if (!lastValue.equals("/")) sb.append(' ').append(lastValue);
                        else foundSlash = true;
                        lastValue = null;
                    }
                areaName = sb.toString();
            }
            else lastValue = (String)value;
        if (lastValue != null && lastValue.length() > 0)
            if (dayValue.length() > 0) areaName += " " + lastValue;
            else if (!Character.isDigit(lastValue.charAt(0)) && !foundSlash) {
                int i = lastValue.indexOf('/');
                if (i != -1) {
                    s = lastValue.substring(0, i).trim();
                    dayValue = lastValue.substring(i + 1);
                }
                else s = lastValue.trim();
                if (s.length() > 0) areaName += " " + s;
            }
            else dayValue = lastValue;
        if ((areaName = areaName.trim().toLowerCase()).length() == 0) areaName = null;
        Calendar cal = Calendar.getInstance();
        int day;
        if (dayValue != null && (dayValue = dayValue.trim()).length() > 0)
            if (!Character.isDigit(dayValue.charAt(0))) {
                String weekdays[] = dayFormat.getDateFormatSymbols().getWeekdays();
                for (day = 0; day < weekdays.length && !weekdays[day].regionMatches(true, 0, dayValue, 0, dayValue.length()); day++);
                if (day == weekdays.length) {
                    respond(res, messages.getString("dayIsInvalid"));
                    return;
                }
                if ((day -= cal.get(Calendar.DAY_OF_WEEK)) < 0) day = 7 + day;
            }
            else day = Integer.parseInt(dayValue);
        else day = 0;
        Calendar cal0 = cal.getInstance(gmt0);
        day += cal.get(cal.DAY_OF_MONTH) - cal0.get(cal.DAY_OF_MONTH);
        Area area = null;
        boolean tried = false;
        int areaGroupIndex;
        for (areaGroupIndex = 0; areaGroupIndex < areaGroups.length; areaGroupIndex++) {
            if (areaGroups[areaGroupIndex].nameBase.startsWith("golf") ^ golf) continue;
            String areaName1 = W3URLConnection.getValue(values, "area" + areaGroupIndex);
            if ((areaName1 = areaName1.trim().toLowerCase()).length() == 0) areaName1 = areaName;
            if (areaName1 != null) {
                tried = true;
                Iterator iter = areaGroups[areaGroupIndex].areas.tailMap(areaName1).entrySet().iterator();
                if (iter.hasNext()) {
                    area = (Area)((Map.Entry)iter.next()).getValue();
                    if (area.name.regionMatches(true, 0, areaName1, 0, areaName1.length())) break;
                }
            }
        }
        if ((area == null && !tried || values.containsKey("ask")) &&
            !values.containsKey("go") || values.containsKey("new")) {
            if (areaName == null) areaName = "";
            PrintWriter writer = res.getWriter();
            String title = Support.htmlString(messages.getString(golf ? "golf" : "weather"));
            writer.println("<html><head><title>" + title + "</title></head><body>");
            if (!mobile) writer.println("<h1>" + title + "</h1>");
            writer.println("<form action=\"" + req.getRequestURI() + "\" method=get>");
            writer.println("<input name=go type=hidden value=>");
            if (!mobile) {
                writer.println(messages.getString("selectArea") + "<br>");
                writer.println("<table>");
                writer.println("<tr>");
                for (areaGroupIndex = 0; areaGroupIndex < areaGroups.length; areaGroupIndex++)
                    if (!(areaGroups[areaGroupIndex].nameBase.startsWith("golf") ^ golf))
                        writer.println("<th>" + areaGroups[areaGroupIndex].nameBase + "</th>");
                writer.println("</tr><tr>");
                for (areaGroupIndex = 0; areaGroupIndex < areaGroups.length; areaGroupIndex++) {
                    if (areaGroups[areaGroupIndex].nameBase.startsWith("golf") ^ golf) continue;
                    writer.println("<td><select name=area" + areaGroupIndex + " size=20>");
                    Iterator iter = areaGroups[areaGroupIndex].areas.entrySet().iterator();
                    while (iter.hasNext()) writer.println("<option>" + ((Area)((Map.Entry)iter.next()).getValue()).name);
                    writer.println("</select></td>");
                }
                writer.println("</tr></table>");
                writer.println("<input type=reset value=\"" + messages.getString("defaults") + "\">");
                writer.println("<input name=new type=submit value=\"" + messages.getString("getNewForm") + "\">");
            }
            writer.println("<input type=submit value=\"" + messages.getString("getReport") + "\">");
            writer.println("<table>");
            writer.println("<tr><th>" + messages.getString("area") + "</th><td><input name=area type=text value=\"" + areaName + "\" size=25></td></tr>");
            writer.println("<tr><th>" + messages.getString("day") + "</th><td><select name=day>");
            cal = Calendar.getInstance();
            for (int i = 0; i < 6; i++) {
                writer.println("<option>" + dayFormat.format(cal.getTime()));
                cal.add(cal.DAY_OF_MONTH, 1);
            }
            writer.println("</select></td></tr></table>");
            writer.println("</form></body></html>");
            return;
        }
        if (area == null) {
            respond(res, messages.getString("areaNotFound"));
            return;
        }
        try {
            readReport(areaGroupIndex, day, area, res, messages, locale, mobile);
        }
        catch (InterruptedException ex) {
            throw new ServletException(ex.toString());
        }
    }

}
