
package FI.realitymodeler.servlet;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import FI.realitymodeler.server.MimeMap;
import java.awt.*;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import namp2.remote.*;

/** Fax servlet. Initialization parameters are:<br>

    domain=domain name to be used in email-addresses. Fax messages for
    FAXmaker program are sent to address fax@domain and notifications are
    checked from mailbox fax_send@domain.<p>

    mercuryDirectory=name of the directory where Mercury Mail Server
    resides. If this is not specified, servlet supposes that some other
    mail server is running.<p>

    smtpServerHost=name of the SMTP-server host if it's not local.<p>

    pop3ServerHost=name of the POP3-server host if it's not local.<p>

    coverPageDirectory=name of the directory where rtf-cover page files
    reside.  This directory is used to check if FAXmaker program user
    fax_send should have cover page named after these files, otherwise they
    are not used.<p>

    inetServerHost=NAMP2 inet server's host name (localhost).<p>

    inetServerPort=NAMP2 inet server's port number (5678).<p>

    testFormActionURL=NAMP1 test form's action url
    (http://localhost/cgi/netgate/wwgcgitestmx.pl).<p>

    testServiceName=name of service which forwards contents of query
    parameter 'message' to destination. (echo).<p>

    waitMins=(1) Waiting time in minutes between servlet daemon thread's
    tries to check notification messages sent by fax server to the mailbox
    fax_send@domain.<p>

    NAMPNotifyMode=0 | 1 | 2 (0). When fax has been sent, message originator
    is notified through test form of NAMP1 if this is set to 1, or through
    push mechanism of NAMP2 Inet Server if this set to 2. NAMP1 test form
    action URL can be specified with parameter 'testFormActionURL' and
    service name with parameter 'testServiceName'. NAMP2 Inet Server host
    can be specified with parameters 'inetServerHost' and 'inetServerPort'.
    MSISDN-number must be available in query parameter 'MSISDN' otherwise
    notification will not be sent.<p>

    Virtual path to fax servlet can be configured as follows:<br>
    &lt;servlet path&gt;?mobile=&amp;receivers=[receivers] */

public class Fax extends HttpServlet implements Runnable
{
    static String messagesBaseName = "FI.realitymodeler.servlet.resources.Messages";

    private File coverPageDirectory = null;
    private FileNameMap fileNameMap = null;
    private Process mercury = null;
    private ResourceBundle defaultMessages;
    private String domain = null;
    private String inetServerHost = "localhost";
    private String testServiceName = "echo";
    private Thread daemon;
    private URL faxSendURL = null;
    private URL testFormActionURL = null;
    private boolean verbose = false;
    private int NAMPNotifyMode = 0;
    private int inetServerPort = 5678;
    private long waitDelay = 1L * 60L * 1000L;

    public void run()
    {
        W3URLConnection uc = null;
        W3Pop3URLConnection pop3uc = null;
        while (!daemon.isInterrupted())
            try {
                if (verbose) log("Waiting in daemon");
                Thread.sleep(waitDelay);
                if (verbose) log("Checking fax_send mailbox");
                pop3uc = new W3Pop3URLConnection(faxSendURL);
                pop3uc.setServletContext(getServletConfig().getServletContext());
                pop3uc.setBasicCredentials("fax_send", "fax_send");
                Vector urls = new Vector();
                InputStream in;
                try {
                    in = pop3uc.getInputStream();
                } catch (IOException ex) {
                    continue;
                }
                if (pop3uc.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    log("POP3 returned status " + pop3uc.getResponseCode());
                    continue;
                }
                W3URLConnection.parse(in, urls, null, null, faxSendURL, null, null, null, null, null, null, null, null, true, false);
                pop3uc.closeStreams();
                if (verbose) log("Mailbox page parsed");
                Enumeration elements = urls.elements();
                while (elements.hasMoreElements()) {
                    URL url = (URL)elements.nextElement();
                    String urlStr = url.toExternalForm();
                    if (verbose) log(urlStr);
                    int i = urlStr.indexOf('?');
                    if (i == -1) continue;
                    try {
                        HashMap values = new HashMap();
                        W3URLConnection.parseQuery(urlStr.substring(i + 1), values);
                        String subject = (String)values.get("subject"), message = null;
                        if (subject == null) continue;
                        String sender, s = Support.decodeWords(subject).trim(), t = s.toLowerCase();
                        boolean success;
                        if ((success = t.startsWith("success:")) ||
                            t.startsWith("failure:")) {
                            i = s.lastIndexOf('(');
                            int j = s.lastIndexOf(' ');
                            if (i == -1 || i < 10 || j == -1 || j < i) {
                                log("Parsing subject field " + subject + " failed");
                                continue;
                            }
                            sender = s.substring(9, i).trim();
                            message = MessageFormat.format(defaultMessages.getString(success ? "faxSentToName" : "sendingFaxToNameFailed"),
                                                           new Object[] {s.substring(j + 1, s.length() - 1).trim()});
                        }
                        else if (s.startsWith("C=")) {
                            sender = s.substring(2).trim();
                            message = defaultMessages.getString("newCoverPageSet");
                        }
                        else {
                            sender = s;
                            message = defaultMessages.getString("sendingFaxFailed");
                            log("Fatal Fax-error, maybe fax_send@" + domain + " is not FAXmaker user or cover page file of " + sender + " is not added or parsing subject field " + subject + " + failed");
                        }
                        if (sender.startsWith("+"))
                            switch (NAMPNotifyMode) {
                            case 1: {
                                if (verbose) log("Trying to send notification to " + sender + " through NAMP1");
                                if (!sender.startsWith("+")) sender = "+" + sender;
                                ByteArrayOutputStream bout = new ByteArrayOutputStream();
                                Support.writeBytes(bout, "tophone=yes&number=" + URLEncoder.encode(sender) + "&parms=" +
                                                   URLEncoder.encode(URLEncoder.encode(message)) + "&send=" + URLEncoder.encode("Perform test") +
                                                   "&service=" + URLEncoder.encode(testServiceName));
                                if (uc == null || !uc.check(testFormActionURL)) {
                                    if (uc != null) uc.disconnect();
                                    uc = W3URLConnection.openConnection(testFormActionURL);
                                    uc.setServletContext(getServletConfig().getServletContext());
                                    uc.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                                    uc.setRequestProperty("Content-Length", String.valueOf(bout.size()));
                                    OutputStream out = uc.getOutputStream();
                                    bout.writeTo(out);
                                    out.flush();
                                }
                                break;
                            }
                            case 2: {
                                if (verbose) log("Trying to send notification to " + sender + " through NAMP2");
                                if (sender.startsWith("+")) sender = sender.substring(1);
                                Namp2Connection n2c = new Namp2Connection(inetServerHost, inetServerPort);
                                Namp2Request n2r = new Namp2Request();
                                n2r.setKeyword("FAXSENT");
                                n2r.setContent(message.getBytes(), false);
                                n2r.setAddr("msisdn://" + sender);
                                n2c.submit(n2r);
                                break;
                            }
                            }
                        else if (sender.indexOf('@') != -1) {
                            if (verbose) log("Trying to send notification to " + sender + " through SMTP");
                            W3SmtpURLConnection smtpuc = null;
                            try {
                                smtpuc = new W3SmtpURLConnection(new URL("mailto:" + sender));
                                smtpuc.setRequestProperty("From", "fax_send@" + domain);
                                smtpuc.setRequestProperty("Subject", Support.encodeWords(message));
                                smtpuc.setRequestProperty("Content-Type", Support.plainType);
                                OutputStream out = smtpuc.getOutputStream();
                                Support.writeBytes(out, message);
                                out.close();
                            }
                            finally {
                                if (smtpuc != null) smtpuc.disconnect();
                            }
                        }
                    }
                    finally {
                        if (pop3uc == null || !pop3uc.check(url)) {
                            if (pop3uc != null) pop3uc.disconnect();
                            pop3uc = new W3Pop3URLConnection(url);
                            pop3uc.setServletContext(getServletConfig().getServletContext());
                        }
                        pop3uc.setBasicCredentials("fax_send", "fax_send");
                        pop3uc.getInputStream();
                        pop3uc.closeStreams();
                    }
                }
            }
            catch (InterruptedException ex) {
                break;
            }
            catch (Exception ex) {
                getServletContext().log(ex.toString(), ex);
            }
            finally {
                try {
                    if (uc != null) {
                        uc.disconnect();
                        uc = null;
                    }
                }
                finally {
                    if (pop3uc != null) {
                        pop3uc.disconnect();
                        pop3uc = null;
                    }
                }
            }
    }

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);
        defaultMessages = ResourceBundle.getBundle("FI.realitymodeler.servlet.resources.Messages");
        try {
            if (Support.isTrue(config.getInitParameter("verbose"), "verbose") ||
                config.getServletContext().getAttribute("FI.realitymodeler.server.W3Server/verbose") == Boolean.TRUE) verbose = true;
            File mercuryDirectory = null;
            String s;
            if ((s = config.getInitParameter("mercuryDirectory")) != null &&
                !(mercuryDirectory = new File(s)).exists()) throw new ServletException(MessageFormat.format(defaultMessages.getString("directoryNotFound"), new Object[] {s}));
            W3SmtpURLConnection.smtpServerHost = (s = config.getInitParameter("smtpServerHost")) != null ? s : "localhost";
            W3Pop3URLConnection.pop3ServerHost = (s = config.getInitParameter("pop3ServerHost")) != null ? s : "localhost";
            if ((s = config.getInitParameter("coverPageDirectory")) == null)
                throw new ServletException(defaultMessages.getString("coverPageDirectoryIsNotSpecified"));
            if (!(coverPageDirectory = new File(s)).exists()) throw new ServletException(MessageFormat.format(defaultMessages.getString("directoryNotFound"), new Object[] {s}));
            if ((s = config.getInitParameter("inetServerHost")) != null) inetServerHost = s;
            if ((s = config.getInitParameter("inetServerPort")) != null) inetServerPort = Integer.parseInt(s);
            if ((s = config.getInitParameter("testServiceName")) != null) testServiceName = s;
            if ((s = config.getInitParameter("NAMPNotifyMode")) != null &&
                ((NAMPNotifyMode = Integer.parseInt(s)) < 0 || NAMPNotifyMode > 2))
                throw new ServletException(defaultMessages.getString("NAMPNotifyModeIsInvalid"));
            if ((s = config.getInitParameter("waitMins")) != null) waitDelay = Long.parseLong(s) * 60L * 1000L;
            domain = (s = config.getInitParameter("domain")) != null ? s :
                InetAddress.getLocalHost().getHostName();
            testFormActionURL = new URL((s = config.getInitParameter("testFormActionURL")) != null ? s :
                                        "http://localhost/cgi/netgate/wwgcgitestmx.pl");
            faxSendURL = new URL("http:?delete=");
            String path;
            InputStream in = ClassLoader.getSystemResourceAsStream(path = "FI/realitymodeler/common/mime_types");
            if (in == null) throw new MissingResourceException(path + " not found from resources", path, path);
            fileNameMap = MimeMap.readMimeMap(new StreamTokenizer(new InputStreamReader(in)), path, null);
            if (mercuryDirectory != null) {
                log("Launching mercury");
                mercury = Runtime.getRuntime().exec(new File(mercuryDirectory, "MERCURY.EXE").getCanonicalPath());
            }
            log("Launching daemon");
            daemon = new Thread(this);
            daemon.setDaemon(true);
            daemon.start();
        }
        catch (Exception ex) {
            destroy();
            throw new ServletException(Support.stackTrace(ex));
        }
    }

    public void destroy()
    {
        super.destroy();
        try {
            if (daemon != null) {
                daemon.interrupt();
                try {
                    daemon.join();
                }
                catch (InterruptedException ex) {}
            }
        }
        finally {
            if (mercury != null) mercury.destroy();
        }
    }

    void respond(HttpServletResponse res, String message) throws IOException
    {
        message = Support.htmlString(message);
        PrintWriter writer = res.getWriter();
        writer.println("<html><head><title>" + message + "</title></head><body>\n");
        writer.println("<h1>" + message + "</h1></body></html>");
    }

    W3URLConnection getConnection(String filename, HashMap values, HttpServletRequest req, HttpServletResponse res, ResourceBundle messages) throws IOException
    {
        String from;
        if ((from = W3URLConnection.getValue(values, "MSISDN").trim()).length() > 0) {
            if (!from.startsWith("+")) from = "+" + from;
        }
        else if (((from = W3URLConnection.getValue(values, "sender")).trim()).length() == 0 &&
                 ((from = req.getRemoteUser()) == null || (from = from.trim()).length() == 0) &&
                 ((from = req.getRemoteAddr()) == null || (from = from.trim()).length() == 0)) from = "<unknown>";
        from = from.replace(',', ';');
        values.put("from", from);
        W3URLConnection uc;
        if (values.containsKey("cover")) {
            uc = new W3SmtpURLConnection(new URL("mailto:fax@" + domain));
            uc.setRequestProperty("To", "faxadmin <fax@" + domain + ">");
            uc.setRequestProperty("Subject", "C=" + from);
        }
        else {
            String receivers = W3URLConnection.getValue(values, "receivers");
            if (receivers == null || (receivers = receivers.trim()).length() == 0) {
                respond(res, messages.getString("receiversAreMissing"));
                return null;
            }
            boolean hasFaxes = false;
            StringBuffer to = new StringBuffer();
            StringTokenizer receiverSt = new StringTokenizer(receivers, ",");
            while (receiverSt.hasMoreTokens()) {
                StringTokenizer st = new StringTokenizer(receiverSt.nextToken().trim());
                if (!st.hasMoreTokens()) continue;
                String address = st.nextToken();
                if (address.length() == 0) continue;
                if (address.indexOf('@') == -1)
                    {
                        hasFaxes = true;
                        continue;
                    }
                if (to.length() > 0) to.append(',');
                to.append(address);
            }
            if (!hasFaxes && to.length() == 0) {
                respond(res, messages.getString("receiversAreMissing"));
                return null;
            }
            uc = new W3SmtpURLConnection(new URL("mailto:" + (hasFaxes ? "fax@" + domain : "") +
                                                  (to.length() > 0 ? (hasFaxes ? "," : "") + to.toString() : "")));
            uc.setRequestProperty("Subject", from);
        }
        uc.setRequestProperty("From", "fax_send@" + domain);
        if (filename != null) {
            uc.setRequestProperty("Content-Type", "multipart/mixed; boundary=\"_\"");
            uc.setRequestProperty("Attached", filename);
        }
        else uc.setRequestProperty("Content-Type", Support.plainType);
        uc.setServletContext(getServletConfig().getServletContext());
        return uc;
    }

    void sendOptions(OutputStream out, HashMap values, HttpServletRequest req, HttpServletResponse res, ResourceBundle messages) throws IOException
    {
        if (values.containsKey("cover")) return;
        String from = (String)values.get("from"), receivers = W3URLConnection.getValue(values, "receivers");
        StringTokenizer receiverSt = new StringTokenizer(receivers, ",");
        while (receiverSt.hasMoreTokens()) {
            StringTokenizer st = new StringTokenizer(receiverSt.nextToken().trim());
            if (!st.hasMoreTokens()) continue;
            String number = st.nextToken();
            if (number.length() == 0 || number.indexOf('@') != -1) continue;
            String name = st.hasMoreTokens() ? st.nextToken("").trim() : number;
            Support.writeBytes(out, "::'" + name + "','" + from + "'," + number + "\r\n");
        }
        if (new File(coverPageDirectory, from + ".rtf").exists()) Support.writeBytes(out, "::C=" + from + "\r\n");
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
    {
        ResourceBundle messages = W3URLConnection.getLanguage(req, res, defaultMessages, messagesBaseName);
        if (messages == null) return;
        HashMap values = new HashMap();
        if (req.getQueryString() != null) W3URLConnection.parseQuery(req.getQueryString(), values);
        String s;
        boolean mobile = (s = req.getHeader("user-agent")) != null && s.trim().equalsIgnoreCase("mobile") ||
            values.containsKey("mobile");
        String receivers = W3URLConnection.getValue(values, "receivers").trim(),
            sender = W3URLConnection.getValue(values, "sender").trim(),
            text = W3URLConnection.getValue(values, "text");
        if (mobile && receivers.length() == 0) {
            respond(res, messages.getString("receiversAreMissing"));
            return;
        }
        Object value = values.get("");
        if (value != null) {
            if (value instanceof Vector) {
                StringBuffer sb = new StringBuffer(receivers);
                Enumeration elements = ((Vector)value).elements();
                while (elements.hasMoreElements()) sb.append(' ').append(((String)elements.nextElement()).trim());
                receivers = sb.toString();
            }
            else receivers = " " + value;
            int i = receivers.indexOf('/');
            if (i != -1) {
                if (text.length() > 0) text += " ";
                text += receivers.substring(i + 1).trim();
                receivers = receivers.substring(0, i).trim();
            }
            else {
                StringTokenizer st = new StringTokenizer(receivers);
                if (st.hasMoreTokens()) {
                    receivers = st.nextToken();
                    if (st.hasMoreTokens()) receivers += " " + st.nextToken();
                    if (st.hasMoreTokens()) {
                        if (text.length() > 0) text += " ";
                        text += st.nextToken("").trim();
                    }
                }
            }
            values.put("receivers", receivers);
            values.put("text", text);
        }
        if (mobile && text.length() == 0) {
            respond(res, messages.getString("textIsMissing"));
            return;
        }
        if (receivers.length() > 0 && text.length() > 0 && !values.containsKey("new")) {
            W3URLConnection uc = null;
            try {
                if ((uc = getConnection(null, values, req, res, messages)) == null) return;
                OutputStream out = uc.getOutputStream();
                sendOptions(out, values, req, res, messages);
                Support.writeBytes(out, text);
                out.close();
            }
            finally {
                if (uc != null) uc.disconnect();
            }
            respond(res, messages.getString("faxPutInQueue"));
            return;
        }
        PrintWriter writer = res.getWriter();
        writer.println("<html><head><title>" + Support.htmlString(receivers.length() == 0 ? messages.getString("fax") : receivers) + "</title></head><body>");
        writer.println("<h1>" + Support.htmlString(messages.getString("fax")) + "</h1>");
        if (!mobile) writer.println(Support.htmlString(messages.getString("faxOrders")) + "<br>");
        writer.println("<form action=\"" + req.getServletPath() + "\" " +
                       (mobile ? "method=get" : "enctype=\"multipart/form-data\" method=post") + ">");
        if (!mobile) {
            writer.println("<input type=reset value=\"" + messages.getString("defaults") + "\">");
            writer.println("<input name=new type=submit value=\"" + messages.getString("getNewForm") + "\">");
            writer.println("<input name=cover type=submit value=\"" + messages.getString("setPersonalCoverPage") + "\">");
        }
        writer.println("<input type=submit value=\"" + messages.getString("sendFax") + "\">");
        writer.println("<table>");
        writer.println("<tr><th>" + messages.getString("receivers") + "</th><td><input name=receivers type=text value=\"" + receivers + "\" size=55 maxlength=275></td></tr>");
        writer.println("<tr><th>" + messages.getString("sender") + "</th><td><input name=sender type=text value=\"" + sender + "\" size=55 maxlength=275></td></tr>");
        if (!mobile) writer.println("<tr><th>" + messages.getString("coverPageFile") + "</th><td><input name=file type=file accept=\"*/*\" size=55 maxlength=275></td></tr>");
        writer.println("</table>");
        writer.println("<textarea name=text rows=20 cols=66 wrap=hard>\n" + text + "\n</textarea><br>");
        writer.println("</form></body></html>");
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
    {
        ResourceBundle messages = W3URLConnection.getLanguage(req, res, defaultMessages, messagesBaseName);
        if (messages == null) return;
        HashMap values = new HashMap(), params = new HashMap();
        String ct = Support.getParameters(req.getContentType(), params).toLowerCase();
        byte b[] = new byte[Support.bufferLength];
        InputStream in = req.getInputStream();
        W3URLConnection uc = null;
        try {
            if (ct.equals("multipart/form-data")) {
                String boundary = (String)params.get("boundary");
                if (boundary != null) boundary = "--" + boundary;
                else if ((boundary = req.getHeader("boundary")) == null) throw new ServletException("Missing boundary");
                InputStream in1 = new ConvertInputStream(in, boundary, true, true);
                while (in1.read() != -1);
                boundary = "\r\n" + boundary;
                OutputStream out = null;
                StringBuffer value = new StringBuffer();
                for (;;) {
                    int c;
                    if ((c = in.read()) == '-' && (c = in.read()) == '-') break;
                    while (c != -1 && c != '\n') c = in.read();
                    if (c == -1) break;
                    in1 = new ConvertInputStream(in, boundary, false, true);
                    HeaderList headerList;
                    try {
                        headerList = new HeaderList(in1);
                    }
                    catch (ParseException ex) {
                        throw new ServletException(ex.toString());
                    }
                    String cd = headerList.getHeaderValue("content-disposition");
                    if (cd == null) throw new ServletException("Missing content-disposition");
                    Support.getParameters(cd, params = new HashMap(), true);
                    String name = (String)params.get("name");
                    if (name == null) throw new ServletException("Missing name in content-disposition");
                    if (name.equals("file")) {
                        String filename = null;
                        if ((filename = (String)params.get("filename")) == null ||
                            (filename = filename.trim()).length() == 0 || values.containsKey("new")) {
                            while (in1.read(b) > 0);
                            continue;
                        }
                        if (values.containsKey("cover") && !filename.substring(filename.lastIndexOf('.') + 1).equalsIgnoreCase("rtf")) {
                            respond(res, messages.getString("personalCoverPageFileMustBeRtfDocument"));
                            return;
                        }
                        if ((uc = getConnection(filename, values, req, res, messages)) == null) return;
                        String from = (String)values.get("from");
                        out = uc.getOutputStream();
                        Support.writeBytes(out, "\r\n--_\r\n");
                        Support.writeBytes(out, "Content-Type: " + Support.plainType + "\r\n");
                        Support.writeBytes(out, "Content-Transfer-Encoding: quoted-printable\r\n\r\n");
                        OutputStream qpOut = new EncoderOutputStream(out, new QPEncoder());
                        sendOptions(qpOut, values, req, res, messages);
                        qpOut.close();
                        Support.writeBytes(out, "\r\n--_\r\n");
                        if ((ct = fileNameMap.getContentTypeFor(filename)) == null) ct = Support.octetStreamType;
                        Support.writeBytes(out, "Content-Type: " + ct + "; name=\"" + filename.substring(Math.max(filename.lastIndexOf('/'), filename.lastIndexOf('\\')) + 1) + "\"\r\n");
                        Support.writeBytes(out, "Content-Transfer-Encoding: base64\r\n\r\n");
                        OutputStream b64Out = new EncoderOutputStream(out, new BASE64Encoder());
                        File file = new File(coverPageDirectory, from + ".rtf");
                        BufferedOutputStream fout = new BufferedOutputStream(new FileOutputStream(file));
                        try {
                            for (int n; (n = in1.read(b)) > 0;) {
                                b64Out.write(b, 0, n);
                                fout.write(b, 0, n);
                            }
                            fout.close();
                            b64Out.close();
                        }
                        catch (IOException ex) {
                            try {
                                fout.close();
                            }
                            finally {
                                file.delete();
                            }
                            throw ex;
                        }
                        Support.writeBytes(out, "\r\n--_--\r\n");
                        out.close();
                        while (in.read(b) > 0);
                        respond(res, messages.getString("faxPutInQueue"));
                        return;
                    }
                    else if (name.equals("text") && !values.containsKey("new") && uc == null) {
                        if ((uc = getConnection(null, values, req, res, messages)) == null) return;
                        out = uc.getOutputStream();
                        sendOptions(out, values, req, res, messages);
                        for (int n; (n = in1.read(b)) > 0;) out.write(b, 0, n);
                        out.close();
                        while (in.read(b) > 0);
                        respond(res, messages.getString("faxPutInQueue"));
                        return;
                    }
                    while ((c = in1.read()) != -1) value.append((char)c);
                    values.put(name, value.toString());
                    value.setLength(0);
                }
            }
            else W3URLConnection.parseQuery(in, values, null);
            while (in.read(b) > 0);
            if (values.containsKey("new")) {
                StringBuffer sb = new StringBuffer();
                Iterator nameIter = values.keySet().iterator();
                while (nameIter.hasNext()) {
                    if (sb.length() > 0) sb.append('&');
                    String name = (String)nameIter.next();
                    sb.append(URLEncoder.encode(name) + "=" + URLEncoder.encode(W3URLConnection.getValue(values, name)));
                }
                res.sendRedirect(req.getRequestURI() + "?" + sb.toString());
            }
            else {
                if ((uc = getConnection(null, values, req, res, messages)) == null) return;
                OutputStream out = uc.getOutputStream();
                sendOptions(out, values, req, res, messages);
                String text = (String)values.get("text");
                if (text != null) Support.writeBytes(out, text);
                out.close();
            }
        }
        finally {
            if (uc != null) uc.disconnect();
        }
    }

}
