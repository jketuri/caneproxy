
package FI.realitymodeler.servlet;

import FI.realitymodeler.common.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class EchoMessage extends HttpServlet
{

public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
{
	String message = Support.getParameterValue(req.getParameterValues("message"));
	if (message == null) throw new ServletException("Message not specified");
	res.getWriter().print(message);
}

}
