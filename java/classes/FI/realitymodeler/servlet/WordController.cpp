
#include <windows.h>
#include <ole2.h>
#include <jni.h>
#include "FI_realitymodeler_servlet_WordController.h"

#define APP_HIDE (dispid[0])
#define APP_SHOW (dispid[1])
#define DDE_INITIATE (dispid[2])
#define DDE_POKE (dispid[3])
#define DDE_TERMINATE (dispid[4])
#define EDIT_REPLACE (dispid[5])
#define FILE_CLOSE (dispid[6])
#define FILE_EXIT (dispid[7])
#define FILE_OPEN (dispid[8])
#define FILE_OPEN_NAME (dispid + 9)
#define FILE_PRINT (dispid[10])
#define FILE_PRINT_SETUP (dispid[11])
#define FILE_PRINT_SETUP_PRINTER (dispid + 12)
#define FILE_SAVE (dispid[13])

#define LENGTH(array) (sizeof(array) / sizeof(*array))

static jclass nullPointerException, runtimeException;

static DISPID *dispid = NULL;

static void throwNew(JNIEnv *env, jclass throwable, DWORD n, LPSTR err, EXCEPINFO *excepinfo)
{
    LPSTR msg = NULL;
    char str[32];
    if (n) sprintf(str, "%x", n);
    else *str = 0;
    int l = strlen(str), l1, l2;
    if (*str) l++;
    l += strlen(err);
    if (excepinfo) {
        if (excepinfo->bstrSource) l += 1 + (l1 = wcslen(excepinfo->bstrSource));
        if (excepinfo->bstrDescription) l += 1 + (l2 = wcslen(excepinfo->bstrDescription));
    }
    if (!(msg = new char[l + 1])) {
        env->ThrowNew(throwable, "error");
        return;
    }
    strcpy(msg, str);
    if (*str) strcat(msg, " ");
    strcat(msg, err);
    if (excepinfo) {
        if (excepinfo->bstrSource) {
            strcat(msg, " ");
            WideCharToMultiByte(CP_ACP, 0, excepinfo->bstrSource, l1, msg + strlen(msg), l1, NULL, NULL);
        }
        if (excepinfo->bstrDescription) {
            strcat(msg, " ");
            WideCharToMultiByte(CP_ACP, 0, excepinfo->bstrDescription, l2, msg + strlen(msg), l2, NULL, NULL);
        }
    }
    env->ThrowNew(throwable, msg);
    delete msg;
}

static void char2OLECHAR(const char *str, OLECHAR *olestr)
{
    int num = strlen(str) + 1;
    char *tostr = (char *)olestr;
    for (int i = 0; i < num; i++) {
        tostr[2 * i] = str[i];
        tostr[2 * i + 1] = 0;
    }
}

static jclass getClass(JNIEnv *env, char *name)
{
    jclass clazz;
    if (clazz = env->FindClass(name)) clazz = (jclass)env->NewGlobalRef(clazz);
    return clazz;
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_servlet_WordController_initialize(JNIEnv *env, jclass clazz)
{
    if (!(nullPointerException = getClass(env, "java/lang/NullPointerException")) ||
        !(runtimeException = getClass(env, "java/lang/RuntimeException"))) return;
}

extern "C" JNIEXPORT jlong JNICALL Java_FI_realitymodeler_servlet_WordController_construct(JNIEnv *env, jobject obj)
{
    CoInitialize(NULL);
    CLSID clsid;
    HRESULT hresult = CLSIDFromProgID(L"Word.Basic", &clsid);
    if (hresult != S_OK) {
        throwNew(env, runtimeException, hresult, "CLSIDFromProgID", NULL);
        return 0L;
    }
    IDispatch *pDisp = NULL;
    hresult = CoCreateInstance(
                               clsid,
                               NULL,
                               CLSCTX_LOCAL_SERVER,
                               IID_IDispatch,
                               (void **)&pDisp);
    if (hresult != S_OK) {
        CoUninitialize();
        throwNew(env, runtimeException, hresult, "CoCreateInstance", NULL);
        return 0L;
    }
    if (dispid) return (jlong)pDisp;
    OLECHAR *mnames[] = {
        L"AppHide",
        L"AppShow",
        L"DDEInitiate",
        L"DDEPoke",
        L"DDETerminate",
        L"EditReplace",
        L"FileClose",
        L"FileExit",
        L"FileOpen", L"Name",
        L"FilePrint",
        L"FilePrintSetup", L"Printer",
        L"FileSave"};
    int lengths[] = {
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        2,
        1,
        2,
        1};
    dispid = new DISPID[LENGTH(mnames)];
    for (int i = 0, j = 0; i < LENGTH(mnames); i += lengths[j++])
        if ((hresult = pDisp->GetIDsOfNames(IID_NULL, mnames + i, lengths[j], LOCALE_SYSTEM_DEFAULT, dispid + i)) != S_OK) {
            pDisp->Release();
            CoUninitialize();
            throwNew(env, runtimeException, hresult, "GetIDsOfNames", NULL);
            return 0L;
        }
    return (jlong)pDisp;
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_servlet_WordController_quit0(JNIEnv *env, jobject obj, jlong instance)
{
    if (!instance) return;
    EXCEPINFO excepinfo;
    unsigned int uArgErr;
    DISPPARAMS dispparams;
    VARIANTARG params[1];
    params[0].vt = VT_I2;
    params[0].iVal = 2;     // No prompt
    dispparams.rgvarg = params;
    dispparams.rgdispidNamedArgs = NULL;
    dispparams.cArgs = 1;
    dispparams.cNamedArgs = 0;
    HRESULT hresult = ((IDispatch *)instance)->Invoke(
                                                      FILE_EXIT,
                                                      IID_NULL,
                                                      LOCALE_SYSTEM_DEFAULT,
                                                      DISPATCH_METHOD,
                                                      &dispparams,
                                                      NULL,
                                                      &excepinfo,
                                                      &uArgErr);
    CoUninitialize();
    ((IDispatch *)instance)->Release();
    if (hresult != S_OK) throwNew(env, runtimeException, hresult, "Exit", &excepinfo);
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_servlet_WordController_open0(JNIEnv *env, jobject obj, jlong instance, jstring name)
{
    if (!name) {
        env->ThrowNew(nullPointerException, "");
        return;
    }
    const char *nameStr = env->GetStringUTFChars(name, NULL);
    if (!nameStr) return;
    VARIANTARG params[1];
    OLECHAR *nameOLE = (OLECHAR *)new OLECHAR[strlen(nameStr) + 1];
    char2OLECHAR(nameStr, nameOLE);
    params[0].vt = VT_BSTR;
    params[0].bstrVal = SysAllocString(nameOLE);
    DISPPARAMS dispparams;
    dispparams.rgvarg = params;
    dispparams.rgdispidNamedArgs = FILE_OPEN_NAME;
    dispparams.cArgs = 1;
    dispparams.cNamedArgs = 1;
    EXCEPINFO excepinfo;
    unsigned int uArgErr;
    HRESULT hresult = ((IDispatch *)instance)->Invoke(
                                                      FILE_OPEN,
                                                      IID_NULL,
                                                      LOCALE_SYSTEM_DEFAULT,
                                                      DISPATCH_METHOD,
                                                      &dispparams,
                                                      NULL,
                                                      &excepinfo,
                                                      &uArgErr);
    delete nameOLE;
    SysFreeString(params[0].bstrVal);
    env->ReleaseStringUTFChars(name, nameStr);
    if (hresult != S_OK) throwNew(env, runtimeException, hresult, "Open", &excepinfo);
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_servlet_WordController_close0(JNIEnv *env, jobject obj, jlong instance)
{
    VARIANTARG params[1];
    params[0].vt = VT_I2;
    params[0].iVal = 2;     // No prompt
    DISPPARAMS dispparams;
    dispparams.rgvarg = params;
    dispparams.rgdispidNamedArgs = NULL;
    dispparams.cArgs = 1;
    dispparams.cNamedArgs = 0;
    EXCEPINFO excepinfo;
    unsigned int uArgErr;
    HRESULT hresult = ((IDispatch *)instance)->Invoke(
                                                      FILE_CLOSE,
                                                      IID_NULL,
                                                      LOCALE_SYSTEM_DEFAULT,
                                                      DISPATCH_METHOD,
                                                      &dispparams,
                                                      NULL,
                                                      &excepinfo,
                                                      &uArgErr);
    if (hresult != S_OK) throwNew(env, runtimeException, hresult, "Close", &excepinfo);
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_servlet_WordController_printSetup0(JNIEnv *env, jobject obj, jlong instance, jstring printer)
{
    const char *printerStr = env->GetStringUTFChars(printer, NULL);
    if (!printerStr) return;
    OLECHAR *printerOLE = (OLECHAR *)new OLECHAR[strlen(printerStr) + 1];
    char2OLECHAR(printerStr, printerOLE);
    VARIANTARG params[1];
    params[0].vt = VT_BSTR;
    params[0].bstrVal = SysAllocString(printerOLE);
    DISPPARAMS dispparams;
    dispparams.rgvarg = params;
    dispparams.rgdispidNamedArgs = FILE_PRINT_SETUP_PRINTER;
    dispparams.cArgs = 1;
    dispparams.cNamedArgs = 1;
    EXCEPINFO excepinfo;
    unsigned int uArgErr;
    HRESULT hresult = ((IDispatch *)instance)->Invoke(
                                                      FILE_PRINT_SETUP,
                                                      IID_NULL,
                                                      LOCALE_SYSTEM_DEFAULT,
                                                      DISPATCH_METHOD,
                                                      &dispparams,
                                                      NULL,
                                                      &excepinfo,
                                                      &uArgErr);
    delete printerOLE;
    SysFreeString(params[0].bstrVal);
    env->ReleaseStringUTFChars(printer, printerStr);
    if (hresult != S_OK) throwNew(env, runtimeException, hresult, "PrintSetup", &excepinfo);
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_servlet_WordController_print0(JNIEnv *env, jobject obj, jlong instance)
{
    DISPPARAMS dispparams = {NULL, NULL, 0, 0};
    EXCEPINFO excepinfo;
    unsigned int uArgErr;
    HRESULT hresult = ((IDispatch *)instance)->Invoke(
                                                      FILE_PRINT,
                                                      IID_NULL,
                                                      LOCALE_SYSTEM_DEFAULT,
                                                      DISPATCH_METHOD,
                                                      &dispparams,
                                                      NULL,
                                                      &excepinfo,
                                                      &uArgErr);
    if (hresult != S_OK) throwNew(env, runtimeException, hresult, "Print", &excepinfo);
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_servlet_WordController_replace0(JNIEnv *env, jobject obj, jlong instance, jstring from, jstring to)
{
    if (!from || !to) {
        env->ThrowNew(nullPointerException, "");
        return;
    }
    const char *fromStr = env->GetStringUTFChars(from, NULL);
    if (!fromStr) return;
    const char *toStr = env->GetStringUTFChars(to, NULL);
    if (!toStr) {
        env->ReleaseStringUTFChars(from, fromStr);
        return;
    }
    OLECHAR *fromOLE=(OLECHAR *)new OLECHAR[strlen(fromStr) + 1];
    char2OLECHAR(fromStr, fromOLE);
    OLECHAR *toOLE=(OLECHAR *)new OLECHAR[strlen(toStr) + 1];
    char2OLECHAR(toStr, toOLE);
    VARIANTARG params[12];
    params[11].vt = VT_BSTR;    // Find
    params[11].bstrVal = SysAllocString(fromOLE);
    params[10].vt = VT_BSTR;    // Replace
    params[10].bstrVal = SysAllocString(toOLE);
    params[9].vt = VT_I2;       // Direction
    params[9].iVal = 0;
    params[8].vt = VT_I2;       // MatchCase
    params[8].iVal = 1;
    params[7].vt = VT_I2;       // WholeWord
    params[7].iVal = 0;
    params[6].vt = VT_I2;       // PatternMatch
    params[6].iVal = 0;
    params[5].vt = VT_I2;       // SoundsLike
    params[5].iVal = 0;
    params[4].vt = VT_BOOL;     // FindNext
    params[4].boolVal = FALSE;
    params[3].vt = VT_BOOL;     // ReplaceOne
    params[3].boolVal = TRUE;
    params[2].vt = VT_BOOL;     // ReplaceAll
    params[2].boolVal = FALSE;
    params[1].vt = VT_I2;       // Format
    params[1].iVal = 0;
    params[0].vt = VT_I2;       // Wrap
    params[0].iVal = 1;
    DISPPARAMS dispparams;
    dispparams.rgvarg = params;
    dispparams.rgdispidNamedArgs = NULL;
    dispparams.cArgs = 12;
    dispparams.cNamedArgs = 0;
    EXCEPINFO excepinfo;
    unsigned int uArgErr;
    HRESULT hresult = ((IDispatch *)instance)->Invoke(
                                                      EDIT_REPLACE,
                                                      IID_NULL,
                                                      LOCALE_SYSTEM_DEFAULT,
                                                      DISPATCH_METHOD,
                                                      &dispparams,
                                                      NULL,
                                                      &excepinfo,
                                                      &uArgErr);
    delete fromOLE;
    SysFreeString(params[10].bstrVal);
    env->ReleaseStringUTFChars(from, fromStr);
    delete toOLE;
    SysFreeString(params[11].bstrVal);
    env->ReleaseStringUTFChars(to, toStr);
    if (hresult != S_OK) throwNew(env, runtimeException, hresult, "Replace", &excepinfo);
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_servlet_WordController_save0(JNIEnv *env, jobject obj, jlong instance)
{
    DISPPARAMS dispparams = {NULL, NULL, 0, 0};
    EXCEPINFO excepinfo;
    unsigned int uArgErr;
    HRESULT hresult = ((IDispatch *)instance)->Invoke(
                                                      FILE_SAVE,
                                                      IID_NULL,
                                                      LOCALE_SYSTEM_DEFAULT,
                                                      DISPATCH_METHOD,
                                                      &dispparams,
                                                      NULL,
                                                      &excepinfo,
                                                      &uArgErr);
    if (hresult != S_OK) throwNew(env, runtimeException, hresult, "Save", &excepinfo);
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_servlet_WordController_show0(JNIEnv *env, jobject obj, jlong instance)
{
    DISPPARAMS dispparams = {NULL, NULL, 0, 0};
    EXCEPINFO excepinfo;
    unsigned int uArgErr;
    HRESULT hresult = ((IDispatch *)instance)->Invoke(
                                                      APP_SHOW,
                                                      IID_NULL,
                                                      LOCALE_SYSTEM_DEFAULT,
                                                      DISPATCH_METHOD,
                                                      &dispparams,
                                                      NULL,
                                                      &excepinfo,
                                                      &uArgErr);
    if (hresult != S_OK) throwNew(env, runtimeException, hresult, "Show", &excepinfo);
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_servlet_WordController_hide0(JNIEnv *env, jobject obj, jlong instance)
{
    DISPPARAMS dispparams = {NULL, NULL, 0, 0};
    EXCEPINFO excepinfo;
    unsigned int uArgErr;
    HRESULT hresult = ((IDispatch *)instance)->Invoke(
                                                      APP_HIDE,
                                                      IID_NULL,
                                                      LOCALE_SYSTEM_DEFAULT,
                                                      DISPATCH_METHOD,
                                                      &dispparams,
                                                      NULL,
                                                      &excepinfo,
                                                      &uArgErr);
    if (hresult != S_OK) throwNew(env, runtimeException, hresult, "Hide", &excepinfo);
}

extern "C" JNIEXPORT jint JNICALL Java_FI_realitymodeler_servlet_WordController_initiate0(JNIEnv *env, jobject obj, jlong instance, jstring app, jstring topic)
{
    if (!app || !topic) {
        env->ThrowNew(nullPointerException, "");
        return 0;
    }
    const char *appStr = env->GetStringUTFChars(app, NULL);
    if (!appStr) return 0;
    const char *topicStr = env->GetStringUTFChars(topic, NULL);
    if (!topicStr) {
        env->ReleaseStringUTFChars(topic, topicStr);
        return 0;
    }
    OLECHAR *appOLE = (OLECHAR *)new OLECHAR[strlen(appStr) + 1];
    char2OLECHAR(appStr, appOLE);
    OLECHAR *topicOLE = (OLECHAR *)new OLECHAR[strlen(topicStr) + 1];
    char2OLECHAR(topicStr, topicOLE);
    VARIANTARG params[2];
    params[1].vt = VT_BSTR;
    params[1].bstrVal = SysAllocString(appOLE);
    params[0].vt = VT_BSTR;
    params[0].bstrVal = SysAllocString(topicOLE);
    DISPPARAMS dispparams;
    dispparams.rgvarg = params;
    dispparams.rgdispidNamedArgs = NULL;
    dispparams.cArgs = 2;
    dispparams.cNamedArgs = 0;
    EXCEPINFO excepinfo;
    unsigned int uArgErr;
    VARIANT channel;
    HRESULT hresult = ((IDispatch *)instance)->Invoke(
                                                      DDE_INITIATE,
                                                      IID_NULL,
                                                      LOCALE_SYSTEM_DEFAULT,
                                                      DISPATCH_METHOD,
                                                      &dispparams,
                                                      &channel,
                                                      &excepinfo,
                                                      &uArgErr);
    delete appOLE;
    SysFreeString(params[0].bstrVal);
    env->ReleaseStringUTFChars(app, appStr);
    delete topicOLE;
    SysFreeString(params[1].bstrVal);
    env->ReleaseStringUTFChars(topic, topicStr);
    if (hresult != S_OK) throwNew(env, runtimeException, hresult, "DDEInitiate", &excepinfo);
    return channel.iVal;
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_servlet_WordController_poke0(JNIEnv *env, jobject obj, jlong instance, jint channel, jstring item, jstring data)
{
    if (!item || !data) {
        env->ThrowNew(nullPointerException, "");
        return;
    }
    const char *itemStr = env->GetStringUTFChars(item, NULL);
    if (!itemStr) return;
    const char *dataStr = env->GetStringUTFChars(data, NULL);
    if (!dataStr) {
        env->ReleaseStringUTFChars(data, dataStr);
        return;
    }
    OLECHAR *itemOLE = (OLECHAR *)new OLECHAR[strlen(itemStr) + 1];
    char2OLECHAR(itemStr, itemOLE);
    OLECHAR *dataOLE = (OLECHAR *)new OLECHAR[strlen(dataStr) + 1];
    char2OLECHAR(dataStr, dataOLE);
    VARIANTARG params[3];
    params[2].vt = VT_I4;
    params[2].lVal = channel;
    params[1].vt = VT_BSTR;
    params[1].bstrVal = SysAllocString(itemOLE);
    params[0].vt = VT_BSTR;
    params[0].bstrVal = SysAllocString(dataOLE);
    DISPPARAMS dispparams;
    dispparams.rgvarg = params;
    dispparams.rgdispidNamedArgs = NULL;
    dispparams.cArgs = 3;
    dispparams.cNamedArgs = 0;
    EXCEPINFO excepinfo;
    unsigned int uArgErr;
    HRESULT hresult = ((IDispatch *)instance)->Invoke(
                                                      DDE_POKE,
                                                      IID_NULL,
                                                      LOCALE_SYSTEM_DEFAULT,
                                                      DISPATCH_METHOD,
                                                      &dispparams,
                                                      NULL,
                                                      &excepinfo,
                                                      &uArgErr);
    delete itemOLE;
    SysFreeString(params[0].bstrVal);
    env->ReleaseStringUTFChars(item, itemStr);
    delete dataOLE;
    SysFreeString(params[1].bstrVal);
    env->ReleaseStringUTFChars(data, dataStr);
    if (hresult != S_OK) throwNew(env, runtimeException, hresult, "DDEPoke", &excepinfo);
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_servlet_WordController_terminate0(JNIEnv *env, jobject obj, jlong instance, jint channel)
{
    VARIANTARG params[1];
    params[0].vt = VT_I2;
    params[0].iVal = channel;
    DISPPARAMS dispparams;
    dispparams.rgvarg = params;
    dispparams.rgdispidNamedArgs = NULL;
    dispparams.cArgs = 1;
    dispparams.cNamedArgs = 0;
    EXCEPINFO excepinfo;
    unsigned int uArgErr;
    HRESULT hresult = ((IDispatch *)instance)->Invoke(
                                                      DDE_TERMINATE,
                                                      IID_NULL,
                                                      LOCALE_SYSTEM_DEFAULT,
                                                      DISPATCH_METHOD,
                                                      &dispparams,
                                                      NULL,
                                                      &excepinfo,
                                                      &uArgErr);
    if (hresult != S_OK) throwNew(env, runtimeException, hresult, "DDETerminate", &excepinfo);
}
