
package FI.realitymodeler.servlet;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import java.util.*;

class ButtonListener implements ActionListener
{
	boolean pressed = false;

public void actionPerformed(ActionEvent e)
{
	pressed = true;
	((Window)((Button)e.getSource()).getParent()).setVisible(false);
}

}

public class install
{

static void configureService(int NAMPMajorVersion, URL actionURL, String name, String path, String queryPattern) throws IOException
{
	ByteArrayOutputStream bout = new ByteArrayOutputStream();
	switch (NAMPMajorVersion) {
	case 1:
		Support.writeBytes(bout,
		"newname="+ URLEncoder.encode(name) +
		"&url=" + URLEncoder.encode(path + "?" + queryPattern) +
		"&desc=" +
		"&fetch=CGI" +
		"&handle=FILTER" +
		"&lang=en"+
		"&maxmsgs=1" +
		"&answer=1" +
		"&visible=1" +
		"&enabled=1" +
		"&tariff=0" +
		"&msisdn=MSISDN" +
		"&service=" + URLEncoder.encode(name) +
		"&save=" + URLEncoder.encode("Save configuration"));
		break;
	case 2:
		Support.writeBytes(bout,
		"task=serviceCreated&" +
		"&keyword=" + URLEncoder.encode(name) +
		"&url=" + URLEncoder.encode(path) +
		"&parlist=" + URLEncoder.encode(queryPattern) +
		"&direct=ON" +
		"&msisdn=MSISDN" +
		"&contentcode=contenttext" +
		"&final=finaltext" +
		"&filter=filter" +
		"&hits=5" +
		"&fetchmethod=cgi" +
		"&valid=always" +
		"&enabled=ON" +
		"&visible=ON" +
		"&answer=ON" +
		"&service=oneshot" +
		"&submit=" + URLEncoder.encode(" Save "));
		break;
	}
	W3URLConnection uc = null;
	try
	{
		System.out.println("Sending " + new String(bout.toByteArray()) + " to " + actionURL);
		uc = W3URLConnection.openConnection(actionURL);
		uc.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		uc.setRequestProperty("Content-Length", String.valueOf(bout.size()));
		OutputStream out = uc.getOutputStream();
		bout.writeTo(out);
		out.flush();
		byte b[] = new byte[Support.bufferLength];
		InputStream in = uc.getInputStream();
		System.out.println("Returned status " + uc.getResponseCode());
		for (int n; (n = in.read(b)) > 0;) System.out.write(b, 0, n);
		System.out.println();
	}
	finally
	{
		if (uc != null) uc.disconnect();
	}
}

static boolean dialog(String title)
{
	Dialog dialog = new Dialog(new Frame(), title);
	dialog.addWindowListener(new WindowAdapter()
	{
		public void windowClosing(WindowEvent e)
		{
			e.getWindow().setVisible(false);
		}
	});
	dialog.setLayout(new FlowLayout());
	Button cancel = new Button("Cancel");
	dialog.add(cancel);
	ButtonListener cancelListener = new ButtonListener();
	cancel.addActionListener(cancelListener);
	dialog.setSize(640, 100);
	dialog.setModal(true);
	dialog.setVisible(true);
	return cancelListener.pressed;
}

static void exec(String cmd, boolean check) throws InterruptedException, IOException
{
	Process process = Runtime.getRuntime().exec("cmd /c " + cmd);
	process.waitFor();
	if (!check || process.exitValue() == 0) return;
	dialog("Error " + process.exitValue());
	System.out.println("Command " + cmd + " returned error value " + process.exitValue());
	System.exit(process.exitValue());
}

public static void main(String argv[])
{
	try {
	FileDialog fileDialog = new FileDialog(new Frame(), "Give Mercury Mail Server directory or choose Cancel if not in use", FileDialog.SAVE);
	fileDialog.setDirectory("\\MERCURY");
	fileDialog.setFile("Mercury Mail Server directory");
	fileDialog.setModal(true);
	fileDialog.setVisible(true);
	String mercuryDirectory;
	if (fileDialog.getFile() == null) mercuryDirectory = null;
	else mercuryDirectory = fileDialog.getDirectory();
	fileDialog = new FileDialog(new Frame(), "Give fax cover page directory or choose Cancel to abort installation", FileDialog.SAVE);
	fileDialog.setDirectory("\\FAXCOVERS");
	fileDialog.setFile("Fax cover page directory");
	fileDialog.setModal(true);
	fileDialog.setVisible(true);
	if (fileDialog.getFile() == null)
	{
		System.out.println("Operation canceled");
		System.exit(1);
	}
	String coverPageDirectory = fileDialog.getDirectory();
	exec("copy fax_cover.rtf " + new File(coverPageDirectory).getCanonicalPath() + File.separator, true);
	exec("bin\\install.exe", true);
// copying files for jrun
	String JSMDirectory = Support.readLine(new FileInputStream("JSMDirectory")).trim();
	exec("xcopy bin\\*.dll \"" + new File(JSMDirectory, "../bin").getCanonicalPath() + "\\\"", true);
	exec("xcopy /s classes\\*.* \"" + new File(JSMDirectory, "../classes").getCanonicalPath() + "\\\"", true);
// asking service configuration settings
	Dialog dialog = new Dialog(new Frame(), "Choose where services should be configured");
	dialog.setModal(true);
	dialog.setLayout(new GridLayout(5, 2));
	dialog.add(new Label("Domain name of the email"));
	TextField domainNameField = new TextField(InetAddress.getLocalHost().getHostName());
	dialog.add(domainNameField);
	dialog.add(new Label("SMTP server host name"));
	TextField smtpServerHostField = new TextField("localhost");
	dialog.add(smtpServerHostField);
	dialog.add(new Label("POP3 server host name"));
	TextField pop3ServerHostField = new TextField("localhost");
	dialog.add(pop3ServerHostField);
	CheckboxGroup group = new CheckboxGroup();
	Checkbox NAMP1Checkbox = new Checkbox("NAMP1 edit service action URL", group, true);
	TextField editServiceURLField = new TextField("http://localhost/cgi/netgate/wwgcgieditmx.pl", 70);
	dialog.add(NAMP1Checkbox);
	dialog.add(editServiceURLField);
	Checkbox NAMP2Checkbox = new Checkbox("NAMP2 management application URL", group, false);
	TextField mgtapplURLField = new TextField("http://localhost/servlet/mgtappl", 70);
	dialog.add(NAMP2Checkbox);
	dialog.add(mgtapplURLField);
	dialog.addWindowListener(new WindowAdapter()
	{
		public void windowClosing(WindowEvent e)
		{
			e.getWindow().setVisible(false);
		}
	});
	dialog.setSize(640, 200);
	int NAMPMajorVersion;
	for (;;)
	{
		dialog.setVisible(true);
		NAMPMajorVersion = group.getSelectedCheckbox() == NAMP1Checkbox ? 1 : 2;
		if (domainNameField.getText().trim().length() == 0 ||
		smtpServerHostField.getText().trim().length() == 0 ||
		pop3ServerHostField.getText().trim().length() == 0 ||
		(NAMPMajorVersion == 1 ? editServiceURLField.getText() :
		mgtapplURLField.getText()).trim().length() == 0)
		if (dialog("Some field was not specified, try again."))
		{
			System.out.println("Operation canceled");
			System.exit(1);
		}
		else continue;
		break;
	}
	URL actionURL = new URL(NAMPMajorVersion == 1 ? editServiceURLField.getText() : mgtapplURLField.getText());
// setting servlet parameters for jrun
	Properties servletsProperties = new Properties();
	File servletsPropertiesFile = new File(JSMDirectory, "services/jse/properties/servlets.properties");
	servletsProperties.load(new FileInputStream(servletsPropertiesFile));
	servletsProperties.put("servlet.echo.preload", "true");
	servletsProperties.put("servlet.echo.code", "FI.realitymodeler.servlet.EchoMessage");
	servletsProperties.put("servlet.fax.preload", "true");
	servletsProperties.put("servlet.fax.code", "FI.realitymodeler.servlet.Fax");
	servletsProperties.put("servlet.fax.args",
	"coverPageDirectory=" + coverPageDirectory +
	",domain=" + domainNameField.getText() +
	",NAMPNotifyMode=" + NAMPMajorVersion + "," +
	(NAMPMajorVersion == 1 ? "testFormActionURL=" + new URL(actionURL, "wwgcgitestmx.pl") :
	"inetServerHost=" + actionURL.getHost()) +
	",waitMins=1" +
	",smtpServerHost=" + smtpServerHostField.getText() +
	",pop3ServerHost=" + pop3ServerHostField.getText() +
	(mercuryDirectory != null ? ",mercuryDirectory=" + mercuryDirectory : ""));
	servletsProperties.put("servlet.wea.preload", "true");
	servletsProperties.put("servlet.wea.code", "FI.realitymodeler.servlet.Weather");
	servletsProperties.put("servlet.wea.args",
	"source=ftp://ftp.smhi.se/ftp/Europol/" +
	",username=Europol" +
	",password=europ_Pa" +
	",cleaningMins=10080" +
	",delayMins=5" +
	",areas=komprog_europol;kommuner.dat;worldprgeuropol;resorter.dat;golfprogeuropol;golforter.dat" +
	",hours=5;10;15");
	servletsProperties.store(new FileOutputStream(servletsPropertiesFile), "Servlets Properties saved by FI.realitymodeler.servlet.install");
	String baseURLString = "http://" + InetAddress.getLocalHost().getHostAddress() + "/servlet/";
	configureService(1, actionURL, "echo", baseURLString + "echo", "message=[message]");
	configureService(1, actionURL, "fax", baseURLString + "fax", "mobile=&receivers=[receivers]");
	configureService(1, actionURL, "wea", baseURLString + "wea", "mobile=&area=[area]");
	configureService(1, actionURL, "golf", baseURLString + "wea/golf", "mobile=&area=[area]");
	System.exit(0);
	} catch (Throwable th)
	{
		th.printStackTrace();
		dialog("Error " + th.toString());
		System.exit(1);
	}
}

}
