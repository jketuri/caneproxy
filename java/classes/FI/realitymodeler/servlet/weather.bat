pushd .
rd /s /q \ep\
xcopy %javahome%\classes\FI\ncscst\*.class \ep\classes\FI\ncscst\
xcopy %javahome%\classes\FI\ncscst\resources\*.* \ep\classes\FI\ncscst\resources\
xcopy %javahome%\classes\FI\ncscst\common\*.class \ep\classes\FI\ncscst\common\
xcopy %javahome%\classes\FI\ncscst\servlet\*.class \ep\classes\FI\ncscst\servlet\
xcopy %javahome%\classes\FI\ncscst\servlet\resources\*.* \ep\classes\FI\ncscst\servlet\resources\
xcopy /s %javahome%\classes\namp2\*.class \ep\classes\namp2\
xcopy %javahome%\classes\FI\ncscst\servlet\readme.txt \ep\
xcopy %javahome%\classes\FI\ncscst\servlet\fax_cover.doc \ep\
cd \ep\classes
%javahome%\bin\jar -xvf \jgl3.1.0\lib\jgl3.1.0.jar
rd /s /q \ep\classes\META-INF
xcopy %javahome%\bin\install.exe \ep\bin\
xcopy %javahome%\bin\servlet.dll \ep\bin\
xcopy %javahome%\bin\own.dll \ep\bin\
xcopy %javahome%\bin\ssl.dll \ep\bin\
xcopy %javahome%\bin\libeay32.dll \ep\bin\
xcopy %javahome%\bin\ssleay32.dll \ep\bin\
popd
