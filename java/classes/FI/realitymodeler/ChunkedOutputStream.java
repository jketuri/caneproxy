
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import java.io.*;

/** Converts output stream to HTTP 1.1 chunked stream. */
public class ChunkedOutputStream extends FilterOutputStream {
    public HeaderList footer = new HeaderList();

    int count = 0;
    boolean closed = false;
    byte buf[] = new byte[Support.bufferLength];

    public ChunkedOutputStream(OutputStream out) {
        super(out);
    }

    public void write(int b)
        throws IOException {
        if (count >= buf.length) flush();
        buf[count++] = (byte)b;
    }

    public void write(byte b[], int off, int len)
        throws IOException {
        while (len > 0) {
            if (count >= buf.length) flush();
            int n = Math.min(buf.length - count, len);
            System.arraycopy(b, off, buf, count, n);
            count += n;
            off += n;
            len -= n;
        }
    }

    public void flush()
        throws IOException {
        if (count == 0) return;
        Support.writeBytes(out, Integer.toHexString(count) + "\r\n");
        out.write(buf, 0, count);
        out.write('\r');
        out.write('\n');
        out.flush();
        count = 0;
    }

    public void close()
        throws IOException {
        if (closed) return;
        flush();
        Support.writeBytes(out, "0\r\n");
        Support.sendHeaderList(out, footer);
        out.flush();
        closed = true;
    }

}
