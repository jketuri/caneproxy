/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class FI_realitymodeler_SocketSet */

#ifndef _Included_FI_realitymodeler_SocketSet
#define _Included_FI_realitymodeler_SocketSet
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     FI_realitymodeler_SocketSet
 * Method:    construct
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL Java_FI_realitymodeler_SocketSet_construct
  (JNIEnv *, jobject);

/*
 * Class:     FI_realitymodeler_SocketSet
 * Method:    destroy
 * Signature: (J)V
 */
JNIEXPORT void JNICALL Java_FI_realitymodeler_SocketSet_destroy
  (JNIEnv *, jobject, jlong);

/*
 * Class:     FI_realitymodeler_SocketSet
 * Method:    clr0
 * Signature: (JI)V
 */
JNIEXPORT void JNICALL Java_FI_realitymodeler_SocketSet_clr0
  (JNIEnv *, jobject, jlong, jint);

/*
 * Class:     FI_realitymodeler_SocketSet
 * Method:    isSet0
 * Signature: (JI)Z
 */
JNIEXPORT jboolean JNICALL Java_FI_realitymodeler_SocketSet_isSet0
  (JNIEnv *, jobject, jlong, jint);

/*
 * Class:     FI_realitymodeler_SocketSet
 * Method:    set0
 * Signature: (JI)V
 */
JNIEXPORT void JNICALL Java_FI_realitymodeler_SocketSet_set0
  (JNIEnv *, jobject, jlong, jint);

/*
 * Class:     FI_realitymodeler_SocketSet
 * Method:    zero0
 * Signature: (J)V
 */
JNIEXPORT void JNICALL Java_FI_realitymodeler_SocketSet_zero0
  (JNIEnv *, jobject, jlong);

#ifdef __cplusplus
}
#endif
#endif
