
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#ifdef _WIN32
#include <io.h>
#include <sys/locking.h>
#include <sys/utime.h>
#include <windows.h>
#define _stat64 _stat
#define _wstat64 _wstat
#elif defined(__APPLE__)
#define _DARWIN_USE_64_BIT_INODE
#include <utime.h>
#define _chmod chmod
#define _stat64 stat
#define _tzset tzset
#define _utimbuf utimbuf
#define _utime utime
#define _S_IREAD S_IREAD
#define _S_IWRITE S_IWRITE
#define _S_IEXEC S_IEXEC
#else
#include <utime.h>
#define _chmod chmod
#define _stat64 stat64
#define _tzset tzset
#define _utimbuf utimbuf
#define _utime utime
#define _S_IREAD S_IREAD
#define _S_IWRITE S_IWRITE
#define _S_IEXEC S_IEXEC
#endif
#include "FI_realitymodeler_W3File.h"

static jclass arrayIndexOutOfBoundsException, ioexception;

#ifdef _WIN32
static void throwNew(JNIEnv *env, jclass throwable, DWORD n)
{
    LPSTR msg = NULL;
    if (FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, NULL, n,
                      MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&msg, 0, NULL)) {
        env->ThrowNew(throwable, msg);
        LocalFree(msg);
        return;
    }
    char str[32];
    _itoa(n, str, 10);
    LPSTR err = "error ";
    size_t l = strlen(err);
    if (!(msg = new char[l + strlen(str) + 1])) {
        env->ThrowNew(throwable, "error");
        return;
    }
    strcpy(msg, err);
    strcpy(msg + l, str);
    env->ThrowNew(throwable, msg);
    delete msg;
}
#endif

static jclass getClass(JNIEnv *env, char *name)
{
    jclass clazz;
    if ((clazz = env->FindClass(name))) clazz = (jclass)env->NewGlobalRef(clazz);
    return clazz;
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_W3File_initialize(JNIEnv *env, jclass clazz)
{
    if (!(arrayIndexOutOfBoundsException = getClass(env, (char *)"java/lang/ArrayIndexOutOfBoundsException")) ||
        !(ioexception = getClass(env, (char *)"java/io/IOException"))) return;
}

extern "C" JNIEXPORT jint JNICALL Java_FI_realitymodeler_W3File_getMaxFilenameLength(JNIEnv *env, jclass clazz)
{
    return FILENAME_MAX;
}

extern "C" JNIEXPORT jint JNICALL Java_FI_realitymodeler_W3File_getTimeZone(JNIEnv *env, jclass clazz)
{
    _tzset();
#ifdef _WIN32
    return (jint)(_timezone - _daylight * 60 * 60);
#else
    return (jint)(timezone - daylight * 60 * 60);
#endif
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_W3File_lockBytes0(JNIEnv *env, jclass clazz, jint fd0, jint nbytes)
{
#ifdef _WIN32
    if (_locking(fd0, _LK_NBRLCK, nbytes) == -1) env->ThrowNew(ioexception, strerror(errno));
#endif
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_W3File_unlockBytes0(JNIEnv *env, jclass clazz, jint fd0, jint nbytes)
{
#ifdef _WIN32
    if (_locking(fd0, _LK_UNLCK, nbytes) == -1) env->ThrowNew(ioexception, strerror(errno));
#endif
}

void readStat(JNIEnv *env, jstring path, struct _stat64 *buf)
{
#ifdef __MSVCRT__
    const jchar *str = env->GetStringChars(path, 0);
#else
    const char *str = env->GetStringUTFChars(path, 0);
#endif
    if (!str) return;
#ifdef __MSVCRT__
    if (_wstat64((wchar_t *)str, buf) == -1) env->ThrowNew(ioexception, strerror(errno));
    env->ReleaseStringChars(path, str);
#else
    if (_stat64(str, buf) == -1) env->ThrowNew(ioexception, strerror(errno));
    env->ReleaseStringUTFChars(path, str);
#endif
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_W3File_info0(JNIEnv *env, jobject clazz, jstring path, jlongArray longValues)
{
    if (longValues && env->GetArrayLength(longValues) < 11) {
        env->ThrowNew(arrayIndexOutOfBoundsException, "");
        return;
    }
    struct _stat64 buf;
    readStat(env, path, &buf);
    jlong *lvalues = env->GetLongArrayElements(longValues, NULL);
    if (!lvalues) return;
    lvalues[0] = buf.st_dev;
    lvalues[1] = buf.st_ino;
    lvalues[2] = buf.st_mode;
    lvalues[3] = buf.st_nlink;
    lvalues[4] = buf.st_uid;
    lvalues[5] = buf.st_gid;
    lvalues[6] = buf.st_rdev;
    lvalues[7] = buf.st_size;
    lvalues[8] = buf.st_atime;
    lvalues[9] = buf.st_mtime;
    lvalues[10] = buf.st_ctime;
    env->ReleaseLongArrayElements(longValues, lvalues, 0);
}

extern "C" JNIEXPORT jint JNICALL Java_FI_realitymodeler_W3File_lastAccess0(JNIEnv *env, jobject obj, jstring path)
{
    struct _stat64 buf;
    readStat(env, path, &buf);
    return (jint)buf.st_atime;
}

extern "C" JNIEXPORT jint JNICALL Java_FI_realitymodeler_W3File_timeCreated0(JNIEnv *env, jobject obj, jstring path)
{
    struct _stat64 buf;
    readStat(env, path, &buf);
    return (jint)buf.st_ctime;
}

extern "C" JNIEXPORT jint JNICALL Java_FI_realitymodeler_W3File_timeModified0(JNIEnv *env, jobject obj, jstring path)
{
    struct _stat64 buf;
    readStat(env, path, &buf);
    return (jint)buf.st_mtime;
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_W3File_setLastTimes0(JNIEnv *env, jobject obj, jstring path, jint atime, jint mtime)
{
    const char *str = env->GetStringUTFChars(path, 0);
    if (!str) return;
    struct _utimbuf utb;
    utb.actime = atime;
    utb.modtime = mtime;
    if (_utime(str, &utb) == -1) env->ThrowNew(ioexception, strerror(errno));
    env->ReleaseStringUTFChars(path, str);
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_W3File_setReadAndWrite0(JNIEnv *env, jobject obj, jstring path)
{
#ifdef _WIN32
    const jchar *str = env->GetStringChars(path, 0);
    if (!str) return;
    DWORD attributes = GetFileAttributesW((WCHAR *)str);
    if (attributes == -1 || !SetFileAttributesW((WCHAR *)str, attributes & (0xffffffff ^ FILE_ATTRIBUTE_READONLY))) throwNew(env, ioexception, GetLastError());
    env->ReleaseStringChars(path, str);
#else
    const char *str = env->GetStringUTFChars(path, 0);
    if (!str) return;
    struct _stat64 buf;
    if (_stat64(str, &buf) == -1) env->ThrowNew(ioexception, strerror(errno));
    if (_chmod(str, buf.st_mode | _S_IREAD | _S_IWRITE) == -1) env->ThrowNew(ioexception, strerror(errno));
    env->ReleaseStringUTFChars(path, str);
#endif
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_W3File_setOffline0(JNIEnv *env, jobject obj, jstring path)
{
#ifdef _WIN32
    const jchar *str = env->GetStringChars(path, 0);
    if (!str) return;
    DWORD attributes = GetFileAttributesW((WCHAR *)str);
    if (attributes == -1 || !SetFileAttributesW((WCHAR *)str, attributes | FILE_ATTRIBUTE_OFFLINE)) throwNew(env, ioexception, GetLastError());
    env->ReleaseStringChars(path, str);
#else
    const char *str = env->GetStringUTFChars(path, 0);
    if (!str) return;
    struct _stat64 buf;
    if (_stat64(str, &buf) == -1) env->ThrowNew(ioexception, strerror(errno));
    if (_chmod(str, buf.st_mode | _S_IEXEC) == -1) env->ThrowNew(ioexception, strerror(errno));
    env->ReleaseStringUTFChars(path, str);
#endif
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_W3File_setOnline0(JNIEnv *env, jobject obj, jstring path)
{
#ifdef _WIN32
    const jchar *str = env->GetStringChars(path, 0);
    if (!str) return;
    DWORD attributes = GetFileAttributesW((WCHAR *)str);
    if (attributes == -1 || !SetFileAttributesW((WCHAR *)str, attributes & (0xffffffff ^ FILE_ATTRIBUTE_OFFLINE))) throwNew(env, ioexception, GetLastError());
    env->ReleaseStringChars(path, str);
#else
    const char *str = env->GetStringUTFChars(path, 0);
    if (!str) return;
    struct _stat64 buf;
    if (_stat64(str, &buf) == -1) env->ThrowNew(ioexception, strerror(errno));
    if (_chmod(str, buf.st_mode & (0xffffffff ^ _S_IEXEC)) == -1) env->ThrowNew(ioexception, strerror(errno));
    env->ReleaseStringUTFChars(path, str);
#endif
}

extern "C" JNIEXPORT jboolean JNICALL Java_FI_realitymodeler_W3File_isOffline0(JNIEnv *env, jobject obj, jstring path)
{
#ifdef _WIN32
    const jchar *str = env->GetStringChars(path, 0);
    if (!str) return JNI_FALSE;
    DWORD attributes = GetFileAttributesW((WCHAR *)str);
    if (attributes == -1) throwNew(env, ioexception, GetLastError());
    env->ReleaseStringChars(path, str);
    return (attributes & FILE_ATTRIBUTE_OFFLINE) != 0;
#else
    const char *str = env->GetStringUTFChars(path, 0);
    if (!str) return JNI_FALSE;
    struct _stat64 buf;
    if (_stat64(str, &buf) == -1) env->ThrowNew(ioexception, strerror(errno));
    env->ReleaseStringUTFChars(path, str);
    return (buf.st_mode & _S_IEXEC) != 0;
#endif
}

extern "C" JNIEXPORT jboolean JNICALL Java_FI_realitymodeler_W3File_isWriteable0(JNIEnv *env, jobject obj, jstring path)
{
#ifdef _WIN32
    const jchar *str = env->GetStringChars(path, 0);
    if (!str) return JNI_FALSE;
    DWORD attributes = GetFileAttributesW((WCHAR *)str);
    if (attributes == -1) throwNew(env, ioexception, GetLastError());
    env->ReleaseStringChars(path, str);
    return (attributes & FILE_ATTRIBUTE_READONLY) == 0;
#else
    const char *str = env->GetStringUTFChars(path, 0);
    if (!str) return JNI_FALSE;
    struct _stat64 buf;
    if (_stat64(str, &buf) == -1) env->ThrowNew(ioexception, strerror(errno));
    env->ReleaseStringUTFChars(path, str);
    return (buf.st_mode & _S_IWRITE) != 0;
#endif
}
