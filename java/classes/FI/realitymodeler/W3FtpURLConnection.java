
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;
import javax.servlet.*;

class FtpPipingThread extends CacheFillingThread {
    W3FtpRequester ftpRequester;
    String currentDir, path, target = null;
    RegexpPool listFilter = null;
    boolean mobile = false;
    int mode = W3FtpURLConnection.ALL;

    FtpPipingThread(W3FtpURLConnection uc) {
        super(null);
        this.ftpRequester = (W3FtpRequester)uc.ftpRequester.clone();
    }

    public void run() {
        try {
            String s = Support.htmlString(currentDir);
            writeBytes("<html><head>\n");
            writeBytes("<style type=\"text/css\">\n");
            writeBytes("span.listcolumn0 { width: 15em; clear: left; float: left }\n");
            writeBytes("span.listcolumn { width: 15em; float: left }\n");
            writeBytes("</style>\n");
            writeBytes("<title>" + s + "</title></head><body>\n");
            writeBytes("<h1>" + MessageFormat.format(uc.messages.getString("directoryOfFile"), new Object[] {s}) + "</h1>\n");
            if (ftpRequester.welcomeMsg != null && currentDir.equals("/")) {
                writeBytes("<hr>");
                Support.writeBytes(Support.getHtmlOutputStream(out, uc.servletPathTable, uc.charsetName), ftpRequester.welcomeMsg + "\0", uc.charsetName);
                writeBytes("<hr>");
            }
            if (!currentDir.equals("/")) writeBytes("<a href=\"" +
                                                    Support.encodePath(currentDir.substring(0, currentDir.lastIndexOf('/', currentDir.length() - 2) + 1)) +
                                                    (target != null ? ";target=" + target : "") + "\">" + uc.messages.getString("parentDirectory") + "</a><p>\n");
            int totalUnits[] = new int[1];
            int numberFiles = ((W3FtpURLConnection)uc).parse(source, out, null, path, target, uc.charsetName, listFilter, true, mobile, mode, totalUnits);
            writeBytes(MessageFormat.format(uc.messages.getString("numberFilesTotalUnits"), new Object[] {new Long(numberFiles), new Long(totalUnits[0])}));
            writeBytes("</body></html>");
            out.close();
        } catch (Exception ex) {
            try {
                if (in != null)
                    synchronized (in) {
                        in.close();
                        in.notifyAll();
                    }
            } catch (IOException ex1) {} finally {
                uc.log(getClass().getName(), ex);
            }
        }
    }

}

/** Implements cacheable URL connection to File Transfer
    Protocol-servers.<br>
    Url must be of form: (refer to rfc1738)<br>

    ftp://host:port/path<br>
    [;type=a(scii) | i(mage) | d(irectory)]<br>
    [;target=file target window]<br>
    [;filter=list filter]<br>
    [;mode=a(ll) | n(ormal) | d(irs) | f(iles) | l(inks)]
*/
public class W3FtpURLConnection extends W3URLConnection {
    public static long cacheCleaningInterval = 7L * 24L * 60L * 60L * 1000L, cacheRefreshingInterval = 48L * 60L * 60L * 1000L, cacheCleaningTime = 0L;
    public static String cacheRoot = "cache/ftp/";
    public static boolean defaultUseCaches = false, useProxy = false;
    public static String proxyHost = null;
    public static int proxyPort = 80;
    public static String version = "FI.realitymodeler.W3FtpURLConnection/2000-5-10";

    static final int ALL = 0, NORMAL = 1, DIRS = 2, FILES = 3, LINKS = 4;
    static final byte UNIX = 0, WINDOWS = 1, VMS = 2;
    static SimpleDateFormat ftpDateFormats[][] = { {
            // Unix-type ftp-date-format
            new SimpleDateFormat("MMM d yyyy", Locale.US),
            // Variation to ibid.
            new SimpleDateFormat("MMM d HH:mm", Locale.US)
        }, {
            // Windows-type ftp-date-format
            new SimpleDateFormat("MM-dd-yy hh:mmaa", Locale.US)
        }, {
            // VMS-type ftp-date-format
            new SimpleDateFormat("dd-MMM-yyyy HH:mm", Locale.US),
            // Variation to ibid.
            new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.US)
        }
    };

    W3FtpRequester ftpRequester = null;
    String currentDir = null;
    char typeCode;

    public static void cleanCache(ServletContext servletContext) {
        cacheCleaningTime = cleanCache(servletContext, cacheRoot, cacheCleaningInterval);
    }

    class Entity {
        Date date = null;
        String size = null;
        String name = null;

        public String toString() {
            return getClass().getName() + "[date=" + date + ",size=" + size + ",name=" + name + "]";
        }

    }

    static int parse(InputStream in, OutputStream out, Entity entity, String path, String target, String charsetName, RegexpPool listFilter, boolean list, boolean mobile, int mode, int totalUnits[])
        throws IOException {
        Date date = null, today = new Date();
        GregorianCalendar calendar = new GregorianCalendar(),
            calendar1 = new GregorianCalendar();
        String name = null, size = null;
        String s, sa[] = new String[4];
        StringTokenizer st;
        int c, i, j, k, numberFiles = 0;
        for (;;) {
            if ((s = Support.readLine(in)) == null) break;
            st = new StringTokenizer(s);
            for (i = 0; i < 4 && st.hasMoreTokens(); i++) sa[i] = st.nextToken();
            if (i > 0 && i < 4) continue;
            if (i > 0)
                if (st.hasMoreTokens())
                    // Unix-type ftp-server
                    for (;;) {
                        size = st.nextToken();
                        if (size.endsWith(",")) size = st.nextToken();
                        if (!Character.isDigit(size.charAt(0))) {
                            s = size + " " + st.nextToken("");
                            size = sa[3];
                        } else s = st.nextToken("");
                        if (!list && !Character.isDigit(size.charAt(0))) break;
                        st = new StringTokenizer(s);
                        StringBuffer sb = new StringBuffer();
                        for (i = 0; i < 3; i++) {
                            if (sb.length() > 0) sb.append(' ');
                            sb.append(st.nextToken());
                        }
                        s = sb.toString();
                        name = st.nextToken("").trim();
                        try {
                            if (s.indexOf(':') == -1) date = ftpDateFormats[UNIX][0].parse(s);
                            else if ((date = ftpDateFormats[UNIX][1].parse(s)) != null) {
                                calendar.setTime(date);
                                calendar1.setTime(today);
                                int year = calendar1.get(Calendar.YEAR);
                                calendar1.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
                                calendar.set(Calendar.YEAR, calendar.after(calendar1) ? year - 1 : year);
                                date = calendar.getTime();
                            }
                        } catch (ParseException ex) {
                            if (verbose)
                                ex.printStackTrace();
                        }
                        if ((i = name.lastIndexOf(" ->")) != -1) s = name.substring(0, i);
                        else s = name;
                        String v = null;
                        switch (sa[0].charAt(0)) {
                        case 'd':
                            if (mode > 2) break;
                            size = null;
                            v = "Directory";
                            if (!s.endsWith("/")) s += "/";
                            break;
                        case 'l':
                            if (mode > 0 && mode < 4) break;
                            size = null;
                            v = "Symbolic link";
                            break;
                        default:
                            if (mode == 2 || mode == 4) break;
                            v = size;
                            if (totalUnits != null)
                                try {
                                    totalUnits[0] += Integer.parseInt(size);
                                } catch (NumberFormatException ex) {}
                        }
                        if (list) {
                            if (!name.startsWith(".") && v != null && (listFilter == null || listFilter.match(s) != null)) {
                                Support.writeBytes(out, "<span class=\"listcolumn0\">", charsetName);
                                writeLine(out, date, v, path + Support.encodePath(s), target, name, charsetName, mobile);
                                numberFiles++;
                            }
                            if ((s = Support.readLine(in)) == null) break;
                            st = new StringTokenizer(s);
                            for (i = 0; i < 4; i++) sa[i] = st.nextToken();
                        } else break;
                    }
                else
                    // Windows-type ftp-server
                    for (;;) {
                        try {
                            date = ftpDateFormats[WINDOWS][0].parse(sa[0] + " " + sa[1]);
                        } catch (ParseException ex) {}
                        size = sa[2];
                        name = sa[3];
                        s = name;
                        String v = null;
                        if (size.equals("<DIR>")) {
                            if (mode < 3) {
                                size = null;
                                v = "Directory";
                                if (!s.endsWith("/")) s += "/";
                            }
                        } else if (mode != 2 && mode != 4) {
                            v = size;
                            if (totalUnits != null)
                                try {
                                    totalUnits[0] += Integer.parseInt(size);
                                } catch (NumberFormatException ex) {}
                        }
                        if (list) {
                            if (!name.startsWith(".") && v != null && (listFilter == null || listFilter.match(s) != null)) {
                                Support.writeBytes(out, "<span class=\"listcolumn0\">", charsetName);
                                writeLine(out, date, v, path + Support.encodePath(s), target, name, charsetName, mobile);
                                numberFiles++;
                            }
                            if ((s = Support.readLine(in)) == null || s.equals("")) break;
                            st = new StringTokenizer(s);
                            for (i = 0; i < 4; i++) sa[i] = st.nextToken();
                        } else break;
                    } else {
                // VMS-type ftp server
                for (i = 0; i < 2; i++) if ((s = Support.readLine(in)) == null) break;
                if (s != null)
                    while ((s = Support.readLine(in)) != null && !s.equals("")) {
                        st = new StringTokenizer(s);
                        name = st.nextToken();
                        if (!st.hasMoreTokens()) {
                            if ((s = Support.readLine(in)) == null || s.equals("")) break;
                            st = new StringTokenizer(s);
                        }
                        String v = st.nextToken();
                        st = new StringTokenizer(st.nextToken(""));
                        StringBuffer sb = new StringBuffer();
                        for (i = 0; i < 2; i++) {
                            if (sb.length() > 0) sb.append(' ');
                            sb.append(st.nextToken());
                        }
                        s = sb.toString();
                        try {
                            if ((i = s.indexOf('.')) == -1) date = ftpDateFormats[VMS][0].parse(s);
                            else date = ftpDateFormats[VMS][1].parse(s.substring(0, i));
                        } catch (ParseException ex) {}
                        if ((i = name.indexOf(".DIR")) != -1) {
                            if (mode < 3) {
                                v = "Directory";
                                s = name.substring(0, i) + "/";
                            }
                        } else if (mode != 2 && mode != 4) {
                            s = name;
                            if (totalUnits != null)
                                try {
                                    totalUnits[0] += Integer.parseInt(v);
                                } catch (NumberFormatException ex) {}
                        }
                        if (list) {
                            if (v != null && (listFilter == null || listFilter.match(s) != null)) {
                                Support.writeBytes(out, "<span class=\"listcolumn0\">", charsetName);
                                writeLine(out, date, v, path + Support.encodePath(s), target, name, charsetName, mobile);
                                numberFiles++;
                            }
                        } else break;
                    }
            }
            break;
        }
        if (entity != null) {
            entity.date = date;
            entity.size = size;
            entity.name = name;
        }
        return numberFiles;
    }

    public W3FtpURLConnection(URL url) {
        super(url);
    }

    public String getVersion() {
        return version;
    }

    public void checkCacheCleaning() {
        if (cacheCleaningInterval >= 0L &&
            System.currentTimeMillis() - cacheCleaningTime > cacheCleaningInterval) cleanCache(servletContext);
    }

    public void open()
        throws IOException {
        if (requester != null) return;
        if (useProxy) {
            proxyConnect(proxyHost, proxyPort);
            return;
        }
        String s;
        typeCode = (s = params.get("type")) != null && !s.equals("") ?
            Character.toLowerCase(s.charAt(0)) : 'i';
        requester = ftpRequester = new W3FtpRequester();
        if (connectTimeout > 0) ftpRequester.setConnectTimeout(connectTimeout);
        if (timeout > 0) ftpRequester.setTimeout(timeout);
        ftpRequester.open(url.getHost(), url.getPort() != -1 ? url.getPort() : W3FtpRequester.FTP_PORT, localAddress, localPort);
        if (getBasicCredentials())
            try {
                ftpRequester.login(username, password);
                loggedIn = true;
                return;
            } catch (LoginException ex) {}
        try {
            ftpRequester.login("anonymous", "anonymous@" + url.getHost());
            loggedIn = true;
        } catch (LoginException ex) {}
    }

    public void doConnect()
        throws IOException {
        if (connected) return;
        if (checkCacheFile(cacheRoot, W3FtpRequester.FTP_PORT)) return;
        open();
    }

    boolean setDir(String name, boolean setToRoot) {
        if (currentDir != null)
            try {
                for (int n = new StringTokenizer(currentDir, "/").countTokens(); n > 0; n--)
                    ftpRequester.cd("..");
            } catch (IOException ex) {
                setToRoot = true;
            }
        if (setToRoot)
            try {
                ftpRequester.cd("/");
            } catch (IOException ex) {
                try {
                    ftpRequester.cd("[000000]");
                } catch (IOException ex1) {}
            }
        currentDir = "/";
        StringTokenizer st = new StringTokenizer(name, "/");
        try {
            while (st.hasMoreTokens()) {
                String s = st.nextToken().trim();
                if (s.equals("") || s.equals(".")) continue;
                ftpRequester.cd(s);
                if (s.equals("..")) currentDir = currentDir.substring(0,
                                                                      currentDir.lastIndexOf('/', currentDir.length() - 2) + 1);
                else currentDir += s + "/";
            }
            return true;
        } catch (IOException ex) {}
        return false;
    }

    public InputStream getInput(boolean multiple)
        throws IOException {
        if (useProxy) return super.getInput(multiple);
        if (verbose)
            requestHeaderFields.dump(logStream);
        requestTime = System.currentTimeMillis();
        mayKeepAlive = true;
        if (!loggedIn) return in = getUnauthorizedStream(ftpRequester.welcomeMsg);
        FtpPipingThread piping = new FtpPipingThread(this);
        filling = piping;
        piping.mobile = isMobile();
        path = Support.decodePath(path);
        boolean list;
        for (boolean setToRoot = true;; setToRoot = false) {
            list = setDir(path, setToRoot);
            headerFields = new HeaderList();
            headerFields.append(new Header("", protocol_1_1 + " " + HTTP_OK));
            if (list) {
                try {
                    in = origin = ftpRequester.list();
                } catch (IOException ex) {
                    if (!setToRoot) throw ex;
                    continue;
                }
                piping.path = requestRoot != null ? requestRoot :
                    path.endsWith("/") ? "" : path.substring(path.lastIndexOf('/') + 1) + "/";
            } else {
                String name = path.substring(path.lastIndexOf("/") + 1);
                Entity entity = new Entity();
                try {
                    in = origin = ftpRequester.list(name);
                } catch (IOException ex1) {
                    in = null;
                }
                if (typeCode != 'd') {
                    if (in != null)
                        try {
                            parse(in, null, entity, piping.path, null, charsetName, null, false, piping.mobile, ALL, null);
                        } finally {
                            in.close();
                            in = null;
                        }
                    if (entity.name == null || !entity.name.equals(name)) entity = null;
                    else if (entity != null) {
                        if (entity.date != null) lastModified = entity.date.getTime();
                        if (entity.size != null) contentLength = Integer.parseInt(entity.size);
                    }
                    if (lastModified != 0L && ifModifiedSince >= lastModified) {
                        headerFields = new HeaderList();
                        headerFields.append(new Header("", protocol_1_1 + " " + HTTP_NOT_MODIFIED));
                        return null;
                    }
                    if (useCaches) {
                        if (cacheFile != null && cacheFile.exists() && cacheFile.lastModified() >= lastModified) {
                            int contentLength1 = contentLength;
                            long lastModified1 = lastModified;
                            if (setCacheFile(cacheHeaders) && contentLength == contentLength1) return in;
                            rangeStart = contentLength;
                            contentLength = contentLength1;
                            lastModified = lastModified1;
                        }
                        checkCacheCleaning();
                    }
                    if (lastModified != 0L) headerFields.append(new Header("Last-Modified", Support.format(new Date(lastModified))));
                    if (contentLength != -1L) headerFields.append(new Header("Content-Length", contentLength));
                    parseRange();
                    String ct;
                    if ((ct = guessContentType(name)) == null) ct = Support.unknownType;
                    headerFields.append(new Header("Content-Type", ct));
                    if (typeCode == 'a') ftpRequester.text();
                    else ftpRequester.binary();
                    try {
                        in = null;
                        if (rangeStart > 0L)
                            try {
                                in = ftpRequester.get(name, rangeStart);
                                headerFields.replace(new Header("", protocol_1_1 + " " + HTTP_PARTIAL));
                                headerFields.replace(new Header("Content-Range", "bytes " + rangeStart + "-" + (contentLength - 1L) + "/" + contentLength));
                                headerFields.replace(new Header("Content-Length", contentLength -= rangeStart));
                            } catch (IOException ex) {
                                in = null;
                            }
                        if (in == null) in = ftpRequester.get(name);
                        origin = in;
                    } catch (IOException ex) {
                        if (!setToRoot) throw ex;
                        continue;
                    }
                    Support.copyHeaderList(headerFields, headerList = new HeaderList());
                    return checkMessage(cacheHeaders);
                }
            }
            headerFields.append(new Header("Content-Type", Support.htmlType));
            headerFields.append(new Header("Cache-Control", "no-cache"));
            headerFields.append(new Header("Pragma", "no-cache"));
            Support.copyHeaderList(headerFields, headerList = new HeaderList());
            break;
        }
        if (in == null) throw new IOException("Failed to get stream");
        piping.currentDir = currentDir;
        piping.listFilter = null;
        if (params != null) {
            String s;
            if ((s = params.get("filter")) != null)
                try {
                    (piping.listFilter = new RegexpPool()).add(s, "");
                } catch (RegexException ex) {
                    piping.listFilter = null;
                }
            if ((s = params.get("mode")) != null) piping.mode = (s = s.toLowerCase()).startsWith("n") ? NORMAL : s.startsWith("d") ? DIRS : s.startsWith("f") ? FILES : s.startsWith("l") ? LINKS : ALL;
            piping.target = params.get("target");
        }
        piping.setup(this);
        in = new BufferedInputStream(piping.in = new PipedInputStream(piping.out = new PipedOutputStream()));
        piping.start();
        return checkMessage(cacheHeaders);
    }

    public InputStream getInputStream()
        throws IOException {
        if (inputDone) return in;
        cached = false;
        if (doOutput) {
            inputDone = true;
            useCaches = false;
            if (requester == null) throw new IOException("Not connected");
            if (useProxy && mayKeepAlive) {
                in = requester.input;
                checkInput();
                return in;
            }
            mayKeepAlive = true;
            if (!useProxy && !loggedIn) return in = getUnauthorizedStream(ftpRequester.welcomeMsg);
            headerFields = new HeaderList();
            headerFields.append(new Header("", protocol_1_1 + " " + HTTP_NO_CONTENT));
            return in = null;
        }
        connect();
        if (inputDone) return in;
        inputDone = true;
        setRequestMethod();
        setRequestFields();
        return getInput(false);
    }

    public OutputStream getOutputStream()
        throws IOException {
        sink = null;
        doOutput = true;
        inputDone = false;
        connect();
        if (!loggedIn) return null;
        setRequestMethod();
        setRequestFields();
        if (useProxy) return getOutput();
        ftpRequester.binary();
        int i = path.lastIndexOf('/');
        if (i != -1) {
            if (!setDir(path.substring(path.startsWith("/") ? 1 : 0, i), true))
                throw new FileNotFoundException();
            path = path.substring(i + 1);
        } else if (!setDir("", true)) throw new FileNotFoundException();
        return sink = ftpRequester.put(path);
    }

    public void setDefaultUseCaches(boolean useCaches) {
        W3FtpURLConnection.defaultUseCaches = useCaches;
    }

    public boolean getDefaultUseCaches() {
        return W3FtpURLConnection.defaultUseCaches;
    }

    public int getDefaultPort() {
        return W3FtpRequester.FTP_PORT;
    }

    public boolean usingProxy() {
        return useProxy;
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public int getProxyPort() {
        return proxyPort;
    }

    public void setCacheCleaningInterval(long cacheCleaningInterval) {
        W3FtpURLConnection.cacheCleaningInterval = cacheCleaningInterval;
    }

    public void setCacheRefreshingInterval(long cacheRefreshingInterval) {
        W3FtpURLConnection.cacheRefreshingInterval = cacheRefreshingInterval;
    }

    public long getCacheRefreshingInterval() {
        return cacheRefreshingInterval;
    }

    public boolean hasBlockMode() {
        return ftpRequester.hasBlockMode();
    }

}
