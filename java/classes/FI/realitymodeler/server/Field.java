
package FI.realitymodeler.server;

import java.io.*;
import java.net.*;
import java.util.*;

public class Field {
    String name;
    String value;
    boolean hidden;

    public Field(String name, String value, boolean hidden) {
        this.name = name;
        this.value = value;
        this.hidden = hidden;
    }

    public Field(String coded) {
        StringTokenizer st = new StringTokenizer(coded, "=");
        name = st.nextToken();
        value = st.nextToken();
        boolean hidden;
        if (name.startsWith("*")) {
            hidden = true;
            name = name.substring(1);
        } else hidden = false;
    }

    public String toString() {
        return hidden ? "(" + name + "=" + value + ")" : name + "=" + value;
    }

    public String toCoded() throws UnsupportedEncodingException {
        return (hidden ? "*" : "") + URLEncoder.encode(name, "8859_1") + '=' + URLEncoder.encode(value, "8859_1");
    }

    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    public boolean isHidden() {
        return hidden;
    }

}
