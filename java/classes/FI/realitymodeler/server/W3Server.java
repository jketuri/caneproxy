
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.lang.reflect.*;
import java.net.*;
import java.nio.*;
import java.nio.channels.*;
import java.nio.channels.spi.*;
import java.sql.*;
import java.text.*;
import java.util.*;
import java.util.Date;
import javax.naming.*;
import javax.naming.directory.*;
import javax.naming.ldap.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

class W3ServerShutdownHook extends Thread {
    W3Server w3 = null;

    W3ServerShutdownHook(W3Server w3) {
        this.w3 = w3;
    }

    public void run() {
        W3Server.shutdown();
    }

}

class CacheFile {
    byte data[];
    long lastModified;
    String entityTag;
}

class Client {
    int connections;
}

class LogFieldAdapter {

    public String getField(W3Request w3r) throws IOException {
        return null;
    }

    public String getField(W3Request w3r, String name) throws IOException {
        return null;
    }

}

class LogRecord {
    String orderKey;
    String hostAddr;
    String hostName;
    long totalRequests;
    long totalTimeTakenMs;
    long totalBytesTransferred;
    long firstTime;
    long lastTime;
}

class LogMap extends HashMap<String, LogRecord> {
    static final long serialVersionUID = 0L;
}

class StatMap extends TreeMap<String, LogRecord> {
    static final long serialVersionUID = 0L;

    StatMap() {
        super(Support.stringComparator);
    }

}

/** World Wide Web (HTTP 1.1) server enhanced with servlet interface.
    See RFC2616. Servlets are called with following URL:<br>

    <span style="white-space: nowrap">
    servlet/&lt;protection directory&gt;/&lt;servlet name&gt;/&lt;extra path&gt;?&lt;arguments&gt;
    </span><br>

    Protection directory may be used to categorize servlets to different
    domains.<br>

    Server is started with the following command:<br>

    <b>java FI.realitymodeler.server.W3Server [options]</b><br>

    where allowed options are: (default value is in parantheses)<br>

    <b>-c &lt;cache root&gt; (cache/)</b><br>

    Proxy cache root directory where files are subdirectoried by protocol
    and host name.<br>

    <b>-d &lt;data directory&gt; (/)</b><br>

    This sets the directory where data files needed by the server reside.<br>

    <b>-e</b><br>

    Indicates that this server instance is running as service. This option
    is used only by W3Service.exe-program and is not intented to be used
    manually.<br>

    <b>-k</b><br>

    This specifies that only caches are checked for foreign content. Can be
    used when browsing offline cache content.<br>

    <b>-l &lt;log level 0 - 3&gt; (0)</b><br>

    This sets logging on. In log level 1 errors are logged to error_log, in
    log level 2 other events are logged to event_log and in log level 3
    access operations are logged to file access_log.<br>

    <b>-m</b><br>

    Sets own security manager, which ignores all checks.<br>

    <b>-p &lt;port number&gt; (80 or 443 when secure)</b><br>

    This is the port number server starts to listen.<br>

    <b>-s</b><br>

    This sets secure mode on. In this mode server is accessible only with
    HTTPS-protocol.  Data directory must have files named cert.pem and
    key.pem containing certificate and RSA private key in PEM format.<br>

    <b>-t</b><br>

    Type files to standard output.<br>

    <b>-v</b><br>

    This sets verbose mode on. All requests are verbosed to the
    standard output. This is usable only when testing. No logging
    information to file is formed. l- and v-options are mutually
    exclusive. That which is last used is effective.<br>

    <b>-z</b><br>

    Separates distinct option sets for different server instances.  Second
    server can be started in e.g. secure mode. Data files common to all
    server instances should be in the data directory of first server.<br>

    <p>Before options in command line are applied, initial options are read
    from file named <i>/KeppiProxyOptions</i> if it exists. Right after -d
    option is encountered file named server.properties is tried to read from
    data directory. It is of standard java permanent properties file format
    (key=value). Following key value pairs are allowed (default value if
    applicable is in parantheses):<br>

    <i>
    autoCaches=true | false (false)<br>
    autoLogStats=true | false (false)<br>
    autoResume=true | false (false)<br>
    backlog=&lt;backlog count&gt; (50)<br>
    bindAddress=&lt;bind address&gt;<br>
    cacheRoot=&lt;directory, static&gt; (&lt;data directory&gt;/cache/)<br>
    characterEncoding=&lt;encoding&gt;
    completionBufferSize&lt;size of buffer in bytes before headers are written&gt; (4 * 1024)<br>
    contentEncoding=gzip | deflate<br>
    dataDirectory=&lt;directory&gt; (/)<br>
    disableProxyPipelining=true | false (false)<br>
    forceCaches=true | false (false)<br>
    ftpProxyCache=true | false (false)<br>
    ftpProxyCacheCleaningInterval=&lt;minutes, static&gt; (one week)<br>
    ftpProxyCacheRefreshingInterval=&lt;minutes, static&gt; (4 hours)<br>
    ftpProxyHost=&lt;host name, static&gt;<br>
    ftpProxyPort=&lt;port number, static&gt;<br>
    gopherProxyCache=true | false (false)<br>
    gopherProxyCacheCleaningInterval=&lt;minutes, static&gt; (one week)<br>
    gopherProxyCacheRefreshingInterval=&lt;minutes, static&gt; (4 hours)<br>
    gopherProxyHost=&lt;host name, static&gt;<br>
    gopherProxyPort=&lt;port number, static&gt;<br>
    httpProxyCache=true | false (false)<br>
    httpProxyCacheCleaningInterval=&lt;minutes, static&gt; (one week)<br>
    httpProxyCacheRefreshingInterval=&lt;minutes, static&gt; (4 hours)<br>
    httpProxyHost=&lt;host name, static&gt;<br>
    httpProxyPort=&lt;port number, static&gt;<br>
    imap4CacheCleaningInterval=&lt;minutes, static&gt; (one day)<br>
    imap4ProxyCache=true | false (false)<br>
    imap4ProxyHost=&lt;host name, static&gt;<br>
    imap4ProxyPort=&lt;port number, static&gt;<br>
    imap4ServerHost=&lt;host name, static&gt;<br>
    keepAliveCount=&lt;number&gt; (10)<br>
    keepAliveTimeoutSecs=&lt;seconds&gt; (30)<br>
    ldapProxyCache=true | false (false)<br>
    locales=&lt;locale name&gt;...<br>
    logFormat=(c-ip time-taken authuser [date time] "request" s-status bytes)<br>
    logLevel=0 - 3 (0)<br>
    logSize=&lt;kbytes&gt; (1024)<br>
    managerCredentials=&lt;string&gt; (manager:reganam)<br>
    maxCacheEntryLength=&lt;kbytes&gt; (128)<br>
    maxConnections=&lt;number&gt; (-1)<br>
    maxInactiveInterval=&lt;seconds&gt; (60)<br>
    maxProxyCacheEntryLength=&lt;kbytes, static&gt; (512)<br>
    maxProxyConnections=&lt;number&gt; (-1)<br>
    maxRequests=&lt;number&gt; (-1)<br>
    mobileServer=true | false (false)<br>
    mobileServerHost=&lt;host name, static&gt;<br>
    mobileServerPort=&lt;port number, static&gt;<br>
    msgCacheCleaningInterval=&lt;minutes, static&gt; (one day)<br>
    msgProxyHost=&lt;host name, static&gt;<br>
    msgProxyPort=&lt;port number, static&gt;<br>
    msgServerHost=&lt;host name, static&gt;<br>
    nativeDomain=&lt;native domain name in NT&gt;<br>
    nntpCacheCleaningInterval=&lt;minutes, static&gt; (one day)<br>
    nntpProxyCache=true | false (false)<br>
    nntpProxyHost=&lt;host name, static&gt;<br>
    nntpProxyPort=&lt;port number, static&gt;<br>
    nntpServerHost=&lt;host name, static&gt;<br>
    noCacheHeaders=true | false (false)<br>
    ownSecurityManager=true | false (false)<br>
    password=&lt;SSL private key password&gt;<br>
    pop3CacheCleaningInterval=&lt;minutes, static&gt; (one day)<br>
    pop3ProxyCache=true | false (false)<br>
    pop3ProxyHost=&lt;host name, static&gt;<br>
    pop3ProxyPort=&lt;port number, static&gt;<br>
    pop3ServerHost=&lt;host name, static&gt;<br>
    port=&lt;port number&gt; (80 or 433 when secure)<br>
    proxyConnectTimeoutSecs=&lt;proxy socket connect timeout in seconds, static&gt; (60)<br>
    proxyKeepAliveCount=&lt;number&gt; (0)<br>
    proxyTimeoutSecs=&lt;proxy socket read timeout in seconds, static&gt; (60)<br>
    requestTimeoutSecs=&lt;seconds&gt; (60)<br>
    secure=true | false (false)<br>
    serverDisabled=true | false (false)<br>
    serverGroup=&lt;server group name or id in unix&gt;<br>
    serverID=&lt;server identifier (maybe pseudonym)&gt;<br>
    serverName=&lt;server host name returned to client&gt;<br>
    serverPort=&lt;server port returned to client&gt;<br>
    serverScheme=&lt;server scheme returned to client&gt;<br>
    serverUser=&lt;server user name or id in unix&gt;<br>
    shutdownCommand=&lt;shutdown command&gt;<br>
    smtpProxyHost=&lt;host name, static&gt;<br>
    smtpProxyPort=&lt;port number, static&gt;<br>
    smtpServerHost=&lt;host name, static&gt;<br>
    sslProxyDisabled=true | false (false)<br>
    sslProxyHost=&lt;host name&gt;<br>
    sslProxyPort=&lt;port number&gt;<br>
    startupCommand=&lt;startup command&gt;<br>
    userDatabase=user database class (FI.realitymodeler.server.UserDatabase)<br>
    verbose=true | false (false)<br>
    virtual=true | false (false)<br>
    waisProxyCache=true | false (false)<br>
    waisProxyHost=&lt;host name, static&gt;<br>
    waisProxyPort=&lt;port number, static&gt;<br>
    </i>
    <p>Log format can be specified with following expressions:
    (<a href="http://www.w3.org/pub/WWW/TR/WD-logfile.html">Extended Log File Format</a>)<br>
    <i>identifier</i><br>
    Relates to the transaction as a whole.<br>
    <i>prefix-identifier</i><br>
    Relates to information transfer between parties defined by the <i>prefix</i>.<br>
    <i>prefix(header)</i><br>
    Identifies the value of the HTTP header field <i>header</i> for transfer
    between parties defined by the <i>prefix</i>.<br>
    The following prefixes are defined:<br>
    c - Client<br>
    s - Server<br>
    r - Remote<br>
    cs - Client to Server<br>
    sc - Server to Client<br>
    sr - Server to Remote Server<br>
    rs - Remote Server to Server<br>
    The following identifiers do not require a prefix:<br>
    date - Date at which transaction completed.<br>
    time - Time at which transaction completed.<br>
    time-taken - Time taken for transaction in seconds.<br>
    bytes - Bytes transferred<br>
    cached - Records whether a cache hit occured.<br>
    authuser - The username as which the user has authenticated himself.<br>
    request - The request line exactly as it came from the client.<br>
    thread - Name of the thread where request is running.<br>
    number - Number of the request in the same connection.<br>
    running-requests - number of running requests.<br>
    waiting-requests - number of waiting requests.<br>
    The following identifiers require a prefix:<br>
    ip - IP address and port<br>
    dns - DNS name<br>
    status - Status code<br>
    comment - Comment returned with status code<br>
    method - Request method<br>
    uri - Request URI<br>
    uri-stem - Stem portion of URI<br>
    uri-query - Query portion of URI<br>

    <p>If proxy cache is set on files are cached to the cache root in
    subdirectories by protocol and host name (with port if it differs from
    default). If file named <span style="white-space: nowrap"><i>&lt;protocol
    name&gt;_cache_paths</i></span> exists in the data directory, only files
    with paths listed there are cached. It is of the following format:<br>

    <i>
    &lt;host/path&gt;<br>
    .<br>
    .<br>
    .<br>
    </i>

    <p>Paths are listed line by line with preceding host name and without
    protocol name.  Subdirectories are included if path ends with *
    (asterisk).  Cleaning interval given in minutes specifies how often
    cache is cleaned.  Cleaning means that files not accessed for cleaning
    interval are deleted.  When cleaning interval is set to -1 cache is
    never cleaned automatically by the server, but must be cleaned through
    Management-program. This is the minimum interval, actual operation is
    triggered only when cache is used from the client side.  Refreshing
    interval given in minutes specifies how often cached files are updated
    from origin server. This defaults to 0 where files are checked every
    time they are accessed and updated if necessary except in gopher cache,
    where cached files are updated only when client sets pragma-header to
    no-cache.

    <P>File named <I>aliases</I> should contain various site names of this
    server. These are all DNS-names, which this server can be referred with
    and addresses not local to server but which are directed to it e.g
    through address converter, or alternative names of server not recognized
    as such through name resolution. This file is common to all server
    instances.

    <p>From data directory is read file named <i>servlets</i> if it exists,
    which contains servlet name and class mapping plus their parameters and
    is of the following format:<br>

    <i>
    &lt;protection directory&gt;/&lt;servlet name&gt; &lt;servlet class&gt;<br>
    &lt;parameter name&gt;=&lt;value&gt;<br>
    .<br>
    .<br>
    .<br>
    </i>

    If servlet class ends with a dollar sign ($) it is considered to be a
    dll-library where servlet is implemented. Dll-library must export three
    functions as follows:<br>

    <i>
    void &lt;library's name&gt;Init(Native *native);<br>
    void &lt;library's name&gt;Service(Context *context);<br>
    void &lt;library's name&gt;Destroy(Native *native);<br>
    </i>

    Source file must include Native.h and dll-library must be linked with
    native.lib which contains support functions resembling methods in
    Servlet-classes. They can be used as a reference when observing the
    functions in header file. Instead of getInputStream and getOutputStream
    -methods there is following functions:<br>

    <i>
    int read(int off, int len);<br>
    void write(int off, int len);<br>
    </i>

    read-function returns number of bytes actually read.  Variable buf in
    Context points to area which should be used with functions above and
    variable len also there contains length of this area.<br>

    <p>Authorization is performed in every request if there is in the data
    directory file named <i>domains</i> which contains domain names and
    request path mapping and which has the following format:<br>

    <i>
    &lt;domain name&gt;[:&lt;authentication servlet&gt;]=&lt;request path&gt;...<br>
    .<br>
    .<br>
    .<br>
    </i>

    <p>If authentication servlet is not specified basic is assumed.
    Subdirectories are separated with a slash (/) as in url-paths.
    Specified domains shall contain all those paths.  Subdirectories are
    included if path ends with asterisk (*).  If request path is absolute it
    restricts use of proxy.  If local path starts with colon (:), it has
    put-access.  Default is that all put-operations are forbidden in local
    machine.

    <p>All directories not found in any domain or found in a special domain
    called <i>public</i> are freely accessible. Access to all directories in
    a special domain called <i>hidden</i> is disabled. Requests paths
    starting with /hidden/ and :/* (writing to all directories) are in hidden
    domain by default. Domain called <i>system</i> is reserved for server's
    internal use. Domain called <i>native</i> contains operating system's own
    user accounts. In NT-environment server property
    <i>nativeDomain</i> must specify domain name for user accounts and
    account where server is running must have advanced user right called
    'act as part of the operating system.'

    <p>From data directory is read file named <i>mime_types</i> if it
    exists, which contains mime type and file extension mapping and is of
    the following format:<br>

    <i>
    &lt;mime type&gt;[&lt;;quality filter servlet name&gt;][ [.]&lt;target mime type&gt;:&lt;filter servlet name&gt;...]=&lt;file extension&gt;...<br>
    .<br>
    .<br>
    .<br>
    </i>

    This file is common to all server instances of this virtual machine.  If
    quality filter servlet is specified it is called if client is asking of
    this type with quality factor less than 1.0.<br>

    If filter servlets are specified they are called if target mime type is
    accepted by client. Dot (.) in front of target mime type starts new
    filter chain.  Filters can be chained also when servlet returns mime
    type which has a filter.  Filter servlets must call request methods
    before writing to output stream.  Type with file extension asterisk (*)
    is considered to be the default.  Dot (.) can be used to indicate empty
    file extension list.<br>

    <p>File named <i>virtual_paths</i> may contain virtual path and physical
    path mapping and is of the following format:<br>

    <i>
    &lt;virtual path&gt; &lt;physical path&gt;<br>
    .<br>
    .<br>
    .<br>
    </i>

    Virtual path can be pattern with an asterisk (*) in the beginning or in
    the end of string. Physical path can be another request path, full file
    path (in Windows, this must be preceded with drive letter and colon (:),
    in Unix this must be preceded with colon to identify it as file path, in
    both cases slash (/) must be used as path separator character), servlet
    path (starting with /servlet) or absolute URL with any protocol. It can
    also contain asterisk (*) which is replaced by the remaining part of
    original path which matched asterisk in the virtual path.<br>

    <p>File named <i>virtual_hosts</i> may contain port numbers of servers
    acting as virtual hosts in this server.<br>

    <p>Comments can be added to parameter files prefixing them with # (hash)
    character.<br>

    Server can be managed remotely with FI.realitymodeler.server.Management -program
    when FI.realitymodeler.server.W3Manager -servlet is initialized with name
    system/W3Manager in <i>servlets</i> file. Program includes for example
    user data base maintenance.

    @see FI.realitymodeler.server.Management
    @see FI.realitymodeler.server.W3Request
    @see javax.servlet.Servlet */
public class W3Server extends SocketServer {
    static final String managerFilename = "manager";
    static final String stopReallyFilename = "stop_really";
    static final String unexp_token = "Unexpected token";
    static final String unexp_end = "Unexpected end";
    static final String logFilenames[] = {"error_log", "event_log", "access_log"};
    static final int ERROR_LOG = 0, EVENT_LOG = 1, ACCESS_LOG = 2;

    static Enumeration emptyEnumeration = new Vector().elements();
    static ResourceBundle messages = null;
    static String serverInfo = "Keppi/2016-6-2";
    static Map<String, CacheFile> filePool = new WeakHashMap<String, CacheFile>();
    static Map<String, Boolean> aliases = null;
    static Map<String, Boolean> authSchemes = null;
    static Map<String, Vector<Step>> filters = null;
    static Map<Integer, W3Server> servers = null;
    static Map<String, Integer> logValues = null;
    static SimpleDateFormat logDateFormat, logTimeFormat;
    static boolean defaultServerSocket = false;
    static boolean shutdownCalled = false;
    static boolean mainVerbose = false;
    static {
        try {
            Support.loadLibrary("FI_realitymodeler_native");
            initialize();
            W3Factory.prepare();
            //W3SecurityManager.set();
            messages = W3URLConnection.getMessages("FI.realitymodeler.server.resources.Messages");
            aliases = new HashMap<String,Boolean>();
            aliases.put("127.0.0.1", Boolean.TRUE);
            aliases.put("localhost", Boolean.TRUE);
            aliases.put("loopback", Boolean.TRUE);
            authSchemes = new HashMap<String,Boolean>();
            authSchemes.put("basic", Boolean.TRUE);
            authSchemes.put("digest", Boolean.TRUE);
            filters = new HashMap<String,Vector<Step>>();
            servers = new HashMap<Integer,W3Server>();
            logDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            logTimeFormat = new SimpleDateFormat("HH:mm:ss");
            TimeZone tz = Support.dateFormat.getTimeZone();
            logDateFormat.setTimeZone(tz);
            logTimeFormat.setTimeZone(tz);
            logValues = new HashMap<String,Integer>();
            logValues.put("date", new Integer(0));
            logValues.put("time", new Integer(1));
            logValues.put("time-taken", new Integer(2));
            logValues.put("bytes", new Integer(3));
            logValues.put("cached", new Integer(4));
            logValues.put("c-ip", new Integer(5));
            logValues.put("c-dns", new Integer(6));
            logValues.put("c-status", new Integer(7));
            logValues.put("c-comment", new Integer(8));
            logValues.put("c-method", new Integer(9));
            logValues.put("c-uri", new Integer(10));
            logValues.put("c-uri-stem", new Integer(11));
            logValues.put("c-uri-query", new Integer(12));
            logValues.put("s-ip", new Integer(13));
            logValues.put("s-dns", new Integer(14));
            logValues.put("s-status", new Integer(7));
            logValues.put("s-comment", new Integer(8));
            logValues.put("s-method", new Integer(9));
            logValues.put("s-uri", new Integer(10));
            logValues.put("s-uri-stem", new Integer(11));
            logValues.put("s-uri-query", new Integer(12));
            logValues.put("r-ip", new Integer(15));
            logValues.put("r-dns", new Integer(16));
            logValues.put("r-status", new Integer(7));
            logValues.put("r-comment", new Integer(8));
            logValues.put("r-method", new Integer(9));
            logValues.put("r-uri", new Integer(10));
            logValues.put("r-uri-stem", new Integer(11));
            logValues.put("r-uri-query", new Integer(12));
            logValues.put("cs-ip", new Integer(5));
            logValues.put("cs-dns", new Integer(6));
            logValues.put("cs-status", new Integer(7));
            logValues.put("cs-comment", new Integer(8));
            logValues.put("cs-method", new Integer(9));
            logValues.put("cs-uri", new Integer(10));
            logValues.put("cs-uri-stem", new Integer(11));
            logValues.put("cs-uri-query", new Integer(12));
            logValues.put("sc-ip", new Integer(13));
            logValues.put("sc-dns", new Integer(14));
            logValues.put("sc-status", new Integer(7));
            logValues.put("sc-comment", new Integer(8));
            logValues.put("sc-method", new Integer(9));
            logValues.put("sc-uri", new Integer(10));
            logValues.put("sc-uri-stem", new Integer(11));
            logValues.put("sc-uri-query", new Integer(12));
            logValues.put("sr-ip", new Integer(13));
            logValues.put("sr-dns", new Integer(14));
            logValues.put("sr-status", new Integer(7));
            logValues.put("sr-comment", new Integer(8));
            logValues.put("sr-method", new Integer(9));
            logValues.put("sr-uri", new Integer(10));
            logValues.put("sr-uri-stem", new Integer(11));
            logValues.put("sr-uri-query", new Integer(12));
            logValues.put("rs-ip", new Integer(15));
            logValues.put("rs-dns", new Integer(16));
            logValues.put("rs-status", new Integer(7));
            logValues.put("rs-comment", new Integer(8));
            logValues.put("rs-method", new Integer(9));
            logValues.put("rs-uri", new Integer(10));
            logValues.put("rs-uri-stem", new Integer(11));
            logValues.put("rs-uri-query", new Integer(12));
            logValues.put("c", new Integer(17));
            logValues.put("s", new Integer(18));
            logValues.put("r", new Integer(19));
            logValues.put("cs", new Integer(17));
            logValues.put("sc", new Integer(18));
            logValues.put("sr", new Integer(20));
            logValues.put("rs", new Integer(19));
            logValues.put("authuser", new Integer(21));
            logValues.put("request", new Integer(22));
            logValues.put("thread", new Integer(23));
            logValues.put("number", new Integer(24));
            logValues.put("running-requests", new Integer(25));
            logValues.put("waiting-requests", new Integer(26));
            logValues.put("queued-requests", new Integer(27));
            logValues.put("filter-threads", new Integer(28));
            logValues.put("sender-threads", new Integer(29));
            logValues.put("responder-threads", new Integer(30));
            logValues.put("cache-threads", new Integer(31));
            logValues.put("other-threads", new Integer(32));
            logValues.put("bytes-received", new Integer(33));
            logValues.put("bytes-sent", new Integer(34));
            logValues.put("proxy-pipelined", new Integer(35));
        } catch (Exception ex) {
            throw new Error(ex.toString());
        }
    }

    static LogFieldAdapter logFields[] = {
        // 0
        new LogFieldAdapter() {
            public String getField(W3Request w3r) {
                return logDateFormat.format(w3r.date);
            }
        },
        // 1
        new LogFieldAdapter() {
            public String getField(W3Request w3r) {
                return logTimeFormat.format(w3r.date);
            }
        },
        // 2
        new LogFieldAdapter() {
            public String getField(W3Request w3r) {
                return String.valueOf(w3r.endTime - w3r.startTime);
            }
        },
        // 3
        new LogFieldAdapter() {
            public String getField(W3Request w3r) {
                return String.valueOf(w3r.byteCountReceived + w3r.byteCountSent);
            }
        },
        // 4
        new LogFieldAdapter() {
            public String getField(W3Request w3r) {
                return w3r.getAttribute(W3Request.class.getName() + "/cached") == Boolean.TRUE ? "Cached" : "-";
            }
        },
        // 5
        new LogFieldAdapter() {
            public String getField(W3Request w3r) {
                return w3r.getRemoteAddr();
            }
        },
        // 6
        new LogFieldAdapter() {
            public String getField(W3Request w3r) {
                return w3r.getRemoteHost();
            }
        },
        // 7
        new LogFieldAdapter() {
            public String getField(W3Request w3r) {
                return String.valueOf(w3r.status);
            }
        },
        // 8
        new LogFieldAdapter() {
            public String getField(W3Request w3r) {
                return "";
            }
        },
        // 9
        new LogFieldAdapter() {
            public String getField(W3Request w3r) {
                return w3r.getMethod();
            }
        },
        // 10
        new LogFieldAdapter() {
            public String getField(W3Request w3r) {
                return w3r.getRequestURI();
            }
        },
        // 11
        new LogFieldAdapter() {
            public String getField(W3Request w3r) {
                return w3r.requestPath;
            }
        },
        // 12
        new LogFieldAdapter() {
            public String getField(W3Request w3r) {
                return w3r.getQueryString();
            }
        },
        // 13
        new LogFieldAdapter() {
            public String getField(W3Request w3r) {
                return w3r.w3server.getHostAddress();
            }
        },
        // 14
        new LogFieldAdapter() {
            public String getField(W3Request w3r) {
                return w3r.w3server.getHostName();
            }
        },
        // 15
        new LogFieldAdapter() {
            public String getField(W3Request w3r) throws UnknownHostException {
                return InetAddress.getByName(w3r.host).getHostAddress();
            }
        },
        // 16
        new LogFieldAdapter() {
            public String getField(W3Request w3r) throws UnknownHostException {
                return InetAddress.getByName(w3r.host).getHostName();
            }
        },
        // 17
        new LogFieldAdapter() {
            public String getField(W3Request w3r, String name) {
                return w3r.getHeader(name);
            }
        },
        // 18
        new LogFieldAdapter() {
            public String getField(W3Request w3r, String name) {
                return w3r.getProperty(name);
            }
        },
        // 19
        new LogFieldAdapter() {
            public String getField(W3Request w3r, String name) {
                W3URLConnection uc = (W3URLConnection)w3r.getAttribute(W3Request.class.getName() + "/uc");
                return uc != null ? uc.getHeaderField(name) : null;
            }
        },
        // 20
        new LogFieldAdapter() {
            public String getField(W3Request w3r, String name) {
                W3URLConnection uc = (W3URLConnection)w3r.getAttribute(W3Request.class.getName() + "/uc");
                return uc != null ? uc.getRequestProperty(name) : null;
            }
        },
        // 21
        new LogFieldAdapter() {
            public String getField(W3Request w3r) {
                return w3r.getRemoteUser();
            }
        },
        // 22
        new LogFieldAdapter() {
            public String getField(W3Request w3r) {
                return w3r.logStore.request;
            }
        },
        // 23
        new LogFieldAdapter() {
            public String getField(W3Request w3r) {
                return w3r.getName();
            }
        },
        // 24
        new LogFieldAdapter() {
            public String getField(W3Request w3r) {
                return String.valueOf(w3r.requestNumber);
            }
        },
        // 25
        new LogFieldAdapter() {
            public String getField(W3Request w3r) {
                return String.valueOf(w3r.w3server.group.activeCount() - w3r.w3server.pool.poolCount());
            }
        },
        // 26
        new LogFieldAdapter() {
            public String getField(W3Request w3r) {
                return String.valueOf(w3r.w3server.pool.poolCount());
            }
        },
        // 27
        new LogFieldAdapter() {
            public String getField(W3Request w3r) {
                return String.valueOf(w3r.requestHeaders.size());
            }
        },
        // 28
        new LogFieldAdapter() {
            public String getField(W3Request w3r) {
                return String.valueOf(w3r.w3server.filterGroup.activeCount());
            }
        },
        // 29
        new LogFieldAdapter() {
            public String getField(W3Request w3r) {
                return String.valueOf(w3r.w3server.senderGroup.activeCount());
            }
        },
        // 30
        new LogFieldAdapter() {
            public String getField(W3Request w3r) {
                return String.valueOf(w3r.w3server.responderGroup.activeCount());
            }
        },
        // 31
        new LogFieldAdapter() {
            public String getField(W3Request w3r) {
                return String.valueOf(W3URLConnection.cacheGroup.activeCount());
            }
        },
        // 32
        new LogFieldAdapter() {
            public String getField(W3Request w3r) {
                return String.valueOf(w3r.w3server.activeCount());
            }
        },
        // 33
        new LogFieldAdapter() {
            public String getField(W3Request w3r) {
                return String.valueOf(w3r.byteCountReceived);
            }
        },
        // 34
        new LogFieldAdapter() {
            public String getField(W3Request w3r) {
                return String.valueOf(w3r.byteCountSent);
            }
        },
        // 35
        new LogFieldAdapter() {
            public String getField(W3Request w3r) {
                return w3r.logStore.uc != null ? "Proxy-pipelined" : "-";
            }
        }
    };

    public URL context = null;
    public URL defaultContext = null;
    public InetAddress bindAddress = null;
    public InetAddress localAddress = null;
    public String contentEncoding = null;
    public String dataDirectory = ".";
    public String characterEncoding = null;
    public String jspCompiler = null;
    public String keepAlive = null;
    public String localHostAddress = null;
    public String logFormat = "c-ip time-taken authuser [date time] \"request\" s-status bytes";
    public String managerCredentials = "manager:reganam";
    public String nativeDomain = "\\\\localhost";
    public String serverGroup = null;
    public String serverID = null;
    public String serverName = null;
    public String serverScheme = null;
    public String serverUser = null;
    public String sessionCookieName = null;
    public String shutdownCommand = null;
    public String sslProxyHost = null;
    public String startupCommand = null;
    public String userDatabaseName = null;
    public SocketChannelServer socketChannelServer = null;
    public ThreadGroup group = null;
    public ThreadGroup filterGroup = null;
    public ThreadGroup senderGroup = null;
    public ThreadGroup responderGroup = null;
    public RegexpPool domains = null;
    public Map<String, Object> attributes = new HashMap<String, Object>();
    public Map<String, Client> clients = null;
    public Map<String, Locale> localeMap = null;
    public Map<String, Object> proxyCaches = new HashMap<String, Object>();
    public Map<String, Domain> domainEntries = null;
    public Map servletContexts = new HashMap();
    public Map<String, String> servletPathTable = new HashMap<String, String>();
    public Map sessions = new HashMap();
    public Map<String, W3Server> virtualHosts = null;
    public TreeMap<String, String> virtualPathMap = new TreeMap<String, String>(Support.stringComparator);
    public RegexpPool virtualPaths = new RegexpPool();
    public W3Servlet core = null;
    public W3Servlet proxy = null;
    public W3Servlet direct = null;
    public W3Servlet auth = null;
    public W3Servlet basic = null;
    public W3Servlet digest = null;
    public W3Servlet mobile = null;
    public W3Servlet account = null;
    public ServletContext defaultServletContext = null;
    public UserDatabase userDatabase = null;
    public Properties properties = null;
    public W3Context w3context = null;
    public Log logs[] = new Log[4];
    public Set<String> currentRequests = new HashSet<String>();
    public Vector<Object> logLine;
    public int backlog = 50;
    public int completionBufferSize = 16 * 1024;
    public int defaultPort = -1;
    public int errorLogTimes = 0;
    public int eventLogTimes = 0;
    public int keepAliveCount = 10;
    public int keepAliveTimeoutSecs = 30;
    public int keepAliveTimeout = 30 * 1000;
    public int logLevel = 0;
    public int logTimes = 0;
    public int maxCacheEntryLength = 128 * 1024;
    public int maxConnections = -1;
    public int maxInactiveInterval = 60 * 1000;
    public int maxProxyConnections = -1;
    public int maxRequests = -1;
    public int port = -1;
    public int serverPort = -1;
    public int sslProxyPort = 80;
    public int sslVerify = SecureSocketsLayer.VERIFY_NONE;
    public long logSize = 1024L * 1024L;
    public long sessionCleaningTime = 0L;
    public boolean alternative = false;
    public boolean autoLogStats = false;
    public boolean debug = false;
    public boolean isService = false;
    public boolean makeLogStats = false;
    public boolean mobileServer = false;
    public boolean ownSecurityManager = false;
    public boolean secure = false;
    public boolean serverDisabled = false;
    public boolean socketChannels = false;
    public boolean sslProxyDisabled = false;
    public boolean type = false;
    public boolean useSslProxy = false;
    public boolean verbose = false;
    public boolean virtual = false;
    public byte password[] = null;

    private static native void initialize();

    static native void waitStop();
    static native boolean isRoot();
    static native void setUser(String user);
    static native void setGroup(String group);
    static native String getAccountName(String username, String domain);
    static native String checkPassword(String username, String domain, String password);

    public class LogColumn {
        int number;
        String string;

        public LogColumn(int number, String string) {
            this.number = number;
            this.string = string;
        }

    }

    public class Log {
        int times;
        File file;
        OutputStream stream;
    }

    public class LogStats extends Thread {
        File file;
        String timeTag;

        public LogStats(File file, String timeTag) {
            this.file = file;
            this.timeTag = timeTag;
        }

        public void run() {
            try {
                simpleStats(W3Server.this, file.getPath(), dataDirectory + "/log_stats_" + timeTag + ".html");
                log("Simple log statistics generated");
            } catch (Exception ex) {
                if (logLevel > 0) log(ex);
            } finally {
                file.delete();
            }
        }

    }

    public static LdapContext open(String name, LdapContext ldapContext[]) throws NamingException {
        if (ldapContext[0] == null) ldapContext[0] = new InitialLdapContext((Hashtable)System.getProperties(), null);
        try {
            return (LdapContext)ldapContext[0].lookup("ou=" + name);
        } catch (NamingException ex) {}
        Attributes attributes = new BasicAttributes();
        Attribute objectClass = new BasicAttribute("objectClass");
        objectClass.add("top");
        objectClass.add("organizationalUnit");
        attributes.put(objectClass);
        attributes.put("ou", name);
        return (LdapContext)ldapContext[0].createSubcontext("ou=" + name, attributes);
    }

    public static void close(LdapContext dirContext, LdapContext ldapContext[]) {
        try {
            if (dirContext != null) dirContext.close();
        } catch (NamingException ex) {} finally {
            try {
                if (ldapContext != null && ldapContext[0] != null) {
                    ldapContext[0].close();
                    ldapContext[0] = null;
                }
            } catch (NamingException ex) {}
        }
    }

    public static String makeName(LdapContext context, String attrId) throws NamingException {
        String baseName  = attrId + "=" + Long.toString(System.currentTimeMillis(), Character.MAX_RADIX), name = baseName;
        try {
            for (int number = 0;; number++) {
                context.lookup(name);
                name = baseName + "-" + Integer.toString(number, Character.MAX_RADIX);
            }
        } catch (NameNotFoundException ex) {}
        return name;
    }

    public static void readSystemProperties(File file) throws IOException {
        if (!file.exists()) return;
        Properties systemProperties = new Properties();
        InputStream in = new BufferedInputStream(new FileInputStream(file));
        try {
            systemProperties.load(in);
        } finally {
            in.close();
        }
        Iterator iter = systemProperties.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry entry = (Map.Entry)iter.next();
            System.setProperty((String)entry.getKey(), (String)entry.getValue());
        }
    }

    /** Changes name of given file to given path name preserving one backup file. */
    public static void changeNames(File file, String path) {
        File file0 = new File(path + ".old"), file1 = new File(path);
        file0.delete();
        if (file1.exists()) file1.renameTo(file0);
        file.renameTo(file1);
    }

    /** Removes slashes from end of path. */
    public static String checkPath(String path) {
        path = path.trim();
        while (path.length() > 1 && path.endsWith("/")) path = path.substring(0, path.length() - 1);
        return path;
    }

    /** Parses log file with given name of given server instance.
        @param w3 server instance
        @param filename name of log file */
    public static void parseLog(W3Server w3, String filename, StatMap stats[]) throws IOException {
        StreamTokenizer sTok = null;
        LogMap statsMap[] = new LogMap[] {new LogMap(), new LogMap()};
        BufferedReader bf = new BufferedReader(new FileReader(filename));
        try {
            sTok = new StreamTokenizer(bf);
            sTok.resetSyntax();
            sTok.eolIsSignificant(true);
            sTok.whitespaceChars(0, 32);
            sTok.wordChars(33, 255);
            sTok.ordinaryChar('[');
            sTok.ordinaryChar(']');
            sTok.commentChar('#');
            sTok.quoteChar('"');
            String date = null, time = null, key[] = new String[2];
            LogRecord record[] = new LogRecord[] {new LogRecord(), new LogRecord()}, record1[] = new LogRecord[2];
            boolean totalBytesCounted = false;
            int n = 0;
            long totalTimeTakenMs = 0L, totalBytesTransferred = 0L;
            for (;;)
                try {
                    if (n >= w3.logLine.size()) {
                        long ms = -1L;
                        if (date != null) {
                            Date dt = logDateFormat.parse(date);
                            if (dt != null) {
                                ms = dt.getTime();
                                if (time != null && (dt = logTimeFormat.parse(time)) != null) ms += dt.getTime();
                            }
                        }
                        for (int i = 0; i < 2; i++) {
                            if (key[i] == null) break;
                            if (record1[i] == null) {
                                record1[i] = new LogRecord();
                                StringBuffer sb = new StringBuffer();
                                int j = record[i].hostName.length();
                                for (int k = j; k > 0; j = k) {
                                    if (sb.length() > 0) sb.append('.');
                                    k = record[i].hostName.lastIndexOf('.', k - 1);
                                    sb.append(record[i].hostName.substring(k + 1, j));
                                }
                                record1[i].orderKey = sb.toString();
                                record1[i].hostAddr = record[i].hostAddr;
                                record1[i].hostName = record[i].hostName;
                                statsMap[i].put(key[i], record1[i]);
                            }
                            record1[i].totalRequests++;
                            record1[i].totalTimeTakenMs += totalTimeTakenMs;
                            record1[i].totalBytesTransferred += totalBytesTransferred;
                            if (ms != -1L) {
                                if (record1[i].firstTime == 0L) record1[i].firstTime = ms;
                                record1[i].lastTime = ms;
                            }
                        }
                        date = time = null;
                        key[0] = key[1] = null;
                        record1[0] = record1[1] = null;
                        totalTimeTakenMs = totalBytesTransferred = 0L;
                        totalBytesCounted = false;
                        n = 0;
                    }
                    if (sTok.nextToken() == sTok.TT_EOF) break;
                    if (sTok.ttype == sTok.TT_EOL) {
                        date = time = null;
                        key[0] = key[1] = null;
                        record1[0] = record1[1] = null;
                        totalTimeTakenMs = totalBytesTransferred = 0L;
                        totalBytesCounted = false;
                        n = 0;
                        continue;
                    }
                    if (sTok.ttype != sTok.TT_WORD && sTok.ttype != '"') continue;
                    Object obj = w3.logLine.elementAt(n);
                    n += 2;
                    if (obj instanceof LogColumn)
                        switch (((LogColumn)obj).number) {
                        case 0:
                            date = sTok.sval;
                            break;
                        case 1:
                            time = sTok.sval;
                            break;
                        case 2:
                            totalTimeTakenMs = Long.parseLong(sTok.sval);
                            break;
                        case 3:
                            totalBytesTransferred = Long.parseLong(sTok.sval);
                            totalBytesCounted = true;
                            break;
                        case 32:
                        case 33:
                            if (totalBytesCounted) continue;
                            totalBytesTransferred += Long.parseLong(sTok.sval);
                            break;
                        case 22: {
                            int i = sTok.sval.indexOf(' ');
                            if (i != -1) {
                                int j = sTok.sval.lastIndexOf(' ');
                                String requestURI = sTok.sval.substring(i + 1, j).trim();
                                if (requestURI.equals("") || !sTok.sval.substring(j + 1).trim().startsWith("HTTP/")) requestURI = sTok.sval.substring(i + 1).trim();
                                URL url = new URL(w3.defaultContext, requestURI);
                                if (!(url.getProtocol().equals(w3.defaultContext.getProtocol()) && w3.isLocal(url.getHost()) && (url.getPort() == w3.port || url.getPort() == -1 && w3.port == w3.defaultPort))) {
                                    if ((record1[1] = statsMap[1].get(key[1] = record[1].hostName = url.getHost().toLowerCase())) != null) break;
                                    try {
                                        record[1].hostAddr = key[1].startsWith("+") ? key[1] : (key[1] = InetAddress.getByName(key[1]).getHostAddress());
                                    } catch (UnknownHostException ex) {
                                        record[1].hostAddr = key[1];
                                    }
                                }
                            }
                            break;
                        }
                        case 5:
                        case 15: {
                            int i;
                            if (((LogColumn)obj).number == 5) i = 0;
                            else if (!sTok.sval.equals("")) i = 1;
                            else continue;
                            if ((record1[i] = statsMap[i].get(key[i] = record[i].hostAddr = sTok.sval)) != null) break;
                            try {
                                record[i].hostName = key[i].startsWith("+") ? key[i] : InetAddress.getByName(key[i]).getHostName().toLowerCase();
                            } catch (UnknownHostException ex) {
                                record[i].hostName = key[i];
                            }
                            break;
                        }
                        case 6:
                        case 16: {
                            int i;
                            if (((LogColumn)obj).number == 6) i = 0;
                            else if (!sTok.sval.equals("")) i = 1;
                            else continue;
                            if ((record1[i] = statsMap[i].get(key[i] = record[i].hostName = sTok.sval.toLowerCase())) != null) break;
                            try {
                                record[i].hostAddr = key[i].startsWith("+") ? key[i] : (key[i] = InetAddress.getByName(key[i]).getHostAddress());
                            } catch (UnknownHostException ex) {
                                record[i].hostAddr = key[i];
                            }
                            break;
                        }
                        }
                } catch (Exception ex) {
                    if (w3.logLevel > 0) w3.log("Failed to parse log file " + filename + " in line " +
                                                sTok.lineno() + "\n" + Support.stackTrace(ex));
                    while (sTok.ttype != sTok.TT_EOF && sTok.ttype != sTok.TT_EOL) sTok.nextToken();
                    if (sTok.ttype == sTok.TT_EOF) break;
                    date = time = null;
                    key[0] = key[1] = null;
                    record1[0] = record1[1] = null;
                    totalTimeTakenMs = totalBytesTransferred = 0L;
                    totalBytesCounted = false;
                    n = 0;
                }
            for (int i = 0; i < 2; i++) {
                Iterator statIter = statsMap[i].entrySet().iterator();
                while (statIter.hasNext()) {
                    LogRecord aRecord = (LogRecord)((Map.Entry)statIter.next()).getValue();
                    stats[i].put(aRecord.orderKey, aRecord);
                }
            }
        } finally {
            bf.close();
        }
    }

    /** Generates simple statistics in HTML-format for given server instance to given outputstream.
        @param w3 server instance
        @param filename name of log file
        @param logStatsFilename filename to write log statistics. */
    public static void simpleStats(W3Server w3, String filename, String logStatsFilename) throws IOException {
        StatMap stats[] = new StatMap[] {new StatMap(), new StatMap()};
        parseLog(w3, filename, stats);
        PrintWriter pw = new PrintWriter(new FileWriter(logStatsFilename));
        try {
            pw.println("<html><head><title>Simple Log Statistics</title></head><body>\n" +
                       "<h1>Simple Log Statistics" + (w3.serverName != null ? " for " + w3.serverName : "") +
                       "</h1><h2>generated at " + Support.format(new Date()) + "</h2>");
            for (int i = 0; i < 2; i++) {
                pw.println((i == 1 ? "<h3>Requests out</h3>" : "<h3>Requests in</h3>\n") +
                           "<table>\n<tr><th>Host name</th><th>Host address</th><th>Total requests</th><th>Total time taken secs</th><th>Total bytes transferred</th><th>First time</th><th>Last time</th></tr>");
                Iterator iter = stats[i].values().iterator();
                while (iter.hasNext()) {
                    LogRecord record = (LogRecord)iter.next();
                    pw.println("<tr><td>" + record.hostName + "</td><td>" + record.hostAddr + "</td><td>" +
                               record.totalRequests + "</td><td>" + (record.totalTimeTakenMs / 1000L) + "</td><td>" + record.totalBytesTransferred + "</td><td>" +
                               Support.format(new Date(record.firstTime)) + "</td><td>" + Support.format(new Date(record.lastTime)) + "</td></tr>");
                }
                pw.println("</table>");
            }
            pw.println("</body></html>");
        } finally {
            pw.close();
        }
    }

    public boolean isLocal(String host) {
        return host.equals(serverName) || host.equals(localHostAddress) || aliases.containsKey(host);
    }

    public boolean isLocalClient(InetAddress address) {
        return address.equals(localAddress) || address.isLoopbackAddress();
    }

    public void handle(Throwable throwable) {
        if (logLevel > 0) log(throwable);
    }

    public void log(String msg) {
        if (logLevel > 1)
            synchronized (logs[EVENT_LOG].stream) {
                Date date = new Date();
                try {
                    if (!verbose) checkLog(EVENT_LOG, false);
                    Support.writeBytes(logs[EVENT_LOG].stream,
                                       msg + " in " + Thread.currentThread().getName() +
                                       " at " + logDateFormat.format(date) + " " + logTimeFormat.format(date) + "\n", null);
                } catch (IOException ex) {
                    logLevel = 0;
                }
            }
    }

    /** @deprecated */
    public void log(Exception exception, String msg) {
        log(msg, exception);
    }

    public void log(String message, Throwable throwable) {
        if (logLevel > 1)
            synchronized (logs[EVENT_LOG].stream) {
                Date date = new Date();
                try {
                    if (!verbose) checkLog(EVENT_LOG, false);
                    Support.writeBytes(logs[EVENT_LOG].stream,
                                       message + " in " + Thread.currentThread().getName() +
                                       " at " + logDateFormat.format(date) + " " + logTimeFormat.format(date) + "\n" +
                                       Support.stackTrace(throwable) +
                                       (throwable instanceof ServletException &&
                                        ((ServletException)throwable).getRootCause() != null ? "\n" +
                                        Support.stackTrace(((ServletException)throwable).getRootCause()) : ""), null);
                } catch (IOException ex) {
                    logLevel = 0;
                }
            }
    }

    public void startLog() throws IOException {
        Date now = new Date();
        Support.writeBytes(logs[ACCESS_LOG].stream, "\n#Version: 1.0\n#Fields: " + logFormat +
                           "\n#Start-Date: " + logDateFormat.format(now) + " " + logTimeFormat.format(now) + "\n", null);
    }

    public boolean checkLog(int logIndex,
                            boolean makeLogStats)
        throws IOException {
        Log log = logs[logIndex];
        if (!makeLogStats && ++log.times < 10) return false;
        log.times = 0;
        if (!makeLogStats && log.file.length() < logSize) return false;
        log.stream.close();
        if (logIndex == ACCESS_LOG && (makeLogStats || autoLogStats)) {
            String timeTag = Long.toString(System.currentTimeMillis(), Character.MAX_RADIX);
            File file = new File(log.file.getPath() + "_" + timeTag);
            if (log.file.renameTo(file)) new LogStats(file, timeTag).start();
        }
        log.stream = new BufferedOutputStream(new FileOutputStream(log.file));
        if (logIndex == ACCESS_LOG) startLog();
        return true;
    }

    public void log(Throwable throwable) {
        if (logLevel > 0 && (!(throwable instanceof SocketException) || verbose))
            synchronized (logs[ERROR_LOG].stream) {
                Date date = new Date();
                try {
                    if (!verbose) checkLog(ERROR_LOG, false);
                    Support.writeBytes(logs[ERROR_LOG].stream,
                                       "Exception in " + Thread.currentThread().getName() +
                                       " at " + logDateFormat.format(date) + " " + logTimeFormat.format(date) + "\n" +
                                       Support.stackTrace(throwable) +
                                       (throwable instanceof ServletException && ((ServletException)throwable).getRootCause() != null ?
                                        Support.stackTrace(((ServletException)throwable).getRootCause()) : ""), null);
                } catch (IOException ex) {
                    logLevel = 0;
                }
            }
    }

    /** Makes log entry of given request to access log file.
        @param w3r request to be logged */
    public void log(W3Request w3r) {
        if (logLevel > 2)
            synchronized (logs[ACCESS_LOG].stream) {
                try {
                    if (!verbose) checkLog(ACCESS_LOG, false);
                    int l = logLine.size();
                    for (int i = 0; i < l; i++) {
                        Object obj = logLine.elementAt(i);
                        String field;
                        if (obj instanceof LogColumn) {
                            LogColumn column = (LogColumn)obj;
                            field = column.string != null ? logFields[column.number].getField(w3r, column.string) : logFields[column.number].getField(w3r);
                        } else field = (String)obj;
                        Support.writeBytes(logs[ACCESS_LOG].stream, field != null ? field : "-", null);
                    }
                    logs[ACCESS_LOG].stream.write('\n');
                } catch (IOException ex) {
                    logLevel = 0;
                }
            }
    }

    public void flushLogs() {
        for (int i = 0; i < logLevel; i++)
            if (logs[i] != null)
                try {
                    logs[i].stream.flush();
                } catch (IOException ex) {
                    logLevel = 0;
                }
    }

    public W3Servlet loadServlet(String servletPath, String className, Hashtable<String,String> initParameters, boolean isNative, boolean isDisabled) throws ClassNotFoundException, IllegalAccessException, InstantiationException, MalformedURLException, RegexException, ServletException {
        String servletName = servletPath.substring(servletPath.lastIndexOf('/') + 1);
        servletPath = "/" + servletPath + "/*";
        W3Servlet w3servlet = w3context.servlets.get(servletName);
        if (w3servlet != null) return w3servlet;
        w3servlet = new W3Servlet(w3context, servletName, className, initParameters, isNative, isDisabled, false);
        w3context.setServlet(w3servlet);
        w3context.servletPatternMap.put(servletPath, w3servlet);
        return w3servlet;
    }

    /** Reads parameters files from data directory. */
    void readParameters() throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException, ParseException, ParserConfigurationException, SAXException, RegexException, ServletException {
        if (dataDirectory == null) return;
        StreamTokenizer sTok = null;
        File f = null;
        try {
            f = new File(dataDirectory + "/aliases");
            if (f.exists()) {
                BufferedReader bf = new BufferedReader(new FileReader(f));
                try {
                    sTok = new StreamTokenizer(bf);
                    sTok.resetSyntax();
                    sTok.whitespaceChars(0, 32);
                    sTok.wordChars(33, 255);
                    sTok.commentChar('#');
                    sTok.quoteChar('"');
                    while (sTok.nextToken() != sTok.TT_EOF)
                        aliases.put(sTok.sval, Boolean.TRUE);
                } finally {
                    bf.close();
                }
            }
            core = loadServlet("frame/core", FI.realitymodeler.server.CoreServlet.class.getName(), null, false, false);
            proxy = loadServlet("frame/proxy", FI.realitymodeler.server.ProxyServlet.class.getName(), null, false, false);
            direct = loadServlet("hidden/direct", FI.realitymodeler.server.DirectServlet.class.getName(), null, false, false);
            auth = loadServlet("public/auth", FI.realitymodeler.server.AuthServlet.class.getName(), null, false, false);
            basic = loadServlet("hidden/basic", FI.realitymodeler.server.BasicServlet.class.getName(), null, false, false);
            digest = loadServlet("hidden/digest", FI.realitymodeler.server.DigestServlet.class.getName(), null, false, false);
            mobile = loadServlet("hidden/mobile", FI.realitymodeler.server.MobileServlet.class.getName(), null, false, false);
            f = new File(dataDirectory, "servlets");
            if (f.exists()) {
                BufferedReader bf = new BufferedReader(new FileReader(f));
                try {
                    sTok = new StreamTokenizer(bf);
                    sTok.resetSyntax();
                    sTok.whitespaceChars(0, 32);
                    sTok.wordChars(33, 255);
                    sTok.ordinaryChar('=');
                    sTok.commentChar('#');
                    sTok.quoteChar('"');
                    Hashtable<String,String> initParameters = null;
                    String path = null, className = null;
                    boolean isDisabled = false, isNative = false;
                    while (sTok.nextToken() != sTok.TT_EOF) {
                        if (sTok.ttype != sTok.TT_WORD && sTok.ttype != '"') throw new ParseException(unexp_token, 0);
                        String s = sTok.sval;
                        if (initParameters != null && initParameters.isEmpty() && s.startsWith(":")) {
                            s = s.substring(1);
                            if (s.equals("disabled")) isDisabled = true;
                            else if (s.equals("native")) isNative = true;
                            else throw new ParseException(unexp_token, 0);
                            continue;
                        }
                        if (sTok.nextToken() == '=') {
                            sTok.nextToken();
                            if (sTok.ttype != sTok.TT_WORD && sTok.ttype != '"' || initParameters == null) throw new ParseException(unexp_token, 0);
                            initParameters.put(s, sTok.sval);
                            continue;
                        }
                        if (sTok.ttype == sTok.TT_EOF) break;
                        if (path != null) loadServlet(path, className, initParameters, isNative, isDisabled);
                        path = s;
                        className = sTok.sval;
                        initParameters = new Hashtable<String,String>();
                        isDisabled = isNative = false;
                    }
                    if (path != null) loadServlet(path, className, initParameters, isNative, isDisabled);
                } finally {
                    bf.close();
                }
            }
            Domain domain = null;
            Vector<String> paths = null;
            boolean hasDomains = false;
            domains = new RegexpPool();
            domainEntries = new HashMap<String,Domain>();
            f = new File(dataDirectory, "domains");
            if (f.exists()) {
                BufferedReader bf = new BufferedReader(new FileReader(f));
                try {
                    sTok = new StreamTokenizer(bf);
                    sTok.resetSyntax();
                    sTok.whitespaceChars(0, 32);
                    sTok.wordChars(33, 255);
                    sTok.ordinaryChar('=');
                    sTok.commentChar('#');
                    sTok.quoteChar('"');
                    domain = null;
                    while (sTok.nextToken() != sTok.TT_EOF) {
                        if (sTok.ttype != sTok.TT_WORD && sTok.ttype != '"') throw new ParseException(unexp_token, 0);
                        String s = sTok.sval;
                        if (sTok.nextToken() == '=') {
                            paths = new Vector<String>();
                            StringTokenizer st = new StringTokenizer(s, ":;", true);
                            String domainName = st.nextToken().trim(), secondName = null;
                            W3Servlet domainAuth = basic;
                            if (st.hasMoreTokens()) {
                                if ((s = st.nextToken()).equals(":")) {
                                    if ((domainAuth = w3context.servlets.get(st.nextToken().trim())) == null)
                                        throw new RuntimeException("Domain authentication servlet " + s + " not found");
                                    if (st.hasMoreTokens()) st.nextToken();
                                }
                                if (st.hasMoreTokens()) secondName = st.nextToken().trim();
                            }
                            domain = new Domain(domainName, paths, domainAuth, secondName);
                            domainEntries.put(domainName, domain);
                            continue;
                        } else sTok.pushBack();
                        if (paths == null) throw new ParseException(unexp_token, 0);
                        domains.add(s, domain);
                        paths.addElement(s);
                        hasDomains = true;
                    }
                } finally {
                    bf.close();
                }
            }
            W3Servlet manager = w3context.servlets.get("W3Manager");
            if (manager != null && manager.isDisabled) {
                manager = null;
            }
            if (userDatabase == null && (hasDomains || manager != null)) {
                userDatabase = userDatabase != null ? (UserDatabase)Class.forName(userDatabaseName).newInstance() : new UserDatabase();
                userDatabase.open(dataDirectory + "/users", this);
                StringTokenizer st = new StringTokenizer(managerCredentials, ":");
                String username = st.nextToken(), password = st.hasMoreTokens() ? st.nextToken() : "";
                if (userDatabase.getRecord(username) == null) userDatabase.saveUser(new Object[] {username, password, "system", "", "", "", "", "", "", "", new Integer(0), new Integer(0)});
            }
            if ((domain = domainEntries.get("hidden")) == null) domain = new Domain("hidden", null, null, null);
            try {
                domains.add("/hidden/*", domain);
            } catch (RegexException ex) {}
            account = w3context.servlets.get("account");
            w3context.servletPatternMap.put("/servlet/*", direct);
            Iterator proxyCacheSchemes = proxyCaches.keySet().iterator();
            while (proxyCacheSchemes.hasNext()) {
                String scheme = (String)proxyCacheSchemes.next();
                w3context.servletPatternMap.put("/" + scheme + "://*", proxy);
            }
            if (manager != null) domains.add(":/system/*", new Domain("system", null, digest, null));
            f = new File(dataDirectory, "mime_types");
            if (f.exists()) {
                String defaultTypeValue[] = new String[1];
                filters = new HashMap<String,Vector<Step>>();
                BufferedReader bf = new BufferedReader(new FileReader(f));
                try {
                    URLConnection.setFileNameMap(MimeMap.readMimeMap(sTok = new StreamTokenizer(bf), w3context.mimeTypeMap, w3context.mimeTypes, filters, w3context.servlets, defaultTypeValue));
                } finally {
                    bf.close();
                }
                W3URLConnection.defaultMimeType = defaultTypeValue[0];
            } else URLConnection.setFileNameMap(new FileNameMap() {
                    public String getContentTypeFor(String fileName) {
                        return null;
                    }
                });
            f = new File(dataDirectory, "virtual_paths");
            if (f.exists()) {
                virtualPaths = new RegexpPool();
                virtualPathMap = new TreeMap<String,String>(Support.stringComparator);
                BufferedReader bf = new BufferedReader(new FileReader(f));
                try {
                    sTok = new StreamTokenizer(bf);
                    sTok.resetSyntax();
                    sTok.whitespaceChars(0, 32);
                    sTok.wordChars(33, 255);
                    sTok.commentChar('#');
                    sTok.quoteChar('"');
                    while (sTok.nextToken() != sTok.TT_EOF) {
                        String s = sTok.sval;
                        if (sTok.nextToken() == sTok.TT_EOF) break;
                        virtualPaths.add(s, new VirtualPathTarget(s, sTok.sval));
                        virtualPathMap.put(s, sTok.sval);
                    }
                } finally {
                    bf.close();
                }
            }
            f = new File(dataDirectory, "virtual_hosts");
            if (f.exists()) {
                virtualHosts = new HashMap<String,W3Server>();
                BufferedReader bf = new BufferedReader(new FileReader(f));
                try {
                    sTok = new StreamTokenizer(bf);
                    sTok.resetSyntax();
                    sTok.whitespaceChars(0, 32);
                    sTok.wordChars(33, 255);
                    sTok.commentChar('#');
                    sTok.parseNumbers();
                    while (sTok.nextToken() != sTok.TT_EOF) {
                        W3Server w3s = servers.get(new Integer((int)sTok.nval));
                        if (w3s == null) throw new RuntimeException("No server at port " + (int)sTok.nval + " found");
                        virtualHosts.put(w3s.serverName, w3s);
                    }
                } finally {
                    bf.close();
                }
            }
        } catch (Exception ex) {
            if (f != null && sTok != null) throw new ParseException("Error in file " + f.getPath() + " in line " +
                                                                    sTok.lineno() + "\n" + Support.stackTrace(ex), sTok.lineno());
            throw new ParseException(Support.stackTrace(ex), 0);
        }
    }

    void readProperties() throws IOException, ParseException, UnknownHostException, MalformedURLException {
        while ((dataDirectory = dataDirectory.trim()).endsWith("/")) dataDirectory = dataDirectory.substring(0, dataDirectory.length() - 1);
        readSystemProperties(new File(dataDirectory, "system.properties"));
        File file = new File(dataDirectory, "server.properties");
        if (!file.exists()) return;
        properties = new Properties();
        InputStream in = new BufferedInputStream(new FileInputStream(file));
        try {
            properties.load(in);
        } finally {
            in.close();
        }
        Enumeration propertyNames = properties.propertyNames();
        while (propertyNames.hasMoreElements()) {
            String name = ((String)propertyNames.nextElement()).trim(), value = ((String)properties.get(name)).trim();
            if (!ProxyServlet.setProxyParameter(name, value, w3context, proxyCaches))
                if (name.equals("autoLogStats")) autoLogStats = Support.isTrue(value, name);
                else if (name.equals("backlog")) backlog = Integer.parseInt(value);
                else if (name.equals("bindAddress")) bindAddress = InetAddress.getByName(value);
                else if (name.equals("characterEncoding")) characterEncoding = value;
                else if (name.equals("completionBufferSize")) completionBufferSize = Integer.parseInt(value);
                else if (name.equals("contentEncoding")) contentEncoding = value.toLowerCase();
                else if (name.equals("defaultServerSocket")) W3Server.defaultServerSocket = Support.isTrue(value, name);
                else if (name.equals("jspCompiler")) jspCompiler = value;
                else if (name.equals("keepAliveCount")) keepAliveCount = Integer.parseInt(value);
                else if (name.equals("keepAliveTimeoutSecs")) keepAliveTimeout = (keepAliveTimeoutSecs = Integer.parseInt(value)) * 1000;
                else if (name.equals("locales")) {
                    localeMap = new HashMap<String,Locale>();
                    Vector locales = W3URLConnection.getLocales(value);
                    Iterator iter = locales.iterator();
                    while (iter.hasNext()) {
                        Locale locale = (Locale)iter.next();
                        localeMap.put(locale.getLanguage() + (locale.getCountry().equals("") ? "" : "-" + locale.getCountry()), locale);
                    }
                } else if (name.equals("logFormat")) logFormat = value;
                else if (name.equals("logLevel")) logLevel = Integer.parseInt(value);
                else if (name.equals("logSize")) logSize = Long.parseLong(value) * 1024L;
                else if (name.equals("managerCredentials")) managerCredentials = value;
                else if (name.equals("maxCacheEntryLength")) {
                    int intValue = Integer.parseInt(value);
                    maxCacheEntryLength = intValue >= 0 ? intValue * 1024 : -1;
                } else if (name.equals("maxConnections")) maxConnections = Integer.parseInt(value);
                else if (name.equals("maxInactiveInterval")) maxInactiveInterval = Integer.parseInt(value) * 1000;
                else if (name.equals("maxRequests")) maxRequests = Integer.parseInt(value);
                else if (name.equals("mobileServer")) mobileServer = Support.isTrue(value, name);
                else if (name.equals("nativeDomain")) nativeDomain = value;
                else if (name.equals("ownSecurityManager")) ownSecurityManager = Support.isTrue(value, name);
                else if (name.equals("password")) password = value.getBytes("ISO-8859-1");
                else if (name.equals("port")) port = Integer.parseInt(value);
                else if (name.equals("requestTimeoutSecs")) timeout = Long.parseLong(value) * 1000L;
                else if (name.equals("secure")) secure = Support.isTrue(value, name);
                else if (name.equals("serverDisabled")) serverDisabled = Support.isTrue(value, name);
                else if (name.equals("serverGroup")) serverGroup = value;
                else if (name.equals("serverID")) attributes.put(W3Context.class.getName() + "/serverID", serverID = value);
                else if (name.equals("serverName")) attributes.put(W3Context.class.getName() + "/serverName", serverName = value.toLowerCase());
                else if (name.equals("serverPort")) serverPort = Integer.parseInt(value);
                else if (name.equals("serverScheme")) serverScheme = value;
                else if (name.equals("serverUser")) serverUser = value;
                else if (name.equals("shutdownCommand")) shutdownCommand = value;
                else if (name.equals("socketChannels")) socketChannels = Support.isTrue(value, name);
                else if (name.equals("sslProxyDisabled")) sslProxyDisabled = Support.isTrue(value, name);
                else if (name.equals("sslProxyHost")) {
                    if (!"".equals(value)) {
                        sslProxyHost = value;
                        useSslProxy = true;
                    }
                } else if (name.equals("sslProxyPort")) sslProxyPort = Integer.parseInt(value);
                else if (name.equals("sslVerify")) {
                    if (value.equals("none")) sslVerify = SecureSocketsLayer.VERIFY_NONE;
                    else if (value.equals("peer")) sslVerify = SecureSocketsLayer.VERIFY_PEER;
                    else if (value.equals("peerCert")) sslVerify = SecureSocketsLayer.VERIFY_FAIL_IF_NO_PEER_CERT;
                    else throw new ParseException("Unknown value for sslVerify " + value, 0);
                } else if (name.equals("startupCommand")) startupCommand = value;
                else if (name.equals("userDatabase")) userDatabaseName = value;
                else if (name.equals("verbose")) {
                    if (Support.isTrue(value, name)) {
                        attributes.put(W3Context.class.getName() + "/verbose", Boolean.TRUE);
                        verbose = true;
                    }
                } else if (name.equals("virtual")) virtual = Support.isTrue(value, name);
                else throw new ParseException("Unknown property " + name, 0);
        }
    }

    public ServerSocket getServerSocket() throws IOException {
        if (!defaultServerSocket) W3Factory.setSocketFactories();
        return defaultServerSocket ? new ServerSocket(port, backlog, bindAddress) : new W3ServerSocket(port, backlog, bindAddress);
    }

    public ServerSocketChannel getServerSocketChannel() throws IOException {
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.configureBlocking(false);
        InetSocketAddress address = new InetSocketAddress(bindAddress, port);
        serverSocketChannel.socket().bind(address);
        return serverSocketChannel;
    }

    public W3Request getW3Request() throws IOException {
        return new W3Request(this);
    }

    public String getHostAddress() {
        return localHostAddress;
    }

    public String getHostName() {
        return serverName;
    }

    public void setup(String argv[], int argOffset, File optionsFile)
        throws ClassNotFoundException, IllegalAccessException, InstantiationException, InterruptedException, IOException, InvocationTargetException, MalformedURLException, NoSuchMethodException, ParseException, ParserConfigurationException, RegexException, SAXException, ServletException {
        List<String[]> argsList = null;
        if (optionsFile != null) argsList = new ArrayList<String[]>();
        else alternative = true;
        timeout = 60L * 1000L;
        proxyCaches = ProxyServlet.makeProxyCaches();
        w3context = new W3Context(this);
        for (;;) {
            if (argv != null)
                for (int argn = argOffset; argn < argv.length; argn++) {
                    char option = 0;
                    String arg = argv[argn];
                    if (arg != null && arg.charAt(0) == '-') {
                        int i;
                        for (i = 1; i < arg.length(); i++) {
                            switch (Character.toLowerCase(option = arg.charAt(i))) {
                            case 'a':
                                attributes.put(W3Context.class.getName() + "/autoCaches", Boolean.TRUE);
                                continue;
                            case 'c':
                                attributes.put(W3Context.class.getName() + "/cacheRoot", argv[++argn]);
                                continue;
                            case 'd':
                                attributes.put(W3Context.class.getName() + "/dataDirectory", dataDirectory = argv[++argn]);
                                readProperties();
                                continue;
                            case 'e':
                                isService = true;
                                continue;
                            case 'f':
                                attributes.put(W3Context.class.getName() + "/forceCaches", Boolean.TRUE);
                                continue;
                            case 'g':
                                debug = verbose = true;
                                logLevel = 3;
                                continue;
                            case 'h':
                                W3URLConnection.setCacheHeaders(false);
                                continue;
                            case 'k':
                                attributes.put(W3Context.class.getName() + "/checkCaches", Boolean.TRUE);
                                continue;
                            case 'l':
                                logLevel = Integer.parseInt(argv[++argn]);
                                verbose = false;
                                continue;
                            case 'm':
                                ownSecurityManager = true;
                                continue;
                            case 'o':
                                makeLogStats = true;
                                continue;
                            case 'p':
                                port = Integer.parseInt(argv[++argn]);
                                continue;
                            case 's':
                                secure = true;
                                continue;
                            case 't':
                                type = true;
                                continue;
                            case 'v':
                                if (optionsFile != null) W3Server.mainVerbose = true;
                                verbose = true;
                                logLevel = 3;
                                continue;
                            case 'z':
                                String args[];
                                if (i + 1 < arg.length()) {
                                    args = new String[argv.length - argn];
                                    args[0] = "-" + arg.substring(i + 1);
                                    System.arraycopy(argv, argn + 1, args, 1, argv.length - argn - 1);
                                } else {
                                    args = new String[argv.length - argn - 1];
                                    System.arraycopy(argv, argn + 1, args, 0, argv.length - argn - 1);
                                }
                                argsList.add(args);
                                i = arg.length();
                                argn = argv.length;
                                break;
                            }
                            break;
                        }
                        if (i == arg.length()) continue;
                    }
                    System.out.println(serverInfo +
                                       "\nAvailable options (default value in parantheses):\n" +
                                       "-a : set auto caches\n" +
                                       "-c <cache root> (cache/)\n" +
                                       "-d <data directory> (/)\n" +
                                       "-e : service mode\n" +
                                       "-f : force cache filling\n" +
                                       "-h : use no cache headers\n" +
                                       "-k : check only caches\n" +
                                       "-l : logLevel 0 - 2 (0)\n" +
                                       "-o : make log statistics at start\n" +
                                       "-p <port number> (80 or 443 when secure)\n" +
                                       "-s : secure on (off)\n" +
                                       "-t : type files to standard output\n" +
                                       "-v : set verbose on (off)\n" +
                                       "-z : separate different option sets");
                    if (option != 0) throw new ParseException("Unknown option " + option, 0);
                    else throw new ParseException("No arguments accepted " + arg, 0);
                }
            if (optionsFile != null && optionsFile.exists()) {
                BufferedReader bf = new BufferedReader(new FileReader(optionsFile));
                Vector<String> v = new Vector<String>();
                try {
                    String s;
                    while ((s = bf.readLine()) != null) {
                        StringTokenizer st = new StringTokenizer(s, "\t ");
                        while (st.hasMoreTokens()) {
                            String arg = st.nextToken("\t ");
                            if (arg.equals("\"")) continue;
                            if (arg.startsWith("\"")) {
                                if (st.hasMoreTokens() && !arg.endsWith("\"")) {
                                    arg += st.nextToken("\"");
                                    arg = arg.substring(1);
                                } else arg = arg.substring(1, arg.length() - 1);
                            }
                            v.addElement(arg);
                        }
                    }
                } finally {
                    bf.close();
                }
                argv = new String[v.size()];
                for (int argn = 0; argn < argv.length; argn++) argv[argn] = v.elementAt(argn);
                optionsFile = null;
            } else break;
            argOffset = 0;
        }
        if (dataDirectory == null) throw new ParseException("Data directory not specified", 0);
        if (!verbose && W3Server.mainVerbose) {
            verbose = true;
            logLevel = 3;
        }
        String cacheRoot  = (String)w3context.getAttribute(W3Context.class.getName() + "/cacheRoot");
        if (cacheRoot == null) cacheRoot = dataDirectory + "/cache/";
        else if (!(cacheRoot = cacheRoot.trim()).endsWith("/")) cacheRoot += "/";
        attributes.put(W3Context.class.getName() + "/cacheRoot", cacheRoot);
        attributes.put(W3Context.class.getName() + "/maxCacheEntryLength", String.valueOf(maxCacheEntryLength));
        for (int i = 0; i < 3; i++) logs[i] = new Log();
        if (verbose) {
            for (int i = 0; i < 3; i++) logs[i].stream = System.out;
            //DriverManager.setLogWriter(new PrintWriter(System.out, true));
        }
        group = new ThreadGroup(getClass().getName() + "_" + port);
        filterGroup = new ThreadGroup(group.getName() + "_filters");
        senderGroup = new ThreadGroup(group.getName() + "_senders");
        responderGroup = new ThreadGroup(group.getName() + "_responders");
        if (!alternative) w3context.setup();
        if (isRoot()) {
            if (serverGroup != null) setGroup(serverGroup);
            if (serverUser != null) setUser(serverUser);
        }
        if (keepAliveTimeout > 0) keepAlive = "timeout=" + keepAliveTimeoutSecs;
        if (maxConnections != -1L) clients = new HashMap<String,Client>();
        defaultPort = secure ? 443 : 80;
        if (port == -1) port = defaultPort;
        if (logLevel > 2) {
            logLine = new Vector<Object>();
            StreamTokenizer sTok = new StreamTokenizer(new StringReader(logFormat));
            sTok.resetSyntax();
            sTok.whitespaceChars(0, 32);
            sTok.wordChars(33, 255);
            sTok.ordinaryChar('"');
            sTok.ordinaryChar('[');
            sTok.ordinaryChar(']');
            boolean ordinary = false;
            StringBuffer sb = new StringBuffer();
            while (sTok.nextToken() != sTok.TT_EOF) {
                if (sTok.ttype == sTok.TT_WORD) {
                    String name = sTok.sval, header = null;
                    if (sb.length() > 0) {
                        if (!ordinary) sb.append('\t');
                        logLine.addElement(sb.toString());
                        sb.setLength(0);
                    } else if (logLine.size() > 0) logLine.addElement(ordinary ? " " : "\t");
                    int i = name.indexOf('(');
                    if (i != -1) {
                        header = name.substring(i + 1, name.length() - 1);
                        name = name.substring(0, i);
                    }
                    Integer number = logValues.get(name.toLowerCase());
                    if (number == null) throw new ParseException("Unknown log field name " + name, 0);
                    logLine.addElement(new LogColumn(number.intValue(), header));
                } else {
                    if (!ordinary && logLine.size() > 0) sb.append('\t');
                    sb.append((char)sTok.ttype);
                    ordinary = !ordinary;
                }
            }
            if (sb.length() > 0) logLine.addElement(sb.toString());
        }
        servers.put(new Integer(port), this);
        if (argsList != null) {
            for (String args[] : argsList) 
                new W3Server(args, 0, null);
        }
    }

    public void begin()
        throws ClassNotFoundException, IllegalAccessException, InstantiationException, InterruptedException, IOException, ParseException, ParserConfigurationException, SAXException, RegexException, ServletException, SocketException {
        if (!isService) Runtime.getRuntime().addShutdownHook(new W3ServerShutdownHook(this));
        if (startupCommand != null) {
            int ev = Runtime.getRuntime().exec(startupCommand).waitFor();
            if (ev != 0) throw new RuntimeException("Startup command returned error value " + ev);
        }
        if (bindAddress == null) {
            Enumeration networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = (NetworkInterface)networkInterfaces.nextElement();
                for (boolean tryIp4 = false;; tryIp4 = true) {
                    Enumeration addresses = networkInterface.getInetAddresses();
                    while (addresses.hasMoreElements()) {
                        InetAddress inetAddress = (InetAddress)addresses.nextElement();
                        if ((tryIp4 ? !(inetAddress instanceof Inet4Address) : !(inetAddress instanceof Inet6Address))
                            || inetAddress.isLoopbackAddress()) continue;
                        localAddress = inetAddress;
                        break;
                    }
                    if (localAddress != null) break;
                    if (tryIp4) break;
                }
                if (localAddress != null) break;
            }
            if (localAddress == null) localAddress = InetAddress.getLocalHost();
        } else localAddress = bindAddress;
        localHostAddress = localAddress.getHostAddress();
        if (serverName == null) attributes.put(W3Context.class.getName() + "/serverName", serverName = localAddress.getHostAddress().toLowerCase());
        if (serverID == null) attributes.put(W3Context.class.getName() + "/serverID", serverID = serverName + (port != defaultPort ? ":" + port : ""));
        sessionCookieName = "JSESSIONID";
        context = new URL(secure ? "https" : "http", serverName, port != defaultPort ? port : -1, "/");
        defaultContext = new URL(secure ? "https" : "http", bindAddress != null ? bindAddress.getHostAddress() : localAddress.getHostAddress(), port, "/");
        if (serverScheme == null) serverScheme = defaultContext.getProtocol();
        if (serverPort == -1) serverPort = defaultContext.getPort();
        if (secure) {
            SecureSocketsLayer.setDefaultReadAhead(true);
            SecureSocketsLayer.setDefaultVerify(sslVerify);
            File caFile = new File(dataDirectory, "caCert.pem");
            if (caFile.exists())
                SecureSocketsLayer.loadVerifyLocations(caFile.getPath(), null);
            SecureSocketsLayer.setCertificateFile(dataDirectory + "/cert.pem", SecureSocketsLayer.FILETYPE_PEM);
            //SecureSocketsLayer.setCertificateChainFile(dataDirectory + "/cert.pem");
            if (password == null) password = "changeit".getBytes("ISO-8859-1");
            SecureSocketsLayer.setRSAPrivateKeyFile(dataDirectory + "/key.pem", SecureSocketsLayer.FILETYPE_PEM, password);
            if (keepAliveTimeoutSecs > 0) SecureSocketsLayer.setDefaultTimeout(keepAliveTimeoutSecs);
        }
        if (!verbose) {
            for (int i = 0; i < logLevel; i++) {
                logs[i].file = new File(dataDirectory, logFilenames[i]);
                logs[i].stream = new BufferedOutputStream(new FileOutputStream(new File(dataDirectory, logFilenames[i]), true));
            }
            if (makeLogStats) checkLog(ACCESS_LOG, true);
        }
        if (logLevel > 2) startLog();
        new File(dataDirectory, stopReallyFilename).delete();
        attributes.put(W3Context.class.getName(), this);
        attributes.put(W3Context.class.getName() + "/proxyCaches", proxyCaches);
        attributes.put(W3Context.class.getName() + "/servletPathTable", servletPathTable);
        attributes.put(W3Context.class.getName() + "/requestTimeout", new Long(timeout));
        if (verbose) w3context.setAttribute(W3Context.class.getName() + "/verbose", Boolean.TRUE);
        if (ownSecurityManager) {
            if (verbose) log("Setting own security manager");
            W3SecurityManager.set();
        }
        readParameters();
        if (!alternative) w3context.prepare(true);
        if (!virtual && !serverDisabled) {
            request = getW3Request();
            if (socketChannels) {
                ServerSocketChannel serverSocketChannel = getServerSocketChannel();
                socketChannelServer = new SocketChannelServer(serverSocketChannel, request, timeout);
                socketChannelServer.start();
            } else {
                serverSocket = getServerSocket();
                start();
            }
        }
        if (!alternative) w3context.prepare(false);
        if (logLevel > 1) {
            log(defaultContext + " (" + serverInfo + ") at " + Support.format(new Date()));
            W3URLConnection.setLogStream(logs[EVENT_LOG].stream, verbose);
        }
        flushLogs();
    }

    public void end() {
        try {
            if (debug) log("Current requests = " + currentRequests);
            log(defaultContext + " (" + serverInfo + ") ending at " + Support.format(new Date()));
            if (socketChannelServer != null) {
                if (socketChannelServer.isAlive())
                    socketChannelServer.interrupt();
            } else if (isAlive()) interrupt();
            flushLogs();
            if (!virtual && !serverDisabled)
                try {
                    if (serverSocket != null)
                        try {
                            new W3Socket(bindAddress != null ? bindAddress : InetAddress.getLocalHost(), port).close();
                        } catch (IOException ex) {
                            if (logLevel > 0) log(ex);
                        } finally {
                            serverSocket.close();
                        }
                } catch (IOException ex) {
                    if (logLevel > 0) log(ex);
                } finally {
                    Thread threads[] = new Thread[group.activeCount()];
                    group.enumerate(threads);
                    for (int i = 0; i < threads.length; i++)
                        if (threads[i] != null) {
                            threads[i].interrupt();
                            try {
                                threads[i].join(1000L);
                            } catch (InterruptedException ex) {}
                        }
                }
        } finally {
            try {
                if (w3context != null) w3context.destroy();
            } finally {
                try {
                    if (userDatabase != null) userDatabase.close();
                } catch (SQLException ex) {
                    if (logLevel > 0) log(ex);
                } finally {
                    try {
                        if (shutdownCommand != null) Runtime.getRuntime().exec(shutdownCommand).waitFor();
                    } catch (Exception ex) {
                        if (logLevel > 0) log(ex);
                    } finally {
                        for (int i = 0; i < logLevel; i++)
                            if (logs[i] != null)
                                try {
                                    if (verbose) logs[i].stream.flush();
                                    else logs[i].stream.close();
                                } catch (IOException ex) {}
                    }
                }
            }
        }
    }

    /** Constructs W3Server, setup and begin -methods must be called separately. */
    public W3Server() {
    }

    /** Constructs W3Server and begins running. */
    public W3Server(String argv[], int argOffset, File optionsFile)
        throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException, InterruptedException, InvocationTargetException, MalformedURLException, NoSuchMethodException, ParseException, ParserConfigurationException, SAXException, RegexException, ServletException, SocketException {
        setup(argv, argOffset, optionsFile);
    }

    public static synchronized void startup()
        throws ClassNotFoundException, IllegalAccessException, InstantiationException, InterruptedException, IOException, ParseException, ParserConfigurationException, SAXException, RegexException, ServletException, SocketException {
        if (W3Server.servers == null) return;
        Iterator serverIter = W3Server.servers.values().iterator();
        while (serverIter.hasNext()) ((W3Server)serverIter.next()).begin();
    }

    public static synchronized void shutdown() {
        if (shutdownCalled || W3Server.servers == null) return;
        W3Server.shutdownCalled = true;
        Iterator serverIter = W3Server.servers.values().iterator();
        while (serverIter.hasNext()) ((W3Server)serverIter.next()).end();
    }

    public static void waitForEnd(W3Server w3)
        throws InterruptedException {
        if (w3.isService) {
            waitStop();
            W3Server.shutdown();
            return;
        }
        synchronized (w3) {
            w3.wait();
        }
    }

    public static void main(String argv[]) {
        int argOffset = 0;
        String dir = argv.length > 0 && !argv[0].startsWith("-") ? argv[argOffset++] : System.getProperty("user.home");
        File optionsFile = new File(dir, "KeppiProxyOptions"),
            errorFile = new File(dir, "KeppiProxyError");
        W3Server w3 = null;
        try {
            errorFile.delete();
            //W3SecurityManager.set();
            w3 = new W3Server(argv, argOffset, optionsFile);
            W3Server.startup();
            W3Server.waitForEnd(w3);
            System.exit(0);
        } catch (Throwable th) {
            try {
                try {
                    W3Server.shutdown();
                } finally {
                    th.printStackTrace(System.out);
                    System.out.flush();
                    PrintWriter pw = new PrintWriter(new FileOutputStream(errorFile));
                    th.printStackTrace(pw);
                    pw.close();
                }
            } catch (Throwable th1) {}
            finally {
                System.exit(1);
            }
        }
    }

}
