
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.text.*;
import java.util.*;
import javax.naming.*;
import javax.naming.directory.*;
import javax.naming.ldap.*;
import javax.servlet.*;

class CookieStoreCleaning extends Thread {
    ServletContext servletContext;

    CookieStoreCleaning(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    public void run() {
        LdapContext ldapContext[] = new InitialLdapContext[1];
        CookieStore cookieStore = null;
        try {
            cookieStore = new CookieStore(ldapContext);
            cookieStore.cleanCookieStore();
        } catch (Exception ex) {
            servletContext.log(ex.toString(), ex);
        } finally {
            if (cookieStore != null) cookieStore.close(ldapContext);
        }
    }

}

/** Cookie data store.<br>
    Stores cookie data to LDAP directory with standard schema.<br>
    Cookie data structure has following properties:<br>
    Object class: organizationalUnit<br>
    Distinguished name: ou=unique id<br>
    Attributes:<br>
    <table>
    <caption>Organizational unit</caption>
    <tr><th>postalAddress</th><td>client</td></tr>
    <tr><th>description</th><td>domainPattern</td></tr>
    <tr><th>street</th><td>pathPattern</td></tr>
    <tr><th>l</th><td>name</td></tr>
    <tr><th>st</th><td>value</td></tr>
    <tr><th>physicalDeliveryOfficeName</th><td>expires</td></tr>
    </table> */
public class CookieStore {
    public static long cleaningInterval = 7L * 24L * 60L * 60L * 1000L, cleaningTime = 0L;

    LdapContext cookieStoreContext;

    public CookieStore(LdapContext ldapContext[]) throws NamingException {
        cookieStoreContext = W3Server.open(this.getClass().getName(), ldapContext);
    }

    public void close(LdapContext ldapContext[]) {
        W3Server.close(cookieStoreContext, ldapContext);
    }

    public void setCookieHeaderValues(HeaderList headerList, String client, String clientHost, String clientPath) throws NamingException, ParseException {
        NamingEnumeration cookieResultEnum = null;
        try {
            cookieResultEnum = cookieStoreContext.search("", "(&(objectClass=organizationalUnit)(postalAddress=" + Support.escapeFilter(client) + ")(description=" + Support.escapeFilter(new StringBuffer(clientHost).reverse().toString()) + "*)(street=" + Support.escapeFilter(clientPath) + "*))", null);
            StringBuffer cookieBuf = new StringBuffer();
            long ctm = System.currentTimeMillis();
            while (cookieResultEnum.hasMore()) {
                SearchResult cookieResult = (SearchResult)cookieResultEnum.next();
                Attributes cookieAttributes = cookieResult.getAttributes();
                long expires = Support.parseGeneralizedTime((String)cookieAttributes.get("physicalDeliveryOfficeName").get());
                if (expires != -1L && expires < ctm) continue;
                if (cookieBuf.length() > 0) cookieBuf.append("; ");
                Attribute valueAttribute = cookieAttributes.get("st");
                String value = valueAttribute != null ? (String)valueAttribute.get() : "";
                cookieBuf.append(cookieAttributes.get("l").get()).append('=').append(value);
            }
            if (cookieBuf.length() > 0) headerList.append(new Header("Cookie", cookieBuf.toString()));
        } finally {
            if (cookieResultEnum != null) cookieResultEnum.close();
        }
    }

    public void getCookieHeaderValues(HeaderList headerList, String client, String clientHost, String clientPath) throws NamingException {
        Header setCookieHeaders[] = headerList.getHeaders("set-cookie");
        if (setCookieHeaders == null) return;
        long ctm = System.currentTimeMillis();
        for (int i = 0; i < setCookieHeaders.length; i++) {
            HashMap<String,String> params = new HashMap<String,String>();
            Support.parse(setCookieHeaders[i].getValue(), params, ";", false);
            String domain = params.get("domain"), path = params.get("path");
            java.util.Date dt;
            String s;
            long expires = (s = params.get("expires")) != null &&
                (dt = Support.parse(s)) != null ? dt.getTime() : -1L;
            boolean removing = expires != -1L && expires < ctm;
            String domainPattern = new StringBuffer(domain != null ? domain : clientHost).reverse().toString(),
                pathPattern = path != null ? path : clientPath;
            Iterator paramIter = params.entrySet().iterator();
            while (paramIter.hasNext()) {
                Map.Entry entry = (Map.Entry)paramIter.next();
                String name = (String)entry.getKey(), value = (String)entry.getValue();
                if (name.equals("domain") || name.equals("expires") || name.equals("path") || name.equals("secure")) continue;
                SearchResult cookieResult = null;
                NamingEnumeration cookieResultEnum = null;
                try {
                    cookieResultEnum = cookieStoreContext.search("",
                                                                 "(&(objectClass=organizationalUnit)(postalAddress=" + Support.escapeFilter(client) +
                                                                 ")(description=" + Support.escapeFilter(domainPattern) +
                                                                 ")(street=" + Support.escapeFilter(pathPattern) + "))",
                                                                 new SearchControls(SearchControls.ONELEVEL_SCOPE, 1L, 0, null, false, true));
                    cookieResult = (SearchResult)cookieResultEnum.next();
                } catch (NameNotFoundException ex) {} catch (NoSuchElementException ex) {} finally {
                    if (cookieResultEnum != null) cookieResultEnum.close();
                }
                if (removing) {
                    if (cookieResult != null)
                        try {
                            cookieStoreContext.unbind(cookieResult.getName());
                        } catch (NamingException ex) {}
                    continue;
                }
                Attributes cookieAttributes = new BasicAttributes();
                Attribute cookieObjectClass = new BasicAttribute("objectClass");
                cookieObjectClass.add("top");
                cookieObjectClass.add("organizationalUnit");
                cookieAttributes.put(cookieObjectClass);
                cookieAttributes.put("postalAddress", client);
                cookieAttributes.put("description", domainPattern);
                cookieAttributes.put("street", pathPattern);
                cookieAttributes.put("l", name);
                if (!value.equals("")) cookieAttributes.put("st", value);
                cookieAttributes.put("physicalDeliveryOfficeName", Support.generalizedTime(expires));
                cookieStoreContext.rebind(cookieResult != null ? cookieResult.getName() : W3Server.makeName(cookieStoreContext, "ou"), null, cookieAttributes);
            }
        }
    }

    public void cleanCookieStore() throws NamingException, IOException {
        String now = Support.generalizedTime(System.currentTimeMillis());
        NamingEnumeration cookieResultEnum = null;
        try {
            //		cookieStoreContext.setRequestControls(new BasicControl[] {new SortControl(new SortKey[] {new SortKey("physicalDeliveryOfficeName")}, SortControl.CRITICAL)});
            cookieResultEnum = cookieStoreContext.search("", "(objectClass=organizationalUnit)",
                                                         new SearchControls(SearchControls.ONELEVEL_SCOPE, 0L, 0, new String[] {"physicalDeliveryOfficeName"}, false, false));
            try {
                while (cookieResultEnum.hasMore()) {
                    SearchResult cookieResult = (SearchResult)cookieResultEnum.next();
                    if (((String)((SearchResult)cookieResultEnum.next()).getAttributes().get("physicalDeliveryOfficeName").get()).compareTo(now) <= 0)
                        cookieStoreContext.unbind(cookieResult.getName());
                }
            } catch (NoSuchElementException ex) {} finally {
                cookieResultEnum.close();
            }
        } finally {
            if (cookieResultEnum != null) cookieResultEnum.close();
        }
    }

    public static void checkCleaning(ServletContext servletContext) {
        if (System.currentTimeMillis() - cleaningTime > cleaningInterval) new CookieStoreCleaning(servletContext).start();
    }

}
