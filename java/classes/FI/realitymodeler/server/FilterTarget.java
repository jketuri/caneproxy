
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;
import java.util.jar.JarFile;
import java.util.jar.JarEntry;
import java.util.zip.ZipEntry;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.SAXParser;
import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;

public class FilterTarget implements Cloneable {
    Regexp pattern = null;
    String urlPattern = null;
    W3Filter w3filter = null;
    EnumSet<DispatcherType> dispatcherTypes = null;

    public FilterTarget(Regexp pattern, String urlPattern, W3Filter w3filter, EnumSet<DispatcherType> dispatcherTypes) {
        if (pattern == null && urlPattern != null) {
            String filterPattern = urlPattern;
            if (filterPattern.equals("/")) filterPattern = "*";
            else if (filterPattern.startsWith("/*"))
                filterPattern = filterPattern.substring(1);
            if (filterPattern.indexOf('*') == -1 && !filterPattern.endsWith("/")) filterPattern += "/";
            pattern = !filterPattern.equals("*") ? new Regexp(filterPattern) : null;
        }
        this.pattern = pattern;
        this.urlPattern = urlPattern;
        this.w3filter = w3filter;
        this.dispatcherTypes = dispatcherTypes;
    }

    public Object clone() {
        return new FilterTarget(pattern, urlPattern, (W3Filter)w3filter.clone(), dispatcherTypes);
    }

    public String toString() {
        return getClass().getName() +
            " {pattern=" + pattern +
            ", urlPattern=" + urlPattern +
            ", filterClass=" + w3filter.className +
            ", dispatcherTypes=" + dispatcherTypes + "}";
    }

}
