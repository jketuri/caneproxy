
package FI.realitymodeler.server;

import java.util.*;

public class W3ServletIteratorEnumeration implements Enumeration {
    Iterator iterator;
    boolean names;

    public W3ServletIteratorEnumeration(Iterator iterator, boolean names) {
        this.iterator = iterator;
        this.names = names;
    }

    public boolean hasMoreElements() {
        return iterator.hasNext();
    }

    public Object nextElement() {
        W3Servlet w3servlet = (W3Servlet)iterator.next();
        if (names) return w3servlet.getServletName();
        return w3servlet.servlet;
    }

}
