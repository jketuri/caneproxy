
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.text.*;
import java.util.*;
import javax.servlet.*;

/** Counter servlet filter which counts visitors in page. */
public class CounterFilter extends GenericServlet {
    static final long serialVersionUID = 0L;
    private static Date first = null;
    private static int count = 0;

    private File dir;
    private ResourceBundle messages;

    public void init(ServletConfig config)
        throws ServletException {
        try {
            super.init(config);
            messages = W3URLConnection.getMessages("FI.realitymodeler.server.resources.Messages");
            String s = getInitParameter("directory");
            if (s == null) {
                ServletContext context = getServletContext();
                if (context instanceof W3Server) s = ((W3Server)context).dataDirectory;
            }
            dir = new File(s != null ? s : ".");
            if ((s = getInitParameter("initial")) == null) {
                File f = new File(dir, "counter_value");
                if (f.exists()) {
                    DataInputStream din = new DataInputStream(new FileInputStream(f));
                    count = din.readInt();
                    din.close();
                } else count = 0;
            } else count = Integer.parseInt(s);
            File f = new File(dir, "counter_first");
            if (f.exists()) {
                DataInputStream din = new DataInputStream(new FileInputStream(f));
                first = new Date(din.readLong());
                din.close();
            } else {
                first = new Date();
                DataOutputStream dout = new DataOutputStream(new FileOutputStream(f));
                dout.writeLong(first.getTime());
                dout.close();
            }
        } catch (IOException ex) {
            throw new ServletException(Support.stackTrace(ex));
        }
    }

    public void destroy() {
        try {
            DataOutputStream dout = new DataOutputStream(new FileOutputStream(new File(dir, "counter_value")));
            dout.writeInt(count);
            dout.close();
        } catch (IOException ex) {}
    }

    public void service(ServletRequest req, ServletResponse res)
        throws IOException {
        count++;
        PrintWriter writer = res.getWriter();
        if (first != null) writer.print(MessageFormat.format(messages.getString("countVisitor(s)SinceFirst"),
                                                             new Object[] {new Long(count), first}) + " ");
        writer.print(MessageFormat.format(messages.getString("todayIsDate"), new Object[] {new Date()}));
    }

}
