
package FI.realitymodeler.server;

import java.util.*;
import javax.servlet.*;

/**
 * @since Servlet 3.0
 * TODO SERVLET3 - Add comments
 */
public class W3FilterRegistration implements FilterRegistration {

    W3Context w3context;
    W3Filter w3filter;
    List<String> servletNameMappings = new ArrayList<>();
    List<String> urlPatternMappings = new ArrayList<>();

    W3FilterRegistration(W3Context w3context, W3Filter w3filter) {
        this.w3context = w3context;
        this.w3filter = w3filter;
    }

    public String getName() {
        return null;
    }

    public String getClassName() {
        return getClass().getName();
    }

    /**
     * Add an initialisation parameter if not already added.
     *
     * @param name  Name of initialisation parameter
     * @param value Value of initialisation parameter
     * @return <code>true</code> if the initialisation parameter was set,
     *         <code>false</code> if the initialisation parameter was not set
     *         because an initialisation parameter of the same name already
     *         existed
     * @throws IllegalArgumentException if name or value is <code>null</code>
     * @throws IllegalStateException if the ServletContext associated with this
     *         registration has already been initialised
     */
    public boolean setInitParameter(String name, String value) {
        boolean wasSet = w3filter.initParameters.containsKey(name);
        w3filter.initParameters.put(name, value);
        return wasSet;
    }

    /**
     * Get the value of an initialisation parameter.
     *
     * @param name  The initialisation parameter whose value is required
     *
     * @return The value of the named initialisation parameter
     */
    public String getInitParameter(String name) {
        return w3filter.initParameters.get(name);
    }

    /**
     * Add multiple initialisation parameters. If any of the supplied
     * initialisation parameter conflicts with an existing initialisation
     * parameter, no updates will be performed.
     *
     * @param initParameters The initialisation parameters to add
     *
     * @return The set of initialisation parameter names that conflicted with
     *         existing initialisation parameter. If there are no conflicts,
     *         this Set will be empty.
     * @throws IllegalArgumentException if any of the supplied initialisation
     *         parameters have a null name or value
     * @throws IllegalStateException if the ServletContext associated with this
     *         registration has already been initialised
     */
    public Set<String> setInitParameters(Map<String, String> initParameters) {
        Set<String> conflicts = new HashSet<String>();
        for (String initParameter : initParameters.keySet()) {
            if (w3filter.initParameters.containsKey(initParameter)) {
                conflicts.add(initParameter);
            } else {
                w3filter.initParameters.put(initParameter, initParameters.get(initParameter));
            }
        }
        return conflicts;
    }

    /**
     * Get the names and values of all the initialisation parameters.
     *
     * @return A Map of initialisation parameter names and associated values
     *         keyed by name
     */
    public Map<String, String> getInitParameters() {
        return w3filter.initParameters;
    }

    /**
     * Add a mapping for this filter to one or more named Servlets.
     *
     * @param dispatcherTypes The dispatch types to which this filter should
     *                        apply
     * @param isMatchAfter    Should this filter be applied after any mappings
     *                        defined in the deployment descriptor
     *                        (<code>true</code>) or before?
     * @param servletNames    Requests mapped to these servlets will be
     *                        processed by this filter
     * @throws IllegalArgumentException if the list of sevrlet names is empty
     *                                  or null
     * @throws IllegalStateException if the associated ServletContext has
     *                               already been initialised
     */
    public void addMappingForServletNames(
                                          EnumSet<DispatcherType> dispatcherTypes,
                                          boolean isMatchAfter, String... servletNames) {
        List<String> servletNameList = new ArrayList<String>();
        for (String servletName : servletNames) {
            servletNameList.add(servletName);
        }
        w3context.addMappingForServletNames(w3filter, servletNameMappings, dispatcherTypes, isMatchAfter, servletNameList);
    }

    /**
     *
     * @return TODO
     */
    public Collection<String> getServletNameMappings() {
        return servletNameMappings;
    }

    /**
     * Add a mapping for this filter to one or more URL patterns.
     *
     * @param dispatcherTypes The dispatch types to which this filter should
     *                        apply
     * @param isMatchAfter    Should this filter be applied after any mappings
     *                        defined in the deployment descriptor
     *                        (<code>true</code>) or before?
     * @param urlPatterns     The URL patterns to which this filter should be
     *                        applied
     * @throws IllegalArgumentException if the list of URL patterns is empty or
     *                                  null
     * @throws IllegalStateException if the associated ServletContext has
     *                               already been initialised
     */
    public void addMappingForUrlPatterns(
                                         EnumSet<DispatcherType> dispatcherTypes,
                                         boolean isMatchAfter, String... urlPatterns) {
        List<String> urlPatternList = new ArrayList<String>();
        for (String urlPattern : urlPatterns) {
            urlPatternList.add(urlPattern);
        }
        w3context.addMappingForUrlPatterns(w3filter, urlPatternMappings, dispatcherTypes, isMatchAfter, urlPatternList);
    }

    /**
     *
     * @return TODO
     */
    public Collection<String> getUrlPatternMappings() {
        return urlPatternMappings;
    }

    public static class W3Dynamic
        extends W3FilterRegistration implements FilterRegistration.Dynamic {

        W3Dynamic(W3Context w3context, W3Filter w3filter) {
            super(w3context, w3filter);
        }

        /**
         * Mark this Servlet/Filter as supported asynchronous processing.
         *
         * @param isAsyncSupported  Should this Servlet/Filter support
         *                          asynchronous processing
         *
         * @throws IllegalStateException if the ServletContext associated with
         *         this registration has already been initialised
         */
        public void setAsyncSupported(boolean isAsyncSupported) {
        }
    }
}
