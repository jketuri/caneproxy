
package FI.realitymodeler.server;

import FI.realitymodeler.common.*;

import java.util.*;

import javax.naming.*;
import javax.naming.spi.*;

class W3NamingEnumeration<T> implements NamingEnumeration<T> {
    Iterator<Binding> iterator;

    W3NamingEnumeration(Iterator<Binding> iterator) {
        this.iterator = iterator;
    }

    public T next() {
        return (T)iterator.next();
    }

    public boolean hasMore() {
        return iterator.hasNext();
    }

    public void close() {
    }

    public boolean hasMoreElements() {
        return hasMore();
    }

    public T nextElement() {
        return next();
    }

}

public class W3Naming implements Context, InitialContextFactory, NameParser {
    static W3Naming instance = new W3Naming();
    static Properties nameProperties = new Properties();
    static {
        nameProperties.put("jndi.syntax.direction", "left_to_right");
        nameProperties.put("jndi.syntax.separator", "/");
    }

    protected Map<String, Binding> map = new TreeMap<String, Binding>();
    protected Hashtable<String, Object> environment = null;
    protected String name = null;

    public W3Naming() {
        environment = new Hashtable<String,Object>();
        name = "";
    }

    public W3Naming(Hashtable environment, String name) {
        this.environment = (Hashtable<String, Object>)environment.clone();
        this.name = name;
    }

    protected Object bind(Name name, Object obj, boolean rebind, boolean subcontext)
        throws NamingException {
        obj = NamingManager.getStateToBind(obj, name, this, environment);
        Object value = this, value1 = null;
        String nameComponent = null;
        Enumeration<String> names = name.getAll();
        while (names.hasMoreElements()) {
            nameComponent = names.nextElement();
            value1 = ((W3Naming)value).map.get(nameComponent);
            if (value1 == null) break;
            value = ((Binding)value1).getObject();
        }
        if (!rebind && value1 != null) throw new NameAlreadyBoundException();
        for (;;) {
            boolean hasMoreNames = names.hasMoreElements();
            value1 = hasMoreNames || subcontext ? new W3Naming(environment, nameComponent) : obj;
            ((W3Naming)value).map.put(nameComponent, new Binding(nameComponent, value1 != null ? value1.getClass().getName() : null, value1, true));
            if (!hasMoreNames) break;
            value = value1;
            nameComponent = names.nextElement();
        }
        return value1;
    }

    public Object lookup(Name name)
        throws NamingException {
        Object value = this;
        Enumeration<String> names = name.getAll();
        while (names.hasMoreElements()) {
            String nameComponent = names.nextElement();
            value = ((W3Naming)value).map.get(nameComponent);
            if (value == null) break;
            value = ((Binding)value).getObject();
        }
        if (value == this || value == null) throw new NameNotFoundException();
        if (value instanceof Reference)
            try {
                value = NamingManager.getObjectInstance(value, name, this, environment);
            } catch (Exception ex) {
                ex.printStackTrace();
                NamingException ex1 = new NamingException();
                ex1.setRootCause(ex);
                throw ex1;
            }
        return value;
    }

    public Object lookup(String name)
        throws NamingException {
        return lookup(parse(name));
    }

    public void bind(Name name, Object obj)
        throws NamingException {
        bind(name, obj, false, false);
    }

    public void bind(String name, Object obj)
        throws NamingException {
        bind(parse(name), obj);
    }

    public void rebind(Name name, Object obj)
        throws NamingException {
        bind(name, obj, true, false);
    }

    public void rebind(String name, Object obj)
        throws NamingException {
        rebind(parse(name), obj);
    }

    public void unbind(Name name)
        throws NameNotFoundException {
        Object value = null, value1 = this;
        String nameComponent = null;
        Enumeration<String> names = name.getAll();
        boolean hasMoreNames = false;
        for (;;) {
            hasMoreNames = names.hasMoreElements();
            if (!hasMoreNames) break;
            value = value1;
            nameComponent = names.nextElement();
            value1 = ((W3Naming)value).map.get(nameComponent);
            if (value1 == null) break;
            value1 = ((Binding)value1).getObject();
        }
        if (value1 == this || value1 == null && hasMoreNames) throw new NameNotFoundException();
        ((W3Naming)value).map.remove(nameComponent);
    }

    public void unbind(String name)
        throws InvalidNameException, NameNotFoundException {
        unbind(parse(name));
    }

    public void rename(Name oldName, Name newName)
        throws NamingException {
        bind(newName, lookup(oldName));
        unbind(oldName);
    }

    public void rename(String oldName, String newName)
        throws NamingException {
        bind(newName, lookup(oldName));
        unbind(oldName);
    }

    public NamingEnumeration<NameClassPair> list(Name name)
        throws NamingException {
        return new W3NamingEnumeration<NameClassPair>(((W3Naming)lookup(name)).map.values().iterator());
    }

    public NamingEnumeration<NameClassPair> list(String name)
        throws NamingException {
        return list(parse(name));
    }

    public NamingEnumeration<Binding> listBindings(Name name)
        throws NamingException {
        return new W3NamingEnumeration<Binding>(((W3Naming)lookup(name)).map.values().iterator());
    }

    public NamingEnumeration<Binding> listBindings(String name)
        throws NamingException {
        return listBindings(parse(name));
    }

    public void destroySubcontext(Name name)
        throws NameNotFoundException {
        unbind(name);
    }

    public void destroySubcontext(String name)
        throws InvalidNameException, NameNotFoundException {
        destroySubcontext(parse(name));
    }

    public Context createSubcontext(Name name)
        throws NamingException {
        return (Context)bind(name, null, false, true);
    }

    public Context createSubcontext(String name)
        throws NamingException {
        return createSubcontext(parse(name));
    }

    public Object lookupLink(Name name)
        throws NamingException {
        return lookup(name);
    }

    public Object lookupLink(String name)
        throws NamingException {
        return lookupLink(parse(name));
    }

    public NameParser getNameParser(Name name) {
        return this;
    }

    public NameParser getNameParser(String name) {
        return this;
    }
    
    public Name composeName(Name name, Name prefix)
        throws InvalidNameException {
        return new CompoundName(prefix.toString() + "/" + name.toString(), nameProperties);
    }

    public String composeName(String name, String prefix) {
        return prefix + "/" + name;
    }

    public Object addToEnvironment(String propName, Object propVal) {
        return environment.put(propName, propVal);
    }

    public Object removeFromEnvironment(String propName) {
        return environment.remove(propName);
    }

    public Hashtable<?,?> getEnvironment() {
        return environment;
    }

    public void close() {
    }

    public String getNameInNamespace() {
        return name;
    }

    public Context getInitialContext(Hashtable environment) {
        return W3Naming.instance;
    }

    public Name parse(String name)
        throws InvalidNameException {
        return new CompoundName(name, nameProperties);
    }

}
