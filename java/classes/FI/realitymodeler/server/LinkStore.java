
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.text.*;
import java.util.*;
import javax.naming.*;
import javax.naming.directory.*;
import javax.naming.ldap.*;
import javax.servlet.*;

class LinkStoreCleaning extends Thread {
    ServletContext servletContext;

    LinkStoreCleaning(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    public void run() {
        LdapContext ldapContext[] = new InitialLdapContext[1];
        LinkStore linkStore = null;
        try {
            linkStore = new LinkStore(ldapContext);
            linkStore.cleanLinkStore();
        } catch (Exception ex) {
            servletContext.log(ex.toString(), ex);
        } finally {
            if (linkStore != null) linkStore.close(ldapContext);
        }
    }

}

/** Link data store.<br>
    Stores link data to LDAP directory with standard schema.<br>
    Link data structure has following properties:<br>
    Object class: organizationalUnit<br>
    Distinguished name: ou=unique id<br>
    Attributes:<br>
    <table>
    <caption>Organizational unit</caption>
    <tr><th>postalAddress</th><td>link number</td></tr>
    <tr><th>description</th><td>url</td></tr>
    <tr><th>physicalDeliveryOfficeName</th><td>time last accessed</td></tr>
    <tr><th>street</th><td>form fields</td></tr>
    <tr><th>l</th><td>method</td></tr>
    <tr><th>st</th><td>encoding type</td></tr>
    </table> */
public class LinkStore {
    public static long linkLifeSpan = 7L * 24L * 60L * 60L * 1000L, cleaningTime = 0L;

    static long linkNumber = 0L;

    LdapContext linkStoreContext;

    public LinkStore(LdapContext ldapContext[]) throws NamingException {
        linkStoreContext = W3Server.open(this.getClass().getName(), ldapContext);
    }

    public void close(LdapContext ldapContext[]) {
        W3Server.close(linkStoreContext, ldapContext);
    }

    public void saveLink(Link link) throws NamingException, UnsupportedEncodingException {
        SearchResult linkResult = null;
        NamingEnumeration linkResultEnum = null;
        try {
            linkResultEnum = linkStoreContext.search("",
                                                     "(&(objectClass=organizationalUnit)(description=" + Support.escapeFilter(link.getUrl()) +
                                                     ")" + (link instanceof Form ? "(l=" + Support.escapeFilter(((Form)link).getMethod()) + ")" : "") + ")",
                                                     new SearchControls(SearchControls.ONELEVEL_SCOPE, 1L, 0, null, false, true));
            linkResult = (SearchResult)linkResultEnum.next();
        } catch (NameNotFoundException ex) {} catch (NoSuchElementException ex) {} finally {
            if (linkResultEnum != null) linkResultEnum.close();
        }
        if (linkResult != null) {
            linkStoreContext.modifyAttributes(linkResult.getName(), linkStoreContext.REPLACE_ATTRIBUTE, new BasicAttributes());
            return;
        }
        Attributes linkAttributes = new BasicAttributes();
        Attribute linkObjectClass = new BasicAttribute("objectClass");
        linkObjectClass.add("top");
        linkObjectClass.add("organizationalUnit");
        linkAttributes.put(linkObjectClass);
        linkAttributes.put("postalAddress", Long.toString(++linkNumber < 0L ? 0L : linkNumber, Character.MAX_RADIX));
        linkAttributes.put("description", link.getUrl());
        linkAttributes.put("physicalDeliveryOfficeName", Support.generalizedTime(System.currentTimeMillis()));
        if (link instanceof Form) {
            Attribute formAttribute = new BasicAttribute("street");
            Iterator iter = ((Form)link).fields.iterator();
            while (iter.hasNext()) {
                Field field = (Field)iter.next();
                formAttribute.add(field.toCoded());
            }
            linkAttributes.put("street", formAttribute);
            linkAttributes.put("l", ((Form)link).getMethod());
            linkAttributes.put("st", String.valueOf(((Form)link).getEncTypeCode()));
        }
        linkStoreContext.rebind(W3Server.makeName(linkStoreContext, "ou"), null, linkAttributes);
    }

    public Link loadLink(long linkNumber, boolean isForm) throws NamingException, IOException {
        NamingEnumeration linkResultEnum = null;
        try {
            linkResultEnum = linkStoreContext.search("", "(&(objectClass=organizationalUnit)(postalAddress=" + Long.toString(linkNumber, Character.MAX_RADIX) + "))",
                                                     new SearchControls(SearchControls.ONELEVEL_SCOPE, 1L, 0, null, false, true));
            if (!linkResultEnum.hasMore()) throw new IOException("Failed to find " + (isForm ? "form" : "link"));
            SearchResult linkResult = (SearchResult)linkResultEnum.next();
            Attributes linkAttributes = linkResult.getAttributes();
            Link link;
            if (isForm) {
                link = new Form();
                Vector<Object> fields = ((Form)link).fields;
                Attribute formAttribute = linkAttributes.get("street");
                NamingEnumeration fieldEnum = formAttribute.getAll();
                while (fieldEnum.hasMore()) fields.addElement(new Field((String)fieldEnum.next()));
                ((Form)link).method = (String)linkAttributes.get("l").get();
                ((Form)link).encTypeCode = Byte.parseByte((String)linkAttributes.get("st").get());
            } else link = new Link();
            link.url = (String)linkAttributes.get("description").get();
            link.number = linkNumber;
            link.time = System.currentTimeMillis();
            linkAttributes = new BasicAttributes();
            linkAttributes.put("physicalDeliveryOfficeName", Support.generalizedTime(System.currentTimeMillis()));
            linkStoreContext.modifyAttributes(linkResult.getName(), linkStoreContext.REPLACE_ATTRIBUTE, linkAttributes);
            return link;
        } finally {
            if (linkResultEnum != null) linkResultEnum.close();
        }
    }

    public void cleanLinkStore() throws NamingException, IOException {
        String time = Support.generalizedTime(System.currentTimeMillis() - linkLifeSpan);
        NamingEnumeration linkResultEnum = null;
        try {
            //		linkStoreContext.setRequestControls(new BasicControl[] {new SortControl(new SortKey[] {new SortKey("physicalDeliveryOfficeName")}, SortControl.CRITICAL)});
            linkResultEnum = linkStoreContext.search("", "(objectClass=organizationalUnit)",
                                                     new SearchControls(SearchControls.ONELEVEL_SCOPE, 0L, 0, new String[] {"physicalDeliveryOfficeName"}, false, false));
            try {
                while (linkResultEnum.hasMore()) {
                    SearchResult linkResult = (SearchResult)linkResultEnum.next();
                    if (((String)((SearchResult)linkResultEnum.next()).getAttributes().get("physicalDeliveryOfficeName").get()).compareTo(time) < 0)
                        linkStoreContext.unbind(linkResult.getName());
                }
            } catch (NoSuchElementException ex) {} finally {
                linkResultEnum.close();
            }
        } finally {
            if (linkResultEnum != null) linkResultEnum.close();
        }
    }

    public static void checkCleaning(ServletContext servletContext) {
        if (System.currentTimeMillis() - cleaningTime > linkLifeSpan) new LinkStoreCleaning(servletContext).start();
    }

}
