
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class Plain2Html extends HttpServlet {
    static final long serialVersionUID = 0L;

    Map<String, String> servletPathTable = null;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        servletPathTable = (Map<String, String>)config.getServletContext().getAttribute("FI.realitymodeler.server.W3Server/servletPathTable");
    }

    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String charset = res.getCharacterEncoding(),
            charsetName = charset != null ? Support.getCharacterEncoding(charset) : null;
        InputStream in = req.getInputStream();
        res.setContentType(Support.htmlType);
        OutputStream out = Support.getHtmlOutputStream(res.getOutputStream(), servletPathTable, charsetName);
        byte b[] = new byte[Support.bufferLength];
        for (int n; (n = in.read(b)) > 0;) out.write(b, 0, n);
        out.write(0);
    }

}
