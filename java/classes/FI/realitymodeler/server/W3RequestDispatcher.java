
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.jar.JarFile;
import java.util.jar.JarEntry;
import java.util.zip.ZipEntry;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.SAXParser;
import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;

/** Defines an object that receives requests from the client and sends them to any
    resource (such as a servlet, HTML file, or JSP file) on the server. The servlet container
    creates the RequestDispatcher object, which is used as a wrapper around a
    server resource located at a particular path or given by a particular name.
    This interface is intended to wrap servlets, but a servlet container can create
    RequestDispatcher objects to wrap any type of resource.
    @see javax.servlet.ServletContext#getRequestDispatcher(String)
    @see javax.servlet.ServletContext#getNamedDispatcher(String)
    @see javax.servlet.ServletRequest#getRequestDispatcher(String)

*/
public class W3RequestDispatcher implements RequestDispatcher {
    W3Context w3context = null;
    W3Servlet w3servlet = null;
    String requestUri = null;
    String contextPath = null;
    String servletPath = null;
    String pathInfo = null;
    String queryString = null;
    String requestPath = null;
    boolean named = false;

    public W3RequestDispatcher(W3Context w3context, W3Servlet w3servlet,
                               String contextPath, String servletPath,
                               String pathInfo, String queryString, String path,
                               boolean named, boolean relative) {
        this.w3context = w3context;
        this.w3servlet = w3servlet;
        this.contextPath = contextPath;
        this.servletPath = servletPath;
        this.pathInfo = pathInfo;
        this.queryString = queryString;
        this.named = named;
        requestPath = null;
        if (servletPath != null) {
            requestPath = servletPath;
            if (pathInfo != null) requestPath += pathInfo;
        }
        if (contextPath != null) {
            requestUri = contextPath;
            if (path != null) requestUri += path;
        }
        if (w3context.w3server.verbose)
            w3context.log((relative ? "relative " : "") + "requestDispatcher=" + this);
    }

    public void invokeServlet(ServletRequest request, ServletResponse response, final DispatcherType dispatcherType)
        throws ServletException, IOException {
        final W3Request w3request;
        if (request instanceof W3Request)
            w3request = (W3Request)request;
        else w3request = (W3Request)request.getAttribute("");
        if (w3servlet.isJsp) w3request.jsp = true;
        if (named) w3request.named = true;
        if (w3servlet.servlet == null)
            try {
                w3context.loadServlet(w3servlet);
            } catch (ClassNotFoundException ex) {
                throw new ServletException(ex);
            } catch (IllegalAccessException ex) {
                throw new ServletException(ex);
            } catch (InstantiationException ex) {
                throw new ServletException(ex);
            }
        HttpServletRequest facadeHttpRequest = w3request.w3requestFacade != null ? w3request.w3requestFacade.request : null;
        HttpServletResponse facadeHttpResponse = w3request.w3responseFacade != null ? w3request.w3responseFacade.response : null;
        if (dispatcherType != DispatcherType.REQUEST) w3request.invokeNumber++;
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(w3context.servletClassLoader);
            if (w3servlet.unavailable) {
                w3servlet.servlet.init(w3servlet);
                w3servlet.unavailable = false;
            }
            if (request instanceof W3Request) {
                w3request.w3requestFacade = new W3RequestFacade((HttpServletRequest)request);
                w3request.w3responseFacade = new W3ResponseFacade((HttpServletResponse)response);
                request = w3request.w3requestFacade;
                response = w3request.w3responseFacade;
                request.setAttribute("org.apache.catalina.jsp_file", w3servlet.jspFile != null ? w3servlet.jspFile : null);
            } else {
                final HttpServletRequest httpRequest
                    = new W3RequestFacade(w3request.w3requestFacade.request) {
                            Map<String, Object> attributes = new HashMap<String, Object>();
                            Map<String, String[]> parameterMap = null;
                            Vector<String> queryKeys = new Vector<String>();
                            String requestUri;
                            String contextPath;
                            String servletPath;
                            String pathInfo;
                            String pathTranslated;
                            String queryString;
                            //String remoteUser = null;
                            //Principal userPrincipal = null;
                            W3Session session = null;
                            boolean useSuper;
                            {
                                if (!named) {
                                    switch (dispatcherType) {
                                    case INCLUDE:
                                        attributes.put("javax.servlet.include.request_uri", W3RequestDispatcher.this.requestUri);
                                        attributes.put("javax.servlet.include.context_path", W3RequestDispatcher.this.contextPath);
                                        attributes.put("javax.servlet.include.servlet_path", W3RequestDispatcher.this.servletPath);
                                        attributes.put("javax.servlet.include.path_info", W3RequestDispatcher.this.pathInfo);
                                        attributes.put("javax.servlet.include.query_string", W3RequestDispatcher.this.queryString);
                                        break;
                                    case FORWARD:
                                        attributes.put("javax.servlet.forward.request_uri", w3request.getRequestURI());
                                        attributes.put("javax.servlet.forward.context_path", w3request.getContextPath());
                                        attributes.put("javax.servlet.forward.servlet_path", w3request.getServletPath());
                                        attributes.put("javax.servlet.forward.path_info", w3request.getPathInfo());
                                        attributes.put("javax.servlet.forward.query_string", w3request.getQueryString());
                                        break;
                                    }
                                }
                                attributes.put("org.apache.catalina.jsp_file", w3servlet.jspFile != null ? w3servlet.jspFile : null);
                                queryString = super.getQueryString() != null ? W3RequestDispatcher.this.queryString != null ? W3RequestDispatcher.this.queryString + "&" + super.getQueryString() : super.getQueryString() : W3RequestDispatcher.this.queryString;
                                if (dispatcherType != DispatcherType.INCLUDE && !named) {
                                    requestUri = W3RequestDispatcher.this.requestUri;
                                    contextPath = W3RequestDispatcher.this.contextPath;
                                    servletPath = W3RequestDispatcher.this.servletPath;
                                    pathInfo = W3RequestDispatcher.this.pathInfo;
                                    pathTranslated = pathInfo != null ? w3context.getRealPath(pathInfo) : null;
                                    useSuper = false;
                                } else useSuper = true;
                            }

                            public Object getAttribute(String name) {
                                if (attributes.containsKey(name)) return attributes.get(name);
                                return w3request.getAttribute(name);
                            }

                            public Enumeration getAttributeNames() {
                                return new EnumerationSequence(new Enumeration[] {w3request.getAttributeNames(),
                                                                                  new IteratorEnumeration<String>(attributes.keySet().iterator())});
                            }

                            public String getParameter(String name) {
                                return W3Request.getParameterValue(getParameterValues(name));
                            }

                            public Map<String, String[]> getParameterMap() {
                                if (parameterMap == null)
                                    if (W3RequestDispatcher.this.queryString != null) {
                                        parameterMap = new HashMap<String, String[]>();
                                        HashMap<String,Object> queryValues = new HashMap<String,Object>();
                                        try {
                                            W3URLConnection.parseQuery(W3RequestDispatcher.this.queryString, queryValues, queryKeys);
                                        } catch (IOException ex) {
                                            throw new RuntimeException(ex);
                                        }
                                        Enumeration parameterNames = super.getParameterNames();
                                        while (parameterNames.hasMoreElements()) {
                                            String name = (String)parameterNames.nextElement(),
                                                parameterValues[] = super.getParameterValues(name);
                                            if (parameterValues == null || parameterValues.length == 0) continue;
                                            Vector<String> vector = null;
                                            Object value = queryValues.get(name);
                                            if (value != null) {
                                                if (value instanceof String) {
                                                    vector = new Vector<String>();
                                                    vector.addElement((String)value);
                                                } else vector = (Vector<String>)value;
                                            } else {
                                                queryKeys.addElement(name);
                                                if (parameterValues.length == 1) {
                                                    queryValues.put(name, parameterValues[0]);
                                                    continue;
                                                }
                                                vector = new Vector<String>();
                                            }
                                            for (int index = 0; index < parameterValues.length; index++)
                                                vector.addElement(parameterValues[index]);
                                            queryValues.put(name, vector);
                                        }
                                        W3Request.setParameters(parameterMap, queryValues);
                                    } else {
                                        parameterMap = (Map<String, String[]>)super.getParameterMap();
                                        Enumeration<String> parameterNames = (Enumeration<String>)super.getParameterNames();
                                        while (parameterNames.hasMoreElements())
                                            queryKeys.addElement(parameterNames.nextElement());
                                    }
                                return parameterMap;
                            }

                            public Enumeration<String> getParameterNames() {
                                getParameterMap();
                                return queryKeys.elements();
                            }

                            public String[] getParameterValues(String name) {
                                getParameterMap();
                                return parameterMap.get(name);
                            }

                            public RequestDispatcher getRequestDispatcher(String path) {
                                return w3context.getRequestDispatcher(W3RequestDispatcher.this.requestUri, path);
                            }

                            public String getRealPath(String path) {
                                return useSuper ? super.getRealPath(path) : w3context.getRealPath(path);
                            }

                            public String getContextPath() {
                                return useSuper ? super.getContextPath() : contextPath;
                            }

                            public String getPathInfo() {
                                return useSuper ? super.getPathInfo() : pathInfo;
                            }

                            public String getPathTranslated() {
                                return useSuper ? super.getPathTranslated() : pathTranslated;
                            }

                            public String getQueryString() {
                                return queryString;
                            }

                            /*
                            public String getRemoteUser() {
                                if (remoteUser == null) getUserPrincipal();
                                return remoteUser;
                            }
                            */

                            public String getRequestURI() {
                                return useSuper ? super.getRequestURI() : requestUri;
                            }

                            public StringBuffer getRequestURL() {
                                return new StringBuffer(getScheme() + "://" + getServerName() + ":" + getServerPort() + getRequestURI());
                            }

                            public String getServletPath() {
                                return useSuper ? super.getServletPath() : servletPath;
                            }

                            public HttpSession getSession() {
                                return useSuper ? super.getSession() : getSession(true);
                            }

                            public HttpSession getSession(boolean create) {
                                if (useSuper) return super.getSession(create);
                                if (!create && session != null) return session;
                                session = w3context.getSession(w3request, create);
                                return session;
                            }

                            /*
                            public Principal getUserPrincipal() {
                                if (useSuper) return super.getUserPrincipal();
                                userPrincipal = w3context.getUserPrincipal(w3request);
                                if (userPrincipal != null) remoteUser = userPrincipal.getName();
                                return userPrincipal;
                            }
                            */

                            public boolean isRequestedSessionIdFromCookie() {
                                if (useSuper) return super.isRequestedSessionIdFromCookie();
                                return false;
                            }

                            public boolean isRequestedSessionIdValid() {
                                if (useSuper) return super.isRequestedSessionIdValid();
                                return session != null && !session.invalidated && !session.isNew;
                            }

                            public ServletContext getServletContext() {
                                return w3context;
                            }

                            public DispatcherType getDispatcherType() {
                                return dispatcherType;
                            }

                        };
                final HttpServletResponse httpResponse
                    = new W3ResponseFacade(w3request.w3responseFacade.response) {

                            public void flushBuffer() throws IOException {
                                if (dispatcherType != DispatcherType.INCLUDE) super.flushBuffer();
                            }

                            public void reset() {
                                if (dispatcherType != DispatcherType.INCLUDE) super.reset();
                            }

                            public void resetBuffer() {
                                if (dispatcherType != DispatcherType.INCLUDE) super.resetBuffer();
                            }

                            public void setBufferSize(int size) {
                                if (dispatcherType != DispatcherType.INCLUDE) super.setBufferSize(size);
                            }

                            public void setCharacterEncoding(String characterEncoding) {
                                if (dispatcherType != DispatcherType.INCLUDE) super.setCharacterEncoding(characterEncoding);
                            }

                            public void setContentLength(int len) {
                                if (dispatcherType != DispatcherType.INCLUDE) super.setContentLength(len);
                            }

                            public void setContentType(String ct) {
                                if (dispatcherType != DispatcherType.INCLUDE) super.setContentType(ct);
                            }

                            public void setLocale(Locale locale) {
                                if (dispatcherType != DispatcherType.INCLUDE) super.setLocale(locale);
                            }

                            public void addCookie(Cookie cookie) {
                                if (dispatcherType != DispatcherType.INCLUDE) super.addCookie(cookie);
                            }

                            public void addDateHeader(String name, long date) {
                                if (dispatcherType != DispatcherType.INCLUDE) super.addDateHeader(name, date);
                            }

                            public void addHeader(String name, String value) {
                                if (dispatcherType != DispatcherType.INCLUDE) super.addHeader(name, value);
                            }

                            public void addIntHeader(String name, int value) {
                                if (dispatcherType != DispatcherType.INCLUDE) super.addIntHeader(name, value);
                            }

                            public void sendError(int sc, String msg) throws IOException {
                                if (dispatcherType != DispatcherType.INCLUDE) w3context.sendError(w3servlet, httpRequest, this, sc, msg);
                            }

                            public void sendRedirect(String location) throws IOException {
                                if (dispatcherType != DispatcherType.INCLUDE) w3context.sendRedirect(httpRequest, this, location);
                                else super.sendRedirect(location);
                            }

                            public void setDateHeader(String name, long date) {
                                if (dispatcherType != DispatcherType.INCLUDE) super.setDateHeader(name, date);
                            }

                            public void setHeader(String name, String value) {
                                if (dispatcherType != DispatcherType.INCLUDE) super.setHeader(name, value);
                            }

                            public void setIntHeader(String name, int value) {
                                if (dispatcherType != DispatcherType.INCLUDE) super.setIntHeader(name, value);
                            }

                            public void setStatus(int sc) {
                                if (dispatcherType != DispatcherType.INCLUDE) super.setStatus(sc);
                            }

                            public void setStatus(int sc, String sm) {
                                if (dispatcherType != DispatcherType.INCLUDE) super.setStatus(sc, sm);
                            }


                        };
                w3request.w3requestFacade.request = httpRequest;
                w3request.w3responseFacade.response = httpResponse;
            }
            List<FilterTarget> filterTargetList = new ArrayList<FilterTarget>();
            Iterator filterTargetIter = w3context.filterTargetList.iterator();
            while (filterTargetIter.hasNext()) {
                FilterTarget filterTarget = (FilterTarget)filterTargetIter.next();
                if (filterTarget.pattern == null || filterTarget.pattern.matches(requestPath) || !requestPath.endsWith("/") && filterTarget.pattern.matches(requestPath + "/"))
                    filterTargetList.add(filterTarget);
            }
            if (!filterTargetList.isEmpty() ||
                !w3servlet.filterTargetList.isEmpty()) {
                W3FilterChain w3filterChain = new W3FilterChain(filterTargetList, w3servlet.filterTargetList, w3servlet);
                w3filterChain.w3request = w3request;
                w3filterChain.dispatcherType = dispatcherType;
                w3filterChain.named = named;
                w3request.filter = true;
                w3filterChain.doFilter(request, response);
            } else w3servlet.service(request, response, dispatcherType, named, false);
            //if (dispatcherType != DispatcherType.INCLUDE) response.flushBuffer();
        } finally {
            Thread.currentThread().setContextClassLoader(contextClassLoader);
            if (dispatcherType != DispatcherType.REQUEST) w3request.invokeNumber--;
            if (facadeHttpRequest != null)
                w3request.w3requestFacade.request = facadeHttpRequest;
            if (facadeHttpResponse != null)
                w3request.w3responseFacade.response = facadeHttpResponse;
            w3request.jsp = false;
        }
    }

    /** Forwards a request from a servlet to another resource (servlet, JSP file, or
        HTML file) on the server. This method allows one servlet to do preliminary
        processing of a request and another resource to generate the response.
        For a RequestDispatcher obtained via getRequestDispatcher(), the
        ServletRequest object has its path elements and parameters adjusted to
        match the path of the target resource.
        forward should be called before the response has been committed to the client
        (before response body output has been flushed). If the response already
        has been committed, this method throws an IllegalStateException.
        Uncommitted output in the response buffer is automatically cleared before
        the forward.
        The request and response parameters must be either the same objects as were
        passed to the calling servlet's service method or be subclasses of the
        ServletRequestWrapper or ServletResponseWrapper classes that wrap
        them.

        @param request a ServletRequest object that represents the request the client makes of the servlet
        @param response a ServletResponse object that represents the response the servlet returns to the client
        @throws ServletException if the target resource throws this exception
        @throws IOException if the target resource throws this exception
        @throws IllegalStateException if the response was already committed

    */
    public void forward(ServletRequest request, ServletResponse response)
        throws ServletException, IOException {
        forward(request, response, DispatcherType.FORWARD);
    }

    public void forward(ServletRequest request, ServletResponse response, DispatcherType dispatcher)
        throws ServletException, IOException {
        if (response.isCommitted()) throw new IllegalStateException();
        response.resetBuffer();
        invokeServlet(request, response, dispatcher);
    }

    /** Includes the content of a resource (servlet, JSP page, HTML file) in the
        response. In essence, this method enables programmatic server-side includes.
        The ServletResponse object has its path elements and parameters remain
        unchanged from the caller's. The included servlet cannot change the response
        status code or set headers; any attempt to make a change is ignored.
        The request and response parameters must be either the same objects as were
        passed to the calling servlet's service method or be subclasses of the
        ServletRequestWrapper or ServletResponseWrapper classes that wrap
        them.

        @param request a ServletRequest object that contains the client's request
        @param response a ServletResponse object that contains the servlet's response

        @throws ServletException if the included resource throws this exception
        @throws IOException if the included resource throws this exception

    */
    public void include(ServletRequest request, ServletResponse response)
        throws ServletException, IOException {
        invokeServlet(request, response, DispatcherType.INCLUDE);
    }

    public String toString() {
        return getClass().getName() +
            " {contextName=" + w3context.w3contextName +
            ", servlet=" + w3servlet +
            ", requestUri=" + requestUri +
            ", contextPath=" + contextPath +
            ", servletPath=" + servletPath +
            ", pathInfo=" + pathInfo +
            ", queryString=" + queryString +
            ", named=" + named + "}";
    }

}
