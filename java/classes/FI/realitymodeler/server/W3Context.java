
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.lang.reflect.*;
import java.net.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.jar.JarFile;
import java.util.jar.JarEntry;
import javax.mail.*;
import javax.naming.*;
import javax.naming.directory.DirContext;
import javax.naming.spi.*;
import javax.servlet.*;
import javax.servlet.descriptor.JspConfigDescriptor;
import javax.servlet.http.*;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.SAXParser;
import org.apache.juli.logging.Log;
import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;

class LoadOnStartupComparator implements Comparator<W3Servlet> {

    public int compare(W3Servlet o1, W3Servlet o2) {
        int delta = o1.loadOnStartup - o2.loadOnStartup;
        return delta != 0 ? delta : o1.servletName.compareTo(o2.servletName);
    }

    public boolean equals(Object obj) {
        return obj instanceof LoadOnStartupComparator;
    }

}

class AuthTarget extends ServletTarget {
    Set<String> httpMethods = null;

    AuthTarget(W3Servlet authServlet, String urlPattern, Set<String> httpMethods) {
        super(authServlet, urlPattern);
        this.httpMethods = httpMethods;
    }

    public Object found(String remainder) {
        AuthTarget authTarget = (AuthTarget)clone();
        authTarget.remainder = remainder;
        return authTarget;
    }

    public Object clone() {
        return new AuthTarget(w3servlet, urlPattern, httpMethods);
    }

    public String toString() {
        return getClass().getName() +
            "{servletClass=" + (w3servlet != null ? w3servlet.className : "<none>") +
            ",urlPattern=" + urlPattern +
            ",httpMethods=" + httpMethods + "}";
    }
}

class ErrorPage {
    Class<?> exceptionType;
    String location;
}

class Taglib {
    String uri;
    String location;
}

class JspPropertyGroup {
}

class Realm {
    String className = null;
    String appName = null;
    String userClassNames = null;
    String roleClassNames = null;
    boolean useContextClassLoader = true;
}

class Valve {
    Hashtable<String, String> attributes = new Hashtable<String, String>();
}

class SecurityRole {
    String description;
    String name;
}

class AuthConstraint {
    String description;
    HashSet<String> roleNames = new HashSet<String>();
}

class UserDataConstraint {
    String description;
    String transportGuarantee;
}

class SecurityConstraint {
    String displayName = null;
    List<WebResourceCollection> webResourceCollections = new ArrayList<WebResourceCollection>();
    AuthConstraint authConstraint = null;
    UserDataConstraint userDataConstraint = null;
}

class WebResourceCollection {
    String webResourceName;
    String description;
    List<String> urlPatterns = new ArrayList<String>();
    Set<String> httpMethods = new HashSet<String>();
}

class FileComparator implements Comparator<File> {

    public int compare(File o1, File o2) {
        return o1.getPath().compareTo(o2.getPath());
    }

    public boolean equals(Object obj) {
        return obj instanceof FileComparator;
    }

}

/** Defines a set of methods that a servlet uses to communicate with its servlet container,
    for example, to get the MIME type of a file, dispatch requests, or write to a
    log file.
    There is one context per 'web application' per Java Virtual Machine. (A 'web
    application' is a collection of servlets and content installed under a specific sub-set
    of the server's URL namespace such as /catalog and possibly installed via a
    .war file.)
    In the case of a web application marked 'distributed' in its deployment descriptor,
    there will be one context instance for each virtual machine. In this situation,
    the context cannot be used as a location to share global information (because the
    information won't be truly global). Use an external resource like a database
    instead.
    The ServletContext object is contained within the ServletConfig object,
    which the Web server provides the servlet when the servlet is initialized.

    @see javax.servlet.Servlet#getServletConfig()
    @see javax.servlet.ServletConfig#getServletContext()

*/
public class W3Context implements ServletContext, HttpSessionContext, Runnable, Comparator<String>, FileNameMap, FileFilter {
    static String comments[][] = { {
            "Continue",
            "Switching Protocols"
        }, {
            "OK",
            "Created",
            "Accepted",
            "Non-Authorative Information",
            "No Content",
            "Reset Content",
            "Partial Content"
        }, {
            "Multiple Choices",
            "Moved Permanently",
            "Moved Temporarily",
            "See Other",
            "Not Modified",
            "Use Proxy"
        }, {
            "Bad Request",
            "Unauthorized",
            "Payment Required",
            "Forbidden",
            "Not Found",
            "Method Not Allowed",
            "Not Acceptable",
            "Proxy Authentication Required",
            "Request Timeout",
            "Conflict",
            "Gone",
            "Length Required",
            "Precondition Failed",
            "Request Entity Too Large",
            "Request-URI Too Long",
            "Unsupported Media Type",
            "Requested Range Not Satisfiable",
            "Expectation Failed"
        }, {
            "Internal Server Error",
            "Not Implemented",
            "Bad Gateway",
            "Service Unavailable",
            "Gateway Timeout",
            "HTTP Version Not Supported"
        }};
    static FileComparator fileComparator = new FileComparator();
    static Map<String,W3Context> w3contexts = new TreeMap<String,W3Context>();
    static Random random = new Random(System.currentTimeMillis());
    static W3Context defaultW3context = null;
    static Vector emptyVector = new Vector();
    static File noFiles[] = new File[0];
    static boolean debug = false;
    static boolean useNetworkServlet = false;
    static boolean namingSet = false;
    static long sessionNumber = 0L;

    javax.naming.Context naming = null;
    File webappsDir = null;
    File w3contextDir = null;
    List<ErrorPage> errorPagesByExceptionType = new ArrayList<ErrorPage>();
    List<FilterTarget> filterTargetList = new ArrayList<FilterTarget>();
    List<W3Servlet> servletList = new ArrayList<W3Servlet>();
    List<Taglib> taglibs = new ArrayList<Taglib>();
    Map<Integer, String> errorPagesByErrorCode = new HashMap<Integer,String>();
    Map<String, W3Filter> filters = new HashMap<String,W3Filter>();
    Map<String, Vector<String>> mimeTypes = new HashMap<String,Vector<String>>();
    Map<String, String> securityRoleRefs = new HashMap<String, String>();
    Map<String, W3Servlet> servlets = new HashMap<String,W3Servlet>();
    Map<String, W3FilterRegistration> filterRegistrations = new HashMap<String, W3FilterRegistration>();
    Set<HttpSessionActivationListener> httpSessionActivationListeners = new HashSet<HttpSessionActivationListener>();
    Set<String> roleNames = new HashSet<String>();
    Set<SecurityRole> securityRoles = new HashSet<SecurityRole>();
    Set<W3Servlet> servletSet = new TreeSet<W3Servlet>(new LoadOnStartupComparator());
    Hashtable<String,Object> attributes = new Hashtable<String,Object>();
    Hashtable<String, String> contextInitParameters = new Hashtable<String, String>();
    Hashtable<String, String> jspServletInitParameters = new Hashtable<String, String>();
    Hashtable<String,W3Session> sessions = new Hashtable<String,W3Session>();
    RegexpPool authPool = new RegexpPool();
    RegexpPool pathPool = new RegexpPool();
    RegexpPool extensionPool = new RegexpPool();
    URLClassLoader servletClassLoader = null;
    URLClassLoader sharedClassLoader = null;
    URL extraClassLoaderUrls[] = null;
    String classPath = null;
    String sharedClassPath = null;
    String authMethod = null;
    String formLoginPage = null;
    String formErrorPage = null;
    String description = null;
    String displayName = null;
    String realmName = null;
    String contextPath = null;
    String w3contextName = null;
    Thread sessionCleaningThread = null;
    Map<String, W3Filter> filterMap = new TreeMap<String,W3Filter>();
    Map<String, String> mimeTypeMap = new TreeMap<String, String>();
    Map<String, W3Servlet> servletPatternMap = new TreeMap<String,W3Servlet>(this);
    List<HttpSessionListener> httpSessionListeners = new ArrayList<HttpSessionListener>();
    List<HttpSessionAttributeListener> httpSessionAttributeListeners = new ArrayList<HttpSessionAttributeListener>();
    List<SecurityConstraint> securityConstraints = new ArrayList<SecurityConstraint>();
    List<ServletContextListener> servletContextListeners = new ArrayList<ServletContextListener>();
    List<ServletContextAttributeListener> servletContextAttributeListeners = new ArrayList<ServletContextAttributeListener>();
    List<ServletRequestListener> servletRequestListeners = new ArrayList<ServletRequestListener>();
    List<ServletRequestAttributeListener> servletRequestAttributeListeners = new ArrayList<ServletRequestAttributeListener>();
    List<ServletContextListener> tldServletContextListeners = new ArrayList<ServletContextListener>();
    List<Valve> valves = new ArrayList<Valve>();
    List<String> welcomeFileList = new ArrayList<String>();
    Class<?> connectorClass = null;
    Class<?> containerClass = null;
    Class<?> contextClass = null;
    Object connectorObject = null;
    Object contextObject = null;
    Object preparationObject = new Object();
    Object realmObject = null;
    Method authenticateMethod = null;
    Method invokeMethod = null;
    Realm realm = null;
    W3Servlet authServlet = null;
    W3Server w3server = null;
    W3Servlet defaultServlet = null;
    W3Servlet jspServlet = null;
    AuthTarget defaultAuthTarget = null;
    int maxInactiveInterval = 120;
    long maxInactiveIntervalInMillis = (long)maxInactiveInterval * 1000L;
    long preparationTime = 0L;

    static File[] sort(File files[]) {
        Arrays.sort(files, W3Context.fileComparator);
        return files;
    }

    /** XML-file handler which reads subset of web.xml files described
        in Servlet Specification 2.4.

    */
    class W3Handler extends DefaultHandler {
        private AuthConstraint authConstraint = null;
        private Hashtable<String, String> initParameters = null;
        private Locator locator = null;
        private SecurityConstraint securityConstraint = null;
        private SecurityRole securityRole = null;
        private String className = null;
        private String errorCode = null;
        private String exceptionType = null;
        private String extension = null;
        private String filterName = null;
        private String jspFile = null;
        private String listenerClass = null;
        private String location = null;
        private String mimeType = null;
        private String paramName = null;
        private String paramValue = null;
        private String roleLink = null;
        private String roleName = null;
        private String servletName = null;
        private String taglibLocation = null;
        private String taglibUri = null;
        private List<String> servletNames = null;
        private List<String> urlPatterns = null;
        private JspPropertyGroup jspPropertyGroup = null;
        private UserDataConstraint userDataConstraint = null;
        private WebResourceCollection webResourceCollection = null;
        private StringBuffer valueBuffer = new StringBuffer();
        private boolean contextXml = false, tld = false;
        private EnumSet<DispatcherType> dispatcherTypes = null;
        private int loadOnStartup = Integer.MIN_VALUE;

        Map<String, Reference> resourceReferences = new HashMap<String, Reference>();

        public W3Handler(boolean contextXml, boolean tld) {
            this.contextXml = contextXml;
            this.tld = tld;
        }

        public InputSource resolveEntity(String publicId, String systemId) {
            return new InputSource(new ByteArrayInputStream(new byte[0]));
        }

        public void setDocumentLocator(Locator locator) {
            this.locator = locator;
        }

        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            if (contextXml) {
                try {
                    if (qName.equals("Realm")) {
                        realm = new Realm();
                        realm.className = attributes.getValue("className");
                        realm.appName = attributes.getValue("appName");
                        realm.userClassNames = attributes.getValue("userClassNames");
                        realm.roleClassNames = attributes.getValue("roleClassNames");
                        String value = attributes.getValue("useContextClassLoader");
                        realm.useContextClassLoader = value != null ? new Boolean(value) : false;
                    } else if (qName.equals("Resource")) {
                        String type = attributes.getValue("type"),
                            factory = attributes.getValue("factory"),
                            name = attributes.getValue("name");
                        if (type != null)
                            if (type.equals("javax.sql.DataSource")) {
                                if (factory == null) factory = "org.apache.tomcat.dbcp.dbcp.BasicDataSourceFactory";
                                Reference reference = new Reference(type, factory, null);
                                int length = attributes.getLength();
                                for (int index = 0; index < length; index++)
                                    reference.add(new StringRefAddr(attributes.getLocalName(index), attributes.getValue(index)));
                                naming.rebind("java:comp/env/" + name, reference);
                            } else if (type.equals("javax.mail.Session")) {
                                Properties properties = new Properties();
                                int length = attributes.getLength();
                                for (int index = 0; index < length; index++)
                                    if (attributes.getLocalName(index).startsWith("mail.")) properties.put(attributes.getLocalName(index), attributes.getValue(index));
                                Session session = Session.getInstance(properties,  null);
                                naming.rebind(name, session);
                            } else if (factory != null) {
                                Reference reference = new Reference(type, factory, null);
                                int length = attributes.getLength();
                                for (int index = 0; index < length; index++)
                                    reference.add(new StringRefAddr(attributes.getLocalName(index), attributes.getValue(index)));
                                resourceReferences.put(name, reference);
                            }
                    } else if (qName.equals("Valve")) {
                        Valve valve = new Valve();
                        int length = attributes.getLength();
                        for (int index = 0; index < length; index++)
                            valve.attributes.put(attributes.getLocalName(index), attributes.getValue(index));
                        valves.add(valve);
                    }
                } catch (Exception ex) {
                    throw new SAXParseException(ex.toString(), locator, ex);
                }
            } else if (qName.equals("listener")) listenerClass = null;
            else if (!tld)
                if (qName.equals("servlet")) {
                    className = servletName = jspFile = null;
                    initParameters = new Hashtable<String, String>();
                    loadOnStartup = Integer.MIN_VALUE;
                } else if (qName.equals("filter")) {
                    className = filterName = null;
                    initParameters = new Hashtable<String, String>();
                    loadOnStartup = Integer.MIN_VALUE;
                } else if (qName.equals("init-param") ||
                           qName.equals("context-param"))
                    paramName = paramValue = null;
                else if (qName.equals("servlet-mapping")) {
                    urlPatterns = new ArrayList<String>();
                } else if (qName.equals("filter-mapping")) {
                    filterName = null;
                    servletNames = new ArrayList<String>();
                    urlPatterns = new ArrayList<String>();
                    dispatcherTypes = null;
                } else if (qName.equals("mime-mapping"))
                    extension = mimeType = null;
                else if (qName.equals("error-page"))
                    errorCode = exceptionType = location = null;
                else if (qName.equals("taglib"))
                    taglibUri = taglibLocation = null;
                else if (qName.equals("jsp-property-group"))
                    jspPropertyGroup = new JspPropertyGroup();
                else if (qName.equals("security-role"))
                    securityRole = new SecurityRole();
                else if (qName.equals("security-role-ref"))
                    roleName = roleLink = null;
                else if (qName.equals("web-resource-collection"))
                    webResourceCollection = new WebResourceCollection();
                else if (qName.equals("auth-constraint"))
                    authConstraint = new AuthConstraint();
                else if (qName.equals("user-data-constraint"))
                    userDataConstraint = new UserDataConstraint();
                else if (qName.equals("security-constraint"))
                    securityConstraint = new SecurityConstraint();
                else if (qName.equals("login-config"))
                    authMethod = realmName = formLoginPage = formErrorPage = null;
        }

        public void endElement(String uri, String localName, String qName) throws SAXException {
            try {
                if (contextXml) return;
                String value = valueBuffer.toString().trim();
                if (qName.equals("listener-class")) listenerClass = value;
                else if (qName.equals("listener")) {
                    Object listener = null;
                    ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
                    try {
                        Thread.currentThread().setContextClassLoader(servletClassLoader);
                        listener = servletClassLoader.loadClass(listenerClass).newInstance();
                    } finally {
                        Thread.currentThread().setContextClassLoader(contextClassLoader);
                    }
                    if (!tld) {
                        if (listener instanceof ServletContextListener)
                            servletContextListeners.add((ServletContextListener)listener);
                        if (listener instanceof ServletContextAttributeListener)
                            servletContextAttributeListeners.add((ServletContextAttributeListener)listener);
                        if (listener instanceof ServletRequestListener)
                            servletRequestListeners.add((ServletRequestListener)listener);
                        if (listener instanceof ServletRequestAttributeListener)
                            servletRequestAttributeListeners.add((ServletRequestAttributeListener)listener);
                        if (listener instanceof HttpSessionListener)
                            httpSessionListeners.add((HttpSessionListener)listener);
                        if (listener instanceof HttpSessionAttributeListener)
                            httpSessionAttributeListeners.add((HttpSessionAttributeListener)listener);
                    } else tldServletContextListeners.add((ServletContextListener)listener);
                } else if (!tld)
                    if (qName.equals("servlet")) {
                        if (servletName == null) servletName = className;
                        W3Servlet w3servlet = checkW3Servlet(servletName);
                        if (jspFile != null && className == null) {
                            className = "org.apache.jasper.servlet.JspServlet";
                            initParameters.putAll(W3Context.defaultW3context.jspServletInitParameters);
                        }
                        if (w3servlet == null) {
                            w3servlet = new W3Servlet(W3Context.this, servletName, className, initParameters, loadOnStartup);
                            setServlet(w3servlet);
                        } else {
                            w3servlet.className = className;
                            w3servlet.initParameters = initParameters;
                        }
                        if (jspFile != null) {
                            w3servlet.jspFile = jspFile;
                            w3servlet.isJsp = true;
                        }
                        initParameters = null;
                    } else if (qName.equals("filter")) {
                        if (filterName == null) filterName = className;
                        W3Filter w3filter = new W3Filter(W3Context.this, null, null, filterName, className, initParameters);
                        setFilter(w3filter);
                        initParameters = null;
                    } else if (qName.equals("init-param")) {
                        if (initParameters != null)
                            initParameters.put(paramName, paramValue != null ? paramValue : "");
                    } else if (qName.equals("param-name")) paramName = value;
                    else if (qName.equals("param-value")) paramValue = value;
                    else if (qName.equals("servlet-name"))
                        if (servletNames != null) servletNames.add(value);
                        else servletName = value;
                    else if (qName.equals("filter-name")) filterName = value;
                    else if (qName.equals("servlet-class")
                             || qName.equals("filter-class")) className = value;
                    else if (qName.equals("jsp-file")) jspFile = value;
                    else if (qName.equals("load-on-startup")) loadOnStartup = Integer.parseInt(value);
                    else if (qName.equals("url-pattern")) {
                        if (webResourceCollection != null) webResourceCollection.urlPatterns.add(value);
                        else if (urlPatterns != null) urlPatterns.add(value);
                    } else if (qName.equals("extension")) extension = value;
                    else if (qName.equals("mime-type")) mimeType = value;
                    else if (qName.equals("display-name")) {
                        if (initParameters == null)
                            displayName = value;
                    } else if (qName.equals("servlet-mapping")) {
                        W3Servlet w3servlet = getW3Servlet(servletName);
                        for (String urlPattern : urlPatterns)
                            servletPatternMap.put(urlPattern, w3servlet);
                        urlPatterns = null;
                    } else if (qName.equals("dispatcher")) {
                        value = value.toUpperCase();
                        if (dispatcherTypes == null)
                            dispatcherTypes = EnumSet.noneOf(DispatcherType.class);
                        dispatcherTypes.add(DispatcherType.valueOf(value));
                    } else if (qName.equals("filter-mapping")) {
                        W3Filter w3filter = filters.get(filterName);
                        if (w3filter == null) throw new RuntimeException("Filter " + filterName + " not found");
                        addMappingForServletNames(w3filter, null, dispatcherTypes, true, servletNames);
                        addMappingForUrlPatterns(w3filter, null, dispatcherTypes, true, urlPatterns);
                        servletNames = urlPatterns = null;
                    } else if (qName.equals("mime-mapping")) {
                        Vector<String> exts = mimeTypes.get(mimeType);
                        if (exts == null) exts = new Vector<String>();
                        exts.add(extension);
                        mimeTypes.put(mimeType, exts);
                        mimeTypeMap.put(extension, mimeType);
                    } else if (qName.equals("context-param"))
                        contextInitParameters.put(paramName, paramValue != null ? paramValue : "");
                    else if (qName.equals("session-timeout")) {
                        int sessionTimeout = Integer.parseInt(value);
                        if (sessionTimeout >= 0) {
                            maxInactiveInterval = sessionTimeout * 60;
                            maxInactiveIntervalInMillis = (long)maxInactiveInterval * 1000L;
                        } else {
                            maxInactiveInterval = -1;
                            maxInactiveIntervalInMillis = -1L;
                        }
                    } else if (qName.equals("welcome-file")) {
                        if (value.startsWith("/")) value = value.substring(1);
                        welcomeFileList.add(value);
                    } else if (qName.equals("error-code")) errorCode = value;
                    else if (qName.equals("exception-type")) exceptionType = value;
                    else if (qName.equals("location")) location = value;
                    else if (qName.equals("error-page")) {
                        if (location == null) throw new RuntimeException("location not specified in error-page");
                        if (exceptionType != null) {
                            Class<?> exceptionClass = null;
                            ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
                            try {
                                Thread.currentThread().setContextClassLoader(servletClassLoader);
                                exceptionClass = servletClassLoader.loadClass(exceptionType);
                            } finally {
                                Thread.currentThread().setContextClassLoader(contextClassLoader);
                            }
                            int i;
                            for (i = 0; i < errorPagesByExceptionType.size(); i++) {
                                ErrorPage errorPage = errorPagesByExceptionType.get(i);
                                if (errorPage.exceptionType.isAssignableFrom(exceptionClass)) break;
                            }
                            ErrorPage errorPage = new ErrorPage();
                            errorPage.exceptionType = exceptionClass;
                            errorPage.location = location;
                            errorPagesByExceptionType.add(i, errorPage);
                        } else errorPagesByErrorCode.put(new Integer(errorCode), location);
                    } else if (qName.equals("taglib-uri"))
                        taglibUri = value;
                    else if (qName.equals("taglib-location"))
                        taglibLocation = value;
                    else if (qName.equals("taglib")) {
                        Taglib taglib = new Taglib();
                        taglib.uri = taglibUri;
                        taglib.location = taglibLocation;
                        taglibs.add(taglib);
                    } else if (qName.equals("jsp-property-group")) {
                        jspPropertyGroup = null;
                    } else if (qName.equals("web-resource-name"))
                        webResourceCollection.webResourceName = value;
                    else if (qName.equals("description"))
                        if (securityRole != null) securityRole.description = value;
                        else if (authConstraint != null) authConstraint.description = value;
                        else if (userDataConstraint != null) userDataConstraint.description = value;
                        else if (webResourceCollection != null) webResourceCollection.description = value;
                        else description = value;
                    else if (qName.equals("http-method"))
                        webResourceCollection.httpMethods.add(value.toLowerCase());
                    else if (qName.equals("security-role")) {
                        securityRole.name = value;
                        securityRoles.add(securityRole);
                        securityRole = null;
                    } else if (qName.equals("security-role-ref")) {
                        if (!securityRoles.contains(roleLink)) throw new RuntimeException("Security role " + roleLink + " not found in " + qName);
                        securityRoleRefs.put(roleName, roleLink);
                    } else if (qName.equals("web-resource-collection")) {
                        securityConstraint.webResourceCollections.add(webResourceCollection);
                        webResourceCollection = null;
                    } else if (qName.equals("role-name"))
                        if (authConstraint != null) authConstraint.roleNames.add(value);
                        else roleNames.add(value);
                    else if (qName.equals("auth-constraint")) {
                        securityConstraint.authConstraint = authConstraint;
                        authConstraint = null;
                    } else if (qName.equals("transport-guarantee"))
                        userDataConstraint.transportGuarantee = value;
                    else if (qName.equals("user-data-constraint")) {
                        securityConstraint.userDataConstraint = userDataConstraint;
                        userDataConstraint = null;
                    } else if (qName.equals("security-constraint")) {
                        securityConstraints.add(securityConstraint);
                        securityConstraint = null;
                    } else if (qName.equals("auth-method")) authMethod = value;
                    else if (qName.equals("realm-name")) realmName = value;
                    else if (qName.equals("form-login-page")) formLoginPage = value;
                    else if (qName.equals("form-error-page")) formErrorPage = value;
                valueBuffer.setLength(0);
            } catch (Exception ex) {
                throw new SAXParseException(ex.toString(), locator, ex);
            }
        }

        public void characters(char[] ch, int start, int length) throws SAXException {
            valueBuffer.append(new String(ch, start, length));
        }

    }

    public W3Context(W3Server w3server) {
        this.w3server = w3server;
    }

    private void saveDeploymentDescriptor(PrintWriter writer) {
        writer.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        writer.println();
        writer.println("<!DOCTYPE web-app");
        writer.println("        PUBLIC \"-//Sun Microsystems, Inc.//DTD Web Application 2.3//EN\"");
        writer.println("        \"http://java.sun.com/j2ee/dtds/web-app_2_3.dtd\">");
        writer.println();
        writer.println("  <!-- Deployment Descriptor saved by " + getClass().getName() + " at " + new Date() + "-->");
        writer.println();
        writer.println("<web-app>");
        writer.println();
        writer.println("  <!-- Servlets -->");
        Iterator servletIter = servletList.iterator();
        while (servletIter.hasNext()) {
            W3Servlet w3servlet = (W3Servlet)servletIter.next();
            writer.println("  <servlet>");
            writer.println("    <servlet-name>" + w3servlet.getServletName() + "</servlet-name>");
            if (w3servlet.className != null) writer.println("    <servlet-class>" + w3servlet.className + "</servlet-class>");
            Iterator initParameterIter = w3servlet.initParameters.entrySet().iterator();
            while (initParameterIter.hasNext()) {
                Map.Entry initParameterEntry = (Map.Entry)initParameterIter.next();
                writer.println("    <init-param>");
                writer.println("      <param-name>" + initParameterEntry.getKey() + "<param-name>");
                writer.println("    <param-value>" + initParameterEntry.getValue() + "<param-value>");
                writer.println("    </init-param>");
            }
            if (w3servlet.loadOnStartup != Integer.MIN_VALUE)
                writer.println("    <load-on-startup>" + w3servlet.loadOnStartup + "</load-on-startup>");
            if (w3servlet.jspFile != null)
                writer.println("    <jsp-file>" + w3servlet.jspFile + "</jsp-file>");
            writer.println("  </servlet>");
            writer.println();
        }
        writer.println("  <!-- Servlet mappings -->");
        Iterator servletPatternIter = servletPatternMap.entrySet().iterator();
        while (servletPatternIter.hasNext()) {
            Map.Entry servletPatternEntry = (Map.Entry)servletPatternIter.next();
            writer.println("  <servlet-mapping>");
            writer.println("    <servlet-name>" + ((W3Servlet)servletPatternEntry.getValue()).getServletName() + "</servlet-name>");
            writer.println("    <url-pattern>" + servletPatternEntry.getKey() + "</url-pattern>");
            writer.println("  </servlet-mapping>");
            writer.println();
        }
        writer.println("  <!-- Filters -->");
        Iterator filterIter = filterMap.values().iterator();
        while (filterIter.hasNext()) {
            W3Filter w3filter = (W3Filter)filterIter.next();
            writer.println("  <filter>");
            writer.println("    <filter-name>" + w3filter.getFilterName() + "</filter-name>");
            writer.println("    <filter-class>" + w3filter.filter.getClass().getName() + "<filter-class>");
            Iterator initParameterIter = w3filter.initParameters.entrySet().iterator();
            while (initParameterIter.hasNext()) {
                Map.Entry initParameterEntry = (Map.Entry)initParameterIter.next();
                writer.println("    <init-param>");
                writer.println("      <param-name>"
                               + initParameterEntry.getKey() + "<param-name>");
                writer.println("      <param-value>"
                               + initParameterEntry.getValue() + "<param-value>");
                writer.println("    </init-param>");
            }
            writer.println("  </filter>");
            writer.println();
        }
        writer.println("  <!-- Filter mappings to URL patterns -->");
        Iterator filterTargetIter = filterTargetList.iterator();
        while (filterTargetIter.hasNext()) {
            FilterTarget filterTarget = (FilterTarget)filterTargetIter.next();
            writer.println("  <filter-mapping>");
            writer.println("    <filter-name>"
                           + filterTarget.w3filter.getFilterName()
                           + "</filter-name>");
            writer.println("    <url-pattern>"
                           + filterTarget.urlPattern
                           + "</url-pattern>");
            writer.println("  </filter-mapping>");
            writer.println();
        }
        writer.println("  <!-- Filter mappings to servlets -->");
        servletIter = servletList.iterator();
        while (servletIter.hasNext()) {
            W3Servlet w3servlet = (W3Servlet)servletIter.next();
            filterIter = w3servlet.filterTargetList.iterator();
            while (filterIter.hasNext()) {
                W3Filter w3filter = (W3Filter)filterIter.next();
                writer.println("  <filter-mapping>");
                writer.println("    <filter-name>"
                               + w3filter.getFilterName()
                               + "</filter-name>");
                writer.println("    <servlet-name>"
                               + w3servlet.getServletName()
                               + "</servlet-name>");
                writer.println("  </filter-mapping>");
                writer.println();
            }
        }
        writer.println("  <!-- Sessions configuration -->");
        writer.println("  <session-config>");
        writer.println("    <session-timeout>" + maxInactiveInterval + "</session-timeout>");
        writer.println("  </session-config>");
        writer.println();
        writer.println("  <!-- Mime types -->");
        Iterator mimeTypeMapIter = mimeTypeMap.entrySet().iterator();
        while (mimeTypeMapIter.hasNext()) {
            Map.Entry mimeTypeEntry = (Map.Entry)mimeTypeMapIter.next();
            writer.println("  <mime-mapping>");
            writer.println("    <extension>"
                           + mimeTypeEntry.getKey() + "</extension>");
            writer.println("    <mime-type>"
                           + mimeTypeEntry.getValue() + "</mime-type>");
            writer.println("  </mime-mapping>");
        }
        writer.println();
        writer.println("  <!-- Servlet context listeners -->");
        Iterator listenerIter = servletContextListeners.iterator();
        while (listenerIter.hasNext()) {
            writer.println("  <listener>");
            writer.println("    <listener-class>"
                           + listenerIter.next().getClass().getName()
                           + "</listener-class>");
            writer.println("  </listener>");
        }
        writer.println();
        writer.println("  <!-- Servlet context attributes listeners -->");
        listenerIter = servletContextAttributeListeners.iterator();
        while (listenerIter.hasNext()) {
            writer.println("  <listener>");
            writer.println("    <listener-class>"
                           + listenerIter.next().getClass().getName()
                           + "</listener-class>");
            writer.println("  </listener>");
        }
        writer.println();
        writer.println("  <!-- Servlet request listeners -->");
        listenerIter = servletRequestListeners.iterator();
        while (listenerIter.hasNext()) {
            writer.println("  <listener>");
            writer.println("    <listener-class>"
                           + listenerIter.next().getClass().getName()
                           + "</listener-class>");
            writer.println("  </listener>");
        }
        writer.println();
        writer.println("  <!-- Servlet request attributes listeners -->");
        listenerIter = servletRequestAttributeListeners.iterator();
        while (listenerIter.hasNext()) {
            writer.println("  <listener>");
            writer.println("    <listener-class>"
                           + listenerIter.next().getClass().getName()
                           + "</listener-class>");
            writer.println("  </listener>");
        }
        writer.println();
        writer.println("  <!-- Http session listeners -->");
        listenerIter = httpSessionListeners.iterator();
        while (listenerIter.hasNext()) {
            writer.println("  <listener>");
            writer.println("    <listener-class>"
                           + listenerIter.next().getClass().getName()
                           + "</listener-class>");
            writer.println("  </listener>");
        }
        writer.println();
        writer.println("  <!-- Http session attributes listeners -->");
        listenerIter = httpSessionAttributeListeners.iterator();
        while (listenerIter.hasNext()) {
            writer.println("  <listener>");
            writer.println("    <listener-class>"
                           + listenerIter.next().getClass().getName()
                           + "</listener-class>");
            writer.println("  </listener>");
        }
        writer.println();
        if (welcomeFileList != null) {
            writer.println("  <!-- Welcome file list -->");
            if (!welcomeFileList.isEmpty()) {
                writer.println("  <welcome-file-list>");
                Iterator<String> welcomeFileIter = welcomeFileList.iterator();
                while (welcomeFileIter.hasNext())
                    writer.println("    <welcome-file>"
                                   + welcomeFileIter.next() + "</welcome-file>");
                writer.println("  </welcome-file-list>");
                writer.println();
            }
        }
        writer.println("</web-app>");
        writer.println();
    }

    private void loadFilter(W3Filter w3filter)
        throws ClassNotFoundException, IllegalAccessException, InstantiationException, ServletException {
        if (w3server.logLevel > 1) log("Loading filter " + w3filter.filterName + " of class " + w3filter.className + " in context " + w3contextName);
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(servletClassLoader);
            if (w3filter.filter == null) {
                if (w3filter.filterClass == null) {
                    w3filter.filter = (Filter)servletClassLoader.loadClass(w3filter.className).newInstance();
                } else {
                    w3filter.filter = w3filter.filterClass.newInstance();
                }
            }
            w3filter.filter.init(w3filter);
        } finally {
            Thread.currentThread().setContextClassLoader(contextClassLoader);
        }
    }

    public W3Servlet checkW3Servlet(String servletName) {
        W3Servlet w3servlet = servlets.get(servletName);
        if (w3servlet != null) return w3servlet;
        if (W3Context.this == W3Context.defaultW3context) return null;
        w3servlet = W3Context.defaultW3context.servlets.get(servletName);
        if (w3servlet == null) return null;
        w3servlet = (W3Servlet)w3servlet.clone();
        w3servlet.w3context = W3Context.this;
        setServlet(w3servlet);
        return w3servlet;
    }

    public W3Servlet getW3Servlet(String servletName) {
        W3Servlet w3servlet = checkW3Servlet(servletName);
        if (w3servlet == null) throw new RuntimeException("Servlet " + servletName + " not found");
        return w3servlet;
    }

    public void setFilter(W3Filter w3filter) {
        filters.put(w3filter.filterName, w3filter);
        filterMap.put(w3filter.filterName, w3filter);
    }

    public void addMappingForServletNames(W3Filter w3filter,
                                          List<String> servletNameMappings,
                                          EnumSet<DispatcherType> dispatcherTypes,
                                          boolean isMatchAfter, List<String> servletNames) {
        for (String aServletName : servletNames) {
            log("Mapping filter " + w3filter.filterName + " to servlet " + aServletName);
            W3Servlet w3servlet = checkW3Servlet(aServletName);
            if (w3servlet == null) {
                w3servlet = new W3Servlet(this, aServletName, null, null);
                setServlet(w3servlet);
            }
            if (isMatchAfter) {
                w3servlet.filterTargetList.add(new FilterTarget(null, null, w3filter, dispatcherTypes));
                if (servletNameMappings != null) {
                    servletNameMappings.add(w3servlet.servletName);
                }
            } else {
                w3servlet.filterTargetList.add(0, new FilterTarget(null, null, w3filter, dispatcherTypes));
                if (servletNameMappings != null) {
                    servletNameMappings.add(0, w3servlet.servletName);
                }
            }
        }
    }

    public void addMappingForUrlPatterns(W3Filter w3filter,
                                         List<String> urlPatternMappings,
                                         EnumSet<DispatcherType> dispatcherTypes,
                                         boolean isMatchAfter, List<String> urlPatterns) {
        for (String urlPattern : urlPatterns) {
            log("Mapping filter " + w3filter.filterName + " to " + urlPattern);
            if (isMatchAfter) {
                filterTargetList.add(new FilterTarget(null, urlPattern, w3filter, dispatcherTypes));
                if (urlPatternMappings != null) {
                    urlPatternMappings.add(urlPattern);
                }
            } else {
                filterTargetList.add(0, new FilterTarget(null, urlPattern, w3filter, dispatcherTypes));
                if (urlPatternMappings != null) {
                    urlPatternMappings.add(0, urlPattern);
                }
            }
        }
    }

    /** Loads servlet class. If servlet is already loaded, returns immediately.
        @param w3servlet identifies the servlet to be loaded */
    public void loadServlet(W3Servlet w3servlet) throws ClassNotFoundException, IllegalAccessException, InstantiationException, MalformedURLException, ServletException {
        if (w3servlet.className == null) return;
        if (w3server.logLevel > 1) log("Loading servlet" + (w3servlet.servletName != null ? " " + w3servlet.servletName : "") + " of class " + w3servlet.className + " in context " + w3contextName);
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(servletClassLoader);
            if (w3servlet.servletName == null) w3servlet.servletName = w3servlet.className;
            if (w3servlet.isNative) w3servlet.servlet = new Native(w3servlet.className);
            else w3servlet.servlet = (Servlet)servletClassLoader.loadClass(w3servlet.className).newInstance();
            if (w3servlet.servlet instanceof SingleThreadModel) {
                if (w3server.logLevel > 1) log("Servlet of " + w3servlet.className + " has single thread model");
                w3servlet.servlet = new PoolServlet(w3servlet.servlet);
            }
            w3servlet.servlet.init(w3servlet);
        } catch (UnavailableException ex) {
            w3servlet.unavailable = true;
            log(ex.toString(), ex);
        } finally {
            Thread.currentThread().setContextClassLoader(contextClassLoader);
        }
        Servlet servlet = w3servlet.servlet;
        if (servlet instanceof MailServlet) w3server.servletPathTable.put("mailto:", "/servlet/" + w3servlet.getServletName());
        else if (servlet instanceof NewsServlet) w3server.servletPathTable.put("news:", "/servlet/" + w3servlet.getServletName());
        else if (servlet instanceof WaisServlet) w3server.servletPathTable.put("wais:", "/servlet/" + w3servlet.getServletName());
    }

    public void setServlet(W3Servlet w3servlet) {
        servlets.put(w3servlet.servletName, w3servlet);
        // Servlets can be invoked also directly by their class name
        if (w3servlet.className != null && !w3servlet.className.equals(w3servlet.servletName))
            servlets.put(w3servlet.className, w3servlet);
        servletList.add(w3servlet);
        servletSet.add(w3servlet);
    }

    public void readTLDListeners(SAXParser saxParser, String resourcePath)
        throws IOException, ParserConfigurationException, SAXException {
        Set resourcePaths = getResourcePaths(resourcePath);
        Iterator resourcePathItems = resourcePaths.iterator();
        while (resourcePathItems.hasNext()) {
            resourcePath = (String)resourcePathItems.next();
            if (resourcePath.endsWith("/classes/")) continue;
            if (resourcePath.endsWith("/")) {
                readTLDListeners(saxParser, resourcePath);
                continue;
            }
            int dot = resourcePath.lastIndexOf('.');
            if (dot == -1) continue;
            String suffix = resourcePath.substring(dot + 1).toLowerCase();
            if (suffix.equals("jar")) {
                File file = new File(getRealPath(resourcePath));
                if (!file.exists()) continue;
                JarFile jarFile = new JarFile(file);
                Enumeration<JarEntry> entries = jarFile.entries();
                while (entries.hasMoreElements()) {
                    JarEntry entry = entries.nextElement();
                    if (!entry.isDirectory() && entry.getName().startsWith("META-INF/") && entry.getName().toLowerCase().endsWith(".tld")) {
                        InputStream in = jarFile.getInputStream(entry);
                        W3Handler w3handler = new W3Handler(false, true);
                        saxParser.parse(in, w3handler);
                        in.close();
                    }
                }
                jarFile.close();
                continue;
            }
            if (!suffix.equals("tld")) continue;
            InputStream in = getResourceAsStream(resourcePath);
            W3Handler w3handler = new W3Handler(false, true);
            saxParser.parse(in, w3handler);
            in.close();
        }
    }

    public void prepareW3Context(File webappsDir, String w3contextName, String contextPath, boolean alsoLoad)
        throws Exception {
        if (w3server.verbose)
            w3server.log("Prepare context " + w3contextName);
        naming = new InitialContext();
        this.webappsDir = webappsDir;
        this.contextPath = contextPath;
        this.w3contextName = w3contextName;
        w3contextDir = new File(webappsDir, w3contextName);
        maxInactiveInterval = W3Context.defaultW3context.maxInactiveInterval;
        maxInactiveIntervalInMillis = W3Context.defaultW3context.maxInactiveIntervalInMillis;
        File warFile = new File(webappsDir, w3contextName + ".war");
        if (warFile.exists() && (!w3contextDir.exists() || w3contextDir.lastModified() < warFile.lastModified())) {
            Support.deleteDirectory(w3contextDir);
            byte[] b = new byte[Support.bufferLength];
            JarFile jarFile = new JarFile(warFile);
            Enumeration<JarEntry> jarFileEntries = jarFile.entries();
            while (jarFileEntries.hasMoreElements()) {
                JarEntry jarEntry = jarFileEntries.nextElement();
                File file = new File(w3contextDir, jarEntry.getName());
                if (jarEntry.isDirectory()) {
                    new File(file.getCanonicalPath()).mkdirs();
                    continue;
                }
                new File(new File(file.getParent()).getCanonicalPath()).mkdirs();
                InputStream in = jarFile.getInputStream(jarEntry);
                OutputStream out = new BufferedOutputStream(new FileOutputStream(file));
                for (int n; (n = in.read(b)) > 0;) out.write(b, 0, n);
                out.close();
                in.close();
            }
            jarFile.close();
        }
        preparationTime = System.currentTimeMillis();
        w3contexts.put(w3contextName, this);
        if (alsoLoad) loadW3Context();
    }

    public boolean accept(File file) {
        return file.getName().toLowerCase().endsWith(".jar");
    }

    public void loadW3Context()
        throws Exception {
        if (w3server.verbose)
            w3server.log("Load context " + w3contextName);
        Enumeration<URL> servicesResources = W3Context.defaultW3context.sharedClassLoader.getResources("META-INF/services/javax.servlet.ServletContainerInitializer");
        while (servicesResources.hasMoreElements()) {
            InputStream in = servicesResources.nextElement().openConnection().getInputStream();
            StringTokenizer tokens = new StringTokenizer(new String(Support.readBytes(in), "UTF-8"), "\n");
            while (tokens.hasMoreTokens()) {
                String className = tokens.nextToken().trim();
                if (className.equals("") || className.startsWith("#")) {
                    continue;
                }
                Class servletContainerInitializerClass
                    = W3Context.defaultW3context.sharedClassLoader.loadClass(className);
                Object servletContainerInitializer
                    = servletContainerInitializerClass.newInstance();
                Method onStartup = servletContainerInitializerClass.getMethod("onStartup", java.util.Set.class, ServletContext.class);
                onStartup.invoke(servletContainerInitializer, null, this);
            }
        }
        File metaInfDir = new File(w3contextDir, "META-INF"),
            contextXmlFile = new File(metaInfDir, "context.xml");
        if (!contextXmlFile.exists())
            contextXmlFile = new File(w3server.dataDirectory, "conf/Catalina/localhost/" + (W3Context.this == W3Context.defaultW3context ? "ROOT.xml" : w3contextName + ".xml"));
        Map<String, Reference> resourceReferences = null;
        if (contextXmlFile.exists()) {
            W3Handler w3handler = new W3Handler(true, false);
            SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
            saxParserFactory.setValidating(false);
            SAXParser saxParser = saxParserFactory.newSAXParser();
            InputStream in = new BufferedInputStream(new FileInputStream(contextXmlFile));
            saxParser.parse(in, w3handler);
            resourceReferences = w3handler.resourceReferences;
            in.close();
        }
        File webInfDir = new File(w3contextDir, "WEB-INF"),
            classesDir = new File(webInfDir, "classes"),
            libDir = new File(webInfDir, "lib"),
            libFiles[] = libDir.exists() ? libDir.listFiles(this) : null,
            tempDir = new File(webInfDir, "temp");
        if (libFiles == null) libFiles = noFiles;
        libFiles = W3Context.sort(libFiles);
        tempDir.mkdirs();
        if (System.getProperty("oracle.j2ee.home") == null)
            System.setProperty("oracle.j2ee.home", webInfDir.getPath());
        URL urls[] = new URL[libFiles.length + 1 + W3Context.defaultW3context.extraClassLoaderUrls.length];
        urls[0] = new URL("file:///" + classesDir.getPath() + "/");
        for (int i = 1; i <= libFiles.length; i++) urls[i] = new URL("file:///" + libFiles[i - 1].getPath());
        for (int i = libFiles.length + 1; i < urls.length; i++) urls[i] = W3Context.defaultW3context.extraClassLoaderUrls[i - libFiles.length - 1];
        servletClassLoader = new W3ClassLoader(urls, W3Context.defaultW3context.sharedClassLoader);
        File webXmlFile = new File(webInfDir, "web.xml");
        if (webXmlFile.exists()) {
            W3Handler w3handler = new W3Handler(false, false);
            SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
            saxParserFactory.setValidating(false);
            SAXParser saxParser = saxParserFactory.newSAXParser();
            InputStream in = new BufferedInputStream(new FileInputStream(webXmlFile));
            saxParser.parse(in, w3handler);
            in.close();
        }
        if (displayName == null) displayName = w3contextName;
        setAttribute(getClass().getName() + ".welcomeFileList", welcomeFileList);
        setAttribute("javax.servlet.context.tempdir", tempDir);
        if (w3server.jspCompiler != null && w3server.jspCompiler.equals("jikes")) {
            // These attributes are set for Tomcat Jasper JSP engine
            StringBuffer servletClassPathBuffer = new StringBuffer(W3Context.defaultW3context.classPath);
            if (servletClassPathBuffer.length() > 0) servletClassPathBuffer.append(File.pathSeparatorChar);
            servletClassPathBuffer.append(W3Context.defaultW3context.sharedClassPath);
            File javaExtDirs = new File(System.getProperty("java.ext.dirs")), extFiles[] = javaExtDirs.exists() ? javaExtDirs.listFiles(this) : null;
            if (extFiles != null) {
                extFiles = W3Context.sort(extFiles);
                for (int i = 0; i < extFiles.length; i++) {
                    if (servletClassPathBuffer.length() > 0)
                        servletClassPathBuffer.append(File.pathSeparatorChar);
                    servletClassPathBuffer.append(extFiles[i].getCanonicalPath());
                }
            }
            if (libFiles != null)
                for (int i = 0; i < libFiles.length; i++) {
                    if (servletClassPathBuffer.length() > 0) servletClassPathBuffer.append(File.pathSeparatorChar);
                    servletClassPathBuffer.append(libFiles[i].getCanonicalPath());
                }
            if (classesDir.exists()) {
                if (servletClassPathBuffer.length() > 0) servletClassPathBuffer.append(File.pathSeparatorChar);
                servletClassPathBuffer.append(classesDir.getCanonicalPath());
            }
            if (servletClassPathBuffer.length() > 0)
                setAttribute("org.apache.catalina.jsp_classpath", servletClassPathBuffer.toString());
        }
        if (W3Context.this == W3Context.defaultW3context) {
            W3Servlet coreServlet = new W3Servlet(this, "default", CoreServlet.class.getName(), null, false, false, true);
            loadServlet(coreServlet);
            setServlet(coreServlet);
            servlets.put("resin-file", coreServlet);
            if (w3server.jspCompiler != null && !w3server.jspCompiler.equals("jdt")) {
                jspServletInitParameters.put("compiler", w3server.jspCompiler);
	    }
            jspServletInitParameters.put("fork", "false");
            if (w3server.jspCompiler != null && w3server.jspCompiler.equals("jikes")) {
                jspServletInitParameters.put("compilerSourceVM", "1.4");
                jspServletInitParameters.put("compilerTargetVM", "1.4");
            } else {
                jspServletInitParameters.put("compilerSourceVM", "1.8");
                jspServletInitParameters.put("compilerTargetVM", "1.8");
            }
            jspServletInitParameters.put("javaEncoding", "UTF-8");
            if (w3server.verbose) jspServletInitParameters.put("logVerbosityLevel", "debug");
            jspServlet = new W3Servlet(this, "jsp", "org.apache.jasper.servlet.JspServlet", jspServletInitParameters, false, false, true);
            jspServlet.isJsp = true;
            loadServlet(jspServlet);
            setServlet(jspServlet);
            String urlPattern = "*.jsp";
            if (!servletPatternMap.containsKey(urlPattern))
                servletPatternMap.put(urlPattern, jspServlet);
            urlPattern = "*.jspx";
            if (!servletPatternMap.containsKey(urlPattern))
                servletPatternMap.put(urlPattern, jspServlet);
            defaultServlet = coreServlet;
            welcomeFileList = new ArrayList<String>();
            welcomeFileList.add("index.htm");
            welcomeFileList.add("index.html");
            welcomeFileList.add("index.shtml");
            welcomeFileList.add("index.jsp");
        } else {
            if (W3Context.defaultW3context.defaultServlet != null) {
                defaultServlet = (W3Servlet)W3Context.defaultW3context.defaultServlet.clone();
                defaultServlet.w3context = W3Context.this;
                setServlet(defaultServlet);
            }
            Iterator iter = W3Context.defaultW3context.servletList.iterator();
            while (iter.hasNext()) {
                W3Servlet w3servlet = (W3Servlet)iter.next();
                if (w3servlet.isDefault && !servlets.containsKey(w3servlet.getServletName())) {
                    w3servlet = (W3Servlet)w3servlet.clone();
                    w3servlet.w3context = W3Context.this;
                    if (w3servlet.isJsp) jspServlet = w3servlet;
                    setServlet(w3servlet);
                }
            }
            iter = W3Context.defaultW3context.servletPatternMap.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry entry = (Map.Entry)iter.next();
                String urlPattern = (String)entry.getKey();
                if (urlPattern.equals("/*") || urlPattern.equals("/") ?
                    !servletPatternMap.containsKey("/*") &&
                    !servletPatternMap.containsKey("/") :
                    !servletPatternMap.containsKey(urlPattern)) {
                    W3Servlet w3servlet = (W3Servlet)entry.getValue();
                    if (w3servlet.isDefault) {
                        w3servlet = servlets.get(w3servlet.getServletName());
                        servletPatternMap.put(urlPattern, w3servlet);
                    }
                }
            }
            Iterator<String> welcomeFileItems = W3Context.defaultW3context.welcomeFileList.iterator();
            while (welcomeFileItems.hasNext()) {
                String welcomeFile = welcomeFileItems.next();
                if (!welcomeFileList.contains(welcomeFile))
                    welcomeFileList.add(welcomeFile);
            }
        }
        Class servletClass = null;
        if (authMethod == null || authMethod.equalsIgnoreCase(HttpServletRequest.BASIC_AUTH))
            servletClass = BasicServlet.class;
        else if (authMethod.equalsIgnoreCase(HttpServletRequest.DIGEST_AUTH))
            servletClass = DigestServlet.class;
        else if (authMethod.equalsIgnoreCase(HttpServletRequest.FORM_AUTH)) {
            W3Servlet formServlet = new W3Servlet(this, "j_security_check", FormServlet.class.getName(), null, false, false, true);
            loadServlet(formServlet);
            setServlet(formServlet);
            servletPatternMap.put("*/j_security_check", formServlet);
        }
        if (servletClass != null) {
            authServlet = new W3Servlet(W3Context.defaultW3context, servletClass.getName(), servletClass.getName(), new Hashtable<String, String>(), 0, false, false, true);
            if (realmName != null)
                authServlet.initParameters.put("realmName", realmName);
            loadServlet(authServlet);
        }
        Class httpJspBaseClass = null;
        Map<String, ServletTarget> servletTargets = new HashMap<String, ServletTarget>();
        Iterator iter = servletPatternMap.entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry entry = (Map.Entry)iter.next();
            String servletPattern = (String)entry.getKey();
            W3Servlet w3servlet = (W3Servlet)entry.getValue();
            ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
            try {
                Thread.currentThread().setContextClassLoader(servletClassLoader);
                if (httpJspBaseClass == null)
                    httpJspBaseClass = servletClassLoader.loadClass("org.apache.jasper.runtime.HttpJspBase");
                if (httpJspBaseClass.isAssignableFrom(servletClassLoader.loadClass(w3servlet.className))) continue;
            } finally {
                Thread.currentThread().setContextClassLoader(contextClassLoader);
            }
            if (servletPattern.startsWith("/*"))
                servletPattern = servletPattern.substring(1);
            if (servletPattern.indexOf('*') == -1 && !servletPattern.endsWith("/")) servletPattern += "/";
            if (servletPattern.equals("/")) defaultServlet = w3servlet;
            else if (servletPattern.startsWith("*.")) {
                log("Mapping servlet " + w3servlet.servletName + " to " + servletPattern);
                extensionPool.add(servletPattern, new ServletTarget(w3servlet, servletPattern));
            } else {
                if (servletPattern.endsWith("/*")) {
                    servletTargets.remove(servletPattern.substring(0, servletPattern.length() - 1));
                } else if (servletTargets.containsKey(servletPattern + "*")) continue;
                servletTargets.put(servletPattern, new ServletTarget(w3servlet, servletPattern));
            }
        }
        for (ServletTarget servletTarget : servletTargets.values()) {
            log("Mapping servlet " + servletTarget.w3servlet.servletName + " to " + servletTarget.urlPattern);
            pathPool.add(servletTarget.urlPattern, servletTarget);
        }
        iter = securityConstraints.iterator();
        while (iter.hasNext()) {
            SecurityConstraint securityConstraint = (SecurityConstraint)iter.next();
            Iterator webResourceCollectionIter = securityConstraint.webResourceCollections.iterator();
            while (webResourceCollectionIter.hasNext()) {
                WebResourceCollection webResourceCollection =
                    (WebResourceCollection)webResourceCollectionIter.next();
                Iterator urlPatternIter =
                    webResourceCollection.urlPatterns.iterator();
                while (urlPatternIter.hasNext()) {
                    String urlPattern = (String)urlPatternIter.next();
                    log("Mapping authenticator to " + urlPattern);
                    if (urlPattern.equals("/")) urlPattern = "*";
                    else if (urlPattern.startsWith("/*"))
                        urlPattern = urlPattern.substring(1);
                    if (urlPattern.indexOf('*') == -1 && !urlPattern.endsWith("/")) urlPattern += "/";
                    AuthTarget authTarget = new AuthTarget(authServlet, urlPattern, webResourceCollection.httpMethods);
                    if (urlPattern.equals("*")) defaultAuthTarget = authTarget;
                    else authPool.replace(urlPattern, authTarget);
                }
            }
        }
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(servletClassLoader);
            if (resourceReferences != null) {
                Iterator<Map.Entry<String, Reference>> resourceReferenceEntries = resourceReferences.entrySet().iterator();
                while (resourceReferenceEntries.hasNext()) {
                    Map.Entry<String, Reference> entry = resourceReferenceEntries.next();
                    String name = entry.getKey();
                    Reference resourceReference = entry.getValue();
                    if (!W3Context.namingSet) {
                        Class<?> serverFactoryClass = servletClassLoader.loadClass("org.apache.catalina.ServerFactory");
                        Method getServerMethod = serverFactoryClass.getMethod("getServer", (Class[])null);
                        Object standardServer = getServerMethod.invoke(null, (Object[])null);
                        Class<?> standardServerClass = servletClassLoader.loadClass("org.apache.catalina.core.StandardServer");
                        Class<?> contextClass = servletClassLoader.loadClass("javax.naming.Context");
                        Method setGlobalNamingContextMethod = standardServerClass.getMethod("setGlobalNamingContext", new Class[] {contextClass});
                        setGlobalNamingContextMethod.invoke(standardServer, new Object[] {naming});
                        W3Context.namingSet = true;
                    }
                    Class objectFactoryClass = servletClassLoader.loadClass(resourceReference.getFactoryClassName());
                    ObjectFactory objectFactory = (ObjectFactory)objectFactoryClass.newInstance();
                    Object instance = objectFactory.getObjectInstance(resourceReference, new CompositeName(name), null, null);
                    naming.rebind(name, instance);
                }
            }
            if (realm != null || !valves.isEmpty()) {
                containerClass = servletClassLoader.loadClass("org.apache.catalina.Container");
                connectorClass = servletClassLoader.loadClass("org.apache.catalina.connector.Connector");
                connectorObject = connectorClass.newInstance();
                contextClass = servletClassLoader.loadClass("org.apache.catalina.core.StandardContext");
                contextObject = contextClass.newInstance();
                Method setDomain = contextClass.getMethod("setDomain", new Class<?>[] {String.class}), 
                    setPathMethod = contextClass.getMethod("setPath", new Class<?>[] {String.class});
                setDomain.invoke(contextObject, new Object[] {"Catalina"});
                setPathMethod.invoke(contextObject, new Object[] {contextPath});
            }
            if (realm != null) {
                Class<?> realmBaseClass = servletClassLoader.loadClass("org.apache.catalina.realm.RealmBase");
                Method setContainerMethod = realmBaseClass.getMethod("setContainer", new Class<?>[] {containerClass});
                authenticateMethod = realmBaseClass.getMethod("authenticate", new Class<?>[] {String.class, String.class});
                Class<?> realmClass = servletClassLoader.loadClass(realm.className);
                realmObject = realmClass.newInstance();
                setContainerMethod.invoke(realmObject, new Object[] {contextObject});
                Class<?> realmInterface = servletClassLoader.loadClass("org.apache.catalina.Realm");
                Method setRealmMethod = contextClass.getMethod("setRealm", new Class<?>[] {realmInterface});
                setRealmMethod.invoke(contextObject, new Object[] {realmObject});
                if (realm.className.equals("org.apache.catalina.realm.JAASRealm")) {
                    Method method = realmClass.getMethod("setUseContextClassLoader", new Class<?>[] {Boolean.TYPE});
                    method.invoke(realmObject, new Object[] {new Boolean(realm.useContextClassLoader)});
                    if (realm.appName != null) {
                        method = realmClass.getMethod("setAppName", new Class<?>[] {String.class});
                        method.invoke(realmObject, new Object[] {realm.appName});
                    }
                    if (realm.userClassNames != null) {
                        method = realmClass.getMethod("setUserClassNames", new Class<?>[] {String.class});
                        method.invoke(realmObject, new Object[] {realm.userClassNames});
                    }
                    if (realm.roleClassNames != null) {
                        method = realmClass.getMethod("setRoleClassNames", new Class<?>[] {String.class});
                        method.invoke(realmObject, new Object[] {realm.roleClassNames});
                    }
                }
                Method method = realmClass.getMethod("start", (Class[])null);
                method.invoke(realmObject, (Object[])null);
            }
            if (!valves.isEmpty()) {
                Class<?> valveBaseClass = servletClassLoader.loadClass("org.apache.catalina.valves.ValveBase"),
                    catalinaRequestClass = servletClassLoader.loadClass("org.apache.catalina.connector.Request"),
                    catalinaResponseClass = servletClassLoader.loadClass("org.apache.catalina.connector.Response");
                invokeMethod = valveBaseClass.getMethod("invoke", new Class<?>[] {catalinaRequestClass, catalinaResponseClass});
            }
        } finally {
            Thread.currentThread().setContextClassLoader(contextClassLoader);
        }
        /*
          Iterator valveItems = valves.iterator();
          while (valveItems.hasNext()) {
          Valve valve = (Valve)valveItems.next();
          String className = (String)valve.attributes.get("className");
          W3Filter w3filter = new W3Filter(W3Context.this, className, ValveFilter.class.getName(), valve.attributes);
          loadFilter(w3filter);
          filterTargetList.add(0, new FilterTarget(null, "/", w3filter, DispatcherType.REQUEST));
          }
        */
        /*
          iter = filterTargetList.iterator();
          while (iter.hasNext()) {
          FilterTarget filterTarget = (FilterTarget)iter.next();
          if (filterTarget instanceof AuthFilterTarget)
          ((AuthFilterTarget)filterTarget).w3filter = authFilter;
          }
        */
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        saxParserFactory.setValidating(false);
        SAXParser saxParser = saxParserFactory.newSAXParser();
        contextClassLoader = Thread.currentThread().getContextClassLoader();
        try {
            Thread.currentThread().setContextClassLoader(servletClassLoader);
            readTLDListeners(saxParser, "/WEB-INF/");
        } finally {
            Thread.currentThread().setContextClassLoader(contextClassLoader);
        }
        /*
          if (W3Context.this != W3Context.defaultW3context) {
          Iterator filterItems = W3Context.defaultW3context.filters.values().iterator();
          while (filterItems.hasNext()) {
          W3Filter w3filter = (W3Filter)filterItems.next();
          if (!filters.containsKey(w3filter.getFilterName())) {
          w3filter = (W3Filter)w3filter.clone();
          w3filter.w3context = W3Context.this;
          loadFilter(w3filter);
          setFilter(w3filter);
          }
          }
        */
        ServletContextEvent servletContextEvent = new ServletContextEvent(this);
        Iterator servletContextListenerItems = new IteratorSequence(new Iterator[] {servletContextListeners.iterator(), tldServletContextListeners.iterator()}, false);
        while (servletContextListenerItems.hasNext()) {
            ServletContextListener servletContextListener = (ServletContextListener)servletContextListenerItems.next();
            log("Initializing context listener " + servletContextListener.getClass().getName());
            contextClassLoader = Thread.currentThread().getContextClassLoader();
            try {
                Thread.currentThread().setContextClassLoader(servletClassLoader);
                servletContextListener.contextInitialized(servletContextEvent);
            } finally {
                Thread.currentThread().setContextClassLoader(contextClassLoader);
            }
            log("Initialized context listener " + servletContextListener.getClass().getName());
        }
        Iterator filterItems = filters.values().iterator();
        while (filterItems.hasNext()) {
            W3Filter w3filter = (W3Filter)filterItems.next();
            loadFilter(w3filter);
        }
        Iterator servletItems = servletSet.iterator();
        while (servletItems.hasNext()) {
            W3Servlet w3servlet = (W3Servlet)servletItems.next();
            if (w3servlet.loadOnStartup == Integer.MIN_VALUE) continue;
            if (!w3servlet.isDisabled) loadServlet(w3servlet);
            if (w3servlet.jspFile == null) continue;
            W3RequestDispatcher w3requestDispatcher = (W3RequestDispatcher)getRequestDispatcher(w3servlet.jspFile);
            if (w3requestDispatcher == null) continue;
            W3Request w3request = new W3Request(w3server);
            w3requestDispatcher.include(w3request, w3request.sender);
        }
        sessionCleaningThread = new Thread(this);
        sessionCleaningThread.start();
        if (w3server.verbose)
            w3server.log("Context loaded=" + this);
    }

    private HashSet<String> getEntityTags(String value) {
        HashSet<String> entityTags  = new HashSet<String>();
        StringTokenizer st = new StringTokenizer(value, ",");
        while (st.hasMoreTokens()) entityTags.add(st.nextToken().trim());
        return entityTags;
    }

    public void setup() throws ClassNotFoundException, IOException, IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchMethodException {
        W3Context.defaultW3context = this;
        if (System.getProperty("org.apache.portals.logdir") == null)
            System.setProperty("org.apache.portals.logdir", w3server.dataDirectory);
        String value = System.getProperty("initialFactory");
        if (value != null)
            System.setProperty(javax.naming.Context.INITIAL_CONTEXT_FACTORY, value);
        value = System.getProperty("providerUrl");
        if (value != null)
            System.setProperty(javax.naming.Context.PROVIDER_URL, value);
        value = System.getProperty("securityPrincipal");
        if (value != null)
            System.setProperty(javax.naming.Context.SECURITY_PRINCIPAL, value);
        value = System.getProperty("securityCredentials");
        if (value != null)
            System.setProperty(javax.naming.Context.SECURITY_CREDENTIALS, value);
        System.setProperty("build.compiler", "jikes");
        System.setProperty("build.compiler.emacs", "true");
        System.setProperty("catalina.base", w3server.dataDirectory);
        System.setProperty("catalina.home", w3server.dataDirectory);
        File extraClassesDir = new File(w3server.dataDirectory, "extra/classes"),
            extraLibDir = new File(w3server.dataDirectory, "extra/lib"),
            extraLibFiles[] = extraLibDir.exists() ? extraLibDir.listFiles(this) : null,
            serverLibDir = new File(w3server.dataDirectory, "bin"),
            serverLibFiles[] = serverLibDir.exists() ? serverLibDir.listFiles(this) : null,
            sharedClassesDir = new File(w3server.dataDirectory, "classes"),
            sharedLibDir = new File(w3server.dataDirectory, "lib"),
            sharedLibFiles[] = sharedLibDir.exists() ? sharedLibDir.listFiles(this) : null;
        if (serverLibFiles == null) serverLibFiles = noFiles;
        if (extraLibFiles == null) extraLibFiles = noFiles;
        if (sharedLibFiles == null) sharedLibFiles = noFiles;
        serverLibFiles = W3Context.sort(serverLibFiles);
        extraLibFiles = W3Context.sort(extraLibFiles);
        sharedLibFiles = W3Context.sort(sharedLibFiles);
        StringBuffer classPathBuffer = new StringBuffer();
        for (int i = 0; i < serverLibFiles.length; i++) {
            if (classPathBuffer.length() > 0) classPathBuffer.append(File.pathSeparatorChar);
            classPathBuffer.append(serverLibFiles[i].getCanonicalPath());
        }
        extraClassLoaderUrls = new URL[extraLibFiles.length + 1];
        if (extraClassesDir.exists()) {
            if (classPathBuffer.length() > 0) classPathBuffer.append(File.pathSeparatorChar);
            classPathBuffer.append(extraClassesDir.getCanonicalPath());
        }
        extraClassLoaderUrls[0] = new URL("file:///" + extraClassesDir.getPath() + "/");
        for (int i = 1; i <= extraLibFiles.length; i++) {
            extraClassLoaderUrls[i] = new URL("file:///" + extraLibFiles[i - 1].getPath());
            if (classPathBuffer.length() > 0) classPathBuffer.append(File.pathSeparatorChar);
            classPathBuffer.append(extraLibFiles[i - 1].getCanonicalPath());
        }
        classPath = classPathBuffer.toString();
        StringBuffer sharedClassPathBuffer = new StringBuffer();
        URL urls[] = new URL[sharedLibFiles.length + 1];
        if (sharedClassesDir.exists()) {
            if (sharedClassPathBuffer.length() > 0) sharedClassPathBuffer.append(File.pathSeparatorChar);
            sharedClassPathBuffer.append(sharedClassesDir.getCanonicalPath());
        }
        urls[0] = new URL("file:///" + sharedClassesDir.getPath() + "/");
        for (int i = 1; i <= sharedLibFiles.length; i++) {
            urls[i] = new URL("file:///" + sharedLibFiles[i - 1].getPath());
            if (sharedClassPathBuffer.length() > 0) sharedClassPathBuffer.append(File.pathSeparatorChar);
            sharedClassPathBuffer.append(sharedLibFiles[i - 1].getCanonicalPath());
        }
        sharedClassPath = sharedClassPathBuffer.toString();
        sharedClassLoader = URLClassLoader.newInstance(urls, w3server.getClass().getClassLoader());
    }

    public void prepare(boolean root)
        throws ServletException {
        try {
            if (root) {
                prepareW3Context(new File(w3server.dataDirectory, "webapps"), "ROOT", "", true);
                return;
            }
            List<W3Context> w3contextList = new ArrayList<W3Context>();
            if (webappsDir.exists()) {
                File w3contextFiles[] = webappsDir.listFiles();
                if (w3contextFiles != null) {
                    w3contextFiles = W3Context.sort(w3contextFiles);
                    File deployDir = new File(w3server.dataDirectory, "conf/Catalina/localhost/");
                    File deployFiles[] = deployDir.listFiles();
                    if (deployDir.exists() && deployDir.isDirectory()) {
                        int deployIndex = 0;
                        for (File deployFile : deployFiles) {
                            String deployName = deployFile.getName();
                            int dot = deployName.lastIndexOf('.');
                            if (dot != -1) deployName = deployName.substring(0, dot);
                            for (int i = 0; i < w3contextFiles.length; i++) {
                                File w3contextFile = w3contextFiles[i];
                                String w3contextName = w3contextFile.getName();
                                if (deployName.equals(w3contextName)) {
                                    File w3contextFileSave = w3contextFiles[deployIndex];
                                    w3contextFiles[deployIndex++] = w3contextFile;
                                    w3contextFiles[i] = w3contextFileSave;
                                    break;
                                }
                            }
                        }
                    }
                    for (File w3contextFile : w3contextFiles) {
                        String w3contextName = w3contextFile.getName();
                        int dot = w3contextName.lastIndexOf('.');
                        if (!w3contextFile.isDirectory() && (dot == -1 || !w3contextName.substring(dot + 1).equalsIgnoreCase("war"))) continue;
                        if (!w3contextFile.isDirectory() && dot != -1) w3contextName = w3contextName.substring(0, dot);
                        if (w3contextName.equalsIgnoreCase("ROOT") ||
                            w3contexts.containsKey(w3contextName)) continue;
                        W3Context w3context = new W3Context(w3server);
                        w3context.prepareW3Context(webappsDir, w3contextName, "/" + w3contextName, false);
                        w3contextList.add(w3context);
                    }
                }
                for (W3Context w3context : w3contextList) {
                    if (w3context.w3contextName.equals("ROOT")) continue;
                    w3context.loadW3Context();
                }
            }
        } catch (Exception ex) {
            if (ex instanceof SAXParseException)
                throw new ServletException(Support.stackTrace(ex)
                                           + ((((SAXParseException)ex).getException() != null ?
                                               ", exception = " + Support.stackTrace(((SAXParseException)ex).getException()) : "")
                                              + ", publicId = " + ((SAXParseException)ex).getPublicId()
                                              + ", systemId = " + ((SAXParseException)ex).getSystemId()
                                              + ", lineNumber = " + ((SAXParseException)ex).getLineNumber()
                                              + ", columnNumber = " + ((SAXParseException)ex).getColumnNumber()), ex);
            throw new ServletException(Support.stackTrace(ex), ex);
        }
    }

    public void destroy() {
        Iterator w3contextIter = w3contexts.values().iterator();
        while (w3contextIter.hasNext()) {
            W3Context w3context = (W3Context)w3contextIter.next();
            w3contextIter.remove();
            // Destroying servlets
            List<W3Servlet> servletsOrdered = new ArrayList<W3Servlet>();
            Iterator<W3Servlet> servletItems = w3context.servletSet.iterator();
            while (servletItems.hasNext())
                servletsOrdered.add(servletItems.next());
            for (int i = servletsOrdered.size() - 1; i >= 0; i--) {
                W3Servlet w3servlet = servletsOrdered.get(i);
                if (w3servlet.servlet == null) continue;
                log("Destroying servlet " + w3servlet.servletName);
                ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
                try {
                    Thread.currentThread().setContextClassLoader(w3context.servletClassLoader);
                    if (w3servlet.servlet != null)
                        w3servlet.servlet.destroy();
                } finally {
                    Thread.currentThread().setContextClassLoader(contextClassLoader);
                }
            }
            // Destroying filters
            if (authServlet != null) {
                log("Destroying servlet " + authServlet.servletName);
                ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
                try {
                    Thread.currentThread().setContextClassLoader(w3context.servletClassLoader);
                    if (authServlet.servlet != null)
                        authServlet.servlet.destroy();
                } finally {
                    Thread.currentThread().setContextClassLoader(contextClassLoader);
                }
            }
            Iterator filterIter =  w3context.filters.values().iterator();
            while (filterIter.hasNext()) {
                W3Filter w3filter = (W3Filter)filterIter.next();
                log("Destroying filter " + w3filter.filterName);
                ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
                try {
                    Thread.currentThread().setContextClassLoader(w3context.servletClassLoader);
                    if (w3filter.filter != null)
                        w3filter.filter.destroy();
                } finally {
                    Thread.currentThread().setContextClassLoader(contextClassLoader);
                }
            }
            // Notifying servlet context listeners about shut down
            ServletContextEvent servletContextEvent = new ServletContextEvent(w3context);
            List<ServletContextListener> contextListeners = new ArrayList<ServletContextListener>();
            contextListeners.addAll(w3context.servletContextListeners);
            contextListeners.addAll(w3context.tldServletContextListeners);
            for (int i = contextListeners.size() - 1; i >= 0; i--) {
                ServletContextListener servletContextListener = contextListeners.get(i);
                log("Destroying context listener " + servletContextListener.getClass().getName());
                ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
                try {
                    Thread.currentThread().setContextClassLoader(w3context.servletClassLoader);
                    servletContextListener.contextDestroyed(servletContextEvent);
                } finally {
                    Thread.currentThread().setContextClassLoader(contextClassLoader);
                }
            }
        }
        if (sessionCleaningThread != null)
            sessionCleaningThread.interrupt();
    }

    /** Returns the servlet container attribute with the given name, or null if there is
        no attribute by that name. An attribute allows a servlet container to give the
        servlet additional information not already provided by this interface. See
        your server documentation for information about its attributes. A list of supported
        attributes can be retrieved using getAttributeNames.
        The attribute is returned as a java.lang.Object or some subclass. Attribute
        names should follow the same convention as package names. The Java Servlet
        API specification reserves names matching java.*, javax.*, and sun.*.

        @param name a String specifying the name of the attribute
        @return an Object containing the value of the attribute, or null if no
        attribute exists matching the given name
        @see getAttributeNames()

    */
    public Object getAttribute(String name) {
        Object value = attributes.get(name);
        if (value != null) return value;
        return w3server.attributes.get(name);
    }

    /** Returns an Enumeration containing the attribute names available within this
        servlet context. Use the getAttribute(String) method with an attribute
        name to get the value of an attribute.

        @return an Enumeration of attribute names
        @see getAttribute(String)

    */
    public Enumeration getAttributeNames() {
        return attributes.keys();
    }

    public String getContextPath() {
        return contextPath;
    }

    /** Returns a ServletContext object that corresponds to a specified URL on the
        server.
        This method allows servlets to gain access to the context for various parts of
        the server, and as needed obtain RequestDispatcher objects from the context.
        The given path must be begin with '/', is interpreted relative to the
        server's document root and is matched against the context roots of other web
        applications hosted on this container.
        In a security conscious environment, the servlet container may return null
        for a given URL.

        @param uripath a String specifying the context path of another web application in
        the container.
        @return the ServletContext object that corresponds to the named URL, or
        null if either none exists or the container wishes to restrict this access.
        @see RequestDispatcher

    */
    public ServletContext getContext(String uripath) {
        return getContext(uripath, false);
    }

    public ServletContext getContext(String uripath, boolean check) {
        if (w3server.verbose)
            log("getContext uripath=" + uripath + ", check=" + check);
        if (uripath == null) return this;
        StringTokenizer st = new StringTokenizer(uripath, "/");
        String w3contextName = st.hasMoreTokens() ? st.nextToken().trim() : null;
        if (w3contextName != null) {
            W3Context w3context = w3contexts.get(w3contextName);
            if (w3context != null && !check) return w3context;
            if (webappsDir.exists()) {
                File contextDir = new File(webappsDir, w3contextName),
                    warFile = new File(webappsDir, w3contextName + ".war");
                if ((!contextDir.exists() || !contextDir.isDirectory())
                    && (!warFile.exists() || !warFile.isFile()))
                    return W3Context.defaultW3context;
                synchronized (preparationObject) {
                    w3context = w3contexts.get(w3contextName);
                    if (w3context != null)
                        if (contextDir.lastModified() <= w3context.preparationTime
                            && warFile.lastModified() <= w3context.preparationTime)
                            return w3context;
                        else w3context.destroy();
                    w3context = new W3Context(w3server);
                    try {
                        w3context.prepareW3Context(webappsDir, w3contextName, "/" + w3contextName, true);
                    } catch (Exception ex) {
                        throw new RuntimeException(ex);
                    }
                }
                return w3context;
            }
        }
        return W3Context.defaultW3context;
    }

    /** Returns a String containing the value of the named context wide initialization
        parameter, or null if the parameter does not exist.
        This method can make available configuration information useful to an entire
        'web application'. For example, it can provide a webmaster's email address
        or the name of a system that holds critical data.

        @param name a String containing the name of the parameter whose value is
        requested

        @return a String containing at least the servlet container name and version
        number

    */
    public String getInitParameter(String name) {
        return contextInitParameters.get(name);
    }

    /** Returns the names of the context's initialization parameters as an
        Enumeration of String objects, or an empty Enumeration if the context has
        no initialization parameters.

        @return an Enumeration of String objects containing the names of the
        context's initialization parameters
        @see javax.servlet.ServletConfig#getInitParameter(String)

    */
    public Enumeration<String> getInitParameterNames() {
        return contextInitParameters.keys();
    }

    /**
     * Sets the context initialization parameter with the given name and value on this ServletContext.
     * @param name the name of the context initialization parameter to set
     * @param value the value of the context initialization parameter to set 
     * @return true if the context initialization parameter with the given name and value was set successfully on this ServletContext, and false if it was not set because this ServletContext already contains a context initialization parameter with a matching name
     * @throws IllegalStateException if this ServletContext has already been initialized
     * @throws UnsupportedOperationException if this ServletContext was passed to the {@link javax.servlet.ServletContextListener#contextInitialized} method of a ServletContextListener that was neither declared in web.xml or web-fragment.xml, nor annotated with WebListener
     * @since Servlet 3.0
     */
    public boolean setInitParameter(String name, String value) {
        if (contextInitParameters.containsKey(name)) return false;
        contextInitParameters.put(name, value);
        return true;
    }

    /** Returns the major version of the Java Servlet API that this servlet container
        supports. All implementations that comply with Version 2.4 must have this
        method return the integer 2.

        @return 2

    */
    public int getMajorVersion() {
        return 2;
    }

    /** Returns the MIME type of the specified file, or null if the MIME type is not
        known. The MIME type is determined by the configuration of the servlet
        container, and may be specified in a web application deployment descriptor.
        Common MIME types are 'text/html' and 'image/gif'.

        @param file a String specifying the name of a file
        @return a String specifying the file's MIME type

    */
    public String getMimeType(String file) {
        String extension =
            file.substring(file.lastIndexOf('.') + 1).toLowerCase(),
            mimeType = mimeTypeMap.get(extension);
        if (mimeType == null && this != W3Context.defaultW3context) mimeType = W3Context.defaultW3context.mimeTypeMap.get(extension);
        return mimeType;
    }

    /** Returns the minor version of the Servlet API that this servlet container supports.
        All implementations that comply with Version 2.4 must have this
        method return the integer 4.

        @return 4

    */
    public int getMinorVersion() {
        return 4;
    }

    /**
     * Gets the major version of the Servlet specification that the application represented by this ServletContext is based on.
     *
     * The value returned may be different from {@link #getMajorVersion()}, which returns the major version of the Servlet specification supported by the Servlet container. 
     * @return the major version of the Servlet specification that the application represented by this ServletContext is based on
     * @throws UnsupportedOperationException if this ServletContext was passed to the {@link javax.servlet.ServletContextListener#contextInitialized} method of a {@link javax.servlet.ServletContextListener} that was neither declared in web.xml or web-fragment.xml, nor annotated with WebListener
     * @since Servlet 3.0
     */
    public int getEffectiveMajorVersion() {
        return 2;
    }

    /**
     * Gets the minor version of the Servlet specification that the application represented by this ServletContext is based on.
     *
     * The value returned may be different from {@link #getMinorVersion()}, which returns the minor version of the Servlet specification supported by the Servlet container. 
     * @return the minor version of the Servlet specification that the application xrepresented by this ServletContext is based on
     * @throws UnsupportedOperationException if this ServletContext was passed to the {@link ServletContextListener#contextInitialized} method of a {@link ServletContextListener} that was neither declared in web.xml or web-fragment.xml, nor annotated with WebListener
     * @since Servlet 3.0
     */
    public int getEffectiveMinorVersion() {
        return 4;
    }

    /** Returns a RequestDispatcher object that acts as a wrapper for the named
        servlet.
        Servlets (and JSP pages also) may be given names via server administration
        or via a web application deployment descriptor. A servlet instance can determine
        its name using ServletConfig.getServletName() .
        This method returns null if the ServletContext cannot return a RequestDispatcher for any reason.

        @param name a String specifying the name of a servlet to wrap
        @return a RequestDispatcher object that acts as a wrapper for the named
        servlet
        @see javax.servlet.RequestDispatcher
        @see javax.servlet.ServletContext#getContext(String)
        @see javax.servlet.ServletConfig#getServletName()

    */
    public RequestDispatcher getNamedDispatcher(String name) {
        W3Servlet w3servlet = servlets.get(name);
        if (w3servlet == null) {
            try {
                w3servlet = new W3Servlet(this, name, name, new Hashtable<String, String>());
                loadServlet(w3servlet);
                setServlet(w3servlet);
            } catch (Exception ex) {
                log(ex.toString(), ex);
                return null;
            }
        }
        return new W3RequestDispatcher(this, w3servlet, contextPath, name, null, null, null, true, false);
    }

    /** Returns a String containing the real path for a given virtual path. For example,
        the path '/index.html' returns the absolute file path on the server's filesystem
        would be served by a request for 'http://host/contextPath/index.html',
        where contextPath is the context path of this ServletContext.
        The real path returned will be in a form appropriate to the computer and operating
        system on which the servlet container is running, including the proper
        path separators. This method returns null if the servlet container cannot
        translate the virtual path to a real path for any reason (such as when the content
        is being made available from a .war archive).

        @param path a String specifying a virtual path
        @return a String specifying the real path, or null if the translation cannot
        be performed

    */
    public String getRealPath(String path) {
        if (path == null) return null;
        if (path.indexOf(":/") != -1) {
            if (path.startsWith(":")) {
                path = path.substring(1);
            }
            return path;
        }
        if (path.startsWith("/")) path = path.substring(1);
        path = path.replace("*", "");
        //path = W3URLConnection.normalize(Support.decodePath(path));
        path = Support.decodePath(path);
        File file = new File(w3contextDir, path);
        if (w3server.verbose)
            w3server.log("realPath=" + file);
        try {
            return file.getCanonicalPath();
        } catch (IOException ex) {
            return null;
        }
    }


    /** Returns a RequestDispatcher object that acts as a wrapper for the resource
        located at the given path. A RequestDispatcher object can be used to forward
        a request to the resource or to include the resource in a response. The
        resource can be dynamic or static.
        The pathname must begin with a '/' and is interpreted as relative to the current
        context root. Use getContext to obtain a RequestDispatcher for
        resources in foreign contexts. This method returns null if the
        ServletContext cannot return a RequestDispatcher.

        SRV.11.1 Use of URL Paths 
        Upon receipt of a client request, the Web container determines the Web application 
        to which to forward it. The Web application selected must have the the longest 
        context path that matches the start of the request URL. The matched part of the URL 
        is the context path when mapping to servlets. 
        The Web container next must locate the servlet to process the request using 
        the path mapping procedure described below. 
        The path used for mapping to a servlet is the request URL from the request 
        object minus the context path and the path parameters. The URL path mapping 
        rules below are used in order. The first successful match is used with no further 
        matches attempted: 
        1. The container will try to find an exact match of the path of the request to the 
        path of the servlet. A successful match selects the servlet. 
        2. The container will recursively try to match the longest path-prefix. This is done 
        by stepping down the path tree a directory at a time, using the ’/’ character as 
        a path separator. The longest match determines the servlet selected.
        3. If the last segment in the URL path contains an extension (e.g. .jsp), the serv- 
        let container will try to match a servlet that handles requests for the extension. 
        An extension is defined as the part of the last segment after the last ’.’ char- 
        acter. 
        4. If neither of the previous three rules result in a servlet match, the container will 
        attempt to serve content appropriate for the resource requested. If a "default" 
        servlet is defined for the application, it will be used. 
        The container must use case-sensitive string comparisons for matching. 
        SRV.11.2 Specification of Mappings 
        In the Web application deployment descriptor, the following syntax is used to define 
        mappings: 
        • A string beginning with a ‘/’ character and ending with a ‘/*’ suffix is used 
        for path mapping. 
        • A string beginning with a ‘*.’ prefix is used as an extension mapping. 
        • A string containing only the ’/’ character indicates the "default" servlet of 
        the application. In this case the servlet path is the request URI minus the con- 
        text path and the path info is null. 
        • All other strings are used for exact matches only.
        SRV.11.2.1 Implicit Mappings 
        If the container has an internal JSP container, the *.jsp extension is mapped to it, 
        allowing JSP pages to be executed on demand. This mapping is termed an implicit 
        mapping. If a *.jsp mapping is defined by the Web application, its mapping takes 
        precedence over the implicit mapping. 
        A servlet container is allowed to make other implicit mappings as long as 
        explicit mappings take precedence. For example, an implicit mapping of *.shtml 
        could be mapped to include functionality on the server.

        @param path a String specifying the pathname to the resource
        @return a RequestDispatcher object that acts as a wrapper for the
        resource at the specified path
        @see javax.servlet.RequestDispatcher
        @see javax.servlet.ServletContext#getContext(String)

    */
    public RequestDispatcher getRequestDispatcher(String path) {
        return getRequestDispatcher(null, path);
    }

    /** Returns a URL to the resource that is mapped to a specified path. The path
        must begin with a '/' and is interpreted as relative to the current context root.
        This method allows the servlet container to make a resource available to servlets
        from any source. Resources can be located on a local or remote file system,
        in a database, or in a .war file.
        The servlet container must implement the URL handlers and URLConnection
        objects that are necessary to access the resource.
        This method returns null if no resource is mapped to the pathname.
        Some containers may allow writing to the URL returned by this method using
        the methods of the URL class.
        The resource content is returned directly, so be aware that requesting a .jsp
        page returns the JSP source code. Use a RequestDispatcher instead to
        include results of an execution.
        This method has a different purpose than java.lang.Class.getResource,
        which looks up resources based on a class loader. This method does not use
        class loaders.

        @param path a String specifying the path to the resource
        @return the resource located at the named path, or null if there is no
        resource at that path
        @throws MalformedURLException if the pathname is not given in the correct form

    */
    public URL getResource(String path) throws MalformedURLException {
        String realPath = getRealPath(path);
        if (realPath == null) return null;
        File file = new File(realPath);
        if (file.exists()) {
            if (file.isDirectory() && !realPath.endsWith(File.separator))
                realPath += File.separator;
            return new URL("file:///" + realPath.replace(File.separatorChar, '/'));
        }
        return null;
    }

    /** Returns the resource located at the named path as an InputStream object.
        The data in the InputStream can be of any type or length. The path must be
        specified according to the rules given in getResource. This method returns
        null if no resource exists at the specified path.
        Meta-information such as content length and content type that is available via
        getResource method is lost when using this method.
        The servlet container must implement the URL handlers and URLConnection
        objects necessary to access the resource.
        This method is different from java.lang.Class.getResourceAsStream,
        which uses a class loader. This method allows servlet containers to make a
        resource available to a servlet from any location, without using a class loader.

        @param path a String specifying the path to the resource
        @return the InputStream returned to the servlet, or null if no resource
        exists at the specified path

    */
    public InputStream getResourceAsStream(String path) {
        try {
            URL resource = getResource(path);
            if (resource == null) return null;
            return resource.openConnection().getInputStream();
        } catch (MalformedURLException ex) {} catch (IOException ex) {}
        return null;
    }

    /** Returns a directory like listing of all the paths to resources within the web
        application whose longest subpath matches the supplied path argument.
        Paths indicating subdirectory paths end with a '/'. The returned paths are all
        relative to the root of the web application and have a leading '/'. For example,
        for a web application containing<br><br>
        /welcome.html<br>
        /catalog/index.html<br>
        /catalog/products.html<br>
        /catalog/offers/books.html<br>
        /catalog/offers/music.html<br>
        /customer/login.jsp<br>
        /WEB-INF/web.xml<br>
        /WEB-INF/classes/com.acme.OrderServlet.class,<br>
        getResourcePaths('/') returns {'/welcome.html', '/catalog/', '/customer/',
        '/WEB-INF/'}<br>
        getResourcePaths('/catalog/') returns {'/catalog/index.html', '/catalog/
        products.html', '/catalog/offers/'}.

        @param path path used to match the resources, which must start with a /
        @return a Set containing the directory listing, or null if there are no
        resources in the web application whose path begins with the supplied path.
        Since: Servlet 2.3
    */
    public Set getResourcePaths(String path) {
        String realPath = getRealPath(path);
        if (realPath == null) return null;
        File file = new File(realPath);
        if (!file.exists() || !file.isDirectory()) return null;
        String list[] = file.list();
        Set<String> resourcePaths = new LinkedHashSet<String>();
        if (!path.endsWith("/")) path += "/";
        for (int i = 0; i < list.length; i++)
            resourcePaths.add(path + list[i] + (new File(file, list[i]).isDirectory() ? "/" : ""));
        return resourcePaths;
    }

    /** Returns the name and version of the servlet container on which the servlet is
        running.
        The form of the returned string is servername/versionnumber. For example,
        the JavaServer Web Development Kit may return the string JavaServer Web
        Dev Kit/1.0.
        The servlet container may return other optional information after the primary
        string in parentheses, for example, JavaServer Web Dev Kit/1.0 (JDK
        1.1.6; Windows NT 4.0 x86).

        @return a String containing at least the servlet container name and version
        number

    */
    public String getServerInfo() {
        return w3server.serverInfo;
    }

    /** @deprecated Deprecated. As of Java Servlet API 2.1, with no direct replacement.
        This method was originally defined to retrieve a servlet from a
        ServletContext. In this version, this method always returns null and
        remains only to preserve binary compatibility. This method will be
        permanently removed in a future version of the Java Servlet API.
        In lieu of this method, servlets can share information using the
        ServletContext class and can perform shared business logic by invoking
        methods on common non-servlet classes.

        @throws ServletException
    */
    @Deprecated
    public Servlet getServlet(String name)
        throws ServletException {
        W3Servlet w3servlet = servlets.get(name);
        return w3servlet != null ? w3servlet.servlet : null;
    }

    /** Returns the name of this web application correponding to this ServletContext
        as specified in the deployment descriptor for this web application by the display-name element.

        @return The name of the web application or null if no name has been
        declared in the deployment descriptor.
        Since: Servlet 2.3

    */
    public String getServletContextName() {
        return displayName;
    }

    /**
     * Adds the servlet with the given name and class name to this servlet context.
     *
     * The registered servlet may be further configured via the returned ServletRegistration object.
     * 
     * The specified className will be loaded using the classloader associated with the application represented by this ServletContext.
     *
     * If this ServletContext already contains a preliminary ServletRegistration for a servlet with the given servletName, it will be completed (by assigning the given className to it) and returned.
     *
     * This method introspects the class with the given className for the {@link javax.servlet.annotation.ServletSecurity}, {@link javax.servlet.annotation.MultipartConfig}, javax.annotation.security.RunAs, and javax.annotation.security.DeclareRoles annotations. In addition, this method supports resource injection if the class with the given className represents a Managed Bean. See the Java EE platform and JSR 299 specifications for additional details about Managed Beans and resource injection. 
     * @param servletName the name of the servlet
     * @param className the fully qualified class name of the servlet 
     * @return a ServletRegistration object that may be used to further configure the registered servlet, or null if this ServletContext already contains a complete ServletRegistration for a servlet with the given servletName
     * @throws IllegalStateException if this ServletContext has already been initialized 
     * @throws java.lang.UnsupportedOperationException if this ServletContext was passed to the {@link javax.servlet.ServletContextListener#contextInitialized} method of a ServletContextListener that was neither declared in web.xml or web-fragment.xml, nor annotated with {@link javax.servlet.annotation.WebListener}
     * @since Servlet 3.0
     */
    public ServletRegistration.Dynamic addServlet(String servletName,
                                                  String className) {
        return null;
    }

    /**
     * @param servletName the name of the servlet
     * @param servlet the servlet instance to register 
     * @return a ServletRegistration object that may be used to further configure the given servlet, or null if this ServletContext already contains a complete ServletRegistration for a servlet with the given servletName or if the same servlet instance has already been registered with this or another ServletContext in the same container 
     * @throws IllegalStateException if this ServletContext has already been initialized 
     * @throws java.lang.UnsupportedOperationException if this ServletContext was passed to the {@link ServletContextListener#contextInitialized} method of a {@link ServletContextListener} that was neither declared in web.xml or web-fragment.xml, nor annotated with WebListener 
     * @throws java.lang.IllegalArgumentException if the given servlet instance implements SingleThreadModel
     * @since Servlet 3.0
     */
    public ServletRegistration.Dynamic addServlet(String servletName,
                                                  Servlet servlet) {
        return null;
    }

    /**
     * @param servletName the name of the servlet
     * @param servletClass the class object from which the servlet will be instantiated 
     * @return a ServletRegistration object that may be used to further configure the registered servlet, or null if this ServletContext already contains a complete ServletRegistration for the given servletName
     * @throws IllegalStateException if this ServletContext has already been initialized 
     * @throws java.lang.UnsupportedOperationException if this ServletContext was passed to the {@link javax.servlet.ServletContextListener#contextInitialized} method of a ServletContextListener that was neither declared in web.xml or web-fragment.xml, nor annotated with {@link javax.servlet.annotation.WebListener}
     * @since Servlet 3.0
     */
    public ServletRegistration.Dynamic addServlet(String servletName,
                                                  Class<? extends Servlet> servletClass) {
        return null;
    }

    /**
     * @param clazz
     * @return the new Servlet instance 
     * @throws ServletException if the given clazz fails to be instantiated 
     * @throws java.lang.UnsupportedOperationException if this ServletContext was passed to the {@link javax.servlet.ServletContextListener#contextInitialized} method of a {@link javax.servlet.ServletContextListener} that was neither declared in web.xml or web-fragment.xml, nor annotated with {@link javax.servlet.annotation.WebListener}
     * @since Servlet 3.0
     */
    public <T extends Servlet> T createServlet(Class<T> clazz)
        throws ServletException {
        return null;
    }

    /**
     * Gets the ServletRegistration corresponding to the servlet with the given servletName.
     * @param servletName the name of the servlet
     * @return the (complete or preliminary) ServletRegistration for the servlet with the given servletName, or null if no ServletRegistration exists under that name
     * @throws UnsupportedOperationException if this ServletContext was passed to the {@link javax.servlet.ServletContextListener#contextInitialized} method of a {@link javax.servlet.ServletContextListener} that was neither declared in web.xml or web-fragment.xml, nor annotated with {@link javax.servlet.annotation.WebListener}
     * @since Servlet 3.0
     */
    public ServletRegistration getServletRegistration(String servletName) {
        return null;
    }

    /**
     * Gets a (possibly empty) Map of the ServletRegistration objects (keyed by servlet name) corresponding to all servlets registered with this ServletContext.
     *
     * The returned Map includes the ServletRegistration objects corresponding to all declared and annotated servlets, as well as the ServletRegistration objects corresponding to all servlets that have been added via one of the addServlet methods.
     *
     * Any changes to the returned Map must not affect this ServletContext. 
     * @return Map of the (complete and preliminary) ServletRegistration objects corresponding to all servlets currently registered with this ServletContext
     * @throws UnsupportedOperationException if this {@link ServletContext} was passed to the {@link javax.servlet.ServletContextListener#contextInitialized} method of a {@link javax.servlet.ServletContextListener} that was neither declared in web.xml or web-fragment.xml, nor annotated with {@link javax.servlet.annotation.WebListener}
     * @since Servlet 3.0
     */
    public Map<String, ? extends ServletRegistration> getServletRegistrations() {
        return null;
    }

    /**
     * Adds the filter with the given name and class name to this servlet context.
     *
     * The registered filter may be further configured via the returned FilterRegistration object.
     *
     * The specified className will be loaded using the classloader associated with the application represented by this ServletContext.
     *
     * If this ServletContext already contains a preliminary FilterRegistration for a filter with the given filterName, it will be completed (by assigning the given className to it) and returned.
     *
     * This method supports resource injection if the class with the given className represents a Managed Bean. See the Java EE platform and JSR 299 specifications for additional details about Managed Beans and resource injection. 
     * @param filterName the name of the filter
     * @param className the fully qualified class name of the filter 
     * @return a FilterRegistration object that may be used to further configure the registered filter, or null if this ServletContext already contains a complete FilterRegistration for a filter with the given filterName
     * @throws IllegalStateException if this ServletContext has already been initialized 
     * @throws java.lang.UnsupportedOperationException - if this {@link ServletContext} was passed to the {@link javax.servlet.ServletContextListener#contextInitialized} method of a {@link javax.servlet.ServletContextListener} that was neither declared in web.xml or web-fragment.xml, nor annotated with {@link javax.servlet.annotation.WebListener}
     * @since Servlet 3.0
     */
    public FilterRegistration.Dynamic addFilter(String filterName,
                                                String className) {
        W3Filter w3filter = new W3Filter(this, null, null, filterName, className, new Hashtable<String, String>());
        setFilter(w3filter);
        W3FilterRegistration filterRegistration = new W3FilterRegistration.W3Dynamic(this, w3filter);
        filterRegistrations.put(filterName, filterRegistration);
        return (FilterRegistration.Dynamic)filterRegistration;
    }

    /**
     * Registers the given filter instance with this ServletContext under the given filterName.
     *
     * The registered filter may be further configured via the returned FilterRegistration object.
     *
     * If this ServletContext already contains a preliminary FilterRegistration for a filter with the given filterName, it will be completed (by assigning the class name of the given filter instance to it) and returned. 
     * @param filterName the name of the filter
     * @param filter the filter instance to register 
     * @return a FilterRegistration object that may be used to further configure the given filter, or null if this ServletContext already contains a complete FilterRegistration for a filter with the given filterName or if the same filter instance has already been registered with this or another ServletContext in the same container
     * @throws IllegalStateException if this ServletContext has already been initialized 
     * @throws java.lang.UnsupportedOperationException if this {@link ServletContext} was passed to the {@link javax.servlet.ServletContextListener#contextInitialized} method of a {@link javax.servlet.ServletContextListener} that was neither declared in web.xml or web-fragment.xml, nor annotated with {@link javax.servlet.annotation.WebListener}
     * @since Servlet 3.0
     */
    public FilterRegistration.Dynamic addFilter(String filterName, Filter filter) {
        W3Filter w3filter = new W3Filter(this, filter, null, filterName, filter.getClass().getName(), new Hashtable<String, String>());
        setFilter(w3filter);
        W3FilterRegistration filterRegistration = new W3FilterRegistration.W3Dynamic(this, w3filter);
        filterRegistrations.put(filterName, filterRegistration);
        return (FilterRegistration.Dynamic)filterRegistration;
    }

    /**
     * Adds the filter with the given name and class type to this servlet context.
     *
     * The registered filter may be further configured via the returned FilterRegistration object.
     *
     * If this ServletContext already contains a preliminary FilterRegistration for a filter with the given filterName, it will be completed (by assigning the name of the given filterClass to it) and returned.
     *
     * This method supports resource injection if the given filterClass represents a Managed Bean. See the Java EE platform and JSR 299 specifications for additional details about Managed Beans and resource injection. 
     * @param filterName the name of the filter
     * @param filterClass the class object from which the filter will be instantiated 
     * @return a FilterRegistration object that may be used to further configure the registered filter, or null if this ServletContext already contains a complete FilterRegistration for a filter with the given filterName
     * @throws IllegalStateException if this ServletContext has already been initialized 
     * @throws java.lang.UnsupportedOperationException - if this {@link ServletContext} was passed to the {@link javax.servlet.ServletContextListener#contextInitialized} method of a {@link javax.servlet.ServletContextListener} that was neither declared in web.xml or web-fragment.xml, nor annotated with {@link javax.servlet.annotation.WebListener}
     * @since Servlet 3.0
     */
    public FilterRegistration.Dynamic addFilter(String filterName,
                                                Class<? extends Filter> filterClass) {
        W3Filter w3filter = new W3Filter(this, null, filterClass, filterName, filterClass.getName(), new Hashtable<String, String>());
        setFilter(w3filter);
        W3FilterRegistration filterRegistration = new W3FilterRegistration.W3Dynamic(this, w3filter);
        filterRegistrations.put(filterName, filterRegistration);
        return (FilterRegistration.Dynamic)filterRegistration;
    }

    /**
     * Instantiates the given Filter class.
     *
     * The returned Filter instance may be further customized before it is registered with this ServletContext via a call to {@link addFilter(String,Filter)}.
     *
     * The given Filter class must define a zero argument constructor, which is used to instantiate it.
     * 
     * This method supports resource injection if the given clazz represents a Managed Bean. See the Java EE platform and JSR 299 specifications for additional details about Managed Beans and resource injection. 
     * @param clazz the Filter class to instantiate 
     * @return the new Filter instance 
     * @throws ServletException if the given clazz fails to be instantiated 
     * @throws java.lang.UnsupportedOperationException if this {@link ServletContext} was passed to the {@link javax.servlet.ServletContextListener#contextInitialized} method of a {@link javax.servlet.ServletContextListener} that was neither declared in web.xml or web-fragment.xml, nor annotated with {@link javax.servlet.annotation.WebListener}
     * @since Servlet 3.0
     */
    public <T extends Filter> T createFilter(Class<T> clazz)
        throws ServletException {
        Exception ex = null;
        try {
            return clazz.newInstance();
        } catch (IllegalAccessException ex1) {
            ex = ex1;
        } catch (InstantiationException ex1) {
            ex = ex1;
        }
        throw new ServletException(ex);
    }

    /**
     * Gets the FilterRegistration corresponding to the filter with the given filterName.
     * @param filterName the name of the filter
     * @return the (complete or preliminary) FilterRegistration for the filter with the given filterName, or null if no FilterRegistration exists under that name
     * @throws UnsupportedOperationException if this {@link ServletContext} was passed to the {@link javax.servlet.ServletContextListener#contextInitialized} method of a {@link javax.servlet.ServletContextListener} that was neither declared in web.xml or web-fragment.xml, nor annotated with {@link javax.servlet.annotation.WebListener}
     * @since Servlet 3.0
     */
    public FilterRegistration getFilterRegistration(String filterName) {
        return filterRegistrations.get(filterName);
    }

    /**
     * Gets a (possibly empty) Map of the FilterRegistration objects (keyed by filter name) corresponding to all filters registered with this ServletContext.
     *
     * The returned Map includes the FilterRegistration objects corresponding to all declared and annotated filters, as well as the FilterRegistration objects corresponding to all filters that have been added via one of the addFilter methods.
     *
     * Any changes to the returned Map must not affect this ServletContext.
     * @return Map of the (complete and preliminary) FilterRegistration objects corresponding to all filters currently registered with this ServletContext
     * @throws UnsupportedOperationException if this {@link ServletContext} was passed to the {@link javax.servlet.ServletContextListener#contextInitialized} method of a {@link javax.servlet.ServletContextListener} that was neither declared in web.xml or web-fragment.xml, nor annotated with {@link javax.servlet.annotation.WebListener}
     * @since Servlet 3.0
     */
    public Map<String, ? extends FilterRegistration> getFilterRegistrations() {
        return filterRegistrations;
    }

    /**
     * Gets the SessionCookieConfig object through which various properties of the session tracking cookies created on behalf of this ServletContext may be configured.
     *
     * Repeated invocations of this method will return the same SessionCookieConfig instance. 
     * @return the SessionCookieConfig object through which various properties of the session tracking cookies created on behalf of this ServletContext may be configured
     * @throws UnsupportedOperationException if this {@link ServletContext} was passed to the {@link javax.servlet.ServletContextListener#contextInitialized} method of a {@link javax.servlet.ServletContextListener} that was neither declared in web.xml or web-fragment.xml, nor annotated with {@link javax.servlet.annotation.WebListener}
     * @since Servlet 3.0
     */
    public SessionCookieConfig getSessionCookieConfig() {
        return null;
    }

    /**
     * Sets the session tracking modes that are to become effective for this ServletContext.
     *
     * The given sessionTrackingModes replaces any session tracking modes set by a previous invocation of this method on this ServletContext. 
     * @param sessionTrackingModes the set of session tracking modes to become effective for this ServletContext
     * @throws IllegalArgumentException if this ServletContext has already been initialized 
     *             If sessionTrackingModes specifies
     *             {@link SessionTrackingMode#SSL} in combination with any other
     *             {@link SessionTrackingMode}
     * @throws IllegalStateException if this ServletContext has already been initialized 
     * @throws UnsupportedOperationException if this {@link ServletContext} was passed to the {@link javax.servlet.ServletContextListener#contextInitialized} method of a {@link javax.servlet.ServletContextListener} that was neither declared in web.xml or web-fragment.xml, nor annotated with {@link javax.servlet.annotation.WebListener}
     * @since Servlet 3.0
     */
    public void setSessionTrackingModes(
            Set<SessionTrackingMode> sessionTrackingModes)
        throws IllegalStateException, IllegalArgumentException {
    }

    /**
     * Gets the session tracking modes that are supported by default for this ServletContext.
     * @return set of the session tracking modes supported by default for this ServletContext
     * @throws UnsupportedOperationException if this {@link ServletContext} was passed to the {@link javax.servlet.ServletContextListener#contextInitialized} method of a {@link javax.servlet.ServletContextListener} that was neither declared in web.xml or web-fragment.xml, nor annotated with {@link javax.servlet.annotation.WebListener}
     * @since Servlet 3.0
     */
    public Set<SessionTrackingMode> getDefaultSessionTrackingModes() {
        return null;
    }

    /**
     * Gets the session tracking modes that are in effect for this ServletContext.
     *
     * The session tracking modes in effect are those provided to {@link #setSessionTrackingModes}.
     *
     * By default, the session tracking modes returned by {@link #getDefaultSessionTrackingModes} are in effect. 
     * @return set of the session tracking modes in effect for this ServletContext
     * @throws UnsupportedOperationException if this {@link ServletContext} was passed to the {@link javax.servlet.ServletContextListener#contextInitialized} method of a {@link javax.servlet.ServletContextListener} that was neither declared in web.xml or web-fragment.xml, nor annotated with {@link javax.servlet.annotation.WebListener}
     * @since Servlet 3.0
     */
    public Set<SessionTrackingMode> getEffectiveSessionTrackingModes() {
        return null;
    }

    /**
     * Adds a listener of the given class type to this ServletContext.
     *
     * The given listenerClass must implement one or more of the following interfaces:
     *
     * {@link ServletContextAttributeListener}
     * {@link ServletRequestListener}
     * {@link ServletRequestAttributeListener}
     * {@link HttpSessionListener}
     * {@link HttpSessionAttributeListener}
     *
     * If this ServletContext was passed to {@link ServletContainerInitializer#onStartup}, then the given listenerClass may also implement ServletContextListener, in addition to the interfaces listed above.
     *
     * If the given listenerClass implements a listener interface whose invocation order corresponds to the declaration order (i.e., if it implements {@link ServletRequestListener}, {@link ServletContextListener}, or {@link HttpSessionListener}), then the new listener will be added to the end of the ordered list of listeners of that interface.
     *
     * This method supports resource injection if the given listenerClass represents a Managed Bean. See the Java EE platform and JSR 299 specifications for additional details about Managed Beans and resource injection. 
     * @param listenerClass the listener class to be instantiated 
     * @throws java.lang.IllegalArgumentException if the class with the given name does not implement any of the above interfaces, or if it implements {@link javax.servlet.ServletContextListener} and this {@link javax.servlet.ServletContext} was not passed to {@link javax.servlet.ServletContainerInitializer#onStartup}
     * @throws IllegalStateException if this {@link ServletContext} has already been initialized 
     * @throws java.lang.UnsupportedOperationException if this {@link ServletContext} was passed to the {@link javax.servlet.ServletContextListener#contextInitialized} method of a {@link javax.servlet.ServletContextListener} that was neither declared in web.xml or web-fragment.xml, nor annotated with {@link javax.servlet.annotation.WebListener}
     * @since Servlet 3.0
     */
    public void addListener(Class<? extends EventListener> listenerClass) {
    }

    /**
     * Adds the listener with the given class name to this ServletContext.
     *
     * The class with the given name will be loaded using the classloader associated with the application represented by this ServletContext, and must implement one or more of the following interfaces:
     *
     * {@link ServletContextAttributeListener}
     * {@link ServletRequestListener}
     * {@link ServletRequestAttributeListener}
     * {@link HttpSessionListener}
     * {@link HttpSessionAttributeListener}
     *
     * If this ServletContext was passed to ServletContainerInitializer#onStartup, then the class with the given name may also implement ServletContextListener, in addition to the interfaces listed above.
     *
     * As part of this method call, the container must load the class with the specified class name to ensure that it implements one of the required interfaces.
     *
     * If the class with the given name implements a listener interface whose invocation order corresponds to the declaration order (i.e., if it implements {@link ServletRequestListener}, {@link ServletContextListener}, or {@link HttpSessionListener}), then the new listener will be added to the end of the ordered list of listeners of that interface.
     *
     * This method supports resource injection if the class with the given className represents a Managed Bean. See the Java EE platform and JSR 299 specifications for additional details about Managed Beans and resource injection. 
     * @param className the fully qualified class name of the listener 
     * @throws java.lang.IllegalArgumentException if the class with the given name does not implement any of the above interfaces, or if it implements {@link ServletContextListener} and this {@link ServletContext} was not passed to {@link ServletContainerInitializer#onStartup}
     * @throws IllegalStateException if this {@link ServletContext} has already been initialized 
     * @throws java.lang.UnsupportedOperationException if this {@link javax.servlet.ServletContext} was passed to the {@link javax.servlet.ServletContextListener#contextInitialized} method of a {@link javax.servlet.ServletContextListener} that was neither declared in web.xml or web-fragment.xml, nor annotated with {@link javax.servlet.annotation.WebListener}
     * @since Servlet 3.0
     */
    public void addListener(String className) {
    }

    /**
     * Adds the given listener to this ServletContext.
     *
     * The given listener must be an instance of one or more of the following interfaces:
     *
     * {@link ServletContextAttributeListener}
     * {@link ServletRequestListener}
     * {@link ServletRequestAttributeListener}
     * {@link HttpSessionListener}
     * {@link HttpSessionAttributeListener}
     *
     * If this ServletContext was passed to {@link ServletContainerInitializer#onStartup}, then the given listener may also be an instance of {@link ServletContextListener}, in addition to the interfaces listed above.
     *
     * If the given listener is an instance of a listener interface whose invocation order corresponds to the declaration order (i.e., if it is an instance of {@link ServletRequestListener}, {@link ServletContextListener}, or {@link HttpSessionListener}), then the listener will be added to the end of the ordered list of listeners of that interface. 
     * @param <T>
     * @param t the listener to be added 
     * @throws java.lang.IllegalArgumentException if the given listener is not an instance of any of the above interfaces, or if it is an instance of ServletContextListener and this ServletContext was not passed to ServletContainerInitializer#onStartup 
     * @throws IllegalStateException if this ServletContext has already been initialized 
     * @throws java.lang.UnsupportedOperationException if this {@link ServletContext} was passed to the {@link javax.servlet.ServletContextListener#contextInitialized} method of a {@link javax.servlet.ServletContextListener} that was neither declared in web.xml or web-fragment.xml, nor annotated with {@link javax.servlet.annotation.WebListener}
     * @since Servlet 3.0 TODO SERVLET3 - Add comments
     */
    public <T extends EventListener> void addListener(T t) {
    }

    /**
     * Instantiates the given EventListener class.
     *
     * The specified {@link EventListener} class must implement at least one of the {@link ServletContextListener}, {@link ServletContextAttributeListener}, {@link ServletRequestListener}, {@link ServletRequestAttributeListener}, {@link HttpSessionListener}, or {@link HttpSessionAttributeListener} interfaces.
     *
     * The returned EventListener instance may be further customized before it is registered with this ServletContext via a call to {@link #addListener(EventListener)}.
     *
     * The given EventListener class must define a zero argument constructor, which is used to instantiate it.
     *
     * This method supports resource injection if the given clazz represents a Managed Bean. See the Java EE platform and JSR 299 specifications for additional details about Managed Beans and resource injection. 
     * @param <T>
     * @param clazz the EventListener class to instantiate 
     * @return the new EventListener instance 
     * @throws ServletException if the given clazz fails to be instantiated
     * @throws java.lang.UnsupportedOperationException if this ServletContext was passed to the {@link javax.servlet.ServletContextListener#contextInitialized} method of a {@link javax.servlet.ServletContextListener} that was neither declared in web.xml or web-fragment.xml, nor annotated with {@link javax.servlet.annotation.WebListener}
    java.lang.IllegalArgumentException - if the specified {@link java.util.EventListener} class does not implement any of the {@link javax.servlet.ServletContextListener}, {@link javax.servlet.ServletContextAttributeListener}, {@link javax.servlet.ServletRequestListener}, {@link javax.servlet.ServletRequestAttributeListener}, {@link javax.servlet.http.HttpSessionListener}, or {@link javax.servlet.http.HttpSessionAttributeListener} interfaces.
     * @since Servlet 3.0
     */
    public <T extends EventListener> T createListener(Class<T> clazz)
        throws ServletException {
        return null;
    }

    /**
     * Declares role names that are tested using {@link javax.servlet.http.HttpServletRequest#isUserInRole}.
     *
     * Roles that are implicitly declared as a result of their use within the {@link javax.servlet.ServletRegistration.Dynamic#setServletSecurity} or {@link javax.servlet.ServletRegistration.Dynamic#setRunAsRole} methods of the {@link javax.servlet.ServletRegistration} interface need not be declared. 
     * @param roleNames the role names being declared 
     * @throws UnsupportedOperationException if this ServletContext was passed to the {@link javax.servlet.ServletContextListener#contextInitialized}
     * @throws IllegalArgumentException if any of the argument roleNames is null or the empty string 
     * @throws IllegalStateException if the ServletContext has already been initialized
     * @since Servlet 3.0
     */
    public void declareRoles(String... roleNames) {
    }

    /**
     * Get the primary name of the virtual host on which this context is
     * deployed. The name may or may not be a valid host name.
     *
     * @return The primary name of the virtual host on which this context is
     *         deployed
     * @since Servlet 3.1
     */
    public String getVirtualServerName() {
	return null;
    }

    /**
     * Gets the class loader of the web application represented by this ServletContext.
     *
     * If a security manager exists, and the caller's class loader is not the same as, or an ancestor of the requested class loader, then the security manager's checkPermission method is called with a RuntimePermission("getClassLoader") permission to check whether access to the requested class loader should be granted. 
     * @return the class loader of the web application represented by this ServletContext 
     * @throws UnsupportedOperationException if this {@link javax.servlet.ServletContext} was passed to the {@link javax.servlet.ServletContextListener#contextInitialized} method of a {@link javax.servlet.ServletContextListener} that was neither declared in web.xml or web-fragment.xml, nor annotated with {@link javax.servlet.annotation.WebListener}
     * @throws SecurityException if a security manager denies access to the requested class loader
     * @since Servlet 3.0
     */
    public ClassLoader getClassLoader() {
        return servletClassLoader;
    }

    /**
     * Gets the &lt;jsp-config&gt; related configuration that was aggregated from the web.xml and web-fragment.xml descriptor files of the web application represented by this ServletContext.
     * @return the &lt;jsp-config&gt; related configuration that was aggregated from the web.xml and web-fragment.xml descriptor files of the web application represented by this ServletContext, or null if no such configuration exists 
     * @throws UnsupportedOperationException if this {@link javax.servlet.ServletContext} was passed to the {@link javax.servlet.ServletContextListener#contextInitialized} method of a {@link javax.servlet.ServletContextListener} that was neither declared in web.xml or web-fragment.xml, nor annotated with {@link javax.servlet.annotation.WebListener}
     * @since Servlet 3.0
     */
    public JspConfigDescriptor getJspConfigDescriptor() {
        return null;
    }

    /** @deprecated Deprecated. As of Java Servlet API 2.1, with no replacement.
        This method was originally defined to return an Enumeration of all the
        servlet names known to this context. In this version, this method always
        returns an empty Enumeration and remains only to preserve binary
        compatibility. This method will be permanently removed in a future version
        of the Java Servlet API.
    */
    @Deprecated
    public Enumeration getServletNames() {
        return new W3ServletIteratorEnumeration(servlets.keySet().iterator(), true);
    }

    /** @deprecated Deprecated. As of Java Servlet API 2.0, with no replacement.
        This method was originally defined to return an Enumeration of all the
        servlets known to this servlet context. In this version, this method always
        returns an empty enumeration and remains only to preserve binary
        compatibility. This method will be permanently removed in a future version
        of the Java Servlet API.

    */
    @Deprecated
    public Enumeration getServlets() {
        return new W3ServletIteratorEnumeration(servlets.values().iterator(), false);
    }

    /** @deprecated Deprecated. As of Java Servlet API 2.1, use log(String, Throwable)
        instead.
        This method was originally defined to write an exception's stack trace and an
        explanatory error message to the servlet log file.

    */
    @Deprecated
    public void log(Exception exception, String msg) {
        log(msg, exception);
    }

    /** Writes the specified message to a servlet log file, usually an event log. The
        name and type of the servlet log file is specific to the servlet container.

        @param msg a String specifying the message to be written to the log file

    */
    public void log(String msg) {
        w3server.log(msg);
    }

    /** Writes an explanatory message and a stack trace for a given Throwable
        exception to the servlet log file. The name and type of the servlet log file is
        specific to the servlet container, usually an event log.

        @param message a String that describes the error or exception
        @param throwable the Throwable error or exception

    */
    public void log(String message, Throwable throwable) {
        w3server.log(message, throwable);
    }

    /** Removes the attribute with the given name from the servlet context. After
        removal, subsequent calls to getAttribute(String) to retrieve the
        attribute's value will return null.
        If listeners are configured on the ServletContext the container notifies them
        accordingly.

        @param name a String specifying the name of the attribute to be removed

    */
    public void removeAttribute(String name) {
        Object value = attributes.remove(name);
        if (value == null) return;
        ServletContextAttributeEvent servletContextAttributeEvent =
            new ServletContextAttributeEvent(this, name, value);
        Iterator servletContextAttributeListenerIter =
            servletContextAttributeListeners.iterator();
        while (servletContextAttributeListenerIter.hasNext())
            ((ServletContextAttributeListener)servletContextAttributeListenerIter.next()).attributeRemoved(servletContextAttributeEvent);
    }

    /** Binds an object to a given attribute name in this servlet context. If the name
        specified is already used for an attribute, this method will replace the attribute
        with the new to the new attribute.
        If listeners are configured on the ServletContext the container notifies them
        accordingly.
        If a null value is passed, the effect is the same as calling removeAttribute().
        Attribute names should follow the same convention as package names. The
        Java Servlet API specification reserves names matching java.*, javax.*,
        and sun.*.

        @param name a String specifying the name of the attribute
        @param object an Object representing the attribute to be bound

    */
    public void setAttribute(String name, Object object) {
        if (object == null) {
            removeAttribute(name);
            return;
        }
        Object oldValue = attributes.get(name);
        attributes.put(name, object);
        Iterator servletContextAttributeListenerIter = servletContextAttributeListeners.iterator();
        if (oldValue != null) {
            ServletContextAttributeEvent servletContextAttributeEvent =
                new ServletContextAttributeEvent(this, name, oldValue);
            while (servletContextAttributeListenerIter.hasNext())
                ((ServletContextAttributeListener)servletContextAttributeListenerIter.next()).attributeReplaced(servletContextAttributeEvent);
        } else {
            ServletContextAttributeEvent servletContextAttributeEvent =
                new ServletContextAttributeEvent(this, name, object);
            while (servletContextAttributeListenerIter.hasNext())
                ((ServletContextAttributeListener)servletContextAttributeListenerIter.next()).attributeAdded(servletContextAttributeEvent);
        }
    }   

    /** @deprecated */
    @Deprecated
    public HttpSession getSession(String sessionId) {
        return getSession(true, sessionId);
    }

    /** @deprecated */
    @Deprecated
    public Enumeration getIds() {
        return emptyVector.elements();
    }

    public void run() {
        try {
            for (;;) {
                synchronized (this) {
                    if (maxInactiveIntervalInMillis < 0L) wait();
                    else wait(maxInactiveIntervalInMillis);
                }
                long ctm = System.currentTimeMillis();
                Iterator iter = sessions.values().iterator();
                while (iter.hasNext()) {
                    W3Session w3Session = (W3Session)iter.next();
                    synchronized (sessions) {
                        if (w3Session.nBindings <= 0 &&
                            (w3Session.invalidated || w3Session.maxInactiveIntervalInMillis > -1L &&
                             ctm - w3Session.lastAccessedTime > w3Session.maxInactiveIntervalInMillis)) {
                            iter.remove();
                            try {
                                w3Session.invalidate();
                            } catch (IllegalStateException ex) {}
                        }
                    }
                }
                if (sessions.isEmpty()) {
                    if (maxInactiveInterval < 0) maxInactiveIntervalInMillis = -1L;
                    else maxInactiveIntervalInMillis = maxInactiveInterval * 1000L;
                }
            }
        } catch (Exception ex) {
            if (ex instanceof InterruptedException) Thread.currentThread().interrupt();
            else log(ex.toString(), ex);
        }
    }

    public int compare(String o1, String o2) {
        int delta = o2.length() - o1.length();
        return delta != 0 ? delta : o1.compareTo(o2);
    }

    public boolean equals(Object obj) {
        return obj instanceof W3Context;
    }

    /**
       Gets the MIME type for the specified file name.
       @param fileName a <code>String</code> specifying the name
       of a file

       @return a <code>String</code> specifying the file's MIME type

    */
    public String getContentTypeFor(String fileName) {
        String mimeType = getMimeType(fileName);
        if (mimeType == null) mimeType = W3URLConnection.guessContentType(fileName);
        return mimeType;
    }

    public String getErrorPageByErrorCode(int errorCode) {
        String errorPage = errorPagesByErrorCode.get(new Integer(errorCode));
        if (errorPage == null && this != W3Context.defaultW3context) return W3Context.defaultW3context.getErrorPageByErrorCode(errorCode);
        return errorPage;
    }

    public String getErrorPageByExceptionType(Throwable throwable) {
        if (throwable == null) return null;
        Class exceptionClass = throwable.getClass();
        int length = errorPagesByExceptionType.size();
        for (int i = 0; i < length; i++) {
            ErrorPage errorPage = errorPagesByExceptionType.get(i);
            if (errorPage.exceptionType.isAssignableFrom(exceptionClass)) return errorPage.location;
        }
        if (this != W3Context.defaultW3context) {
            String location = W3Context.defaultW3context.getErrorPageByExceptionType(throwable);
            if (location != null) return location;
        }
        if (throwable instanceof ServletException) return getErrorPageByExceptionType(((ServletException)throwable).getRootCause());
        return null;
    }

    public void requestInitialized(ServletRequest request) {
        ServletRequestEvent servletRequestEvent = new ServletRequestEvent(this, request);
        Iterator<ServletRequestListener> servletRequestListenerItems = servletRequestListeners.iterator();
        while (servletRequestListenerItems.hasNext()) {
            ServletRequestListener servletRequestListener = servletRequestListenerItems.next();
            ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
            try {
                Thread.currentThread().setContextClassLoader(servletClassLoader);
                servletRequestListener.requestInitialized(servletRequestEvent);
            } finally {
                Thread.currentThread().setContextClassLoader(contextClassLoader);
            }
        }
    }

    public void requestDestroyed(ServletRequest request) {
        ServletRequestEvent servletRequestEvent = new ServletRequestEvent(this, request);
        Iterator<ServletRequestListener> servletRequestListenerItems = servletRequestListeners.iterator();
        while (servletRequestListenerItems.hasNext()) {
            ServletRequestListener servletRequestListener = servletRequestListenerItems.next();
            ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
            try {
                Thread.currentThread().setContextClassLoader(servletClassLoader);
                servletRequestListener.requestDestroyed(servletRequestEvent);
            } finally {
                Thread.currentThread().setContextClassLoader(contextClassLoader);
            }
        }
    }

    public void requestAttributeAdded(ServletRequest request, String name, Object object) {
        ServletRequestAttributeEvent servletRequestAttributeEvent =
            new ServletRequestAttributeEvent(this, request, name, object);
        Iterator<ServletRequestAttributeListener> servletRequestAttributeListenerItems = servletRequestAttributeListeners.iterator();
        while (servletRequestAttributeListenerItems.hasNext())
            servletRequestAttributeListenerItems.next().attributeAdded(servletRequestAttributeEvent);
    }

    public void requestAttributeRemoved(ServletRequest request, String name, Object object) {
        ServletRequestAttributeEvent servletRequestAttributeEvent =
            new ServletRequestAttributeEvent(this, request, name, object);
        Iterator<ServletRequestAttributeListener> servletRequestAttributeListenerItems = servletRequestAttributeListeners.iterator();
        while (servletRequestAttributeListenerItems.hasNext())
            servletRequestAttributeListenerItems.next().attributeRemoved(servletRequestAttributeEvent);
    }

    public void requestAttributeReplaced(ServletRequest request, String name, Object object) {
        ServletRequestAttributeEvent servletRequestAttributeEvent =
            new ServletRequestAttributeEvent(this, request, name, object);
        Iterator<ServletRequestAttributeListener> servletRequestAttributeListenerItems = servletRequestAttributeListeners.iterator();
        while (servletRequestAttributeListenerItems.hasNext())
            servletRequestAttributeListenerItems.next().attributeReplaced(servletRequestAttributeEvent);
    }

    public W3Session getSession(boolean create, String sessionId) {
        W3Session httpSession = null;
        boolean created = false;
        if (create)
            synchronized (sessions) {
                httpSession = sessionId != null ? sessions.get(sessionId) : null;
                if (httpSession == null || httpSession.invalidated) {
                    if (sessionId == null || httpSession != null && httpSession.invalidated) {
                        sessionId = Long.toString(sessionNumber++, Character.MAX_RADIX) + "-" + Integer.toString(Math.abs(random.nextInt()), Character.MAX_RADIX);
                        created = true;
                    }
                    httpSession = new W3Session(this, sessionId);
                    httpSession.maxInactiveInterval = maxInactiveInterval;
                    sessions.put(sessionId, httpSession);
                }
            }
        else {
            httpSession = sessionId != null ? sessions.get(sessionId) : null;
            if (httpSession == null || httpSession.invalidated) return null;
        }
        if (!created) {
            httpSession.nBindings++;
            httpSession.isNew = false;
            httpSession.invalidated = false;
        }
        HttpSessionEvent httpSessionEvent = new HttpSessionEvent(httpSession);
        if (created) {
            Iterator httpSessionListenerIter = httpSessionListeners.iterator();
            while (httpSessionListenerIter.hasNext())
                ((HttpSessionListener)httpSessionListenerIter.next()).sessionCreated(httpSessionEvent);
        }
        // Notifying objects implementing HttpSessionActivationListener
        // and that are bound to session about session activation
        /*
          Iterator httpSessionActivationListenerIter = httpSessionActivationListeners.iterator();
          while (httpSessionActivationListenerIter.hasNext())
          ((HttpSessionActivationListener)httpSessionActivationListenerIter.next()).sessionDidActivate(httpSessionEvent);
        */
        if (w3server.verbose)
            w3server.log(httpSession.toString());
        return httpSession;
    }

    public void sendRedirect(HttpServletRequest request, HttpServletResponse response, String location) throws IOException {
        if (response.isCommitted()) throw new IllegalStateException();
        if (location.indexOf("://") == -1)
            if (!location.startsWith("/")) {
                String basePath = request.getRequestURI();
                if (location.startsWith("?")) location = basePath + location;
                else location = basePath.substring(0, basePath.lastIndexOf('/', basePath.length() > 1 ? basePath.length() - 2 : 1) + 1) + location;
            }
        response.reset();
        response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
        response.setContentType(Support.htmlType);
        response.setHeader("Location", location);
        PrintWriter writer = response.getWriter();
        String title = HttpServletResponse.SC_MOVED_TEMPORARILY + " " + w3server.messages.getString("movedTemporarily");
        writer.println("<html><head><title>" + title + "</title></head><body>");
        writer.println("<h3>Go to <a href=" + location + ">" + Support.htmlString(location) + "</a></h3>");
        writer.print("</body></html>");
        response.flushBuffer();
    }

    public final String getComment(int sc) {
        int i = sc / 100, j = sc % 100;
        return i > 0 && i <= W3Context.comments.length && j >= 0 && j < W3Context.comments[i - 1].length ? W3Context.comments[i - 1][j] : String.valueOf(sc);
    }

    public void sendError(W3Servlet w3servlet, HttpServletRequest request, HttpServletResponse response, int sc, String msg) throws IOException {
        if (response.isCommitted()) throw new IllegalStateException();
        response.reset();
        String sm = getComment(sc);
        response.setStatus(sc, sm);
        if (request.getAttribute("javax.servlet.error.status_code") == null) {
            String location = getErrorPageByErrorCode(sc);
            if (location != null)
                try {
                    request.setAttribute("javax.servlet.error.status_code", new Integer(sc));
                    request.setAttribute("javax.servlet.error.message", msg);
                    request.setAttribute("javax.servlet.error.request_uri", request.getRequestURI());
                    if (w3servlet != null)
                        request.setAttribute("javax.servlet.error.servlet_name", w3servlet.getServletName());
                    W3RequestDispatcher requestDispatcher = (W3RequestDispatcher)getRequestDispatcher(location);
                    if (requestDispatcher != null) {
                        requestDispatcher.forward(request, response, DispatcherType.ERROR);
                        return;
                    }
                } catch (ServletException ex) {
                    throw new IOException(ex);
                }
        }
        response.setContentType(Support.htmlType);
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Pragma", "no-cache");
        PrintWriter writer = response.getWriter();
        String title = sc + (sm != null ? " " + sm : "");
        writer.println("<html><head><title>" + title + "</title></head><body>");
        writer.println("<h1>" + title + "</h1>");
        if (msg != null) writer.println("<b>" + Support.htmlString(msg) + "</b>");
        writer.println("<p><i>from " + w3server.serverID + "</i>");
        writer.print("</body></html>");
        response.flushBuffer();
    }

    public W3Session getSession(W3Request w3request, boolean create) {
        W3Session session = getSession(create, w3request.requestedSessionId);
        if (session == null) return null;
        w3request.requestedSessionId = session.id;
        if (session.isNew()) {
            Cookie sessionCookie = new Cookie(w3server.sessionCookieName, session.id);
            sessionCookie.setPath("/");
            w3request.sessionSetCookie = W3URLConnection.cookieSetHeader(sessionCookie);
        }
        return session;
    }

    public RequestDispatcher getRequestDispatcher(String basePath, String path) {
        if (basePath != null && !path.startsWith("/")) {
            path = basePath.substring(0, basePath.lastIndexOf('/') + 1) + path;
            int slash = path.indexOf('/', 1);
            if (slash != -1) path = path.substring(slash);
        }
        String pathInfo = null, queryString = null;
        int questionMark = path.indexOf('?');
        if (questionMark != -1) {
            queryString = path.substring(questionMark + 1);
            path = path.substring(0, questionMark);
        } else queryString = null;
        String servletPath = path, urlPath = path;
        W3Servlet w3servlet = null;
        ServletTarget servletTarget = (ServletTarget)extensionPool.match(path);
        if (servletTarget == null) servletTarget = (ServletTarget)pathPool.match(urlPath);
        if (servletTarget == null && !urlPath.endsWith("/")) servletTarget = (ServletTarget)pathPool.match(urlPath += "/");
        if (servletTarget != null) {
            w3servlet = servletTarget.w3servlet;
            if (!servletTarget.urlPattern.startsWith("*.")) {
                pathInfo = servletTarget.remainder;
                if (!pathInfo.startsWith("/")) pathInfo = "/" + pathInfo;
                servletPath = urlPath.substring(0, urlPath.length() - pathInfo.length());
            }
        } else {
            if (defaultServlet == null) return null;
            w3servlet = defaultServlet;
            servletPath = path;
            if (servletPath.endsWith("/")) servletPath = servletPath.substring(0, servletPath.length() - 1);
            pathInfo = null;
        }
        if (pathInfo != null && pathInfo.equals("")) pathInfo = null;
        if (w3servlet.isDisabled) return null;
        return new W3RequestDispatcher(this, w3servlet, contextPath, servletPath, pathInfo, queryString, path, false, basePath != null);
    }

    public Principal getUserPrincipal(W3Request w3request) {
        W3Session session = getSession(w3request, false);
        return session != null ? session.principal : null;
    }

    public boolean checkAuth(W3RequestDispatcher requestDispatcher, W3Request w3request, ServletResponse response)
        throws IOException, ServletException {
        String urlPath = requestDispatcher.servletPath + (requestDispatcher.pathInfo != null ? requestDispatcher.pathInfo : "");
        AuthTarget authTarget = (AuthTarget)authPool.match(urlPath);
        if (authTarget == null && !urlPath.endsWith("/")) authTarget = (AuthTarget)authPool.match(urlPath += "/");
        if (authTarget == null) authTarget = defaultAuthTarget;
        if (authTarget == null) return false;
        if (!authTarget.httpMethods.isEmpty() && !authTarget.httpMethods.contains(w3request.getMethod())) return false;
        if (authTarget.w3servlet != null) {
            authTarget.w3servlet.service(w3request, response, null, false, false);
            return false;
        }
        Principal userPrincipal = w3request.getUserPrincipal();
        if (userPrincipal != null) return false;
        W3Session session = getSession(w3request, true);
        session.uri = requestDispatcher.requestUri;
        getRequestDispatcher(formLoginPage).forward(w3request, response);
        return true;
    }

    public String toString() {
        return getClass().getName() +
            " {w3contextName=" + w3contextName +
            ", filterTargetList=" + filterTargetList +
            ", servletList=" + servletList +
            ", servletPatternMap=" + servletPatternMap + "}";
    }

    public W3Server getW3Server() {
        return w3server;
    }

}
