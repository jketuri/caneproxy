
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.awt.*;
import java.io.*;
import java.net.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ImageMapServlet extends HttpServlet {
    static final long serialVersionUID = 0L;

    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        Point point = new Point(0, 0);
        String s;
        if ((s = req.getQueryString()) != null) {
            StringTokenizer st = new StringTokenizer(s, ",");
            if (st.countTokens() != 2) throw new ServletException("Wrong number of arguments");
            try {
                point = new Point(Integer.parseInt(st.nextToken()), Integer.parseInt(st.nextToken()));
            } catch (NumberFormatException e) {
                throw new ServletException("Arguments in unknown format");
            }
        }
        boolean flag = false;
        Point point1, point2;
        String s1, s2, s3, s4;
        int i, j = Integer.MAX_VALUE;
        URL url = null, url1, url2 = null, url3;
        StringTokenizer st, st1;
        StreamTokenizer mt;
        Vector<Point> vector;
        if (point.x == 0 && point.y == 0) flag = true;
        mt = new StreamTokenizer(new InputStreamReader(req.getInputStream()));
        mt.eolIsSignificant(true);
        mt.wordChars(33, 33);
        mt.wordChars(35, 47);
        mt.wordChars(58, 59);
        mt.wordChars(61, 61);
        mt.wordChars(63, 64);
        mt.wordChars(95, 95);
        st = new StringTokenizer(req.getProtocol(), "/");
        s3 = (req.getPathInfo() == null) ? "/" : req.getPathInfo();
        if (req.getServerPort() != 80)
            url1 = new URL(st.nextToken() + "://" + req.getServerName() + ":" + req.getServerPort() + s3);
        else url1 = new URL(st.nextToken() + "://" + req.getServerName() + s3);
        try {
            while (mt.nextToken() != -1) {
                switch (mt.ttype) {
                case 10:
                    continue;
                case -3:
                    s = mt.sval;
                    if (s.startsWith("#"))
                        while (mt.ttype != 10 && mt.ttype != -1) mt.nextToken();
                    else if (s.equalsIgnoreCase("rect") || s.equalsIgnoreCase("rectangle")) {
                        mt.nextToken();
                        s1 = mt.sval;
                        url3 = new URL(url1, s1);
                        point1 = parsePoint(mt);
                        point2 = parsePoint(mt);
                        mt.nextToken();
                        if (flag) s3 = parseMenuText(mt, url3, s, s3);
                        else if (pointInRect(point, point1, point2)) {
                            sendResponse(res, url3);
                            return;
                        } while (mt.ttype != 10 && mt.ttype != -1) mt.nextToken();
                    } else if (s.equalsIgnoreCase("circle") || s.equalsIgnoreCase("circ")) {
                        mt.nextToken();
                        s1 = mt.sval;
                        url3 = new URL(url1, s1);
                        point1 = parsePoint(mt);
                        point2 = parsePoint(mt);
                        mt.nextToken();
                        if (flag) s3 = parseMenuText(mt, url3, s, s3);
                        else if (pointInCircle(point, point1, point2)) {
                            sendResponse(res, url3);
                            return;
                        } while (mt.ttype != 10 && mt.ttype != -1)
                              mt.nextToken();
                    } else if (s.equalsIgnoreCase("poly") || s.equalsIgnoreCase("polygon")) {
                        mt.nextToken();
                        s1 = mt.sval;
                        url3 = new URL(url1, s1);
                        mt.nextToken();
                        vector = new Vector<Point>(3);
                        while (mt.ttype == -2) {
                            mt.pushBack();
                            point1 = parsePoint(mt);
                            vector.addElement(point1);
                            mt.nextToken();
                        }
                        if (flag) s3 = parseMenuText(mt, url3, s, s3);
                        else if (pointInPoly(point, vector)) {
                            sendResponse(res, url3);
                            return;
                        } while (mt.ttype != 10 && mt.ttype != -1) i = mt.nextToken();
                    } else if (s.equalsIgnoreCase("point")) {
                        mt.nextToken();
                        s1 = mt.sval;
                        url3 = new URL(url1, s1);
                        point1 = parsePoint(mt);
                        if (distanceSquared(point, point1) < j) {
                            j = distanceSquared(point, point1);
                            url2 = url3;
                        }
                        if (flag) s3 = parseMenuText(mt, url3, s, s3);
                        while (mt.ttype != 10 && mt.ttype != -1) mt.nextToken();
                    } else if (s.equalsIgnoreCase("default")) {
                        mt.nextToken();
                        s1 = mt.sval;
                        url = new URL(url1, s1);
                        mt.nextToken();
                        if (flag) s3 = parseMenuText(mt, url, s, s3);
                        while (mt.ttype != 10 && mt.ttype != -1) mt.nextToken();
                    } else if (s.equalsIgnoreCase("base") || s.equalsIgnoreCase("base_uri")) {
                        mt.nextToken();
                        s1 = mt.sval;
                        s4 = (req.getPathInfo() == null) ? "/" : req.getPathInfo();
                        if (s1.equalsIgnoreCase("map")) {
                            st1 = new StringTokenizer(req.getProtocol(), "/");
                            if (req.getServerPort() != 80) url1 = new URL(st1.nextToken() + "://" + req.getServerName() + ":" + req.getServerPort() + s4);
                            else url1 = new URL(st1.nextToken() + "://" + req.getServerName() + s4);
                        } else if (s1.equalsIgnoreCase("referer")) {
                            s1 = req.getHeader("referer");
                            if (s1 != null) url1 = new URL(s1);
                        } else url1 = new URL(s1);
                        while (mt.ttype != 10 && mt.ttype != -1) mt.nextToken();
                    } else {
                        if (!s.equalsIgnoreCase("text")) throw new ServletException("Unknown type: " + mt.sval);
                        mt.nextToken();
                        if (flag && mt.ttype == 34) {
                            s3 += mt.sval;
                            mt.nextToken();
                        } while (mt.ttype != 10 && mt.ttype != -1) mt.nextToken();
                    }
                    break;
                default:
                    continue;
                }
            }
            if (flag) {
                sendMenuResponse(res, s3);
                return;
            }
            if (url2 != null) {
                sendResponse(res, url2);
                return;
            }
            sendResponse(res, url);
        } catch (Exception ex) {
            throw new ServletException(ex.getMessage() + " at line " + mt.lineno());
        }
    }

    protected static Point parsePoint(StreamTokenizer mt) throws ServletException, IOException {
        Point point = new Point(0, 0);
        mt.ordinaryChar(44);
        mt.nextToken();
        if (mt.ttype != -2) throw new ServletException("Number expected in mapfile line " + mt.lineno());
        point.x = (int)mt.nval;
        int i = mt.nextToken();
        if (i != 44) throw new ServletException("Comma expected in mapfile line " + mt.lineno());
        i = mt.nextToken();
        if (mt.ttype != -2) throw new ServletException("Number expected in mapfile line " + mt.lineno());
        point.y = (int)mt.nval;
        mt.wordChars(44, 44);
        return point;
    }

    protected static String parseMenuText(StreamTokenizer mt, URL url, String s, String s1) throws IOException {
        s1 += "<p><a href=\"" + url + "\">";
        if (mt.ttype == 34) {
            s1 += mt.sval;
            mt.nextToken();
        } else s1 += "(" + s + ") " + url;
        s1 += "</a>\n";
        return s1;
    }

    protected static final boolean pointInRect(Point point, Point point1, Point point2) {
        return point.x >= point1.x && point.x <= point2.x && point.y >= point1.y && point.y <= point2.y;
    }

    protected static final boolean pointInCircle(Point point, Point point1, Point point2) {
        return distanceSquared(point, point1) < distanceSquared(point2, point1);
    }

    protected static final int distanceSquared(Point point, Point point1) {
        return (point.x - point1.x) * (point.x - point1.x) + (point.y - point1.y) * (point.y - point1.y);
    }

    protected static boolean pointInPoly(Point point, Vector<Point> vector) {
        int an1[] = new int[vector.size()];
        int an2[] = new int[vector.size()];
        for (int i = 0; i < vector.size(); i++) {
            an1[i] = vector.elementAt(i).x;
            an2[i] = vector.elementAt(i).y;
        }
        return new Polygon(an1, an2, vector.size()).contains(point.x, point.y);
    }

    protected void sendMenuResponse(HttpServletResponse res, String s) throws IOException {
        ServletOutputStream out = res.getOutputStream();
        res.setContentType(Support.htmlType);
        out.println("<html>");
        out.println("<head><title>Imagemap Menu</title></head>");
        out.println("<body>");
        out.println("<h1>Imagemap Menu</h1>");
        out.println(s);
        out.println("</body>");
        out.println("</html>");
    }

    protected void sendResponse(HttpServletResponse res, URL url) throws IOException {
        if (url != null) res.sendRedirect(url.toString());
        else res.sendError(HttpURLConnection.HTTP_NOT_FOUND, "No default specified.");
    }

}
