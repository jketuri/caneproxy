
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Serves local and static HTTP-requests. */
public class CoreServlet extends HttpServlet {
    static final long serialVersionUID = 0L;
    private static String messagesBaseName = "FI.realitymodeler.server.resources.Messages";

    private ResourceBundle defaultMessages = null;
    private String cacheRoot = null;
    private String defaultContentType = null;
    private String metaInfRealPath = null;
    private String webInfRealPath = null;
    private int maxCacheEntryLength = -1;

    class ListFilter implements FilenameFilter {
        RegexpPool pool;

        ListFilter(String filter)
            throws RegexException {
            pool = new RegexpPool();
            pool.add(filter, Boolean.TRUE);
        }

        public boolean accept(File dir, String name) {
            return pool.match(name) != null;
        }

    }

    private final void setContentType(ServletContext servletContext, HttpServletResponse res, String fileName) {
        String mimeType = servletContext.getMimeType(fileName);
        if (mimeType == null) mimeType = defaultContentType;
        if (mimeType != null) res.setContentType(mimeType);
    }

    private final File getLinkFile(String parent, StringBuilder link, String prefix, String suffix,
                                   long number, long numberLength, boolean minus) {
        File linkFile = null;
        for (long delta = 1; delta <= 5; delta++) {
            link.setLength(0);
            link.append(String.valueOf(number + (minus ? - delta : delta)));
            while (link.length() < numberLength)
                link.insert(0, '0');
            link.insert(0, prefix);
            link.append(suffix);
            linkFile = new File(parent, link.toString());
            if (linkFile.exists()) break;
        }
        return linkFile;
    }

    public void init(ServletConfig config)
        throws ServletException {
        super.init(config);
        ServletContext servletContext = config.getServletContext();
        cacheRoot  = (String)servletContext.getAttribute(servletContext.getClass().getName() + "/cacheRoot");
        maxCacheEntryLength = Integer.parseInt((String)servletContext.getAttribute(servletContext.getClass().getName() + "/maxCacheEntryLength"));
        metaInfRealPath = servletContext.getRealPath("META-INF");
        webInfRealPath = servletContext.getRealPath("WEB-INF");
        if (metaInfRealPath != null && metaInfRealPath.endsWith(File.separator)) metaInfRealPath = metaInfRealPath.substring(0, metaInfRealPath.length() - 1);
        if (webInfRealPath != null && webInfRealPath.endsWith(File.separator)) webInfRealPath = webInfRealPath.substring(0, webInfRealPath.length() - 1);
        defaultContentType = servletContext.getMimeType("*");
        defaultMessages = W3URLConnection.getMessages(messagesBaseName);
    }

    public void service(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException {
        W3Request w3r = (W3Request)req.getAttribute("");
        ServletContext servletContext = getServletContext();
        /*
          if (req.getPathInfo() != null) {
          RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(req.getPathInfo());
          requestDispatcher.forward(req, res);
          return;
          }
        */
        String servletPath = null, pathInfo = null, requestURI = (String)req.getAttribute("javax.servlet.include.request_uri");
        if (requestURI != null) {
            servletPath = (String)req.getAttribute("javax.servlet.include.servlet_path");
            pathInfo = (String)req.getAttribute("javax.servlet.include.servlet_path");
        } else {
            requestURI = req.getRequestURI();
            servletPath = req.getServletPath();
            pathInfo = req.getPathInfo();
        }
        String path = servletPath + (pathInfo != null ? pathInfo : ""), pathTranslated = servletContext.getRealPath(path);
        if (pathTranslated == null || webInfRealPath != null && (pathTranslated.regionMatches(true, 0, webInfRealPath, 0, webInfRealPath.length()) || pathTranslated.equalsIgnoreCase(webInfRealPath)) || metaInfRealPath != null && (pathTranslated.regionMatches(true, 0, metaInfRealPath, 0, metaInfRealPath.length()) || pathTranslated.equalsIgnoreCase(metaInfRealPath))) {
            res.sendError(HttpServletResponse.SC_NOT_FOUND, "Not Found " + requestURI);
            return;
        }
        boolean cache = cacheRoot != null && pathTranslated.startsWith(cacheRoot);
        String canPath = new File(cache ? pathTranslated : Support.decodePath(pathTranslated)).getCanonicalPath(), method = req.getMethod();
        W3File file = new W3File(canPath), file1 = file;
        boolean exists = file.exists(), isDir = exists && (file.isDirectory() || !file.isFile());
        if (isDir && !canPath.endsWith(File.separator)) canPath += File.separator;
        String canPath1 = canPath;
        byte buffer[] = new byte[Support.bufferLength];
        if (method.equals("GET") || method.equals("FILTER") || method.equals("HEAD")) {
            String s;
            boolean mobile = (s = req.getHeader("user-agent")) != null && s.trim().equalsIgnoreCase("mobile");
            if (isDir) {
                if (!requestURI.endsWith("/") && w3r.invokeNumber == 0) {
                    // URL doesn't end with a slash (links can be broken)
                    res.sendRedirect(requestURI.substring(requestURI.lastIndexOf('/') + 1) + "/");
                    return;
                }
                exists = false;
                List welcomeFileList = ((W3Context)servletContext).welcomeFileList;
                if (welcomeFileList != null) {
                    Iterator welcomeFiles = welcomeFileList.iterator();
                    while (welcomeFiles.hasNext()) {
                        String welcomeFile = (String)welcomeFiles.next();
                        file = new W3File(canPath = canPath1 + welcomeFile);
                        if (file.exists()) {
                            if (!path.endsWith("/")) path += "/";
                            req.getRequestDispatcher(path + welcomeFile).forward(req, res);
                            return;
                        }
                    }
                }
                if (!exists) {
                    // index files not found or proxy cache directory, returning directory
                    if (welcomeFileList != null) {
                        Iterator welcomeFiles = welcomeFileList.iterator();
                        while (welcomeFiles.hasNext())
                            W3Server.filePool.remove(canPath1 + welcomeFiles.next());
                    }
                    String filter = req.getParameter("filter");
                    ListFilter listFilter;
                    if (filter != null)
                        try {
                            listFilter = new ListFilter(filter);
                        } catch (RegexException ex) {
                            throw new ServletException(Support.stackTrace(ex));
                        } else listFilter = null;
                    res.setStatus(HttpServletResponse.SC_OK);
                    res.setContentType(Support.htmlType);
                    res.setHeader("Cache-Control", "no-cache");
                    res.setHeader("Pragma", "no-cache");
                    String target = req.getParameter("target");
                    PrintWriter writer = res.getWriter();
                    ResourceBundle messages = W3URLConnection.getLanguage(req, res, defaultMessages, messagesBaseName);
                    String list[] = file1.list(listFilter),
                        lastDir = requestURI.endsWith("/") ? "" :
                        requestURI.substring(requestURI.lastIndexOf('/') + 1) + "/";
                    s = Support.htmlString(Support.decodePath(requestURI));
                    writer.println("<html><head>");
                    writer.println("<style type=\"text/css\">");
                    writer.println("span.listcolumn0 { width: 15em; clear: left; float: left }");
                    writer.println("span.listcolumn { width: 15em; float: left }");
                    writer.println("</style>");
                    writer.println("<title>" + s + "</title></head><body>");
                    writer.println("<h1>" + MessageFormat.format(messages.getString("directoryOfFile"), new Object[] {s}) + "</h1>");
                    String dirTarget = target != null ? ";target=" + target : "";
                    if (!requestURI.equals("/")) {
                        String parentDir = requestURI.endsWith("/") ? requestURI.substring(0, requestURI.lastIndexOf('/', requestURI.length() - 2) + 1) : requestURI.substring(0, requestURI.lastIndexOf('/') + 1);
                        writer.println("<a href=\"" + parentDir + dirTarget + "\">" + messages.getString("parentDirectory") + "</a><p>\n");
                    }
                    long numberOfFiles = 0L;
                    for (int i = 0; i < list.length; i++) {
                        file = new W3File(file1, list[i]);
                        String canonicalPath = file.getCanonicalPath();
                        if (webInfRealPath != null && canonicalPath.equalsIgnoreCase(webInfRealPath) || metaInfRealPath != null && canonicalPath.equalsIgnoreCase(metaInfRealPath)) continue;
                        String size, target1, target2;
                        if (file.isDirectory()) {
                            size = "Directory";
                            target1 = dirTarget;
                            target2 = null;
                            list[i] += "/";
                        } else {
                            size = String.valueOf(file.length());
                            try {
                                if (file.isOffline()) size = "(" + size + ")";
                            } catch (IOException ex) {
                                size = "[" + size + "]";
                            }
                            target1 = "";
                            target2 = target;
                        }
                        writer.print("<span class=\"listcolumn0\">");
                        W3URLConnection.writeLine(writer, new Date(file.lastModified()), size, lastDir + Support.encodePath(list[i]) + target1, target2, Support.htmlString(list[i]), mobile);
                        numberOfFiles++;
                    }
                    writer.println(MessageFormat.format(messages.getString("numberFiles"), new Object[] {new Long(numberOfFiles)}));
                    writer.println("</body></html>");
                    return;
                } else exists = true;
            }
            if (!exists) {
                // no file nor directory found
                W3Server.filePool.remove(canPath);
                if (isDir) {
                    List welcomeFileList = ((W3Context)servletContext).welcomeFileList;
                    if (welcomeFileList != null) {
                        Iterator welcomeFiles = welcomeFileList.iterator();
                        while (welcomeFiles.hasNext())
                            W3Server.filePool.remove(canPath + welcomeFiles.next());
                    }
                }
                res.sendError(HttpServletResponse.SC_NOT_FOUND, "Not Found " + requestURI);
                return;
            }
            if (req.getParameter("image") != null || req.getParameter("show") != null) {
                String name = file.getName(), prefix = null, suffix = null;
                StringBuilder builder = new StringBuilder();
                int length = name.length();
                for (int index = 0; index < length; index++)
                    if (Character.isDigit(name.charAt(index))) {
                        if (builder.length() == 0) prefix = name.substring(0, index);
                        builder.append(name.charAt(index));
                    } else if (builder.length() > 0) {
                        suffix = name.substring(index);
                        break;
                    }
                File nextFile = null, previousFile = null;
                StringBuilder nextLink = new StringBuilder(),
                    previousLink = new StringBuilder();
                if (builder.length() > 0) {
                    String delay = req.getParameter("delay");
                    if (delay == null) delay = "4";
                    long number = Long.parseLong(builder.toString()),
                        numberLength = builder.toString().length();
                    String parent = file.getParent();
                    previousFile = getLinkFile(parent, previousLink, prefix, suffix, number, numberLength, true);
                    nextFile = getLinkFile(parent, nextLink, prefix, suffix, number, numberLength, false);
                    if (nextFile.exists() && req.getParameter("show") != null) {
                        res.setHeader("Refresh", delay + "; URL=" + nextLink.toString() + "?" + (req.getParameter("image") != null ? "image=1&" : "")
                                      + "show=1&delay=" + delay);
                    }
                }
                if (builder.length() > 0 && req.getParameter("image") != null) {
                    res.setStatus(HttpServletResponse.SC_OK);
                    res.setContentType(Support.htmlType);
                    PrintWriter writer = res.getWriter();
                    writer.println("<html><head>");
                    writer.println("<title>" + name + "</title></head><body>");
                    writer.println("<img src=\"" + name + "\" width=\"100%\" height=\"auto\""
                                   + (req.getParameter("flip") != null ? " style=\"filter: flipv;\"" : "") + "></img>");
                    writer.println((previousFile.exists() ? "<a href=\"" + previousLink.toString() + "?image=1\">" + previousLink.toString() + "</a>" : "")
                                   + (nextFile.exists() ? (previousFile.exists() ? " " : "") + "<a href=\"" + nextLink.toString() + "?image=1\">" + nextLink.toString() + "</a>" : ""));
                    writer.println("</body></html>");
                    return;
                }
            }
            // returning file
            CacheFile cacheFile = null;
            long longValues[] = new long[11];
            file.info(null, longValues);
            long contentLength = longValues[file.ST_SIZE],
                lastModified = longValues[file.ST_MTIME] * 1000L;
            res.setStatus(HttpServletResponse.SC_OK);
            if (!cache && (cacheFile = W3Server.filePool.get(canPath)) != null &&
                contentLength == cacheFile.data.length && lastModified == cacheFile.lastModified) {
                // file comes from memory cache
                res.setHeader("Content-Length", String.valueOf(contentLength));
                res.setDateHeader("Last-Modified", lastModified);
                setContentType(servletContext, res, file.getName());
                String protocol = req.getProtocol();
                if (protocol != null && protocol.compareTo(W3URLConnection.protocol_1_1) >= 0) {
                    res.setHeader("ETag", cacheFile.entityTag);
                    res.setHeader("Accept-Ranges", "bytes");
                }
                OutputStream out = res.getOutputStream();
                out.write(cacheFile.data);
                req.setAttribute("FI.realitymodeler.server.W3Request/cached", Boolean.TRUE);
            } else {
                // file comes from mass memory
                BufferedInputStream in = null;
                FileInputStream fin = null;
                try {
                    fin = new FileInputStream(file);
                    in = new BufferedInputStream(fin);
                    if (cache && W3URLConnection.cacheHeaders)
                        try {
                            CountInputStream cin = new CountInputStream(in);
                            Iterator headerItems = new HeaderList(cin).iterator();
                            while (headerItems.hasNext()) {
                                Header header = (Header)headerItems.next();
                                res.setHeader(header.getName(), header.getValue());
                            }
                            contentLength -= cin.getCount();
                        } catch (ParseException ex) {
                            throw new ServletException(Support.stackTrace(ex));
                        } else setContentType(servletContext, res, file.getName());
                    String entityTag = "\"" +
                        Long.toString(lastModified, Character.MAX_RADIX) + "-" +
                        Long.toString(contentLength, Character.MAX_RADIX) + "\"";
                    res.setHeader("Content-Length", String.valueOf(contentLength));
                    res.setDateHeader("Last-Modified", lastModified);
                    String protocol = req.getProtocol();
                    if (protocol != null && protocol.compareTo(W3URLConnection.protocol_1_1) >= 0) {
                        res.setHeader("ETag", entityTag);
                        res.setHeader("Accept-Ranges", "bytes");
                    }
                    OutputStream out = res.getOutputStream();
                    if ((maxCacheEntryLength == -1 && contentLength < (long)Integer.MAX_VALUE || contentLength <= (long)maxCacheEntryLength) && !cache) {
                        // file goes to memory cache
                        byte data[] = new byte[(int)contentLength];
                        int offset = 0;
                        for (int n; (n = in.read(data, offset, Math.min(Support.bufferLength, (int)contentLength - offset))) > 0; offset += n)
                            out.write(data, offset, n);
                        cacheFile = new CacheFile();
                        cacheFile.data = data;
                        cacheFile.lastModified = lastModified;
                        cacheFile.entityTag = entityTag;
                        W3Server.filePool.put(canPath, cacheFile);
                    }
                    // file is too large for memory cache or it's proxy cache file
                    else for (int n; (n = in.read(buffer)) > 0;) out.write(buffer, 0, n);
                } finally {
                    if (in != null) in.close();
                    else if (fin != null) fin.close();
                }
            }
        } else if (method.equals("PUT")) {
            if (w3r.accountName == null) {
                res.sendError(HttpServletResponse.SC_FORBIDDEN, "Forbidden to access " + requestURI);
                return;
            }
            FileOutputStream fout = new FileOutputStream(file);
            InputStream in = req.getInputStream();
            try {
                for (int n; (n = in.read(buffer)) > 0;) fout.write(buffer, 0, n);
            } catch (IOException ex) {
                fout.close();
                file.delete();
                throw ex;
            }
            fout.close();
            res.setStatus(HttpServletResponse.SC_NO_CONTENT);
        } else if (method.equals("OPTIONS")) {
            res.setContentLength(0);
            res.getOutputStream();
        } else if (method.equals("TRACE")) {
            Enumeration headers = w3r.getHeaders();
            while (headers.hasMoreElements()) {
                Header header = (Header)headers.nextElement();
                res.addHeader(header.getName(), header.getValue());
            }
        } else res.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "Method Not Allowed " + method);
    }

}
