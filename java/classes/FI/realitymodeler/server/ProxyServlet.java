
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.math.*;
import java.net.*;
import java.text.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Serves requests to remote Internet servers. */
public class ProxyServlet extends HttpServlet {
    static final long serialVersionUID = 0L;
    static final Object DISABLED = new Object[0];
    static boolean disableProxyPersistentConnection = false;
    static boolean disableProxyPipelining = true;
    static Set<String> sendingMethods = new HashSet<String>();
    static {
        try {
            W3Factory.prepare();
            sendingMethods.add("MKCOL");
            sendingMethods.add("POST");
            sendingMethods.add("PUT");
            sendingMethods.add("PROPFIND");
            sendingMethods.add("PROPPATCH");
            W3URLConnection.setCacheHeaders(true);
            new W3FtpURLConnection(new URL("ftp", "127.0.0.1", "")).setDefaultUseCaches(false);
            new W3GopherURLConnection(new URL("gopher", "127.0.0.1", "")).setDefaultUseCaches(false);
            new W3HttpURLConnection(new URL("http", "127.0.0.1", "")).setDefaultUseCaches(false);
            new W3Imap4URLConnection(new URL("imap4", "127.0.0.1", "")).setDefaultUseCaches(false);
            new W3LdapURLConnection(new URL("ldap", "127.0.0.1", "")).setDefaultUseCaches(false);
            new W3NntpURLConnection(new URL("nntp", "127.0.0.1", "")).setDefaultUseCaches(false);
            new W3Pop3URLConnection(new URL("pop3", "127.0.0.1", "")).setDefaultUseCaches(false);
            new W3WaisURLConnection(new URL("wais", "127.0.0.1", "")).setDefaultUseCaches(false);
        } catch (Exception ex) {
            throw new Error(ex.toString());
        }
    }

    Map<String, Object> proxyCaches = null;
    ServletContext servletContext;
    String serverID = null;
    String sslProxyHost = null;
    boolean autoCaches = false;
    boolean autoResume = false;
    boolean checkCaches = false;
    boolean forceCaches = false;
    boolean verbose = false;
    int connectTimeout = 0, timeout = 0;
    int keepAliveCount = 0;
    int maxProxyConnections = -1;
    int sslProxyPort = 80;

    public static Map<String,Object> makeProxyCaches() {
        Map<String,Object> proxyCaches = new HashMap<String,Object>();
        proxyCaches.put("ftp", Boolean.FALSE);
        proxyCaches.put("gopher", Boolean.FALSE);
        proxyCaches.put("http", Boolean.FALSE);
        proxyCaches.put("https", Boolean.FALSE);
        proxyCaches.put("imap4", Boolean.FALSE);
        proxyCaches.put("msg", Boolean.FALSE);
        proxyCaches.put("nntp", Boolean.FALSE);
        proxyCaches.put("pop3", Boolean.FALSE);
        proxyCaches.put("smtp", Boolean.FALSE);
        proxyCaches.put("wais", Boolean.FALSE);
        return proxyCaches;
    }

    public static void readProxyCacheParameters(Map<String,Object> proxyCaches, String dataDirectory)
        throws ParseException {
        String schemes[] = {"ftp", "gopher", "http"};
        File f = null;
        StreamTokenizer sTok = null;
        for (int num = 0; num < schemes.length; num++)
            try {
                Object obj = proxyCaches.get(schemes[num]);
                if (obj != Boolean.FALSE && obj != DISABLED &&
                    (f = new File(dataDirectory, schemes[num] + "_cache_paths")).exists()) {
                    RegexpPool pool = new RegexpPool();
                    TreeMap<String,String> map = new TreeMap<String,String>(Support.stringComparator);
                    proxyCaches.put(schemes[num], new Object[] {pool, map});
                    BufferedReader bf = new BufferedReader(new FileReader(f));
                    try {
                        sTok = new StreamTokenizer(bf);
                        sTok.resetSyntax();
                        sTok.whitespaceChars(0, 32);
                        sTok.wordChars(33, 255);
                        sTok.commentChar('#');
                        sTok.quoteChar('"');
                        while (sTok.nextToken() != sTok.TT_EOF) {
                            pool.add(sTok.sval, sTok.sval);
                            map.put(sTok.sval, sTok.sval);
                        }
                    } finally {
                        bf.close();
                    }
                }
            } catch (Exception ex) {
                if (f != null && sTok != null) throw new ParseException("Error in file " + f.getPath() + " in line " +
                                                                        sTok.lineno() + "\n" + Support.stackTrace(ex), sTok.lineno());
                throw new ParseException(Support.stackTrace(ex), 0);
            }
    }

    public static boolean setProxyParameter(String name, String value, ServletContext servletContext, Map<String,Object> proxyCaches)
        throws ParseException, MalformedURLException {
        if (name.equals("autoCaches")) {
            if (Support.isTrue(value, name)) servletContext.setAttribute(servletContext.getClass().getName() + "/autoCaches", Boolean.TRUE);
        } else if (name.equals("autoResume") || name.equals("autoRecovery")) {
            if (Support.isTrue(value, name)) servletContext.setAttribute(servletContext.getClass().getName() + "/autoResume", Boolean.TRUE);
        } else if (name.equals("cacheRoot")) servletContext.setAttribute(servletContext.getClass().getName() + "/cacheRoot", value);
        else if (name.equals("checkCaches")) {
            if (Support.isTrue(value, name)) servletContext.setAttribute(servletContext.getClass().getName() + "/checkCaches", Boolean.TRUE);
        } else if (name.equals("dataDirectory")) servletContext.setAttribute(servletContext.getClass().getName() + "/dataDirectory", value);
        else if (name.equals("disableProxyPersistentConnection")) {
            if (Support.isTrue(value, name)) servletContext.setAttribute(servletContext.getClass().getName() + "/disableProxyPersistentConnection", Boolean.TRUE);
        } else if (name.equals("disableProxyPipelining")) {
            if (Support.isTrue(value, name)) servletContext.setAttribute(servletContext.getClass().getName() + "/disableProxyPipelining", Boolean.TRUE);
        } else if (name.equals("forceCaches")) {
            if (Support.isTrue(value, name)) servletContext.setAttribute(servletContext.getClass().getName() + "/forceCaches", Boolean.TRUE);
        } else if (name.equals("ftpProxyCache")) {
            if (Support.isTrue(value, name)) {
                proxyCaches.put("ftp", Boolean.TRUE);
                new W3FtpURLConnection(new URL("ftp", "127.0.0.1", "")).setDefaultUseCaches(true);
            }
        } else if (name.equals("ftpProxyCacheCleaningInterval")) {
            long longValue = Long.parseLong(value);
            W3FtpURLConnection.cacheCleaningInterval = longValue >= 0L ? longValue * 60L * 1000L : -1L;
        } else if (name.equals("ftpProxyCacheRefreshingInterval")) {
            long longValue = Long.parseLong(value);
            W3FtpURLConnection.cacheRefreshingInterval = longValue >= 0L ? longValue * 60L * 1000L : -1L;
        } else if (name.equals("ftpProxyDisabled")) {
            if (Support.isTrue(value, name))
                proxyCaches.put("ftp", DISABLED);
        } else if (name.equals("ftpProxyHost")) {
            if (!"".equals(value)) {
                W3FtpURLConnection.proxyHost = value;
                W3FtpURLConnection.useProxy = true;
            }
        } else if (name.equals("ftpProxyPort")) W3FtpURLConnection.proxyPort = Integer.parseInt(value);
        else if (name.equals("gopherProxyCache")) {
            if (Support.isTrue(value, name)) {
                proxyCaches.put("gopher", Boolean.TRUE);
                new W3GopherURLConnection(new URL("gopher", "127.0.0.1", "")).setDefaultUseCaches(true);
            }
        } else if (name.equals("gopherProxyCacheCleaningInterval")) {
            long longValue = Long.parseLong(value);
            W3GopherURLConnection.cacheCleaningInterval = longValue >= 0L ? longValue * 60L * 1000L : -1L;
        } else if (name.equals("gopherProxyCacheRefreshingInterval")) {
            long longValue = Long.parseLong(value);
            W3GopherURLConnection.cacheRefreshingInterval = longValue >= 0L ? longValue * 60L * 1000L : -1L;
        } else if (name.equals("gopherProxyDisabled")) {
            if (Support.isTrue(value, name))
                proxyCaches.put("gopher", DISABLED);
        } else if (name.equals("gopherProxyHost")) {
            if (!"".equals(value)) {
                W3GopherURLConnection.proxyHost = value;
                W3GopherURLConnection.useProxy = true;
            }
        } else if (name.equals("gopherProxyPort")) W3GopherURLConnection.proxyPort = Integer.parseInt(value);
        else if (name.equals("httpProxyCache")) {
            if (Support.isTrue(value, name)) {
                proxyCaches.put("http", Boolean.TRUE);
                new W3HttpURLConnection(new URL("http", "127.0.0.1", "")).setDefaultUseCaches(true);
            }
        } else if (name.equals("httpProxyCacheCleaningInterval")) {
            long longValue = Long.parseLong(value);
            W3HttpURLConnection.cacheCleaningInterval = longValue >= 0L ? longValue * 60L * 1000L : -1L;
        } else if (name.equals("httpProxyCacheRefreshingInterval")) {
            long longValue = Long.parseLong(value);
            W3HttpURLConnection.cacheRefreshingInterval = longValue >= 0L ? longValue * 60L * 1000L : -1L;
        } else if (name.equals("httpProxyDisabled")) {
            if (Support.isTrue(value, name))
                proxyCaches.put("http", DISABLED);
        } else if (name.equals("httpProxyHost")) {
            if (!"".equals(value)) {
                W3HttpURLConnection.proxyHost = value;
                W3HttpURLConnection.useProxy = true;
            }
        } else if (name.equals("httpProxyPort")) W3HttpURLConnection.proxyPort = Integer.parseInt(value);
        else if (name.equals("imap4ProxyCache")) {
            if (Support.isTrue(value, name)) {
                proxyCaches.put("imap4", Boolean.TRUE);
                new W3Imap4URLConnection(new URL("imap4", "127.0.0.1", "")).setDefaultUseCaches(true);
            }
        } else if (name.equals("imap4CacheCleaningInterval")) {
            long longValue = Long.parseLong(value);
            W3Imap4URLConnection.cacheCleaningInterval = longValue >= 0L ? longValue * 60L * 1000L : -1L;
        } else if (name.equals("imap4ProxyDisabled")) {
            if (Support.isTrue(value, name))
                proxyCaches.put("imap4", DISABLED);
        } else if (name.equals("imap4ProxyHost")) {
            if (!"".equals(value)) {
                W3Imap4URLConnection.proxyHost = value;
                W3Imap4URLConnection.useProxy = true;
            }
        } else if (name.equals("imap4ProxyPort")) W3Imap4URLConnection.proxyPort = Integer.parseInt(value);
        else if (name.equals("imap4ServerHost")) W3Imap4URLConnection.imap4ServerHost = value;
        else if (name.equals("ldapProxyCache")) {
            if (Support.isTrue(value, name)) {
                proxyCaches.put("ldap", Boolean.TRUE);
                new W3FtpURLConnection(new URL("ldap", "127.0.0.1", "")).setDefaultUseCaches(true);
            }
        } else if (name.equals("ldapProxyDisabled")) {
            if (Support.isTrue(value, name))
                proxyCaches.put("ldap", DISABLED);
        } else if (name.equals("maxProxyCacheEntryLength")) {
            int intValue = Integer.parseInt(value);
            W3URLConnection.maxCacheEntryLength = intValue >= 0 ? intValue * 1024 : -1;
        } else if (name.equals("maxProxyCacheEntryLength")) W3URLConnection.maxCacheEntryLength = Integer.parseInt(value);
        else if (name.equals("maxProxyConnections")) servletContext.setAttribute(servletContext.getClass().getName() + "/maxProxyConnections", new Integer(value));
        else if (name.equals("mobileServerHost")) {
            if (!"".equals(value)) {
                W3FtpURLConnection.proxyHost = value;
                W3FtpURLConnection.useProxy = true;
                W3GopherURLConnection.proxyHost = value;
                W3GopherURLConnection.useProxy = true;
                W3HttpURLConnection.proxyHost = value;
                W3HttpURLConnection.useProxy = true;
                W3Imap4URLConnection.proxyHost = value;
                W3Imap4URLConnection.useProxy = true;
                W3MsgURLConnection.proxyHost = value;
                W3MsgURLConnection.useProxy = true;
                W3NntpURLConnection.proxyHost = value;
                W3NntpURLConnection.useProxy = true;
                W3Pop3URLConnection.proxyHost = value;
                W3Pop3URLConnection.useProxy = true;
                W3SmtpURLConnection.proxyHost = value;
                W3SmtpURLConnection.useProxy = true;
                W3WaisURLConnection.proxyHost = value;
                W3WaisURLConnection.useProxy = true;
                W3URLConnection.useMobile = true;
            }
        } else if (name.equals("mobileServerPort")) {
            int port = Integer.parseInt(value);
            W3FtpURLConnection.proxyPort = port;
            W3GopherURLConnection.proxyPort = port;
            W3HttpURLConnection.proxyPort = port;
            W3NntpURLConnection.proxyPort = port;
            W3Pop3URLConnection.proxyPort = port;
            W3SmtpURLConnection.proxyPort = port;
            W3WaisURLConnection.proxyPort = port;
        } else if (name.equals("msgCacheCleaningInterval")) {
            long longValue = Long.parseLong(value);
            W3MsgURLConnection.cacheCleaningInterval = longValue >= 0L ? longValue * 60L * 1000L : -1L;
        } else if (name.equals("msgProxyDisabled")) {
            if (Support.isTrue(value, name))
                proxyCaches.put("msg", DISABLED);
        } else if (name.equals("msgProxyHost")) {
            if (!"".equals(value)) {
                W3MsgURLConnection.proxyHost = value;
                W3MsgURLConnection.useProxy = true;
            }
        } else if (name.equals("msgProxyPort")) W3MsgURLConnection.proxyPort = Integer.parseInt(value);
        else if (name.equals("msgServerHost")) W3MsgURLConnection.msgServerHost = value;
        else if (name.equals("nntpCacheCleaningInterval")) {
            long longValue = Long.parseLong(value);
            W3NntpURLConnection.cacheCleaningInterval = longValue >= 0L ? longValue * 60L * 1000L : -1L;
        } else if (name.equals("nntpProxyCache")) {
            if (Support.isTrue(value, name)) {
                proxyCaches.put("nntp", Boolean.TRUE);
                new W3NntpURLConnection(new URL("nntp", "127.0.0.1", "")).setDefaultUseCaches(true);
            }
        } else if (name.equals("nntpProxyDisabled")) {
            if (Support.isTrue(value, name))
                proxyCaches.put("nntp", DISABLED);
        } else if (name.equals("nntpProxyHost")) {
            if (!"".equals(value)) {
                W3NntpURLConnection.proxyHost = value;
                W3NntpURLConnection.useProxy = true;
            }
        } else if (name.equals("nntpProxyPort")) W3NntpURLConnection.proxyPort = Integer.parseInt(value);
        else if (name.equals("nntpServerHost")) W3NntpURLConnection.nntpServerHost = value;
        else if (name.equals("noCacheHeaders")) W3URLConnection.setCacheHeaders(!Support.isTrue(value, name));
        else if (name.equals("pop3CacheCleaningInterval")) {
            long longValue = Long.parseLong(value);
            W3Pop3URLConnection.cacheCleaningInterval = longValue >= 0L ? longValue * 60L * 1000L : -1L;
        } else if (name.equals("pop3ProxyCache")) {
            if (Support.isTrue(value, name)) {
                proxyCaches.put("pop3", Boolean.TRUE);
                new W3Pop3URLConnection(new URL("pop3", "127.0.0.1", "")).setDefaultUseCaches(true);
            }
        } else if (name.equals("pop3ProxyDisabled")) {
            if (Support.isTrue(value, name))
                proxyCaches.put("pop3", DISABLED);
        } else if (name.equals("pop3ProxyHost")) {
            if (!"".equals(value)) {
                W3Pop3URLConnection.proxyHost = value;
                W3Pop3URLConnection.useProxy = true;
            }
        } else if (name.equals("pop3ProxyPort")) W3Pop3URLConnection.proxyPort = Integer.parseInt(value);
        else if (name.equals("pop3ServerHost")) W3Pop3URLConnection.pop3ServerHost = value;
        else if (name.equals("proxyConnectTimeoutSecs"))
            servletContext.setAttribute(servletContext.getClass().getName() + "/proxyConnectTimeoutSecs", new Integer(value));
        else if (name.equals("proxyKeepAliveCount"))
            servletContext.setAttribute(servletContext.getClass().getName() + "/proxyKeepAliveCount", new Integer(value));
        else if (name.equals("proxyTimeoutSecs"))
            servletContext.setAttribute(servletContext.getClass().getName() + "/proxyTimeoutSecs", new Integer(value));
        else if (name.equals("serverID")) servletContext.setAttribute(servletContext.getClass().getName() + "/serverID", value);
        else if (name.equals("smtpProxyDisabled")) {
            if (Support.isTrue(value, name))
                proxyCaches.put("smtp", DISABLED);
        } else if (name.equals("smtpProxyHost")) {
            if (!"".equals(value)) {
                W3SmtpURLConnection.proxyHost = value;
                W3SmtpURLConnection.useProxy = true;
            }
        } else if (name.equals("smtpProxyPort")) W3SmtpURLConnection.proxyPort = Integer.parseInt(value);
        else if (name.equals("smtpServerHost")) W3SmtpURLConnection.smtpServerHost = value;
        else if (name.equals("verbose")) {
            if (Support.isTrue(value, name)) servletContext.setAttribute(servletContext.getClass().getName() + "/verbose", Boolean.TRUE);
        } else if (name.equals("waisProxyCache")) {
            if (Support.isTrue(value, name)) {
                proxyCaches.put("wais", Boolean.TRUE);
                new W3WaisURLConnection(new URL("wais", "127.0.0.1", "")).setDefaultUseCaches(true);
            }
        } else if (name.equals("waisProxyDisabled")) {
            if (Support.isTrue(value, name))
                proxyCaches.put("wais", DISABLED);
        } else if (name.equals("waisProxyHost")) {
            if (!"".equals(value)) {
                W3WaisURLConnection.proxyHost = value;
                W3WaisURLConnection.useProxy = true;
            }
        } else if (name.equals("waisProxyPort")) W3WaisURLConnection.proxyPort = Integer.parseInt(value);
        else return false;
        return true;
    }

    public void init(ServletConfig config)
        throws ServletException {
        super.init(config);
        servletContext = config.getServletContext();
        proxyCaches = (Map<String, Object>)servletContext.getAttribute(servletContext.getClass().getName() + "/proxyCaches");
        if (proxyCaches == null) proxyCaches = makeProxyCaches();
        Integer connectTimeoutSecsValue = (Integer)servletContext.getAttribute(servletContext.getClass().getName() + "/proxyConnectTimeoutSecs");
        connectTimeout = connectTimeoutSecsValue != null ? connectTimeoutSecsValue.intValue() * 1000 : 5000;
        Integer keepAliveCountValue = (Integer)servletContext.getAttribute(servletContext.getClass().getName() + "/proxyKeepAliveCount");
        keepAliveCount = keepAliveCountValue != null ? keepAliveCountValue.intValue() : 0;
        Integer timeoutSecsValue = (Integer)servletContext.getAttribute(servletContext.getClass().getName() + "/proxyTimeoutSecs");
        timeout = timeoutSecsValue != null ? timeoutSecsValue.intValue() * 1000 : 0;
        Long requestTimeoutSecsValue = (Long)servletContext.getAttribute(servletContext.getClass().getName() + "/requestTimeoutSecs");
        W3URLConnection.useCachePool(requestTimeoutSecsValue != null ? requestTimeoutSecsValue.longValue() * 1000L : 0L);
        Enumeration initParameterNames = config.getInitParameterNames();
        if (initParameterNames != null)
            while (initParameterNames.hasMoreElements()) {
                Exception ex = null;
                try {
                    String name = (String)initParameterNames.nextElement(), value = config.getInitParameter(name);
                    if (!setProxyParameter(name, value, servletContext, proxyCaches)) throw new ServletException("Unknown initialization parameter " + name);
                } catch (ParseException ex1) {
                    ex = ex1;
                } catch (MalformedURLException ex1) {
                    ex = ex1;
                }
                if (ex != null) throw new ServletException(ex.toString());
            }
        if (servletContext.getAttribute(servletContext.getClass().getName() + "/verbose") == Boolean.TRUE) verbose = true;
        serverID = (String)servletContext.getAttribute(servletContext.getClass().getName() + "/serverID");
        Integer integer = (Integer)servletContext.getAttribute(servletContext.getClass().getName() + "/maxProxyConnections");
        if (integer != null) maxProxyConnections = integer.intValue();
        if (servletContext.getAttribute(servletContext.getClass().getName() + "/autoCaches") == Boolean.TRUE) autoCaches = true;
        if (servletContext.getAttribute(servletContext.getClass().getName() + "/autoResume") == Boolean.TRUE) autoResume = true;
        if (servletContext.getAttribute(servletContext.getClass().getName() + "/checkCaches") == Boolean.TRUE) checkCaches = true;
        if (servletContext.getAttribute(servletContext.getClass().getName() + "/disableProxyPersistentConnection") == Boolean.TRUE) ProxyServlet.disableProxyPersistentConnection = true;
        if (servletContext.getAttribute(servletContext.getClass().getName() + "/disableProxyPipelining") == Boolean.TRUE) ProxyServlet.disableProxyPipelining = true;
        if (servletContext.getAttribute(servletContext.getClass().getName() + "/forceCaches") == Boolean.TRUE) forceCaches = true;
        String cacheRoot  = (String)servletContext.getAttribute(servletContext.getClass().getName() + "/cacheRoot"),
            dataDirectory = (String)servletContext.getAttribute(servletContext.getClass().getName() + "/dataDirectory");
        if (dataDirectory == null) throw new ServletException("Data directory not specified");
        while ((dataDirectory = dataDirectory.trim()).endsWith("/")) dataDirectory = dataDirectory.substring(0, dataDirectory.length() - 1);
        if (cacheRoot == null) cacheRoot = dataDirectory + "/cache/";
        else if (!(cacheRoot = cacheRoot.trim()).endsWith("/")) cacheRoot += "/";
        servletContext.setAttribute(servletContext.getClass().getName() + "/cacheRoot", cacheRoot);
        W3FtpURLConnection.cacheRoot = cacheRoot + "ftp/";
        W3GopherURLConnection.cacheRoot = cacheRoot + "gopher/";
        W3HttpURLConnection.cacheRoot = cacheRoot + "http/";
        W3Imap4URLConnection.cacheRoot = cacheRoot + "imap4/";
        W3LdapURLConnection.cacheRoot = cacheRoot + "ldap/";
        W3MsgURLConnection.cacheRoot = cacheRoot + "msg/";
        W3NntpURLConnection.cacheRoot = cacheRoot + "nntp/";
        W3Pop3URLConnection.cacheRoot = cacheRoot + "pop3/";
        W3WaisURLConnection.cacheRoot = cacheRoot + "wais/";
        try {
            readProxyCacheParameters(proxyCaches, dataDirectory);
        } catch (ParseException ex) {
            throw new ServletException(Support.stackTrace(ex));
        }
    }

    public void service(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException {
        URL url = null;
        String method = null, requestURI = null, scheme = null;
        W3Request w3r = null;
        if (req.getPathInfo() != null) {
            url = new URL(req.getPathInfo().substring(1));
            requestURI = url.toString();
            scheme = url.getProtocol();
            w3r = (W3Request)req.getAttribute("");
            w3r.store.headerList.remove("Host");
        } else {
            scheme = req.getScheme();
            w3r = req instanceof W3Request ? (W3Request)req : null;
        }
        // checking if request can be cached
        Object obj = proxyCaches.get(scheme);
        if (obj == null) {
            res.sendError(HttpURLConnection.HTTP_BAD_REQUEST, "Bad scheme");
            return;
        }
        Object proxyCache;
        if (obj == DISABLED) {
            String virtualPath = (String)req.getAttribute(W3Request.class.getName() + "/virtualPath");
            if (virtualPath == null) {
                res.sendError(HttpServletResponse.SC_FORBIDDEN, "Forbidden");
                return;
            }
            proxyCache = null;
        } else {
            // caching is restricted to some sites, no caching or unrestricted caching
            proxyCache = obj instanceof Object[] ? ((Object[])obj)[0] : obj;
        }
        byte buffer[] = w3r != null ? w3r.buffer : null;
        if (buffer == null) buffer = new byte[Support.bufferLength];
        if (url == null) {
            url = w3r != null ? w3r.url : null;
            requestURI = req.getRequestURI();
        }
        method = req.getMethod();
        if (url == null) {
            StringBuffer sb = req.getRequestURL();
            if (req.getQueryString() != null) sb.append('?').append(req.getQueryString());
            url = new URL(sb.toString());
        }
        if (method.equals("OPTIONS")) {
            String maxForwards = req.getHeader("max-forwards");
            if (maxForwards != null && maxForwards.trim().equals("0")) {
                res.setContentLength(0);
                res.getOutputStream();
                return;
            }
        }
        if (method.equals("TRACE")) {
            String maxForwards = req.getHeader("max-forwards");
            if (maxForwards != null && maxForwards.trim().equals("0")) {
                Enumeration headers = w3r.getHeaders();
                while (headers.hasMoreElements()) {
                    Header header = (Header)headers.nextElement();
                    res.addHeader(header.getName(), header.getValue());
                }
                return;
            }
        }
        Map<String, W3URLConnection> connectionPool = (Map<String, W3URLConnection>)req.getAttribute(ProxyServlet.class.getName() + "/connectionPool");
        W3URLConnection uc = (W3URLConnection)req.getAttribute(W3Request.class.getName() + "/uc"), uc0 = null;
        String connectionName = url.getProtocol() + "://" + url.getHost() + (url.getPort() != -1 ? ":" + url.getPort() : "");
        boolean idempotent = false, mustClose = false, respond = false;
        try {
            if (uc == null) {
                idempotent = method.equals("GET") || method.equals("HEAD");
                respond = !idempotent || ProxyServlet.disableProxyPipelining;
                synchronized (connectionPool) {
                    uc0 = uc = connectionPool.get(connectionName);
                    if (uc != null && uc.getKeepAliveTimeoutMillis() != 0L
                        && System.currentTimeMillis() + 500L >= uc.getClientTime() + uc.getKeepAliveTimeoutMillis()) {
                        uc.disconnect();
                        uc0 = uc = null;
                    }
                    // making new connection
                    if (!ProxyServlet.disableProxyPipelining && uc != null
                        && uc.getProtocol().compareTo(W3URLConnection.protocol_1_1) >= 0
                        || uc == null) {
                        uc = W3URLConnection.openConnection(url);
                        if (connectTimeout > 0)
                            uc.setConnectTimeout(connectTimeout);
                        if (timeout > 0)
                            uc.setTimeout(timeout);
                        uc.setInstanceFollowRedirects(false);
                        uc.setServletContext(servletContext);
                        if (uc0 == null) respond = true;
                    } else uc.set(url);
                    uc.connectionPool = connectionPool;
                    uc.connectionName = connectionName;
                    if (uc0 != null) {
                        uc.uc = uc0;
                        uc.setRequester(uc0.getRequester());
                    } else {
                        uc.uc = uc;
                        connectionPool.put(uc.connectionName, uc);
                    }
                    uc.uc.numberInUse++;
                }
                int i;
                if ((i = requestURI.indexOf(';')) != -1 || (i = requestURI.indexOf('?')) != -1) requestURI = requestURI.substring(0, i).trim();
                uc.setRequestRoot(requestURI.endsWith("/") ? "" : requestURI.substring(requestURI.lastIndexOf('/') + 1) + "/");
                if (autoCaches) uc.setAutoCaches(true);
                if (autoResume) uc.setAutoResume(true);
                if (checkCaches) uc.setCheckCaches(true);
                if (forceCaches) uc.setForceCaches(true);
                uc.setForceCaches(idempotent && (proxyCache == Boolean.TRUE || proxyCache instanceof RegexpPool && ((RegexpPool)proxyCache).match(url.getHost() + Support.requestPath(requestURI)) != null));
                uc.setRequestMethod(method);
                if (ProxyServlet.disableProxyPersistentConnection) uc.setDisablePersistentConnection(true);
                Header viaHeaders[] = w3r.getHeaderList().getHeaders("via");
                if (serverID == null) {
                    serverID = req.getServerName();
                    if (req.getServerPort() != 80) serverID += ":" + req.getServerPort();
                }
                String viaValue = "1/1 " + serverID;
                if (viaHeaders != null) viaHeaders[viaHeaders.length - 1].setValue(viaHeaders[viaHeaders.length - 1].getValue() + ", " + viaValue);
                // setting request headers
                Enumeration headers = w3r.getHeaders();
                while (headers.hasMoreElements()) {
                    Header header = (Header)headers.nextElement();
                    uc.addRequestProperty(header.getName(), header.getValue());
                }
                if (method.equals("OPTIONS") || method.equals("TRACE")) {
                    String maxForwards = req.getHeader("max-forwards");
                    if (maxForwards != null) uc.setRequestProperty("Max-Forwards", new BigInteger(maxForwards.trim()).subtract(BigInteger.ONE).toString());
                }
                if (viaHeaders == null) uc.addRequestProperty("Via", viaValue);
                if (sendingMethods.contains(method) || req.getHeader("content-length") != null) {
                    uc.setDoOutput(true);
                    // sending data to remote server
                    InputStream in = req.getInputStream();
                    if (method.equals("POST")) {
                        int contentLength = req.getContentLength();
                        if (contentLength == -1) {
                            ByteArrayOutputStream bout = new ByteArrayOutputStream();
                            for (int n; (n = in.read(buffer)) > 0;)
                                bout.write(buffer, 0, n);
                            uc.addRequestProperty("Content-Length", String.valueOf(bout.size()));
                            in = new ByteArrayInputStream(bout.toByteArray());
                        }
                    }
                    String expect = req.getHeader("expect");
                    if ("100-continue".equals(expect)) {
                        uc.connect();
                    }
                    OutputStream out = uc.getOutputStream();
                    if (out != null) {
                        if (verbose)
                            for (int l = 0, n; (n = in.read(buffer)) > 0;) {
                                out.write(buffer, 0, n);
                                System.out.print("\r" + (l += n) + " written");
                            } else for (int n; (n = in.read(buffer)) > 0;)
                                       out.write(buffer, 0, n);
                        out.flush();
                        if (verbose) System.out.println();
                        uc.closeStreams();
                    }
                } else uc.connect();
            } else respond = true;
            String cache = "HIT by " + serverID,
                viaValue = req.getProtocol();
            if (viaValue.toUpperCase().startsWith("HTTP/")) viaValue = viaValue.substring(5);
            viaValue += " " + serverID;
            if (respond) {
                InputStream in = uc.getInputStream();
                int responseCode = uc.getResponseCode();
                // setting response headers
                res.setStatus(responseCode);
                Header viaHeaders[] = uc.getHeaderFieldList().getHeaders("via");
                if (viaHeaders != null) viaHeaders[viaHeaders.length - 1].setValue(viaHeaders[viaHeaders.length - 1].getValue() + ", " + viaValue);
                String key;
                for (int i = 1; (key = uc.getHeaderFieldKey(i)) != null; i++)
                    if (key.length() > 0) res.addHeader(key, uc.getHeaderField(i));
                if (viaHeaders == null) res.addHeader("Via", viaValue);
                if (uc.cached()) res.addHeader("X-Cache", cache);
                List<HeaderList> continueHeadersList = uc.getContinueHeaderFieldsList();
                if (continueHeadersList != null)
                    req.setAttribute(W3Request.class.getName() + "/continueHeadersList", continueHeadersList);
                OutputStream out = res.getOutputStream();
                if (in != null && !method.equals("HEAD") &&
                    responseCode != 100 &&
                    responseCode != 101 &&
                    responseCode != 199 &&
                    responseCode != HttpURLConnection.HTTP_NO_CONTENT &&
                    responseCode != HttpURLConnection.HTTP_NOT_MODIFIED &&
                    responseCode != HttpURLConnection.HTTP_PRECON_FAILED) {
                    // receiving data from remote server
                    for (int n; (n = in.read(buffer)) > 0;)
                        out.write(buffer, 0, n);
                }
            } else {
                Response response = new Response();
                response.store = w3r.store;
                response.uc = uc;
                response.res = res;
                response.method = method;
                response.viaValue = viaValue;
                response.cache = cache;
                req.setAttribute(ProxyServlet.class.getName() + "/response", response);
            }
        } catch (IOException ex) {
            mustClose = true;
            throw ex;
        } finally {
            if (uc == null) return;
            if (uc.cached()) req.setAttribute("FI.realitymodeler.server.W3Request/cached", Boolean.TRUE);
            if (respond) {
                uc.closeStreams();
                uc.uc.setClientTime(System.currentTimeMillis());
                if (uc.connectionPool != null)
                    synchronized (uc.connectionPool) {
                        uc.uc.numberInUse--;
                        uc.uc.requestNumber++;
                        if (keepAliveCount != 0 && uc.uc.requestNumber >= keepAliveCount
                            || uc.uc.getKeepAliveCount() != 0L && uc.uc.requestNumber >= uc.uc.getKeepAliveCount()
                            || !uc.mayKeepAlive() || mustClose || maxProxyConnections != -1 && connectionPool.size() >= maxProxyConnections) {
                            uc.connectionPool.remove(uc.connectionName);
                            uc.disconnect();
                        }
                    }
                else uc.uc.disconnect();
            }
        }
    }

}
