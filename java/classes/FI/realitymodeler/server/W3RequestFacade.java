
package FI.realitymodeler.server;

import java.io.*;
import java.security.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class W3RequestFacade implements HttpServletRequest {
    HttpServletRequest request;

    public W3RequestFacade(HttpServletRequest request) {
        this.request = request;
    }

    /** Returns the value of the named attribute as an Object, or null if no attribute
        of the given name exists.
        Attributes can be set two ways. The servlet container may set attributes to
        make available custom information about a request. For example, for requests
        made using HTTPS, the attribute
        javax.servlet.request.X509Certificate can be used to retrieve information
        on the certificate of the client. Attributes can also be set programatically
        using setAttribute(String, Object). This allows information to be
        embedded into a request before a RequestDispatcher call.
        Attribute names should follow the same conventions as package names. This
        specification reserves names matching java.*, javax.*, and sun.*.

        @param name a String specifying the name of the attribute
        @return an Object containing the value of the attribute, or null if the
        attribute does not exist

    */
    public Object getAttribute(String name) {
        return request.getAttribute(name);
    }

    /** Returns an Enumeration containing the names of the attributes available to
        this request. This method returns an empty Enumeration if the request has no
        attributes available to it.

        @return an Enumeration of strings containing the names of the request's
        attributes

    */
    public Enumeration getAttributeNames() {
        return request.getAttributeNames();
    }

    /** Returns the name of the character encoding used in the body of this request.
        This method returns <code>null</code> if the request does not specify a character encoding.

        @return a <code>String</code> containing the name of the chararacter encoding, or <code>null</code>
        if the request does not specify a character encoding.

    */
    public String getCharacterEncoding() {
        return request.getCharacterEncoding();
    }

    /** Returns the length, in bytes, of the request body and made available by the
        input stream, or -1 if the length is not known. For HTTP servlets, same as the
        value of the CGI variable CONTENT_LENGTH.

        @return an integer containing the length of the request body or -1 if the
        length is not known

    */
    public int getContentLength() {
        return request.getContentLength();
    }

    /**
     * Returns the length, in bytes, of the request body and made available by
     * the input stream, or -1 if the length is not known. For HTTP servlets,
     * same as the value of the CGI variable CONTENT_LENGTH.
     *
     * @return a long integer containing the length of the request body or -1 if
     *         the length is not known
     * @since Servlet 3.1
     */
    public long getContentLengthLong() {
        return request.getContentLengthLong();
    }

    /** Returns the MIME type of the body of the request, or null if the type is not
        known. For HTTP servlets, same as the value of the CGI variable
        CONTENT_TYPE.

        @return a String containing the name of the MIME type of the request, or
        null if the type is not known

    */
    public String getContentType() {
        return request.getContentType();
    }

    /** Retrieves the body of the request as binary data using a
        ServletInputStream. Either this method or getReader() may be called to
        read the body, not both.

        @return a ServletInputStream object containing the body of the request

        @throws IllegalStateException if the getReader() method has already been
        called for this request

        @throws IOException if an input or output exception occurred

    */
    public ServletInputStream getInputStream() throws IOException {
        return request.getInputStream();
    }

    /** Returns the host name of the Internet Protocol (IP) interface on
        which the request was received.

        @return a <code>String</code> containing the host name of the IP on
        which the request was received. 

    */
    public String getLocalName() {
        return request.getLocalName();
    }

    /** Returns the Internet Protocol (IP) address of the interface on which
        the request was received.

        @return a <code>String</code> containing the IP address on which the
        request was received.

    */       
    public String getLocalAddr() {
        return request.getLocalAddr();
    }

    /** Returns the Internet Protocol (IP) port number of the interface on
        which the request was received.

        @return an integer specifying the port number

    */
    public int getLocalPort() {
        return request.getLocalPort();
    }

    /**
     * @return TODO
     * @since Servlet 3.0 TODO SERVLET3 - Add comments
     */
    public ServletContext getServletContext() {
        return request.getServletContext();
    }

    /**
     * @return TODO
     * @throws java.lang.IllegalStateException
     *             If async is not supported for this request
     * @since Servlet 3.0 TODO SERVLET3 - Add comments
     */
    public AsyncContext startAsync() {
        return request.startAsync();
    }

    /**
     * @param servletRequest
     * @param servletResponse
     * @return TODO
     * @throws java.lang.IllegalStateException
     * @since Servlet 3.0 TODO SERVLET3 - Add comments
     */
    public AsyncContext startAsync(ServletRequest servletRequest,
                                   ServletResponse servletResponse) {
        return request.startAsync(servletRequest, servletResponse);
    }

    /**
     * @return TODO
     * @since Servlet 3.0 TODO SERVLET3 - Add comments
     */
    public boolean isAsyncStarted() {
        return request.isAsyncStarted();
    }

    /**
     * @return TODO
     * @since Servlet 3.0 TODO SERVLET3 - Add comments
     */
    public boolean isAsyncSupported() {
        return request.isAsyncSupported();
    }

    /**
     * @return TODO
     * @throws java.lang.IllegalStateException
     * @since Servlet 3.0 TODO SERVLET3 - Add comments
     */
    public AsyncContext getAsyncContext() {
        return request.getAsyncContext();
    }

    /**
     * @return TODO
     * @since Servlet 3.0 TODO SERVLET3 - Add comments
     */
    public DispatcherType getDispatcherType() {
        return request.getDispatcherType();
    }

    /** Returns the preferred Locale that the client will accept content in, based on
        the Accept-Language header. If the client request doesn't provide an Accept-Language
        header, this method returns the default locale for the server.

        @return the preferred Locale for the client

        @see javax.servlet.ServletResponse#setLocale(Locale)
    */
    public Locale getLocale() {
        return request.getLocale();
    }

    /** Returns an Enumeration of Locale objects indicating, in decreasing order
        starting with the preferred locale, the locales that are acceptable to the client
        based on the Accept-Language header. If the client request doesn't provide an
        Accept-Language header, this method returns an Enumeration containing
        one Locale, the default locale for the server.

        @return an Enumeration of preferred Locale objects for the client

    */
    public Enumeration getLocales() {
        return request.getLocales();
    }

    /** Returns the value of a request parameter as a String, or null if the parameter
        does not exist. Request parameters are extra information sent with the
        request. For HTTP servlets, parameters are contained in the query string or
        posted form data.
        You should only use this method when you are sure the parameter has only
        one value. If the parameter might have more than one value, use
        getParameterValues(String).
        If you use this method with a multivalued parameter, the value returned is
        equal to the first value in the array returned by getParameterValues.
        If the parameter data was sent in the request body, such as occurs with an
        HTTP POST request, then reading the body directly via getInputStream()
        or getReader() can interfere with the execution of this method.

        @param name a String specifying the name of the parameter
        @return a String representing the single value of the parameter
        @see getParameterValues(String)

    */
    public String getParameter(String name) {
        return request.getParameter(name);
    }

    /** Returns a java.util.Map of the parameters of this request. Request parameters
        are extra information sent with the request. For HTTP servlets, parameters
        are contained in the query string or posted form data.

        @return an immutable java.util.Map containing parameter names as keys
        and parameter values as map values. The keys in the parameter map are of
        type String. The values in the parameter map are of type String array.

    */
    public Map<String,String[]> getParameterMap() {
        return request.getParameterMap();
    }

    /** Returns an Enumeration of String objects containing the names of the
        parameters contained in this request. If the request has no parameters, the
        method returns an empty Enumeration.

        @return an Enumeration of String objects, each String containing the
        name of a request parameter; or an empty Enumeration if the request has no
        parameters

    */
    public Enumeration<String> getParameterNames() {
        return request.getParameterNames();
    }

    /** Returns an array of String objects containing all of the values the given
        request parameter has, or null if the parameter does not exist.
        If the parameter has a single value, the array has a length of 1.

        @param name a String containing the name of the parameter whose value is
        requested
        @return an array of String objects containing the parameter's values
        @see getParameter(String)

    */
    public String[] getParameterValues(String name) {
        return request.getParameterValues(name);
    }

    /** Returns the name and version of the protocol the request uses in the form
        protocol/majorVersion.minorVersion, for example, HTTP/1.1. For HTTP
        servlets, the value returned is the same as the value of the CGI variable
        SERVER_PROTOCOL.

        @return a String containing the protocol name and version number

    */
    public String getProtocol() {
        return request.getProtocol();
    }

    /** Retrieves the body of the request as character data using a <code>BufferedReader</code>.
        The reader translates the character data according to the character encoding
        used on the body. Either this method or {@link #getInputStream()} may be called to
        read the body, not both.

        @return a <code>BufferedReader</code> containing the body of the request

        @throws UnsupportedEncodingException if the character set encoding used is not
        supported and the text cannot be decoded

        @throws IllegalStateException if getInputStream() method has been called on
        this request

        @throws IOException if an input or output exception occurred

        @see getInputStream()

    */
    public BufferedReader getReader() throws IOException {
        return request.getReader();
    }

    /** @deprecated Deprecated as of Version 2.1 of the Java Servlet API, use
        ServletContext.getRealPath(String) instead.

    */
    @Deprecated
    public String getRealPath(String path) {
        return request.getRealPath(path);
    }

    /** Returns the Internet Protocol (IP) address of the client that sent the request.
        For HTTP servlets, same as the value of the CGI variable REMOTE_ADDR.

        @return a String containing the IP address of the client that sent the
        request

    */
    public String getRemoteAddr() {
        return request.getRemoteAddr();
    }

    /** Returns the fully qualified name of the client that sent the request. If the
        engine cannot or chooses not to resolve the hostname (to improve performance),
        this method returns the dotted-string form of the IP address. For
        HTTP servlets, same as the value of the CGI variable REMOTE_HOST.

        @return a String containing the fully qualified name of the client

    */
    public String getRemoteHost() {
        return request.getRemoteHost();
    }

    /** Returns the Internet Protocol (IP) source port of the client or last
        proxy that sent the request. 

        @return an integer specifying the port number

    */
    public int getRemotePort() {
        return request.getRemotePort();
    }

    /** Returns a RequestDispatcher object that acts as a wrapper for the resource
        located at the given path. A RequestDispatcher object can be used to forward
        a request to the resource or to include the resource in a response. The
        resource can be dynamic or static.
        The pathname specified may be relative, although it cannot extend outside the
        current servlet context. If the path begins with a "/" it is interpreted as relative
        to the current context root. This method returns null if the servlet container
        cannot return a RequestDispatcher. The difference between this method and
        ServletContext.getRequestDispatcher(String) is that this method can
        take a relative path.

        @param path a String specifying the pathname to the resource
        @return a RequestDispatcher object that acts as a wrapper for the
        resource at the specified path
        @see javax.servlet.RequestDispatcher
        @see javax.servlet.ServletContext#getRequestDispatcher(String)

    */
    public RequestDispatcher getRequestDispatcher(String path) {
        return request.getRequestDispatcher(path);
    }

    /** Returns the name of the scheme used to make this request, for example, http,
        https,or ftp. Different schemes have different rules for constructing URLs,
        as noted in RFC 1738.

        @return a String containing the name of the scheme used to make this
        request

    */
    public String getScheme() {
        return request.getScheme();
    }

    /** Returns the host name of the server that received the request. For HTTP servlets,
        same as the value of the CGI variable SERVER_NAME.

        @return a String containing the name of the server to which the request
        was sent

    */
    public String getServerName() {
        return request.getServerName();
    }

    /** Returns the port number on which this request was received. For HTTP serv-lets,
        same as the value of the CGI variable SERVER_PORT.

        @return an integer specifying the port number

    */
    public int getServerPort() {
        return request.getServerPort();
    }

    /** Returns a boolean indicating whether this request was made using a secure
        channel, such as HTTPS.

        @return a boolean indicating if the request was made using a secure
        channel

    */
    public boolean isSecure() {
        return request.isSecure();
    }

    /** Removes an attribute from this request. This method is not generally needed
        as attributes only persist as long as the request is being handled.
        Attribute names should follow the same conventions as package names.
        Names beginning with java.*, javax.*, and com.sun.*, are reserved for use
        by Sun Microsystems.

        @param name a String specifying the name of the attribute to remove

    */
    public void removeAttribute(String name) {
        request.removeAttribute(name);
    }

    /** Stores an attribute in this request. Attributes are reset between requests. This
        method is most often used in conjunction with RequestDispatcher.
        Attribute names should follow the same conventions as package names.
        Names beginning with java.*, javax.*, and com.sun.*, are reserved for use
        by Sun Microsystems.
        If the value passed in is null, the effect is the same as calling
        removeAttribute(String).

        @param name a String specifying the name of the attribute
        @param object the Object to be stored

    */
    public void setAttribute(String name, Object object) {
        request.setAttribute(name, object);
    }

    /** Overrides the name of the character encoding used in the body of this
        request. This method must be called prior to reading request parameters or
        reading input using getReader().

        @param characterEncoding a string containing the name of the chararacter encoding.

        @throws java.io.UnsupportedEncodingException - if this is not a valid
        encoding

    */
    public void setCharacterEncoding(String characterEncoding) throws UnsupportedEncodingException {
        request.setCharacterEncoding(characterEncoding);
    }

    /** Returns the name of the authentication scheme used to protect the servlet. All
        servlet containers support basic, form and client certificate authentication,
        and may additionally support digest authentication. If the servlet is not
        authenticated null is returned.
        Same as the value of the CGI variable AUTH_TYPE.

        @return one of the static members BASIC_AUTH, FORM_AUTH,
        CLIENT_CERT_AUTH, DIGEST_AUTH (suitable for == comparison)
        indicating the authentication scheme, or null if the request was not
        authenticated.

    */
    public String getAuthType() {
        return request.getAuthType();
    }

    /** Returns the portion of the request URI that indicates the context of the
        request. The context path always comes first in a request URI. The path starts
        with "/" character but does not end with a "/" character. For servlets in the
        default (root) context, this method returns "". The container does not decode
        this string.

        @return a String specifying the portion of the request URI that indicates
        the context of the request

    */
    public String getContextPath() {
        return request.getContextPath();
    }

    /** Returns an array containing all of the Cookie objects the client sent with this
        request. This method returns null if no cookies were sent.

        @return an array of all the Cookies included with this request, or null if
        the request has no cookies

    */
    public Cookie[] getCookies() {
        return request.getCookies();
    }

    /** Returns the value of the specified request header as a long value that represents
        a Date object. Use this method with headers that contain dates, such as
        If-Modified-Since.
        The date is returned as the number of milliseconds since January 1, 1970 GMT.
        The header name is case insensitive.
        If the request did not have a header of the specified name, this method returns
        -1. If the header can't be converted to a date, the method throws an IllegalArgumentException.

        @param name a String specifying the name of the header
        @return a long value representing the date specified in the header
        expressed as the number of milliseconds since January 1, 1970 GMT, or -1 if
        the named header was not included with the reqest
        @throws IllegalArgumentException If the header value can't be converted to a date

    */
    public long getDateHeader(String name) {
        return request.getDateHeader(name);
    }

    /** Returns the value of the specified request header as a String. If the request
        did not include a header of the specified name, this method returns null. The
        header name is case insensitive. You can use this method with any request
        header.

        @param name a String specifying the header name
        @return a String containing the value of the requested header, or null if
        the request does not have a header of that name

    */
    public String getHeader(String name) {
        return request.getHeader(name);
    }

    /** Returns an enumeration of all the header names this request contains. If the
        request has no headers, this method returns an empty enumeration.
        Some servlet containers do not allow do not allow servlets to access headers
        using this method, in which case this method returns null

        @return an enumeration of all the header names sent with this request; if
        the request has no headers, an empty enumeration; if the servlet container
        does not allow servlets to use this method, null

    */
    public Enumeration getHeaderNames() {
        return request.getHeaderNames();
    }

    /** Returns all the values of the specified request header as an Enumeration of
        String objects.
        Some headers, such as Accept-Language can be sent by clients as several
        headers each with a different value rather than sending the header as a
        comma separated list.
        If the request did not include any headers of the specified name, this method
        returns an empty Enumeration. The header name is case insensitive. You can
        use this method with any request header.

        @param name a String specifying the header name
        @return an Enumeration containing the values of the requested header. If
        the request does not have any headers of that name return an empty
        enumeration. If the container does not allow access to header information,
        return null

    */
    public Enumeration getHeaders(String name) {
        return request.getHeaders(name);
    }

    /** Returns the value of the specified request header as an int. If the request
        does not have a header of the specified name, this method returns -1. If the
        header cannot be converted to an integer, this method throws a Number-
        FormatException.
        The header name is case insensitive.

        @param name a String specifying the name of a request header
        @return an integer expressing the value of the request header or -1 if the
        request doesn't have a header of this name
        @throws NumberFormatException If the header value can't be converted to an int

    */
    public int getIntHeader(String name) {
        return request.getIntHeader(name);
    }

    /** Returns the name of the HTTP method with which this request was made, for
        example, GET, POST, or PUT. Same as the value of the CGI variable
        REQUEST_METHOD.

        @return a String specifying the name of the method with which this
        request was made

    */
    public String getMethod() {
        return request.getMethod();
    }

    /** Returns any extra path information associated with the URL the client sent
        when it made this request. The extra path information follows the servlet path
        but precedes the query string. This method returns null if there was no extra
        path information.
        Same as the value of the CGI variable PATH_INFO.

        @return a String, decoded by the web container, specifying extra path
        information that comes after the servlet path but before the query string in the
        request URL; or null if the URL does not have any extra path information

    */
    public String getPathInfo() {
        return request.getPathInfo();
    }

    /** Returns any extra path information after the servlet name but before the
        query string, and translates it to a real path. Same as the value of the CGI
        variable PATH_TRANSLATED.
        If the URL does not have any extra path information, this method returns
        null. The web container does not decode this string.

        @return a String specifying the real path, or null if the URL does not
        have any extra path information

    */
    public String getPathTranslated() {
        return request.getPathTranslated();
    }

    /** Returns the query string that is contained in the request URL after the path.
        This method returns null if the URL does not have a query string. Same as
        the value of the CGI variable QUERY_STRING.

        @return a String containing the query string or null if the URL contains
        no query string. The value is not decoded by the container.

    */
    public String getQueryString() {
        return request.getQueryString();
    }

    /** Returns the login of the user making this request, if the user has been authenticated,
        or null if the user has not been authenticated. Whether the user name
        is sent with each subsequent request depends on the browser and type of
        authentication. Same as the value of the CGI variable REMOTE_USER.

        @return a String specifying the login of the user making this request, or
        <code>null</code> if the user login is not known

    */
    public String getRemoteUser() {
        return request.getRemoteUser();
    }

    /** Returns the session ID specified by the client. This may not be the same as
        the ID of the actual session in use. For example, if the request specified an old
        (expired) session ID and the server has started a new session, this method
        gets a new session with a new ID. If the request did not specify a session ID,
        this method returns null.

        @return a String specifying the session ID, or null if the request did not
        specify a session ID

        @see isRequestedSessionIdValid()

    */
    public String getRequestedSessionId() {
        return request.getRequestedSessionId();
    }

    /** Returns the part of this request's URL from the protocol name up to the query
        string in the first line of the HTTP request. The web container does not
        decode this String. For example:

        <table>
        <caption>Request</caption>
        <tr><th>First line of HTTP request</th><th>Returned Value</th></tr>
        <tr><td>POST /some/path.html HTTP/1.1</td><td>/some/path.html</td></tr>
        <tr><td>GET http://foo.bar/a.html HTTP/1.0</td><td>/a.html</td></tr>
        <tr><td>HEAD /xyz?a=b HTTP/1.1</td><td>/xyz</td></tr>
        </table>

        To reconstruct an URL with a scheme and host, use
        HttpUtils.getRequestURL(HttpServletRequest).

        @return a String containing the part of the URL from the protocol
        name up to the query string

        @see javax.servlet.http.HttpUtils#getRequestURL(HttpServletRequest)

    */
    public String getRequestURI() {
        return request.getRequestURI();
    }

    /** Reconstructs the URL the client used to make the request. The returned URL
        contains a protocol, server name, port number, and server path, but it does not
        include query string parameters.
        Because this method returns a StringBuffer, not a string, you can modify the
        URL easily, for example, to append query parameters.
        This method is useful for creating redirect messages and for reporting errors.

        @return a StringBuffer object containing the reconstructed URL

    */
    public StringBuffer getRequestURL() {
        return request.getRequestURL();
    }

    /** Returns the part of this request's URL that calls the servlet. This includes either
        the servlet name or a path to the servlet, but does not include any extra path
        information or a query string. Same as the value of the CGI variable
        SCRIPT_NAME.

        @return a String containing the name or path of the servlet being
        called, as specified in the request URL, decoded.

    */
    public String getServletPath() {
        return request.getServletPath();
    }

    /** Returns the current session associated with this request, or if the request does
        not have a session, creates one.

        @return the HttpSession associated with this request

        @see getSession(boolean)

    */
    public HttpSession getSession() {
        return request.getSession();
    }

    /** Returns the current HttpSession associated with this request or, if if there is no
        current session and create is true, returns a new session.
        If create is false and the request has no valid HttpSession, this method
        returns null.
        To make sure the session is properly maintained, you must call this method
        before the response is committed. If the container is using cookies to maintain
        session integrity and is asked to create a new session when the response is
        committed, an IllegalStateException is thrown.
        Parameters:
        <code>true</code> - to create a new session for this request if necessary; false
        to return null if there is no current session

        @return the HttpSession associated with this request or null if create
        is false and the request has no valid session

        @see getSession()

    */
    public HttpSession getSession(boolean create) {
        return request.getSession(create);
    }

    /**
     * Changes the session ID of the session associated with this request. This
     * method does not create a new session object it only changes the ID of the
     * current session.
     *
     * @return the new session ID allocated to the session
     * @see HttpSessionIdListener
     * @since Servlet 3.1
     */
    public String changeSessionId() {
	return null;
    }

    /** Returns a java.security.Principal object containing the name of the current
        authenticated user. If the user has not been authenticated, the method returns
        null.

        @return a java.security.Principal containing the name of the user
        making this request; null if the user has not been authenticated

    */
    public Principal getUserPrincipal() {
        return request.getUserPrincipal();
    }

    /** Checks whether the requested session ID came in as a cookie.

        @return true if the session ID came in as a cookie; otherwise, false

        @see getSession(boolean)

    */
    public boolean isRequestedSessionIdFromCookie() {
        return request.isRequestedSessionIdFromCookie();
    }

    /** @deprecated Deprecated as of Version 2.1 of the Java Servlet API, use
        isRequestedSessionIdFromURL() instead.

    */
    @Deprecated
    public boolean isRequestedSessionIdFromUrl() {
        return request.isRequestedSessionIdFromUrl();
    }

    /** Checks whether the requested session ID came in as part of the request URL.

        @return true if the session ID came in as part of a URL; otherwise,
        false

        @see getSession(boolean)

    */
    public boolean isRequestedSessionIdFromURL() {
        return request.isRequestedSessionIdFromURL();
    }

    /** Checks whether the requested session ID is still valid.

        @return true if this request has an id for a valid session in the current
        session context; false otherwise

        @see javax.servlet.http.HttpServletRequest#getRequestedSessionId()
        @see javax.servlet.http.HttpServletRequest#getSession(boolean)
        @see javax.servlet.http.HttpSessionContext

    */
    public boolean isRequestedSessionIdValid() {
        return request.isRequestedSessionIdValid();
    }

    /** Returns a boolean indicating whether the authenticated user is included in the
        specified logical "role". Roles and role membership can be defined using
        deployment descriptors. If the user has not been authenticated, the method
        returns false.

        @param role a String specifying the name of the role
        @return a boolean indicating whether the user making this request
        belongs to a given role; false if the user has not been authenticated

    */
    public boolean isUserInRole(String role) {
        return request.isUserInRole(role);
    }

    /**
     * @param response
     * @return TODO
     * @throws IOException
     * @throws ServletException
     * @since Servlet 3.0 TODO SERVLET3 - Add comments
     */
    public boolean authenticate(HttpServletResponse response)
        throws IOException, ServletException {
        return request.authenticate(response);
    }

    /**
     * @param username
     * @param password
     * @throws ServletException
     *             If any of {@link #getRemoteUser()},
     *             {@link #getUserPrincipal()} or {@link #getAuthType()} are
     *             non-null, if the configured authenticator does not support
     *             user name and password authentication or if the
     *             authentication fails
     * @since Servlet 3.0 TODO SERVLET3 - Add comments
     */
    public void login(String username, String password)
        throws ServletException {
        request.login(username, password);
    }

    /**
     * @throws ServletException
     *             If the logout fails
     * @since Servlet 3.0 TODO SERVLET3 - Add comments
     */
    public void logout()
        throws ServletException {
        request.logout();
    }

    /**
     * Return a collection of all uploaded Parts.
     * 
     * @return A collection of all uploaded Parts.
     * @throws IOException
     *             if an I/O error occurs
     * @throws IllegalStateException
     *             if size limits are exceeded
     * @throws ServletException
     *             if the request is not multipart/form-data
     * @since Servlet 3.0
     */
    public Collection<Part> getParts()
        throws IOException, IllegalStateException, ServletException {
        return request.getParts();
    }

    /**
     * Gets the named Part or null if the Part does not exist. Triggers upload
     * of all Parts.
     * 
     * @param name
     * @return The named Part or null if the Part does not exist
     * @throws IOException
     *             if an I/O error occurs
     * @throws IllegalStateException
     *             if size limits are exceeded
     * @throws ServletException
     *             if the request is not multipart/form-data
     * @since Servlet 3.0
     */
    public Part getPart(String name)
        throws IOException, IllegalStateException,
               ServletException {
        return request.getPart(name);
    }

    /**
     * Start the HTTP upgrade process and pass the connection to the provided
     * protocol handler once the current request/response pair has completed
     * processing. Calling this method sets the response status to {@link
     * HttpServletResponse#SC_SWITCHING_PROTOCOLS} and flushes the response.
     * Protocol specific headers must have already been set before this method
     * is called.
     *
     * @param <T>                     The type of the upgrade handler
     * @param httpUpgradeHandlerClass The class that implements the upgrade
     *                                handler
     *
     * @return A newly created instance of the specified upgrade handler type
     *
     * @throws IOException
     *             if an I/O error occurred during the upgrade
     * @throws ServletException
     *             if the given httpUpgradeHandlerClass fails to be instantiated
     * @since Servlet 3.1
     */
    public <T extends HttpUpgradeHandler> T upgrade(
            Class<T> httpUpgradeHandlerClass)
	throws IOException, ServletException {
	return (T)null;
    }

}
