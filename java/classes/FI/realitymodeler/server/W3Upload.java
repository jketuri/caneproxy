
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** File uploading servlet. Initialization parameter 'directory' may give the directory where
    files are uploaded (defaults to 'incoming').
*/
public class W3Upload extends HttpServlet {
    static final long serialVersionUID = 0L;

    private File dir;
    private ResourceBundle messages;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        messages = W3URLConnection.getMessages("FI.realitymodeler.server.resources.Messages");
        String s = getInitParameter("directory");
        if (s == null) {
            ServletContext context = getServletContext();
            if (context instanceof W3Context) s = ((W3Context)context).w3server.dataDirectory + "incoming";
        }
        dir = new File(s != null ? s : "incoming");
    }

    public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
        String charset = res.getCharacterEncoding(),
            charsetName = charset != null ? Support.getCharacterEncoding(charset) : null;
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        Support.writeBytes(bout, "<html><head><title>" + messages.getString("uploadFile") + "</title></head><body>\n", charsetName);
        Support.writeBytes(bout, "<h1>" + messages.getString("uploadFile") + "</h1>\n", charsetName);
        Support.writeBytes(bout, messages.getString("chooseAFileFromTheLocalFileSystemAndPressTheUploadFileButton") + "<br>\n", charsetName);
        Support.writeBytes(bout, "<form action=\"" + req.getServletPath() + "\" enctype=\"multipart/form-data\" method=post>\n", charsetName);
        Support.writeBytes(bout, "<table>\n", charsetName);
        Support.writeBytes(bout, "<tr><th>" + messages.getString("file") + "</th><td><input name=file type=file accept=\"*/*\" size=55 maxlength=275></td></tr>\n", charsetName);
        Support.writeBytes(bout, "</table>\n", charsetName);
        Support.writeBytes(bout, "<input type=submit value=\"" + messages.getString("uploadFile") + "\">\n", charsetName);
        Support.writeBytes(bout, "</form></body></html>", charsetName);
        res.setContentLength(bout.size());
        res.setContentType(Support.htmlType);
        bout.writeTo(res.getOutputStream());
    }

    public void doHead(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        doGet(req, res);
    }

    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String charset = res.getCharacterEncoding(),
            charsetName = charset != null ? Support.getCharacterEncoding(charset) : null;
        InputStream in = req.getInputStream();
        Map<String,String> params = new HashMap<String,String>();
        String ct = Support.getParameters(req.getContentType(), params).toLowerCase();
        if (!ct.equals("multipart/form-data")) throw new ServletException("Wrong content type");
        String boundary = params.get("boundary");
        if (boundary == null) throw new ServletException("Missing boundary");
        boundary = "--" + boundary;
        InputStream in1 = new ConvertInputStream(in, boundary, "\r", true, true);
        while (in1.read() != -1);
        boundary = "\r\n" + boundary;
        int c;
        if ((c = in.read()) == '-' && (c = in.read()) == '-') throw new ServletException("Missing content");
        while (c != '\n' && (c = in.read()) != -1);
        in1 = new ConvertInputStream(in, boundary, false, true);
        HeaderList headerList;
        try {
            headerList = new HeaderList(in1);
        } catch (ParseException ex) {
            throw new ServletException(ex.toString());
        }
        String cd = headerList.getHeaderValue("content-disposition");
        if (cd == null) throw new ServletException("Missing content-disposition");
        Support.getParameters(cd, params = new HashMap<String,String>(), true);
        String filename = params.get("filename");
        if (filename == null ||
            (filename = filename.substring(Math.max(filename.lastIndexOf('/'), filename.lastIndexOf('\\')) + 1).trim()).equals(""))
            throw new ServletException("Missing or invalid filename");
        byte b[];
        if (req instanceof W3Request) b = ((W3Request)req).buffer;
        else b = new byte[Support.bufferLength];
        File file = new File(dir, filename);
        FileOutputStream fout = null;
        try {
            fout = new FileOutputStream(file);
            for (int n; (n = in1.read(b)) > 0;) fout.write(b, 0, n);
        } catch (IOException ex) {
            try {
                if (fout != null) fout.close();
            } catch (IOException ex1) {}
            file.delete();
            log(ex.toString(), ex);
            throw ex;
        }
        fout.close();
        while (in.read(b) > 0);
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        Support.writeBytes(bout, "<html><head><title>" + messages.getString("fileUploaded") + "</title></head><body>\n", charsetName);
        Support.writeBytes(bout, "<h3>" + MessageFormat.format(messages.getString("fileFilenameUploadedSuccessfully"), new Object[] {filename}) + "</h3>\n", charsetName);
        Support.writeBytes(bout, "</body></html>", charsetName);
        res.setContentLength(bout.size());
        res.setContentType(Support.htmlType);
        bout.writeTo(res.getOutputStream());
    }

}
