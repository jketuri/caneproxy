
package FI.realitymodeler.server;

import java.net.*;

public class Link {
    String url;
    long number;
    long time;

    public Link() {
    }

    public Link(String url) {
        this.url = url;
    }

    public Link(URL url) {
        this(url.toString());
    }

    public String toString() {
        return url;
    }

    public String getUrl() {
        return url;
    }

    public long getNumber() {
        return number;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

}
