
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class Enriched2Html extends HttpServlet {
    static final long serialVersionUID = 0L;

    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String charset = res.getCharacterEncoding(),
            charsetName = charset != null ? Support.getCharacterEncoding(charset) : null;
        res.setContentType(Support.htmlType);
        Enriched2HtmlInputStream.convert(req.getInputStream(), res.getOutputStream(), charsetName);
    }

}
