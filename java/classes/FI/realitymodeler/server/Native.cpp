
/* Native servlet interface */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef _WIN32
#include <windows.h>
#endif
#include "FI_realitymodeler_server_Native.h"
#include "Native.h"

static jclass runtimeException, stringArrayID;
static jmethodID getInitParameterID, getInitParameterNamesID,
    getAttributeID, getAttributeNamesID, getCharacterEncodingID,
    getContentLengthID, getContentTypeID, getInputStreamID,
    getParameterID, getParameterNamesID, getParameterValuesID,
    getProtocolID, getSchemeID, getServerNameID, getServerPortID,
    getRemoteAddrID, getRemoteHostID, getReaderID, setAttributeID,
    removeAttributeID, getLocaleID, getLocalesID, isSecureID,
    getAuthTypeID, getCookiesID, getDateHeaderID,
    getHeaderID, getHeadersID, getHeaderNamesID, getIntHeaderID, getMethodID,
    getPathInfoID, getPathTranslatedID, getContextPathID, getQueryStringID,
    getRemoteUserID, isUserInRoleID, getUserPrincipalID, getRequestedSessionIdID,
    getRequestURIID, getServletPathID, getSessionID,
    getOutputStreamID, getWriterID, setContentLengthID, setContentTypeID,
    setBufferSizeID, getBufferSizeID, flushBufferID, isCommittedID, resetID, setLocaleID,
    addCookieID, containsHeaderID, encodeURLID, encodeRedirectURLID,
    sendErrorID, sendRedirectID, setDateHeaderID, addDateHeaderID,
    setHeaderID, addHeaderID, setIntHeaderID, addIntHeaderID, setStatusID,
    getRealPathID, logID, readID, writeID, hasMoreElementsID, nextElementID;
static jfieldID bufferID;

#ifdef _WIN32
extern "C" void throwNew(JNIEnv *env, jclass throwable, DWORD n);
#endif

#ifdef _WIN32
static int __cdecl compare(const void *elem1, const void *elem2)
#else
    static int compare(const void *elem1, const void *elem2)
#endif
{
    return strcmp((char *)((ENTRY *)elem1)->key, (char *)((ENTRY *)elem2)->key);
}

static jclass getClass(JNIEnv *env, char *name)
{
    jclass clazz;
    if ((clazz = env->FindClass(name))) clazz = (jclass)env->NewGlobalRef(clazz);
    return clazz;
}

static void release(JNIEnv *env, ENTRY *entries, jint number)
{
    for (int i = 0; i < number; i++) {
        if (entries[i].jkey && entries[i].key) env->ReleaseStringUTFChars(entries[i].jkey, entries[i].key);
        if (entries[i].n_values) {
            for (int j = 0; j < entries[i].n_values; j++)
                if (entries[i].jvalue.jvalues[j] && entries[i].value.values[j])
                    env->ReleaseStringUTFChars(entries[i].jvalue.jvalues[j], entries[i].value.values[j]);
            delete entries[i].jvalue.jvalues;
            delete entries[i].value.values;
        }
        else env->ReleaseStringUTFChars(entries[i].jvalue.jvalue, entries[i].value.value);
    }
    delete entries;
}

static ENTRY *getEnumeration(JNIEnv *env, jobject obj, jmethodID getKeysID, jmethodID getElementID, jint *number)
{
    ENTRY *entries = NULL;
    for (;;) {
        jobject keys = env->CallObjectMethod(obj, getKeysID);
        if (env->ExceptionOccurred()) break;
        for (;;) {
            if (env->CallBooleanMethod(keys, hasMoreElementsID)) (*number)++;
            if (env->ExceptionOccurred()) break;
        }
        if (!(entries = new ENTRY[*number])) break;
        keys = env->CallObjectMethod(obj, getKeysID);
        if (env->ExceptionOccurred()) break;
        for (int i = 0; i < *number; i++) {
            entries[i].jkey = (jstring)env->CallObjectMethod(keys, nextElementID);
            if (env->ExceptionOccurred() ||
                !(entries[i].key = env->GetStringUTFChars(entries[i].jkey, NULL))) break;
            jobject value = env->CallObjectMethod(obj, getElementID, entries[i].jkey);
            if (env->ExceptionOccurred()) break;
            if (env->IsInstanceOf(value, stringArrayID)) {
                entries[i].n_values = env->GetArrayLength((jobjectArray)value);
                entries[i].jvalue.jvalues = new jstring[entries[i].n_values];
                entries[i].value.values = new STRING[entries[i].n_values];
                for (int j = 0; j < entries[i].n_values; j++) {
                    entries[i].jvalue.jvalues[j] = (jstring)env->GetObjectArrayElement((jobjectArray)value, j);
                    if (env->ExceptionOccurred() ||
                        !(entries[i].value.values[j] = env->GetStringUTFChars(entries[i].jvalue.jvalues[j], NULL))) break;
                }
            }
            else {
                entries[i].n_values = 0;
                entries[i].jvalue.jvalue = (jstring)value;
                if (!(entries[i].value.value = env->GetStringUTFChars(entries[i].jvalue.jvalue, NULL))) break;
            }
            if (i) entries[i - 1].next_entry = entries + i;
        }
        qsort(entries, *number, sizeof(ENTRY), compare);
        return entries;
    }
    if (entries) release(env, entries, *number);
    *number = 0;
    return NULL;
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_server_Native_initialize(JNIEnv *env, jclass clazz)
{
    if (!(getInitParameterID = env->GetMethodID(clazz, "getInitParameter", "(Ljava/lang/String;)Ljava/lang/String;")) ||
        !(getInitParameterNamesID = env->GetMethodID(clazz, "getInitParameterNames", "()Ljava/util/Hashtable;")) ||
        !(clazz = getClass(env, (char *)"FI/realitymodeler/server/W3Request")) ||
        !(getAttributeID = env->GetMethodID(clazz, "getAttribute", "(Ljava/lang/String;)Ljava/lang/String;")) ||
        !(getAttributeNamesID = env->GetMethodID(clazz, "getAttributeNames", "()Ljava/util/Enumeration;")) ||
        !(getCharacterEncodingID = env->GetMethodID(clazz, "getCharacterEncoding", "()Ljava/lang/String;")) ||
        !(getContentLengthID = env->GetMethodID(clazz, "getContentLength", "()I")) ||
        !(getContentTypeID = env->GetMethodID(clazz, "getContentType", "()Ljava/lang/String;")) ||
        !(getInputStreamID = env->GetMethodID(clazz, "getInputStream", "()Ljava/servlet/ServletInputStream")) ||
        !(getParameterID = env->GetMethodID(clazz, "getParameter", "(Ljava/lang/String;)Ljava/lang/String;")) ||
        !(getParameterNamesID = env->GetMethodID(clazz, "getParameterNames", "()Ljava/util/Enumeration;")) ||
        !(getParameterValuesID = env->GetMethodID(clazz, "getParameterValues", "(Ljava/lang/String;)Ljava/lang/String;")) ||
        !(getProtocolID = env->GetMethodID(clazz, "getProtocol", "()Ljava/lang/String;")) ||
        !(getSchemeID = env->GetMethodID(clazz, "getScheme", "()Ljava/lang/String;")) ||
        !(getServerNameID = env->GetMethodID(clazz, "getServerName", "()Ljava/lang/String;")) ||
        !(getServerPortID = env->GetMethodID(clazz, "getServerPort", "()I")) ||
        !(getReaderID = env->GetMethodID(clazz, "getReader", "()Ljava/io/BufferedReader;")) ||
        !(getRemoteAddrID = env->GetMethodID(clazz, "getRemoteAddr", "()Ljava/lang/String;")) ||
        !(getRemoteHostID = env->GetMethodID(clazz, "getRemoteHost", "()Ljava/lang/String;")) ||
        !(setAttributeID = env->GetMethodID(clazz, "setAttribute", "(Ljava/lang/String;)Ljava/lang/Object;")) ||
        !(removeAttributeID = env->GetMethodID(clazz, "removeAttribute", "(Ljava/lang/String;)")) ||
        !(getLocaleID = env->GetMethodID(clazz, "getLocale", "()Ljava/util/Locale;")) ||
        !(getLocalesID = env->GetMethodID(clazz, "getLocales", "()Ljava/util/Enumeration;")) ||
        !(getAuthTypeID = env->GetMethodID(clazz, "getAuthType", "()Ljava/lang/String;")) ||
        !(getCookiesID = env->GetMethodID(clazz, "getCookies", "()[Ljavax/servlet/Cookie;")) ||
        !(getDateHeaderID = env->GetMethodID(clazz, "getDateHeader", "(Ljava/lang/String;)J")) ||
        !(getHeaderID = env->GetMethodID(clazz, "getHeader", "(Ljava/lang/String;)Ljava/lang/String;")) ||
        !(getHeadersID = env->GetMethodID(clazz, "getHeaders", "(Ljava/lang/String;)Ljava/util/Enumeration;")) ||
        !(getHeaderNamesID = env->GetMethodID(clazz, "getHeaderNames", "()Ljava/util/Enumeration;")) ||
        !(getIntHeaderID = env->GetMethodID(clazz, "getIntHeader", "(Ljava/lang/String;)I")) ||
        !(getMethodID = env->GetMethodID(clazz, "getMethod", "()Ljava/lang/String;")) ||
        !(getPathInfoID = env->GetMethodID(clazz, "getPathInfo", "()Ljava/lang/String;")) ||
        !(getPathTranslatedID = env->GetMethodID(clazz, "getPathTranslated", "()Ljava/lang/String;")) ||
        !(getContextPathID = env->GetMethodID(clazz, "getContextPathID", "()Ljava/lang/String;")) ||
        !(getQueryStringID = env->GetMethodID(clazz, "getQueryString", "()Ljava/lang/String;")) ||
        !(getRemoteUserID = env->GetMethodID(clazz, "getRemoteUserID", "()Ljava/lang/String;")) ||
        !(isUserInRoleID = env->GetMethodID(clazz, "isUserInRoleID", "(Ljava/lang/String;)Z")) ||
        !(getUserPrincipalID = env->GetMethodID(clazz, "getUserPrincipalID", "()Ljava/security/Principal;")) ||
        !(getRequestedSessionIdID = env->GetMethodID(clazz, "getRequestedSessionId", "()Ljava/lang/String;")) ||
        !(getRequestURIID = env->GetMethodID(clazz, "getRequestURI", "()Ljava/lang/String;")) ||
        !(getServletPathID = env->GetMethodID(clazz, "getServletPath", "()Ljava/lang/String;")) ||
        !(getSessionID = env->GetMethodID(clazz, "getSessionID", "()Ljavax/servlet/http/Session;")) ||
        !(getOutputStreamID = env->GetMethodID(clazz, "getOutputStream", "()Ljava/servlet/ServletOutputStream;")) ||
        !(getWriterID = env->GetMethodID(clazz, "getWriterID", "()Ljava/io/PrintWriter;")) ||
        !(setContentLengthID = env->GetMethodID(clazz, "setContentLength", "(I)B")) ||
        !(setContentTypeID = env->GetMethodID(clazz, "setContentType", "(Ljava/lang/String;)V")) ||
        !(setBufferSizeID = env->GetMethodID(clazz, "setBufferSize", "(I)V")) ||
        !(getBufferSizeID = env->GetMethodID(clazz, "getBufferSize", "()I")) ||
        !(flushBufferID = env->GetMethodID(clazz, "flushBuffer", "()V")) ||
        !(isCommittedID = env->GetMethodID(clazz, "isCommitted", "()Z")) ||
        !(resetID = env->GetMethodID(clazz, "reset", "()")) ||
        !(setLocaleID = env->GetMethodID(clazz, "setLocale", "(Ljava/util/Locale;)V")) ||
        !(addCookieID = env->GetMethodID(clazz, "addCookie", "(ILjavax/servlet/http/Cookie;)V")) ||
        !(containsHeaderID = env->GetMethodID(clazz, "containsHeader", "(Ljava/lang/String;)Z")) ||
        !(encodeURLID = env->GetMethodID(clazz, "encodeURL", "(Ljava/lang/String;)Ljava/lang/String;")) ||
        !(encodeRedirectURLID = env->GetMethodID(clazz, "encodeRedirectURL", "(Ljava/lang/String;)Ljava/lang/String;")) ||
        !(sendErrorID = env->GetMethodID(clazz, "sendError", "(ILjava/lang/String;)V")) ||
        !(sendRedirectID = env->GetMethodID(clazz, "sendRedirect", "(Ljava/lang/String;)V")) ||
        !(setDateHeaderID = env->GetMethodID(clazz, "setDateHeader", "(Ljava/lang/String;J)V")) ||
        !(addDateHeaderID = env->GetMethodID(clazz, "addDateHeader", "(Ljava/lang/String;J)V")) ||
        !(setHeaderID = env->GetMethodID(clazz, "setHeader", "(Ljava/lang/String;Ljava/lang/String;)V")) ||
        !(addHeaderID = env->GetMethodID(clazz, "addHeader", "(Ljava/lang/String;Ljava/lang/String;)V")) ||
        !(setIntHeaderID = env->GetMethodID(clazz, "setIntHeader", "(Ljava/lang/String;I)V")) ||
        !(addIntHeaderID = env->GetMethodID(clazz, "addIntHeader", "(Ljava/lang/String;I)V")) ||
        !(setStatusID = env->GetMethodID(clazz, "setStatus", "(ILjava/lang/String;)")) ||
        !(getRealPathID = env->GetMethodID(clazz, "getRealPath", "()Ljava/lang/String;")) ||
        !(logID = env->GetMethodID(clazz, "log", "(Ljava/lang/String;)")) ||
        !(bufferID = env->GetFieldID(clazz, "buffer", "[B")) ||
        !(clazz = getClass(env, (char *)"java/io/InputStream")) ||
        !(readID = env->GetMethodID(clazz, "read", "([BII)I")) ||
        !(clazz = getClass(env, (char *)"java/io/OutputStream")) ||
        !(writeID = env->GetMethodID(clazz, "write", "([BII)V")) ||
        !(clazz = getClass(env, (char *)"java/util/Enumeration")) ||
        !(hasMoreElementsID = env->GetMethodID(clazz, "hasMoreElements", "()Z")) ||
        !(nextElementID = env->GetMethodID(clazz, "nextElement", "()Ljava/lang/Object;")) ||
        !(runtimeException = getClass(env, (char *)"java/lang/RuntimeException")) ||
        !(stringArrayID = getClass(env, (char *)"[Ljava/lang/String;"))) return;
}

extern "C" JNIEXPORT jlong JNICALL Java_FI_realitymodeler_server_Native_construct(JNIEnv *env, jobject obj)
{
    jlong instance = (jlong)new Native;
    if (!instance) env->ThrowNew(runtimeException, "Out of memory");
    return instance;
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_server_Native_init0(JNIEnv *env, jobject obj, jlong instance, jstring name)
{
#ifdef _WIN32
    Native *native = (Native *)instance;
    native->env = env;
    native->obj = obj;
    const char *s = env->GetStringUTFChars(name, NULL);
    if (!s) return;
    size_t l = strlen(s);
    char *ss = new char[l + 8];
    if (ss) strcpy(ss, s);
    env->ReleaseStringUTFChars(name, s);
    if (!ss) {
        env->ThrowNew(runtimeException, "Out of memory");
        return;
    }
    if (!(native->hlib = LoadLibrary(ss)) ||
        !(native->init = (INIT)GetProcAddress((HINSTANCE)native->hlib, strcpy(ss + l, "Init"))) ||
        !(native->service = (SERVICE)GetProcAddress((HINSTANCE)native->hlib, strcpy(ss + l, "Service"))) ||
        !(native->destroy = (DESTROY)GetProcAddress((HINSTANCE)native->hlib, strcpy(ss + l, "Destroy")))) {
        throwNew(env, runtimeException, GetLastError());
        delete ss;
        return;
    }
    delete ss;
    if (!(native->parameters = getEnumeration(env, obj, getInitParameterNamesID, getInitParameterID, &native->num))) return;
    if (native->init) native->init(native);
#endif
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_server_Native_service0(JNIEnv *env, jobject obj, jlong instance, jobject w3r)
{
#ifdef _WIN32
    Native *native = (Native *)instance;
    Context *context = new Context;
    context->env = env;
    context->obj = w3r;
    context->nativeObj = obj;
    for (;;) {
        context->in = env->CallObjectMethod(w3r, getInputStreamID);
        if (env->ExceptionOccurred()) break;
        context->out = env->CallObjectMethod(w3r, getOutputStreamID);
        if (env->ExceptionOccurred()) break;
        context->buffer = (jbyteArray)env->GetObjectField(w3r, bufferID);
        if (env->ExceptionOccurred()) break;
        context->buf = env->GetByteArrayElements(context->buffer, NULL);
        if (env->ExceptionOccurred()) break;
        context->len = env->GetArrayLength(context->buffer);
        if (!(context->parameters = getEnumeration(env, w3r, getParameterNamesID, getParameterID, &context->num)) ||
            !(context->header = getEnumeration(env, w3r, getHeaderNamesID, getHeaderID, &context->size))) break;
        context->jcontentType = (jstring)env->CallObjectMethod(w3r, getContentTypeID);
        if (env->ExceptionOccurred()) break;
        context->jprotocol = (jstring)env->CallObjectMethod(w3r, getProtocolID);
        if (env->ExceptionOccurred()) break;
        context->jscheme = (jstring)env->CallObjectMethod(w3r, getSchemeID);
        if (env->ExceptionOccurred()) break;
        context->jserverName = (jstring)env->CallObjectMethod(w3r, getServerNameID);
        if (env->ExceptionOccurred()) break;
        context->jremoteAddr = (jstring)env->CallObjectMethod(w3r, getRemoteAddrID);
        if (env->ExceptionOccurred()) break;
        context->jremoteHost = (jstring)env->CallObjectMethod(w3r, getRemoteHostID);
        if (env->ExceptionOccurred()) break;
        context->jmethod = (jstring)env->CallObjectMethod(w3r, getMethodID);
        if (env->ExceptionOccurred()) break;
        context->jrequestURI = (jstring)env->CallObjectMethod(w3r, getRequestURIID);
        if (env->ExceptionOccurred()) break;
        context->jservletPath = (jstring)env->CallObjectMethod(w3r, getServletPathID);
        if (env->ExceptionOccurred()) break;
        context->jpathInfo = (jstring)env->CallObjectMethod(w3r, getPathInfoID);
        if (env->ExceptionOccurred()) break;
        context->jpathTranslated = (jstring)env->CallObjectMethod(w3r, getPathTranslatedID);
        if (env->ExceptionOccurred()) break;
        context->jqueryString = (jstring)env->CallObjectMethod(w3r, getQueryStringID);
        if (env->ExceptionOccurred()) break;
        context->jauthType = (jstring)env->CallObjectMethod(w3r, getAuthTypeID);
        if (!(context->contentType = env->GetStringUTFChars(context->jcontentType, NULL)) ||
            !(context->protocol = env->GetStringUTFChars(context->jprotocol, NULL)) ||
            !(context->scheme = env->GetStringUTFChars(context->jscheme, NULL)) ||
            !(context->serverName = env->GetStringUTFChars(context->jserverName, NULL)) ||
            !(context->remoteAddr = env->GetStringUTFChars(context->jremoteAddr, NULL)) ||
            !(context->remoteHost = env->GetStringUTFChars(context->jremoteHost, NULL)) ||
            !(context->method = env->GetStringUTFChars(context->jmethod, NULL)) ||
            !(context->requestURI = env->GetStringUTFChars(context->jrequestURI, NULL)) ||
            !(context->servletPath = env->GetStringUTFChars(context->jservletPath, NULL)) ||
            !(context->pathInfo = env->GetStringUTFChars(context->jpathInfo, NULL)) ||
            !(context->pathTranslated = env->GetStringUTFChars(context->jpathTranslated, NULL)) ||
            !(context->queryString = env->GetStringUTFChars(context->jqueryString, NULL)) ||
            !(context->authType = env->GetStringUTFChars(context->jauthType, NULL))) break;
        if (native->service) native->service(context);
        break;
    }
    if (context && context->header) release(env, context->header, context->size);
    if (context && context->parameters) release(env, context->parameters, context->num);
    if (context->buffer && context->buf) env->ReleaseByteArrayElements(context->buffer, context->buf, JNI_ABORT);
    if (context->jcontentType && context->contentType) env->ReleaseStringUTFChars(context->jcontentType, context->contentType);
    if (context->jprotocol && context->protocol) env->ReleaseStringUTFChars(context->jprotocol, context->protocol);
    if (context->jscheme && context->scheme) env->ReleaseStringUTFChars(context->jscheme, context->scheme);
    if (context->jserverName && context->serverName) env->ReleaseStringUTFChars(context->jserverName, context->serverName);
    if (context->jremoteHost && context->remoteHost) env->ReleaseStringUTFChars(context->jremoteHost, context->remoteHost);
    if (context->jmethod && context->method) env->ReleaseStringUTFChars(context->jmethod, context->method);
    if (context->jrequestURI && context->requestURI) env->ReleaseStringUTFChars(context->jrequestURI, context->requestURI);
    if (context->jservletPath && context->servletPath) env->ReleaseStringUTFChars(context->jservletPath, context->servletPath);
    if (context->jpathInfo && context->pathInfo) env->ReleaseStringUTFChars(context->jpathInfo, context->pathInfo);
    if (context->jpathTranslated && context->pathTranslated) env->ReleaseStringUTFChars(context->jpathTranslated, context->pathTranslated);
    if (context->jqueryString && context->queryString) env->ReleaseStringUTFChars(context->jqueryString, context->queryString);
    if (context->jauthType && context->authType) env->ReleaseStringUTFChars(context->jauthType, context->authType);
    if (context) delete context;
#endif
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_server_Native_destroy0(JNIEnv *env, jobject obj, jlong instance)
{
    Native *native = (Native *)instance;
    if (!native) return;
    if (native->destroy) native->destroy(native);
#ifdef _WIN32
    if (native->hlib) FreeLibrary(native->hlib);
#endif
    release(env, native->parameters, native->num);
    delete native;
}

JNIEXPORT const char *Native::getInitParameter(char *name)
{
    ENTRY *entry = (ENTRY *)bsearch(name, parameters, num, sizeof(ENTRY), compare);
    return entry ? entry->value.value : (const char *)NULL;
}

JNIEXPORT void Native::log(char *msg)
{
    env->CallVoidMethod(obj, logID, env->NewStringUTF(msg));
}

JNIEXPORT const char *Context::getAttribute(char *name)
{
    return NULL;
}

JNIEXPORT const char *Context::getAttributeNames()
{
    return NULL;
}

JNIEXPORT const char *Context::getCharacterEncoding()
{
    return NULL;
}

JNIEXPORT long Context::getContentLength()
{
    return env->CallIntMethod(obj, getContentLengthID);
}

JNIEXPORT const char *Context::getContentType()
{
    return contentType;
}

JNIEXPORT const char *Context::getParameter(char *name)
{
    return NULL;
}

JNIEXPORT const ENTRY *Context::getParameterNames()
{
    return NULL;
}

JNIEXPORT const ENTRY *Context::getParameterValues(char *name)
{
    return (ENTRY *)bsearch(name, parameters, num, sizeof(ENTRY), compare);
}

JNIEXPORT const char *Context::getProtocol()
{
    return protocol;
}

JNIEXPORT const char *Context::getScheme()
{
    return scheme;
}

JNIEXPORT const char *Context::getServerName()
{
    return serverName;
}

JNIEXPORT int Context::getServerPort()
{
    return env->CallIntMethod(obj, getServerPortID);
}

JNIEXPORT const char *Context::getRemoteAddr()
{
    return remoteAddr;
}

JNIEXPORT const char *Context::getRemoteHost()
{
    return remoteHost;
}

JNIEXPORT void Context::setAttribute(char *name, char *value)
{
}

JNIEXPORT void Context::removeAttribute(char *name)
{
}

JNIEXPORT int Context::isSecure()
{
    return JNI_FALSE;
}

JNIEXPORT const char *Context::getAuthType()
{
    return authType;
}

JNIEXPORT const char *Context::getCookies()
{
    return NULL;
}

JNIEXPORT __int64 Context::getDateHeader(char *name, __int64 def)
{
    return env->CallLongMethod(obj, getDateHeaderID, env->NewStringUTF(name), def);
}

JNIEXPORT const char *Context::getHeader(char *name)
{
    ENTRY *entry = (ENTRY *)bsearch(name, header, size, sizeof(ENTRY), compare);
    return entry ? entry->value.value : (const char *)NULL;
}

JNIEXPORT const char *Context::getHeaders(char *name)
{
    return NULL;
}

JNIEXPORT const char *Context::getHeaderNames()
{
    return NULL;
}

JNIEXPORT long Context::getIntHeader(char *name, long def)
{
    return env->CallIntMethod(obj, getIntHeaderID, env->NewStringUTF(name), def);
}

JNIEXPORT const char *Context::getMethod()
{
    return method;
}

JNIEXPORT const char *Context::getPathInfo()
{
    return pathInfo;
}

JNIEXPORT const char *Context::getPathTranslated()
{
    return pathTranslated;
}

JNIEXPORT const char *Context::getContextPath()
{
    return pathTranslated;
}

JNIEXPORT const char *Context::getQueryString()
{
    return queryString;
}

JNIEXPORT const char *Context::getRemoteUser()
{
    return getHeader((char *)"from");
}

JNIEXPORT int Context::isUserInRole(char *role)
{
    return JNI_FALSE;
}

JNIEXPORT char *Context::getUserPrincipal()
{
    return NULL;
}

JNIEXPORT char *Context::getRequestedSessionId()
{
    return NULL;
}

JNIEXPORT const char *Context::getRequestURI()
{
    return requestURI;
}

JNIEXPORT const char *Context::getServletPath()
{
    return servletPath;
}

JNIEXPORT const char *Context::getSession()
{
    return NULL;
}

JNIEXPORT void Context::setContentLength(long len)
{
    env->CallVoidMethod(obj, setContentLengthID, len);
}

JNIEXPORT void Context::setContentType(char *type)
{
    env->CallVoidMethod(obj, setContentTypeID, env->NewStringUTF(type));
}

JNIEXPORT void Context::setBufferSize(long size)
{
    env->CallVoidMethod(obj, setBufferSizeID, size);
}

JNIEXPORT long Context::getBufferSize()
{
    return env->CallIntMethod(obj, getBufferSizeID);
}

JNIEXPORT const void Context::flushBuffer()
{
}

JNIEXPORT const int Context::isCommitted()
{
    return env->CallBooleanMethod(obj, isCommittedID);
}

JNIEXPORT const void Context::reset()
{
}

JNIEXPORT const void Context::addCookie(char *name, char *value)
{
}

JNIEXPORT const int Context::containsHeader(char *name)
{
    return JNI_FALSE;
}

JNIEXPORT const char *Context::encodeURL(char *url)
{
    return NULL;
}

JNIEXPORT const char *Context::encodeRedirectURL(char *url)
{
    return NULL;
}

JNIEXPORT void Context::sendError(long sc, char *msg)
{
    env->CallVoidMethod(obj, sendErrorID, sc, env->NewStringUTF(msg));
}

JNIEXPORT void Context::sendRedirect(char *location)
{
    env->CallVoidMethod(obj, sendRedirectID, env->NewStringUTF(location));
}

JNIEXPORT void Context::setDateHeader(char *name, __int64 date)
{
    env->CallVoidMethod(obj, setDateHeaderID, date);
}

JNIEXPORT void Context::addDateHeader(char *name, __int64 date)
{
    env->CallVoidMethod(obj, addDateHeaderID, date);
}

JNIEXPORT void Context::setHeader(char *name, char *value)
{
    env->CallVoidMethod(obj, setHeaderID, env->NewStringUTF(name), env->NewStringUTF(value));
}

JNIEXPORT void Context::addHeader(char *name, char *value)
{
    env->CallVoidMethod(obj, addHeaderID, env->NewStringUTF(name), env->NewStringUTF(value));
}

JNIEXPORT void Context::setIntHeader(char *name, long value)
{
    env->CallVoidMethod(obj, setIntHeaderID, value);
}

JNIEXPORT void Context::addIntHeader(char *name, long value)
{
    env->CallVoidMethod(obj, addIntHeaderID, value);
}

JNIEXPORT void Context::setStatus(long sc, char *sm)
{
    env->CallVoidMethod(obj, setStatusID, sc, env->NewStringUTF(sm));
}

JNIEXPORT const char *Context::getHeaderName(long n)
{
    return n < size ? header[n].key : (const char *)NULL;
}

JNIEXPORT const char *Context::getHeaderByNumber(long n)
{
    return n < size ? header[n].value.value : (const char *)NULL;
}

JNIEXPORT int Context::read(int off, int len)
{
    int n = env->CallIntMethod(in, readID, buffer, off, len);
    buf = env->GetByteArrayElements(buffer, NULL);
    return n;
}

JNIEXPORT void Context::write(int off, int len)
{
    env->ReleaseByteArrayElements(buffer, buf, JNI_COMMIT);
    env->CallVoidMethod(out, writeID, buffer, off, len);
}

JNIEXPORT const char *Context::getRealPath(char *path)
{
    jstring realPath = (jstring)env->CallObjectMethod(obj, getRealPathID, env->NewStringUTF(path));
    if (!realPath) return NULL;
    const char *s = env->GetStringUTFChars(realPath, NULL);
    if (!s) return NULL;
    char *p = new char[strlen(s) + 1];
    strcpy(p, s);
    env->ReleaseStringUTFChars(realPath, s);
    return p;
}
