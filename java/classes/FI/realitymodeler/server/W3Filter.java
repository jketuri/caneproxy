
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.sql.*;
import java.text.*;
import java.util.*;
import java.util.Date;
import javax.naming.*;
import javax.naming.directory.*;
import javax.naming.ldap.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** A filter configuration object used by a servlet container used to pass information
    to a filter during initialization.
    Since: Servlet 2.3

    @see Filter

*/
class W3Filter implements FilterConfig, Cloneable {
    Hashtable<String, String> initParameters = null;
    Filter filter = null;
    Class<? extends Filter> filterClass = null;
    String className = null;
    String filterName = null;
    W3Context w3context = null;

    public W3Filter(W3Context w3context, Filter filter, Class<? extends Filter> filterClass, String filterName, String className, Hashtable<String, String> initParameters) {
        this.w3context = w3context;
        this.filter = filter;
        this.filterClass = filterClass;
        this.filterName = filterName;
        this.className = className;
        this.initParameters = initParameters;
    }

    /** Returns the filter name of this filter as defined in the deployment descriptor.

     */
    public String getFilterName() {
        return filterName;
    }

    /** Returns a String containing the value of the named initialization parameter,
        or null if the parameter does not exist.

        @param name a String specifying the name of the initialization parameter
        @return a String containing the value of the initialization parameter

    */
    public String getInitParameter(String name) {
        return initParameters.get(name);
    }

    /** Returns the names of the servlet's initialization parameters as an
        Enumeration of String objects, or an empty Enumeration if the servlet has
        no initialization parameters.

        @return an Enumeration of String objects containing the names of the
        servlet's initialization parameters

    */
    public Enumeration getInitParameterNames() {
        return initParameters.keys();
    }

    /** Returns a reference to the ServletContext in which the caller is executing.

        @return a ServletContext object, used by the caller to interact with its
        servlet container
        @see ServletContext

    */
    public ServletContext getServletContext() {
        return w3context;
    }

    public Object clone() {
        return new W3Filter(w3context, filter, filterClass, filterName, className, initParameters);
    }

    public String toString() {
        return getClass().getName() +
            " {filterName=" + filterName +
            ", className=" + className +
            ", filterClass=" + filterClass +
            ", filter=" + filter +
            ", w3context=" + w3context + "}";
    }

}
