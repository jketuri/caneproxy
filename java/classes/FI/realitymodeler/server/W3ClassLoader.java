
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import java.io.*;
import java.net.*;
import java.util.*;

public class W3ClassLoader extends URLClassLoader {

    public W3ClassLoader(URL urls[], ClassLoader parent) {
        super(urls, parent, W3Factory.instance);
    }

    public void addURL(URL url) {
        super.addURL(url);
    }

    public Class loadClass(String name, boolean resolve)
        throws ClassNotFoundException {
        if (name.startsWith("javax.servlet.") || name.startsWith("javax.xml.") || name.startsWith("org.xml.") || name.startsWith("org.w3c.dom.")) return super.loadClass(name, resolve);
        Class clazz = findLoadedClass(name);
        if (clazz != null) return clazz;
        try {
            clazz = findClass(name);
            if (resolve) resolveClass(clazz);
            return clazz;
        } catch (ClassNotFoundException ex) {}
        return super.loadClass(name, resolve);
    }

    public URL findResource(String name) {
        URL url = super.findResource(name);
        if (url == null && name.startsWith("/"))
            url = super.findResource(name.substring(1));
        return url;
    }

    public Enumeration<URL> findResources(String name)
        throws IOException {
        Enumeration<URL> urls = super.findResources(name);
        if (urls == null && name.startsWith("/"))
            urls = super.findResources(name.substring(1));
        return urls;
    }

}
