
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.util.*;
import javax.servlet.http.*;

public class Response {
    public Store store;
    public W3URLConnection uc;
    public HttpServletResponse res;
    public Set<W3URLConnection> sharedConnections;
    public Map<String, Set<W3URLConnection>> sharedConnectionsPool;
    public String cache;
    public String method;
    public String viaValue;
}
