
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class Html2Plain extends HttpServlet {
    static final long serialVersionUID = 0L;

    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        res.setContentType(Support.plainType);
        Support.filter(req.getInputStream(), res.getOutputStream());
    }

}
