
package FI.realitymodeler.server;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class W3ResponseFacade implements HttpServletResponse {
    HttpServletResponse response;

    public W3ResponseFacade(HttpServletResponse response) {
        this.response = response;
    }

    /** Returns the name of the character encoding (MIME charset) used for
        the body sent in this response. The character encoding may have been
        specified explicitly using the setCharacterEncoding(java.lang.String) or
        setContentType(java.lang.String) methods, or implicitly using the
        setLocale(java.util.Locale) method. Explicit specifications take
        precedence over implicit specifications. Calls made to these methods
        after getWriter has been called or after the response has been committed
        have no effect on the character encoding. If no character encoding has
        been specified, ISO-8859-1 is returned.  See RFC 2047
        (http://www.ietf.org/rfc/rfc2047.txt) for more information about
        character encoding and MIME. 

        @return a String specifying the name of the character encoding, for
        example, UTF-8

    */
    public String getCharacterEncoding() {
        return response.getCharacterEncoding();
    }

    /** Returns the content type used for the MIME body sent in this
        response.  The content type proper must have been specified using
        setContentType(java.lang.String) before the response is committed. If no
        content type has been specified, this method returns null. If a content
        type has been specified and a character encoding has been explicitly or
        implicitly specified as described in getCharacterEncoding(), the charset
        parameter is included in the string returned. If no character encoding
        has been specified, the charset parameter is omitted. 

        @return a String specifying the content type, for example, text/html;
        charset=UTF-8, or null

    */
    public String getContentType() {
        return response.getContentType();
    }

    /** Forces any content in the buffer to be written to the client. A call to this
        method automatically commits the response, meaning the status code and
        headers will be written.

        @throws IOException

        @see javax.servlet.ServletResponse#setBufferSize(int)
        @see javax.servlet.ServletResponse#getBufferSize()
        @see javax.servlet.ServletResponse#isCommitted()
        @see javax.servlet.ServletResponse#reset()

    */
    public void flushBuffer() throws IOException {
        response.flushBuffer();
    }

    /** Returns the actual buffer size used for the response. If no buffering is used,
        this method returns 0.

        @return the actual buffer size used

        @see javax.servlet.ServletResponse#setBufferSize(int)
        @see javax.servlet.ServletResponse#flushBuffer()
        @see javax.servlet.ServletResponse#isCommitted()
        @see javax.servlet.ServletResponse#reset()

    */
    public int getBufferSize() {
        return response.getBufferSize();
    }

    /** Returns a ServletOutputStream suitable for writing binary data in the
        response. The servlet container does not encode the binary data.
        Calling flush() on the ServletOutputStream commits the response. Either this
        method or getWriter() may be called to write the body, not both.

        @return a ServletOutputStream for writing binary data

        @throws IllegalStateException if the getWriter method has been called on this
        response

        @throws IOException if an input or output exception occurred

        @see getWriter()

    */
    public ServletOutputStream getOutputStream()
        throws IOException {
        return response.getOutputStream();
    }

    /** Returns a PrintWriter object that can send character text to the client. The
        character encoding used is the one specified in the charset-property of the
        setContentType(String) method, which must be called before calling this
        method for the charset to take effect.
        If necessary, the MIME type of the response is modified to reflect the character
        encoding used.
        Calling flush() on the PrintWriter commits the response.
        Either this method or getOutputStream() may be called to write the body,
        not both.

        @return a PrintWriter object that can return character data to the client

        @throws UnsupportedEncodingException if the charset specified in
        setContentType cannot be used

        @throws IllegalStateException if the getOutputStream method has already been
        called for this response object

        @throws IOException if an input or output exception occurred

        @see javax.servlet.ServletResponse#getOutputStream()
        @see javax.servlet.ServletResponse#setContentType(String)

    */
    public PrintWriter getWriter() throws IOException, UnsupportedEncodingException {
        return response.getWriter();
    }

    /** Returns a boolean indicating if the response has been committed. A commited
        response has already had its status code and headers written.

        @return a boolean indicating if the response has been committed

        @see javax.servlet.ServletResponse#setBufferSize(int)
        @see javax.servlet.ServletResponse#getBufferSize()
        @see javax.servlet.ServletResponse#flushBuffer()
        @see javax.servlet.ServletResponse#reset()

    */
    public boolean isCommitted() {
        return response.isCommitted();
    }

    /** Clears any data that exists in the buffer as well as the status code and headers.
        If the response has been committed, this method throws an IllegalStateException.

        @throws IllegalStateException if the response has already been committed

        @see javax.servlet.ServletResponse#setBufferSize(int)
        @see javax.servlet.ServletResponse#getBufferSize()
        @see javax.servlet.ServletResponse#flushBuffer()
        @see javax.servlet.ServletResponse#isCommitted()

    */
    public void reset() {
        response.reset();
    }

    /** Clears the content of the underlying buffer in the response without clearing
        headers or status code. If the response has been committed, this method
        throws an IllegalStateException.
        Since: 2.3

        @see javax.servlet.ServletResponse#setBufferSize(int)
        @see javax.servlet.ServletResponse#getBufferSize()
        @see javax.servlet.ServletResponse#isCommitted()
        @see javax.servlet.ServletResponse#reset()

    */
    public void resetBuffer() {
        response.resetBuffer();
    }

    /** Sets the preferred buffer size for the body of the response. The servlet container
        will use a buffer at least as large as the size requested. The actual buffer
        size used can be found using getBufferSize.
        A larger buffer allows more content to be written before anything is actually
        sent, thus providing the servlet with more time to set appropriate status codes
        and headers. A smaller buffer decreases server memory load and allows the
        client to start receiving data more quickly.
        This method must be called before any response body content is written; if
        content has been written, this method throws an IllegalStateException.

        @param size the preferred buffer size
        @throws IllegalStateException if this method is called after content has been
        written
        @see javax.servlet.ServletResponse#getBufferSize()
        @see javax.servlet.ServletResponse#flushBuffer()
        @see javax.servlet.ServletResponse#isCommitted()
        @see javax.servlet.ServletResponse#reset()

    */
    public void setBufferSize(int size) {
        response.setBufferSize(size);
    }

    /** Sets the character encoding (MIME charset) of the response being
        sent to the client, for example, to UTF-8. If the character encoding has
        already been set by setContentType(java.lang.String) or
        setLocale(java.util.Locale), this method overrides it. Calling
        setContentType(java.lang.String) with the String of text/html and
        calling this method with the String of UTF-8 is equivalent with calling
        setContentType with the String of text/html; charset=UTF-8.  This method
        has no effect if it is called after getWriter has been called or after
        the response has been committed. 

        @param characterEncoding - a String specifying only the character set
        defined by IANA Character Sets
        (http://www.iana.org/assignments/character-sets)

        @see setLocale

    */
    public void setCharacterEncoding(String characterEncoding) {
        response.setCharacterEncoding(characterEncoding);
    }

    /** Sets the length of the content body in the response In HTTP servlets, this
        method sets the HTTP Content-Length header.

        @param len an integer specifying the length of the content being returned to the
        client; sets the Content-Length header

    */
    public void setContentLength(int len) {
        response.setContentLength(len);
    }

    /**
     * Sets the length of the content body in the response In HTTP servlets,
     * this method sets the HTTP Content-Length header.
     *
     * @param length
     *            an integer specifying the length of the content being returned
     *            to the client; sets the Content-Length header
     *
     * @since Servlet 3.1
     */
    public void setContentLengthLong(long length) {
        response.setContentLengthLong(length);
    }

    /** Sets the content type of the response being sent to the client. The content type
        may include the type of character encoding used, for example,
        text/html; charset=ISO-8859-4.
        If obtaining a PrintWriter, this method should be called first.

        @param ct a String specifying the MIME type of the content
        @see javax.servlet.ServletResponse#getOutputStream()
        @see javax.servlet.ServletResponse#getWriter()

    */
    public void setContentType(String ct) {
        response.setContentType(ct);
    }

    /** Sets the locale of the response, setting the headers (including the
        Content-Type's charset) as appropriate. This method should be called
        before a call to getWriter(). By default, the response locale is the
        default locale for the server. 

        @param locale the locale of the response
        @see javax.servlet.ServletResponse#getLocale()

    */
    public void setLocale(Locale locale) {
        response.setLocale(locale);
    }

    /** Returns the locale specified for this response using the
        setLocale(java.util.Locale) method. Calls made to setLocale after the
        response is committed have no effect. If no locale has been specified,
        the container's default locale is returned. 

        @return the locale assigned to the response.

        @see setLocale(java.util.Locale)

    */
    public Locale getLocale() {
        return response.getLocale();
    }

    /** Adds the specified cookie to the response. This method can be called multiple
        times to set more than one cookie.

        @param cookie the Cookie to return to the client

    */
    public void addCookie(Cookie cookie) {
        response.addCookie(cookie);
    }

    /** Adds a response header with the given name and date value. The date is specified
        in terms of milliseconds since the epoch. This method allows response
        headers to have multiple values.

        @param name the name of the header to set
        @param date the additional date value
        @see javax.servlet.http.HttpServletResponse#setDateHeader(String, long)

    */
    public void addDateHeader(String name, long date) {
        response.addDateHeader(name, date);
    }

    /** Adds a response header with the given name and value. This method allows
        response headers to have multiple values.

        @param name the name of the header
        @param value the additional header value
        @see javax.servlet.http.HttpServletResponse#setHeader(String, String)

    */
    public void addHeader(String name, String value) {
        response.addHeader(name, value);
    }

    /** Adds a response header with the given name and integer value. This method
        allows response headers to have multiple values.

        @param name the name of the header
        @param value the assigned integer value
        @see javax.servlet.http.HttpServletResponse#setIntHeader(String, int)

    */
    public void addIntHeader(String name, int value) {
        response.addIntHeader(name, value);
    }

    /** Returns a boolean indicating whether the named response header has already
        been set.

        @param name the header name
        @return true if the named response header has already been set; false
        otherwise

    */
    public boolean containsHeader(String name) {
        return response.containsHeader(name);
    }

    /** @deprecated Deprecated as of version 2.1, use encodeRedirectURL(String url) instead
        @param url the url to be encoded.
        @return the encoded URL if encoding is needed; the unchanged URL
        otherwise.

    */
    @Deprecated
    public String encodeRedirectUrl(String url) {
        return response.encodeRedirectUrl(url);
    }

    /** Encodes the specified URL for use in the sendRedirect method or, if encoding
        is not needed, returns the URL unchanged. The implementation of this
        method includes the logic to determine whether the session ID needs to be
        encoded in the URL. Because the rules for making this determination can differ
        from those used to decide whether to encode a normal link, this method is
        seperate from the encodeURL method.
        All URLs sent to the HttpServletResponse.sendRedirect method should
        be run through this method. Otherwise, URL rewriting cannot be used with
        browsers which do not support cookies.

        @param url the url to be encoded.
        @return the encoded URL if encoding is needed; the unchanged URL
        otherwise.
        @see javax.servlet.http.HttpServletResponse#sendRedirect(String)
        @see javax.servlet.http.HttpServletResponse#encodeUrl(String)

    */
    public String encodeRedirectURL(String url) {
        return response.encodeRedirectURL(url);
    }

    /** @deprecated Deprecated as of version 2.1, use encodeURL(String url) instead
        @param url - the url to be encoded.
        @return the encoded URL if encoding is needed; the unchanged URL
        otherwise.

    */
    @Deprecated
    public String encodeUrl(String url) {
        return response.encodeUrl(url);
    }

    /** Encodes the specified URL by including the session ID in it, or, if encoding is
        not needed, returns the URL unchanged. The implementation of this method
        includes the logic to determine whether the session ID needs to be encoded in
        the URL. For example, if the browser supports cookies, or session tracking is
        turned off, URL encoding is unnecessary.
        For robust session tracking, all URLs emitted by a servlet should be run
        through this method. Otherwise, URL rewriting cannot be used with brows-ers
        which do not support cookies.

        @param url the url to be encoded.
        @return the encoded URL if encoding is needed; the unchanged URL
        otherwise.

    */
    public String encodeURL(String url) {
        return response.encodeURL(url);
    }

    /** Sends an error response to the client using the specified status code and clearing
        the buffer.
        If the response has already been committed, this method throws an IllegalStateException.
        After using this method, the response should be considered
        to be committed and should not be written to.

        @param sc the error status code
        @throws IOException If an input or output exception occurs
        @throws IllegalStateException If the response was committed before this method
        call

    */
    public void sendError(int sc) throws IOException {
        response.sendError(sc);
    }

    /** Sends an error response to the client using the specified status clearing the
        buffer. The server defaults to creating the response to look like an HTML-formatted
        server error page containing the specified message, setting the content
        type to "text/html", leaving cookies and other headers unmodified. If an
        error-page declaration has been made for the web application corresponding
        to the status code passed in, it will be served back in preference to the suggested
        msg parameter.
        If the response has already been committed, this method throws an IllegalStateException.
        After using this method, the response should be considered
        to be committed and should not be written to.

        @param sc the error status code
        @param msg the descriptive message
        @throws IOException If an input or output exception occurs
        @throws IllegalStateException If the response was committed

    */
    public void sendError(int sc, String msg) throws IOException {
        response.sendError(sc, msg);
    }

    /** Sends a temporary redirect response to the client using the specified redirect
        location URL. This method can accept relative URLs; the servlet container
        must convert the relative URL to an absolute URL before sending the
        response to the client. If the location is relative without a leading "/" the container
        interprets it as relative to the current request URI. If the location is relative
        with a leading "/" the container interprets it as relative to the servlet
        container root.
        If the response has already been committed, this method throws an IllegalStateException.
        After using this method, the response should be considered
        to be committed and should not be written to.

        @param location the redirect location URL
        @throws IOException If an input or output exception occurs
        @throws IllegalStateException If the response was committed

    */
    public void sendRedirect(String location) throws IOException {
        response.sendRedirect(location);
    }

    /** Sets a response header with the given name and date-value. The date is specified
        in terms of milliseconds since the epoch. If the header had already been
        set, the new value overwrites the previous one. The containsHeader method
        can be used to test for the presence of a header before setting its value.

        @param name the name of the header to set
        @param date the assigned date value
        @see javax.servlet.http.HttpServletResponse#containsHeader(String)
        @see javax.servlet.http.HttpServletResponse#addDateHeader(String, long)

    */
    public void setDateHeader(String name, long date) {
        response.setDateHeader(name, date);
    }

    /** Sets a response header with the given name and value. If the header had
        already been set, the new value overwrites the previous one. The containsHeader
        method can be used to test for the presence of a header before setting its value.

        @param name the name of the header
        @param value the header value
        @see javax.servlet.http.HttpServletResponse#containsHeader(String)
        @see javax.servlet.http.HttpServletResponse#addHeader(String, String)

    */
    public void setHeader(String name, String value) {
        response.setHeader(name, value);
    }

    /** Sets a response header with the given name and integer value. If the header
        had already been set, the new value overwrites the previous one. The
        containsHeader method can be used to test for the presence of a header
        before setting its value.

        @param name the name of the header
        @param value the assigned integer value
        @see javax.servlet.http.HttpServletResponse#containsHeader(String)
        @see javax.servlet.http.HttpServletResponse#addIntHeader(String, int)

    */
    public void setIntHeader(String name, int value) {
        response.setIntHeader(name, value);
    }

    /** Sets the status code for this response. This method is used to set the return
        status code when there is no error (for example, for the status codes SC_OK
        or SC_MOVED_TEMPORARILY). If there is an error, and the caller wishes
        to invoke an defined in the web applicaion, the sendError method should be
        used instead.
        The container clears the buffer and sets the Location header, preserving cook-ies
        and other headers.

        @param sc the status code
        @see sendError(int, String)

    */
    public void setStatus(int sc) {
        response.setStatus(sc);
    }

    /** @deprecated Deprecated as of version 2.1, due to ambiguous meaning of the message
        parameter. To set a status code use setStatus(int), to send an error with a
        description use sendError(int, String). Sets the status code and message
        for this response.

        @param sc the status code
        @param sm the status message

    */
    @Deprecated
    public void setStatus(int sc, String sm) {
        response.setStatus(sc, sm);
    }

    /**
     * @return TODO
     * @since Servlet 3.0 TODO SERVLET3 - Add comments
     */
    public int getStatus() {
        return response.getStatus();
    }

    /**
     * @param name
     * @return TODO
     * @since Servlet 3.0 TODO SERVLET3 - Add comments
     */
    public String getHeader(String name) {
        return response.getHeader(name);
    }

    /**
     * @param name
     * @return TODO
     * @since Servlet 3.0 TODO SERVLET3 - Add comments
     */
    public Collection<String> getHeaders(String name) {
        return response.getHeaders(name);
    }

    /**
     * @return TODO
     * @since Servlet 3.0 TODO SERVLET3 - Add comments
     */
    public Collection<String> getHeaderNames() {
        return response.getHeaderNames();
    }

}
