
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.sql.*;
import java.text.*;
import java.util.*;
import java.util.Date;
import javax.naming.*;
import javax.naming.directory.*;
import javax.naming.ldap.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** A FilterChain is an object provided by the servlet container to the developer giving
    a view into the invocation chain of a filtered request for a resource. Filters use
    the FilterChain to invoke the next filter in the chain, or if the calling filter is the
    last filter in the chain, to invoke the resource at the end of the chain.
    Since: Servlet 2.3

    @see Filter

*/
public class W3FilterChain implements FilterChain, Cloneable {
    FilterTarget authFilterTarget = null;
    Iterator filterTargetItems = null;
    List<FilterTarget> filterTargetList = null, servletFilterTargetList = null;
    W3Request w3request = null;
    W3Servlet w3servlet = null;
    DispatcherType dispatcherType = null;
    boolean named = false;

    public W3FilterChain(List<FilterTarget> filterTargetList, List<FilterTarget> servletFilterTargetList, W3Servlet w3servlet) {
        this.filterTargetList = filterTargetList;
        this.servletFilterTargetList = servletFilterTargetList;
        this.w3servlet = w3servlet;
        filterTargetItems = new IteratorSequence(new Iterator[] {filterTargetList.iterator(), servletFilterTargetList.iterator()}, false);
    }

    /** Causes the next filter in the chain to be invoked, or if the calling filter is the
        last filter in the chain, causes the resource at the end of the chain to be
        invoked.

        @param request the request to pass along the chain.
        @param response the response to pass along the chain.

        @throws ServletException
        @throws IOException
        Since: 2.3

    */
    public void doFilter(ServletRequest request, ServletResponse response)
        throws IOException, ServletException {
        FilterTarget filterTarget = null;
        if (authFilterTarget != null) {
            filterTarget = authFilterTarget;
            authFilterTarget = null;
        } else {
            if (filterTargetItems == null) return;
            do {
                if (!filterTargetItems.hasNext()) {
                    filterTargetItems = null;
                    w3servlet.service(request, response, dispatcherType, named, true);
                    return;
                }
                filterTarget = (FilterTarget)filterTargetItems.next();
            } while (dispatcherType != null &&
                     filterTarget.dispatcherTypes != null &&
                     !filterTarget.dispatcherTypes.contains(dispatcherType));
        }
        if (w3servlet.w3context.w3server.verbose)
            w3servlet.w3context.log("filterChain filter=" + filterTarget.w3filter.className + ", dispatcherTypes=" + filterTarget.dispatcherTypes);
        if (filterTarget.w3filter.filter != null)
            filterTarget.w3filter.filter.doFilter(request, response, this);
    }

    public Object clone() {
        return new W3FilterChain(filterTargetList, servletFilterTargetList, w3servlet);
    }

    public String toString() {
        return getClass().getName() +
            " {filterTargetList=" + filterTargetList +
            ", servletFilterTargetList=" + servletFilterTargetList +
            ", authFilterTarget=" + authFilterTarget +
            ", w3servlet=" + w3servlet + "}";
    }

}
