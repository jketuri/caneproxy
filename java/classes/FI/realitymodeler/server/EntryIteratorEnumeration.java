
package FI.realitymodeler.server;

import java.util.*;

public class EntryIteratorEnumeration implements Enumeration {
    Iterator iterator;

    public EntryIteratorEnumeration(Iterator iterator) {
        this.iterator = iterator;
    }

    public boolean hasMoreElements() {
        return iterator.hasNext();
    }

    public Object nextElement() {
        return ((Map.Entry)iterator.next()).getValue();
    }

}
