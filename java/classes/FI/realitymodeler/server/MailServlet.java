
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Email (SMTP/POP3) gateway servlet.<br>
    With PUT-method path info must be of form:<br>
    /to/subject/from<br>
    With GET-method:<br>
    /host<br>
    If path info is empty or new-parameter is specified, servlet returns general mail form.
    If form-parameter is specified, servlet returns mailing form.
*/
public class MailServlet extends HttpServlet {
    static final long serialVersionUID = 0L;

    Map<String, String> servletPathTable = null;
    ResourceBundle messages;

    public void init(ServletConfig config)
        throws ServletException {
        super.init(config);
        messages = W3URLConnection.getMessages("FI.realitymodeler.server.resources.Messages");
        servletPathTable = (Map<String, String>)config.getServletContext().getAttribute("FI.realitymodeler.server.W3Server/servletPathTable");
    }

    public void doPost(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException {
        try {
            Messaging.post(this, req, res, messages, "pop3");
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }

    public void doPut(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException {
        Messaging.put(this, req, res, null);
    }

    public void doGet(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException {
        try {
            Messaging.get(this, req, res, messages, "mail", W3Pop3URLConnection.pop3ServerHost, servletPathTable);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }

    public void doHead(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException {
        doGet(req, res);
    }

}
