
package FI.realitymodeler.server;

import javax.servlet.*;

public class Step {
    public String mimeName;
    public W3Servlet w3servlet;
    public boolean start;

    public Step(String mimeName, W3Servlet w3servlet, boolean start) {
        this.mimeName = mimeName;
        this.w3servlet = w3servlet;
        this.start = start;
    }

}
