
package FI.realitymodeler.server;

import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;

class MimeFileNameMap implements FileNameMap {
    Map<String,String> mimeMap;
    String defaultType;

    MimeFileNameMap(Map<String,String> mimeMap, String defaultType) {
        this.mimeMap = mimeMap;
        this.defaultType = defaultType;
    }

    public String getContentTypeFor(String fileName) {
        int i = fileName.lastIndexOf('.');
        String contentType = i != -1 ? mimeMap.get(fileName.substring(i + 1).toLowerCase()) : null;
        return i == -1 || contentType != null ? contentType : defaultType;
    }

}

public class MimeMap {
    public static final String unexp_token = "Unexpected token";

    /** Reads mime type map from given stram tokenizer
        @param sTok stream tokenizer containing input stream
        @param mimeMap hash map which is used inside file name map
        @param mimeTypes tree map where keys are mime types and values vectors containing file extensions
        @param filters hash map where keys are mime types and values filter servlet chain vectors
        @param servlets must contain servlet names mapped to servlets if filters are used
        @return file name map containing mime types */
    public static FileNameMap readMimeMap(StreamTokenizer sTok, Map<String,String> mimeMap, Map<String,Vector<String>> mimeTypes, Map<String,Vector<Step>> filters, Map servlets, String defaultTypeValue[])
        throws ParseException, IOException {
        sTok.resetSyntax();
        sTok.whitespaceChars(0, 32);
        sTok.wordChars(33, 255);
        sTok.ordinaryChar('=');
        sTok.commentChar('#');
        sTok.quoteChar('"');
        Vector<String> exts = null;
        Object type = null;
        String defaultType = Support.plainType;
        String mimeName = null;
        while (sTok.nextToken() != sTok.TT_EOF) {
            if (sTok.ttype != sTok.TT_WORD && sTok.ttype != '"') throw new ParseException(unexp_token, 0);
            String s = sTok.sval.trim();
            if (sTok.nextToken() == '=') {
                mimeName = null;
                Vector<Step> chain = null;
                StringTokenizer st = new StringTokenizer(s);
                while (st.hasMoreTokens()) {
                    StringTokenizer chainSt = new StringTokenizer(st.nextToken(), ":");
                    if (!chainSt.hasMoreTokens()) throw new ParseException(unexp_token, 0);
                    String chainMimeName = chainSt.nextToken();
                    boolean start = false;
                    if (mimeName == null) {
                        mimeTypes.put((String)(type = mimeName = chainMimeName), exts = new Vector<String>());
                        if (filters == null || servlets == null || !st.hasMoreTokens()) break;
                        filters.put(mimeName, chain = new Vector<Step>());
                        if (!chainSt.hasMoreTokens()) continue;
                        chainMimeName = null;
                    } else {
                        type = chain;
                        if (chainMimeName.startsWith(".")) {
                            chainMimeName = chainMimeName.substring(1);
                            start = true;
                        }
                        if (!chainSt.hasMoreTokens()) throw new ParseException(unexp_token, 0);
                    }
                    W3Servlet w3servlet = (W3Servlet)servlets.get(s = chainSt.nextToken());
                    if (w3servlet == null) throw new ParseException((chainMimeName != null ? "Filter" : "Quality filter") + " servlet " + s + " not found", 0);
                    chain.addElement(new Step(chainMimeName, w3servlet, start));
                }
                continue;
            } else sTok.pushBack();
            exts.addElement(s = s.toLowerCase());
            if (s.equals(".")) continue;
            if (s.equals("*")) defaultType = mimeName;
            mimeMap.put(s, mimeName);
        }
        if (defaultTypeValue != null) {
            defaultTypeValue[0] = defaultType;
            defaultType = null;
        }
        return new MimeFileNameMap(mimeMap, defaultType);
    }

    public static FileNameMap readMimeMap(StreamTokenizer sTok, String path, String defaultTypeValue[])
        throws IOException, ParseException {
        try {
            return MimeMap.readMimeMap(sTok, new HashMap<String, String>(), new TreeMap<String,Vector<String>>(), null, null, defaultTypeValue);
        } catch (ParseException ex) {
            throw new ParseException("Error in path " + path + " in line " +
                                     sTok.lineno() + "\n" + Support.stackTrace(ex), sTok.lineno());
        }
    }

    public static FileNameMap readMimeMap(String filename, String defaultTypeValue[])
        throws IOException, ParseException {
        BufferedReader bf = null;
        try {
            return MimeMap.readMimeMap(new StreamTokenizer(bf = new BufferedReader(new FileReader(filename))), filename, defaultTypeValue);
        } finally {
            if (bf != null) bf.close();
        }
    }

}