
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Custom authentication servlet.
 */
public class AuthServlet extends HttpServlet {
    static final long serialVersionUID = 0L;

    public Cookie makeCookie(String location, String username, String password)
        throws MalformedURLException, UnknownHostException, UnsupportedEncodingException {
        Cookie cookie = new Cookie(getClass().getName(), URLEncoder.encode(W3URLConnection.basicCredentials(username, password), "8859_1"));
        String path;
        if (location.indexOf(':') != -1) {
            URL url = new URL(location);
            cookie.setDomain(url.getHost());
            path = url.getFile();
        } else path = location;
        int i = path.indexOf('?');
        if (i != -1) path = path.substring(0, i);
        cookie.setPath(path);
        return cookie;
    }

    public void service(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException {
        String location = req.getParameter("location"), challenge = req.getParameter("challenge"),
            username = req.getParameter("username"), password = req.getParameter("password");
        if (location == null || challenge == null || username == null || password == null) throw new ServletException("Invalid form");
        challenge = java.net.URLDecoder.decode(challenge, "8859_1");
        res.addCookie(makeCookie(java.net.URLDecoder.decode(location, "8859_1"), username, password));
        res.sendRedirect(location);
    }

}
