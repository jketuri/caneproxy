
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Server side include servlet. */
public class SSIServlet extends HttpServlet {
    static final long serialVersionUID = 0L;

    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        W3Request w3r = (W3Request)req;
        InputStream in = req.getInputStream();
        res.setContentType("text/html");
        res.setHeader("Pragma", "no-cache");
        res.setHeader("Cache-Control", "no-cache");
        PrintWriter writer = res.getWriter();
        String name = null, code = null, codebase = null;
        Hashtable<String,String> initParameters = null;
        int c;
        while ((c = in.read()) != -1) {
            if (c != '<') {
                writer.write(c);
                continue;
            }
            if ((c = Support.scanTag(in, "/servlet", false)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) {
                if (name != null || code != null) {
                    Servlet servlet = null;
                    if (name != null) servlet = w3r.w3context.getServlet(name);
                    if (servlet != null) {
                        if (w3r.w3server.verbose) log("Calling servlet " + name);
                        servlet.service(req, res);
                    } else if (code != null)
                        try {
                            W3Servlet w3servlet = w3r.w3server.loadServlet(name, codebase != null ? codebase + code : code, initParameters, false, false);
                            servlet = w3servlet.servlet;
                            if (w3r.w3server.verbose) log("Calling servlet of " + code);
                            servlet.service(req, res);
                        } catch (Exception ex) {
                            log(ex.toString());
                        } finally {
                            if (name == null && servlet != null) servlet.destroy();
                        }
                    if (servlet == null) log("Loading servlet " + (name != null ? "name=" + name : "code=" + code) + " failed");
                } else log("Invalid servlet-tag");
                while (c > -1 && c != '>') c = in.read();
                continue;
            }
            if (c == '/')
                if (Support.scanTag(in, "param", true) == Support.TAG_WITH_VALUES) {
                    Map<String,String> values = new HashMap<String,String>();
                    Support.scanValues(in, values);
                    String paramName = values.get("name"), paramValue = values.get("value");
                    if (paramName != null && paramValue != null) {
                        Object queryValue = w3r.queryParameters.get(paramName);
                        if (queryValue != null) {
                            if (queryValue instanceof Vector) ((Vector<String>)queryValue).add(paramValue);
                            else {
                                Vector<String> vector = new Vector<String>();
                                vector.add((String)queryValue);
                                vector.add(paramValue);
                                queryValue = vector;
                            }
                        } else queryValue = paramValue;
                        w3r.queryValues.put(paramName, queryValue);
                    }
                    else log("Invalid param-tag");
                    continue;
                } else if (Support.scanTag(in, "servlet", true) == Support.TAG_WITH_VALUES) {
                    w3r.queryParameters = new HashMap<String,String[]>();
                    initParameters = new Hashtable<String,String>();
                    Map<String,String> values = new HashMap<String,String>();
                    Support.scanValues(in, values);
                    name = values.get("name");
                    code = values.get("code");
                    codebase = values.get("codebase");
                    continue;
                } else if (Support.scanTag(in, "!--#include", true) == Support.TAG_WITH_VALUES) {
                    writer.flush();
                    Map<String,String> values = new HashMap<String,String>();
                    Support.scanValues(in, values);
                    String file = values.get("file");
                    if (file != null) {
                        ServletContext servletContext = getServletContext();
                        String uri = req.getRequestURI();
                        RequestDispatcher requestDispatcher = servletContext.getRequestDispatcher(uri.substring(0, uri.lastIndexOf('/') + 1) + file);
                        requestDispatcher.include(req, res);
                    }
                }
            in.reset();
            writer.write('<');
        }
    }

}
