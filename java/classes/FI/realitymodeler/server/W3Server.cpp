
/* Native code for W3Server class */

#ifdef _WIN32
#include <windows.h>
#else
#include <grp.h>
#include <pwd.h>
#include <unistd.h>
#ifdef __linux__
#include <crypt.h>
#include <wait.h>
#endif
#endif
#include <ctype.h>
#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "FI_realitymodeler_server_W3Server.h"

static jclass nullPointerException, runtimeException;

#ifdef _WIN32
HANDLE event;

extern "C" void throwNew(JNIEnv *env, jclass throwable, DWORD n)
{
    LPSTR msg = NULL;
    if (FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, NULL, n,
                      MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&msg, 0, NULL)) {
        env->ThrowNew(throwable, msg);
        LocalFree(msg);
        return;
    }
    char str[32];
    _itoa(n, str, 10);
    LPSTR err = "error ";
    size_t l = strlen(err);
    if (!(msg = new char[l + strlen(str) + 1])) {
        env->ThrowNew(throwable, "error");
        return;
    }
    strcpy(msg, err);
    strcpy(msg + l, str);
    env->ThrowNew(throwable, msg);
    delete msg;
}
#endif

#ifdef _WIN32
extern "C" JNIEXPORT BOOL WINAPI dllMain(HANDLE hDll, DWORD reason, LPVOID reserved)
{
    return TRUE;
}
#endif

static jclass getClass(JNIEnv *env, char *name)
{
    jclass clazz;
    if ((clazz = env->FindClass(name))) clazz = (jclass)env->NewGlobalRef(clazz);
    return clazz;
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_server_W3Server_initialize(JNIEnv *env, jclass clazz)
{
    if (!(nullPointerException = getClass(env, (char *)"java/lang/NullPointerException")) ||
        !(runtimeException = getClass(env, (char *)"java/lang/RuntimeException"))) return;
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_server_W3Server_waitStop(JNIEnv *env, jclass clazz)
{
#ifdef _WIN32
    if (!event && !(event = OpenEvent(EVENT_ALL_ACCESS, FALSE, "W3Service"))) {
        throwNew(env, runtimeException, GetLastError());
        return;
    }
    if (!event || !PulseEvent(event) ||
        WaitForSingleObjectEx(event, 0, FALSE) == WAIT_FAILED ||
        WaitForSingleObjectEx(event, INFINITE, FALSE) == WAIT_FAILED) throwNew(env, runtimeException, GetLastError());
#endif
}

extern "C" JNIEXPORT jboolean JNICALL Java_FI_realitymodeler_server_W3Server_isRoot(JNIEnv *env, jclass clazz)
{
#ifdef _WIN32
    return FALSE;
#else
    return !getuid() || !geteuid();
#endif
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_server_W3Server_setUser(JNIEnv *env, jclass clazz, jstring user)
{
#ifdef _WIN32
#else
    if (!user) {
        env->ThrowNew(nullPointerException, "");
        return;
    }
    const char *s = env->GetStringUTFChars(user, NULL);
    if (!s) return;
    if (isdigit(s[0])) {
        if (setuid(atoi(s)) == -1) env->ThrowNew(runtimeException, strerror(errno));
    }
    else {
        struct passwd *ent = getpwnam(s);
        if (!ent || setuid(ent->pw_uid) == -1) env->ThrowNew(runtimeException, strerror(errno));
    }
    env->ReleaseStringUTFChars(user, s);
#endif
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_server_W3Server_setGroup(JNIEnv *env, jclass clazz, jstring group)
{
#ifdef _WIN32
#else
    if (!group) {
        env->ThrowNew(nullPointerException, "");
        return;
    }
    const char *s = env->GetStringUTFChars(group, NULL);
    if (!s) return;
    if (isdigit(s[0])) {
        if (setgid(atoi(s)) == -1) env->ThrowNew(runtimeException, strerror(errno));
    }
    else {
        struct group *ent = getgrnam(s);
        if (!ent || setgid(ent->gr_gid) == -1) env->ThrowNew(runtimeException, strerror(errno));
    }
    env->ReleaseStringUTFChars(group, s);
#endif
}

#ifdef _WIN32
jstring makeAccountName(JNIEnv *env, jstring username, jstring domain)
{
    /*
      jstring accountName = NULL;
      LPWSTR servername = NULL;
      USER_INFO_0 *userInfo0 = NULL;
      const jchar *usernameStr = NULL, *domainStr = NULL;
      if ((usernameStr = env->GetStringChars(username, NULL)) &&
      (domainStr = env->GetStringChars(domain, NULL)))
      if (NetGetDCName(NULL, domainStr, (LPBYTE *)&servername) == NERR_Success &&
      NetUserGetInfo(servername, usernameStr, 0, (LPBYTE *)&userInfo0) == NERR_Success)
      accountName = env->NewString((const jchar *)userInfo0->usri0_name, wcslen(userInfo0->usri0_name));
      else throwNew(env, runtimeException, GetLastError());
      if (userInfo0) NetApiBufferFree(userInfo0);
      if (servername) NetApiBufferFree(servername);
      if (usernameStr) env->ReleaseStringChars(username, usernameStr);
      if (domainStr) env->ReleaseStringChars(domain, domainStr);
      return accountName;
    */
    return env->NewStringUTF("");
}
#endif

extern "C" JNIEXPORT jstring JNICALL Java_FI_realitymodeler_server_W3Server_getAccountName(JNIEnv *env, jclass clazz, jstring username, jstring domain)
{
    jstring accountName = NULL;
#ifdef _WIN32
    if (!username || !domain) {
        env->ThrowNew(nullPointerException, "");
        return NULL;
    }
    accountName = makeAccountName(env, username, domain);
#else
    if (!username) {
        env->ThrowNew(nullPointerException, "");
        return NULL;
    }
    char *usernameStr = NULL;
    if ((usernameStr = (char *)env->GetStringUTFChars(username, NULL))) {
        char buf[BUFSIZ];
        struct passwd *ent = getpwnam(usernameStr);
        if (ent) accountName = env->NewStringUTF(ent->pw_gecos);
    }
    if (usernameStr) env->ReleaseStringUTFChars(username, usernameStr);
#endif
    return accountName;
}

extern "C" JNIEXPORT jstring JNICALL Java_FI_realitymodeler_server_W3Server_checkPassword(JNIEnv *env, jclass clazz, jstring username, jstring domain, jstring password)
{
    jstring accountName = NULL;
#ifdef _WIN32
    if (!username || !domain || !password) {
        env->ThrowNew(nullPointerException, "");
        return NULL;
    }
    HANDLE token = NULL;
    LPTSTR usernameStr = NULL, domainStr = NULL, passwordStr = NULL;
    if ((usernameStr = (LPTSTR)env->GetStringUTFChars(username, NULL)) &&
        (domainStr = (LPTSTR)env->GetStringUTFChars(domain, NULL)) &&
        (passwordStr = (LPTSTR)env->GetStringUTFChars(password, NULL)))
        if (!LogonUser(usernameStr, domainStr, passwordStr, LOGON32_LOGON_BATCH, LOGON32_PROVIDER_DEFAULT, &token)) {
            DWORD n = GetLastError();
            if (n != ERROR_LOGON_FAILURE) throwNew(env, runtimeException, n);
        }
        else accountName = makeAccountName(env, username, domain);
    if (token) CloseHandle(token);
    if (usernameStr) env->ReleaseStringUTFChars(username, usernameStr);
    if (domainStr) env->ReleaseStringUTFChars(domain, domainStr);
    if (passwordStr) env->ReleaseStringUTFChars(password, passwordStr);
#else
    if (!username || !password) {
        env->ThrowNew(nullPointerException, "");
        return NULL;
    }
    /*
      char *usernameStr = NULL, *passwordStr = NULL;
      if ((usernameStr = (char *)env->GetStringUTFChars(username, NULL)) &&
      (passwordStr = (char *)env->GetStringUTFChars(password, NULL))) {
      char buf[BUFSIZ];
      struct passwd *ent = getpwnam(usernameStr);
      if (ent) {
      char *computed = crypt(passwordStr, ent->pw_passwd);
      if (!computed) env->ThrowNew(runtimeException, strerror(errno));
      else if (!strcmp(computed, ent->pw_passwd)) accountName = env->NewStringUTF(ent->pw_gecos);
      }
      }
      if (usernameStr) env->ReleaseStringUTFChars(username, usernameStr);
      if (passwordStr) env->ReleaseStringUTFChars(password, passwordStr);
    */
#endif
    return accountName;
}
