
package FI.realitymodeler.server;

import java.lang.reflect.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

class ValveFilter implements Filter {
    FilterConfig filterConfig = null;
    Object valveObject = null;
    Class<?> catalinaRequestClass = null;
    Class<?> catalinaResponseClass = null;
    Class<?> coyoteRequestClass = null;
    Class<?> coyoteResponseClass = null;
    Class<?> mimeHeadersClass = null;
    Class<?> messageBytesClass = null;
    Class<?> parametersClass = null;
    Method setNextMethod = null;
    Method getMimeHeadersMethod = null;
    Method queryStringMethod = null;
    Method getParametersMethod = null;
    Method decodedURIMethod = null;
    Method setValueMethod = null;
    Method setStringMethod = null;
    Method addParameterValuesMethod = null;
    Method requestSetConnectorMethod = null;
    Method getRequestPathMBMethod = null;
    Method setCoyoteRequestMethod = null;
    Method setCoyoteResponseMethod = null;
    Method getHeaderNamesMethod = null;
    Method getHeaderValuesMethod = null;
    Method getMessageMethod = null;
    Method getStatusMethod = null;
    Method responseSetConnectorMethod = null;
    Method setRequestMethod = null;

    public void init(FilterConfig filterConfig)
        throws ServletException {
        try {
            this.filterConfig = filterConfig;
            String className = filterConfig.getInitParameter("className");
            W3Context w3context = (W3Context)filterConfig.getServletContext();
            Class<?> valveClass = w3context.servletClassLoader.loadClass(className);
            valveObject = valveClass.newInstance();
            Class<?> valveInterface = w3context.servletClassLoader.loadClass("org.apache.catalina.Valve");
            Method setContainerMethod = valveClass.getMethod("setContainer", new Class<?>[] {w3context.containerClass}),
                setNext = valveClass.getMethod("setNext", new Class<?>[] {valveInterface});
            setContainerMethod.invoke(valveObject, new Object[] {w3context.contextObject});
            Object remoteHostValveObject = w3context.servletClassLoader.loadClass("org.apache.catalina.valves.RemoteHostValve").newInstance();
            setNext.invoke(valveObject, new Object[] {remoteHostValveObject});
            if (className.equals("org.apache.catalina.authenticator.FormAuthenticator")) {
                String characterEncoding = filterConfig.getInitParameter("characterEncoding");
                Method method = valveClass.getMethod("setCharacterEncoding", new Class<?>[] {String.class});
                method.invoke(valveObject, new Object[] {characterEncoding});
            }
            catalinaRequestClass = w3context.servletClassLoader.loadClass("org.apache.catalina.connector.Request");
            catalinaResponseClass = w3context.servletClassLoader.loadClass("org.apache.catalina.connector.Response");
            coyoteRequestClass = w3context.servletClassLoader.loadClass("org.apache.coyote.Request");
            coyoteResponseClass = w3context.servletClassLoader.loadClass("org.apache.coyote.Response");
            mimeHeadersClass = w3context.servletClassLoader.loadClass("org.apache.tomcat.util.http.MimeHeaders");
            messageBytesClass = w3context.servletClassLoader.loadClass("org.apache.tomcat.util.buf.MessageBytes");
            parametersClass = w3context.servletClassLoader.loadClass("org.apache.tomcat.util.http.Parameters");
            getMimeHeadersMethod = coyoteRequestClass.getMethod("getMimeHeaders", (Class<?>[])null);
            queryStringMethod = coyoteRequestClass.getMethod("queryString", (Class<?>[])null);
            getParametersMethod = coyoteRequestClass.getMethod("getParameters", (Class<?>[])null);
            decodedURIMethod = coyoteRequestClass.getMethod("decodedURI", (Class<?>[])null);
            setValueMethod = mimeHeadersClass.getMethod("setValue", new Class<?>[] {String.class});
            setStringMethod = messageBytesClass.getMethod("setString", new Class<?>[] {String.class});
            addParameterValuesMethod = parametersClass.getMethod("addParameterValues", new Class<?>[] {String.class, Class.forName("[Ljava.lang.String;")});
            requestSetConnectorMethod = catalinaRequestClass.getMethod("setConnector", new Class<?>[] {w3context.connectorClass});
            getRequestPathMBMethod = catalinaRequestClass.getMethod("getRequestPathMB", (Class<?>[])null);
            setCoyoteRequestMethod = catalinaRequestClass.getMethod("setCoyoteRequest", new Class<?>[] {coyoteRequestClass});
            setCoyoteResponseMethod = catalinaResponseClass.getMethod("setCoyoteResponse", new Class<?>[] {coyoteResponseClass});
            getHeaderNamesMethod = catalinaResponseClass.getMethod("getHeaderNames", (Class<?>[])null);
            getHeaderValuesMethod = catalinaResponseClass.getMethod("getHeaderValues", new Class<?>[] {String.class});
            getMessageMethod = catalinaResponseClass.getMethod("getMessage", (Class<?>[])null);
            getStatusMethod = catalinaResponseClass.getMethod("getStatus", (Class<?>[])null);
            responseSetConnectorMethod = catalinaResponseClass.getMethod("setConnector", new Class<?>[] {w3context.connectorClass});
            setRequestMethod = catalinaResponseClass.getMethod("setRequest", new Class<?>[] {catalinaRequestClass});
            Class<?> securityConstraintClass = w3context.servletClassLoader.loadClass("org.apache.catalina.deploy.SecurityConstraint"),
                securityCollectionClass = w3context.servletClassLoader.loadClass("org.apache.catalina.deploy.SecurityCollection");
            Method addPatternMethod = securityCollectionClass.getMethod("addPattern", new Class<?>[] {String.class}),
                setAuthConstraintMethod = securityConstraintClass.getMethod("setAuthConstraint", new Class<?>[] {Boolean.TYPE}),
                addCollectionMethod = securityConstraintClass.getMethod("addCollection", new Class<?>[] {securityCollectionClass}),
                addConstraintMethod = w3context.contextClass.getMethod("addConstraint", new Class<?>[] {securityConstraintClass});
            Iterator iter = w3context.securityConstraints.iterator();
            while (iter.hasNext()) {
                SecurityConstraint securityConstraint = (SecurityConstraint)iter.next();
                Object securityConstraintObject = securityConstraintClass.newInstance();
                setAuthConstraintMethod.invoke(securityConstraintObject, new Object[] {Boolean.TRUE});
                Iterator webResourceCollectionIter = securityConstraint.webResourceCollections.iterator();
                while (webResourceCollectionIter.hasNext()) {
                    WebResourceCollection webResourceCollection =
                        (WebResourceCollection)webResourceCollectionIter.next();
                    Object securityCollectionObject = securityCollectionClass.newInstance();
                    Iterator urlPatternIter =
                        webResourceCollection.urlPatterns.iterator();
                    while (urlPatternIter.hasNext()) {
                        String urlPattern = (String)urlPatternIter.next();
                        addPatternMethod.invoke(securityCollectionObject, new Object[] {urlPattern});
                    }
                    addCollectionMethod.invoke(securityConstraintObject, new Object[] {securityCollectionObject});
                }
                addConstraintMethod.invoke(w3context.contextObject, new Object[] {securityConstraintObject});
            }
        } catch (Exception ex) {
            throw new ServletException(ex);
        }
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws ServletException {
        try {
            W3Context w3context = (W3Context)filterConfig.getServletContext();
            Object coyoteRequestObject = coyoteRequestClass.newInstance();
            Object mimeHeadersObject = getMimeHeadersMethod.invoke(coyoteRequestObject, (Object[])null);
            HttpServletRequest httpRequest = (HttpServletRequest)request;
            Enumeration headerNameItems = httpRequest.getHeaderNames();
            while (headerNameItems.hasMoreElements()) {
                String headerName = (String)headerNameItems.nextElement(),
                    value = httpRequest.getHeader(headerName);
                Object messageBytesObject = setValueMethod.invoke(mimeHeadersObject, new Object[] {headerName});
                setStringMethod.invoke(messageBytesObject, new Object[] {value});
            }
            Object messageBytesObject = queryStringMethod.invoke(coyoteRequestObject, (Object[])null);
            setStringMethod.invoke(messageBytesObject, new Object[] {httpRequest.getQueryString()});
            Object parametersObject = getParametersMethod.invoke(coyoteRequestObject, (Object[])null);
            Enumeration parameterNameItems = request.getParameterNames();
            while (parameterNameItems.hasMoreElements()) {
                String parameterName = (String)parameterNameItems.nextElement(),
                    values[] = request.getParameterValues(parameterName);
                addParameterValuesMethod.invoke(parametersObject, new Object[] {parameterName, values});
            }
            messageBytesObject = decodedURIMethod.invoke(coyoteRequestObject, (Object[])null);
            setStringMethod.invoke(messageBytesObject, new Object[] {httpRequest.getRequestURI()});
            Object catalinaRequestObject = catalinaRequestClass.newInstance();
            setCoyoteRequestMethod.invoke(catalinaRequestObject, new Object[] {coyoteRequestObject});
            requestSetConnectorMethod.invoke(catalinaRequestObject, new Object[] {w3context.connectorObject});
            messageBytesObject = getRequestPathMBMethod.invoke(catalinaRequestObject, (Object[])null);
            String pathInfo = httpRequest.getPathInfo();
            setStringMethod.invoke(messageBytesObject, new Object[] {httpRequest.getServletPath() + (pathInfo != null ? pathInfo : "")});
            Object catalinaResponseObject = catalinaResponseClass.newInstance(),
                coyoteResponseObject = coyoteResponseClass.newInstance();
            responseSetConnectorMethod.invoke(catalinaResponseObject, new Object[] {w3context.connectorObject});
            setRequestMethod.invoke(catalinaResponseObject, new Object[] {catalinaRequestObject});
            setCoyoteResponseMethod.invoke(catalinaResponseObject, new Object[] {coyoteResponseObject});
            w3context.invokeMethod.invoke(valveObject, new Object[] {catalinaRequestObject, catalinaResponseObject});
            HttpServletResponse httpResponse = (HttpServletResponse)response;
            String headerNames[] = (String[])getHeaderNamesMethod.invoke(catalinaResponseObject, (Object[])null);
            for (int headerNameIndex = 0; headerNameIndex < headerNames.length; headerNameIndex++) {
                String headerName = headerNames[headerNameIndex],
                    values[] = (String[])getHeaderValuesMethod.invoke(catalinaResponseObject, new Object[] {headerName});
                for (int valueIndex = 0; valueIndex < values.length; valueIndex++)
                    httpResponse.addHeader(headerName, values[valueIndex]);
            }
            String message = (String)getMessageMethod.invoke(catalinaResponseObject, (Object[])null);
            Integer statusObject = (Integer)getStatusMethod.invoke(catalinaResponseObject, (Object[])null);
            System.out.println("VALVE status="+statusObject+",message="+message);
            if (statusObject != null)
                httpResponse.setStatus(statusObject.intValue(), message);
            chain.doFilter(request, response);
        } catch (Exception ex) {
            throw new ServletException(ex);
        }
    }

    public void destroy() {
    }

}
