
package FI.realitymodeler.server;

import FI.realitymodeler.common.*;

public class VirtualPathTarget implements RegexpTarget {
    String pattern;
    String target;

    VirtualPathTarget(String pattern, String target) {
        this.pattern = pattern;
        this.target = target;
    }

    public Object found(String remainder) {
        return Support.replace(target, "*", remainder);
    }

}
