
package FI.realitymodeler.server;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class MobileServlet extends HttpServlet {
    static final long serialVersionUID = 0L;

    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        W3Request w3r = (W3Request)req;
        if (!w3r.domain.name.equals("native")) {
            UserRecord user = w3r.w3server.userDatabase.getRecord(req.getRemoteAddr());
            if (user != null && user.domains.contains(w3r.domain.name)) {
                w3r.accountName = user.name;
                return;
            }
        } else if ((w3r.accountName = W3Server.getAccountName(req.getRemoteAddr(), w3r.w3server.nativeDomain)) != null) return;
        w3r.challenge = "Mobile domain=\"" + w3r.domain.name + "\"";
        throw new ServletException("Mobile authorization failed");
    }

}
