
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class EchoServlet extends HttpServlet {
    static final long serialVersionUID = 0L;

    public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
        String charset = res.getCharacterEncoding(),
            charsetName = charset != null ? Support.getCharacterEncoding(charset) : null;
        res.setStatus(res.SC_OK);
        res.setContentType(Support.plainType);
        OutputStream out = res.getOutputStream();
        String query = req.getQueryString();
        if (query != null) Support.writeBytes(out, query, charsetName);
    }

    public void doHead(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        doGet(req, res);
    }

    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        res.setContentType(Support.plainType);
        OutputStream out = res.getOutputStream();
        InputStream in = req.getInputStream();
        byte b[];
        if (req instanceof W3Request) b = ((W3Request)req).buffer;
        else b = new byte[Support.bufferLength];
        for (int n; (n = in.read(b)) > 0;) out.write(b, 0, n);
    }

}
