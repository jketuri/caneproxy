
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;

public class Store {
    public HeaderList headerList;
    public W3URLConnection uc = null;
    public Response response = null;
    /** Original request method. */
    public String clientMethod;
    /** Message header originally received from client. */
    public HeaderList clientHeaderList;
    public String request;
    public String method;
    public String uri;
    public String requestURI;
    public String protocol = W3URLConnection.protocol_1_0;
    public boolean keep = false;
    public boolean mime = false;
    public boolean parsePostData = false;
    public boolean receiving = false;
    public boolean simple = false;
    public int requestNumber;

    public String toString() {
        return getClass().getName() + "{headerList=" + headerList +
            ",request=" + request + ",method=" + method +
            ",requestURI=" + requestURI + ",protocol=" + protocol +
            ",keep=" + keep + ",mime=" + mime + ",receiving=" + receiving + "}";
    }

}
