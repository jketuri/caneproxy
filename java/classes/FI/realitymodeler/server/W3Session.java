
package FI.realitymodeler.server;

import java.security.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Provides a way to identify a user across more than one page request or visit to a
    Web site and to store information about that user.
    The servlet container uses this interface to create a session between an HTTP client
    and an HTTP server. The session persists for a specified time period, across
    more than one connection or page request from the user. A session usually corresponds
    to one user, who may visit a site many times. The server can maintain a
    session in many ways such as using cookies or rewriting URLs.
    This interface allows servlets to
    -View and manipulate information about a session, such as the session identifier,
    creation time, and last accessed time
    -Bind objects to sessions, allowing user information to persist across multiple
    user connections
    When an application stores an object in or removes an object from a session, the
    session checks whether the object implements HttpSessionBindingListener.
    If it does, the servlet notifies the object that it has been bound to or unbound from
    the session. Notifications are sent after the binding methods complete. For session
    that are invalidated or expire, notifications are sent after the session has been
    invalidated or expired.
    When container migrates a session between VMs in a distributed container setting,
    all session atributes implementing the HttpSessionActivationListener
    interface are notified.
    A servlet should be able to handle cases in which the client does not choose to
    join a session, such as when cookies are intentionally turned off. Until the client
    joins the session, isNew returns true. If the client chooses not to join the session,
    getSession will return a different session on each request, and isNew will always
    return true.
    Session information is scoped only to the current web application
    (ServletContext), so information stored in one context will not be directly visible
    in another.

    @see javax.servlet.http.HttpSessionBindingListener
    @see javax.servlet.http.HttpSessionContext
*/
public class W3Session implements HttpSession {
    Principal principal = null;
    Map<String,Object> attributes = new HashMap<String,Object>();
    W3Context w3context = null;
    String id = null;
    String uri = null;
    boolean invalidated = false;
    boolean invalidation = false;
    boolean isNew = true;
    int maxInactiveInterval = -1;
    int nBindings = 1;
    long creationTime = System.currentTimeMillis();
    long lastAccessedTime;
    long maxInactiveIntervalInMillis = -1L;

    public W3Session(W3Context w3context, String id) {
        this.w3context = w3context;
        this.id = id;
    }

    /** Returns the object bound with the specified name in this session, or
        null if no object is bound under the name. 

        @param name a string specifying the name of the object
        @return the object with the specified name
        @throws IllegalStateException if this method is called on an invalidated session
    */
    public Object getAttribute(String name) {
        //if (invalidated) throw new IllegalStateException();
        return attributes.get(name);
    }

    /** Returns an Enumeration of String objects containing the names of all the
        objects bound to this session.

        @return an Enumeration of String objects specifying the names of all the
        objects bound to this session
        @throws IllegalStateException if this method is called on an invalidated session

    */
    public Enumeration<String> getAttributeNames() {
        if (invalidated) throw new IllegalStateException();
        return new IteratorEnumeration<String>(attributes.keySet().iterator());
    }

    /** Returns the time when this session was created, measured in milliseconds
        since midnight January 1, 1970 GMT.

        @return a long specifying when this session was created, expressed in
        milliseconds since 1/1/1970 GMT
        @throws IllegalStateException if this method is called on an invalidated session

    */
    public long getCreationTime() {
        if (invalidated) throw new IllegalStateException();
        return creationTime;
    }

    /** Returns a string containing the unique identifier assigned to this
        session. The identifier is assigned by the servlet container and is
        implementation depen-dent. 

        @return a string specifying the identifier assigned to this session

    */
    public String getId() {
        if (invalidated) throw new IllegalStateException();
        return id;
    }

    /** Returns the last time the client sent a request associated with this
        session, as the number of milliseconds since midnight January 1, 1970
        GMT, and marked by the time the container recieved the request.  Actions
        that your application takes, such as getting or setting a value
        associ-ated with the session, do not affect the access time. 

        @return a long representing the last time the client sent a request
        associated with this session, expressed in milliseconds since 1/1/1970 GMT

    */
    public long getLastAccessedTime() {
        if (invalidated) throw new IllegalStateException();
        return lastAccessedTime;
    }

    /** Returns the maximum time interval, in seconds, that the servlet container will
        keep this session open between client accesses. After this interval, the servlet
        container will invalidate the session. The maximum time interval can be set
        with the setMaxInactiveInterval method. A negative time indicates the
        session should never timeout.

        @return an integer specifying the number of seconds this session remains
        open between client requests
        @see setMaxInactiveInterval(int)

    */
    public int getMaxInactiveInterval() {
        if (invalidated) throw new IllegalStateException();
        return maxInactiveInterval;
    }


    /** Returns the ServletContext to which this session belongs.

        @return The ServletContext object for the web application
        Since: 2.3

    */
    public ServletContext getServletContext() {
        return w3context;
    }

    /** @deprecated Deprecated. As of Version 2.1, this method is deprecated and has no
        replacement. It will be removed in a future version of the Java Servlet API.

    */
    @Deprecated
        public HttpSessionContext getSessionContext() {
        return w3context;
    }

    /** @deprecated Deprecated. As of Version 2.2, this method is replaced by
        getAttribute(String) .

        @param name a string specifying the name of the object
        @return the object with the specified name
        @throws IllegalStateException if this method is called on an invalidated session

    */
    @Deprecated
        public Object getValue(String name) {
        return getAttribute(name);
    }

    /** @deprecated Deprecated. As of Version 2.2, this method is replaced by
        getAttributeNames()

        @return an array of String objects specifying the names of all the objects
        bound to this session
        @throws IllegalStateException if this method is called on an invalidated session

    */
    @Deprecated
        public String[] getValueNames() {
        if (invalidated) throw new IllegalStateException();
        Enumeration<String> attributeNames = getAttributeNames();
        List<String> list = new ArrayList<String>();
        while (attributeNames.hasMoreElements()) list.add(attributeNames.nextElement());
        return list.toArray(new String[list.size()]);
    }

    /** Invalidates this session then unbinds any objects bound to it.

        @throws IllegalStateException if this method is called on an already invalidated
        session

    */
    public void invalidate() {
        synchronized (this) {
            if (invalidation) return;
            invalidation = true;
        }
        try {
            HttpSessionEvent httpSessionEvent = new HttpSessionEvent(this);
            /*
              Iterator httpSessionActivationListenerIter = w3context.httpSessionActivationListeners.iterator();
              while (httpSessionActivationListenerIter.hasNext())
              ((HttpSessionActivationListener)httpSessionActivationListenerIter.next()).sessionWillPassivate(httpSessionEvent);
            */
            Iterator attributeNames = attributes.keySet().iterator();
            while (attributeNames.hasNext()) {
                String name = (String)attributeNames.next();
                Object value = attributes.get(name);
                attributeRemove(name, value);
            }
            attributes.clear();
            Iterator httpSessionListenerIter = w3context.httpSessionListeners.iterator();
            while (httpSessionListenerIter.hasNext())
                ((HttpSessionListener)httpSessionListenerIter.next()).sessionDestroyed(httpSessionEvent);
        } finally {
            invalidated = true;
        }
    }

    /** Returns true if the client does not yet know about the session or if the client
        chooses not to join the session. For example, if the server used only cookie-based
        sessions, and the client had disabled the use of cookies, then a session
        would be new on each request.

        @return true if the server has created a session, but the client has not yet
        joined
        @throws IllegalStateException if this method is called on an already invalidated
        session

    */
    public boolean isNew() {
        if (invalidated) throw new IllegalStateException();
        return isNew;
    }

    /** @deprecated Deprecated. As of Version 2.2, this method is replaced by
        setAttribute(String, Object)

        @param name the name to which the object is bound; cannot be null
        @param value the object to be bound; cannot be null
        @throws IllegalStateException if this method is called on an invalidated session

    */
    @Deprecated
        public void putValue(String name, Object value) {
        setAttribute(name, value);
    }

    /** Removes the object bound with the specified name from this session. If the
        session does not have an object bound with the specified name, this method
        does nothing.
        After this method executes, and if the object implements HttpSessionBindingListener, the container calls
        HttpSessionBindingListener.valueUnbound. The container then notifies any
        HttpSessionAttributeListeners in the web application.

        @param name the name of the object to remove from this session
        @throws IllegalStateException if this method is called on an invalidated session

    */
    public void removeAttribute(String name) {
        if (invalidated) throw new IllegalStateException();
        attributeRemove(name, attributes.remove(name));
    }

    /** @deprecated Deprecated. As of Version 2.2, this method is replaced by
        removeAttribute(String)

        @param name the name of the object to remove from this session
        @throws IllegalStateException if this method is called on an invalidated session

    */
    @Deprecated
        public void removeValue(String name) {
        removeAttribute(name);
    }

    /** Binds an object to this session, using the name specified. If an object of the
        same name is already bound to the session, the object is replaced.
        After this method executes, and if the new object implements HttpSessionBindingListener,
        the container calls HttpSessionBindingListener.valueBound. The container then notifies any
        HttpSessionAttributeListeners in the web application.
        If an object was already bound to this session of this name that implements
        HttpSessionBindingListener, its HttpSessionBindingListener.valueUnbound method is called.
        If the value passed in is null, this has the same effect as calling removeAttribute().

        @param name the name to which the object is bound; cannot be null
        @param value the object to be bound
        @throws IllegalStateException if this method is called on an invalidated session

    */
    public void setAttribute(String name, Object value) {
        if (invalidated) throw new IllegalStateException();
        if (value == null) {
            removeAttribute(name);
            return;
        }
        Object oldValue = attributes.get(name);
        attributes.put(name, value);
        HttpSessionBindingEvent httpSessionBindingEvent = new HttpSessionBindingEvent(this, name, value);
        if (value instanceof HttpSessionBindingListener)
            ((HttpSessionBindingListener)value).valueBound(httpSessionBindingEvent);
        Iterator httpSessionAttributeListenerIter = w3context.httpSessionAttributeListeners.iterator();
        if (oldValue != null)
            while (httpSessionAttributeListenerIter.hasNext())
                ((HttpSessionAttributeListener)httpSessionAttributeListenerIter.next()).attributeReplaced(httpSessionBindingEvent);
        else while (httpSessionAttributeListenerIter.hasNext())
                 ((HttpSessionAttributeListener)httpSessionAttributeListenerIter.next()).attributeAdded(httpSessionBindingEvent);
        if (oldValue != null &&
            oldValue instanceof HttpSessionBindingListener) {
            httpSessionBindingEvent = new HttpSessionBindingEvent(this, name, oldValue);
            ((HttpSessionBindingListener)value).valueUnbound(httpSessionBindingEvent);
        }
    }

    /** Specifies the time, in seconds, between client requests before the servlet container
        will invalidate this session. A negative time indicates the session
        should never timeout.

        @param interval An integer specifying the number of seconds

    */
    public void setMaxInactiveInterval(int interval) {
        if (invalidated) throw new IllegalStateException();
        maxInactiveInterval = interval;
        if (interval >= 0) maxInactiveIntervalInMillis = (long)interval * 1000L;
        else maxInactiveIntervalInMillis = -1L;
    }

    public String toString() {
        return getClass().getName() + "{id=" + id + ", invalidated=" + invalidated + ", isNew=" + isNew + "}";
    }

    private void attributeRemove(String name, Object value) {
        if (value == null) return;
        HttpSessionBindingEvent httpSessionBindingEvent = new HttpSessionBindingEvent(this, name, value);
        if (value instanceof HttpSessionBindingListener)
            ((HttpSessionBindingListener)value).valueUnbound(httpSessionBindingEvent);
        Iterator httpSessionAttributeListenerIter = w3context.httpSessionAttributeListeners.iterator();
        while (httpSessionAttributeListenerIter.hasNext())
            ((HttpSessionAttributeListener)httpSessionAttributeListenerIter.next()).attributeRemoved(httpSessionBindingEvent);
    }

}
