
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Short message servlet. Available initialization parameters are:<br>
    msisdn=virtual phone number acting as origin of sent short messages (if this is not specified
    it is taken from system property sms.fromaddr)<br>
    datagramType=datagram protocol which is used in transport (must be 'cimd', 'nbs', 'mobile' or 'inet', which is default)<br>
    maxPlainMessages=maximun number of plain messages sent in succession (defaults to 10).<br>
    maxNbsMessages=maximum number of NBS-messages which can be sent in succession (currently 3).
*/
public class SMServlet extends HttpServlet {
    static final long serialVersionUID = 0L;
    private ResourceBundle messages;
    private String msisdn;
    private String datagramType;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        messages = W3URLConnection.getMessages("FI.realitymodeler.server.resources.Messages");
        if ((msisdn = getInitParameter("msisdn")) == null &&
            (msisdn = System.getProperty("sms.fromaddr")) == null) throw new ServletException("msisdn to bind not specified");
        if ((datagramType = config.getInitParameter("datagramType")) == null) datagramType = "inet";
        else if (!(datagramType = datagramType.toLowerCase()).equals("cimd") &&
                 !datagramType.equals("nbs") && !datagramType.equals("mobile") &&
                 !datagramType.equals("inet")) throw new ServletException("Unknown datagram type " + datagramType);
        String s;
        if ((s = config.getInitParameter("maxPlainMessages")) != null) CellularSocket.setMaxPlainMsgs(Integer.parseInt(s));
        if ((s = config.getInitParameter("maxNbsMessages")) != null) CellularSocket.setMaxNbsMsgs(Integer.parseInt(s));
    }

    public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
        String charset = res.getCharacterEncoding(),
            charsetName = charset != null ? Support.getCharacterEncoding(charset) : null;
        String number;
        if ((number = Support.getParameterValue(req.getParameterValues("number"))) == null) number = "";
        String s;
        boolean mobile = (s = req.getHeader("user-agent")) != null && s.trim().equalsIgnoreCase("mobile") ||
            req.getParameterValues("mobile") != null;
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        Support.writeBytes(bout, "<html><head><title>" + Support.htmlString(messages.getString("sendShortMessage")) + "</title></head><body>\n", charsetName);
        Support.writeBytes(bout, "<h1>" + Support.htmlString(messages.getString("sendShortMessage")) + "</h1>\n", charsetName);
        Support.writeBytes(bout, "<form action=\"" + req.getServletPath() + "\" enctype=\"multipart/form-data\" method=post>\n", charsetName);
        Support.writeBytes(bout, "<input name=nbs type=checkbox " + (req.getParameterValues("nbs") != null ? "checked" : "") + ">" +
                           Support.htmlString(messages.getString("includeNBSHeaders")) + "<br>\n", charsetName);
        if (!mobile) {
            Support.writeBytes(bout, "<input type=reset value=\"" + Support.htmlString(messages.getString("defaults")) + "\">\n", charsetName);
            Support.writeBytes(bout, "<input name=new type=submit value=\"" + Support.htmlString(messages.getString("getNewForm")) + "\">\n", charsetName);
        }
        Support.writeBytes(bout, "<input type=submit value=\"" + Support.htmlString(messages.getString("sendShortMessage")) + "\">\n", charsetName);
        Support.writeBytes(bout, "<table>\n", charsetName);
        Support.writeBytes(bout, "<tr><th>" + Support.htmlString(messages.getString("phoneNumber(s)")) + "</th><td><input name=number type=text value=\"" + number + "\" size=55 maxlength=275></td></tr>\n", charsetName);
        Support.writeBytes(bout, "<tr><th>" + Support.htmlString(messages.getString("textFile")) + "</th><td><input name=file type=file accept=\"*/*\" size=55 maxlength=275></td></tr>\n", charsetName);
        Support.writeBytes(bout, "</table>\n", charsetName);
        Support.writeBytes(bout, "<textarea name=text rows=20 cols=66 wrap=hard></textarea><br>\n", charsetName);
        Support.writeBytes(bout, "</form></body></html>", charsetName);
        res.setContentLength(bout.size());
        res.setContentType(Support.htmlType);
        bout.writeTo(res.getOutputStream());
    }

    public void doHead(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        doGet(req, res);
    }

    W3SmsURLConnection getSMConnection(HttpServletResponse res, Map<String,String> values) throws IOException, MalformedURLException {
        String number = values.get("number");
        if (number == null || (number = number.trim()).equals("")) {
            res.sendError(HttpURLConnection.HTTP_OK, messages.getString("phoneNumberMissing"));
            return null;
        }
        return new W3SmsURLConnection(new URL("sms:" + number + "/ascii/" + datagramType + "/" +
                                               (values.containsKey("nbs") ? "nbs" : "plain") + "/" + msisdn));
    }

    public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        Map<String,String> values = new HashMap<String,String>(), params = new HashMap<String,String>();
        String ct = Support.getParameters(req.getContentType(), params).toLowerCase();
        InputStream in = req.getInputStream();
        W3SmsURLConnection uc = null;
        try {
            if (ct.equals("multipart/form-data")) {
                String boundary = params.get("boundary");
                if (boundary != null) boundary = "--" + boundary;
                else if ((boundary = req.getHeader("boundary")) == null) throw new ServletException("Missing boundary");
                InputStream in1 = new ConvertInputStream(in, boundary, "\r", true, true);
                while (in1.read() != -1);
                boundary = "\r\n" + boundary;
                OutputStream out = null;
                StringBuffer value = new StringBuffer();
                byte b[] = new byte[Support.bufferLength];
                for (;;) {
                    int c;
                    if ((c = in.read()) == '-' && (c = in.read()) == '-') break;
                    while (c != -1 && c != '\n') c = in.read();
                    if (c == -1) break;
                    in1 = new ConvertInputStream(in, boundary, false, true);
                    HeaderList headerList;
                    try {
                        headerList = new HeaderList(in1);
                    } catch (ParseException ex) {
                        throw new ServletException(ex.toString());
                    }
                    String cd = headerList.getHeaderValue("content-disposition");
                    if (cd == null) throw new ServletException("Missing content-disposition");
                    Support.getParameters(cd, params = new HashMap<String,String>(), true);
                    String name = params.get("name");
                    if (name == null) throw new ServletException("Missing name in content-disposition");
                    if (name.equals("file") || name.equals("text")) {
                        String filename = null;
                        if (out == null) {
                            if (name.equals("file") && ((filename = params.get("filename")) == null || (filename = filename.trim()).equals("")) || values.containsKey("new")) {
                                while (in1.read(b) > 0);
                                continue;
                            }
                            if ((uc = getSMConnection(res, values)) == null) return;
                            uc.setServletContext(getServletConfig().getServletContext());
                            out = uc.getOutputStream();
                        }
                        in1 = Support.getTextInputStream(in1);
                        for (int n; (n = in1.read(b)) > 0;) out.write(b, 0, n);
                        continue;
                    } while ((c = in1.read()) != -1) value.append((char)c);
                    values.put(name, value.toString());
                    value.setLength(0);
                }
            } else {
                FI.realitymodeler.common.URLDecoder urld = new FI.realitymodeler.common.URLDecoder(true);
                for (;;) {
                    String name = new String(urld.decodeStream(in), "UTF-8"), value;
                    if (urld.c != '=') break;
                    if (name.equals("text")) {
                        if (values.containsKey("new")) break;
                        if ((uc = getSMConnection(res, values)) == null) return;
                        uc.setServletContext(getServletConfig().getServletContext());
                        OutputStream out = uc.getOutputStream();
                        urld.decodeStream(in, out);
                    }
                    value = new String(urld.decodeStream(in), "UTF-8");
                    if (urld.c != '&') break;
                    values.put(name, value);
                }
            }
            if (uc != null) {
                res.sendError(HttpURLConnection.HTTP_OK, messages.getString("shortMessageSent"));
                return;
            }
        } finally {
            if (uc != null) uc.disconnect();
        }
        if (values.containsKey("new")) {
            Iterator valueIter = values.entrySet().iterator();
            StringBuffer sb = new StringBuffer();
            while (valueIter.hasNext()) {
                Map.Entry entry = (Map.Entry)valueIter.next();
                String name = (String)entry.getKey(), value = (String)entry.getValue();
                if (name.equals("new")) continue;
                if (sb.length() > 0) sb.append('&');
                sb.append(name + "=" + URLEncoder.encode(value, "8859_1"));
            }
            res.sendRedirect(req.getRequestURI() + "?" + sb.toString());
            return;
        }
        res.sendError(HttpURLConnection.HTTP_OK, messages.getString("invalidFormContents"));
    }

}
