
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

class CommandAdapter {

    void execute(W3Manager w3m)
        throws Exception {
    }

}

public class W3Manager extends HttpServlet {
    static final long serialVersionUID = 0L;
    static final String db_not_found = "User database not found";
    static HashMap<String,CommandAdapter> commands = new HashMap<String,CommandAdapter>();
    static {
        commands.put("save user", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    if (w3m.w3.userDatabase == null) throw new Exception(db_not_found);
                    w3m.w3.userDatabase.saveUser((Object[])w3m.parameter);
                    w3m.respond("User saved");
                }});
        commands.put("search user", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    if (w3m.w3.userDatabase == null) throw new Exception(db_not_found);
                    Object object = w3m.w3.userDatabase.searchUser((String)w3m.parameter);
                    if (object == null) throw new Exception("No user found");
                    w3m.respond(object);
                }});
        commands.put("first user", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    if (w3m.w3.userDatabase == null) throw new Exception(db_not_found);
                    Object object = w3m.w3.userDatabase.firstUser();
                    if (object == null) throw new Exception("No first user");
                    w3m.respond(object);
                }});
        commands.put("next user", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    if (w3m.w3.userDatabase == null) throw new Exception(db_not_found);
                    Object object = w3m.w3.userDatabase.nextUser();
                    if (object == null) throw new Exception("No next user");
                    w3m.respond(object);
                }});
        commands.put("remove user", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    if (w3m.w3.userDatabase == null) throw new Exception(db_not_found);
                    w3m.w3.userDatabase.removeUser((String)w3m.parameter);
                    w3m.respond("User removed");
                }});
        commands.put("clean ftp cache", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    if (w3m.w3.w3context.getAttribute("FI.realitymodeler.server.W3Server/cache/ftp") == Boolean.FALSE) throw new Exception("No ftp cache found");
                    W3FtpURLConnection.cleanCache(w3m.getServletConfig().getServletContext());
                    w3m.respond("Started to clean ftp cache");
                }});
        commands.put("clean gopher cache", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    if (w3m.w3.w3context.getAttribute("FI.realitymodeler.server.W3Server/cache/gopher") == Boolean.FALSE) throw new Exception("No gopher cache found");
                    W3GopherURLConnection.cleanCache(w3m.getServletConfig().getServletContext());
                    w3m.respond("Started to clean gopher cache");
                }});
        commands.put("clean http cache", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    if (w3m.w3.w3context.getAttribute("FI.realitymodeler.server.W3Server/cache/http") == Boolean.FALSE) throw new Exception("No http cache found");
                    W3HttpURLConnection.cleanCache(w3m.getServletConfig().getServletContext());
                    w3m.respond("Started to clean http cache");
                }});
        commands.put("empty server log", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    if (w3m.w3.verbose || w3m.w3.logLevel == 0) throw new Exception("No server log found");
                    for (int i = 0; i < w3m.w3.logLevel; i++)
                        synchronized (w3m.w3.logs[i]) {
                            w3m.w3.logs[i].stream.close();
                            w3m.w3.logs[i].stream = new BufferedOutputStream(new FileOutputStream(w3m.w3.logs[i].file));
                        }
                    if (w3m.w3.logLevel > 2) w3m.w3.startLog();
                    w3m.respond("Server log emptied");
                }});
        commands.put("read data file", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    String s = (String)w3m.parameter;
                    s = s.substring(Math.max(s.lastIndexOf('/'), s.lastIndexOf(File.separatorChar)) + 1);
                    File f = new File(w3m.w3.dataDirectory, s);
                    if (!f.exists()) throw new Exception("No file " + w3m.parameter + " found");
                    w3m.oout.writeBoolean(true);
                    w3m.oout.flush();
                    FileInputStream fin = new FileInputStream(f);
                    try {
                        int n;
                        while ((n = fin.read(w3m.buffer)) > 0) w3m.out.write(w3m.buffer, 0, n);
                    } finally {
                        try {
                            w3m.out.flush();
                        } finally {
                            fin.close();
                        }
                    }
                }});
        commands.put("write data file", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    String name = (String)w3m.parameter;
                    name = name.substring(Math.max(name.lastIndexOf('/'), name.lastIndexOf(File.separatorChar)) + 1);
                    String path = w3m.w3.dataDirectory + "/" + name;
                    File file = new File(path + ".new");
                    FileOutputStream fout = new FileOutputStream(file);
                    w3m.oout.writeBoolean(true);
                    w3m.oout.flush();
                    try {
                        int n;
                        while ((n = w3m.in.read(w3m.buffer)) > 0) fout.write(w3m.buffer, 0, n);
                    } finally {
                        fout.close();
                    }
                    w3m.w3.changeNames(file, path);
                }});
        commands.put("reload parameters", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    if (w3m.w3.port == -1) throw new Exception("Server instance is disabled");
                    w3m.w3.readParameters();
                    w3m.respond("Parameters reloaded");
                }});
        commands.put("put domain", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    String domainName = (String)((Object[])w3m.parameter)[0];
                    Vector paths = (Vector)((Object[])w3m.parameter)[1];
                    Domain domain = w3m.w3.domainEntries.get(domainName);
                    String authName = (String)((Object[])w3m.parameter)[2];
                    W3Servlet auth = authName.equals("") ? w3m.w3.basic : w3m.w3.w3context.servlets.get(authName);
                    if (auth == null) throw new Exception("Authentication servlet not found");
                    String secondName = (String)((Object[])w3m.parameter)[3];
                    if (secondName.equals("")) secondName = null;
                    if (domain != null) {
                        w3m.w3.domainEntries.remove(domainName);
                        Enumeration domainPathElements = domain.paths.elements();
                        while (domainPathElements.hasMoreElements()) w3m.w3.domains.delete((String)domainPathElements.nextElement());
                        domain.paths = paths;
                        domain.auth = auth;
                    } else domain = new Domain(domainName, paths, auth, secondName);
                    Enumeration pathElements = paths.elements();
                    while (pathElements.hasMoreElements()) w3m.w3.domains.add((String)pathElements.nextElement(), domain);
                    w3m.w3.domainEntries.put(domainName, domain);
                    w3m.respond("Domain put");
                }});
        commands.put("get domain", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    Domain domain = w3m.w3.domainEntries.get(w3m.parameter);
                    if (domain == null) throw new Exception("No domain " + w3m.parameter + " found");
                    w3m.respond(new Object[] {domain.name, domain.paths, domain.auth.getServletName()});
                }});
        commands.put("first domain", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    w3m.domainIter = w3m.w3.domainEntries.entrySet().iterator();
                    if (!w3m.domainIter.hasNext()) throw new ServletException("No domains");
                    Domain domain = (Domain)((Map.Entry)w3m.domainIter.next()).getValue();
                    w3m.respond(new Object[] {domain.name, domain.paths,
                                              domain.auth.getServletName(),
                                              domain.secondName != null ? domain.secondName : ""});
                }});
        commands.put("next domain", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    if (w3m.domainIter == null) w3m.domainIter = w3m.w3.domainEntries.entrySet().iterator();
                    if (!w3m.domainIter.hasNext()) throw new ServletException("No next domain");
                    Domain domain = (Domain)((Map.Entry)w3m.domainIter.next()).getValue();
                    w3m.respond(new Object[] {domain.name, domain.paths,
                                              domain.auth.getServletName(),
                                              domain.secondName != null ? domain.secondName : ""});
                }});
        commands.put("drop domain", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    Domain domain = w3m.w3.domainEntries.get(w3m.parameter);
                    if (domain == null) throw new ServletException("No domain " + w3m.parameter + " found");
                    Enumeration domainPathElements = domain.paths.elements();
                    while (domainPathElements.hasMoreElements()) w3m.w3.domains.delete((String)domainPathElements.nextElement());
                    w3m.w3.domainEntries.remove(w3m.parameter);
                    w3m.respond("Domain dropped");
                }});
        commands.put("save domains", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    String path = w3m.w3.dataDirectory + "/domains";
                    File file = new File(path + ".new");
                    PrintWriter pw = new PrintWriter(new FileOutputStream(file));
                    pw.println("\n# Domains saved by W3Server at " + new Date() + "\n");
                    Iterator iter = w3m.w3.domainEntries.entrySet().iterator();
                    while (iter.hasNext()) {
                        Domain domain = (Domain)((Map.Entry)iter.next()).getValue();
                        String domainName = domain.name;
                        if (!domain.auth.equals(w3m.w3.basic))
                            domainName += ":" + domain.auth.getServletName();
                        if (domain.secondName != null) domainName += ";" + domain.secondName;
                        StringBuffer paths = new StringBuffer();
                        Enumeration domainPathElements = domain.paths.elements();
                        for (;;) {
                            paths.append((String)domainPathElements.nextElement());
                            if (!domainPathElements.hasMoreElements()) break;
                            paths.append(" ");
                        }
                        pw.println(domainName + "=" + paths.toString());
                    }
                    pw.close();
                    w3m.w3.changeNames(file, path);
                    w3m.respond("Domains saved");
                }});
        commands.put("put servlet", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    Object objs[] = (Object[])w3m.parameter;
                    W3Servlet w3servlet = w3m.w3.w3context.servlets.get((String)objs[0]);
                    if (w3servlet != null) {
                        w3m.w3.w3context.servlets.remove(w3m.parameter);
                        w3servlet.servlet.destroy();
                    }
                    w3servlet = new W3Servlet(w3m.w3.w3context, (String)objs[0], (String)objs[1], (Hashtable<String,String>)objs[2], ((Boolean)objs[3]).booleanValue(), false, false);
                    w3m.w3.w3context.setServlet(w3servlet);
                    if (!((Boolean)objs[3]).booleanValue()) w3m.w3.w3context.loadServlet(w3servlet);
                    w3m.respond("Servlet put");
                }});
        commands.put("get servlet", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    W3Servlet w3servlet = w3m.w3.w3context.servlets.get(w3m.parameter);
                    if (w3servlet == null) throw new Exception("No servlet " + w3m.parameter + " found");
                    w3m.respond(new Object[] {w3servlet.getServletName(), w3servlet.className, w3servlet.initParameters, new Boolean(w3servlet.isNative), new Boolean(!w3m.w3.w3context.servlets.containsKey(w3servlet.getServletName()))});
                }});
        commands.put("first servlet", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    w3m.servletIter = w3m.w3.w3context.servlets.values().iterator();
                    if (!w3m.servletIter.hasNext()) throw new ServletException("No servlets");
                    W3Servlet w3servlet = (W3Servlet)w3m.servletIter.next();
                    w3m.respond(new Object[] {w3servlet.getServletName(), w3servlet.className, w3servlet.initParameters, new Boolean(w3servlet.isNative), new Boolean(!w3m.w3.w3context.servlets.containsKey(w3servlet.getServletName()))});
                }});
        commands.put("next servlet", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    if (w3m.servletIter == null) w3m.servletIter = w3m.w3.w3context.servlets.values().iterator();
                    if (!w3m.servletIter.hasNext()) throw new ServletException("No next servlet");
                    W3Servlet w3servlet = (W3Servlet)w3m.servletIter.next();
                    w3m.respond(new Object[] {w3servlet.getServletName(), w3servlet.className, w3servlet.initParameters, new Boolean(w3servlet.isNative), new Boolean(!w3m.w3.w3context.servlets.containsKey(w3servlet.getServletName()))});
                }});
        commands.put("save servlets", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    String path = w3m.w3.dataDirectory + "/servlets";
                    File file = new File(path + ".new");
                    PrintWriter pw = new PrintWriter(new FileOutputStream(file));
                    pw.println("\n# Servlets saved by W3Server at " + new Date() + "\n");
                    Iterator servletIter = w3m.w3.w3context.servlets.values().iterator();
                    while (servletIter.hasNext()) {
                        W3Servlet w3servlet = (W3Servlet)servletIter.next();
                        pw.println(w3servlet.getServletName() + " " + w3servlet.className + (w3m.w3.w3context.servlets.containsKey(w3servlet.getServletName()) ? "": "  :disabled"));
                        Hashtable initParameters = w3servlet.initParameters;
                        if (initParameters != null) {
                            Iterator initParameterIter = initParameters.entrySet().iterator();
                            while (initParameterIter.hasNext()) {
                                Map.Entry entry = (Map.Entry)initParameterIter.next();
                                pw.println("\t" + entry.getKey() + "=\"" + encode((String)entry.getValue()) + "\"");
                            }
                        }
                        pw.println();
                    }
                    pw.close();
                    w3m.w3.changeNames(file, path);
                    w3m.respond("Servlets saved");
                }});
        commands.put("put path", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    String scheme = ((String[])w3m.parameter)[0];
                    Object obj = w3m.w3.w3context.getAttribute("FI.realitymodeler.server.W3Server/cache/" + scheme);
                    if (!(obj instanceof Object[])) throw new Exception("No " + scheme + " cache paths found");
                    String path = ((String[])w3m.parameter)[1];
                    Object objs[] = (Object[])obj;
                    ((RegexpPool)objs[0]).add(path, path);
                    ((Map<String, String>)objs[1]).put(path, path);
                    w3m.respond("Path put");
                }});
        commands.put("first path", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    Object obj = w3m.w3.w3context.getAttribute("FI.realitymodeler.server.W3Server/cache/" + w3m.parameter);
                    if (!(obj instanceof Object[])) throw new Exception("No " + w3m.parameter + " cache paths found");
                    w3m.pathIter = ((TreeMap)((Object[])obj)[1]).values().iterator();
                    if (!w3m.pathIter.hasNext()) throw new Exception("No paths");
                    w3m.respond(w3m.pathIter.next());
                }});
        commands.put("next path", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    if (w3m.pathIter == null || !w3m.pathIter.hasNext()) throw new Exception("No next path");
                    w3m.respond(w3m.pathIter.next());
                }});
        commands.put("drop path", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    String scheme = ((String[])w3m.parameter)[0];
                    Object obj = w3m.w3.w3context.getAttribute("FI.realitymodeler.server.W3Server/cache/" + scheme);
                    if (!(obj instanceof Object[])) throw new Exception("No " + scheme + " cache paths found");
                    Object objs[] = (Object[])obj;
                    String path = ((String[])w3m.parameter)[1];
                    ((RegexpPool)objs[0]).delete(path);
                    ((TreeMap)objs[1]).remove(path);
                    w3m.respond("Path dropped");
                }});
        commands.put("save paths", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    Object obj = w3m.w3.w3context.getAttribute("FI.realitymodeler.server.W3Server/cache/" + w3m.parameter);
                    if (!(obj instanceof Object[])) throw new Exception("No " + w3m.parameter + " cache paths found");
                    TreeMap map = (TreeMap)((Object[])obj)[1];
                    String path = w3m.w3.dataDirectory + "/" + w3m.parameter + "_cache_paths";
                    File file = new File(path + ".new");
                    PrintWriter pw = new PrintWriter(new FileOutputStream(file));
                    pw.println("# Cache paths saved by W3Server at " + Support.dateFormat.format(new Date()) + "\n");
                    Iterator pathIter = map.values().iterator();
                    while (pathIter.hasNext()) pw.println(pathIter.next());
                    pw.close();
                    w3m.w3.changeNames(file, path);
                    w3m.respond("Paths saved");
                }});
        commands.put("put virtual", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    String pattern = ((String[])w3m.parameter)[0], target = ((String[])w3m.parameter)[1];
                    w3m.w3.virtualPaths.add(pattern, new VirtualPathTarget(pattern, target));
                    w3m.w3.virtualPathMap.put(pattern, target);
                    w3m.respond("Virtual path put");
                }});
        commands.put("get virtual", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    String path = w3m.w3.virtualPathMap.get(w3m.parameter);
                    if (path == null) throw new Exception("No virtual path " + w3m.parameter + " found");
                    w3m.respond(new String[] {(String)w3m.parameter, path});
                }});
        commands.put("first virtual", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    w3m.virtualPathIter = w3m.w3.virtualPathMap.entrySet().iterator();
                    if (!w3m.virtualPathIter.hasNext()) throw new ServletException("No virtual paths");
                    Map.Entry entry = (Map.Entry)w3m.virtualPathIter.next();
                    w3m.respond(new String[] {(String)entry.getKey(), (String)entry.getValue()});
                }});
        commands.put("next virtual", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    if (w3m.virtualPathIter == null) w3m.virtualPathIter = w3m.w3.virtualPathMap.entrySet().iterator();
                    if (!w3m.virtualPathIter.hasNext()) throw new ServletException("No next name");
                    Map.Entry entry = (Map.Entry)w3m.virtualPathIter.next();
                    w3m.respond(new String[] {(String)entry.getKey(), (String)entry.getValue()});
                }});
        commands.put("drop virtual", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    w3m.w3.virtualPaths.delete((String)w3m.parameter);
                    w3m.w3.virtualPathMap.remove(w3m.parameter);
                    w3m.respond("Virtual path dropped");
                }});
        commands.put("save virtuals", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    String path = w3m.w3.dataDirectory + "/virtual_paths";
                    File file = new File(path + ".new");
                    PrintWriter pw = new PrintWriter(new FileOutputStream(file));
                    pw.println("\n# Virtual paths saved by W3Server at " + new Date() + "\n");
                    Iterator pathIter = w3m.w3.virtualPathMap.entrySet().iterator();
                    while (pathIter.hasNext()) {
                        Map.Entry entry = (Map.Entry)pathIter.next();
                        pw.println(entry.getKey() + " " + entry.getValue());
                    }
                    pw.close();
                    w3m.w3.changeNames(file, path);
                    w3m.respond("Virtual paths saved");
                }});
        commands.put("put type", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    String mimeName = (String)((Object[])w3m.parameter)[0];
                    Vector<String> exts = (Vector<String>)((Object[])w3m.parameter)[1],
                        vector = (Vector<String>)((Object[])w3m.parameter)[2];
                    if (!vector.isEmpty()) {
                        Vector<Step> chain = new Vector<Step>();
                        Enumeration<String> vectorElements = vector.elements();
                        while (vectorElements.hasMoreElements()) {
                            StringTokenizer st = new StringTokenizer(vectorElements.nextElement(), ":");
                            String targetMimeName = st.nextToken().trim(), servletName = st.nextToken().trim();
                            boolean start;
                            if (targetMimeName.startsWith(".")) {
                                targetMimeName = targetMimeName.substring(1);
                                start = true;
                            } else start = false;
                            W3Servlet w3servlet = w3m.w3.w3context.servlets.get(servletName);
                            if (w3servlet == null) throw new Exception("Servlet " + servletName + " not found");
                            chain.addElement(new Step(targetMimeName, w3servlet, start));
                        }
                        w3m.w3.filters.put(mimeName, chain);
                    } else w3m.w3.filters.remove(mimeName);
                    Enumeration extElements = exts.elements();
                    while (extElements.hasMoreElements()) w3m.w3.w3context.mimeTypeMap.put((String)extElements.nextElement(), mimeName);
                    w3m.w3.w3context.mimeTypes.put(mimeName, exts);
                    w3m.respond("Type put");
                }});
        commands.put("get type", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    Vector<String> exts = w3m.w3.w3context.mimeTypes.get(w3m.parameter);
                    if (exts == null) throw new Exception("No mime type " + w3m.parameter + " found");
                    w3m.respondType((String)w3m.parameter, exts, (Vector)w3m.w3.filters.get(w3m.parameter));
                }});
        commands.put("first type", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    w3m.typeIter = w3m.w3.w3context.mimeTypes.entrySet().iterator();
                    if (!w3m.typeIter.hasNext()) throw new ServletException("No mime types");
                    Map.Entry entry = (Map.Entry)w3m.typeIter.next();
                    String mimeName = (String)entry.getKey();
                    w3m.respondType(mimeName, (Vector)entry.getValue(), (Vector)w3m.w3.filters.get(mimeName));
                }});
        commands.put("next type", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    if (w3m.typeIter == null) w3m.typeIter = w3m.w3.w3context.mimeTypes.entrySet().iterator();
                    if (!w3m.typeIter.hasNext()) throw new ServletException("No next type");
                    Map.Entry entry = (Map.Entry)w3m.typeIter.next();
                    String mimeName = (String)entry.getKey();
                    w3m.respondType(mimeName, (Vector)entry.getValue(), (Vector)w3m.w3.filters.get(mimeName));
                    return;
                }});
        commands.put("drop type", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    Vector<String> exts = w3m.w3.w3context.mimeTypes.get(w3m.parameter);
                    if (exts == null) throw new ServletException("No mime type " + w3m.parameter + " found");
                    Enumeration extElements = exts.elements();
                    while (extElements.hasMoreElements()) w3m.w3.w3context.mimeTypes.remove(extElements.nextElement());
                    w3m.w3.w3context.mimeTypes.remove(w3m.parameter);
                    w3m.respond("Mime type dropped");
                }});
        commands.put("save types", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    String path = w3m.w3.dataDirectory + "/mime_types";
                    File file = new File(path + ".new");
                    PrintWriter pw = new PrintWriter(new FileOutputStream(file));
                    pw.println("\n# Mime types saved by W3Server at " + new Date() + "\n");
                    Iterator iter = w3m.w3.w3context.mimeTypes.entrySet().iterator();
                    while (iter.hasNext()) {
                        Map.Entry entry = (Map.Entry)iter.next();
                        String mimeName = (String)entry.getKey();
                        pw.print("\"" + mimeName);
                        Vector chain = (Vector)w3m.w3.filters.get(mimeName);
                        if (chain != null) {
                            Enumeration chainEnum = chain.elements();
                            while (chainEnum.hasMoreElements()) {
                                Step step = (Step)chainEnum.nextElement();
                                pw.print(" " + (step.start ? "." : "") + step.mimeName + ":" + step.w3servlet.getServletName());
                            }
                        }
                        pw.print("\"=");
                        Enumeration extEnum = ((Vector)entry.getValue()).elements();
                        for (;;) {
                            pw.print(extEnum.nextElement());
                            if (!extEnum.hasMoreElements()) break;
                            pw.print(" ");
                        }
                        pw.println();
                    }
                    pw.close();
                    w3m.w3.changeNames(file, path);
                    w3m.respond("Types saved");
                }});
        commands.put("stop server", new CommandAdapter() {
                void execute(W3Manager w3m)
                    throws Exception {
                    if (!new File(w3m.w3.dataDirectory + "/" + w3m.w3.stopReallyFilename).exists())
                        throw new Exception("Failed to stop server");
                    w3m.w3.end();
                    w3m.respond("Server stopped");
                }});
    }

    private transient W3Server w3;
    private transient InputStream in;
    private transient OutputStream out;
    private transient ObjectOutputStream oout;
    private transient Iterator pathIter, domainIter, servletIter, typeIter, virtualPathIter;
    private transient Object parameter;
    private transient byte buffer[];

    static String encode(String value) {
        StringBuffer valueBuf = new StringBuffer();
        int l = value.length();
        for (int i = 0; i < l; i++) {
            if ("\\\"".indexOf(value.charAt(i)) != -1) valueBuf.append('\\');
            valueBuf.append(value.charAt(i));
        }
        return valueBuf.toString();
    }

    void respond(Object value)
        throws IOException {
        oout.writeBoolean(true);
        oout.flush();
        oout.writeObject(value);
        oout.flush();
    }

    void respondType(String mimeName, Vector exts, Vector chain)
        throws IOException {
        Vector<String> vector = new Vector<String>();
        if (chain != null) {
            Enumeration chainElements = chain.elements();
            while (chainElements.hasMoreElements()) {
                Step step = (Step)chainElements.nextElement();
                vector.addElement((step.start ? "." : "") + step.mimeName + ":" + step.w3servlet.getServletName());
            }
        }
        respond(new Object[] {mimeName, exts, vector});
    }

    public void init(ServletConfig config)
        throws ServletException {
        super.init(config);
    }

    public synchronized void doPut(HttpServletRequest req, HttpServletResponse res)
        throws IOException, ServletException {
        W3Request w3r = (W3Request)req.getAttribute("");
        oout = null;
        w3 = w3r.w3server;
        buffer = w3r.buffer;
        try {
            in = req.getInputStream();
            ObjectInputStream oin = new ObjectInputStream(in);
            out = res.getOutputStream();
            oout = new ObjectOutputStream(out);
            int port = oin.readInt();
            w3 = W3Server.servers.get(new Integer(port));
            if (w3 == null) throw new Exception("No server in port " + port);
            String name = (String)oin.readObject();
            parameter = oin.readObject();
            CommandAdapter command = commands.get(name);
            if (command == null) throw new Exception("Unknown command");
            String parameter1 = parameter instanceof String ? (String)parameter :
                parameter instanceof String[] ? ((String[])parameter)[0] : null;
            log(port + ": " + name + (parameter1 != null ? " " + parameter1 : ""));
            command.execute(this);
        } catch (Exception ex) {
            w3.log(ex);
            if (oout != null) {
                oout.writeBoolean(false);
                oout.flush();
                oout.writeObject(Support.stackTrace(ex));
                oout.flush();
            }
        }
    }

}
