
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;
import javax.naming.*;
import javax.naming.directory.*;
import javax.naming.ldap.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Content push/pull engine. Initialization parameters:<br>
    from=email address set as origin when sending to SMTP/NNTP-servers.<br>
    host=host name where database is kept or null if in localhost.<br>
    Stores subscription data to LDAP directory with standard schema.<br>
    Subscription structure has following properties:<br>
    Object class: organizationalUnit<br>
    Distinguished name: ou=unique id<br>
    Attributes:<br>
    <table>
    <caption>Organizational unit</caption>
    <tr><th>postalAddress</th><td>subscriber</td></tr>
    <tr><th>description</th><td>sourceUrl</td></tr>
    <tr><th>registeredAddress</th><td>targetUrl</td></tr>
    <tr><th>st</th><td>stamp</td></tr>
    <tr><th>street</th><td>time</td></tr>
    <tr><th>l</th><td>logic bag</td></tr>
    </table>
    Source structure has following properties:<br>
    Object class: locality<br>
    Distinguished name: l=unique id<br>
    Attributes:<br>
    <table>
    <caption>Locality</caption>
    <tr><th>description</th><td>sourceUrl</td></tr>
    <tr><th>street</th><td>time</td></tr>
    </table>
*/
public class ContentEngine extends HttpServlet {
    static final long serialVersionUID = 0L;
    CookieStore cookieStore;
    LdapContext ldapContext[] = new InitialLdapContext[1], contentEngineContext;
    ResourceBundle messages;
    String from;
    DaemonThread daemon = null;
    SortControl timeSortControl[];
    ThreadGroup group = new ThreadGroup(getClass().getName());
    W3Server w3;
    boolean verbose = false;

    class TransferThread extends Thread {
        String source;

        TransferThread(String source) {
            super(ContentEngine.this.group, source);
            this.source = source;
        }

        public void run() {
            if (verbose) log("Transfer started for source " + source);
            LdapContext ldapContext[] = new InitialLdapContext[1], contentEngineContext = null;
            CookieStore cookieStore = null;
            LinkStore linkStore = null;
            W3URLConnection ucIn = null, ucOut = null;
            try {
                contentEngineContext = W3Server.open(ContentEngine.this.getClass().getName(), ldapContext);
                linkStore = new LinkStore(ldapContext);
                cookieStore = new CookieStore(ldapContext);
                RegexpPool nameParts[] = null;
                URL url;
                int i = source.indexOf('*');
                if (i != -1) {
                    // Remove asterisk from initial URL
                    url = new URL(source.substring(0, i) + source.substring(i + 1));
                    String urlKey = new URL(new URL(source.substring(0, i + 1)), "").toString();
                    nameParts = new RegexpPool[2];
                    nameParts[0] = new RegexpPool();
                    nameParts[0].add(urlKey, urlKey);
                } else url = new URL(source);
                String protocol = url.getProtocol().toLowerCase();
                byte b[] = new byte[Support.bufferLength];
                HashMap<String,String> credentials = new HashMap<String,String>();
                HashMap<String,Object> doneUrls = new HashMap<String,Object>();
                Vector<URL> urls = new Vector<URL>();
                urls.addElement(url);
                boolean forward = false;
                for (int index = 0; index < urls.size(); index++) {
                    url = urls.elementAt(index);
                    if (verbose) log("Reading url " + url);
                    boolean forceClose = false;
                    try {
                        if (ucIn == null || !ucIn.mayKeepAlive() || !ucIn.check(url)) {
                            if (ucIn != null) {
                                ucIn.disconnect();
                                ucIn = null;
                            }
                            ucIn = W3URLConnection.openConnection(url);
                        } else ucIn.set(url);
                        ucIn.setForceCaches(true);
                        String clientHost = InetAddress.getByName(url.getHost()).getHostAddress(), clientPath = url.getFile();
                        if ((i = clientPath.indexOf('?')) != -1) clientPath = clientPath.substring(0, i).trim();
                        cookieStore.setCookieHeaderValues(ucIn.getRequestHeaderFields(), source, clientHost, clientPath);
                        String authorization = ucIn.getRequestProperty("authorization");
                        if (authorization == null && (authorization = credentials.get(clientHost)) != null)
                            ucIn.setRequestProperty("Authorization", authorization);
                        InputStream in = ucIn.getInputStream();
                        if (ucIn.getResponseCode() != HttpURLConnection.HTTP_OK) throw new IOException("Wrong status " + ucIn.getResponseCode());
                        if ((authorization = ucIn.getRequestProperty("authorization")) != null) credentials.put(clientHost, authorization);
                        cookieStore.getCookieHeaderValues((HeaderList)ucIn.getHeaderFields(), source, clientHost, clientPath);
                        if (!ucIn.isHtml() || forward) {
                            while (in.read(b) > 0);
                            doneUrls.put(url.toString(), Boolean.TRUE);
                        } else {
                            if (index == 0 && ucIn instanceof W3MsgURLConnection) forward = ((W3MsgURLConnection)ucIn).getForward();
                            W3URLConnection.parse(in, urls, null, null, url, nameParts, doneUrls,
                                                   null, null, null, null, null, null, true, false);
                        }
                        if (forward) break;
                    } catch (IOException ex) {
                        forceClose = true;
                        log(ex.toString(), ex);
                    } finally {
                        if (ucIn != null)
                            if (!ucIn.mayKeepAlive() || forceClose) {
                                ucIn.disconnect();
                                ucIn = null;
                            } else ucIn.closeStreams();
                    }
                }
                if (ucIn != null) {
                    ucIn.disconnect();
                    ucIn = null;
                }
                if (nameParts != null && protocol.equals("pop3") || protocol.equals("nntp")) urls.removeElementAt(0);
                Calendar cal = Calendar.getInstance();
                long newTime = Long.MAX_VALUE;
                contentEngineContext.setRequestControls(timeSortControl);
                NamingEnumeration subscriptionResultEnum = null;
                try {
                    subscriptionResultEnum = contentEngineContext.search("", "(&(objectClass=organizationalUnit)(description=" + Support.escapeFilter(source) + ")(street<=" + Support.generalizedTime(System.currentTimeMillis()) + "))", null);
                    while (subscriptionResultEnum.hasMore()) {
                        SearchResult subscriptionResult = (SearchResult)subscriptionResultEnum.next();
                        Attributes subscriptionAttributes = subscriptionResult.getAttributes();
                        Attribute logicBagAttribute = subscriptionAttributes.get("l"),
                            targetUrlAttribute = subscriptionAttributes.get("registeredAddress");
                        if (targetUrlAttribute == null) continue;
                        String logicBag = logicBagAttribute != null ? (String)logicBagAttribute.get() : null, targetUrl = (String)targetUrlAttribute.get();
                        if (verbose) log("Handling target " + targetUrl);
                        URL urlOut = new URL(targetUrl);
                        String protocolOut = urlOut.getProtocol().toLowerCase();
                        boolean reverse = protocolOut.equals("mailto") || protocolOut.equals("nntp");
                        int inc = reverse ? -1 : 1;
                        for (int index = reverse ? urls.size() - 1 : 0; reverse ? index >= 0 : index < urls.size(); index += inc) {
                            URL urlIn = urls.elementAt(index);
                            if (verbose) log("Trying to read " + urlIn);
                            boolean forceClose = false;
                            try {
                                if (ucIn == null || !ucIn.mayKeepAlive() || !ucIn.check(urlIn)) {
                                    if (ucIn != null) {
                                        ucIn.disconnect();
                                        ucIn = null;
                                    }
                                    ucIn = W3URLConnection.openConnection(urlIn);
                                } else ucIn.set(urlIn);
                                ucIn.setUseCaches(true);
                                String clientHost = InetAddress.getByName(url.getHost()).getHostAddress(), clientPath = url.getFile(), authorization = credentials.get(clientHost);
                                if (authorization != null) ucIn.setRequestProperty("Authorization", authorization);
                                cookieStore.setCookieHeaderValues(ucIn.getRequestHeaderFields(), source, clientHost, clientPath);
                                InputStream in = ucIn.getInputStream();
                                if (ucIn.getResponseCode() != HttpURLConnection.HTTP_OK) throw new IOException("Wrong status " + ucIn.getResponseCode());
                                cookieStore.getCookieHeaderValues((HeaderList)ucIn.getHeaderFields(), source, clientHost, clientPath);
                                if (logicBag != null && !W3Request.evaluate(logicBag, (HeaderList)ucIn.getHeaderFields())) continue;
                                if (verbose) log("Sending " + urlIn + " to " + urlOut);
                                if (ucOut == null || !ucOut.mayKeepAlive() || !ucOut.check(urlOut)) {
                                    if (ucOut != null) {
                                        ucOut.disconnect();
                                        ucOut = null;
                                    }
                                    ucOut = W3URLConnection.openConnection(urlOut);
                                } else ucOut.set(urlOut);
                                String key;
                                for (i = 1; (key = ucIn.getHeaderFieldKey(i)) != null; i++)
                                    ucOut.addRequestProperty(key, ucIn.getHeaderField(i));
                                if (protocolOut.equals("sms")) {
                                    if (!ucIn.isText()) continue;
                                    continue;
                                }
                                ucOut.setRequestProperty("X-SourceURL", urlIn.toString());
                                if (reverse) {
                                    if (ucIn.getHeaderField("subject") == null &&
                                        !protocol.equals("pop3") && !protocol.equals("imap4"))
                                        ucOut.setRequestProperty("Subject", urlIn.toString());
                                    if (ucIn.getHeaderField("from") == null && from != null)
                                        ucOut.setRequestProperty("From", from);
                                    if (forward) {
                                        OutputStream out = ucOut.getOutputStream();
                                        for (int n; (n = in.read(b)) > 0;) out.write(b, 0, n);
                                        out.close();
                                        ucIn.complete();
                                        continue;
                                    }
                                    boolean text = ucIn.isText();
                                    ucOut.setRequestProperty("Content-Transfer-Encoding",
                                                             text ? "quoted-printable" : "base64");
                                    OutputStream out = ucOut.getOutputStream();
                                    if (text) new QPEncoder().encode(in, out);
                                    else new BASE64Encoder().encode(in, out);
                                    out.close();
                                } else {
                                    OutputStream out = ucOut.getOutputStream();
                                    for (int n; (n = in.read(b)) > 0;) out.write(b, 0, n);
                                    out.flush();
                                }
                                in = ucOut.getInputStream();
                                if (ucOut.getResponseCode() != HttpURLConnection.HTTP_OK &&
                                    ucOut.getResponseCode() != HttpURLConnection.HTTP_NO_CONTENT)
                                    throw new IOException("Wrong status " + ucOut.getResponseCode());
                                if (in != null) for (int n; (n = in.read(b)) > 0;);
                                ucIn.complete();
                            } catch (IOException ex) {
                                forceClose = true;
                                log(ex.toString(), ex);
                            } finally {
                                try {
                                    if (ucOut != null)
                                        if (!ucOut.mayKeepAlive() || forceClose) {
                                            ucOut.disconnect();
                                            ucOut = null;
                                        } else ucOut.closeStreams();
                                } finally {
                                    if (ucIn != null) {
                                        if (!ucIn.mayKeepAlive() || forceClose) {
                                            ucIn.disconnect();
                                            ucIn = null;
                                        } else ucIn.closeStreams();
                                    }
                                }
                            }
                        }
                        cal.setTime(new java.util.Date());
                        Attribute stampAttribute = subscriptionAttributes.get("st");
                        Stamp stamp = new Stamp(stampAttribute != null ? (String)stampAttribute.get() : "");
                        String subscriptionDn = subscriptionResult.getName();
                        if (stamp.changeCal(cal)) {
                            long time = cal.getTime().getTime();
                            if (time < newTime) newTime = time;
                            if (verbose) log("Setting new time " + new Date(time) + " for " + subscriptionDn);
                            subscriptionAttributes = new BasicAttributes();
                            subscriptionAttributes.put("street", Support.generalizedTime(time));
                            contentEngineContext.modifyAttributes(subscriptionDn, contentEngineContext.REPLACE_ATTRIBUTE, subscriptionAttributes);
                        } else {
                            if (verbose) log("Unbinding subscription " + subscriptionDn);
                            contentEngineContext.unbind(subscriptionDn);
                        }
                    }
                } finally {
                    if (subscriptionResultEnum != null) subscriptionResultEnum.close();
                }
                changeSourceTime(source, newTime, true);
            } catch (Exception ex) {
                log(ex.toString(), ex);
            } finally {
                try {
                    if (ucIn != null) ucIn.disconnect();
                    if (ucOut != null) ucOut.disconnect();
                } finally {
                    try {
                        if (cookieStore != null) cookieStore.close(null);
                    } finally {
                        try {
                            if (linkStore != null) linkStore.close(null);
                        } finally {
                            W3Server.close(contentEngineContext, ldapContext);
                        }
                    }
                }
            }
        }

    }

    class DaemonThread extends Thread {

        synchronized void pause(long millis) throws InterruptedException {
            if (millis == -1) wait();
            else if (millis > 0) wait(millis);
        }

        synchronized void proceed() {
            notifyAll();
        }

        public void run() {
            try {
                while (!daemon.isInterrupted()) {
                    long ctm = System.currentTimeMillis(), delay = -1L;
                    java.util.Date date = new java.util.Date(ctm);
                    Calendar cal = Calendar.getInstance();
                    try {
                        if (verbose) log("Checking transfer");
                        contentEngineContext.setRequestControls(timeSortControl);
                        NamingEnumeration sourceResultEnum = null;
                        try {
                            sourceResultEnum = contentEngineContext.search("", "(&(objectClass=locality)(street<=" + Support.generalizedTime(System.currentTimeMillis()) + "))", null);
                            while (sourceResultEnum.hasMore()) new TransferThread((String)((SearchResult)sourceResultEnum.next()).getAttributes().get("description").get()).start();
                        } finally {
                            if (sourceResultEnum != null) sourceResultEnum.close();
                        }
                        if (verbose) log("Checking transfer done");
                    } finally {
                        Thread threads[] = new Thread[group.activeCount()];
                        if (threads.length > 0) {
                            group.enumerate(threads);
                            if (verbose) log("Waiting for " + threads);
                            for (int i = 0; i < threads.length; i++)
                                if (threads[i] != null) threads[i].join();
                            if (verbose) log("Threads completed");
                        }
                    }
                    CookieStore.checkCleaning(w3.w3context);
                    contentEngineContext.setRequestControls(timeSortControl);
                    NamingEnumeration sourceResultEnum = null;
                    try {
                        sourceResultEnum = contentEngineContext.search("", "(&(objectClass=locality))",
                                                                       new SearchControls(SearchControls.ONELEVEL_SCOPE, 1L, 0, null, false, true));
                        if (sourceResultEnum.hasMore() &&
                            (delay = Support.parseGeneralizedTime((String)((SearchResult)sourceResultEnum.next()).getAttributes().get("street").get()) -
                             (ctm = System.currentTimeMillis())) < 0L) delay = 0L;
                    } finally {
                        if (sourceResultEnum != null) sourceResultEnum.close();
                    }
                    if (verbose) log("Pausing for " + delay);
                    pause(delay);
                }
            } catch (Exception ex) {
                if (ex instanceof InterruptedException) return;
                log(ex.toString(), ex);
            }
        }

    }

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        messages = W3URLConnection.getMessages("FI.realitymodeler.server.resources.Messages");
        ServletContext servletContext = config.getServletContext();
        if (servletContext.getAttribute("FI.realitymodeler.server.W3Server/verbose") == Boolean.TRUE) verbose = true;
        from = config.getInitParameter("from");
        boolean optional = false;
        Exception ex = null;
        try {
            if (Support.isTrue(config.getInitParameter("optional"), "optional")) optional = true;
            contentEngineContext = W3Server.open(this.getClass().getName(), ldapContext);
            cookieStore = new CookieStore(ldapContext);
            timeSortControl = new SortControl[] {new SortControl(new String[] {"street"}, false)};
        } catch (ParseException ex1) {
            ex = ex1;
        } catch (NamingException ex1) {
            ex = ex1;
        } catch (IOException ex1) {
            ex = ex1;
        }
        if (ex != null) {
            if (optional) {
                log(ex.toString(), ex);
                return;
            }
            throw new ServletException(Support.stackTrace(ex));
        }
        daemon = new DaemonThread();
        daemon.setDaemon(true);
        daemon.start();
    }

    public void changeSourceTime(String source, long newTime, boolean force) throws NamingException, ParseException {
        Attributes sourceAttributes = new BasicAttributes();
        sourceAttributes.put("street", Support.generalizedTime(newTime));
        SearchResult sourceResult = null;
        NamingEnumeration sourceResultEnum = null;
        try {
            if (verbose) log("Searching source " + source);
            sourceResultEnum = contentEngineContext.search("",
                                                           "(&(objectClass=locality)(description=" + Support.escapeFilter(source) + "))",
                                                           new SearchControls(SearchControls.ONELEVEL_SCOPE, 1L, 0, null, false, true));
            sourceResult = (SearchResult)sourceResultEnum.next();
            if (sourceResult != null) {
                if (verbose) log("Found source " + sourceResult.getName() + " with attributes " + sourceResult.getAttributes());
                if (newTime == Long.MAX_VALUE) {
                    if (verbose) log("Unbinding source " + sourceResult.getName() + " with attributes " + sourceResult.getAttributes());
                    contentEngineContext.unbind(sourceResult.getName());
                    return;
                }
                if (force || Support.parseGeneralizedTime((String)sourceResult.getAttributes().get("street").get()) > newTime) {
                    if (verbose) log("Modifying " + sourceResult.getName() + " with attributes " + sourceAttributes);
                    contentEngineContext.modifyAttributes(sourceResult.getName(), contentEngineContext.REPLACE_ATTRIBUTE, sourceAttributes);
                }
            }
        } catch (NameNotFoundException ex) {} catch (NoSuchElementException ex) {} finally {
            if (sourceResultEnum != null) sourceResultEnum.close();
        }
        if (newTime == Long.MAX_VALUE) return;
        if (sourceResult == null) {
            Attribute sourceObjectClass = new BasicAttribute("objectClass");
            sourceObjectClass.add("top");
            sourceObjectClass.add("locality");
            sourceAttributes.put(sourceObjectClass);
            sourceAttributes.put("description", source);
            if (verbose) log("Rebinding source with attributes " + sourceAttributes);
            contentEngineContext.rebind(W3Server.makeName(contentEngineContext, "l"), null, sourceAttributes);
        }
    }

    public void subscribe(LdapContext contentEngineContext, String subscriber, URL sourceURL, Vector stamps, Vector targets, String logicBag, Date received) throws NamingException, ParseException {
        if (verbose) log("Subscribing " + sourceURL + " to " + subscriber);
        Calendar cal = Calendar.getInstance();
        String source = sourceURL.toString();
        long newTime = Long.MAX_VALUE;
        Enumeration targetEnum = targets != null ? targets.elements() : null;
        do {
            String target = "";
            if (targetEnum != null)
                if (targetEnum.hasMoreElements()) target = (String)targetEnum.nextElement();
                else break;
            Enumeration stampEnum = stamps.elements();
            while (stampEnum.hasMoreElements()) {
                Stamp stamp = (Stamp)stampEnum.nextElement();
                cal.setTime(received);
                if (stamp.changeCal(cal)) {
                    SearchResult subscriptionResult = null;
                    NamingEnumeration subscriptionResultEnum = null;
                    try {
                        subscriptionResultEnum = contentEngineContext.search("",
                                                                             "(&(objectClass=organizationalUnit)(postalAddress=" + Support.escapeFilter(subscriber) +
                                                                             ")(description=" + Support.escapeFilter(source) +
                                                                             ")" + (target.equals("") ? "" : "(registeredAddress=" + Support.escapeFilter(target) + ")") + ")",
                                                                             new SearchControls(SearchControls.ONELEVEL_SCOPE, 1L, 0, null, false, true));
                        subscriptionResult = (SearchResult)subscriptionResultEnum.next();
                        if (verbose) log("Found subscription " + subscriptionResult.getName() + " with attributes " + subscriptionResult.getAttributes());
                    } catch (NameNotFoundException ex) {} catch (NoSuchElementException ex) {} finally {
                        if (subscriptionResultEnum != null) subscriptionResultEnum.close();
                    }
                    Attributes subscriptionAttributes = new BasicAttributes();
                    Attribute subscriptionObjectClass = new BasicAttribute("objectClass");
                    subscriptionObjectClass.add("top");
                    subscriptionObjectClass.add("organizationalUnit");
                    subscriptionAttributes.put(subscriptionObjectClass);
                    subscriptionAttributes.put("postalAddress", subscriber);
                    subscriptionAttributes.put("description", source);
                    if (logicBag != null && !logicBag.equals("")) subscriptionAttributes.put("l", logicBag);
                    String stampString = stamp.toString();
                    if (!stampString.equals("")) subscriptionAttributes.put("st", stampString);
                    long time = cal.getTime().getTime();
                    if (time < newTime) newTime = time;
                    subscriptionAttributes.put("street", Support.generalizedTime(time));
                    if (!target.equals("")) subscriptionAttributes.put("registeredAddress", target);
                    if (verbose) log("Rebinding subscription with attributes " + subscriptionAttributes);
                    contentEngineContext.rebind((subscriptionResult != null ? subscriptionResult.getName() : W3Server.makeName(contentEngineContext, "ou")), null, subscriptionAttributes);
                }
            }
        } while (targetEnum != null);
        changeSourceTime(source, newTime, false);
        daemon.proceed();
    }

    public void signoff(LdapContext contentEngineContext, String encoding, PrintWriter writer, String action, String subscriber, URL sourceURL, Vector targets, boolean list) throws InterruptedException, ServletException, NamingException, UnsupportedEncodingException {
        Enumeration targetEnum = targets != null ? targets.elements() : null;
        if (list) writer.println("<table>");
        do {
            StringBuffer filter = new StringBuffer();
            filter.append("(&(objectClass=organizationalUnit)(postalAddress=").append(Support.escapeFilter(subscriber)).append(')');
            if (sourceURL != null) filter.append("(description=").append(Support.escapeFilter(sourceURL.toString())).append(')');
            if (targetEnum != null)
                if (targetEnum.hasMoreElements()) {
                    filter.append("(registeredAddress=").append(Support.escapeFilter((String)targetEnum.nextElement())).append(")");
                } else break;
            filter.append(')');
            NamingEnumeration subscriptionResultEnum = null;
            try {
                subscriptionResultEnum = contentEngineContext.search("", filter.toString(), null);
                if (list)
                    while (subscriptionResultEnum.hasMore()) {
                        SearchResult subscriptionResult = (SearchResult)subscriptionResultEnum.next();
                        Attributes subscriptionAttributes = subscriptionResult.getAttributes();
                        Attribute logicBagAttribute = subscriptionAttributes.get("l"),
                            sourceAttribute = subscriptionAttributes.get("description"),
                            stampAttribute = subscriptionAttributes.get("st"),
                            targetAttribute = subscriptionAttributes.get("registeredAddress");
                        String source = sourceAttribute != null ? (String)sourceAttribute.get() : "",
                            stampString = stampAttribute != null ? (String)stampAttribute.get() : "",
                            target = targetAttribute != null ? (String)targetAttribute.get() : "",
                            logicBag = logicBagAttribute != null ? (String)logicBagAttribute.get() : "";
                        writer.println("<a href=\"" + action + "?new=&source=" +
                                       URLEncoder.encode(source, encoding) + "&stamp=" +
                                       URLEncoder.encode(stampString, encoding) + "&targets=" +
                                       URLEncoder.encode(target, encoding) + "\"><tr><td>" +
                                       Support.htmlString(source) + "</td><td>" +
                                       Support.htmlString(target) + "</td><td>" +
                                       Support.htmlString(stampString) + "</td><td>" +
                                       Support.htmlString(logicBag) + "</td></tr></a>");
                    } else while (subscriptionResultEnum.hasMore()) contentEngineContext.unbind(((SearchResult)subscriptionResultEnum.next()).getName());
            } finally {
                if (subscriptionResultEnum != null) subscriptionResultEnum.close();
            }
        } while (targetEnum != null);
        if (list) writer.println("</table>");
    }

    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String action = req.getRequestURI(), s,
            source = (s = Support.getParameterValue(req.getParameterValues("source"))) != null ? s.trim() : "",
            stamps = (s = Support.getParameterValue(req.getParameterValues("stamps"))) != null ? s.trim() : "",
            targets = (s = Support.getParameterValue(req.getParameterValues("targets"))) != null ? s.trim() : "",
            logicBag = (s = Support.getParameterValue(req.getParameterValues("logicBag"))) != null ? s.trim() : "";
        String encoding = req.getCharacterEncoding();
        if (encoding == null) encoding = "8859_1";
        boolean mobile = (s = req.getHeader("user-agent")) != null && s.trim().equalsIgnoreCase("mobile");
        int i = action.indexOf('?');
        if (i != -1) action = action.substring(0, i).trim();
        if ((req.getQueryString() == null || source.equals("") || req.getParameterValues("new") != null) &&
            req.getParameterValues("list") == null) {
            res.setContentType(Support.htmlType);
            PrintWriter writer = res.getWriter();
            writer.println("<html><head><title>" + req.getServerName() + "</title></head><body>");
            if (!mobile) writer.println("<h1>" + messages.getString("contentEngine") + "</h1>");
            writer.println("<form action=\"" + action + "\" method=get>");
            if (!mobile) {
                writer.println("<input type=reset value=\"" + messages.getString("defaults") + "\">");
                writer.println("<input name=new type=submit value=\"" + messages.getString("getNewForm") + "\">");
                writer.println("<input name=list type=submit value=\"" + messages.getString("listSubscriptions") + "\">");
                writer.println("<input name=signoff type=submit value=\"" + messages.getString("signoff") + "\">");
            }
            writer.println("<input type=submit value=\"" + messages.getString("subscribe") + "\">");
            writer.println("<table>");
            writer.println("<tr><th>" + messages.getString("sourceURL") + "</th><td><input name=source type=text value=\"" + source + "\" size=55></td></tr>");
            writer.println("<tr><th>" + messages.getString("targetURLs") + "</th><td><input name=targets type=text value=\"" + targets + "\" size=55></td></tr>");
            writer.println("<tr><th>" + messages.getString("timeStamps") + "</th><td><input name=stamps type=text value=\"" + stamps + "\" size=55></td></tr>");
            writer.println("<tr><th>" + messages.getString("logicBag") + "</th><td><input name=logicBag type=text value=\"" + logicBag + "\" size=55></td></tr>");
            writer.println("</table>");
            writer.println("</form>");
            writer.println(new Date() + "<br>");
            writer.println(messages.getString("timeStampOrders"));
            writer.println("</body></html>");
            return;
        }
        Date received = new Date();
        LdapContext contentEngineContext = null;
        try {
            contentEngineContext = W3Server.open(this.getClass().getName(), ldapContext);
            String remoteUser = req.getRemoteUser();
            if (remoteUser == null || (remoteUser = remoteUser.trim()).equals("")) throw new ServletException(messages.getString("noRemoteUserFound"));
            if (logicBag.equals("")) logicBag = null;
            StringTokenizer st = new StringTokenizer(targets, ",");
            Vector<String> targetVector = st.hasMoreTokens() ? new Vector<String>() : null;
            while (st.hasMoreTokens()) targetVector.addElement(st.nextToken().trim());
            Vector<Stamp> stampVector = new Vector<Stamp>();
            if (!stamps.equals("")) {
                st = new StringTokenizer(stamps, ",");
                while (st.hasMoreTokens()) stampVector.addElement(new Stamp(st.nextToken().trim()));
            } else stampVector.addElement(new Stamp(""));
            URL sourceURL = source.equals("") ? null : new URL(source);
            res.setContentType(Support.htmlType);
            PrintWriter writer = res.getWriter();
            if (req.getParameterValues("list") != null) {
                writer.println("<html><head><title>" + remoteUser + "</title></head><body>");
                if (!mobile) writer.println("<h1>" + MessageFormat.format(messages.getString("subscriptionsOfUser"), new Object[] {remoteUser}) + "</h1>");
                signoff(contentEngineContext, encoding, writer, action, remoteUser, sourceURL, targetVector, true);
                writer.println("</body></html>");
                return;
            }
            res.setContentType(Support.htmlType);
            if (req.getParameterValues("signoff") != null) {
                writer.println("<html><head><title>" + messages.getString("subscriptionCanceled") + "</title></head><body>");
                if (!mobile) writer.println("<h1>" + messages.getString("subscriptionCanceled") + "</h1>");
                signoff(contentEngineContext, encoding, writer, action, remoteUser, sourceURL, targetVector, false);
                writer.println("</body></html>");
            } else {
                subscribe(contentEngineContext, remoteUser, sourceURL, stampVector, targetVector, logicBag, received);
                writer.println("<html><head><title>" + source + "</title></head><body>");
                if (!mobile) writer.println("<h1>" + messages.getString("subscriptionStarted") + "</h1>");
                signoff(contentEngineContext, encoding, writer, action, remoteUser, sourceURL, targetVector, true);
                writer.println("</body></html>");
            }
        } catch (Exception ex) {
            throw new ServletException(Support.stackTrace(ex));
        } finally {
            W3Server.close(contentEngineContext, ldapContext);
        }
    }

    public void doHead(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        doGet(req, res);
    }

    public void destroy() {
        try {
            W3Server.close(contentEngineContext, ldapContext);
        } finally {
            if (cookieStore != null) cookieStore.close(null);
        }
    }

}
