
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;
import java.util.jar.JarFile;
import java.util.jar.JarEntry;
import java.util.zip.ZipEntry;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.SAXParser;
import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;

public class ServletTarget implements RegexpTarget, Cloneable {
    String remainder = null;
    String urlPattern = null;
    W3Servlet w3servlet = null;

    public ServletTarget() {
    }

    public ServletTarget(W3Servlet w3servlet, String urlPattern) {
        this.w3servlet = w3servlet;
        this.urlPattern = urlPattern;
    }

    public Object found(String remainder) {
        ServletTarget servletTarget = (ServletTarget)clone();
        servletTarget.remainder = remainder;
        return servletTarget;
    }

    public Object clone() {
        return new ServletTarget(w3servlet, urlPattern);
    }

    public String toString() {
        return getClass().getName() +
            "{servletClass=" + w3servlet.className +
            ",urlPattern=" + urlPattern + "}";
    }

}
