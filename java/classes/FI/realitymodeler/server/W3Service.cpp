
#include <io.h>
#include <direct.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <process.h>
#include <windows.h>
#include <string>

using namespace std;

#define EVENT handles[0]
#define PROGRAM handles[1]

#ifdef __GNUC__
unsigned int _CRT_fmode = _O_BINARY;
#endif

HANDLE handles[2];
SERVICE_STATUS ss;
SERVICE_STATUS_HANDLE ssh;
LPSTR name = TEXT("W3Service"), path;

LPSTR message(DWORD n)
{
    LPSTR msg = NULL;
    if (FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, NULL, n,
                      MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&msg, 0, NULL)) return msg;
    char str[32];
    _itoa(n, str, 10);
    LPSTR err = "error ";
    size_t l = strlen(err);
    if (!(msg = (LPSTR)LocalAlloc(LPTR, l + strlen(str) + 1))) return NULL;
    strcpy(msg, err);
    strcpy(msg + l, str);
    return msg;
}

void report(LPCSTR msg)
{
    if (!msg) return;
    LPCTSTR strs[] = {msg};
    HANDLE es = RegisterEventSource(NULL, name);
    if (!es) return;
    ReportEvent(es, EVENTLOG_ERROR_TYPE, 0, 0, NULL, 1, 0, strs, NULL);
    DeregisterEventSource(es);
}

VOID WINAPI W3ServiceHandler(DWORD control)
{
    if (control == SERVICE_CONTROL_STOP) {
        if (!SetEvent(EVENT)) report(message(GetLastError()));
        ss.dwCurrentState = SERVICE_STOP_PENDING;
        ss.dwControlsAccepted = 0;
        ss.dwCheckPoint = 0;
        ss.dwWaitHint = 5000;
        if (!SetServiceStatus(ssh, &ss)) report(message(GetLastError()));
        DWORD rv;
        for (;;) {
            if ((rv = WaitForSingleObjectEx(PROGRAM, ss.dwWaitHint, FALSE)) == WAIT_FAILED) {
                report(message(GetLastError()));
                break;
            }
            if (rv != WAIT_TIMEOUT) break;
            ss.dwCheckPoint++;
            if (!SetServiceStatus(ssh, &ss)) report(message(GetLastError()));
        }
        ss.dwCurrentState = SERVICE_STOPPED;
    }
    if (!SetServiceStatus(ssh, &ss)) report(message(GetLastError()));
}

void WINAPI W3Service(DWORD argc, LPTSTR *argv)
{
    ss.dwServiceType = SERVICE_WIN32_OWN_PROCESS;
    ss.dwCurrentState = SERVICE_START_PENDING;
    ss.dwControlsAccepted = 0;
    ss.dwWaitHint = 5000;
    try {
        if (!(ssh = RegisterServiceCtrlHandler(TEXT(argv[0]), W3ServiceHandler))) throw GetLastError();
        if (!SetServiceStatus(ssh, &ss)) report(message(GetLastError()));
        /*
          LPSTR cmdline = new char[strlen(path) + 80];
          strcpy(strrchr(strcpy(cmdline, path), '\\') + 1, "javaw.exe FI.realitymodeler.server.W3Server -e");
          STARTUPINFO startupInfo;
          memset(&startupInfo, 0, sizeof startupInfo);
          PROCESS_INFORMATION processInformation;
          memset(&processInformation, 0, sizeof processInformation);
          if (!CreateProcess(NULL, cmdline, NULL, NULL, TRUE, NORMAL_PRIORITY_CLASS, NULL, NULL, &startupInfo, &processInformation)) throw GetLastError();
          delete cmdline;
          PROGRAM = (HANDLE)processInformation.hProcess;
        */
        LPSTR dir = new char[strlen(path)];
        *strrchr(strcpy(dir, path), '\\') = '\0';
        *strrchr(dir, '\\') = '\0';
        if (_chdir(dir) == -1) throw (DWORD)-errno;
        char *envPath = getenv("PATH"),
            *envSystemRoot = getenv("SystemRoot"),
            *envJavaOpts = getenv("JAVA_OPTS");
        /*
          size_t size;
          if (getenv_s(&size, NULL, 0, "PATH")) throw GetLastError();
          envPath = new char[size];
          if (getenv_s(&size, envPath, size, "PATH")) throw GetLastError();
        */
        char *pathEnv = new char[strlen("PATH=") + strlen(dir) + strlen("\\bin;") + strlen(envPath) + 1];
        strcat(strcat(strcat(strcpy(pathEnv, "PATH="), dir), "\\bin;"), envPath);
        char *systemRootEnv = new char[strlen("SystemRoot=") + strlen(envSystemRoot) + 1];
        strcat(strcpy(systemRootEnv, "SystemRoot="), envSystemRoot);
        char *env[3];
        env[0] = pathEnv;
        env[1] = systemRootEnv;
        env[2] = NULL;
        if (!envJavaOpts) envJavaOpts = "";
        static char *javaOpts = new char[strlen(envJavaOpts) + 1];
        strcpy(javaOpts, envJavaOpts);
        /*
          struct _finddata_t finddata;
          intptr_t h = _findfirst(".\\lib\\*.*", &finddata);
          if (h == -1) throw GetLastError();
          string classpath;
          do {
          const char *fileName = finddata.name;
          if (*fileName == '.') continue;
          if (finddata.attrib & _A_SUBDIR) continue;
          classpath += string("./lib/") + string(fileName);
          } while (_findnext(h, &finddata) == 0);
          LPSTR cmdname = new char[strlen(path) + 80];
          strcpy(strrchr(strcpy(cmdname, path), '\\') + 1, "javaw.exe");
        */
        if ((PROGRAM = (HANDLE)_spawnlpe(_P_NOWAIT, "javaw.exe", "javaw.exe", javaOpts, "-jar", ".\\bin\\FI_realitymodeler_server.jar", ".", "-e", NULL, env)) == (HANDLE)-1) throw (DWORD)-errno;
        //delete cmdname;
        delete pathEnv;
        delete path;
        DWORD rv;
        for (;;) {
            if ((rv = WaitForMultipleObjectsEx(2, handles, FALSE, ss.dwWaitHint, FALSE)) == WAIT_FAILED) throw GetLastError();
            if (rv != WAIT_TIMEOUT) break;
            ss.dwCheckPoint++;
            if (!SetServiceStatus(ssh, &ss)) report(message(GetLastError()));
        }
        if (rv == WAIT_OBJECT_0 + 1) throw (DWORD)0;
        ss.dwCurrentState = SERVICE_RUNNING;
        ss.dwControlsAccepted = SERVICE_ACCEPT_STOP;
        if (!SetServiceStatus(ssh, &ss)) report(message(GetLastError()));
        int ts;
        if (_cwait(&ts, (intptr_t)PROGRAM, _WAIT_CHILD) == -1) throw (DWORD)-errno;
    }
    catch (DWORD n) {
        ss.dwCurrentState = SERVICE_STOPPED;
        ss.dwControlsAccepted = 0;
        if (n <= 0) {
            ss.dwWin32ExitCode = ERROR_SERVICE_SPECIFIC_ERROR;
            ss.dwServiceSpecificExitCode = -n;
            report(n == 0 ? "Startup failed (check KeppiProxyError)" : strerror(-n));
        }
        else {
            ss.dwWin32ExitCode = n;
            report(message(n));
        }
        if (!SetServiceStatus(ssh, &ss)) report(message(GetLastError()));
    }
}

int main(int argc, char *argv[])
{
    path = _fullpath(NULL, argv[0], 0);
    if (argc == 1) {
        SERVICE_TABLE_ENTRY st[] = {
            {name, W3Service},
            {NULL, NULL}};
        if (!(EVENT = CreateEvent(NULL, TRUE, FALSE, name)) ||
            !StartServiceCtrlDispatcher(st)) report(message(GetLastError()));
        return 0;
    }
    try {
        SC_HANDLE sc = OpenSCManager(NULL, NULL, SC_MANAGER_CREATE_SERVICE);
        if (!sc) throw GetLastError();
        if (!_stricmp(argv[1], "-install")) {
            LPSTR binaryPathName = new char[strlen(path) + 2 + 1];
            strcat(strcat(strcpy(binaryPathName, "\""), path), "\"");
            SC_HANDLE sh = CreateService(sc, name, name, SERVICE_QUERY_STATUS | SERVICE_START, SERVICE_WIN32_OWN_PROCESS,
                                         SERVICE_AUTO_START, SERVICE_ERROR_NORMAL, binaryPathName, NULL, NULL, NULL, NULL, NULL);
            if (!sh) throw GetLastError();
            CloseServiceHandle(sc);
            HKEY eventLogKey;
            DWORD disposition;
            if (RegCreateKeyEx(HKEY_LOCAL_MACHINE, "System\\CurrentControlSet\\Services\\EventLog\\Application\\W3Service",
                               0, "", 0, KEY_ALL_ACCESS, NULL, &eventLogKey, &disposition) != ERROR_SUCCESS) throw GetLastError();
            while (!StartService(sh, 0, NULL)) {
                DWORD n = GetLastError();
                if (n != ERROR_SERVICE_DATABASE_LOCKED) throw n;
                Sleep(2000);
            }
            DWORD checkPoint = 0;
            for (;;) {
                SERVICE_STATUS ss;
                if (!QueryServiceStatus(sh, &ss)) throw GetLastError();
                if (ss.dwCurrentState == SERVICE_RUNNING)
                    {
                        puts("Service started");
                        return 0;
                    }
                if (ss.dwCurrentState == SERVICE_STOPPED || checkPoint && checkPoint >= ss.dwCheckPoint)
                    {
                        puts("Failed to start service");
                        if (ss.dwWin32ExitCode == ERROR_SERVICE_SPECIFIC_ERROR)
                            printf("Service specific exit code %d\n", ss.dwServiceSpecificExitCode);
                        else printf("Exit code %d\n", ss.dwWin32ExitCode);
                        return -ss.dwWin32ExitCode;
                    }
                checkPoint = ss.dwCheckPoint;
                Sleep(ss.dwWaitHint);
            }
        }
        else if (!_stricmp(argv[1], "-remove")) {
            SC_HANDLE sh = OpenService(sc, name, SERVICE_STOP | STANDARD_RIGHTS_REQUIRED);
            if (!sh) throw GetLastError();
            CloseServiceHandle(sc);
            SERVICE_STATUS ss;
            ControlService(sh, SERVICE_CONTROL_STOP, &ss);
            if (!DeleteService(sh)) throw GetLastError();
            puts("Service removed");
        }
    }
    catch (DWORD n) {
        puts(message(n));
        return n;
    }
    return 0;
}
