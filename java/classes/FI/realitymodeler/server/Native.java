
package FI.realitymodeler.server;

import FI.realitymodeler.common.*;
import javax.servlet.*;

public class Native extends GenericServlet {
    static final long serialVersionUID = 0L;
    static {
        try {
            Support.loadLibrary("FI_realitymodeler_native");
            initialize();
        } catch (Exception ex) {
            throw new Error(ex.toString());
        }
    }

    String name;

    private long instance = 0;

    static private native void initialize();

    private native long construct();
    private native void init0(long instance, String name);
    private native void service0(long instance, W3Request w3r);
    private native void destroy0(long instance);

    public Native(String name) {
        this.name = name;
        instance = construct();
    }

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        init0(instance, name);
    }

    public void service(ServletRequest req, ServletResponse res) {
        service0(instance, (W3Request)req);
    }

    public void destroy() {
        if (instance != 0) destroy0(instance);
    }

    protected void finalize() {
        destroy();
    }

}
