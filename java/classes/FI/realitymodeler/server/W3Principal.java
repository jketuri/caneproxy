
package FI.realitymodeler.server;

import java.security.*;

public class W3Principal implements Principal {
    private String name = null;

    public W3Principal(String name) {
        this.name = name;
    }

    public boolean equals(Object another) {
        return name.equals(((W3Principal)another).getName());
    }

    public String toString() {
        return getClass().getName() + "{name=" + name + "}";
    }

    public int hasCode() {
        return name.hashCode();
    }

    public String getName() {
        return name;
    }

}

        
