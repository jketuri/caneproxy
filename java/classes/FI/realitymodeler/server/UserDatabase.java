
package FI.realitymodeler.server;

import FI.realitymodeler.sql.*;
import java.io.*;
import java.sql.*;
import java.util.*;

class UserDatabase {
    public static final int PASSWORD = 0, REALM = 1, NAME = 2;

    W3Server w3;
    ResultSet rs;
    DatabaseConnection dbc;
    PreparedStatement dbd, dbf, dbi, dbs, dbu;
    HashMap<String,UserRecord> users = new HashMap<String,UserRecord>();

    public void open(String name, W3Server w3) throws IOException, SQLException {
        this.w3 = w3;
        dbc = new DatabaseConnection(name);
        Statement s = dbc.createStatement();
        if (!dbc.created()) {
            s.execute("CREATE TABLE USERS (USERNAME CHAR(25),PASSWORD CHAR(25),REALM CHAR(25)," +
                      "NAME CHAR(50),PROFESSION CHAR(50),MOBILE CHAR(50),FAX CHAR(50),EMAIL CHAR(50)," +
                      "ADDRESS CHAR(50),USERGROUPS CHAR(50),AGE TINYINT,SEX BIT)");
            s.execute("CREATE UNIQUE INDEX USERS ON USERS (USERNAME)");
        }
        dbd = dbc.prepareStatement("DELETE FROM USERS\nWHERE USERNAME=?");
        dbf = dbc.prepareStatement("SELECT * FROM USERS\nWHERE USERNAME=?");
        dbi = dbc.prepareStatement("INSERT INTO USERS VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
        dbs = dbc.prepareStatement("SELECT * FROM USERS");
        dbu = dbc.prepareStatement("UPDATE USERS SET USERNAME=?,PASSWORD=?,REALM=?,NAME=?," +
                                   "PROFESSION=?,MOBILE=?,FAX=?,EMAIL=?,ADDRESS=?,USERGROUPS=?,AGE=?,SEX=?\nWHERE USERNAME=?");
        rs = s.executeQuery("SELECT USERNAME,PASSWORD,REALM,NAME FROM USERS");
        while (rs.next()) users.put(rs.getString(1), new UserRecord(rs.getString(2), rs.getString(3), rs.getString(4)));
        rs = dbs.executeQuery();
    }

    public void close() throws SQLException {
        dbc.close();
    }

    public UserRecord getRecord(String username) {
        return users.get(username);
    }

    public synchronized boolean saveUser(Object userFields[]) throws SQLException {
        for (int i = 0; i < 12; i++) dbu.setObject(i + 1, userFields[i]);
        dbu.setObject(13, userFields[0]);
        int n;
        if ((n = dbu.executeUpdate()) == 0) {
            for (int i = 0; i < 12; i++) dbi.setObject(i + 1, userFields[i]);
            n = dbi.executeUpdate();
        }
        if (n <= 0) return false;
        users.put((String)userFields[0], new UserRecord((String)userFields[1], (String)userFields[2], (String)userFields[3]));
        return true;
    }

    public synchronized boolean removeUser(String username) throws SQLException {
        dbd.setString(1, username);
        int n = dbd.executeUpdate();
        if (n <= 0) return false;
        users.remove(username);
        return true;
    }

    private Object[] getUserFields(ResultSet rs) throws SQLException {
        if (!rs.next()) return null;
        Object userFields[] = new Object[12];
        for (int i = 0; i < 12; i++) userFields[i] = rs.getObject(i + 1);
        return userFields;
    }

    public Object[] searchUser(String username) throws SQLException {
        dbf.setString(1, username);
        return getUserFields(dbf.executeQuery());
    }

    public Object[] nextUser() throws SQLException {
        return getUserFields(rs);
    }

    public Object[] firstUser() throws SQLException {
        return getUserFields(rs = dbs.executeQuery());
    }

}
