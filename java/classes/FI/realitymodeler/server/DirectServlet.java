
package FI.realitymodeler.server;

import FI.realitymodeler.common.*;
import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class DirectServlet extends HttpServlet {
    static final long serialVersionUID = 0L;

    public void service(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException {
        ServletContext servletContext = getServletContext();
        String pathInfo = req.getPathInfo();
        if (pathInfo == null) throw new ServletException("No servlet name specified");
        int slash = pathInfo.indexOf('/', 1);
        String servletName;
        if (slash != -1) {
            servletName = pathInfo.substring(1, slash);
            pathInfo = pathInfo.substring(slash);
        } else {
            servletName = pathInfo.substring(1);
            pathInfo = null;
        }
        W3RequestDispatcher w3requestDispatcher = (W3RequestDispatcher)servletContext.getNamedDispatcher(servletName);
        if (w3requestDispatcher == null) {
            Exception ex = null;
            try {
                ((W3Context)servletContext).w3server.loadServlet(servletName, "file:///" + ((W3Context)servletContext).w3server.dataDirectory + "/" + servletName + ".class", null, false, false);
            } catch (ClassNotFoundException ex1) {
                ex = ex1;
            } catch (IllegalAccessException ex1) {
                ex = ex1;
            } catch (InstantiationException ex1) {
                ex = ex1;
            } catch (RegexException ex1) {
                ex = ex1;
            }
            if (ex != null) throw new ServletException(ex.toString(), ex);
            w3requestDispatcher = (W3RequestDispatcher)servletContext.getNamedDispatcher(servletName);
        }
        if (w3requestDispatcher == null) throw new ServletException("Servlet " + servletName + " not found");
        String servletPath = req.getRequestURI();
        if (pathInfo != null) servletPath = servletPath.substring(0, servletPath.length() - pathInfo.length());
        w3requestDispatcher.requestUri = req.getRequestURI();
        w3requestDispatcher.contextPath = req.getContextPath();
        w3requestDispatcher.servletPath = servletPath;
        w3requestDispatcher.pathInfo = pathInfo;
        w3requestDispatcher.queryString = req.getQueryString();
        w3requestDispatcher.forward(req, res);
    }

}
