
package FI.realitymodeler.server;

import java.util.*;
import javax.servlet.*;

/** Authentication domain */
public class Domain {
    String name, secondName;
    W3Servlet auth;
    Vector paths;

    public Domain(String name, Vector paths, W3Servlet auth, String secondName) {
        this.name = name;
        this.paths = paths;
        this.auth = auth;
        this.secondName = secondName;
    }

}
