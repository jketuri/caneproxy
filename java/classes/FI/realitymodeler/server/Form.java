
package FI.realitymodeler.server;

import java.io.*;
import java.net.*;
import java.util.*;

public class Form extends Link {
    public static final int URLENCODED, FORM_DATA;
    static Vector<String> encodingTypes = new Vector<String>();
    static {
        encodingTypes.addElement("application/x-www-form-urlencoded");
        encodingTypes.addElement("multipart/form-data");
        int i = 0;
        URLENCODED = i++;
        FORM_DATA = i++;
    }

    String method;
    Vector<Object> fields = new Vector<Object>();
    byte encTypeCode;

    public Form() {
    }

    public Form(String url, String method, String encType) throws IOException {
        super(url);
        this.method = method.trim().toUpperCase();
        if (encType != null && (encTypeCode = (byte)encodingTypes.indexOf(encType.trim().toLowerCase())) == -1)
            throw new IOException("Unsupported encoding type " + encType);
        else encTypeCode = 0;
    }

    public Form(URL url, String method, String encType) throws IOException {
        this(url.toString(), method, encType);
    }

    public String toString() {
        return "{" + method + " " + url + " " + fields + "}";
    }

    public String fieldsToCoded() throws UnsupportedEncodingException {
        StringBuffer sb = new StringBuffer();
        Iterator iter = fields.iterator();
        while (iter.hasNext()) {
            Field field = (Field)iter.next();
            if (sb.length() > 0) sb.append('&');
            sb.append(field.toCoded());
        }
        return sb.toString();
    }

    public Vector<Object> fieldsFromCoded(String codedFields) {
        StringTokenizer st = new StringTokenizer(codedFields, "&");
        while (st.hasMoreTokens()) fields.addElement(new Field(st.nextToken()));
        return fields;
    }

    public String getMethod() {
        return method;
    }

    public byte getEncTypeCode() {
        return encTypeCode;
    }

    public String getEncType() {
        return encodingTypes.elementAt(encTypeCode);
    }

    public Vector<Object> getFields() {
        return fields;
    }

    public void addField(Field field) {
        fields.addElement(field);
    }

}
