
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.text.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Common Gateway Interface servlet. */
public class CGIServlet extends HttpServlet {
    static final long serialVersionUID = 0L;

    private String scriptDirectory = null;
    private String scriptProgram = null;

    class InputThread extends Thread {
        InputStream in;
        OutputStream to;

        InputThread(InputStream in, OutputStream to) {
            this.in = in;
            this.to = to;
        }

        public void run() {
            byte b[] = new byte[Support.bufferLength];
            try {
                for (int n; (n = in.read(b)) > 0;) {
                    to.write(b, 0, n);
                    to.flush();
                }
            } catch (IOException ex) {} finally {
                try {
                    to.close();
                } catch (IOException ex) {}
            }
        }

    }

    public void init(ServletConfig config)
        throws ServletException {
        super.init(config);
        scriptDirectory = getInitParameter("scriptDirectory");
        scriptProgram = getInitParameter("scriptProgram");
    }

    public void service(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException {
        String path = req.getPathInfo();
        if (path == null) path = "";
        StringTokenizer st = new StringTokenizer(path, "/");
        String cmda[] = new String[st.countTokens() + (scriptDirectory != null && scriptProgram != null ? 1 : 0)],
            script = st.hasMoreTokens() ? st.nextToken().trim() : null;
        StringBuffer programPath = new StringBuffer();
        int index = 0;
        if (script != null) {
            String scriptName = null;
            if (scriptDirectory != null) {
                if (scriptProgram != null) {
                    cmda[index++] = scriptProgram;
                    programPath.append(scriptProgram).append(' ');
                }
                scriptName = new File(scriptDirectory, new File(script).getName()).getCanonicalPath();
            } else scriptName = getInitParameter("script_" + script);
            cmda[index++] = scriptName;
            programPath.append(scriptName);
        } else throw new FileNotFoundException("CGI-program not found");
        while (index < cmda.length) {
            String parameter = st.nextToken().trim();
            programPath.append(' ').append(parameter);
            cmda[index++] = parameter;
        }
        log("Running CGI-program " + programPath.toString());
        Vector<String> envVec = new Vector<String>();
        envVec.addElement("SERVER_SOFTWARE=" + getServletInfo());
        envVec.addElement("SERVER_NAME=" + req.getServerName());
        envVec.addElement("GATEWAY_INTERFACE=CGI/1.1");
        envVec.addElement("SERVER_PROTOCOL=" + req.getProtocol());
        envVec.addElement("SERVER_PORT=" + req.getServerPort());
        envVec.addElement("REQUEST_METHOD=" + req.getMethod());
        envVec.addElement("PATH_INFO=" + req.getPathInfo());
        envVec.addElement("PATH_TRANSLATED=" + req.getPathTranslated());
        envVec.addElement("SCRIPT_NAME=" + req.getServletPath());
        envVec.addElement("QUERY_STRING=" + req.getQueryString());
        envVec.addElement("REMOTE_HOST=" + req.getRemoteHost());
        envVec.addElement("REMOTE_ADDR=" + req.getRemoteAddr());
        envVec.addElement("AUTH_TYPE=" + req.getAuthType());
        envVec.addElement("REMOTE_USER=" + req.getRemoteUser());
        envVec.addElement("CONTENT_TYPE=" + req.getContentType());
        envVec.addElement("CONTENT_LENGTH=" + req.getContentLength());
        String s;
        Enumeration e = req.getHeaderNames();
        while (e.hasMoreElements())
            envVec.addElement("HTTP_" +
                              (s = (String)e.nextElement()).replace('-','_') + "=" + req.getHeader(s));
        Map<String, String> envMap = System.getenv();
        String env[] = new String[envVec.size() + envMap.size()];
        int envIndex = 0, envSize = envVec.size();
        while (envIndex < envSize) {
            env[envIndex] = envVec.elementAt(envIndex);
            envIndex++;
        }
        Iterator<Map.Entry<String, String>> envEntries
            = envMap.entrySet().iterator();
        while (envEntries.hasNext()) {
            Map.Entry envEntry = envEntries.next();
            env[envIndex++] = envEntry.getKey() + "=" + envEntry.getValue();
        }
        Process pr = Runtime.getRuntime().exec(cmda, env);
        OutputStream to = pr.getOutputStream();
        String method = req.getMethod().toUpperCase();
        if (method.equals("POST") || method.equals("PUT"))
            new InputThread(req.getInputStream(), to).start();
        else to.close();
        InputStream from = pr.getInputStream();
        HeaderList headerList;
        try {
            headerList = new HeaderList(from);
        } catch (ParseException ex) {
            throw new ServletException(ex.toString());
        }
        if (!script.startsWith("nph-")) {
            Iterator headerItems = headerList.iterator();
            while (headerItems.hasNext()) {
                Header header = (Header)headerItems.next();
                String name = header.getName(), value = header.getValue(), comp = name.toLowerCase();
                if (comp.equals("location")) {
                    if (value.indexOf(':') == -1) {
                        RequestDispatcher requestDispatcher = req.getRequestDispatcher(value);
                        requestDispatcher.include(req, res);
                    } else res.sendRedirect(value);
                    return;
                } else if (comp.equals("status")) {
                    st = new StringTokenizer(value);
                    res.setStatus(Integer.parseInt(st.nextToken()));
                } else res.setHeader(name, value);
            }
        } else {
            Iterator headerItems = headerList.iterator();
            if (headerItems.hasNext()) {
                String status = ((Header)headerItems.next()).getValue();
                if (status != null) {
                    st = new StringTokenizer(status);
                    st.nextToken();
                    res.setStatus(Integer.parseInt(st.nextToken()));
                } while (headerItems.hasNext()) {
                    Header header = (Header)headerItems.next();
                    res.setHeader(header.getName(), header.getValue());
                }
            }
        }
        byte b[] = new byte[Support.bufferLength];
        int n;
        try {
            OutputStream out = res.getOutputStream();
            while ((n = from.read(b)) > 0) out.write(b, 0, n);
        } finally {
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            InputStream error = pr.getErrorStream();
            if (error != null) {
                while ((n = error.read(b)) > 0) bout.write(b, 0, n);
                try {
                    int exitValue = pr.waitFor();
                    if (exitValue != 0)
                        log(new String(bout.toByteArray(), "8859_1"), new IOException("CGI error exit value " + exitValue));
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
            }
        }
    }

}