
package FI.realitymodeler.server;

import java.util.*;

public class UserRecord {
    String password;
    Vector<String> domains;
    String name;

    public UserRecord(String password, String domainList, String name) {
        this.password = password;
        domains = new Vector<String>();
        StringTokenizer st = new StringTokenizer(domainList, ",");
        while (st.hasMoreTokens()) domains.addElement(st.nextToken().trim());
        this.name = name;
    }

}
