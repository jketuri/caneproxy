
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Searches fragments from HTML-stream with keywords. Following parameters can be given in request content type
    with format: filter/search; name=value; name=value; ...:<br>
    header = string which is printed to output stream before other data (if specified)<br>
    trailer = string which is printed to output stream after other data (if specified)<br>
    start = string which marks start of required fragment<br>
    end = string which marks end of required fragment<br>
    search = all fragments containing one of these strings (separated with comma) are printed to output stream (if specified)

*/
public class SearchFilter extends HttpServlet {
    static final long serialVersionUID = 0L;

    class SearchOutputStream extends ConvertOutputStream {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        OutputStream out1;
        String searchStrings[];
        String start, end;

        SearchOutputStream(OutputStream out, String searchStrings[], String start, String end, String charsetName) {
            super(out, start, start);
            setCharsetName(charsetName);
            this.searchStrings = searchStrings;
            this.start = start;
            this.end = end;
            out1 = out;
        }

        public String convert(String scan, String replace) throws IOException {
            if (this.scan == start) {
                this.scan = end;
                out = bout;
                return "";
            }
            this.scan = start;
            out = out1;
            byte bytes[] = bout.toByteArray();
            String fragment = new String(bytes, charsetName);
            InputStream bin = new ByteArrayInputStream(bytes);
            bout.reset();
            Support.filter(bin, bout);
            String fragmentString = new String(bout.toByteArray(), charsetName).toLowerCase();
            for (int i = 0; i < searchStrings.length; i++)
                if (fragmentString.indexOf(searchStrings[i]) != -1) {
                    Support.writeBytes(out, start, charsetName);
                    Support.writeBytes(out, fragment, charsetName);
                    Support.writeBytes(out, end, charsetName);
                    out.flush();
                    break;
                }
            bout.reset();
            return "";
        }

    }

    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String charset = req.getCharacterEncoding(),
            charsetName = charset != null ? Support.getCharacterEncoding(charset) : null;
        if (charsetName == null) charsetName = "8859_1";
        W3Request w3r = (W3Request)req;
        Map<String,String> parameters = new HashMap<String,String>();
        String ct = req.getHeader("content-type");
        Support.getParameters(ct, parameters);
        InputStream in = req.getInputStream();
        res.setContentType(Support.htmlType + "; charset=" + charset);
        ServletOutputStream sout = res.getOutputStream();
        String header = parameters.get("header"), trailer = parameters.get("trailer"),
            start = parameters.get("start"), end = parameters.get("end"), search = parameters.get("search");
        StringTokenizer st = new StringTokenizer(search, ",");
        String searchStrings[] = new String[st.countTokens()];
        for (int i = 0; i < searchStrings.length; i++) searchStrings[i] = st.nextToken().toLowerCase();
        if (header != null) sout.print(header);
        SearchOutputStream out = new SearchOutputStream(sout, searchStrings, start, end, charsetName);
        byte b[] = new byte[Support.bufferLength];
        for (int n; (n = in.read(b)) > 0;) out.write(b, 0, n);
        if (trailer != null) sout.print(trailer);
    }

}
