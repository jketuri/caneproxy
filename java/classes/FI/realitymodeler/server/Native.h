
/* Native servlet header */

#include <jni.h>

#ifndef _WIN32
#ifndef __int64
#define __int64 int64_t
#endif
#endif

typedef const char *STRING;

class Native;
class Context;

typedef void (*INIT)(Native *native);
typedef void (*SERVICE)(Context *context);
typedef void (*DESTROY)(Native *native);

typedef struct ENTRY
{
	jstring jkey;
	const char *key;
	union
	{
		jstring jvalue;
		jstring *jvalues;
	} jvalue;
	union
	{
		const char *value;
		const char **values;
	} value;
	int n_values;
	struct ENTRY *next_entry;
} ENTRY;

class Context
{
public:
	JNIEnv *env;
	jobject obj, nativeObj, in, out;
	jbyteArray buffer;
	jbyte *buf;
	int len;
	ENTRY *parameters, *header;
	jint num, size;
	jstring jcontentType, jprotocol, jscheme, jserverName, jremoteAddr, jremoteHost,
	jmethod, jrequestURI, jservletPath, jpathInfo, jpathTranslated,
	jqueryString, jauthType;
	const char *contentType, *protocol, *scheme, *serverName, *remoteAddr, *remoteHost,
	*method, *requestURI, *servletPath, *pathInfo, *pathTranslated,
	*queryString, *authType;

JNIEXPORT const char *getInitParameter(char *);
JNIEXPORT void log(char *);
JNIEXPORT const char *getAttribute(char *name);
JNIEXPORT const char *getAttributeNames();
JNIEXPORT const char *getCharacterEncoding();
JNIEXPORT long getContentLength();
JNIEXPORT const char *getContentType();
JNIEXPORT const char *getParameter(char *name);
JNIEXPORT const ENTRY *getParameterNames();
JNIEXPORT const ENTRY *getParameterValues(char *name);
JNIEXPORT const char *getProtocol();
JNIEXPORT const char *getScheme();
JNIEXPORT const char *getServerName();
JNIEXPORT int getServerPort();
JNIEXPORT const char *getRemoteAddr();
JNIEXPORT const char *getRemoteHost();
JNIEXPORT void setAttribute(char *name, char *value);
JNIEXPORT void removeAttribute(char *name);
JNIEXPORT int isSecure();
JNIEXPORT const char *getAuthType();
JNIEXPORT const char *getCookies();
JNIEXPORT __int64 getDateHeader(char *, __int64);
JNIEXPORT const char *getHeader(char *);
JNIEXPORT const char *getHeaders(char *);
JNIEXPORT const char *getHeaderNames();
JNIEXPORT long getIntHeader(char *, long);
JNIEXPORT const char *getMethod();
JNIEXPORT const char *getPathInfo();
JNIEXPORT const char *getPathTranslated();
JNIEXPORT const char *getContextPath();
JNIEXPORT const char *getQueryString();
JNIEXPORT const char *getRemoteUser();
JNIEXPORT int isUserInRole(char *role);
JNIEXPORT char *getUserPrincipal();
JNIEXPORT char *getRequestedSessionId();
JNIEXPORT const char *getRequestURI();
JNIEXPORT const char *getServletPath();
JNIEXPORT const char *getSession();
JNIEXPORT void setContentLength(long);
JNIEXPORT void setContentType(char *);
JNIEXPORT void setBufferSize(long size);
JNIEXPORT long getBufferSize();
JNIEXPORT const void flushBuffer();
JNIEXPORT const int isCommitted();
JNIEXPORT const void reset();
JNIEXPORT const void addCookie(char *name, char *value);
JNIEXPORT const int containsHeader(char *name);
JNIEXPORT const char *encodeURL(char *url);
JNIEXPORT const char *encodeRedirectURL(char *url);
JNIEXPORT void sendError(long, char *);
JNIEXPORT void sendRedirect(char *);
JNIEXPORT void setDateHeader(char *, __int64);
JNIEXPORT void addDateHeader(char *, __int64);
JNIEXPORT void setHeader(char *, char *);
JNIEXPORT void addHeader(char *, char *);
JNIEXPORT void setIntHeader(char *, long);
JNIEXPORT void addIntHeader(char *, long);
JNIEXPORT void setStatus(long, char *);
JNIEXPORT const char *getHeaderName(long);
JNIEXPORT const char *getHeaderByNumber(long);
JNIEXPORT int read(int off, int len);
JNIEXPORT void write(int off, int len);
JNIEXPORT const char *getRealPath(char *);
JNIEXPORT void unsetHeader(char *);

};

class Native
{

public:
	JNIEnv *env;
	jobject obj;
#ifdef _WIN32
	HINSTANCE hlib;
#endif
	INIT init;
	SERVICE service;
	DESTROY destroy;
	ENTRY *parameters;
	jint num;

JNIEXPORT const char *getInitParameter(char *);
JNIEXPORT void log(char *);

};
