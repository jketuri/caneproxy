
package FI.realitymodeler.server;

import javax.naming.*;

public class BinderBean {
    private String name = null;
    private Object value = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        checkBind();
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
        checkBind();
    }

    private void checkBind() {
        if (name == null || value == null) return;
        try {
            Context context = new InitialContext();
            context.rebind("java:comp/env/" + name, value);
        } catch (NamingException ex) {
            throw new RuntimeException(ex);
        }
    }

}
