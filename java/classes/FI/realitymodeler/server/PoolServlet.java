
package FI.realitymodeler.server;

import FI.realitymodeler.common.*;
import java.io.*;
import java.util.*;
import javax.servlet.*;

public class PoolServlet implements Servlet {
    Object servicingFlag = new Object();
    Servlet servlet;
    WeakHashMap<Envelope,Boolean> envelopePool = new WeakHashMap<Envelope,Boolean>();
    boolean servicing = false;

    class Envelope {
        Servlet servlet;
        boolean destroyed = false;
        Object destroyedFlag = new Object();

        Envelope(Servlet servlet) {
            this.servlet = servlet;
        }

        void destroy() {
            synchronized (destroyedFlag) {
                if (destroyed) return;
                destroyed = true;
            }
            servlet.destroy();
        }

        protected void finalize() {
            destroy();
        }

    }

    public PoolServlet(Servlet servlet) {
        this.servlet = servlet;
    }

    public void init(ServletConfig config) throws ServletException {
        servlet.init(config);
    }

    public ServletConfig getServletConfig() {
        return servlet.getServletConfig();
    }

    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        boolean isServicing;
        synchronized (servicingFlag) {
            if (!(isServicing = servicing)) servicing = true;
        }
        if (!isServicing) {
            try {
                servlet.service(req, res);
            } finally {
                servicing = false;
            }
            return;
        }
        Envelope envelope;
        try {
            synchronized (envelopePool) {
                envelopePool.remove(envelope = envelopePool.keySet().iterator().next());
            }
        } catch (NoSuchElementException ex0) {
            Exception ex = null;
            Servlet envelopeServlet = null;
            try {
                envelopeServlet = (Servlet)Class.forName(servlet.getClass().getName()).newInstance();
            } catch (ClassNotFoundException ex1) {
                ex = ex1;
            } catch (InstantiationException ex1) {
                ex = ex1;
            } catch (IllegalAccessException ex1) {
                ex = ex1;
            }
            if (ex != null) throw new ServletException(Support.stackTrace(ex), ex);
            envelopeServlet.init(servlet.getServletConfig());
            envelope = new Envelope(envelopeServlet);
        }
        try {
            envelope.servlet.service(req, res);
        } finally {
            envelopePool.put(envelope, Boolean.TRUE);
        }
    }

    public String getServletInfo() {
        return servlet.getServletInfo();
    }

    public void destroy() {
        try {
            Iterator envelopeIterator = envelopePool.keySet().iterator();
            for (;;) ((Envelope)envelopeIterator.next()).destroy();
        } catch (NoSuchElementException ex) {} finally {
            servlet.destroy();
        }
    }

}
