
package FI.realitymodeler.server;

import FI.realitymodeler.common.*;
import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Basic authentication servlet.
 */
public class BasicServlet extends HttpServlet {
    static final long serialVersionUID = 0L;

    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        W3Request w3request = (W3Request)req.getAttribute("");
        try {
            StringTokenizer st;
            if (w3request.authorization != null) {
                st = new StringTokenizer(w3request.authorization);
                if (!st.hasMoreTokens() || !st.nextToken().equalsIgnoreCase("basic") || !st.hasMoreTokens())
                    throw new ServletException("Bad basic header");
                st = new StringTokenizer(new String(new BASE64Decoder().decodeStream(st.nextToken())), ":");
            } else if (w3request.url == null || w3request.url.getUserInfo() == null) throw new ServletException("Authorization required");
            else st = new StringTokenizer(w3request.url.getUserInfo(), ":");
            if (!st.hasMoreTokens()) throw new ServletException("Bad credentials");
            w3request.remoteUser = st.nextToken();
            if (w3request.remoteUser.trim().startsWith("+")) throw new ServletException("Bad username");
            w3request.userPrincipal = new W3Principal(w3request.remoteUser);
            String password = st.hasMoreTokens() ? st.nextToken("").substring(1) : "";
            if (!w3request.domain.name.equals("native")) {
                UserRecord user = w3request.w3server.userDatabase.getRecord(w3request.remoteUser);
                if (user != null && user.domains.contains(w3request.domain.name) && user.password.equals(password)) {
                    w3request.accountName = user.name;
                    return;
                }
            } else if ((w3request.accountName = W3Server.checkPassword(w3request.remoteUser, w3request.w3server.nativeDomain, password)) != null) return;
            throw new ServletException("Basic authorization failed");
        } catch (ServletException ex) {
            w3request.challenge = "Basic realm=\"" + w3request.domain.name + "\"";
            throw ex;
        }
    }

}
