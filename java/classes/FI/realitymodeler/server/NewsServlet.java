
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Network news gateway servlet.<br>
    With PUT-method path info must be of form:<br>
    /groups/subject/from<br>
    With GET-method:<br>
    /host/group<br>
    These may also be in query parameters host and group.<br>
    If path info is empty or new-parameter is specified, servlet returns general news form.<br>
    If form-parameter is specified, servlet returns posting form. */
public class NewsServlet extends HttpServlet {
    static final long serialVersionUID = 0L;

    Map<String, String> servletPathTable = null;
    ResourceBundle messages;

    public void init(ServletConfig config)
        throws ServletException {
        super.init(config);
        messages = W3URLConnection.getMessages("FI.realitymodeler.server.resources.Messages");
        servletPathTable = (Map<String, String>)config.getServletContext().getAttribute("FI.realitymodeler.server.W3Server/servletPathTable");
    }

    public void doPost(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException {
        try {
            Messaging.post(this, req, res, messages, "nntp");
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }

    public void doPut(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException {
        Messaging.put(this, req, res, null);
    }

    public void doGet(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException {
        try {
            Messaging.get(this, req, res, messages, "news", W3NntpURLConnection.nntpServerHost, servletPathTable);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }

    public void doHead(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException {
        doGet(req, res);
    }

}
