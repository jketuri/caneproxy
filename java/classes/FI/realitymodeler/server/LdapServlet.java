
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Lightweight Directory Access Protocol gateway servlet. Urls must be
    of form:<br>

    /host/dn?attributes?scope?filter?extensions<br>
    ?count=count limit of returned results.<br>

    These may also be in query parameters with same names. When these are
    given in initialization parameters they are used as default values and
    possibly overwritten by query parameters.  If path info is empty or
    new-parameter is specified, servlet returns general LDAP form.
*/
public class LdapServlet extends HttpServlet {
    static final long serialVersionUID = 0L;

    Map<String, String> servletPathTable = null;
    ResourceBundle messages;
    String defaultHost = null, defaultDnPath = null, defaultAttributes = null, defaultScope = null, defaultFilter = null, defaultExtensions = null;
    long defaultCount = 0L;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        messages = W3URLConnection.getMessages("FI.realitymodeler.server.resources.Messages");
        servletPathTable = (Map<String, String>)config.getServletContext().getAttribute("FI.realitymodeler.server.W3Server/servletPathTable");
        if ((defaultHost = config.getInitParameter("host")) == null) defaultHost = "";
        if ((defaultDnPath = config.getInitParameter("dn")) == null) defaultDnPath = "";
        if ((defaultAttributes  = config.getInitParameter("attributes")) == null) defaultAttributes = "";
        if ((defaultScope = config.getInitParameter("scope")) == null) defaultScope = "";
        if ((defaultFilter = config.getInitParameter("filter")) == null) defaultFilter = "";
        if ((defaultExtensions = config.getInitParameter("extensions")) == null) defaultExtensions = "";
        String s;
        if ((s = config.getInitParameter("count")) != null) defaultCount = Long.parseLong(s.trim());
    }

    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        if (req.getParameterValues("empty") != null) {
            Messaging.getEmpty(res);
            return;
        }
        String action, root = "", path = req.getPathInfo(), s,
            host = (s = Support.getParameterValue(req.getParameterValues("host"))) != null && !(s = s.trim()).equals("") ? s : defaultHost,
            dnPath = (s = Support.getParameterValue(req.getParameterValues("dn"))) != null && !(s = s.trim()).equals("") ? s : defaultDnPath,
            attributes = (s = Support.getParameterValue(req.getParameterValues("attributes"))) != null ? s.trim() : defaultAttributes,
            scope = (s = Support.getParameterValue(req.getParameterValues("scope"))) != null ? s.trim() : defaultScope,
            filter = (s = Support.getParameterValue(req.getParameterValues("filter"))) != null ? s.trim() : defaultFilter,
            extensions = (s = Support.getParameterValue(req.getParameterValues("extensions"))) != null ? s.trim() : defaultExtensions;
        if (path == null) path = "";
        long count = (s = Support.getParameterValue(req.getParameterValues("count"))) != null ? Long.parseLong(s.trim()) : 0L;
        boolean mobile = (s = req.getHeader("user-agent")) != null && s.trim().equalsIgnoreCase("mobile") ||
            req.getParameterValues("mobile") != null;
        int tc = new StringTokenizer(path, "/").countTokens();
        if (tc == 0) root += Support.encodePath(host) + "/";
        if (tc <= 1) root += Support.encodePath(dnPath) + "/";
        int i = (action = req.getRequestURI()).indexOf('?');
        if (i != -1) action = action.substring(0, i).trim();
        if (!root.equals("") && !path.endsWith("/")) path += "/";
        path += root;
        if (!action.endsWith("/")) root = action.substring(action.lastIndexOf('/') + 1) + "/" + root;
        if (tc < 2 && (dnPath.equals("") || req.getParameterValues("ask") != null) &&
            req.getParameterValues("go") == null || req.getParameterValues("new") != null) {
            res.setContentType(Support.htmlType);
            PrintWriter writer = res.getWriter();
            writer.println("<html><head><title>" + (!dnPath.equals("") ? dnPath : !host.equals("") ? host : Support.htmlString(messages.getString("ldap"))) + "</title></head><body>");
            writer.println("<h1>" + Support.htmlString(messages.getString("ldap")) + "</h1>");
            writer.println("<form action=\"" + action + "\" method=get>");
            writer.println("<input name=go type=hidden>");
            if (!mobile) {
                writer.println("<input name=frames type=checkbox" +
                               (req.getParameterValues("frames") != null ? " checked" : "") + ">" +
                               Support.htmlString(messages.getString("useFrames")) + "<br>");
                writer.println("<input name=scope type=radio value=base" + (scope.equals("base") ? " checked" : "") + ">" + Support.htmlString(messages.getString("baseObject")));
                writer.println("<input name=scope type=radio value=one" + (scope.equals("one") ? " checked" : "") + ">" + Support.htmlString(messages.getString("oneLevel")));
                writer.println("<input name=scope type=radio value=sub" + (scope.equals("sub") ? " checked" : "") + ">" + Support.htmlString(messages.getString("subTree")) + "<br>");
                writer.println("<input type=reset value=\"" + Support.htmlString(messages.getString("defaults")) + "\">");
                writer.println("<input name=new type=submit value=\"" + Support.htmlString(messages.getString("getNewForm")) + "\">");
            }
            writer.println("<input type=submit value=\"" + Support.htmlString(messages.getString("getResults")) + "\">");
            writer.println("<table>");
            if (!mobile || host.equals(""))
                writer.println("<tr><th>" + Support.htmlString(messages.getString("host")) + "</th><td>" +
                               (tc == 0 ? "<input name=host type=text value=\"" + Support.htmlString(host) + "\" size=55>" : host) + "</td></tr>");
            if (!mobile || dnPath.equals(""))
                writer.println("<tr><th>" + Support.htmlString(messages.getString("distinguishedName")) + "</th><td>" +
                               (tc <= 1 ? "<input name=dn type=text value=\"" + Support.htmlString(dnPath) + "\" size=55>" : dnPath) + "</td></tr>");
            writer.println("<tr><th>" + Support.htmlString(messages.getString("attributes")) + "</th><td><input name=attributes type=text value=\"" + Support.htmlString(attributes) + "\" size=55></td></tr>");
            writer.println("<tr><th>" + Support.htmlString(messages.getString("filter")) + "</th><td><input name=filter type=text value=\"" + Support.htmlString(filter) + "\" size=55></td></tr>");
            writer.println("<tr><th>" + Support.htmlString(messages.getString("extensions")) + "</th><td><input name=extensions type=text value=\"" + Support.htmlString(extensions) + "\" size=55></td></tr>");
            writer.println("<tr><th>" + Support.htmlString(messages.getString("resultCountLimit")) + "</th><td><input name=count type=text value=\"" + count + "\" size=55></td></tr>");
            writer.println("</table>");
            writer.println("</form></body></html>");
            return;
        }
        if (req.getParameterValues("frames") != null && req.getParameterValues("done") == null) {
            Messaging.getFrames(req, res, action, !dnPath.equals("") ? dnPath : !host.equals("") ? host : messages.getString("ldap"), messages);
            return;
        }
        byte b[];
        if (req instanceof W3Request) b = ((W3Request)req).buffer;
        else b = new byte[Support.bufferLength];
        s = req.getQueryString();
        W3URLConnection uc = null;
        try {
            uc = new W3LdapURLConnection(new URL("ldap:/" + path + "?" +
                                                  (tc < 2 || s == null ? attributes + "?" + scope + "?" + filter + "?" + extensions + (count > 0L ? "?count=" + count : "") : s)));
            uc.setRequestRoot(root);
            uc.setServletPathTable(servletPathTable);
            uc.setServletContext(getServletConfig().getServletContext());
            Messaging.get(req, res, uc, null, null, false);
        } finally {
            if (uc != null) uc.disconnect();
        }
    }

    public void doHead(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        doGet(req, res);
    }

}
