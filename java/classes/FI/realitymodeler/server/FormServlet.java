
package FI.realitymodeler.server;

import FI.realitymodeler.common.*;
import java.io.*;
import java.util.*;
import java.security.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Form authentication servlet.
 */
public class FormServlet extends HttpServlet {
    static final long serialVersionUID = 0L;

    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        W3Context w3context = (W3Context)getServletContext();
        W3Request w3request = (W3Request)req.getAttribute("");
        try {
            w3request.remoteUser = req.getParameter("j_username");
            if (w3request.remoteUser.trim().startsWith("+")) throw new ServletException("Bad username");
            String password = req.getParameter("j_password");
            if (w3context.realmObject == null) {
                boolean failed = false;
                if (!w3request.domain.name.equals("native")) {
                    UserRecord user = w3request.w3server.userDatabase.getRecord(w3request.remoteUser);
                    if (user != null && user.domains.contains(w3request.domain.name) && user.password.equals(password)) w3request.accountName = user.name;
                    else w3request.accountName = null;
                } else w3request.accountName = W3Server.checkPassword(w3request.remoteUser, w3request.w3server.nativeDomain, password);
                if (w3request.accountName != null)
                    w3request.userPrincipal = new W3Principal(w3request.remoteUser);
            } else w3request.userPrincipal = (Principal)w3context.authenticateMethod.invoke(w3context.realmObject, new Object[] {w3request.remoteUser, password});
            if (w3request.userPrincipal == null) throw new ServletException("Form authorization failed");
            W3Session session = w3context.getSession(w3request, true);
            session.principal = w3request.userPrincipal;
            res.sendRedirect(session.uri);
        } catch (ServletException ex) {
            w3context.getRequestDispatcher(w3context.formErrorPage).forward(req, res);
        } catch (Exception ex) {
            throw new ServletException(ex);
        }
    }

}
