
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Wide Area Information Server gateway servlet. Urls must be of form:<br>
    /host/databases?words<br>
    These may also be in query parameters with same names.<br>
    If path info is empty or new-parameter is specified, servlet returns general wais form.
*/
public class WaisServlet extends HttpServlet {
    static final long serialVersionUID = 0L;

    Map<String, String> servletPathTable = null;
    ResourceBundle messages;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        messages = W3URLConnection.getMessages("FI.realitymodeler.server.resources.Messages");
        servletPathTable = (Map<String, String>)config.getServletContext().getAttribute("FI.realitymodeler.server.W3Server/servletPathTable");
    }

    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        if (req.getParameterValues("empty") != null) {
            Messaging.getEmpty(res);
            return;
        }
        String action, root = "", path = req.getPathInfo(), s,
            host = (s = Support.getParameterValue(req.getParameterValues("host"))) != null && !(s = s.trim()).equals("") ? s : req.getServerName(),
            databases = (s = Support.getParameterValue(req.getParameterValues("databases"))) != null && !(s = s.trim()).equals("") ? s : "index",
            words = (s = Support.getParameterValue(req.getParameterValues("words"))) != null ? s.trim() : "",
            start = (s = Support.getParameterValue(req.getParameterValues("start"))) != null ? s.trim() : "",
            end = (s = Support.getParameterValue(req.getParameterValues("end"))) != null ? s.trim() : "",
            base = (s = Support.getParameterValue(req.getParameterValues("base"))) != null ? !(s = s.trim()).equals("") ? s : "/" : null;
        if (path == null) path = "";
        boolean mobile = (s = req.getHeader("user-agent")) != null && s.trim().equalsIgnoreCase("mobile") ||
            req.getParameterValues("mobile") != null;
        int tc = new StringTokenizer(path, "/").countTokens();
        if (tc == 0) root += host + "/";
        if (tc <= 1) root += databases + "/";
        int i = (action = req.getRequestURI()).indexOf('?');
        if (i != -1) action = action.substring(0, i).trim();
        if (!root.equals("") && !path.endsWith("/")) path += "/";
        path += root;
        if (!action.endsWith("/")) root = action.substring(action.lastIndexOf('/') + 1) + "/" + root;
        if (tc < 4 && (words.equals("") || req.getParameterValues("ask") != null) &&
            req.getParameterValues("go") == null || req.getParameterValues("new") != null) {
            res.setContentType(Support.htmlType);
            PrintWriter writer = res.getWriter();
            writer.println("<html><head><title>" + (!databases.equals("") ? databases : !host.equals("") ? host : Support.htmlString(messages.getString("wais"))) + "</title></head><body>");
            writer.println("<h1>" + Support.htmlString(messages.getString("wais")) + "</h1>");
            writer.println("<form action=\"" + action + "\" method=get>");
            writer.println("<input name=go type=hidden>");
            if (base != null) {
                writer.println("<input name=base type=hidden value=\"" + Support.htmlString(base) + "\">");
                writer.println("<input name=host type=hidden value=\"" + Support.htmlString(host) + "\">");
            }
            if (!mobile) {
                writer.println("<input name=frames type=checkbox" +
                               (req.getParameterValues("frames") != null ? " checked" : "") + ">" +
                               Support.htmlString(messages.getString("useFrames")) + "<br>");
                writer.println("<input name=type type=radio value=wb>" + Support.htmlString(messages.getString("byte")));
                writer.println("<input name=type type=radio value=wl checked>" + Support.htmlString(messages.getString("line")));
                writer.println("<input name=type type=radio value=wp>" + Support.htmlString(messages.getString("paragraph")) + "<br>");
                writer.println("<input type=reset value=\"" + Support.htmlString(messages.getString("defaults")) + "\">");
                writer.println("<input name=new type=submit value=\"" + Support.htmlString(messages.getString("getNewForm")) + "\">");
            }
            writer.println("<input type=submit value=\"" + Support.htmlString(messages.getString("getResults")) + "\">");
            writer.println("<table>");
            if (base == null && (!mobile || host.equals("")))
                writer.println("<tr><th>" + Support.htmlString(messages.getString("host")) + "</th><td>" +
                               (tc == 0 ? "<input name=host type=text value=\"" + Support.htmlString(host) + "\" size=55>" : host) + "</td></tr>");
            if (!mobile || databases.equals(""))
                writer.println("<tr><th>" + Support.htmlString(messages.getString("databases")) + "</th><td>" +
                               (tc <= 1 ? "<input name=databases type=text value=\"" + Support.htmlString(databases) + "\" size=55>" : databases) + "</td></tr>");
            writer.println("<tr><th>" + Support.htmlString(messages.getString("words")) + "</th><td><input name=words type=text value=\"" + Support.htmlString(words) + "\" size=55></td></tr>");
            if (!mobile) {
                writer.println("<tr><th>" + Support.htmlString(messages.getString("start")) + "</th><td><input name=start type=text value=\"" + Support.htmlString(start) + "\" size=15></td></tr>");
                writer.println("<tr><th>" + Support.htmlString(messages.getString("end")) + "</th><td><input name=end type=text value=\"" + Support.htmlString(end) + "\" size=15></td></tr>");
            }
            writer.println("</table>");
            writer.println("</form></body></html>");
            return;
        }
        if (req.getParameterValues("frames") != null && req.getParameterValues("done") == null) {
            Messaging.getFrames(req, res, action, !databases.equals("") ? databases : !host.equals("") ? host : messages.getString("wais"), messages);
            return;
        }
        if (!words.equals("")) path += "?" + URLEncoder.encode(words, "8859_1");
        if ((s = req.getQueryString()) != null && !(s = s.trim()).equals("")) path += (words.equals("") ? "?" : "&") + s;
        byte b[];
        if (req instanceof W3Request) b = ((W3Request)req).buffer;
        else b = new byte[Support.bufferLength];
        W3URLConnection uc = null;
        try {
            uc = new W3WaisURLConnection(new URL("wais:/" + path));
            uc.setRequestRoot(root);
            uc.setServletPathTable(servletPathTable);
            uc.setServletContext(getServletConfig().getServletContext());
            Messaging.get(req, res, uc, null, null, false);
        } finally {
            if (uc != null) uc.disconnect();
        }
    }

    public void doHead(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        doGet(req, res);
    }

}
