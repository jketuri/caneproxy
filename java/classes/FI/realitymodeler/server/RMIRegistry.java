
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.rmi.*;
import java.rmi.registry.*;
import java.rmi.server.*;
import javax.servlet.*;

public class RMIRegistry extends GenericServlet {
    static final long serialVersionUID = 0L;

    Registry registry;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        W3SecurityManager.set();
        //	System.setSecurityManager(new RMISecurityManager());
        String s;
        int port = (s = config.getInitParameter("port")) != null ? Integer.parseInt(s) : Registry.REGISTRY_PORT;
        try {
            if (Support.isTrue(config.getInitParameter("verbose"), "verbose") ||
                config.getServletContext().getAttribute("FI.realitymodeler.server.W3Server/verbose") == Boolean.TRUE) RemoteServer.setLog(System.out);
            registry = LocateRegistry.createRegistry(port);
        } catch (Exception ex) {
            throw new ServletException(Support.stackTrace(ex));
        }
    }

    public void service(ServletRequest req, ServletResponse res) {
    }

    public void destroy() {
        registry = null;
    }

}
