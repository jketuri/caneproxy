
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.sql.*;
import java.text.*;
import java.util.*;
import java.util.Date;
import javax.naming.*;
import javax.naming.directory.*;
import javax.naming.ldap.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** A servlet configuration object used by a servlet container used to pass information
    to a servlet during initialization.

*/
public class W3Servlet implements ServletConfig, Cloneable {
    Hashtable<String,String> initParameters = null;
    List<FilterTarget> filterTargetList = new ArrayList<FilterTarget>();
    Servlet servlet = null;
    String className = null;
    String jspFile = null;
    String servletName = null;
    W3Context w3context = null;
    boolean isDefault = false;
    boolean isDisabled = false;
    boolean isJsp = false;
    boolean isNative = false;
    boolean unavailable = false;
    int loadOnStartup = Integer.MIN_VALUE;

    public W3Servlet(W3Context w3context, String servletName, String className, Hashtable<String,String> initParameters, int loadOnStartup) {
        this.w3context = w3context;
        this.servletName = servletName;
        this.className = className;
        this.initParameters = initParameters;
        this.loadOnStartup = loadOnStartup;
    }

    public W3Servlet(W3Context w3context, String servletName, String className, Hashtable<String,String> initParameters, int loadOnStartup, boolean isNative, boolean isDisabled, boolean isDefault) {
        this(w3context, servletName, className, initParameters, loadOnStartup);
        this.isNative = isNative;
        this.isDisabled = isDisabled;
        this.isDefault = isDefault;
    }

    public W3Servlet(W3Context w3context, String servletName, String className, Hashtable<String,String> initParameters) {
        this(w3context, servletName, className, initParameters, 0);
    }

    public W3Servlet(W3Context w3context, String servletName, String className, Hashtable<String,String> initParameters, boolean isNative, boolean isDisabled, boolean isDefault) {
        this(w3context, servletName, className, initParameters, 0, isNative, isDisabled, isDefault);
    }

    /** Returns a String containing the value of the named initialization parameter,
        or null if the parameter does not exist.

        @param name a String specifying the name of the initialization parameter
        @return a String containing the value of the initialization parameter

    */
    public String getInitParameter(String name) {
        if (initParameters == null) return null;
        return initParameters.get(name);
    }

    /** Returns the names of the servlet's initialization parameters as an
        Enumeration of String objects, or an empty Enumeration if the servlet has
        no initialization parameters.
        Returns: an Enumeration of String objects containing the names of the
        servlet's initialization parameters

    */
    public Enumeration getInitParameterNames() {
        if (initParameters == null) return w3context.w3server.emptyEnumeration;
        return initParameters.keys();
    }

    /** Returns a reference to the ServletContext in which the caller is executing.

        @return a ServletContext object, used by the caller to interact with its
        servlet container
        @see ServletContext

    */
    public ServletContext getServletContext() {
        return w3context;
    }

    /** Returns the name of this servlet instance. The name may be provided via
        server administration, assigned in the web application deployment descriptor,
        or for an unregistered (and thus unnamed) servlet instance it will be the servlet's
        class name.

        @return the name of the servlet instance

    */
    public String getServletName() {
        return servletName;
    }

    public Servlet getServlet() {
        return servlet;
    }

    public void loadServlet()
        throws MalformedURLException, ServletException {
        if (servlet == null)
            try {
                w3context.loadServlet(this);
            } catch (ClassNotFoundException ex) {
                throw new ServletException(ex);
            } catch (IllegalAccessException ex) {
                throw new ServletException(ex);
            } catch (InstantiationException ex) {
                throw new ServletException(ex);
            }
    }

    public final void service(ServletRequest request, ServletResponse response, DispatcherType dispatcherType, boolean named, boolean filter)
        throws IOException, ServletException {
        if (w3context.w3server.verbose)
            w3context.log("service servlet=" + className +
                          ", dispatcherType=" + dispatcherType +
                          ", named=" + named +
                          ", filter=" + filter +
                          ", contextPath=" + ((HttpServletRequest)request).getContextPath() +
                          ", servletPath=" + ((HttpServletRequest)request).getServletPath() +
                          ", pathInfo=" + ((HttpServletRequest)request).getPathInfo() +
                          ", queryString=" + ((HttpServletRequest)request).getQueryString() +
                          ", requestUri=" + ((HttpServletRequest)request).getRequestURI() +
                          ", contextName=" + w3context.w3contextName);
        if (isJsp && !named && dispatcherType == DispatcherType.INCLUDE) {
            String contextPath = ((HttpServletRequest)request).getContextPath();
            if (!("/" + w3context.w3contextName).equals(contextPath)) {
                W3Context context = (W3Context)w3context.getContext(contextPath);
                context.jspServlet.servlet.service(request, response);
                return;
            }
        }
        servlet.service(request, response);
        /*
          if (dispatcherType == DispatcherType.REQUEST || dispatcherType == DispatcherType.FORWARD)
          response.flushBuffer();
        */
    }

    public Map getAttributes(ServletRequest request) {
        Map<String,Object> attributes = new HashMap<String,Object>();
        Enumeration attributeNames = request.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            String attributeName = (String)attributeNames.nextElement();
            attributes.put(attributeName, request.getAttribute(attributeName));
        }
        return attributes;
    }

    public Object clone() {
        W3Servlet w3servlet = new W3Servlet(w3context, servletName, className, initParameters);
        w3servlet.isDefault = isDefault;
        w3servlet.isDisabled = isDisabled;
        w3servlet.isJsp = isJsp;
        w3servlet.isNative = isNative;
        w3servlet.unavailable = unavailable;
        w3servlet.loadOnStartup = loadOnStartup;
        Iterator filterTargetIter = filterTargetList.iterator();
        while (filterTargetIter.hasNext())
            w3servlet.filterTargetList.add((FilterTarget)((FilterTarget)filterTargetIter.next()).clone());
        return w3servlet;
    }

    public String toString() {
        return getClass().getName() +
            " {servletName=" + servletName +
            ", className=" + className +
            ", initParameters=" + initParameters +
            ", filterTargetList=" + filterTargetList +
            ", w3contextName=" + w3context.w3contextName +
            ", isDefault=" + isDefault +
            ", isDisabled=" + isDisabled +
            ", isNative=" + isNative +
            ", unavailable=" + unavailable +
            ", loadOnStartup=" + loadOnStartup + "}";
    }

}
