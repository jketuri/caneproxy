
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;
import javax.naming.*;
import javax.naming.directory.*;
import javax.naming.ldap.*;
import javax.servlet.*;
import javax.servlet.http.*;

class ClientRobotShutdownHook extends Thread {
    Vector threads;

    ClientRobotShutdownHook(Vector threads) {
        this.threads = threads;
    }

    public void run() {
        Iterator iter = threads.iterator();
        try {
            while (iter.hasNext()) {
                Thread thread = (Thread)iter.next();
                thread.interrupt();
                thread.join(1000L);
            }
        } catch (InterruptedException ex) {}
    }

}

/** General purpose internet client robot.<br>

    1. Mirrors content in servers to local cache.<br>

    2. Tests servers simulating client which is sending requests and forms.<br>

    3. Searches keywords from text content in servers.<br>

    4. Searches keywords from other public robots.<br>

    5. Sends content and messages to receiving servers and mail systems.<br>

    Checks robot exclusion file

    <a href="http://info.webcrawler.com/mak/projects/robots/norobots.html">robots.txt</a>.<br>

    Handles session-specific

    <a href="http://home.netscape.com/newsref/std/cookie_spec.html">cookies</a>.<br>

    Class can be used also as a standalone program which is invoked in the
    following way:<br>

    <b>java FI.realitymodeler.server.ClientRobot [options] URL [keywords]</b><br>

    where allowed options are: (default value is in parantheses if applicable)<br>

    <b>-3 &lt;pop3 proxy host&gt; &lt;pop3 proxy port&gt;</b><br>

    This sets the host name and port of pop3-server proxy if used (must be
    FI.realitymodeler.server.W3Server).<br>

    <b>-A : append remaining user values to query</b><br>

    When filling html-forms automatically this adds remaining
    name-value-pairs specified in parameter file for -m option to the query
    even though they do not exist in original form.<br>

    <b>-C &lt;accept content type list&gt;</b><br>

    Gives value for Accept-header used in requests.<br>

    <b>-D &lt;header name&gt; &lt;default value&gt;</b><br>

    Sets default value for specified HTTP-header name used in all
    requests.<br>

    <b>-F : fill only caches, do not read input stream</b><br>

    <b>-E : force resuming to cache filling</b><br>

    Use this when cache filling was interrupted out of control. This avoids
    checking if partial cache file is valid.<br>

    <b>-N &lt;nntp proxy host&gt; &lt;nntp proxy port&gt;</b><br>

    This sets the host name and port of nntp-server proxy if used (must be
    FI.realitymodeler.server.W3Server).<br>

    <b>-P &lt;file listing parts of name collected protocol://host -names
    must start or end with or not to start nor end with if part of name is
    prefixed with !&gt;</b><br>

    This constraints set of URLs which are collected from parsed HTML-pages
    when recursively navigating the web.  For example 'host.domain' means
    that only URL's starting or ending with string 'host.domain' are
    collected.<br>

    <b>-R : trace only form handling</b><br>

    Says that form handling specified with -m option is only traced but not
    actually processed.<br>

    <b>-S &lt;smtp proxy&gt; &lt;port&gt;</b><br>

    This sets the host name and port of smtp-server proxy if used (must be
    FI.realitymodeler.server.W3Server).<br>

    <b>-U &lt;file listing root URLs&gt;</b><br>

    Specifies the file name where URLs are listed used as starting points of
    navigation. This can be used to specify more than one URL specified in
    command line parameter.<br>

    <b>-a &lt;username&gt; &lt;password&gt;</b><br>

    Specifies username and password used when logging in to remote
    servers.<br>

    <b>-b &lt;file listing paths branched URLs should start with or not
    start with if path is prefixed with !&gt;</b><br>

    Specifies file which lists parts of URLs by which collected URLs must
    start with.  Asterisk can be used as a wild card in the end of string.
    For example: http://host.domain*<br>

    <b>-c &lt;file listing paths sending URLs should start with or not start
    with if path is prefixed with !&gt;</b><br> Specifies file which lists
    parts of URLs by which sending URLs must start with.  Used with options
    -d or -e. Asterisk can be used as a wild card in the end of string.  For
    example: mailto:*<br>

    <b>-d : send files given in place of keywords to URLs found in given
    url.</b><br> Sends files given in place of keywords to URLs found from
    specified url.<br>

    <b>-e : send files given in place of keywords to URL.</b><br> Sends
    files given in place of keywords to specified URL.<br>

    <b>-f : set ftp proxy cache on (off)</b><br>
    Sets ftp-proxy cache on so.<br>

    <b>-g : set gopher proxy cache on (off)</b><br>
    Sets gopher-proxy cache on.<br>

    <b>-h : set http proxy cache on (off)</b><br>
    Sets http-proxy cache on.<br>

    <b>-i &lt;local ip address to bind&gt; &lt;local port&gt;</b><br>
    Sets the ip address socket used when connecting is bound.<br>

    <b>-j &lt;gopher proxy&gt; &lt;port&gt;</b><br>

    This sets the host name and port of gopher-server proxy if used (can be
    any web-server supporting gopher-proxy).<br>

    <b>-k : only keywords given and they are searched from robots</b><br>

    Keywords are searched from search engine URLs specified in file
    'robots'. Forms sent to search engines are specified in file
    'robot_forms' and parts of URLs not collected from parsed result pages
    are specified in file 'robot_parts'.<br>

    <b>-l : test all links</b><br>

    <b>-m &lt;form values file containing action paths with list of
    name=value pairs&gt;</b><br>

    Specifies file where assignments for form handing are defined. File
    'robot_forms' can used as example.<br>

    <b>-n &lt;number of threads in client simulation&gt;</b><br>

    <b>-o &lt;http proxy&gt; &lt;port&gt;</b><br>

    This sets the host name and port of http-server proxy if used (can be
    any web-server supporting http-proxy).<br>

    <b>-p</b><br>

    Use POST-method when sending files.<br>

    <b>-q : type files to standard output</b><br>

    <b>-r : read all files so that cache gets filled</b><br>

    <b>-s : stay only under given URL</b><br>

    <b>-t : trace only and do not actually send</b><br>

    <b>-u &lt;ftp proxy&gt; &lt;port&gt;</b><br>

    This sets the host name and port of ftp-server proxy if used (can be any
    web-server supporting ftp-proxy).<br>

    <b>-w &lt;delay in seconds between requests&gt;</b><br>

    <b>-x : force cache filling with dynamic content</b><br>

    Forces cache to be filled also with request containing queries.<br>

    <b>-y : &lt;socket read timeout in secs&gt; (0)</b><br>

    <b>-z : set cache header storing on (off)</b><br>

    Specifies if response headers are stored to cache files, so that they
    can be used with FI.realitymodeler.server.W3Server's proxy mechanism or if not
    stored, they can be used directly with browser if applicable (e.g. text,
    images and audio files).<p>

    Searched keyword and url data is stored to LDAP directory with standard schema.<br>
    Object class: organizationalUnit<br>
    Distinguished name: ou=unique id<br>
    Attributes:<br>
    <table>
    <caption>Organizational unit</caption>
    <tr><th>registeredAddress</th><td>keyword, keyword...</td></tr>
    <tr><th>description</th><td>url</td></tr>
    <tr><th>st</th><td>last time modified</td></tr>
    </table>
*/
public class ClientRobot extends HttpServlet implements Runnable {
    static final long serialVersionUID = 0L;
    static final String unexp_token = "Unexpected token";

    static long cleaningTime = 0L, entryLifeSpan = 7L * 24L * 60L * 60L * 1000L;
    static ClientRobot clientRobot = null;
    static String version = null;
    static String host = null;
    static URL context;

    /** Append remaining user values to query. */
    public boolean appendUserValues = false;
    /** Handles parsed forms when form values are given. */
    public boolean handling = false;
    /** Reads fetched files always entirely so that cache gets filled. */
    public boolean read = false;
    public boolean fillCaches = false;
    /** Forces cache filling also with dynamic content. */
    public boolean force = false;
    /** Forces resuming to cache filling. */
    public boolean forceResume = false;
    /** Request fresh copy from origin server. */
    public boolean requestFreshCopy = false;
    /** Parsing is done for sending only and no branching to embedded urls should be done. */
    public boolean sending = false;
    /** Instance is running as a standalone program and verbosing is done directly to System.out. */
    public boolean standalone = false;
    /** Test all links. */
    public boolean test = false;
    /** Types files to standard output. */
    public boolean type = false;
    /** Trace only and do not actually send. */
    public boolean trace = false;
    /** Trace only form handling. */
    public boolean traceForms = false;
    public boolean usePost = false;
    /** Verbosing is done to System.out. */
    public boolean verbose = false;
    /** Paths branched urls should (not) match. */
    public int timeout = 0;
    public RegexpPool branchPaths[] = null;
    /** Paths sending urls should (not) match. */
    public RegexpPool sendPaths[] = null;
    /** Parts of name collected protocol://host -names must (not) contain. */
	public RegexpPool nameParts[] = null;
    /** Lists in keys which protocols should be cached. */
    public Map<String, Boolean> caches = new HashMap<String, Boolean>();
    /** Gives parameters to fetch-method if thread is used. */
    public Vector<String> keywords = null;
    public Vector<URL> urls = null;
    /** Contains form values. */
    public Map<String, HtmlForm> userForms = null;
    /** Local address and port to bind in connections. */
    public InetAddress localAddress = null;
    public int localPort = 0;
    /** Delay between requests. */
    public long delay = 0L;
    /** Accept header. */
    public String accept = null;
    /** From address. */
    public String from = null;

    RegexpPool cookiePool = new RegexpPool();
    ResourceBundle messages;
    String credentials = null, searchKeyword = null;
    java.util.Random random = new java.util.Random();
    byte searchUrl[] = null;

    class CheckingClass implements Checking {
        Map<String, Object> doneUrls;
        boolean keepAlive;

        public void redirecting(W3URLConnection uc) {
            uc.disconnect();
            setConnection(uc, this);
        }

    }

    class CleaningThread extends Thread {

        public void run() {
            LdapContext ldapContext[] = new InitialLdapContext[1], clientRobotContext = null;
            try {
                clientRobotContext = W3Server.open(ClientRobot.this.getClass().getName(), ldapContext);
                NamingEnumeration resultEnum = clientRobotContext.search("", "(modifyTimestamp <= {1})",
                                                                         new Object[] {Support.generalizedTime((cleaningTime = System.currentTimeMillis()) - entryLifeSpan)}, null);
                try {
                    while (resultEnum.hasMore()) clientRobotContext.unbind(((SearchResult)resultEnum.next()).getName());
                } finally {
                    resultEnum.close();
                }
                log(getClass().getName() + " directory cleaned");
            } catch (Exception ex) {
                if (W3URLConnection.logging) log(Support.stackTrace(ex));
            } finally {
                W3Server.close(clientRobotContext, ldapContext);
            }
        }

    }

    public ClientRobot()
        throws IOException {
        if (version == null) version = "Java/" + getClass().getName() + " 1997.8";
        if (context == null) context = new URL("http", InetAddress.getLocalHost().getHostAddress(), "/");
    }

    public void log(String msg) {
        if (standalone)
            System.out.println(msg);
        else super.log(msg);
    }

    public RegexpPool[] readList(String filename, boolean nameParts)
        throws IOException, ParseException {
        RegexpPool pools[] = new RegexpPool[2];
        BufferedReader bf = new BufferedReader(new FileReader(filename));
        StreamTokenizer sTok = new StreamTokenizer(bf);
        try {
            sTok.resetSyntax();
            sTok.whitespaceChars(0, 32);
            sTok.wordChars(33, 255);
            sTok.ordinaryChar('!');
            sTok.commentChar('#');
            sTok.quoteChar('"');
            while (sTok.nextToken() != sTok.TT_EOF) {
                RegexpPool pool;
                if (sTok.ttype == '!') {
                    if (pools[1] == null) pools[1] = new RegexpPool();
                    pool = pools[1];
                    sTok.nextToken();
                } else {
                    if (pools[0] == null) pools[0] = new RegexpPool();
                    pool = pools[0];
                }
                if (sTok.ttype != sTok.TT_WORD && sTok.ttype != '"') throw new ParseException(unexp_token, 0);
                if (nameParts) {
                    pool.add(sTok.sval + "*", sTok.sval);
                    pool.add("*" + sTok.sval, sTok.sval);
                } else if (sTok.sval.endsWith("/")) pool.add(sTok.sval + "*", sTok.sval);
                else pool.add(sTok.sval, sTok.sval);
            }
        } catch (Exception ex) {
            throw new ParseException("Error in file " + filename + " in line " +
                                     sTok.lineno() + "\n" + Support.stackTrace(ex), sTok.lineno());
        } finally {
            bf.close();
        }
        return pools;
    }

    public Map<String, HtmlForm> readUserForms(String filename)
        throws IOException, ParseException {
        Map<String, HtmlForm> userForms = new HashMap<String, HtmlForm>();
        BufferedReader bf = new BufferedReader(new FileReader(filename));
        StreamTokenizer sTok = new StreamTokenizer(bf);
        try {
            sTok.resetSyntax();
            sTok.whitespaceChars(0, 32);
            sTok.wordChars(33, 255);
            sTok.ordinaryChar('=');
            sTok.commentChar('#');
            sTok.quoteChar('"');
            HtmlForm userForm = null;
            while (sTok.nextToken() != sTok.TT_EOF) {
                if (sTok.ttype != sTok.TT_WORD && sTok.ttype != '"') throw new ParseException(unexp_token, 0);
                String s = sTok.sval;
                if (sTok.nextToken() == '=') {
                    sTok.nextToken();
                    if (sTok.ttype != sTok.TT_WORD && sTok.ttype != '"' || userForm == null) throw new ParseException(unexp_token, 0);
                    userForm.putField(s, Support.replace(sTok.sval, "\n", "\r\n"));
                    continue;
                } else sTok.pushBack();
                userForm = new HtmlForm(null, null, new URL(s));
                String key = userForm.getURL().toString();
                int i = key.lastIndexOf('?');
                if (i != -1) key = key.substring(0, i);
                userForms.put(key, userForm);
            }
        } catch (Exception ex) {
            throw new ParseException("Error in file " + filename + " in line " +
                                     sTok.lineno() + "\n" + Support.stackTrace(ex), sTok.lineno());
        } finally {
            bf.close();
        }
        return userForms;
    }

    public Vector<URL> readUrls(String filename)
        throws IOException, ParseException {
        Vector<URL> urls = new Vector<URL>();
        BufferedReader bf = new BufferedReader(new FileReader(filename));
        StreamTokenizer sTok = new StreamTokenizer(bf);
        try {
            sTok.resetSyntax();
            sTok.whitespaceChars(0, 32);
            sTok.wordChars(33, 255);
            sTok.commentChar('#');
            sTok.quoteChar('"');
            while (sTok.nextToken() != sTok.TT_EOF) urls.addElement(new URL(sTok.sval));
        } catch (Exception ex) {
            throw new ParseException("Error in file " + filename + " in line " +
                                     sTok.lineno() + "\n" + Support.stackTrace(ex), sTok.lineno());
        } finally {
            bf.close();
        }
        return urls;
    }

    /** Sets common connection attributes. */
    String setConnection(W3URLConnection uc, CheckingClass checking) {
        String protocol = uc.getURL().getProtocol().toLowerCase();
        if (protocol.equals("http") || protocol.equals("https")) {
            if (accept != null) uc.setRequestProperty("Accept", accept);
            if (checking.keepAlive) uc.setRequestProperty("Connection", "Keep-Alive");
            String host = uc.getURL().getHost().toLowerCase();
            RegexpPool pool = (RegexpPool)cookiePool.match(host);
            if (pool != null) {
                String path = uc.getURL().getFile();
                StringBuffer sb = new StringBuffer();
                do {
                    Map cookies = (Map)pool.match(path);
                    if (cookies != null)
                        do {
                            Iterator iter = cookies.entrySet().iterator();
                            while (iter.hasNext()) {
                                Map.Entry entry = (Map.Entry)iter.next();
                                if (sb.length() > 0) sb.append("; ");
                                sb.append(entry.getKey()).append('=').append(entry.getValue());
                            }
                        } while ((cookies = (Map)pool.matchNext(path)) != null);
                } while ((pool = (RegexpPool)cookiePool.matchNext(host)) != null);
                if (sb.length() > 0) uc.addRequestProperty("Cookie", sb.toString());
            }
        }
        if (protocol.equals("mailto")) {
            if (from == null) from = System.getProperty("user.fromaddr");
            if (from != null) uc.setRequestProperty("From", from);
        } else {
            if (credentials != null) uc.setRequestProperty("Authorization", credentials);
            if (requestFreshCopy) {
                uc.setRequestProperty("Pragma", "no-cache");
                uc.setRequestProperty("Cache-Control", "no-cache");
            }
        }
        if (checking.doneUrls != null) {
            Object referer = checking.doneUrls.get(uc.getURL().toString());
            if (referer instanceof URL) uc.setRequestProperty("Referer", referer.toString());
        }
        if (localAddress != null) uc.setLocalAddress(localAddress, localPort);
        uc.setUseCaches(caches.containsKey(uc.getURL().getProtocol().toLowerCase()));
        if (fillCaches) uc.setFillCaches(uc.getUseCaches());
        if (force) uc.setForceCaches(uc.getUseCaches());
        if (forceResume) uc.setForceResume(uc.getUseCaches());
        uc.setChecking(checking);
        return protocol;
    }

    boolean skipped(URL url, RegexpPool paths[], String key)
        throws MalformedURLException {
        key = new URL(url, key).toString();
        String path0 = null, path1 = null;
        // if path is not found in regexp pool of allowed paths it is skipped
        if (paths[0] != null && (path0 = (String)paths[0].match(key)) == null) return true;
        // if path is found in regexp pool of forbidden paths and it's regexp is longer than
        // that found in allowed paths it is skipped
        return paths[1] != null && (path1 = (String)paths[1].match(key)) != null &&
            (path0 == null || path0.length() < path1.length()) ? true : false;
    }

    /** Handles form. */
    public W3URLConnection handle(W3URLConnection uc, HtmlForm form, Map<String, HtmlForm> userForms, Map<String, Object> doneUrls, Vector keywords, int keywordN)
        throws IOException {
        URL url = form.getURL();
        if (verbose) log("Checking for handling " + url);
        String path = url.getFile();
        int i = path.lastIndexOf('?');
        if (i != -1) path = path.substring(0, i);
        HtmlForm userForm = userForms.get(new URL(url, path).toString());
        if (userForm == null &&
            ((i = path.lastIndexOf('/')) <= 0 || (path = path.substring(i).trim()).length() <= 1 ||
             (userForm = userForms.get(new URL(url, path).toString())) == null)) return uc;
        FieldMap fields[] = new FieldMap[] {userForm.getFields(), form.getFields()}, values[] = new FieldMap[2];
        for (int num = 0; num < 2; num++) {
            values[num] = new FieldMap();
            Iterator fieldIter = fields[num].entrySet().iterator();
            while (fieldIter.hasNext()) {
                Map.Entry entry = (Map.Entry)fieldIter.next();
                values[num].put((String)entry.getKey(), entry.getValue() instanceof Vector ? ((Vector)entry.getValue()).elements() : entry.getValue());
            }
            if (test) {
                values[1] = fields[1];
                break;
            }
        }
        StringBuffer sb = new StringBuffer();
        Enumeration elems = form.getNames().elements();
        if (elems.hasMoreElements())
            for (boolean appending = false;;) {
                Object obj;
                String name = (String)elems.nextElement(), value;
                int maxNum = appending ? 1 : 2;
                for (int num = 0; num < maxNum; num++)
                    if ((obj = values[num].get(name)) != null) {
                        if (!test || num == 0) {
                            if (obj instanceof Enumeration) {
                                value = (String)((Enumeration)obj).nextElement();
                                if (!((Enumeration)obj).hasMoreElements()) values[num].remove(name);
                            } else {
                                value = (String)obj;
                                values[num].remove(name);
                            }
                            if (num == 0) {
                                if ((i = value.indexOf('*')) != -1) {
                                    String head = value.substring(0, i), tail = value.substring(i + 1);
                                    if (keywords == null) continue;
                                    if (!test) {
                                        if (keywordN >= keywords.size()) {
                                            values[num].remove(name);
                                            keywords = null;
                                            continue;
                                        }
                                        value = head + keywords.elementAt(keywordN++) + tail;
                                    } else value = head + keywords.elementAt(random.nextInt() % keywords.size()) + tail;
                                }
                            }
                        } else value = (String)(obj instanceof Vector ?
                                                ((Vector)obj).elementAt(random.nextInt() % ((Vector)obj).size()) : obj);
                        if (sb.length() > 0) sb.append('&');
                        sb.append(URLEncoder.encode(name, uc.getCharsetName())).append('=').append(URLEncoder.encode(value, uc.getCharsetName()));
                        break;
                    }
                if (!elems.hasMoreElements())
                    if (appendUserValues) {
                        if (appending) break;
                        elems = userForm.getNames().elements();
                        appending = true;
                    } else break;
            }
        String method = form.getMethod(), query = sb.toString();
        if (traceForms) {
            System.out.println("would try to handle " + url + " with query " + query);
            return uc;
        }
        if (verbose) System.out.println("handling " + url + " with query " + query);
        if (uc == null || !uc.check(url)) {
            if (uc != null) uc.disconnect();
            uc = W3URLConnection.openConnection(url);
            uc.setTimeout(timeout);
        } else uc.set(url);
        try {
            CheckingClass checking = new CheckingClass();
            checking.doneUrls = doneUrls;
            setConnection(uc, checking);
            path = url.getFile().trim();
            if (method.equals("GET")) url = new URL(url, path +
                                                    ((i = path.indexOf('?')) != -1 ? i < path.length() - 1 ? "&" : "" : "?") + query);
            uc.setRequestMethod(method);
            if (method.equals("POST")) {
                uc.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                uc.setRequestProperty("Content-Length", String.valueOf(query.length()));
                if (verbose) Support.sendMessage(System.out, uc.getRequestHeaderFields());
                OutputStream out = uc.getOutputStream();
                if (out != null) {
                    Support.writeBytes(out, query, null);
                    out.flush();
                }
                uc.closeStreams();
            }
        } catch (IOException ex) {
            uc.disconnect();
            uc = null;
            if (W3URLConnection.logging) log(Support.stackTrace(ex));
        }
        return uc;
    }

    /** Sends files to network. */
    public W3URLConnection send(W3URLConnection uc, Vector urls, int un, Map<String, Object> doneUrls, Vector keywords)
        throws IOException {
        if (verbose) log("Trying to send files");
        byte b[] = new byte[Support.bufferLength];
        for (; un < urls.size(); un++) {
            URL url = (URL)urls.elementAt(un);
            String path = url.toString(), query = null;
            int i = path.indexOf('?');
            if (i != -1) {
                query = path.substring(i);
                path = path.substring(0, i);
            }
            if (sendPaths != null && skipped(url, sendPaths, path)) continue;
            if (!path.endsWith("/")) path += "/";
            if (uc == null || !uc.check(url)) {
                if (uc != null) uc.disconnect();
                uc = W3URLConnection.openConnection(url);
                uc.setTimeout(timeout);
            } else uc.set(url);
            CheckingClass checking = new CheckingClass();
            checking.doneUrls = doneUrls;
            for (i = 0; i < keywords.size(); i++) {
                String filename = (String)keywords.elementAt(i),
                    subject = filename.substring(filename.lastIndexOf('/') + 1);
                if (trace) {
                    System.out.println("would try to send " + filename + " to " + url);
                    continue;
                }
                String ct = W3URLConnection.guessContentType(filename),
                    protocol = setConnection(uc, checking);
                boolean forceClose = false,
                    fileLess = protocol.equals("mailto") || protocol.equals("nntp");
                if (!fileLess) uc.set(new URL(url, path + filename + (query != null ? query : "")));
                uc.setRequestProperty("Content-Type", ct = ct != null ? ct : Support.plainType);
                File file = new File(filename);
                if (verbose) System.out.println("sending " + file + " to " + url);
                BufferedInputStream in = new BufferedInputStream(new FileInputStream(file));
                try {
                    OutputStream out;
                    uc.setRequestMethod(usePost ? "POST" : "PUT");
                    if (fileLess) {
                        boolean text = ct.startsWith("text/");
                        uc.setRequestProperty("Content-Transfer-Encoding",
                                              text ? "quoted-printable" : "base64");
                        uc.setRequestProperty("Subject", filename);
                        out = uc.getOutputStream();
                        if (text) new QPEncoder().encode(in, out);
                        else new BASE64Encoder().encode(in, out);
                    } else {
                        uc.setRequestProperty("Content-Length", String.valueOf(file.length()));
                        out = uc.getOutputStream();
                        for (int l = 0, n; (n = in.read(b)) > 0;) {
                            out.write(b, 0, n);
                            System.out.print("\r" + (l += n) + " written");
                        }
                        System.out.println();
                    }
                    out.flush();
                    uc.closeStreams();
                    InputStream in1 = uc.getInputStream();
                    if (verbose) Support.sendMessage(System.out, uc.getHeaderFieldList());
                    if (type && in1 != null) {
                        int n;
                        while ((n = in1.read(b)) > 0) System.out.write(b);
                    }
                    out.close();
                } catch (IOException ex) {
                    forceClose = true;
                    if (W3URLConnection.logging) log(Support.stackTrace(ex));
                } finally {
                    if (uc != null)
                        if (!uc.mayKeepAlive() || forceClose) {
                            uc.disconnect();
                            uc = null;
                        } else uc.closeStreams();
                    in.close();
                }
            }
        }
        return uc;
    }

    int getNextKeyword(LdapContext clientRobotContext, String urlKey, Vector keywords, int keywordN)
        throws NamingException {
        while (++keywordN < keywords.size()) {
            searchKeyword = (String)keywords.elementAt(keywordN);
            NamingEnumeration resultEnum = clientRobotContext.search("ou=" + Support.escapeDn(urlKey), "(description=*" + Support.escapeFilter("/" + searchKeyword + "/") + "*)", null);
            try {
                if (!resultEnum.hasMore()) break;
            } finally {
                resultEnum.close();
            }
        }
        return keywordN;
    }

    /** Fetches urls from given root urls and finds keywords if specified.
        Additional flags are given in instance variables (see above).
        @param urls root urls to be parsed
        @param keywords keywords to be searched or null
        @param robots specifies if search robots are used */
    public void fetch(LdapContext clientRobotContext, Vector<URL> urls, Vector<String> keywords, boolean robots)
        throws IOException, InterruptedException, NamingException, ParseException {
        Map<String, Object> disallow = new HashMap<String, Object>(), doneUrls = new HashMap<String, Object>();
        boolean searching = keywords != null && !handling && !sending,
            started = false;
        int nUrls = urls.size(), same[] = null, sames[] = null;
        if (searching) {
            if (keywords.size() == 0) return;
            // scratch array keeping number of same characters as keyword
            same = new int[keywords.size()];
            // array keeping number of hits with keyword
            sames = new int[keywords.size()];
        }
        W3URLConnection uc = null;
        CheckingClass checking = new CheckingClass();
        checking.doneUrls = doneUrls;
        checking.keepAlive = true;
        for (boolean forceClose = false; !urls.isEmpty() && (!robots || --nUrls >= 0) && !Thread.currentThread().isInterrupted(); forceClose = false)
            try {
                URL url = urls.firstElement();
                urls.removeElementAt(0);
                int keywordN = -1;
                if (started && branchPaths != null && skipped(url, branchPaths, url.toString())) continue;
                started = true;
                if (verbose) System.out.println((robots ? nUrls : urls.size()) + " url(s) left\n" + url);
                if (uc == null || !uc.check(url)) {
                    if (uc != null) uc.disconnect();
                    uc = W3URLConnection.openConnection(url);
                    uc.setTimeout(timeout);
                } else uc.set(url);
                setConnection(uc, checking);
                if (robots) {
                    int nextKeywordN = getNextKeyword(clientRobotContext, url.toString(), keywords, keywordN);
                    if (nextKeywordN >= keywords.size()) break;
                }
                if (!robots && searching) {
                    // checks if remote server has robots.txt-file constraining robot traversal
                    if (url.getProtocol().equalsIgnoreCase("http") || url.getProtocol().equalsIgnoreCase("https")) {
                        Object obj = disallow.get(url.getHost().toLowerCase());
                        if (obj == null) {
                            URL urlRobots = new URL(url, "/robots.txt");
                            W3URLConnection ucRobots = W3URLConnection.openConnection(urlRobots);
                            ucRobots.setTimeout(timeout);
                            try {
                                setConnection(ucRobots, checking);
                                ucRobots.setUseCaches(false);
                                InputStream in = ucRobots.getInputStream();
                                RegexpPool pool = new RegexpPool();
                                StreamTokenizer sTok = new StreamTokenizer(new InputStreamReader(in));
                                sTok.resetSyntax();
                                sTok.whitespaceChars(0, 32);
                                sTok.wordChars(33, 255);
                                sTok.ordinaryChar(':');
                                sTok.commentChar('#');
                                while (sTok.nextToken() != sTok.TT_EOF)
                                    if (sTok.ttype == sTok.TT_WORD &&
                                        sTok.sval.equalsIgnoreCase("disallow")) {
                                        if (sTok.nextToken() == sTok.TT_EOF) break;
                                        if (sTok.ttype != ':') continue;
                                        if (sTok.nextToken() == sTok.TT_EOF) break;
                                        if (sTok.ttype != sTok.TT_WORD) continue;
                                        try {
                                            pool.add(sTok.sval + "*", sTok.sval);
                                            if (verbose) System.out.println("disallowed " + sTok.sval);
                                        } catch (RegexException ex) {}
                                    }
                                disallow.put(url.getHost().toLowerCase(), obj = pool);
                            } catch (IOException ex) {
                                disallow.put(url.getHost().toLowerCase(), obj = Boolean.TRUE);
                                if (W3URLConnection.logging) log(Support.stackTrace(ex));
                            } finally {
                                ucRobots.disconnect();
                            }
                        }
                        if (obj instanceof RegexpPool && ((RegexpPool)obj).match(url.getFile()) != null) break;
                    }
                    // checks if url with all specified keywords is already in directory
                    try {
                        Attributes attributes = clientRobotContext.getAttributes("ou=" + Support.escapeDn(url.toString()), new String[] {"description", "st"});
                        String keywordString = (String)attributes.get("description").get();
                        int i;
                        for (i = 0; i < keywords.size(); i++)
                            if (keywordString.indexOf("|" + keywords.elementAt(i) + "|") == -1) break;
                        if (i == keywords.size()) {
                            long time = Support.parseGeneralizedTime((String)attributes.get("st").get());
                            uc.setRequestProperty("If-Modified-Since", Support.format(new java.util.Date(time)));
                            uc.setIfModifiedSince(time);
                        }
                    } catch (NamingException ex) {}
                }
                Vector<HtmlForm> forms = new Vector<HtmlForm>();
                while (!Thread.currentThread().isInterrupted()) {
                    forceClose = false;
                    InputStream in = uc.getInputStream();
                    if (verbose) Support.sendMessage(System.out, uc.getHeaderFieldList());
                    HeaderList headers = uc.getHeaderFieldList();
                    Header setCookieHeaders[] = headers.getHeaders("set-cookie");
                    // saves cookie values to memory to be used in further requests
                    if (setCookieHeaders != null)
                        for (int i = 0; i < setCookieHeaders.length; i++) {
                            Map<String, String> params = new HashMap<String, String>();
                            Vector<Assignment> assignments = new Vector<Assignment>();
                            Support.parse(setCookieHeaders[i].getValue(), params, ";", true, false, assignments);
                            String domain = params.get("domain"), path = params.get("path"),
                                domainPattern = (domain != null ? "*" + domain : url.getHost()).toLowerCase(),
                                pathPattern = (path != null ? path : url.getFile()) + "*";
                            Map<String, String> cookies;
                            RegexpPool pool = (RegexpPool)cookiePool.delete(domainPattern);
                            if (pool == null) {
                                pool = new RegexpPool();
                                cookies = null;
                            } else cookies = (Map<String, String>)pool.delete(pathPattern);
                            if (cookies == null) cookies = new HashMap<String, String>();
                            try {
                                pool.add(pathPattern, cookies);
                            } catch (RegexException ex) {}
                            try {
                                cookiePool.add(domainPattern, pool);
                            } catch (RegexException ex) {}
                            Iterator assignmentIter = assignments.iterator();
                            while (assignmentIter.hasNext()) {
                                Assignment assignment = (Assignment)assignmentIter.next();
                                String key = assignment.name.toLowerCase();
                                if (key.equals("domain") || key.equals("expires") || key.equals("path") || key.equals("secure")) continue;
                                cookies.put(assignment.name, assignment.value);
                            }
                        }
                    if (in == null && !fillCaches ||
                        uc.getResponseCode() == HttpURLConnection.HTTP_NO_CONTENT ||
                        uc.getResponseCode() == HttpURLConnection.HTTP_NOT_MODIFIED ||
                        uc.getResponseCode() == HttpURLConnection.HTTP_PRECON_FAILED) break;
                    boolean html;
                    if (uc.getResponseCode() != HttpURLConnection.HTTP_OK ||
                        !(html = uc.isHtml()) && (robots || !searching || !uc.isText())) {
                        // response is skipped or read to cache
                        if (type) {
                            byte b[] = new byte[Support.bufferLength];
                            for (int n; (n = in.read(b)) > 0;) System.out.write(b, 0, n);
                        } else if (read || test)
                            if (!fillCaches) {
                                boolean interrupted = false;
                                byte b[] = new byte[Support.bufferLength];
                                int l = 0;
                                for (int n; !Thread.currentThread().isInterrupted() && (n = in.read(b)) > 0;) System.out.print("\r" + (l += n) + " read");
                                System.out.println();
                                if (Thread.currentThread().isInterrupted()) in.close();
                            } else uc.waitForFillingCaches();
                        else forceClose = true;
                        if (robots) keywordN = keywords.size();
                        break;
                    }
                    if (Thread.currentThread().isInterrupted()) break;
                    // response is parsed for keywords and/or new urls
                    if (type) in = new OutputInputStream(in, System.out);
                    Vector<URL> urlVector = robots && keywordN > -1 ? new Vector<URL>() : urls;
                    int urlVectorSize = urlVector.size(),
                        nWords = W3URLConnection.parse(in, urlVector, !robots && searching ? keywords : null,
                                                        handling || robots && keywordN == -1 ? forms : null, !robots || keywordN == -1 ? url : null,
                                                        !robots || keywordN > -1 ? nameParts : null, doneUrls, same, sames, null, null, null, null, html, test);
                    if (robots && keywordN == -1) {
                        keywordN = getNextKeyword(clientRobotContext, url.toString(), keywords, keywordN);
                        if (keywordN >= keywords.size()) break;
                        if (forms.isEmpty() ||
                            (uc = handle(uc, forms.firstElement(), userForms, doneUrls, keywords, keywordN)) == null) {
                            keywordN = keywords.size();
                            break;
                        }
                        continue;
                    }
                    // sends files listed in keywords to found urls
                    if (sending) uc = send(uc, urlVector, urlVectorSize, doneUrls, keywords);
                    if (handling) {
                        if (!forms.isEmpty()) {
                            // fills found forms with user supplied values
                            HtmlForm form = forms.firstElement();
                            forms.removeElementAt(0);
                            if ((uc = handle(uc, form, userForms, doneUrls, keywords, 0)) == null) break;
                            url = form.getURL();
                        }
                        if (!robots) continue;
                    }
                    if (searching) {
                        // updates directory with found keywords and urls
                        if (!robots)
                            for (int i = 0; i < keywords.size(); i++) {
                                String keyword = keywords.elementAt(i);
                                if (keyword.length() == 0) continue;
                                if (verbose) System.out.println("Found " + sames[i] + " of " + keyword);
                            }
                        int urlVectorNum = robots ? urlVector.size() : 1, nNewUrls = 0;
                        for (int num = robots ? -1 : 0; num < urlVectorNum; num++) {
                            String newUrlKey;
                            if (robots && num != -1) {
                                URL newUrl = urlVector.elementAt(num);
                                newUrlKey = newUrl.toString();
                                if (newUrlKey.indexOf('?') != -1) continue;
                                if (verbose) System.out.println(newUrlKey);
                                nNewUrls++;
                            } else newUrlKey = url.toString();
                            int i;
                            if (!robots) {
                                i = 0;
                                keywordN = keywords.size() - 1;
                            } else i = keywordN;
                            SearchResult urlResult = null;
                            NamingEnumeration urlResultEnum = null;
                            try {
                                urlResultEnum = clientRobotContext.search("", "(&(objectClass=organizationalUnit)(description=" +
                                                                          Support.escapeFilter(newUrlKey) + "))",
                                                                          new SearchControls(SearchControls.ONELEVEL_SCOPE, 1L, 0, null, false, true));
                                urlResult = (SearchResult)urlResultEnum.next();
                                if (verbose) log("Found url " + urlResult.getName() + " with attributes " + urlResult.getAttributes());
                            } catch (NameNotFoundException ex) {} catch (NoSuchElementException ex) {} finally {
                                if (urlResultEnum != null) urlResultEnum.close();
                            }
                            Attribute keywordAttribute = urlResult == null ?
                                new BasicAttribute("registeredAddress") : urlResult.getAttributes().get("registeredAddress");
                            boolean added = false;
                            long lastModified = uc.getLastModified();
                            for (; i <= keywordN; i++) {
                                String keyword = keywords.elementAt(i);
                                if (robots || nWords > 0 && (float)sames[i] / (float)nWords > .01 &&
                                    !keywordAttribute.contains(keyword)) {
                                    keywordAttribute.add(keyword);
                                    added = true;
                                }
                            }
                            if (urlResult == null) {
                                Attributes attributes = new BasicAttributes();
                                Attribute objectClass = new BasicAttribute("objectClass");
                                objectClass.add("top");
                                objectClass.add("organizationalUnit");
                                attributes.put(objectClass);
                                attributes.put("description", newUrlKey);
                                if (added) attributes.put(keywordAttribute);
                                attributes.put("st", Support.generalizedTime(lastModified));
                                if (verbose) log("Rebinding url with attributes " + attributes);
                                clientRobotContext.rebind(W3Server.makeName(clientRobotContext, "ou"), null, attributes);
                            } else if (added) {
                                Attributes attributes = new BasicAttributes();
                                attributes.put(keywordAttribute);
                                if (verbose) log("Modifying url " + urlResult.getName() + " with attributes " + attributes);
                                clientRobotContext.modifyAttributes(urlResult.getName(), clientRobotContext.REPLACE_ATTRIBUTE, attributes);
                            }
                            if (robots && verbose) System.out.println("Found " + nNewUrls + " urls containing " + keywords.elementAt(keywordN));
                        }
                    }
                    if (robots) {
                        keywordN = getNextKeyword(clientRobotContext, url.toString(), keywords, keywordN);
                        if (keywordN >= keywords.size()) break;
                        if (uc != null) uc.disconnect();
                        if ((uc = handle(uc, forms.firstElement(), userForms, doneUrls, keywords, keywordN)) == null) {
                            keywordN = keywords.size();
                            break;
                        }
                    } else break;
                }
            } catch (InterruptedException ex) {
                forceClose = true;
            } catch (IOException ex) {
                forceClose = true;
                if (W3URLConnection.logging) log(Support.stackTrace(ex));
            } finally {
                if (uc != null) {
                    if (!forceClose) uc.complete();
                    if (!uc.mayKeepAlive() || forceClose) {
                        uc.disconnect();
                        uc = null;
                    } else uc.closeStreams();
                }
                if (delay > 0L) Thread.sleep(delay);
            }
    }

    void setRobots(String dir)
        throws IOException, ParseException {
        if (userForms == null) userForms = readUserForms(dir + "robot_forms");
        if (nameParts == null) nameParts = readList(dir + "robot_parts", true);
        if (urls != null) return;
        urls = readUrls(dir + "robots");
    }

    public void init(ServletConfig config)
        throws ServletException {
        super.init(config);
        clientRobot = this;
        messages = W3URLConnection.getMessages("FI.realitymodeler.server.resources.Messages");
        String s;
        if ((s = getInitParameter("entryLifeSpan")) != null) entryLifeSpan = Long.parseLong(s) * 60L * 60L * 1000L;
        String dir = getInitParameter("directory");
        ServletContext context = getServletContext();
        if (context instanceof W3Server) {
            W3Server w3 = (W3Server)context;
            if (dir == null) dir = w3.dataDirectory;
            verbose = w3.verbose;
        }
        if (dir == null) dir = "";
        else if (!dir.endsWith("/")) dir += "/";
        try {
            setRobots(dir);
        } catch (Exception ex) {
            throw new ServletException(Support.stackTrace(ex));
        }
    }

    public void doGet(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException {
        LdapContext ldapContext[] = new LdapContext[1], clientRobotContext = null;
        try {
            clientRobotContext = W3Server.open(this.getClass().getName(), ldapContext);
            String query = Support.getParameterValue(req.getParameterValues("query")),
                refresh = Support.getParameterValue(req.getParameterValues("refresh"));
            if (query == null) {
                res.setContentType(Support.htmlType);
                PrintWriter writer = res.getWriter();
                writer.println("<html><head><title>" +
                               messages.getString("callClientRobot") + "</title></head><body>");
                writer.println("<h1>" + messages.getString("callClientRobot") + "</h1>");
                writer.println(messages.getString("clientRobotOrders1"));
                writer.println(messages.getString("clientRobotOrders2"));
                writer.println("<form action=\"" + req.getServletPath() + "\" method=get>");
                writer.println("<input name=query type=text value=\"\">");
                writer.println("<input type=submit value=\"" + messages.getString("search") + "\">");
                writer.print("</form></body></html>");
                return;
            }
            if ((query = query.trim()).equals("")) {
                NamingEnumeration nodeResultEnum = clientRobotContext.search("", "(objectClass=*)", new SearchControls(SearchControls.OBJECT_SCOPE, 1L, 0, null, false, true));
                if (!nodeResultEnum.hasMoreElements()) {
                    res.sendError(HttpServletResponse.SC_NOT_FOUND, messages.getString("databaseIsEmpty"));
                    return;
                }
                if (refresh != null) res.setHeader("Refresh", refresh);
                res.sendRedirect(((SearchResult)nodeResultEnum.nextElement()).getName());
                return;
            }
            String keyword, delims = "()&|!";
            StringBuffer sb = new StringBuffer();
            StringTokenizer st = new StringTokenizer(query, delims, true);
            Vector<String> keywords = new Vector<String>();
            while (st.hasMoreTokens()) {
                String token = st.nextToken();
                if (token.length() == 1 && delims.indexOf(token.charAt(0)) != -1) {
                    sb.append(token);
                    continue;
                }
                sb.append("(registeredAddress=").append(Support.escapeFilter((token = token.toUpperCase()))).append(')');
                if (!keywords.contains(token)) keywords.addElement(token);
            }
            fetch(clientRobotContext, (Vector<URL>)urls.clone(), keywords, true);
            NamingEnumeration resultEnum = clientRobotContext.search("", sb.toString(), new SearchControls(SearchControls.ONELEVEL_SCOPE, 0L, 0, new String[] {"description"}, false, true));
            res.setContentType(Support.htmlType);
            PrintWriter writer = res.getWriter();
            String title = MessageFormat.format(messages.getString("resultsFromClientRobotOfQuery"), new Object[] {query});
            writer.println("<html><head><title>" + title + "</title></head><body><h3>" + title + "</h3>");
            int n = 0;
            String encoding = req.getCharacterEncoding();
            if (encoding == null) encoding = "8859_1";
            while (resultEnum.hasMore()) {
                String url = (String)((SearchResult)resultEnum.next()).getAttributes().get("ou").get();
                writer.println("<a href=\"" + URLEncoder.encode(url, encoding) + "\">" + Support.htmlString(url) + "</a><br>");
                n++;
            }
            writer.println("<p>" + MessageFormat.format(messages.getString("numberHits"), new Object[] {new Integer(n)}));
            writer.print("</body></html>");
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
            return;
        } catch (NamingException ex) {
            throw new ServletException(Support.stackTrace(ex));
        } catch (ParseException ex) {
            throw new ServletException(Support.stackTrace(ex));
        } finally {
            W3Server.close(clientRobotContext, ldapContext);
        }
        if (System.currentTimeMillis() - cleaningTime > entryLifeSpan) new CleaningThread().start();
    }

    public void doHead(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException {
        doGet(req, res);
    }

    public void run() {
        LdapContext ldapContext[] = new InitialLdapContext[1], clientRobotContext = null;
        try {
            if (!sending && keywords != null) clientRobotContext = W3Server.open(this.getClass().getName(), ldapContext);
            fetch(clientRobotContext, (Vector<URL>)urls.clone(), keywords, false);
        } catch (Exception ex) {
            if (W3URLConnection.logging) log(Support.stackTrace(ex));
        } finally {
            W3Server.close(clientRobotContext, ldapContext);
        }
    }

    public static void main(String argv[])
        throws Exception {
        ClientRobot cr = null;
        String arg, username = null, password = null;
        URL url = null;
        boolean keywords = false, send = false, stay = false;
        int argn, n = 1, timeout = 0;
        W3Factory.prepare();
        W3Factory.setSocketFactories();
        URL.setURLStreamHandlerFactory(W3Factory.instance);
        W3Server.readSystemProperties(new File("system.properties"));
        W3Factory.getSystemProperties();
        cr = new ClientRobot();
        for (argn = 0; argn < argv.length; argn++) {
            arg = argv[argn];
            if (arg.charAt(0) == '-') {
                int i;
                for (i = 1; i < arg.length(); i++) {
                    switch (arg.charAt(i)) {
                    case '3':
                        W3Pop3URLConnection.useProxy = true;
                        W3Pop3URLConnection.proxyHost = argv[++argn];
                        W3Pop3URLConnection.proxyPort = Integer.parseInt(argv[++argn]);
                        continue;
                    case 'A':
                        cr.appendUserValues = true;
                        continue;
                    case 'C':
                        cr.accept = argv[++argn];
                        continue;
                    case 'D':
                        W3URLConnection.setDefaultRequestProperty(argv[++argn], argv[++argn]);
                        continue;
                    case 'E':
                        cr.forceResume = true;
                        cr.read = true;
                        continue;
                    case 'F':
                        cr.fillCaches = true;
                        cr.read = true;
                        continue;
                    case 'H':
                        cr.requestFreshCopy = true;
                        continue;
                    case 'N':
                        W3NntpURLConnection.useProxy = true;
                        W3NntpURLConnection.proxyHost = argv[++argn];
                        W3NntpURLConnection.proxyPort = Integer.parseInt(argv[++argn]);
                        continue;
                    case 'P':
                        cr.nameParts = cr.readList(argv[++argn], true);
                        continue;
                    case 'R':
                        cr.traceForms = true;
                        continue;
                    case 'S':
                        W3SmtpURLConnection.useProxy = true;
                        W3SmtpURLConnection.proxyHost = argv[++argn];
                        W3SmtpURLConnection.proxyPort = Integer.parseInt(argv[++argn]);
                        continue;
                    case 'U':
                        cr.urls = cr.readUrls(argv[++argn]);
                        continue;
                    case 'a':
                        cr.credentials = W3URLConnection.basicCredentials(argv[++argn], argv[++argn]);
                        continue;
                    case 'b':
                        cr.branchPaths = cr.readList(argv[++argn], false);
                        continue;
                    case 'c':
                        cr.sendPaths = cr.readList(argv[++argn], false);
                        continue;
                    case 'e':
                        cr.sending = send = true;
                        continue;
                    case 'd':
                        cr.sending = true;
                        continue;
                    case 'f':
                        cr.caches.put("file", Boolean.TRUE);
                        cr.caches.put("ftp", Boolean.TRUE);
                        continue;
                    case 'g':
                        cr.caches.put("gopher", Boolean.TRUE);
                        continue;
                    case 'h':
                        cr.caches.put("http", Boolean.TRUE);
                        cr.caches.put("https", Boolean.TRUE);
                        continue;
                    case 'i':
                        cr.localAddress = InetAddress.getByName(argv[++argn]);
                        cr.localPort = Integer.parseInt(argv[++argn]);
                        continue;
                    case 'j':
                        W3GopherURLConnection.useProxy = true;
                        W3GopherURLConnection.proxyHost = argv[++argn];
                        W3GopherURLConnection.proxyPort = Integer.parseInt(argv[++argn]);
                        continue;
                    case 'k':
                        keywords = true;
                        continue;
                    case 'l':
                        cr.test = true;
                        continue;
                    case 'm':
                        cr.handling = true;
                        cr.userForms = cr.readUserForms(argv[++argn]);
                        continue;
                    case 'n':
                        n = Integer.parseInt(argv[++argn]);
                        continue;
                    case 'o':
                        W3HttpURLConnection.useProxy = true;
                        W3HttpURLConnection.proxyHost = argv[++argn];
                        W3HttpURLConnection.proxyPort = Integer.parseInt(argv[++argn]);
                        continue;
                    case 'p':
                        cr.usePost = true;
                        continue;
                    case 'q':
                        cr.type = true;
                        continue;
                    case 'r':
                        cr.read = true;
                        continue;
                    case 's':
                        stay = true;
                        continue;
                    case 't':
                        cr.trace = true;
                        continue;
                    case 'u':
                        W3FtpURLConnection.useProxy = true;
                        W3FtpURLConnection.proxyHost = argv[++argn];
                        W3FtpURLConnection.proxyPort = Integer.parseInt(argv[++argn]);
                        continue;
                    case 'w':
                        cr.delay = Long.parseLong(argv[++argn]) * 1000L;
                        continue;
                    case 'x':
                        cr.force = cr.read = true;
                        continue;
                    case 'y':
                        timeout = Integer.parseInt(argv[++argn]) * 1000;
                        continue;
                    case 'z':
                        W3URLConnection.setCacheHeaders(true);
                        continue;
                    }
                    break;
                }
                if (i == arg.length()) continue;
                break;
            } else {
                if (keywords || cr.urls != null) {
                    if (cr.keywords == null) cr.keywords = new Vector<String>();
                    if (cr.handling || cr.sending) cr.keywords.addElement(argv[argn]);
                    else cr.keywords.addElement(argv[argn].toUpperCase());
                } else {
                    if (cr.urls == null) cr.urls = new Vector<URL>();
                    cr.urls.addElement(url = new URL(context, argv[argn]));
                }
                continue;
            }
        }
        if (argn < argv.length || cr.keywords == null && cr.urls == null) {
            System.out.println("Give url and optionally keywords as parameters.\n" +
                               "If keywords are not given, only client simulation is taken place.\n"+
                               "Available options (case sensitive):\n" +
                               "-3 <pop3 proxy> <port>\n" +
                               "-A : append remaining user values to query\n" +
                               "-C <accept content types> (text/html, */*)\n" +
                               "-D <header name> <default value>\n" +
                               "-E : force resuming to cache filling\n" +
                               "-F : fill only caches\n" +
                               "-H : request fresh copy from origin server\n" +
                               "-N <nntp proxy> <port>\n" +
                               "-P <file listing parts of name collected protocol://host -names must contain or must not contain if part is prefixed with !>\n" +
                               "-R : trace only form handling\n" +
                               "-S <smtp proxy> <port>\n" +
                               "-U <file listing root urls>\n" +
                               "-a <username> <password>\n" +
                               "-b <file listing paths branched urls should start with or not start with if path is prefixed with !>\n" +
                               "-c <file listing paths sending urls should start with or not start with if path is prefixed with !>\n" +
                               "-d : send files given in place of keywords to urls found in given url\n" +
                               "-e : send files given in place of keywords to url\n" +
                               "-f : set ftp proxy cache on (off)\n" +
                               "-g : set gopher proxy cache on (off)\n" +
                               "-h : set http proxy cache on (off)\n" +
                               "-i <local ip address to bind> <local port>\n" +
                               "-j <gopher proxy> <port>\n" +
                               "-k : only keywords given and they are searched from robots\n" +
                               "-l : test all links\n" +
                               "-m <form values file containing action paths with list of name=value pairs>\n" +
                               "-n <number of threads in client simulation>\n" +
                               "-o <http proxy> <port>\n" +
                               "-p : use POST-method when sending files\n" +
                               "-q : type files to standard output\n" +
                               "-r : read all files so that cache gets filled\n" +
                               "-s : stay only under given url\n" +
                               "-t : trace only and do not actually send\n" +
                               "-u <ftp proxy> <port>\n" +
                               "-w <delay in seconds between requests>\n" +
                               "-x : force cache filling with dynamic content\n" +
                               "-y : <socket read timeout in secs> (0)\n" +
                               "-z : set cache header storing on (off)");
            return;
        }
        W3URLConnection.setFollowRedirects(true);
        W3URLConnection.setFileNameMap(MimeMap.readMimeMap("mime_types", null));
        cr.verbose = cr.standalone = true;
        W3URLConnection.maxCacheEntryLength = -1;
        W3FtpURLConnection.cacheCleaningInterval = -1L;
        W3GopherURLConnection.cacheCleaningInterval = -1L;
        W3HttpURLConnection.cacheCleaningInterval = -1L;
        W3URLConnection.setLogStream(System.out, true);
        cr.timeout = timeout;
        if (keywords) cr.setRobots("");
        if (stay) {
            String urlKey = url.toString();
            urlKey = urlKey.substring(0, urlKey.lastIndexOf('/') + 1);
            if (cr.nameParts == null) cr.nameParts = new RegexpPool[2];
            cr.nameParts[0] = new RegexpPool();
            cr.nameParts[0].add(urlKey + "*", urlKey);
        }
        if (cr.sending && cr.keywords == null) {
            System.out.println("No files given for sending");
            return;
        }
        if (send) {
            W3URLConnection uc = null;
            while (--n >= 0) uc = cr.send(uc, cr.urls, 0, null, cr.keywords);
            System.exit(0);
        }
        Vector<Thread> threads = new Vector<Thread>();
        while (--n >= 0) {
            Thread thread = new Thread(cr);
            threads.addElement(thread);
            thread.start();
        }
        Runtime.getRuntime().addShutdownHook(new ClientRobotShutdownHook(threads));
    }

}
