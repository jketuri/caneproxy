
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import java.util.*;

/** W3Server management frontend, which is started as follows:<br>
    <b>java FI.realitymodeler.server.Management [options] [&lt;server host&gt;]</b><br>
    Parameter gives host name of the server and defaults to local machine.
    Allowed options are: (default value is in parantheses)<br>
    <b>-f : clean ftp proxy cache and exit</b><br>
    This cleans all files accessed via ftp-proxy and then exits from
    the management program. This option is usable in scheduled cleaning
    operations. It must be noted that machine where operations are
    scheduled needs not be the same where server runs. Cleaning proxy
    cache means that all files which haven't been accessed since last
    cleaning are deleted.<br>
    <b>-g : clean gopher proxy cache and exit</b><br>
    This cleans all files accessed via gopher-proxy and then exits from
    the management program. Cleaning is done in the same manner as with
    ftp-proxy.<br>
    <b>-h : clean http proxy cache and exit</b><br>
    This cleans all files accessed via http-proxy and then exits from
    the management program. Cleaning is done in the same manner as with
    ftp-proxy.<br>
    <b>-m &lt;username&gt; &lt;password&gt;</b><br>
    Gives manager username and password.<br>
    <b>-p &lt;port number&gt; (80 or 443 when secure)</b><br>
    Gives port number of the server host.<br>
    <b>-s : secure on (off)</b><br>
    This sets secure mode on.<br>
    <b>-t : stop server and exit (works only from server host)</b><br>
    This stops server. Program must be started in server host machine
    in the data directory.<br>
    <b>-y &lt;socket read timeout in seconds&gt; (60)</b><br>
    Gives the read timeout for socket in seconds. */
public class Management extends Frame implements ActionListener, ItemListener {
    static final long serialVersionUID = 0L;
    public static int timeout = 60000;

    static String categories[] = {
        "User database",
        "Domains",
        "Servlets",
        "MIME types",
        "Virtual paths",
        "Cache paths"
    };
    static String titles[][] = { {
            "Operations",
            "Clean ftp cache",
            "Clean gopher cache",
            "Clean http cache",
            "Empty server log",
            "Read data file",
            "Write data file",
            "Reload parameters"
        }
    };
    static String fieldNames[][] = { {
            "Username",
            "Password",
            "Domains",
            "Name",
            "Profession",
            "Mobile telephone number",
            "Fax number",
            "E-mail address",
            "Post address",
            "Participation in the user groups",
            "Age",
            "Sex (M/F)"
        }, {
            "Domain name",
            "Domain paths",
            "Authentication servlet",
            "Secondary domain name"
        }, {
            "Servlet name",
            "Servlet class",
            "Parameter name",
            "Parameter value"
        }, {
            "MIME name",
            "File extensions",
            "Target MIME type",
            "Filter servlet"
        }, {
            "Virtual path",
            "Physical path"
        }, {
            "Cache path"
        }
    };
    static int fieldLengths[][] = { {
            25,
            13,
            25,
            50,
            50,
            50,
            50,
            50,
            50,
            50,
            3,
            1
        }, {
            -25,
            -50,
            -25,
            -25
        }, {
            -25,
            -50,
            -25,
            -50
        }, {
            -25,
            -50,
            -25,
            -25
        }, {
            -25,
            -50
        }, {
            -50
        }
    };
    String buttonNames[][] = { {
            "Save user",
            "Search user",
            "First user",
            "Next user",
            "Remove user",
        }, {
            "Get domain",
            "Put domain",
            "First domain",
            "Next domain",
            "Drop domain",
            "Save domains"
        }, {
            "Get servlet",
            "Put servlet",
            "First servlet",
            "Next servlet",
            "Save servlets",
            "Put servlet parameter",
            "Drop servlet parameters"
        }, {
            "Get type",
            "Put type",
            "First type",
            "Next type",
            "Drop type",
            "Save types",
            "Put filter servlet",
            "Drop filter servlet"
        }, {
            "Get virtual",
            "Put virtual",
            "First virtual",
            "Next virtual",
            "Drop virtual",
            "Save virtuals"
        }, {
            "Get path",
            "Put path",
            "First path",
            "Next path",
            "Drop path",
            "Save paths"
        }
    };
    static String caches[] = {
        "ftp",
        "gopher",
        "http"
    };
    static int UC, USERNAME, PASSWORD, CERTIFICATE, PORT, MESSAGE, OUT, IN, CONTROL_SIZE;
    static {
        int i = 0;
        UC = i++;
        USERNAME = i++;
        PASSWORD = i++;
        CERTIFICATE = i++;
        PORT = i++;
        MESSAGE = i++;
        OUT = i++;
        IN = i++;
        CONTROL_SIZE = i;
    }

    private transient Button buttons[][], browseButton, firstUserButton, nextUserButton, removeUserButton, saveUserButton, searchUserButton;
    private transient Checkbox isNative, isDisabled;
    private transient Choice choice, cacheChoice;
    private transient Label labels[][];
    private transient java.awt.List initParameterList, filterServletList;
    private transient Menu menus[];
    private transient MenuBar menuBar;
    private transient Object control[];
    private transient W3HttpURLConnection uc;
    private transient Panel cards, fieldArea[], buttonArea[], panels[];
    private transient TextArea message;
    private transient TextField bytesField, fields[][], filenameField, portField, sizeField;

    public Management(Object control[])
        throws IOException {
        setSize(800, 800);
        this.control = control;
        uc = (W3HttpURLConnection)control[UC];
        setTitle("W3Server Management");
        menuBar = new MenuBar();
        menus = new Menu[titles.length];
        for (int i = 0; i < titles.length; i++) {
            menus[i] = new Menu(titles[i][0]);
            for (int j = 1; j < titles[i].length; j++) {
                MenuItem menuItem = new MenuItem(titles[i][j]);
                menus[i].add(menuItem);
                menuItem.addActionListener(this);
            }
            menuBar.add(menus[i]);
        }
        setMenuBar(menuBar);
        cards = new Panel();
        cards.setLayout(new CardLayout());
        choice = new Choice();
        choice.addItemListener(this);
        GridBagLayout gbl = new GridBagLayout();
        panels = new Panel[categories.length];
        fieldArea = new Panel[categories.length];
        buttonArea = new Panel[categories.length];
        labels = new Label[categories.length][];
        fields = new TextField[categories.length][];
        buttons = new Button[categories.length][];
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.anchor = gbc.WEST;
        gbc.weightx = gbc.weighty = 1;
        for (int i = 0; i < categories.length; i++) {
            panels[i] = new Panel();
            panels[i].setLayout(new BorderLayout());
            cards.add(categories[i], panels[i]);
            choice.addItem(categories[i]);
            fieldArea[i] = new Panel();
            fieldArea[i].setLayout(gbl);
            panels[i].add("North", fieldArea[i]);
            buttonArea[i] = new Panel();
            panels[i].add("South", buttonArea[i]);
            labels[i] = new Label[fieldNames[i].length];
            fields[i] = new TextField[fieldNames[i].length];
            for (int j = 0; j < fieldNames[i].length; j++) {
                labels[i][j] = new Label(fieldNames[i][j]);
                if (fieldLengths[i][j] > 0) fields[i][j] = new LimitTextField(fieldLengths[i][j]);
                else fields[i][j] = new TextField(-fieldLengths[i][j]);
                gbc.gridx = 0;
                gbc.gridy = j;
                gbl.setConstraints(labels[i][j], gbc);
                gbc.gridx = 1;
                gbl.setConstraints(fields[i][j], gbc);
                fieldArea[i].add(labels[i][j]);
                fieldArea[i].add(fields[i][j]);
            }
            gbc.gridy = fieldNames[i].length + 1;
            buttons[i] = new Button[buttonNames[i].length];
            for (int j = 0; j < buttons[i].length; j++) {
                buttons[i][j] = new Button(buttonNames[i][j]);
                buttons[i][j].addActionListener(this);
                gbc.gridx = j;
                gbl.setConstraints(buttons[i][j], gbc);
                buttonArea[i].add(buttons[i][j]);
            }
        }
        Panel p = new Panel();
        isNative = new Checkbox(":native");
        p.add(isNative);
        isDisabled = new Checkbox(":disabled");
        p.add(isDisabled);
        gbc.gridx = 1;
        gbc.gridy = 4;
        gbl.setConstraints(p, gbc);
        fieldArea[2].add(p);
        (initParameterList = new java.awt.List()).setMultipleMode(true);
        initParameterList.addItemListener(this);
        panels[2].add("Center", initParameterList);
        (filterServletList = new java.awt.List()).setMultipleMode(true);
        panels[3].add("Center", filterServletList);
        cacheChoice = new Choice();
        for (int i = 0; i < caches.length; i++) cacheChoice.addItem(caches[i]);
        p = new Panel();
        p.add(cacheChoice);
        panels[5].add("Center", p);
        fields[0][1].setEchoChar('#');
        p = new Panel();
        p.setLayout(new GridLayout(4, 1));
        Panel p1 = new Panel();
        p1.add(new Label("Port:"));
        p1.add(portField = new TextField(control[PORT].toString(), 10));
        p.add(p1);
        p1 = new Panel();
        p1.add(new Label("Category:"));
        p1.add(choice);
        p.add(p1);
        p1 = new Panel();
        p1.add(new Label("Filename:"));
        filenameField = new TextField(40);
        filenameField.addActionListener(this);
        p1.add(filenameField);
        browseButton = new Button("Browse");
        browseButton.addActionListener(this);
        p1.add(browseButton);
        p.add(p1);
        p1 = new Panel();
        p1.add(new Label("Bytes:"));
        bytesField = new TextField(10);
        bytesField.setEditable(false);
        p1.add(bytesField);
        p1.add(new Label("Size:"));
        sizeField = new TextField(10);
        sizeField.setEditable(false);
        p1.add(sizeField);
        p.add(p1);
        add("North", p);
        message = new TextArea(10, 80);
        message.setEditable(false);
        add("Center", cards);
        add("South", message);
        control[MESSAGE] = message;
        addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    System.exit(0);
                }
            });
        ((CardLayout)cards.getLayout()).show(cards, "User database");
    }

    /** Calls W3Manager-servlet. */
    public static ObjectInputStream call(Object control[], String action, Object parameter, boolean returnValue)
        throws Exception {
        TextArea message = (TextArea)control[MESSAGE];
        if (message != null) message.setText("");
        W3HttpURLConnection uc = (W3HttpURLConnection)control[UC];
        uc.set(uc.getURL());
        uc.setRequestMethod("PUT");
        OutputStream out = uc.getOutputStream();
        uc.closeStreams();
        InputStream in = uc.getInputStream();
        String auth = uc.getHeaderField("www-authenticate");
        uc.disconnect();
        if (auth == null) return null;
        Map<String,String> args = new HashMap<String,String>();
        Support.getArguments(auth, args);
        uc.set(uc.getURL());
        uc.setRequestMethod("PUT");
        uc.setDigestCredentials((String)control[USERNAME], (String)control[PASSWORD], args.get("realm"), "PUT", args.get("nonce"), args.get("domain"));
        control[OUT] = out = uc.getOutputStream();
        ObjectOutputStream oout = new ObjectOutputStream(out);
        oout.writeInt(((Integer)control[PORT]).intValue());
        oout.writeObject(action.toLowerCase());
        oout.writeObject(parameter);
        oout.flush();
        out.flush();
        control[IN] = in = uc.getInputStream();
        ObjectInputStream oin = new ObjectInputStream(in);
        if (oin.readBoolean() && returnValue) return oin;
        String s = (String)oin.readObject();
        if (message != null) message.setText(s);
        System.out.println(s);
        return null;
    }

    void setFields(ObjectInputStream oin, int category)
        throws Exception {
        if (oin == null) return;
        try {
            switch (category) {
            case 0: {
                Object userFields[] = (Object[])oin.readObject();
                for (int i = 0; i < 10; i++) fields[0][i].setText((String)userFields[i]);
                fields[0][10].setText(String.valueOf(((Integer)userFields[10]).intValue()));
                int sex = ((Integer)userFields[11]).intValue();
                fields[0][11].setText(sex == 0 ? "" : sex == 1 ? "M" : "F");
                return;
            }
            case 2: {
                Object objs[] = (Object[])oin.readObject();
                fields[2][0].setText((String)objs[0]);
                fields[2][1].setText((String)objs[1]);
                fields[2][2].setText("");
                fields[2][3].setText("");
                isNative.setState(((Boolean)objs[3]).booleanValue());
                isDisabled.setState(((Boolean)objs[4]).booleanValue());
                Hashtable initParameters = (Hashtable)objs[2];
                initParameterList.removeAll();
                if (initParameters == null) return;
                Iterator initParameterIter = initParameters.entrySet().iterator();
                while (initParameterIter.hasNext()) {
                    Map.Entry entry = (Map.Entry)initParameterIter.next();
                    initParameterList.add(entry.getKey() + "=" + entry.getValue());
                }
                return;
            }
            case 4: {
                String str[] = (String[])oin.readObject();
                fields[category][0].setText(str[0]);
                fields[category][1].setText(str[1]);
                return;
            }
            case 5: {
                fields[5][0].setText((String)oin.readObject());
                return;
            }
            }
            Object objs[] = (Object[])oin.readObject();
            Enumeration e = ((Vector)objs[1]).elements();
            StringBuffer sb = new StringBuffer();
            for (;;) {
                sb.append((String)e.nextElement());
                if (!e.hasMoreElements()) break;
                sb.append(' ');
            }
            fields[category][0].setText((String)objs[0]);
            fields[category][1].setText(sb.toString());
            switch (category) {
            case 1: {
                fields[1][2].setText((String)objs[2]);
                fields[1][3].setText((String)objs[3]);
                return;
            }
            case 3: {
                fields[3][2].setText("");
                fields[3][3].setText("");
                Vector vector = (Vector)objs[2];
                filterServletList.removeAll();
                e = vector.elements();
                while (e.hasMoreElements()) filterServletList.add((String)e.nextElement());
                return;
            }
            }
        } finally {
            oin.close();
        }
    }

    void setFields(int category, java.awt.List list, ItemEvent e) {
        StringTokenizer st = new StringTokenizer(list.getItem(((Integer)e.getItem()).intValue()), "=");
        for (int i = 2; i < 4; i++) fields[category][i].setText(st.nextToken().trim());
    }

    Object[] getFields(int category) {
        switch (category) {
        case 2: {
            Hashtable<String,String> initParameters = new Hashtable<String,String>();
            int n = initParameterList.getItemCount();
            for (int i = 0; i < n; i++) {
                StringTokenizer st = new StringTokenizer(initParameterList.getItem(i), "=");
                initParameters.put(st.nextToken().trim(), st.nextToken().trim());
            }
            return new Object[] {fields[2][0].getText().trim(), fields[2][1].getText().trim(), initParameters, new Boolean(isNative.getState()), new Boolean(isDisabled.getState())};
        }
        case 4:
            return new String[] {fields[category][0].getText().trim(), fields[category][1].getText().trim()};
        case 5:
            return new String[] {cacheChoice.getSelectedItem(), fields[5][0].getText().trim()};
        }
        Vector<String> vector = new Vector<String>();
        StringTokenizer st = new StringTokenizer(fields[category][1].getText());
        while (st.hasMoreTokens()) vector.addElement(st.nextToken());
        switch (category) {
        case 1:
            return new Object[] {fields[category][0].getText().trim(), vector, fields[category][2].getText().trim(), fields[category][3].getText().trim()};
        case 3: {
            Vector<String> vector1 = new Vector<String>();
            int n = filterServletList.getItemCount();
            for (int i = 0; i < n; i++) vector1.addElement(filterServletList.getItem(i));
            return new Object[] {fields[3][0].getText().trim(), vector, vector1};
        }
        }
        return new Object[] {fields[category][0].getText().trim(), vector};
    }

    void putItem(int category, java.awt.List list) {
        int n = list.getItemCount();
        String name = fields[category][2].getText().trim(),
            item = name + (category == 3 ? ":" : "=") + fields[category][3].getText().trim();
        if (category == 3) {
            int sis[] = list.getSelectedIndexes();
            if (sis.length > 0) {
                list.add(item, sis[0]);
                return;
            }
        }
        else
            for (int i = 0; i < n; i++)
                if (list.getItem(i).startsWith(name)) {
                    list.remove(i);
                    break;
                }
        list.add(item);
    }

    void dropItems(java.awt.List list) {
        int sis[] = list.getSelectedIndexes();
        for (int i = 0; i < sis.length; i++) list.remove(sis[i] - i);
    }

    public void actionPerformed(ActionEvent e) {
        try {
            if (e.getSource() == browseButton) {
                FileDialog dialog = new FileDialog(this, "Choose the file");
                dialog.setVisible(true);
                if (dialog.getDirectory() == null || dialog.getFile() == null) return;
                File file = new File(dialog.getDirectory(), dialog.getFile());
                filenameField.setText(file.getCanonicalPath());
                sizeField.setText(String.valueOf(file.length()));
                return;
            }
            if (e.getSource() == filenameField) {
                File file = new File(filenameField.getText());
                sizeField.setText(file.exists() ? String.valueOf(file.length()) : "File not found");
                return;
            }
            String command = e.getActionCommand();
            control[PORT] = new Integer(portField.getText());
            if (command.equals("Save user")) {
                int i;
                byte age, sex;
                for (i = 0; i < 3; i++)
                    if (fields[0][i].getText().trim().equals("")) {
                        message.setText("Give user name, password and domain");
                        return;
                    }
                if (fields[0][10].getText().trim().equals("")) age = 0;
                else try {
                        age = (byte)Integer.parseInt(fields[0][10].getText());
                    } catch (Exception ex) {
                        message.setText("Age is not a valid number");
                        return;
                    }
                String s = fields[0][11].getText().trim().toUpperCase();
                if (s.equals("")) sex = 0;
                else if (s.equals("M")) sex = 1;
                else if (s.equals("F")) sex = 2;
                else {
                    message.setText("Sex must be M or F (for male and female)");
                    return;
                }
                Object userFields[] = new Object[12];
                for (i = 0; i < 10 ; i++) userFields[i] = fields[0][i].getText().trim();
                userFields[10] = new Integer(age);
                userFields[11] = new Integer(sex);
                call(control, command, userFields, false);
            } else if (command.equals("Remove user")) call(control, command, fields[0][0].getText(), false);
            else if (command.equals("Search user")) setFields(call(control, command, fields[0][0].getText(), true), 0);
            else if (command.equals("First user")) setFields(call(control, command, null, true), 0);
            else if (command.equals("Next user")) setFields(call(control, command, null, true), 0);
            else if (command.equals("Get domain")) setFields(call(control, command, fields[1][0].getText().trim(), true), 1);
            else if (command.equals("Put domain")) call(control, command, getFields(1), false);
            else if (command.equals("Drop domain")) call(control, command, fields[1][0].getText().trim(), false);
            else if (command.equals("First domain")) setFields(call(control, command, null, true), 1);
            else if (command.equals("Next domain")) setFields(call(control, command, null, true), 1);
            else if (command.equals("Save domains")) call(control, command, null, false);
            else if (command.equals("Get servlet")) setFields(call(control, command, fields[2][0].getText().trim(), true), 2);
            else if (command.equals("First servlet")) setFields(call(control, command, null, true), 2);
            else if (command.equals("Next servlet")) setFields(call(control, command, null, true), 2);
            else if (command.equals("Put servlet")) call(control, command, getFields(2), false);
            else if (command.equals("Save servlets")) call(control, command, null, false);
            else if (command.equals("Put servlet parameter")) putItem(2, initParameterList);
            else if (command.equals("Drop servlet parameters")) dropItems(initParameterList);
            else if (command.equals("Get type")) setFields(call(control, command, fields[3][0].getText().trim(), true), 3);
            else if (command.equals("Put type")) call(control, command, getFields(3), false);
            else if (command.equals("Drop type")) call(control, command, fields[3][0].getText().trim(), false);
            else if (command.equals("First type")) setFields(call(control, command, null, true), 3);
            else if (command.equals("Next type")) setFields(call(control, command, null, true), 3);
            else if (command.equals("Save types")) call(control, command, null, false);
            else if (command.equals("Put filter servlet")) putItem(3, filterServletList);
            else if (command.equals("Drop filter servlet")) dropItems(filterServletList);
            else if (command.equals("Get virtual")) setFields(call(control, command, fields[4][0].getText().trim(), true), 4);
            else if (command.equals("Put virtual")) call(control, command, getFields(4), false);
            else if (command.equals("Drop virtual")) call(control, command, fields[4][0].getText().trim(), false);
            else if (command.equals("First virtual")) setFields(call(control, command, null, true), 4);
            else if (command.equals("Next virtual")) setFields(call(control, command, null, true), 4);
            else if (command.equals("Save virtuals")) call(control, command, null, false);
            else if (command.equals("Put path")) call(control, command, getFields(5), false);
            else if (command.equals("Drop path")) call(control, command, getFields(5), false);
            else if (command.equals("First path")) setFields(call(control, command, cacheChoice.getSelectedItem(), true), 5);
            else if (command.equals("Next path")) setFields(call(control, command, null, true), 5);
            else if (command.equals("Save paths")) call(control, command, cacheChoice.getSelectedItem(), false);
            else if (command.equals("Clean ftp cache")) call(control, command, null, false);
            else if (command.equals("Clean gopher cache")) call(control, command, null, false);
            else if (command.equals("Clean http cache")) call(control, command, null, false);
            else if (command.equals("Empty server log")) call(control, command, null, false);
            else if (command.equals("Read data file")) {
                String s = filenameField.getText();
                if (call(control, command, s, true) != null) {
                    InputStream in = (InputStream)control[IN];
                    FileOutputStream fout = new FileOutputStream(s);
                    try {
                        byte b[] = new byte[Support.bufferLength];
                        int l = 0, n;
                        while ((n = in.read(b)) > 0) {
                            fout.write(b, 0, n);
                            bytesField.setText(String.valueOf(l += n));
                        }
                        s += " read";
                        message.setText(s);
                        System.out.println(s);
                    } finally {
                        fout.close();
                    }
                }
            } else if (command.equals("Write data file")) {
                String s = filenameField.getText();
                if (call(control, command, s, true) != null) {
                    sizeField.setText(String.valueOf(new File(s).length()));
                    OutputStream out = (OutputStream)control[OUT];
                    FileInputStream fin = new FileInputStream(s);
                    try {
                        byte b[] = new byte[Support.bufferLength];
                        int l = 0, n;
                        while ((n = fin.read(b)) > 0) {
                            out.write(b, 0, n);
                            bytesField.setText(String.valueOf(l += n));
                        }
                        out.flush();
                        s += " written";
                        message.setText(s);
                        System.out.println(s);
                    } finally {
                        fin.close();
                    }
                }
            } else if (command.equals("Reload parameters")) call(control, command, null, false);
        } catch (Exception ex) {
            message.setText(Support.stackTrace(ex));
            ex.printStackTrace();
        } finally {
            ((W3HttpURLConnection)control[UC]).disconnect();
        }
    }

    public void itemStateChanged(ItemEvent e) {
        Object source = e.getSource();
        if (source == choice) ((CardLayout)cards.getLayout()).show(cards, (String)e.getItem());
        else if (source == initParameterList) setFields(2, initParameterList, e);
    }

    public static void main(String argv[])
        throws Exception {
        File file = new File("./server.properties");
        Properties properties = new Properties();
        if (file.exists()) {
            InputStream in = new BufferedInputStream(new FileInputStream(file));
            try {
                properties.load(in);
            } finally {
                in.close();
            }
        }
        boolean secure = false, stop = false;
        int port = Integer.parseInt(properties.getProperty("port", "-1"));
        String host = null, credentials = properties.getProperty("managerCredentials", "manager:reganam");
        W3URLConnection.setLogStream(System.out, true);
        Vector<String> queue = new Vector<String>();
        if (argv != null)
            for (int argn = 0; argn < argv.length; argn++) {
                String arg = argv[argn];
                if (arg != null) {
                    if (arg.charAt(0) == '-') {
                        int i;
                        for (i = 1; i < arg.length(); i++) {
                            switch (Character.toLowerCase(arg.charAt(i))) {
                            case 'f':
                                queue.addElement("clean ftp cache");
                                continue;
                            case 'g':
                                queue.addElement("clean gopher cache");
                                continue;
                            case 'h':
                                queue.addElement("clean http cache");
                                continue;
                            case 'l': {
                                W3Server w3 = new W3Server();
                                w3.dataDirectory = ".";
                                w3.readProperties();
                                w3.setup(null, 0, null);
                                w3.verbose = true;
                                w3.logs[W3Server.EVENT_LOG].stream = System.out;
                                w3.simpleStats(w3, w3.dataDirectory + "/" + w3.logFilenames[w3.ACCESS_LOG], w3.dataDirectory + "log_stats.html");
                                return;
                            }
                            case 'm':
                                credentials = argv[++argn];
                                continue;
                            case 'o':
                                W3HttpURLConnection.proxyHost = argv[++argn];
                                W3HttpURLConnection.proxyPort = Integer.parseInt(argv[++argn]);
                                W3HttpURLConnection.useProxy = true;
                                continue;
                            case 'p':
                                port = Integer.parseInt(argv[++argn]);
                                continue;
                            case 's':
                                secure = true;
                                continue;
                            case 't':
                                stop = true;
                                continue;
                            case 'y':
                                timeout = Integer.parseInt(argv[++argn]) * 1000;
                                continue;
                            }
                            break;
                        }
                        if (i == arg.length()) continue;
                    } else {
                        host = argv[argn];
                        continue;
                    }
                    System.out.println("Give host name as a parameter.\n" +
                                       "Available options (default value in parantheses):\n" +
                                       "-f : clean ftp cache and exit\n" +
                                       "-g : clean gopher cache and exit\n" +
                                       "-h : clean http cache and exit\n" +
                                       "-l : generate simple log statistics and exit\n" +
                                       "-m <manager username>:<manager password>\n" +
                                       "-o <http proxy host> <proxy port>\n" +
                                       "-p <port number> (80 or 443 when secure)\n" +
                                       "-s : secure on (off)\n" +
                                       "-t : stop server and exit (works only from server host)\n" +
                                       "-y : <socket read timeout in seconds> (30)");
                    System.exit(0);
                }
            }
        if (host == null) host = InetAddress.getLocalHost().getHostAddress();
        if (port == -1) port = secure ? 443 : 80;
        if (secure) SecureSocketsLayer.setDefaultReadAhead(true);
        W3HttpURLConnection uc = new W3HttpURLConnection(new URL(secure ? "https" : "http", host, port, "/system/W3Manager"));
        if (timeout > 0) uc.setTimeout(timeout);
        Object control[] = new Object[CONTROL_SIZE];
        control[UC] = uc;
        StringTokenizer st = new StringTokenizer(credentials, ":");
        control[USERNAME] = st.nextToken();
        control[PASSWORD] = st.hasMoreTokens() ? st.nextToken() : "";
        control[PORT] = new Integer(port);
        if (!queue.isEmpty() || stop) {
            Enumeration<String> e = queue.elements();
            while (e.hasMoreElements()) call(control, e.nextElement(), null, false);
            if (stop)
                if (!InetAddress.getByName(host).equals(InetAddress.getLocalHost())) {
                    System.out.println("Stopping server is allowed only from server host");
                    System.exit(1);
                } else {
                    new FileOutputStream(W3Server.stopReallyFilename).close();
                    call(control, "stop server", null, false);
                }
            System.exit(0);
        }
        new Management(control).setVisible(true);
    }

}
