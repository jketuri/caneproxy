
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import java.io.*;
import java.text.*;
import java.util.*;

public class Stamp implements Serializable {
    static final long serialVersionUID = 0L;

    public static final int DAY_OF_YEAR = 0, DAY_OF_WEEK = 1, WEEK_OF_MONTH = 2, WEEK_OF_YEAR = 3,
        YEAR = 4, MONTH = 5, DAY = 6, HOUR = 7, MINUTE = 8, SECOND = 9,
        LONG_TIME = 10, SHORT_TIME = 11, NUM_OF_FORMATS = 12, NUM_OF_VALUES = 10;

    static int calFields[] = {Calendar.DAY_OF_YEAR, Calendar.DAY_OF_WEEK, Calendar.WEEK_OF_MONTH, Calendar.WEEK_OF_YEAR,
                              Calendar.YEAR, Calendar.MONTH, Calendar.DAY_OF_MONTH, Calendar.HOUR_OF_DAY, Calendar.MINUTE, Calendar.SECOND};
    static SimpleDateFormat fieldFormats[][];
    static String weekdays[], shortWeekdays[];
    static {
        try {
            fieldFormats = new SimpleDateFormat[NUM_OF_FORMATS][];
            fieldFormats[YEAR] = new SimpleDateFormat[] {new SimpleDateFormat("yyyy")};
            fieldFormats[MONTH] = new SimpleDateFormat[] {new SimpleDateFormat("MMM"), new SimpleDateFormat("MMMM"), new SimpleDateFormat("-MM")};
            fieldFormats[DAY] = new SimpleDateFormat[] {new SimpleDateFormat("d.")};
            fieldFormats[HOUR] = new SimpleDateFormat[] {new SimpleDateFormat("HH;")};
            fieldFormats[MINUTE] = new SimpleDateFormat[] {new SimpleDateFormat(";mm")};
            fieldFormats[SECOND] = new SimpleDateFormat[] {new SimpleDateFormat(";;ss")};
            fieldFormats[DAY_OF_YEAR] = new SimpleDateFormat[] {new SimpleDateFormat("(D)")};
            fieldFormats[DAY_OF_WEEK] = new SimpleDateFormat[] {new SimpleDateFormat("EEE"), new SimpleDateFormat("EEEE")};
            fieldFormats[WEEK_OF_YEAR] = new SimpleDateFormat[] {new SimpleDateFormat("{w}")};
            fieldFormats[WEEK_OF_MONTH] = new SimpleDateFormat[] {new SimpleDateFormat("[W]")};
            fieldFormats[LONG_TIME] = new SimpleDateFormat[] {new SimpleDateFormat("HH:mm:ss")};
            fieldFormats[SHORT_TIME] = new SimpleDateFormat[] {new SimpleDateFormat("HH:mm")};
            for (int i = 0; i < NUM_OF_FORMATS; i++)
                for (int j = 0; j < fieldFormats[i].length; j++) fieldFormats[i][j].setLenient(false);
            DateFormatSymbols dateFormatSymbols = fieldFormats[DAY_OF_WEEK][0].getDateFormatSymbols();
            weekdays = dateFormatSymbols.getWeekdays();
            shortWeekdays = dateFormatSymbols.getShortWeekdays();
        } catch (Exception ex) {
            throw new Error(ex.toString());
        }
    }

    public short values[] = new short[NUM_OF_VALUES];

    public static void reset(Stamp stamp) {
        for (int i = 0; i < NUM_OF_VALUES; i++) stamp.values[i] = -1;
    }

    public static Stamp parseStamp(String s, Stamp stamp) throws ParseException {
        reset(stamp);
        if (s.equals(".")) {
            Calendar cal = Calendar.getInstance();
            cal.add(cal.SECOND, 1);
            stamp.setValue(YEAR, cal.get(cal.YEAR));
            stamp.setValue(MONTH, cal.get(cal.MONTH));
            stamp.setValue(DAY, cal.get(cal.DAY_OF_MONTH));
            stamp.setValue(HOUR, cal.get(cal.HOUR_OF_DAY));
            stamp.setValue(MINUTE, cal.get(cal.MINUTE));
            stamp.setValue(SECOND, cal.get(cal.SECOND));
            return stamp;
        }
        Calendar cal = Calendar.getInstance();
        StringTokenizer st = new StringTokenizer(s);
        ParsePosition pos = new ParsePosition(0);
        for (int n = 1; st.hasMoreElements(); n++) {
            s = st.nextToken();
            boolean found = false;
            for (int i = 0; i < NUM_OF_FORMATS; i++)
                for (int j = 0; j < fieldFormats[i].length; j++) {
                    pos.setIndex(0);
                    Date d = fieldFormats[i][j].parse(s, pos);
                    if (d == null || pos.getIndex() < s.length()) continue;
                    cal.setTime(d);
                    found = true;
                    switch (i) {
                    case DAY_OF_WEEK: {
                        boolean tried = false;
                        for (String names[] = shortWeekdays;; names = weekdays) {
                            int value;
                            for (value = 0; value < names.length; value++)
                                if (s.equalsIgnoreCase(names[value])) {
                                    stamp.setValue(DAY_OF_WEEK, value);
                                    break;
                                }
                            if (tried || value < names.length) break;
                            tried = true;
                        }
                        break;
                    }
                    case MONTH:
                        stamp.setValue(MONTH, cal.get(cal.MONTH));
                        break;
                    case LONG_TIME:
                        stamp.setValue(HOUR, cal.get(cal.HOUR_OF_DAY));
                        stamp.setValue(MINUTE, cal.get(cal.MINUTE));
                        stamp.setValue(SECOND, cal.get(cal.SECOND));
                        break;
                    case SHORT_TIME:
                        stamp.setValue(HOUR, cal.get(cal.HOUR_OF_DAY));
                        stamp.setValue(MINUTE, cal.get(cal.MINUTE));
                        break;
                    default:
                        stamp.setValue(i, cal.get(calFields[i]));
                        break;
                    }
                }
            if (!found) throw new ParseException("Invalid field " + s, n);
        }
        return stamp;
    }

    public Stamp() {
        reset(this);
    }

    public Stamp(String s) throws ParseException {
        parseStamp(s, this);
    }

    public short[] getValues() {
        return values;
    }

    public int getValue(int index) {
        return values[index];
    }

    public void setValue(int index, int value) {
        values[index] = (short)value;
    }

    public boolean checkCal(Calendar cal) {
        for (int i = DAY_OF_YEAR; i <= HOUR; i++)
            if (getValue(i) != -1 && cal.get(calFields[i]) != getValue(i)) return false;
        return true;
    }

    public boolean changeCal(Calendar cal) {
        long time = cal.getTime().getTime();
        for (int i = DAY_OF_YEAR; i <= SECOND; i++)
            if (getValue(i) != -1) cal.set(calFields[i], getValue(i));
        if (getValue(DAY_OF_WEEK) != -1)
            for (int n = 0; cal.get(cal.DAY_OF_WEEK) != getValue(DAY_OF_WEEK) && n < 7; n++) cal.add(cal.DAY_OF_YEAR, 1);
        if (getValue(WEEK_OF_MONTH) != -1)
            for (int n = 0; cal.get(cal.WEEK_OF_MONTH) != getValue(WEEK_OF_MONTH) && n < 4; n++) cal.add(cal.WEEK_OF_YEAR, 1);
        if (checkCal(cal) && cal.getTime().getTime() > time) return true;
        if (getValue(DAY_OF_YEAR) != -1 || getValue(WEEK_OF_YEAR) != -1) cal.add(cal.YEAR, 1);
        else if (getValue(DAY_OF_WEEK) != -1) cal.add(cal.WEEK_OF_YEAR, 1);
        else if (getValue(WEEK_OF_MONTH) != -1) cal.add(cal.MONTH, 1);
        else if (getValue(DAY) == -1) cal.add(cal.DAY_OF_YEAR, 1);
        else if (getValue(MONTH) == -1) cal.add(cal.MONTH, 1);
        else if (getValue(YEAR) == -1) cal.add(cal.YEAR, 1);
        return checkCal(cal) && cal.getTime().getTime() > time;
    }

    public boolean equals(Object obj) {
        for (int i = 0; i < NUM_OF_VALUES; i++)
            if (values[i] != ((Stamp)obj).values[i]) return false;
        return true;
    }

    public String toString() {
        int value;
        StringBuffer sb = new StringBuffer();
        Calendar cal = Calendar.getInstance();
        for (int i = 0; i < NUM_OF_VALUES; i++)
            if ((value = getValue(i)) != -1) {
                if (sb.length() > 0) sb.append(' ');
                switch (i) {
                case DAY_OF_WEEK:
                    sb.append(shortWeekdays[value]);
                    break;
                default:
                    cal.set(calFields[i], value);
                    sb.append(fieldFormats[i][0].format(cal.getTime()));
                }
            }
        return sb.toString();
    }

}
