
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Boldfaces reserved words in program code. */
public class PrettyPrinter extends HttpServlet {
    static final long serialVersionUID = 0L;
    static String delimChars = Support.whites + "; {}",
        javaKeywords[] = {"abstract", "boolean", "break", "byte",
                          "case", "catch", "char", "class", "const", "continue",
                          "default", "do", "double", "else", "extends",
                          "final", "finally", "float", "for", "goto",
                          "if", "implements", "import", "instanceof", "int", "interface",
                          "long", "native", "new", "package", "private", "protected", "public",
                          "return", "static", "super", "synchronized",
                          "this", "throw", "throws", "transient", "try", "void", "volatile", "while"},
        cppKeywords[] = {"asm", "auto", "bad_cast", "bad_typeid", "break",
                         "case", "catch", "char", "class", "const", "const_cast", "continue",
                         "default", "delete", "do", "double", "dynamic_cast", "else", "enum", "except", "extern",
                         "finally", "float", "for", "friend", "goto", "if", "inline", "int", "long",
                         "namespace", "new", "operator", "private", "protected", "public",
                         "register", "reinterpret_cast", "return", "short", "signed", "sizeof", "static", "static_cast",
                         "struct", "switch", "template", "this", "throw", "try", "type_info", "typedef", "typeid",
                         "union", "unsigned", "using", "virtual", "void", "volatile", "while", "xalloc"};
    static Set<String> javaSet = getSet(javaKeywords), cppSet = getSet(cppKeywords);
    Map servletPathTable = null;

    static Set<String> getSet(String keywords[]) {
        Set<String> set = new HashSet<String>();
        for (int i = 0; i < keywords.length; i++) set.add(keywords[i]);
        return set;
    }

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        servletPathTable = (Map)config.getServletContext().getAttribute("FI.realitymodeler.server.W3Server/servletPathTable");
    }

    @SuppressWarnings("fallthrough")
        public void service(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException {
        Set<String> set = null;
        String path = req.getRequestURI();
        int i = path.lastIndexOf('.');
        if (i != -1) {
            String suffix = path.substring(i + 1).toLowerCase();
            if (suffix.equals("c") || suffix.equals("cpp") || suffix.equals("h")) set = cppSet;
            else if (suffix.equals("java")) set = javaSet;
        }
        res.setContentType(Support.htmlType);
        StreamTokenizer sTok = new StreamTokenizer(new BufferedReader(new InputStreamReader(req.getInputStream())));
        sTok.resetSyntax();
        sTok.wordChars('_', '_');
        sTok.wordChars('a', 'z');
        sTok.wordChars('A', 'Z');
        boolean quoted = false;
        int commentType = 0, lastType = 0, nSpaces = 0;
        PrintWriter writer = res.getWriter();
        for (; sTok.nextToken() != StreamTokenizer.TT_EOF; lastType = sTok.ttype)
            if (sTok.ttype == ' ') {
                if (++nSpaces == 2) {
                    writer.print("&nbsp;");
                    nSpaces = 0;
                }
            } else {
                if (nSpaces == 1) writer.print(' ');
                nSpaces = 0;
                switch (sTok.ttype) {
                case StreamTokenizer.TT_WORD:
                    if (!quoted && set.contains(sTok.sval)) {
                        writer.print("<b>");
                        writer.print(sTok.sval);
                        writer.print("</b>");
                    } else writer.print(sTok.sval);
                    break;
                case '<':
                    writer.print("&lt;");
                    break;
                case '>':
                    writer.print("&gt;");
                    break;
                case '&':
                    writer.print("&amp;");
                    break;
                case '"':
                    if (commentType == 0)
                        if (quoted) {
                            if (lastType != '\\') quoted = false;
                        } else if (lastType != '\'') quoted = true;
                    writer.print("&quot;");
                    break;
                case '/':
                    if (quoted && commentType == '*' && lastType == '*') {
                        commentType = 0;
                        quoted = false;
                        writer.write(sTok.ttype);
                        break;
                    }
                case '*':
                    if (!quoted && lastType == '/') {
                        commentType = sTok.ttype;
                        quoted = true;
                    }
                    writer.write(sTok.ttype);
                    break;
                case '\t':
                    writer.print("&nbsp;&nbsp;&nbsp;&nbsp;");
                    break;
                case '\n':
                case '\r':
                    switch (lastType) {
                    case '\n':
                        if (sTok.ttype == '\r') {
                            sTok.ttype = 0;
                            continue;
                        }
                        break;
                    case '\r':
                        if (sTok.ttype == '\n') {
                            sTok.ttype = 0;
                            continue;
                        }
                    }
                    if (commentType == '/') {
                        commentType = 0;
                        quoted = false;
                    }
                    writer.print("<br>\n");
                    break;
                case StreamTokenizer.TT_EOF:
                    return;
                default:
                    writer.write(sTok.ttype);
                }
            }
    }

}
