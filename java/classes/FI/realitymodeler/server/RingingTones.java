
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Ringing tones servlet. Initialization parameters are:<br>
    directory=directory containing ready-made ringing tones<br>
    msisdn=virtual phone number acting as origin of sent short messages (if this is not specified
    it is taken from system property sms.fromaddr) */
public class RingingTones extends HttpServlet implements FilenameFilter, Comparator<String> {
    static final long serialVersionUID = 0L;
    static Map<String,Integer> noteValues = new HashMap<String,Integer>();
    static {
        String noteNames[] = {"P", "C", "CIS", "D", "DIS", "E", "F", "FIS", "G", "GIS", "A", "AIS", "H"};
        for (int i = 0; i < noteNames.length; i++) noteValues.put(noteNames[i], new Integer(i));
        noteValues.put("DES", noteValues.get("CIS"));
        noteValues.put("ES", noteValues.get("DIS"));
        noteValues.put("GES", noteValues.get("FIS"));
        noteValues.put("AS", noteValues.get("GIS"));
        noteValues.put("B", noteValues.get("AIS"));
    }
    private ResourceBundle messages;
    private String directory, msisdn;
    private List<Tone> toneList;
    private Map<String,Tone> tones;

    class Tone implements Comparable<String> {
        String key;
        String name;
        String filename;

        Tone(String name, String filename) {
            this.name = name;
            this.filename = filename;
            this.key = name.toLowerCase();
        }

        public int compareTo(String o) {
            int l = o.length();
            return key.length() > l ? key.substring(0, l).compareTo(o) : key.compareTo(o);
        }

    }

    public boolean accept(File dir, String name) {
        return name.endsWith(".nfo");
    }

    public int compare(String o1, String o2) {
        return o1.compareTo(o2);
    }

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        messages = W3URLConnection.getMessages("FI.realitymodeler.servlet.resources.Messages");
        if ((directory = config.getInitParameter("directory")) == null) throw new ServletException("Parameter 'directory' not specified");
        if (!new File(directory).exists()) throw new ServletException("Specified directory " + directory + " doesn't exist");
        if (!directory.endsWith("/")) directory += "/";
        if ((msisdn = getInitParameter("msisdn")) == null &&
            (msisdn = System.getProperty("sms.fromaddr")) == null) throw new ServletException("Parameter 'msisdn' not specified");
        try {
            tones = new TreeMap<String,Tone>(this);
            String names[] = new File(directory).list(this);
            for (int i = 0; i < names.length; i++) {
                File file = new File(directory, names[i]);
                String filename = names[i].substring(0, names[i].lastIndexOf('.') + 1) + "ota";
                if (!new File(directory, filename).exists()) throw new ServletException("File " + filename + " doesn't exist");
                BufferedReader reader = new BufferedReader(new FileReader(file));
                try {
                    names[i] = reader.readLine();
                } finally {
                    reader.close();
                }
                tones.put(names[i], new Tone(names[i], filename));
            }
            toneList = new ArrayList<Tone>();
            Iterator<Tone> iter = tones.values().iterator();
            while (iter.hasNext()) toneList.add(iter.next());
        } catch (IOException ex) {
            throw new ServletException(Support.stackTrace(ex));
        }
    }

    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        String charset = res.getCharacterEncoding(),
            charsetName = charset != null ? Support.getCharacterEncoding(charset) : null;
        String s,
            name = (s = Support.getParameterValue(req.getParameterValues("name"))) != null ? s.trim() : "",
            number = (s = Support.getParameterValue(req.getParameterValues("number"))) != null ? s.trim() : "";
        boolean mobile = (s = req.getHeader("user-agent")) != null && s.trim().equalsIgnoreCase("mobile") ||
            req.getParameterValues("mobile") != null;
        if (mobile && number.equals("")) number = req.getRemoteAddr();
        if ((number.equals("") || name.equals("")) && req.getParameterValues("go") == null ||
            req.getParameterValues("new") != null) {
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            Support.writeBytes(bout, "<html><head><title>" + messages.getString("ringingTones") + "</title></head><body>\n", charsetName);
            if (!mobile) Support.writeBytes(bout, "<h1>" + messages.getString("ringingTones") + "</h1>", charsetName);
            Support.writeBytes(bout, "<form action=\"" + req.getRequestURI() + "\" method=get>\n", charsetName);
            Support.writeBytes(bout, "<input name=go type=hidden value=>\n", charsetName);
            Support.writeBytes(bout, messages.getString("selectRingingTone") + "<br>\n", charsetName);
            Support.writeBytes(bout, "<td><select name=name size=20>\n", charsetName);
            Iterator iter = tones.values().iterator();
            while (iter.hasNext())
                Support.writeBytes(bout, "<option>" + ((Tone)iter.next()).name + "\n", charsetName);
            Support.writeBytes(bout, "</select><br>\n", charsetName);
            if (!mobile) {
                Support.writeBytes(bout, "<input type=reset value=\"" + messages.getString("defaults") + "\">\n", charsetName);
                Support.writeBytes(bout, "<input name=new type=submit value=\"" + messages.getString("getNewForm") + "\">\n", charsetName);
            }
            Support.writeBytes(bout, "<input type=submit value=\"" + messages.getString("sendRingingTone") + "\">\n", charsetName);
            Support.writeBytes(bout, "<table>\n", charsetName);
            Support.writeBytes(bout, "<tr><th>" + messages.getString("phoneNumber") + "</th><td><input name=number type=text value=\"" + number + "\" size=55 maxlength=275></td></tr>\n", charsetName);
            Support.writeBytes(bout, "</table>\n", charsetName);
            Support.writeBytes(bout, "</form></body></html>", charsetName);
            res.setContentLength(bout.size());
            res.setContentType("text/html");
            bout.writeTo(res.getOutputStream());
            return;
        }
        int index = Collections.binarySearch(toneList, name.toLowerCase());
        if (index == -1) throw new ServletException("Tone not found");
        W3SmsURLConnection uc = new W3SmsURLConnection(new URL("sms://" + number + ":5505/binary/nbs/" + msisdn));
        try {
            uc.setServletContext(getServletConfig().getServletContext());
            OutputStream out = uc.getOutputStream();
            BufferedInputStream in = new BufferedInputStream(new FileInputStream(new File(directory, toneList.get(index).filename)));
            try {
                byte b[] = new byte[Support.bufferLength];
                for (int n; (n = in.read(b)) > 0;) out.write(b, 0, n);
                out.flush();
            } finally {
                in.close();
            }
            res.sendError(HttpURLConnection.HTTP_OK, messages.getString("ringingToneSent"));
        } finally {
            uc.disconnect();
        }
    }

    public void doHead(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        doGet(req, res);
    }

}
