
package FI.realitymodeler.server;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.security.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Digest authentication servlet */
public class DigestServlet extends HttpServlet {
    static final long serialVersionUID = 0L;

    private String privateKey;
    private long nonceExpiration;
    private HashMap<String,Object[]> nonceTable = new HashMap<String,Object[]>();
    private boolean stale = false;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        String s;
        if ((s = getInitParameter("PrivateKey")) != null) privateKey = s;
        else privateKey = "a29b6867efd867ce34d3a29b6867efd8";
        if ((s = getInitParameter("NonceExpiration")) != null) nonceExpiration = Long.parseLong(s);
        else nonceExpiration = 86400000;
    }

    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        MessageDigest md5;
        try {
            md5 = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException ex) {
            throw new ServletException(ex.toString());
        }
        W3Request w3request = (W3Request)req.getAttribute("");
        try {
            if (w3request.authorization == null) throw new ServletException("Authorization required");
            HashMap<String,String> args = new HashMap<String,String>();
            if (!Support.getArguments(w3request.authorization, args).startsWith("Digest")) throw new ServletException("Missing digest header");
            if (!args.containsKey("username") || !args.containsKey("response") ||
                !args.containsKey("nonce") || !args.containsKey("realm") ||
                !args.containsKey("uri")) throw new ServletException("Bad digest header");
            w3request.remoteUser = args.get("username");
            if (w3request.remoteUser.trim().startsWith("+")) throw new ServletException("Bad username");
            w3request.userPrincipal = new W3Principal(w3request.remoteUser);
            String response = args.get("response"),
                nonce = args.get("nonce"),
                uri = args.get("uri"),
                remoteAddr = req.getRemoteAddr();
            if (!uri.equals(req.getRequestURI())) throw new ServletException("Wrong URI");
            if (!nonceTable.containsKey(remoteAddr)) throw new ServletException("Invalid nonce");
            UserRecord user = w3request.w3server.userDatabase.getRecord(w3request.remoteUser);
            if (user == null) throw new ServletException("Unknown user");
            String digest = W3URLConnection.compute(md5, w3request.remoteUser, w3request.domain.name, user.password, nonce, req.getMethod(), uri);
            if (!digest.equals(response)) throw new ServletException("Bad password");
            Object nonceTableElement[] = nonceTable.get(remoteAddr);
            long i = System.currentTimeMillis(), timeStamp = ((Long)nonceTableElement[0]).longValue();
            if (i - timeStamp > nonceExpiration) {
                nonceTable.remove(remoteAddr);
                stale = true;
            }
            if (((String)nonceTableElement[1]).equals(nonce)) {
                if (user.domains != null && user.domains.contains(w3request.domain.name)) {
                    w3request.accountName = user.name;
                    return;
                }
                throw new ServletException("Digest authorization failed");
            }
            nonceTable.remove(remoteAddr);
            stale = true;
            throw new ServletException("Wrong client address");
        } catch (ServletException ex) {
            String remoteAddr = req.getRemoteAddr(), nonce;
            if (!nonceTable.containsKey(remoteAddr)) {
                long ctm = System.currentTimeMillis();
                nonce = W3URLConnection.hash(md5, remoteAddr + ":" + ctm + ":" + privateKey);
                Object nonceTableElement[] = new Object[2];
                nonceTableElement[0] = new Long(ctm);
                nonceTableElement[1] = nonce;
                nonceTable.put(remoteAddr, nonceTableElement);
            } else nonce = (String)nonceTable.get(remoteAddr)[1];
            w3request.challenge = "Digest realm=\"" + w3request.domain.name + "\",\n" +
                "\tdomain=\"" + req.getRequestURI() + "\",\n" +
                "\tnonce=\"" + nonce + "\",\n" + 
                "\tstale=" + (stale ? "true" : "false") + ",\n" +
                "\talgorithm=MD5";
            stale = false;
            throw ex;
        }
    }

}
