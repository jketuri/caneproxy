
package FI.realitymodeler;

import java.net.*;
import java.util.*;

public class HtmlForm {
    FieldMap fields = new FieldMap();
    Vector<String> names = new Vector<String>();
    String encType;
    String method;
    URL url;

    public HtmlForm(String method, String encType, URL url) {
        this.method = method != null ? method.toUpperCase() : "GET";
        this.encType = encType;
        this.url = url;
    }

    public String toString() {
        return "{" + method + " " + url + " " + fields + "}";
    }

    public Vector<String> getNames() {
        return names;
    }

    public FieldMap getFields() {
        return fields;
    }

    public String getMethod() {
        return method;
    }

    public URL getURL() {
        return url;
    }

    public void addField(String name, String value) {
        Object obj = fields.get(name);
        if (obj != null) {
            Vector<String> vector;
            if (obj instanceof String) {
                vector = new Vector<String>();
                vector.addElement((String)obj);
                fields.put(name, vector);
            } else vector = (Vector<String>)obj;
            vector.addElement(value);
        } else fields.put(name, value);
        names.addElement(name);
    }

    public void putField(String name, String value) {
        if (!fields.containsKey(name)) names.addElement(name);
        fields.put(name, value);
    }

    public Object getField(String name) {
        return fields.get(name);
    }

}
