
package FI.realitymodeler.p2p;

import java.rmi.*;
import javax.jms.*;

public class Peer2PeerServerSession implements ServerSession {
    RemotePeer2PeerServerSession rp2pss;

    public Peer2PeerServerSession(RemotePeer2PeerServerSession rp2pss) {
        this.rp2pss = rp2pss;
    }

    public Session getSession() throws JMSException {
        try {
            return new Peer2PeerSession(rp2pss.getSession());
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public void start() throws JMSException {
        try {
            rp2pss.start();
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

}
