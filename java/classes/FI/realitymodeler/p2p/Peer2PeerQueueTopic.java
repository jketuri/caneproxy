
package FI.realitymodeler.p2p;

import java.rmi.*;
import javax.jms.*;

public class Peer2PeerQueueTopic implements TemporaryQueue, TemporaryTopic {
    RemotePeer2PeerQueueTopic rp2pqt;
    String name;

    public Peer2PeerQueueTopic(RemotePeer2PeerQueueTopic rp2pqt) throws JMSException {
        this.rp2pqt = rp2pqt;
        try {
            name = rp2pqt.getQueueTopicName();
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public String getQueueName() throws JMSException {
        return name;
    }

    public String getTopicName() throws JMSException {
        return name;
    }

    public String toString() {
        return name;
    }

    public void delete() throws JMSException {
        try {
            rp2pqt.delete();
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

}
