
package FI.realitymodeler.p2p;

import java.rmi.*;
import java.rmi.server.*;
import javax.jms.*;

public class RemotePeer2PeerConnectionImpl extends UnicastRemoteObject implements RemotePeer2PeerConnection {
    static final long serialVersionUID = 0L;

    RemotePeer2Peer p2p;

    public RemotePeer2PeerConnectionImpl(RemotePeer2Peer p2p) throws RemoteException {
        this.p2p = p2p;
    }

    public RemotePeer2PeerSession createSession(boolean transacted, int acknowledgeMode) throws JMSException, RemoteException {
        return new RemotePeer2PeerSessionImpl(p2p, transacted, acknowledgeMode, false);
    }

    public String getClientID() throws JMSException {
        return null;
    }

    public void setClientID(String clientID) throws JMSException {
    }

    public RemotePeer2PeerConnectionMetaData getMetaData() throws JMSException {
        return null;
    }

    public ExceptionListener getExceptionListener() throws JMSException {
        return null;
    }

    public void setExceptionListener(ExceptionListener listener) throws JMSException {
    }

    public void start() throws JMSException {
    }

    public void stop() throws JMSException {
    }

    public void close() throws JMSException {
    }

    public RemotePeer2PeerSession createQueueSession(boolean transacted, int acknowledgeMode) throws RemoteException, JMSException {
        return new RemotePeer2PeerSessionImpl(p2p, transacted, acknowledgeMode, false);
    }

    public RemotePeer2PeerConnectionConsumer createConnectionConsumer(RemotePeer2PeerQueueTopic queue, String messageSelector, RemotePeer2PeerServerSessionPool sessionPool, int maxMessages) throws RemoteException, JMSException {
        return new RemotePeer2PeerConnectionConsumerImpl(queue, messageSelector, sessionPool, maxMessages, false, false);
    }

    public RemotePeer2PeerSession createTopicSession(boolean transacted, int acknowledgeMode) throws RemoteException, JMSException {
        return new RemotePeer2PeerSessionImpl(p2p, transacted, acknowledgeMode, true);
    }

    public RemotePeer2PeerConnectionConsumer createDurableConnectionConsumer(RemotePeer2PeerQueueTopic topic, String subscriptionName, String messageSelector, RemotePeer2PeerServerSessionPool sessionPool, int maxMessages) throws RemoteException, JMSException {
        return new RemotePeer2PeerConnectionConsumerImpl(topic, messageSelector, sessionPool, maxMessages, true, true);
    }

}
