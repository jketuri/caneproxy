
package FI.realitymodeler.p2p;

import java.rmi.*;
import javax.jms.*;

public interface RemotePeer2PeerConnection extends Remote {

    RemotePeer2PeerSession createSession(boolean transacted, int acknowledgeMode) throws RemoteException, JMSException;

    String getClientID() throws RemoteException, JMSException;

    void setClientID(String clientID) throws RemoteException, JMSException;

    RemotePeer2PeerConnectionMetaData getMetaData() throws RemoteException, JMSException;

    ExceptionListener getExceptionListener() throws RemoteException, JMSException;

    void setExceptionListener(ExceptionListener listener) throws RemoteException, JMSException;

    void start() throws RemoteException, JMSException;

    void stop() throws RemoteException, JMSException;

    void close() throws RemoteException, JMSException;

    RemotePeer2PeerSession createQueueSession(boolean transacted, int acknowledgeMode) throws RemoteException, JMSException;

    RemotePeer2PeerConnectionConsumer createConnectionConsumer(RemotePeer2PeerQueueTopic queue, String messageSelector, RemotePeer2PeerServerSessionPool sessionPool, int maxMessages) throws RemoteException, JMSException;

    RemotePeer2PeerSession createTopicSession(boolean transacted, int acknowledgeMode) throws RemoteException, JMSException;

    RemotePeer2PeerConnectionConsumer createDurableConnectionConsumer(RemotePeer2PeerQueueTopic topic, String subscriptionName, String messageSelector, RemotePeer2PeerServerSessionPool sessionPool, int maxMessages) throws RemoteException, JMSException;

}
