
package FI.realitymodeler.p2p;

import java.io.*;
import java.rmi.*;
import javax.jms.*;

public interface RemotePeer2PeerSession extends Remote {

    RemotePeer2PeerSenderPublisher createProducer(RemotePeer2PeerQueueTopic destination) throws RemoteException, JMSException;

    RemotePeer2PeerReceiverSubscriber createConsumer(RemotePeer2PeerQueueTopic destination) throws RemoteException, JMSException;

    RemotePeer2PeerReceiverSubscriber createConsumer(RemotePeer2PeerQueueTopic destination, String messageSelector) throws RemoteException, JMSException;

    RemotePeer2PeerReceiverSubscriber createConsumer(RemotePeer2PeerQueueTopic destination, String messageSelector, boolean noLocal) throws RemoteException, JMSException;

    RemotePeer2PeerQueueTopic createQueue(String queueName) throws RemoteException, JMSException;

    RemotePeer2PeerReceiverSubscriber createReceiver(RemotePeer2PeerQueueTopic queue) throws RemoteException, JMSException;

    RemotePeer2PeerReceiverSubscriber createReceiver(RemotePeer2PeerQueueTopic queue, String messageSelector) throws RemoteException, JMSException;

    RemotePeer2PeerSenderPublisher createSender(RemotePeer2PeerQueueTopic queue) throws RemoteException, JMSException;

    RemotePeer2PeerReceiverSubscriber createBrowser(RemotePeer2PeerQueueTopic queue) throws RemoteException, JMSException;

    RemotePeer2PeerReceiverSubscriber createBrowser(RemotePeer2PeerQueueTopic queue, String messageSelector) throws RemoteException, JMSException;

    RemotePeer2PeerQueueTopic createTemporaryQueue() throws RemoteException, JMSException;

    RemotePeer2PeerQueueTopic createTopic(String topicName) throws RemoteException, JMSException;

    RemotePeer2PeerReceiverSubscriber createSubscriber(RemotePeer2PeerQueueTopic topic) throws RemoteException, JMSException;

    RemotePeer2PeerReceiverSubscriber createSubscriber(RemotePeer2PeerQueueTopic topic, String messageSelector, boolean noLocal) throws RemoteException, JMSException;

    RemotePeer2PeerReceiverSubscriber createDurableSubscriber(RemotePeer2PeerQueueTopic topic, String name) throws RemoteException, JMSException;

    RemotePeer2PeerReceiverSubscriber createDurableSubscriber(RemotePeer2PeerQueueTopic topic, String name, String messageSelector, boolean noLocal) throws RemoteException, JMSException;

    RemotePeer2PeerSenderPublisher createPublisher(RemotePeer2PeerQueueTopic topic) throws RemoteException, JMSException;

    RemotePeer2PeerSenderPublisher createDurablePublisher(RemotePeer2PeerQueueTopic topic, String name) throws RemoteException, JMSException;

    RemotePeer2PeerQueueTopic createTemporaryTopic() throws RemoteException, JMSException;

    void unpublish(String name) throws RemoteException, JMSException;

    void unsubscribe(String name) throws RemoteException, JMSException;

}
