
package FI.realitymodeler.p2p;

import java.rmi.*;
import java.util.*;
import javax.jms.*;

public interface RemotePeer2PeerConnectionMetaData extends Remote {

    String getJMSVersion() throws RemoteException, JMSException;

    int getJMSMajorVersion() throws RemoteException, JMSException;

    int getJMSMinorVersion() throws RemoteException, JMSException;

    String getJMSProviderName() throws RemoteException, JMSException;

    String getProviderVersion() throws RemoteException, JMSException;

    int getProviderMajorVersion() throws RemoteException, JMSException;

    int getProviderMinorVersion() throws RemoteException, JMSException;

    Enumeration getJMSXPropertyNames() throws RemoteException, JMSException;

}
