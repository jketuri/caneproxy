
package FI.realitymodeler.p2p;

import java.rmi.*;
import java.rmi.server.*;
import javax.jms.*;

public class RemotePeer2PeerConnectionConsumerImpl extends UnicastRemoteObject implements RemotePeer2PeerConnectionConsumer {
    static final long serialVersionUID = 0L;

    RemotePeer2PeerQueueTopic queueTopic;
    String messageSelector;
    RemotePeer2PeerServerSessionPool serverSessionPool;
    int maxMessages;
    boolean topic;
    boolean durable;

    public RemotePeer2PeerConnectionConsumerImpl(RemotePeer2PeerQueueTopic queueTopic, String messageSelector, RemotePeer2PeerServerSessionPool serverSessionPool, int maxMessages, boolean topic, boolean durable) throws RemoteException {
        this.queueTopic = queueTopic;
        this.messageSelector = messageSelector;
        this.serverSessionPool = serverSessionPool;
        this.maxMessages = maxMessages;
        this.topic = topic;
        this.durable = durable;
    }

    public RemotePeer2PeerServerSessionPool getServerSessionPool() throws RemoteException, JMSException {
        return serverSessionPool;
    }

    public void close() throws RemoteException, JMSException {
    }

}
