
package FI.realitymodeler.p2p;

import java.rmi.*;
import java.rmi.server.*;
import javax.jms.*;

public class RemotePeer2PeerReceiverSubscriberImpl extends UnicastRemoteObject implements RemotePeer2PeerReceiverSubscriber {
    static final long serialVersionUID = 0L;

    RemotePeer2PeerQueueTopic queueTopic;
    String messageSelector;
    long number;
    int count;

    public RemotePeer2PeerReceiverSubscriberImpl(RemotePeer2PeerQueueTopic queueTopic, String messageSelector) throws RemoteException {
        this.queueTopic = queueTopic;
        this.messageSelector = messageSelector;
    }

    public RemotePeer2PeerReceiverSubscriberImpl(RemotePeer2PeerQueueTopic queueTopic, String name, String messageSelector, boolean noLocal) throws RemoteException {
        this.queueTopic = queueTopic;
        this.messageSelector = messageSelector;
    }

    public String getMessageSelector() throws RemoteException, JMSException {
        return messageSelector;
    }

    public Message receive() throws RemoteException, JMSException, InterruptedException {
        return queueTopic.get(true, -1L, this);
    }

    public Message receive(long timeOut) throws RemoteException, JMSException, InterruptedException {
        return queueTopic.get(true, timeOut, this);
    }

    public Message receiveNoWait() throws RemoteException, JMSException, InterruptedException {
        return queueTopic.get(true, 0L, this);
    }

    public void close() throws RemoteException, JMSException {
    }

    public RemotePeer2PeerQueueTopic getQueueTopic() throws RemoteException, JMSException {
        return queueTopic;
    }

    public boolean getNoLocal() throws RemoteException, JMSException {
        return false;
    }

    public void setNoLocal(boolean noLocal) throws RemoteException, JMSException {
    }

    public long getNumber() throws RemoteException {
        return number;
    }

    public long incrementNumber() throws RemoteException {
        return number++;
    }

}

