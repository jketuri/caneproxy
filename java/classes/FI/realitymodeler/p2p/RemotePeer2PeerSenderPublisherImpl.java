
package FI.realitymodeler.p2p;

import java.rmi.*;
import java.rmi.server.*;
import javax.jms.*;

public class RemotePeer2PeerSenderPublisherImpl extends UnicastRemoteObject implements RemotePeer2PeerSenderPublisher {
    static final long serialVersionUID = 0L;

    RemotePeer2PeerQueueTopic queueTopic;

    public RemotePeer2PeerSenderPublisherImpl(RemotePeer2PeerQueueTopic queueTopic) throws RemoteException {
        this.queueTopic = queueTopic;
    }

    public void setDisableMessageID(boolean value) throws JMSException {
    }

    public boolean getDisableMessageID() throws JMSException {
        return false;
    }

    public void setDisableMessageTimestamp(boolean value) throws JMSException {
    }

    public boolean getDisableMessageTimestamp() throws JMSException {
        return false;
    }

    public void setDeliveryMode(int deliveryMode) throws JMSException {
    }

    public int getDeliveryMode() throws JMSException {
        return 0;
    }

    public void setPriority(int priority) throws JMSException {
    }

    public int getPriority() throws JMSException {
        return 0;
    }

    public void setTimeToLive(long timeToLive) throws JMSException {
    }

    public long getTimeToLive() throws JMSException {
        return 0;
    }

    public RemotePeer2PeerQueueTopic getDestination() throws JMSException {
        return queueTopic;
    }

    public void close() throws JMSException {
    }

    public RemotePeer2PeerQueueTopic getQueue() throws RemoteException, JMSException {
        return queueTopic;
    }

    public void send(Message message) throws RemoteException, JMSException {
        queueTopic.add(message);
    }

    public void send(Message message, int deliveryMode, int priority, long timeToLive) throws RemoteException, JMSException {
        send(message);
    }

    public void send(RemotePeer2PeerQueueTopic queue, Message message) throws RemoteException, JMSException {
        queue.add(message);
    }

    public void send(RemotePeer2PeerQueueTopic queue, Message message, int deliveryMode, int priority, long timeToLive) throws RemoteException, JMSException {
        send(queue, message);
    }

    public RemotePeer2PeerQueueTopic getTopic() throws RemoteException, JMSException {
        return queueTopic;
    }

    public void publish(Message message) throws RemoteException, JMSException {
        send(message);
    }

    public void publish(Message message, int deliveryMode, int priority, long timeToLive) throws RemoteException, JMSException {
        publish(message);
    }

    public void publish(RemotePeer2PeerQueueTopic topic, Message message) throws RemoteException, JMSException {
        send(topic, message);
    }

    public void publish(RemotePeer2PeerQueueTopic topic, Message message, int deliveryMode, int priority, long timeToLive) throws RemoteException, JMSException {
        publish(topic, message);
    }

}
