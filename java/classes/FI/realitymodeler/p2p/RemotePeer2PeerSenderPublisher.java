
package FI.realitymodeler.p2p;

import java.rmi.*;
import javax.jms.*;

public interface RemotePeer2PeerSenderPublisher extends Remote {

    void setDisableMessageID(boolean value) throws RemoteException, JMSException;

    boolean getDisableMessageID() throws RemoteException, JMSException;

    void setDisableMessageTimestamp(boolean value) throws RemoteException, JMSException;

    boolean getDisableMessageTimestamp() throws RemoteException, JMSException;

    void setDeliveryMode(int deliveryMode) throws RemoteException, JMSException;

    int getDeliveryMode() throws RemoteException, JMSException;

    void setPriority(int priority) throws RemoteException, JMSException;

    int getPriority() throws RemoteException, JMSException;

    void setTimeToLive(long timeToLive) throws RemoteException, JMSException;

    long getTimeToLive() throws RemoteException, JMSException;

    RemotePeer2PeerQueueTopic getDestination() throws RemoteException, JMSException;

    void close() throws RemoteException, JMSException;

    RemotePeer2PeerQueueTopic getQueue() throws RemoteException, JMSException;

    void send(Message message) throws RemoteException, JMSException;

    void send(Message message, int deliveryMode, int priority, long timeToLive) throws RemoteException, JMSException;

    void send(RemotePeer2PeerQueueTopic queue, Message message) throws RemoteException, JMSException;

    void send(RemotePeer2PeerQueueTopic queue, Message message, int deliveryMode, int priority, long timeToLive) throws RemoteException, JMSException;

    RemotePeer2PeerQueueTopic getTopic() throws RemoteException, JMSException;

    void publish(Message message) throws RemoteException, JMSException;

    void publish(Message message, int deliveryMode, int priority, long timeToLive) throws RemoteException, JMSException;

    void publish(RemotePeer2PeerQueueTopic topic, Message message) throws RemoteException, JMSException;

    void publish(RemotePeer2PeerQueueTopic topic, Message message, int deliveryMode, int priority, long timeToLive) throws RemoteException, JMSException;

}
