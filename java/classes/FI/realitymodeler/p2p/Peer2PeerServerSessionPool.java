
package FI.realitymodeler.p2p;

import java.rmi.*;
import javax.jms.*;

public class Peer2PeerServerSessionPool implements ServerSessionPool {
    RemotePeer2PeerServerSessionPool rp2pssp;

    public Peer2PeerServerSessionPool(RemotePeer2PeerServerSessionPool rp2pssp) {
        this.rp2pssp = rp2pssp;
    }

    public ServerSession getServerSession() throws JMSException {
        try {
            return new Peer2PeerServerSession(rp2pssp.getServerSession());
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

}
