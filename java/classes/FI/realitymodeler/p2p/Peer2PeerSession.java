
package FI.realitymodeler.p2p;

import java.io.*;
import java.rmi.*;
import javax.jms.*;

public class Peer2PeerSession implements QueueSession, TopicSession {
    RemotePeer2PeerSession rp2ps;

    public Peer2PeerSession(RemotePeer2PeerSession rp2ps) {
        this.rp2ps = rp2ps;
    }

    public void run() {
    }

    public BytesMessage createBytesMessage() throws JMSException {
        return null;
    }

    public MapMessage createMapMessage() throws JMSException {
        return null;
    }

    public Message createMessage() throws JMSException {
        return new Peer2PeerMessage();
    }

    public ObjectMessage createObjectMessage() throws JMSException {
        return new Peer2PeerMessage();
    }

    public ObjectMessage createObjectMessage(Serializable object) throws JMSException {
        return new Peer2PeerMessage(object);
    }

    public StreamMessage createStreamMessage() throws JMSException {
        return null;
    }

    public TextMessage createTextMessage() throws JMSException {
        return null;
    }

    public TextMessage createTextMessage(String text) throws JMSException {
        return null;
    }

    public boolean getTransacted() throws JMSException {
        return false;
    }

    public int getAcknowledgeMode() throws JMSException {
        return 0;
    }

    public void commit() throws JMSException {
    }

    public void rollback() throws JMSException {
    }

    public void close() throws JMSException {
    }

    public void recover() throws JMSException {
    }

    public MessageListener getMessageListener() throws JMSException {
        return null;
    }

    public void setMessageListener(MessageListener listener) throws JMSException {
    }

    public MessageProducer createProducer(Destination destination) throws JMSException {
        try {
            return new Peer2PeerSenderPublisher(rp2ps.createProducer(((Peer2PeerQueueTopic)destination).rp2pqt));
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public MessageConsumer createConsumer(Destination destination) throws JMSException {
        try {
            return new Peer2PeerReceiverSubscriber(rp2ps.createConsumer(((Peer2PeerQueueTopic)destination).rp2pqt));
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public MessageConsumer createConsumer(Destination destination, String messageSelector) throws JMSException {
        try {
            return new Peer2PeerReceiverSubscriber(rp2ps.createConsumer(((Peer2PeerQueueTopic)destination).rp2pqt, messageSelector));
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public MessageConsumer createConsumer(Destination destination, String messageSelector, boolean noLocal) throws JMSException {
        try {
            return new Peer2PeerReceiverSubscriber(rp2ps.createConsumer(((Peer2PeerQueueTopic)destination).rp2pqt, messageSelector, noLocal));
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public Queue createQueue(String queueName) throws JMSException {
        try {
            return new Peer2PeerQueueTopic(rp2ps.createQueue(queueName));
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public QueueReceiver createReceiver(Queue queue) throws JMSException {
        try {
            return new Peer2PeerReceiverSubscriber(rp2ps.createReceiver(((Peer2PeerQueueTopic)queue).rp2pqt));
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public QueueReceiver createReceiver(Queue queue, String messageSelector) throws JMSException {
        try {
            return new Peer2PeerReceiverSubscriber(rp2ps.createReceiver(((Peer2PeerQueueTopic)queue).rp2pqt, messageSelector));
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public QueueSender createSender(Queue queue) throws JMSException {
        try {
            return new Peer2PeerSenderPublisher(rp2ps.createSender(((Peer2PeerQueueTopic)queue).rp2pqt));
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public QueueBrowser createBrowser(Queue queue) throws JMSException {
        try {
            return new Peer2PeerReceiverSubscriber(rp2ps.createBrowser(((Peer2PeerQueueTopic)queue).rp2pqt));
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public QueueBrowser createBrowser(Queue queue, String messageSelector) throws JMSException {
        try {
            return new Peer2PeerReceiverSubscriber(rp2ps.createBrowser(((Peer2PeerQueueTopic)queue).rp2pqt, messageSelector));
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public TemporaryQueue createTemporaryQueue() throws JMSException {
        try {
            return new Peer2PeerQueueTopic(rp2ps.createTemporaryQueue());
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public Topic createTopic(String topicName) throws JMSException {
        try {
            return new Peer2PeerQueueTopic(rp2ps.createTopic(topicName));
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public TopicSubscriber createSubscriber(Topic topic) throws JMSException {
        try {
            return new Peer2PeerReceiverSubscriber(rp2ps.createSubscriber(((Peer2PeerQueueTopic)topic).rp2pqt));
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public TopicSubscriber createSubscriber(Topic topic, String messageSelector, boolean noLocal) throws JMSException {
        try {
            return new Peer2PeerReceiverSubscriber(rp2ps.createSubscriber(((Peer2PeerQueueTopic)topic).rp2pqt, messageSelector, noLocal));
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public TopicSubscriber createDurableSubscriber(Topic topic, String name) throws JMSException {
        try {
            return new Peer2PeerReceiverSubscriber(rp2ps.createDurableSubscriber(((Peer2PeerQueueTopic)topic).rp2pqt, name));
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public TopicSubscriber createDurableSubscriber(Topic topic, String name, String messageSelector, boolean noLocal) throws JMSException {
        try {
            return new Peer2PeerReceiverSubscriber(rp2ps.createDurableSubscriber(((Peer2PeerQueueTopic)topic).rp2pqt, name, messageSelector, noLocal));
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public TopicPublisher createPublisher(Topic topic) throws JMSException {
        try {
            return new Peer2PeerSenderPublisher(rp2ps.createPublisher(((Peer2PeerQueueTopic)topic).rp2pqt));
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public TopicPublisher createDurablePublisher(Topic topic, String name) throws JMSException {
        try {
            return new Peer2PeerSenderPublisher(rp2ps.createDurablePublisher(((Peer2PeerQueueTopic)topic).rp2pqt, name));
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public TemporaryTopic createTemporaryTopic() throws JMSException {
        try {
            return new Peer2PeerQueueTopic(rp2ps.createTemporaryTopic());
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public void unpublish(String name) throws JMSException {
        try {
            rp2ps.unpublish(name);
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public void unsubscribe(String name) throws JMSException {
        try {
            rp2ps.unsubscribe(name);
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

}
