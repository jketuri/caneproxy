
package FI.realitymodeler.p2p;

import java.rmi.*;
import java.util.*;
import javax.jms.*;

public class Peer2PeerReceiverSubscriber implements QueueReceiver, QueueBrowser, TopicSubscriber
{
    RemotePeer2PeerReceiverSubscriber rp2prs;

    public Peer2PeerReceiverSubscriber(RemotePeer2PeerReceiverSubscriber rp2prs)
    {
        this.rp2prs = rp2prs;
    }

    public String getMessageSelector() throws JMSException
    {
        try
            {
                return rp2prs.getMessageSelector();
            } catch (RemoteException ex)
            {
                throw new JMSException(ex.toString());
            }
    }

    public MessageListener getMessageListener() throws JMSException
    {
        return null;
    }

    public void setMessageListener(MessageListener listener) throws JMSException
    {
    }

    public Message receive() throws JMSException
    {
        try
            {
                return rp2prs.receive();
            } catch (InterruptedException ex)
            {
                throw new JMSException(ex.toString());
            } catch (RemoteException ex)
            {
                throw new JMSException(ex.toString());
            }
    }

    public Message receive(long timeOut) throws JMSException
    {
        try
            {
                return rp2prs.receive(timeOut);
            } catch (InterruptedException ex)
            {
                throw new JMSException(ex.toString());
            } catch (RemoteException ex)
            {
                throw new JMSException(ex.toString());
            }
    }

    public Message receiveNoWait() throws JMSException
    {
        try
            {
                return rp2prs.receiveNoWait();
            } catch (InterruptedException ex)
            {
                throw new JMSException(ex.toString());
            } catch (RemoteException ex)
            {
                throw new JMSException(ex.toString());
            }
    }

    public void close() throws JMSException
    {
        try
            {
                rp2prs.close();
            } catch (RemoteException ex)
            {
                throw new JMSException(ex.toString());
            }
    }

    public javax.jms.Queue getQueue() throws JMSException
    {
        try
            {
                return new Peer2PeerQueueTopic(rp2prs.getQueueTopic());
            } catch (RemoteException ex)
            {
                throw new JMSException(ex.toString());
            }
    }

    public Enumeration getEnumeration() throws JMSException
    {
        return null;
    }

    public Topic getTopic() throws JMSException
    {
        return (Topic)getQueue();
    }

    public boolean getNoLocal() throws JMSException
    {
        try
            {
                return rp2prs.getNoLocal();
            } catch (RemoteException ex)
            {
                throw new JMSException(ex.toString());
            }
    }

    public void setNoLocal(boolean noLocal) throws JMSException
    {
        try
            {
                rp2prs.setNoLocal(noLocal);
            } catch (RemoteException ex)
            {
                throw new JMSException(ex.toString());
            }
    }

}
