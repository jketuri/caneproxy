
package FI.realitymodeler.p2p;

import java.rmi.*;
import java.rmi.server.*;
import javax.jms.*;

public class RemotePeer2PeerServerSessionImpl extends UnicastRemoteObject implements RemotePeer2PeerServerSession {
    static final long serialVersionUID = 0L;

    public RemotePeer2PeerServerSessionImpl() throws RemoteException {
    }

    public RemotePeer2PeerSession getSession() throws RemoteException, JMSException {
        return null;
    }

    public void start() throws RemoteException, JMSException {
    }

}
