
package FI.realitymodeler.p2p;

import java.rmi.*;
import javax.jms.*;

public class Peer2PeerConnection implements QueueConnection, TopicConnection {
    RemotePeer2PeerConnection rp2pc;

    public Peer2PeerConnection(RemotePeer2PeerConnection rp2pc) {
        this.rp2pc = rp2pc;
    }

    public Session createSession(boolean transacted, int acknowledgeMode) throws JMSException {
        try {
            return new Peer2PeerSession(rp2pc.createSession(transacted, acknowledgeMode));
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public String getClientID() throws JMSException {
        try {
            return rp2pc.getClientID();
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public void setClientID(String clientID) throws JMSException {
        try {
            rp2pc.setClientID(clientID);
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public ConnectionMetaData getMetaData() throws JMSException {
        try {
            return new Peer2PeerConnectionMetaData(rp2pc.getMetaData());
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public ExceptionListener getExceptionListener() throws JMSException {
        try {
            return rp2pc.getExceptionListener();
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public void setExceptionListener(ExceptionListener listener) throws JMSException {
        try {
            rp2pc.setExceptionListener(listener);
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public void start() throws JMSException {
    }

    public void stop() throws JMSException {
    }

    public void close() throws JMSException {
        try {
            rp2pc.close();
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public ConnectionConsumer createConnectionConsumer(Destination destination, String messageSelector, ServerSessionPool sessionPool, int maxMessages) throws JMSException {
        try {
            return new Peer2PeerConnectionConsumer(rp2pc.createConnectionConsumer(((Peer2PeerQueueTopic)destination).rp2pqt, messageSelector, ((Peer2PeerServerSessionPool)sessionPool).rp2pssp, maxMessages));
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public QueueSession createQueueSession(boolean transacted, int acknowledgeMode) throws JMSException {
        try {
            return new Peer2PeerSession(rp2pc.createQueueSession(transacted, acknowledgeMode));
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public ConnectionConsumer createConnectionConsumer(Queue queue, String messageSelector, ServerSessionPool sessionPool, int maxMessages) throws JMSException {
        try {
            return new Peer2PeerConnectionConsumer(rp2pc.createConnectionConsumer(((Peer2PeerQueueTopic)queue).rp2pqt, messageSelector, ((Peer2PeerServerSessionPool)sessionPool).rp2pssp, maxMessages));
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public TopicSession createTopicSession(boolean transacted, int acknowledgeMode) throws JMSException {
        try {
            return new Peer2PeerSession(rp2pc.createTopicSession(transacted, acknowledgeMode));
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public ConnectionConsumer createConnectionConsumer(Topic topic, String messageSelector, ServerSessionPool sessionPool, int maxMessages) throws JMSException {
        try {
            return new Peer2PeerConnectionConsumer(rp2pc.createConnectionConsumer(((Peer2PeerQueueTopic)topic).rp2pqt, messageSelector, ((Peer2PeerServerSessionPool)sessionPool).rp2pssp, maxMessages));
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public ConnectionConsumer createDurableConnectionConsumer(Topic topic, String subscriptionName, String messageSelector, ServerSessionPool sessionPool, int maxMessages) throws JMSException {
        try {
            return new Peer2PeerConnectionConsumer(rp2pc.createDurableConnectionConsumer(((Peer2PeerQueueTopic)topic).rp2pqt, subscriptionName, messageSelector, ((Peer2PeerServerSessionPool)sessionPool).rp2pssp, maxMessages));
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

}
