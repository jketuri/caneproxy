
package FI.realitymodeler.p2p;

import java.rmi.*;
import javax.jms.*;

public interface RemotePeer2PeerReceiverSubscriber extends Remote {

    String getMessageSelector() throws RemoteException, JMSException;

    Message receive() throws RemoteException, JMSException, InterruptedException;

    Message receive(long timeOut) throws RemoteException, JMSException, InterruptedException;

    Message receiveNoWait() throws RemoteException, JMSException, InterruptedException;

    void close() throws RemoteException, JMSException;

    RemotePeer2PeerQueueTopic getQueueTopic() throws RemoteException, JMSException;

    boolean getNoLocal() throws RemoteException, JMSException;

    void setNoLocal(boolean noLocal) throws RemoteException, JMSException;

    long getNumber() throws RemoteException;

    long incrementNumber() throws RemoteException;

}
