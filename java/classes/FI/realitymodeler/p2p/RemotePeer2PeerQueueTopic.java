
package FI.realitymodeler.p2p;

import java.rmi.*;
import javax.jms.*;

public interface RemotePeer2PeerQueueTopic extends Remote {

    String getQueueTopicName() throws RemoteException, JMSException;

    void delete() throws RemoteException, JMSException;

    void add(Message message) throws RemoteException, JMSException;

    Peer2PeerMessage get(boolean remove, long timeOut, RemotePeer2PeerReceiverSubscriber receiverSubscriber) throws RemoteException, JMSException, InterruptedException;

}

