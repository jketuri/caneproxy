
package FI.realitymodeler.p2p;

import java.rmi.*;
import java.rmi.server.*;
import javax.jms.*;

public class RemotePeer2PeerSessionImpl extends UnicastRemoteObject implements RemotePeer2PeerSession {
    static final long serialVersionUID = 0L;

    RemotePeer2Peer p2p;
    boolean transacted;
    int acknowledgeMode;
    boolean topic;

    public RemotePeer2PeerSessionImpl(RemotePeer2Peer p2p, boolean transacted, int acknowledgeMode, boolean topic) throws RemoteException {
        this.p2p = p2p;
        this.transacted = transacted;
        this.acknowledgeMode = acknowledgeMode;
        this.topic = topic;
    }

    public RemotePeer2PeerSenderPublisher createProducer(RemotePeer2PeerQueueTopic destination) throws RemoteException, JMSException {
        return new RemotePeer2PeerSenderPublisherImpl(destination);
    }

    public RemotePeer2PeerReceiverSubscriber createConsumer(RemotePeer2PeerQueueTopic destination) throws RemoteException, JMSException {
        return new RemotePeer2PeerReceiverSubscriberImpl(destination, null);
    }

    public RemotePeer2PeerReceiverSubscriber createConsumer(RemotePeer2PeerQueueTopic destination, String messageSelector) throws RemoteException, JMSException {
        return new RemotePeer2PeerReceiverSubscriberImpl(destination, messageSelector);
    }

    public RemotePeer2PeerReceiverSubscriber createConsumer(RemotePeer2PeerQueueTopic destination, String messageSelector, boolean noLocal) throws RemoteException, JMSException {
        return new RemotePeer2PeerReceiverSubscriberImpl(destination, messageSelector);
    }

    public RemotePeer2PeerQueueTopic createQueue(String queueName) throws RemoteException, JMSException {
        return p2p.createQueue(queueName);
    }

    public RemotePeer2PeerReceiverSubscriber createReceiver(RemotePeer2PeerQueueTopic queue) throws RemoteException, JMSException {
        return new RemotePeer2PeerReceiverSubscriberImpl(queue, null);
    }

    public RemotePeer2PeerReceiverSubscriber createReceiver(RemotePeer2PeerQueueTopic queue, String messageSelector) throws RemoteException, JMSException {
        return new RemotePeer2PeerReceiverSubscriberImpl(queue, messageSelector);
    }

    public RemotePeer2PeerSenderPublisher createSender(RemotePeer2PeerQueueTopic queue) throws RemoteException, JMSException {
        return new RemotePeer2PeerSenderPublisherImpl(queue);
    }

    public RemotePeer2PeerReceiverSubscriber createBrowser(RemotePeer2PeerQueueTopic queue) throws RemoteException, JMSException {
        return new RemotePeer2PeerReceiverSubscriberImpl(queue, null);
    }

    public RemotePeer2PeerReceiverSubscriber createBrowser(RemotePeer2PeerQueueTopic queue, String messageSelector) throws RemoteException, JMSException {
        return new RemotePeer2PeerReceiverSubscriberImpl(queue, messageSelector);
    }

    public RemotePeer2PeerQueueTopic createTemporaryQueue() throws RemoteException, JMSException {
        return new RemotePeer2PeerQueueTopicImpl(null);
    }

    public RemotePeer2PeerQueueTopic createTopic(String topicName) throws RemoteException, JMSException {
        return p2p.createTopic(topicName);
    }

    public RemotePeer2PeerReceiverSubscriber createSubscriber(RemotePeer2PeerQueueTopic topic) throws RemoteException, JMSException {
        return new RemotePeer2PeerReceiverSubscriberImpl(topic, null);
    }

    public RemotePeer2PeerReceiverSubscriber createSubscriber(RemotePeer2PeerQueueTopic topic, String messageSelector, boolean noLocal) throws RemoteException, JMSException {
        return new RemotePeer2PeerReceiverSubscriberImpl(topic, null, messageSelector, noLocal);
    }

    public RemotePeer2PeerReceiverSubscriber createDurableSubscriber(RemotePeer2PeerQueueTopic topic, String name) throws RemoteException, JMSException {
        return new RemotePeer2PeerReceiverSubscriberImpl(topic, name, null, false);
    }

    public RemotePeer2PeerReceiverSubscriber createDurableSubscriber(RemotePeer2PeerQueueTopic topic, String name, String messageSelector, boolean noLocal) throws RemoteException, JMSException {
        return new RemotePeer2PeerReceiverSubscriberImpl(topic, name, messageSelector, noLocal);
    }

    public RemotePeer2PeerSenderPublisher createPublisher(RemotePeer2PeerQueueTopic topic) throws RemoteException, JMSException {
        return new RemotePeer2PeerSenderPublisherImpl(topic);
    }

    public RemotePeer2PeerSenderPublisher createDurablePublisher(RemotePeer2PeerQueueTopic topic, String name) throws RemoteException, JMSException {
        return null;
    }

    public RemotePeer2PeerQueueTopic createTemporaryTopic() throws RemoteException, JMSException {
        return new RemotePeer2PeerQueueTopicImpl(null);
    }

    public void unpublish(String name) throws RemoteException, JMSException {
    }

    public void unsubscribe(String name) throws RemoteException, JMSException {
    }

}
