
package FI.realitymodeler.p2p;

import java.rmi.*;
import javax.jms.*;

public interface RemotePeer2PeerServerSessionPool extends Remote {

    RemotePeer2PeerServerSession getServerSession() throws RemoteException, JMSException;

}
