
package FI.realitymodeler.p2p;

import java.rmi.*;
import javax.jms.*;

public class Peer2PeerSenderPublisher implements QueueSender, TopicPublisher {
    RemotePeer2PeerSenderPublisher rp2psp;

    public Peer2PeerSenderPublisher(RemotePeer2PeerSenderPublisher rp2psp) {
        this.rp2psp = rp2psp;
    }

    public void setDisableMessageID(boolean value) throws JMSException {
        try {
            rp2psp.setDisableMessageID(value);
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public boolean getDisableMessageID() throws JMSException {
        try {
            return rp2psp.getDisableMessageID();
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public void setDisableMessageTimestamp(boolean value) throws JMSException {
        try {
            rp2psp.setDisableMessageTimestamp(value);
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public boolean getDisableMessageTimestamp() throws JMSException {
        try {
            return rp2psp.getDisableMessageTimestamp();
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public void setDeliveryMode(int deliveryMode) throws JMSException {
        try {
            rp2psp.setDeliveryMode(deliveryMode);
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public int getDeliveryMode() throws JMSException {
        try {
            return rp2psp.getDeliveryMode();
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public void setPriority(int priority) throws JMSException {
        try {
            rp2psp.setPriority(priority);
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public int getPriority() throws JMSException {
        try {
            return rp2psp.getPriority();
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public void setTimeToLive(long timeToLive) throws JMSException {
        try {
            rp2psp.setTimeToLive(timeToLive);
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public long getTimeToLive() throws JMSException {
        try {
            return rp2psp.getTimeToLive();
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public Destination getDestination() throws JMSException {
        try {
            return new Peer2PeerQueueTopic(rp2psp.getDestination());
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public void close() throws JMSException {
        try {
            rp2psp.close();
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public Queue getQueue() throws JMSException {
        try {
            return new Peer2PeerQueueTopic(rp2psp.getQueue());
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public void send(Message message) throws JMSException {
        try {
            rp2psp.send(message);
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public void send(Message message, int deliveryMode, int priority, long timeToLive) throws JMSException {
        try {
            rp2psp.send(message, deliveryMode, priority, timeToLive);
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public void send(Destination destination, Message message) throws JMSException {
        try {
            rp2psp.send(((Peer2PeerQueueTopic)destination).rp2pqt, message);
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public void send(Destination destination, Message message, int deliveryMode, int priority, long timeToLive) throws JMSException {
        try {
            rp2psp.send(((Peer2PeerQueueTopic)destination).rp2pqt, message, deliveryMode, priority, timeToLive);
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public void send(Queue queue, Message message) throws JMSException {
        try {
            rp2psp.send(((Peer2PeerQueueTopic)queue).rp2pqt, message);
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public void send(Queue queue, Message message, int deliveryMode, int priority, long timeToLive) throws JMSException {
        try {
            rp2psp.send(((Peer2PeerQueueTopic)queue).rp2pqt, message, deliveryMode, priority, timeToLive);
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public Topic getTopic() throws JMSException {
        try {
            return new Peer2PeerQueueTopic(rp2psp.getTopic());
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public void publish(Message message) throws JMSException {
        try {
            rp2psp.publish(message);
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public void publish(Message message, int deliveryMode, int priority, long timeToLive) throws JMSException {
        try {
            rp2psp.publish(message, deliveryMode, priority, timeToLive);
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public void publish(Topic topic, Message message) throws JMSException {
        try {
            rp2psp.publish(((Peer2PeerQueueTopic)topic).rp2pqt, message);
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public void publish(Topic topic, Message message, int deliveryMode, int priority, long timeToLive) throws JMSException {
        try {
            rp2psp.publish(((Peer2PeerQueueTopic)topic).rp2pqt, message, deliveryMode, priority, timeToLive);
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

}
