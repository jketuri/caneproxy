
package FI.realitymodeler.p2p;

import java.rmi.*;
import java.rmi.server.*;
import java.util.*;
import javax.jms.*;

public class RemotePeer2PeerConnectionMetaDataImpl extends UnicastRemoteObject implements RemotePeer2PeerConnectionMetaData {
    static final long serialVersionUID = 0L;

    public RemotePeer2PeerConnectionMetaDataImpl() throws RemoteException {
    }

    public String getJMSVersion() throws JMSException {
        return null;
    }

    public int getJMSMajorVersion() throws JMSException {
        return 0;
    }

    public int getJMSMinorVersion() throws JMSException {
        return 0;
    }

    public String getJMSProviderName() throws JMSException {
        return null;
    }

    public String getProviderVersion() throws JMSException {
        return null;
    }

    public int getProviderMajorVersion() throws JMSException {
        return 0;
    }

    public int getProviderMinorVersion() throws JMSException {
        return 0;
    }

    public Enumeration getJMSXPropertyNames() throws JMSException {
        return null;
    }

}
