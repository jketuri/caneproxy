
package FI.realitymodeler.p2p;

import java.rmi.*;
import javax.jms.*;

public class Peer2PeerConnectionConsumer implements ConnectionConsumer {
    RemotePeer2PeerConnectionConsumer rp2pcc;

    public Peer2PeerConnectionConsumer(RemotePeer2PeerConnectionConsumer rp2pcc) {
        this.rp2pcc = rp2pcc;
    }

    public ServerSessionPool getServerSessionPool() throws JMSException {
        try {
            return new Peer2PeerServerSessionPool(rp2pcc.getServerSessionPool());
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public void close() throws JMSException {
        try {
            rp2pcc.close();
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

}
