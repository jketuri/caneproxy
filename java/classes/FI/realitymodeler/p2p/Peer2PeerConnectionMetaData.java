
package FI.realitymodeler.p2p;

import java.rmi.*;
import java.util.*;
import javax.jms.*;

public class Peer2PeerConnectionMetaData implements ConnectionMetaData {
    RemotePeer2PeerConnectionMetaData rp2pcmd;

    public Peer2PeerConnectionMetaData(RemotePeer2PeerConnectionMetaData rp2pcmd) {
        this.rp2pcmd = rp2pcmd;
    }

    public String getJMSVersion() throws JMSException {
        try {
            return rp2pcmd.getJMSVersion();
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public int getJMSMajorVersion() throws JMSException {
        try {
            return rp2pcmd.getJMSMajorVersion();
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public int getJMSMinorVersion() throws JMSException {
        try {
            return rp2pcmd.getJMSMinorVersion();
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public String getJMSProviderName() throws JMSException {
        try {
            return rp2pcmd.getJMSProviderName();
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public String getProviderVersion() throws JMSException {
        try {
            return rp2pcmd.getProviderVersion();
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public int getProviderMajorVersion() throws JMSException {
        try {
            return rp2pcmd.getProviderMajorVersion();
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public int getProviderMinorVersion() throws JMSException {
        try {
            return rp2pcmd.getProviderMinorVersion();
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }

    }

    public Enumeration getJMSXPropertyNames() throws JMSException {
        try {
            return rp2pcmd.getJMSXPropertyNames();
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

}
