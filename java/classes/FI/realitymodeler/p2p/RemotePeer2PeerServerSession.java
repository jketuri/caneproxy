
package FI.realitymodeler.p2p;

import java.rmi.*;
import javax.jms.*;

public interface RemotePeer2PeerServerSession extends Remote {

    RemotePeer2PeerSession getSession() throws RemoteException, JMSException;

    void start() throws RemoteException, JMSException;

}
