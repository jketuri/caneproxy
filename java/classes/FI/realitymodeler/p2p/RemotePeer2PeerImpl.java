
package FI.realitymodeler.p2p;

import FI.realitymodeler.common.*;
import java.rmi.*;
import java.rmi.registry.*;
import java.rmi.server.*;
import java.security.*;
import java.util.*;
import javax.jms.*;
import javax.servlet.*;

public class RemotePeer2PeerImpl extends UnicastRemoteObject implements RemotePeer2Peer, Servlet, Comparator<String> {
    static final long serialVersionUID = 0L;

    boolean verbose = false;

    private String name;
    private ServletConfig config;
    private ServletContext context;
    private WeakHashMap<String,RemotePeer2PeerQueueTopic> queues = new WeakHashMap<String,RemotePeer2PeerQueueTopic>();
    private WeakHashMap<String,RemotePeer2PeerQueueTopic> topics = new WeakHashMap<String,RemotePeer2PeerQueueTopic>();
    private TreeMap<String,RemotePeer2PeerQueueTopic> topicMap = new TreeMap<String,RemotePeer2PeerQueueTopic>(this);

    public RemotePeer2PeerImpl()
        throws RemoteException {
    }

    public int compare(String o1, String o2) {
        return o1.compareTo(o2);
    }

    public void init(ServletConfig config)
        throws ServletException {
        this.config = config;
        context = config.getServletContext();
        boolean optional = false;
        try {
            if (Support.isTrue(config.getInitParameter("optional"), "optional")) optional = true;
            if (Support.isTrue(config.getInitParameter("verbose"), "verbose") ||
                context.getAttribute("FI.realitymodeler.server.W3Server/verbose") == Boolean.TRUE) verbose = true;
            if ((name = config.getInitParameter("bindName")) == null)
                name = "rmi://localhost:" + Registry.REGISTRY_PORT + "/" + getClass().getName();
            Naming.rebind(name, this);
            if (verbose) context.log("Bound " + name);
        } catch (Exception ex) {
            if (optional) {
                context.log(ex.toString(), ex);
                return;
            }
            throw new ServletException(Support.stackTrace(ex));
        }
    }

    public ServletConfig getServletConfig() {
        return config;
    }

    public void service(ServletRequest req, ServletResponse res) {
    }

    public String getServletInfo() {
        return getClass().getName();
    }

    public void destroy() {
        try {
            Naming.unbind(name);
        } catch (Exception ex) {}
    }

    public RemotePeer2PeerConnection createConnection()
        throws RemoteException, JMSException {
        return new RemotePeer2PeerConnectionImpl(this);
    }

    public RemotePeer2PeerConnection createConnection(String userName, String password)
        throws RemoteException, JMSException {
        return createConnection();
    }

    public RemotePeer2PeerConnection createQueueConnection()
        throws RemoteException, JMSException {
        return createConnection();
    }

    public RemotePeer2PeerConnection createQueueConnection(String userName, String password)
        throws RemoteException, JMSException {
        return createConnection();
    }

    public RemotePeer2PeerConnection createTopicConnection()
        throws RemoteException, JMSException {
        return createConnection();
    }

    public RemotePeer2PeerConnection createTopicConnection(String userName, String password)
        throws RemoteException, JMSException {
        return createConnection();
    }

    public RemotePeer2PeerQueueTopic createQueue(String queueName)
        throws RemoteException, JMSException {
        RemotePeer2PeerQueueTopic queue = new RemotePeer2PeerQueueTopicImpl(queueName);
        queues.put(queueName, queue);
        return queue;
    }

    public RemotePeer2PeerQueueTopic createTopic(String topicName)
        throws RemoteException, JMSException {
        RemotePeer2PeerQueueTopic topic = new RemotePeer2PeerQueueTopicImpl(topicName);
        topics.put(topicName, topic);
        topicMap.put(topicName, topic);
        return topic;
    }

    public RemotePeer2PeerQueueTopic getQueue(String queueName)
        throws RemoteException, JMSException {
        return queues.get(queueName);
    }

    public RemotePeer2PeerQueueTopic getTopic(String topicName)
        throws RemoteException, JMSException {
        return topics.get(topicName);
    }

    public RemotePeer2PeerQueueTopic iterateTopics()
        throws RemoteException, JMSException {
        return topicMap.values().iterator().next();
    }

    public RemotePeer2PeerQueueTopic iterateTopic(Topic topic)
        throws RemoteException, JMSException {
        return topicMap.tailMap(topic.getTopicName()).values().iterator().next();
    }

}
