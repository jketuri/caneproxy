
package FI.realitymodeler.p2p;

import java.rmi.*;
import java.rmi.server.*;
import java.util.*;
import javax.jms.*;

public class RemotePeer2PeerQueueTopicImpl extends UnicastRemoteObject implements RemotePeer2PeerQueueTopic {
    static final long serialVersionUID = 0L;

    private String name;
    private WeakHashMap<String,Message> messages = new WeakHashMap<String,Message>();

    volatile long number = 0;

    private synchronized void waitForMessage(long timeOut) throws InterruptedException {
        if (timeOut == -1L) wait();
        else wait(timeOut);
    }

    public RemotePeer2PeerQueueTopicImpl(String name) throws RemoteException {
        this.name = name;
    }

    public String getQueueTopicName() {
        return name;
    }

    public void delete() throws JMSException {
    }

    public synchronized void add(Message message) {
        messages.put(Long.toString(number++, Character.MAX_RADIX), message);
        notifyAll();
    }

    public Peer2PeerMessage get(boolean remove, long timeOut, RemotePeer2PeerReceiverSubscriber receiverSubscriber) throws RemoteException, InterruptedException {
        Peer2PeerMessage message;
        String ID;
        for (;;) {
            while (receiverSubscriber.getNumber() < number)
                if ((message = (Peer2PeerMessage)messages.get(ID = Long.toString(receiverSubscriber.incrementNumber(), Character.MAX_RADIX))) != null) {
                    if (remove && --message.count <= 0) messages.remove(ID);
                    return message;
                }
            if (timeOut == 0L) return null;
            waitForMessage(timeOut);
        }
    }

}
