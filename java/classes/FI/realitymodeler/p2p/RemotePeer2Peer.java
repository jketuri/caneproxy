
package FI.realitymodeler.p2p;

import java.rmi.*;
import java.security.*;
import javax.jms.*;

public interface RemotePeer2Peer extends Remote {

    RemotePeer2PeerConnection createConnection() throws RemoteException, JMSException;

    RemotePeer2PeerConnection createConnection(String userName, String password) throws RemoteException, JMSException;

    RemotePeer2PeerConnection createQueueConnection() throws RemoteException, JMSException;

    RemotePeer2PeerConnection createQueueConnection(String userName, String password) throws RemoteException, JMSException;

    RemotePeer2PeerConnection createTopicConnection() throws RemoteException, JMSException;

    RemotePeer2PeerConnection createTopicConnection(String userName, String password) throws RemoteException, JMSException;

    RemotePeer2PeerQueueTopic createQueue(String queueName) throws RemoteException, JMSException;

    RemotePeer2PeerQueueTopic createTopic(String topicName) throws RemoteException, JMSException;

    RemotePeer2PeerQueueTopic getQueue(String queueName) throws RemoteException, JMSException;

    RemotePeer2PeerQueueTopic getTopic(String topicName) throws RemoteException, JMSException;

    RemotePeer2PeerQueueTopic iterateTopics() throws RemoteException, JMSException;

    RemotePeer2PeerQueueTopic iterateTopic(Topic topic) throws RemoteException, JMSException;

}
