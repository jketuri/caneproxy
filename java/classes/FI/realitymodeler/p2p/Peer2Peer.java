
package FI.realitymodeler.p2p;

import java.net.*;
import java.rmi.*;
import java.rmi.registry.*;
import java.security.*;
import java.util.*;
import javax.jms.*;

public class Peer2Peer implements QueueConnectionFactory, TopicConnectionFactory {
    RemotePeer2Peer rp2p;

    public static Peer2Peer getPeer2Peer(String host) throws RemoteException, MalformedURLException, NotBoundException {
        return new Peer2Peer((RemotePeer2Peer)Naming.lookup("rmi://" + (host.indexOf(':') != -1 ? host : host + ":" + Registry.REGISTRY_PORT) + "/FI.realitymodeler.p2p.RemotePeer2PeerImpl"));
    }

    public Peer2Peer(RemotePeer2Peer rp2p) {
        this.rp2p = rp2p;
    }

    public Connection createConnection() throws JMSException {
        try {
            return new Peer2PeerConnection(rp2p.createConnection());
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public Connection createConnection(String userName, String password) throws JMSException {
        try {
            return new Peer2PeerConnection(rp2p.createConnection(userName, password));
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public QueueConnection createQueueConnection() throws JMSException {
        try {
            return new Peer2PeerConnection(rp2p.createQueueConnection());
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public QueueConnection createQueueConnection(String userName, String password) throws JMSException {
        try {
            return new Peer2PeerConnection(rp2p.createQueueConnection(userName, password));
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public TopicConnection createTopicConnection() throws JMSException {
        try {
            return new Peer2PeerConnection(rp2p.createTopicConnection());
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public TopicConnection createTopicConnection(String userName, String password) throws JMSException {
        try {
            return new Peer2PeerConnection(rp2p.createTopicConnection(userName, password));
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public javax.jms.Queue getQueue(String queueName) throws JMSException {
        try {
            RemotePeer2PeerQueueTopic queue = rp2p.getQueue(queueName);
            return queue != null ? new Peer2PeerQueueTopic(queue) : null;
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public Topic getTopic(String topicName) throws JMSException {
        try {
            RemotePeer2PeerQueueTopic topic = rp2p.getTopic(topicName);
            return topic != null ? new Peer2PeerQueueTopic(topic) : null;
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public Topic iterateTopics() throws JMSException {
        try {
            return new Peer2PeerQueueTopic(rp2p.iterateTopics());
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

    public Topic iterateTopic(Topic topic) throws JMSException {
        try {
            return new Peer2PeerQueueTopic(rp2p.iterateTopic(topic));
        } catch (RemoteException ex) {
            throw new JMSException(ex.toString());
        }
    }

}
