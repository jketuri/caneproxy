
package FI.realitymodeler.p2p;

import java.rmi.*;
import javax.jms.*;

public interface RemotePeer2PeerConnectionConsumer extends Remote {

    RemotePeer2PeerServerSessionPool getServerSessionPool() throws RemoteException, JMSException;

    void close() throws RemoteException, JMSException;

}
