
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.security.*;
import java.text.*;
import java.util.*;

/** Implements URL connection to Simple Mail Transfer Protocol Servers. URL must be of form:<br>
    mailto:recipients/subject/from
*/
public class W3SmtpURLConnection extends W3URLConnection {
    public static boolean useProxy = false;
    public static String proxyHost = null;
    public static int proxyPort = 80;
    public static String smtpServerHost = null;
    public static String fromAddr = null;
    public static String version = "FI.realitymodeler.W3SmtpURLConnection/2000-10-5";

    W3SmtpRequester smtpRequester;
    boolean secure = false;

    public W3SmtpURLConnection(URL url) {
        super(url);
        secure = url.getProtocol().equalsIgnoreCase("smailto");
    }

    public String getVersion() {
        return version;
    }

    public void open()
        throws IOException, ParseException {
        if (requester != null) return;
        if (useProxy) {
            proxyConnect(proxyHost, proxyPort);
            return;
        }
        requester = smtpRequester = new W3SmtpRequester(url.getHost().length() > 0 ? url.getHost() : smtpServerHost, url.getPort() != -1 ? url.getPort() : secure ? W3SmtpRequester.SMTP_SECURE_PORT : W3SmtpRequester.SMTP_PORT, localAddress, localPort, secure);
        if (timeout > 0) smtpRequester.setTimeout(timeout);
    }

    public void doConnect()
        throws IOException {
        getBasicCredentials();
        try {
            open();
        } catch (ParseException ex) {
            throw new IOException(Support.stackTrace(ex));
        }
    }

    public InputStream getInputStream()
        throws IOException {
        if (inputDone) return in;
        if (!loggedIn) return in = getUnauthorizedStream("Authorization required");
        if (doOutput) {
            closeStreams();
            inputDone = true;
            useCaches = false;
            if (requester == null) throw new IOException("Not connected");
            if (useProxy && mayKeepAlive) {
                in = requester.input;
                checkInput();
                return in;
            }
            mayKeepAlive = true;
            headerFields = new HeaderList();
            headerFields.append(new Header("", protocol + " " + HTTP_NO_CONTENT));
            return in = null;
        }
        mayKeepAlive = true;
        setRequestMethod();
        setRequestFields();
        return super.getInputStream();
    }

    public String mailRequestProperties(String address[], String serverHost)
        throws IOException {
        String to = null, from = null, subject = null;
        path = Support.decodePath(path);
        StringTokenizer st = new StringTokenizer(path, "/");
        if (st.hasMoreTokens()) {
            to = st.nextToken().trim();
            if (st.hasMoreTokens()) {
                subject = st.nextToken().trim();
                if (st.hasMoreTokens()) from = st.nextToken().trim();
            }
        }
        if (from == null)
            if ((from = getRequestProperty("from")) != null) from = Support.decodeWords(from);
            else if ((from = fromAddr) == null) from = System.getProperty("user.fromaddr");
        if (from != null) getAddress(address, from, serverHost);
        if (from != null && getRequestProperty("from") == null) setRequestProperty("From", Support.encodeWords(from));
        if (address[0] != null && getRequestProperty("reply-to") == null) setRequestProperty("Reply-To", address[0]);
        if (to != null && getRequestProperty("to") == null) setRequestProperty("To", Support.encodeWords(to));
        if (subject != null && getRequestProperty("subject") == null) setRequestProperty("Subject", Support.encodeWords(subject));
        if (getRequestProperty("x-mailer") == null) setRequestProperty("X-Mailer", getVersion());
        return to;
    }

    public OutputStream getOutputStream()
        throws IOException {
        doOutput = true;
        inputDone = false;
        connect();
        setRequestMethod();
        setDefaultRequestFields();
        setRequestPropertiesFromQuery();
        if (useProxy) return getOutput();
        unsetRequestProperty("authorization");
        for (boolean tried = false;; tried = true)
            try {
                String address[] = new String[2], to = mailRequestProperties(address, smtpServerHost);
                smtpRequester.from("<" + (address[0] != null ? address[0] : "Unknown") + ">");
                if (to != null) smtpRequester.to(to);
                OutputStream out = smtpRequester.startMessage();
                Support.sendHeaderList(out, requestHeaderFields, -1);
                loggedIn = true;
                return out;
            } catch (IOException ex) {
                if (loggedIn || tried || username == null || username.equals("")) throw ex;
                try {
                    if (smtpRequester.mechanisms != null) {
                        try {
                            smtpRequester.close();
                        } catch (IOException ex1) {}
                        smtpRequester.login(username, password);
                    }
                } catch (GeneralSecurityException ex1) {
                    throw new IOException(Support.stackTrace(ex1));
                }
                loggedIn = true;
            }
    }

    public int getDefaultPort() {
        return W3SmtpRequester.SMTP_PORT;
    }

    public boolean usingProxy() {
        return useProxy;
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public int getProxyPort() {
        return proxyPort;
    }

}
