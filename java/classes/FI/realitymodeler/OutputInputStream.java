
package FI.realitymodeler;

import java.io.*;

public class OutputInputStream extends FilterInputStream {
    OutputStream out;
    int markPos = 0, markedPos = 0, realPos = 0;

    public OutputInputStream(InputStream in, OutputStream out) {
        super(in);
        this.out = out;
    }

    public int read() throws IOException {
        int c = in.read();
        if (c == -1) return c;
        if (++markPos <= realPos) return c;
        out.write(c);
        realPos++;
        return c;
    }

    public int read(byte b[], int off, int len) throws IOException {
        int n = in.read(b, off, len);
        if (n <= 0) return n;
        int m = markPos - realPos;
        if ((markPos += n) <= realPos) return n;
        off += m;
        m = n - m;
        if (m > 0) out.write(b, off, m);
        realPos += m;
        return n;
    }

    public int read(byte b[]) throws IOException {
        return read(b, 0, b.length);
    }

    public void mark(int readlimit) {
        in.mark(readlimit);
        markedPos = markPos;
    }

    public void reset() throws IOException {
        in.reset();
        markPos = markedPos;
    }

}
