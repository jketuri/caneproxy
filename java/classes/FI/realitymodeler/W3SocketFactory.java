
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.util.*;
import javax.net.*;

public class W3SocketFactory extends SocketFactory {
    public static W3SocketFactory instance = null;

    public static synchronized void prepare() {
        if (instance != null) return;
        Support.prepare();
        instance = new W3SocketFactory();
    }

    public static SocketFactory getDefault() {
        return instance;
    }

    public Socket createSocket() throws IOException {
        return new W3Socket();
    }

    public Socket createSocket(String host, int port) throws IOException {
        return new W3Socket(host, port);
    }

    public Socket createSocket(String host, int port, InetAddress localHost, int localPort) throws IOException {
        return new W3Socket(host, port, localHost, localPort);
    }

    public Socket createSocket(InetAddress host, int port) throws IOException {
        return new W3Socket(host, port);
    }

    public Socket createSocket(InetAddress address, int port, InetAddress localAddress, int localPort) throws IOException {
        return new W3Socket(address, port, localAddress, localPort);
    }

}
