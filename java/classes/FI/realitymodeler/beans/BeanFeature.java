
package FI.realitymodeler.beans;

import java.beans.*;

public class BeanFeature {
    public Object bean;
    public FeatureDescriptor featureDescriptor;
    public String parameter;

    public BeanFeature(Object bean, FeatureDescriptor featureDescriptor) {
        this.bean = bean;
        this.featureDescriptor = featureDescriptor;
        this.parameter = null;
    }

    public BeanFeature(Object bean, FeatureDescriptor featureDescriptor, String parameter) {
        this.bean = bean;
        this.featureDescriptor = featureDescriptor;
        this.parameter = parameter;
    }

}
