
package FI.realitymodeler.beans;

import java.beans.*;
import java.beans.beancontext.*;
import java.io.*;
import java.lang.reflect.*;
import java.text.*;
import java.util.*;

public class BeanContainer {
    public static BeanContainer instance = null;
    public static BeanContextServicesSupport bcss = null;
    public static Map<String, Map<String, FeatureDescriptor>> featureDescriptorsMap = new HashMap<String, Map<String, FeatureDescriptor>>();
    public static Map<Class<?>, Class<?>> classesOfPrimitiveTypes = new HashMap<Class<?> ,Class<?>>();
    static {
        try {
            classesOfPrimitiveTypes.put(Boolean.TYPE, Class.forName("java.lang.Boolean"));
            classesOfPrimitiveTypes.put(Byte.TYPE, Class.forName("java.lang.Byte"));
            classesOfPrimitiveTypes.put(Character.TYPE, Class.forName("java.lang.Character"));
            classesOfPrimitiveTypes.put(Double.TYPE, Class.forName("java.lang.Double"));
            classesOfPrimitiveTypes.put(Float.TYPE, Class.forName("java.lang.Float"));
            classesOfPrimitiveTypes.put(Integer.TYPE, Class.forName("java.lang.Integer"));
            classesOfPrimitiveTypes.put(Long.TYPE, Class.forName("java.lang.Long"));
        } catch (Exception ex) {
            throw new Error(ex.toString());
        }
    }

    public Map<String, Object> beans = new HashMap<String, Object>();

    public static synchronized void prepare() {
        if (instance != null) return;
        instance = new BeanContainer();
        bcss = new BeanContextServicesSupport(null, Locale.getDefault(), false, false);
    }

    public static Map examine(Object bean)
        throws IntrospectionException {
        Map<String, FeatureDescriptor> map = featureDescriptorsMap.get(bean.getClass().getName());
        if (map != null) return map;
        map = new HashMap<String, FeatureDescriptor>();
        BeanInfo beanInfo = Introspector.getBeanInfo(bean.getClass());
        PropertyDescriptor propertyDescriptors[] = beanInfo.getPropertyDescriptors();
        for (int i = 0; i < propertyDescriptors.length; i++) map.put(propertyDescriptors[i].getName(), propertyDescriptors[i]);
        MethodDescriptor methodDescriptors[] = beanInfo.getMethodDescriptors();
        for (int i = 0; i < methodDescriptors.length; i++) map.put(methodDescriptors[i].getName(), methodDescriptors[i]);
        featureDescriptorsMap.put(bean.getClass().getName(), map);
        return map;
    }

    public static Method getCloseMethod(Map<String, FeatureDescriptor> map) {
        FeatureDescriptor featureDescriptor = map.get("close");
        return featureDescriptor instanceof MethodDescriptor ? ((MethodDescriptor)featureDescriptor).getMethod() : null;
    }

    public static void callCloseMethod(Object bean, Method closeMethod) {
        try {
            closeMethod.invoke(bean, new Object[0]);
        } catch (IllegalAccessException ex) {} catch (InvocationTargetException ex) {}
    }

    public static Object getBean(String name, BeanContainer beanContainers[]) {
        Object bean;
        for (int i = beanContainers.length - 1; i >= 0; i--)
            if ((bean = beanContainers[i].beans.get(name)) != null) return bean;
        return null;
    }

    public static BeanFeature getBeanFeature(Object bean, String beanName, String propertyName, boolean setting)
        throws IllegalAccessException, InvocationTargetException, ParseException {
        if (propertyName != null)
            for (StringTokenizer st = new StringTokenizer(propertyName, ":{}", true); st.hasMoreTokens();) {
                String name = st.nextToken().trim();
                if (name.equals(":")) continue;
                if (name.equals("}")) break;
                if (name.equals("{")) {
                    // Property name is evaluated
                    Object value = getBeanProperty(bean, beanName, st.nextToken("").substring(1));
                    name = value instanceof String ? (String)value : value.toString();
                }
                if (name.equals("")) throw new ParseException("Invalid property name " + propertyName + " in " + beanName, 0);
                Map map = featureDescriptorsMap.get(bean.getClass().getName());
                FeatureDescriptor featureDescriptor = (FeatureDescriptor)map.get(name);
                boolean isLastName = !st.hasMoreTokens();
                if (featureDescriptor == null) featureDescriptor = (FeatureDescriptor)map.get((isLastName && setting ? "set" : "get") +
                                                                                              Character.toUpperCase(name.charAt(0)) + name.substring(1));
                if (featureDescriptor == null) throw new ParseException("Property " + name + " not found from " + propertyName + " in " + beanName, 0);
                if (isLastName) return new BeanFeature(bean, featureDescriptor);
                if (featureDescriptor instanceof MethodDescriptor) {
                    if (!st.nextToken().equals(":") || !st.hasMoreTokens()) throw new ParseException("Invalid property name " + propertyName + " in " + beanName, 0);
                    String parameter = st.nextToken();
                    if (!st.hasMoreTokens()) return new BeanFeature(bean, featureDescriptor, parameter);
                    bean = ((MethodDescriptor)featureDescriptor).getMethod().invoke(bean, new Object[] {parameter});
                    if (bean == null) throw new ParseException("Property " + name + " with parameter " + parameter + " has no value in " + beanName, 0);
                } else {
                    bean = ((PropertyDescriptor)featureDescriptor).getReadMethod().invoke(bean, new Object[0]);
                    if (bean == null) throw new ParseException("Property " + name + " has no value in " + beanName, 0);
                }
                beanName = name;
            }
        return bean instanceof BeanFeature ? (BeanFeature)bean : new BeanFeature(bean, null);
    }

    public static Object getProperty(BeanFeature beanFeature)
        throws IllegalAccessException, InvocationTargetException, ParseException {
        return beanFeature.featureDescriptor == null ? beanFeature.bean :
            beanFeature.featureDescriptor instanceof PropertyDescriptor ?
            ((PropertyDescriptor)beanFeature.featureDescriptor).getReadMethod().invoke(beanFeature.bean, new Object[0]) :
            ((MethodDescriptor)beanFeature.featureDescriptor).getMethod().invoke(beanFeature.bean, new Object[] {beanFeature.parameter});
    }

    public static Object getProperty(BeanFeature beanFeature, int index)
        throws IllegalAccessException, InvocationTargetException, ParseException {
        return beanFeature.featureDescriptor instanceof PropertyDescriptor ?
            ((PropertyDescriptor)beanFeature.featureDescriptor).getReadMethod().invoke(beanFeature.bean, new Object[] {new Integer(index)}) :
            ((MethodDescriptor)beanFeature.featureDescriptor).getMethod().invoke(beanFeature.bean, new Object[] {beanFeature.parameter, new Integer(index)});
    }

    /** Gets property value of a bean.  The name of the property is
        expressed using the dynamic hierarchical naming scheme. A property name
        is constructed as follows:<br>

        [<i>propertyName</i>[: {]<i>propertyName</i>[}]]+]<br>

        If there is more than one property name, it is considered nested
        property. If property is dynamic, it's parameter must be specified after
        it's name separated with colon. If property name is enclosed in braces,
        it is evaluated, and the result is used as property name or parameter.

        @param bean Bean whose value is accessed

        @param propertyName property name expressed using dynamic hierarchical naming scheme */

    public static Object getBeanProperty(Object bean, String beanName, String propertyName)
        throws IllegalAccessException, InvocationTargetException, ParseException {
        return getProperty(getBeanFeature(bean, beanName, propertyName, false));
    }

    public static Object getBeanProperty(Object bean, String beanName, String propertyName, int index)
        throws IllegalAccessException, InvocationTargetException, ParseException {
        return getProperty(getBeanFeature(bean, beanName, propertyName, false), index);
    }

    public static Object convertValue(Object value, Method method)
        throws InstantiationException, IllegalAccessException, InvocationTargetException {
        Class<?> parameterTypes[] = method.getParameterTypes(), valueClass = value.getClass();
        if (valueClass != parameterTypes[parameterTypes.length - 1])
            try {
                Class<?> parameterClass = parameterTypes[parameterTypes.length - 1];
                if (parameterClass.isPrimitive()) parameterClass = (Class)classesOfPrimitiveTypes.get(parameterClass);
                Constructor constructor = parameterClass.getConstructor(new Class<?>[] {valueClass});
                value = constructor.newInstance(new Object[] {value});
            } catch (NoSuchMethodException ex) {}
        return value;
    }

    public static void setProperty(BeanFeature beanFeature, Object value)
        throws InstantiationException, IllegalAccessException, InvocationTargetException, ParseException {
        Method method = beanFeature.featureDescriptor instanceof PropertyDescriptor ?
            ((PropertyDescriptor)beanFeature.featureDescriptor).getWriteMethod() :
            ((MethodDescriptor)beanFeature.featureDescriptor).getMethod();
        value = convertValue(value, method);
        method.invoke(beanFeature.bean, beanFeature.parameter == null ?
                      new Object[] {value} : new Object[] {beanFeature.parameter, value});
    }

    public static void setProperty(BeanFeature beanFeature, int index, Object value)
        throws InstantiationException, IllegalAccessException, InvocationTargetException, ParseException {
        Method method = beanFeature.featureDescriptor instanceof PropertyDescriptor ?
            ((PropertyDescriptor)beanFeature.featureDescriptor).getWriteMethod() :
            ((MethodDescriptor)beanFeature.featureDescriptor).getMethod();
        value = convertValue(value, method);
        method.invoke(beanFeature.bean, beanFeature.parameter == null ?
                      new Object[] {new Integer(index), value} : new Object[] {beanFeature.parameter, new Integer(index), value});
    }

    /** Sets property value of a bean.

        @param bean Bean whose value is accessed

        @param propertyName property name expressed using dynamic hierarchical naming scheme

        @param value property value to be set.

        @see getBeanProperty */

    public static void setBeanProperty(Object bean, String beanName, String propertyName, Object value)
        throws InstantiationException, IllegalAccessException, InvocationTargetException, ParseException {
        setProperty(getBeanFeature(bean, beanName, propertyName, true), value);
    }

    public static void setBeanProperty(Object bean, String beanName, String propertyName, int index, Object value)
        throws InstantiationException, IllegalAccessException, InvocationTargetException, ParseException {
        setProperty(getBeanFeature(bean, beanName, propertyName, true), index, value);
    }

    public static void invokeMethod(Object bean, String methodName, Object args[])
        throws IllegalAccessException, InvocationTargetException, ParseException {
        Map map = featureDescriptorsMap.get(bean.getClass().getName());
        FeatureDescriptor featureDescriptor = (FeatureDescriptor)map.get(methodName);
        if (featureDescriptor instanceof MethodDescriptor) ((MethodDescriptor)featureDescriptor).getMethod().invoke(bean, args);
        else throw new ParseException("Method " + methodName + " of bean class " + bean.getClass().getName() + " not found", 0);
    }

    public final boolean containsBean(String name) {
        return beans.containsKey(name);
    }

    public final Object getBean(String name) {
        return beans.get(name);
    }

    public Object createBean(String name, String clazz)
        throws ClassNotFoundException, IntrospectionException, IOException {
        Object bean = bcss.instantiateChild(clazz);
        examine(bean);
        beans.put(name, bean);
        return bean;
    }

    public synchronized void close() {
        Iterator iter = beans.values().iterator();
        while (iter.hasNext()) {
            Object bean = iter.next();
            Map<String, FeatureDescriptor> map = featureDescriptorsMap.get(bean.getClass().getName());
            if (map == null) continue;
            Method closeMethod = getCloseMethod(map);
            if (closeMethod != null) callCloseMethod(bean, closeMethod);
        }
        beans.clear();
    }

}
