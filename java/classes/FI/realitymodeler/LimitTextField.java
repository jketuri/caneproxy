
package FI.realitymodeler;

import java.awt.*;
import java.awt.event.*;

public class LimitTextField extends TextField implements TextListener {
    static final long serialVersionUID = 0L;

    String old = null;

    public LimitTextField(int columns) {
        super(columns);
        addTextListener(this);
    }

    public void textValueChanged(TextEvent e) {
        if (getText().length() <= getColumns()) {
            old = getText();
            return;
        }
        if (old == null) return;
        int pos = getCaretPosition();
        setText(old);
        setCaretPosition(pos);
    }

}
