
package FI.realitymodeler;

import java.util.*;

public class VectorSet<E> extends Vector<E> {
    static final long serialVersionUID = 0L;

    public VectorSet union(VectorSet<E> vs) {
        Iterator<E> vsIter = vs.iterator();
        while (vsIter.hasNext()) {
            E element = vsIter.next();
            if (!contains(element)) addElement(element);
        }
        return this;
    }

    public VectorSet exclusiveUnion(VectorSet<E> vs) {
        Iterator<E> vsIter = vs.iterator();
        while (vsIter.hasNext()) {
            E element = vsIter.next();
            if (!remove(element)) addElement(element);
        }
        return this;
    }

    public VectorSet intersection(VectorSet<Object> vs) {
        Iterator<E> iter = iterator();
        while (iter.hasNext())
            if (!vs.contains(iter.next())) iter.remove();
        return this;
    }

    public VectorSet difference(VectorSet<Object> vs) {
        Iterator<E> iter = iterator();
        while (iter.hasNext())
            if (vs.contains(iter.next())) iter.remove();
        return this;
    }

}
