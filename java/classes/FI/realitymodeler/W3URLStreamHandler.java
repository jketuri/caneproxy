
package FI.realitymodeler;

import java.net.*;

public abstract class W3URLStreamHandler extends URLStreamHandler
{

    public abstract URLConnection openConnection(URL u);

    public URLConnection openConnection(URL u, Proxy p) {
        return openConnection(u);
    }

}
