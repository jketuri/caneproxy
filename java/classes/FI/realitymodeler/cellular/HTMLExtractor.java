
package FI.realitymodeler.cellular;

import java.io.*;
import java.net.*;
import java.util.*;

public class HTMLExtractor extends LinkExtractor {
    static Hashtable<String, String> names = new Hashtable<String, String>();
    static {
        names.put("a", "href");
        names.put("img", "src");
    }

    /** Extracts links from HTML-page.
        @param in Input stream where HTML-page is read
        @param baseURL URL where HTML-page originally comes from or null
        @param foundURLs Vector where found links are added
        @param doneURLs Hashtable which is used to check links already found or null
    */
    public static void extract(InputStream in, URL baseURL, Vector<URL> foundURLs, Hashtable<URL, Boolean> doneURLs)
        throws IOException {
	LinkExtractor.extract(in, baseURL, foundURLs, doneURLs, names);
    }

}
