
package FI.realitymodeler.cellular;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import FI.realitymodeler.server.*;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;
import javax.naming.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Internet gateway for mobile phones which is configured as servlet and runs as server.<br>
    Available initialization parameters are:<br>
    bindAddress=virtual phone number, which receives short message requests (if this is not specified
    it is taken from system property sms.fromaddr). In case of UDP, this must IP-address to bind. It defaults to loopback.<br>
    datagramType=datagram protocol which is used in transport (must be 'cimd', 'nbs', 'mobile' or 'udp', default is 'udp')<br>
    keepAliveTimeout=session time in seconds input is waited from client and temporary cookies and
    authentication information is preserved (defaults to 15 minutes).<br>
    linkLifeSpan=life span in minutes links stay in store if they are not used (defaults to one week).<br>
    maxPlainMessages=maximun number of plain messages sent in succession as one response (defaults to 10).<br>
    maxNbsMessages=maximum number of NBS-messages which can be sent in succession (currently 3).<br>
    pollingInterval=interval in minutes virtual phone number is polled for received requests.
*/
public class CellularEngine extends W3Server implements Servlet {
    static Boolean initializedLock = new Boolean(true);
    static String language = Locale.getDefault().getLanguage();
    static volatile boolean initialized = false;

    ServletConfig config = null;
    W3Server w3server = null;
    byte bindAddress[] = null;
    int pollingInterval;
    int type;

    public ServerSocket getServerSocket() throws IOException {
	return new CellularServerSocket(bindAddress, port, type);
    }

    public W3Request getW3Request() {
	return new CellularRequest(this);
    }

    public String getHostAddress() {
	return new String(bindAddress);
    }

    public String getHostName() {
	return getHostAddress();
    }

    public void init(ServletConfig config) throws ServletException {
	this.config = config;
	w3server = ((W3Context)config.getServletContext()).getW3Server();
	if (w3server.logLevel > 1) w3server.log("Initializing " + getClass().getName());
	type = CellularSocket.UDP;
	String s;
	if ((s = config.getInitParameter("datagramType")) != null) {
            if ((s = s.toLowerCase()).equals("cimd")) type = CellularSocket.CIMD;
            else if (s.equals("nbs")) type = CellularSocket.NBS;
            else if (s.equals("mobile")) type = CellularSocket.MOBILE;
            else if (!s.equals("udp")) throw new ServletException("Unknown datagram type " + s);
	}
	keepAliveTimeout = ((s = config.getInitParameter("keepAliveTimeout")) != null ?
                            Integer.parseInt(s) : 15 * 60) * 1000;
	LinkStore.linkLifeSpan = ((s = config.getInitParameter("linkLifeSpan")) != null ?
                                  Long.parseLong(s) : 7L * 24L * 60L) * 60L * 1000L;
	pollingInterval = ((s = config.getInitParameter("pollingInterval")) != null ?
                           Integer.parseInt(s) : 5) * 1000;
	if ((s = config.getInitParameter("maxPlainMessages")) != null) CellularSocket.setMaxPlainMsgs(Integer.parseInt(s));
	if ((s = config.getInitParameter("maxNbsMessages")) != null) CellularSocket.setMaxNbsMsgs(Integer.parseInt(s));
	verbose = w3server.verbose;
	logLevel = w3server.logLevel;
	context = w3server.context;
	defaultContext = w3server.defaultContext;
	maxRequests = w3server.maxRequests;
	timeout = w3server.timeout;
        w3context = new W3Context(w3server);
	for (int i = 0; i < 3; i++) logs[i] = w3server.logs[i];
	port = type == CellularSocket.UDP ? CellularSocket.WAP_CL_PORT : CellularSocket.TTML_PORT;
	dataDirectory = null;
	try {
            s = config.getInitParameter("bindAddress");
            if (type != CellularSocket.UDP)
		if (s == null && (s = System.getProperty("sms.fromaddr")) == null)
                    throw new ServletException("Bind address is not specified");
		else bindAddress = s.getBytes();
            else bindAddress = InetAddress.getByName(s).getAddress();
            synchronized (initializedLock) {
                if (!initialized) {
                    initialized = true;
                    EntityEncoding.initialize(null, null, false);
                }
            }
            begin();
	} catch (Exception ex) {
            if (ex instanceof ServletException) throw (ServletException)ex;
            throw new ServletException(Support.stackTrace(ex));
	}
    }

    public ServletConfig getServletConfig() {
	return config;
    }

    public void service(ServletRequest req, ServletResponse res) {
    }

    public String getServletInfo() {
	return getClass().getName();
    }

    public void destroy() {
	if (w3server.logLevel > 1) w3server.log("Destroying " + getClass().getName());
	end();
    }

    /** Makes a minimal label from link. */
    public static String getLabel(String s) {
	int i = s.lastIndexOf('?');
	if (i != -1) s = s.substring(0, i);
	return s.substring(s.lastIndexOf('/') + 1).trim();
    }

    public static String modify(String s) {
	return s.replace('_', '\306').replace('@', '\346');
    }

    public static String restore(String s) {
	return s.replace('\306', '_').replace('\346', '@');
    }

    /** Filters HTML- or plain text input stream to TTML or reduced text.
        @param in input stream to be filtered
        @param out output stream to write
        @param context URL where input stream comes from
        @param nbs indicates that output stream is to NBS-TTML-port
        @param html indicates that input stream is HTML
        @return returns true if reading of input stream was completed. */
    public static boolean filter(LinkStore linkStore, InputStream in, OutputStream out, URL context, boolean nbs, boolean html) throws IOException, NamingException {
	ByteArrayOutputStream bout = new ByteArrayOutputStream();
	Form form = null;
	Vector<Object> fields = null;
	OutputStream out0 = out;
	String label = null, setName = null, setType = null, submit = null;
	URL url = null;
	Vector<Link> linkList = new Vector<Link>();
        Vector<Form> formList = new Vector<Form>();
	boolean added = false, eof[] = new boolean[1], inForm = false, inLink = false, info = false, valid = false, wasChar = false;
	int c, footNum = 1, formRefNum = 1;
	while ((c = in.read()) != -1) {
            if (!html || c != '<') {
                // coded character
                if (html && c == '&' && (c = Support.convert(in, eof)) == -1 || inForm)
                    if (eof[0]) break;
                    else continue;
                // character
                boolean isChar = c > 32;
                if (isChar) {
                    if (info) {
                        if (!wasChar) out.write(' ');
                    } else if (CellularSocket.reserved.indexOf(c) != -1)
                        if (eof[0]) break;
                        else continue;
                    out.write(c);
                    info = true;
                }
                wasChar = isChar;
                if (eof[0]) break;
                continue;
            }
            // tag
            wasChar = false;
            if ((c = Support.scanTag(in, "a", false)) == Support.TAG_WITH_VALUES) {
                // a
                Map<String, String> values = new HashMap<String, String>();
                c = Support.scanValues(in, values);
                if (c == -1) break;
                String href = values.get("href");
                url = null;
                if (href != null && context != null)
                    try {
                        url = new URL(context, href);
                        label = getLabel(href);
                        out = bout;
                        bout.reset();
                        inLink = true;
                    } catch (MalformedURLException ex) {}
            } else if ((c = Support.scanTag(in, "/a", true)) <= Support.TAG_WITH_VALUES) {
                if (inLink) {
                    String link = new String(bout.toByteArray()).trim();
                    if (!link.equals("")) label = link;
                    linkList.addElement(new Link(url));
                    label += "*" + footNum++;
                    out = out0;
                    if (info) out.write(' ');
                    Support.writeBytes(out, label, null);
                    inLink = false;
                    info = true;
                }
            } else if ((c = Support.scanTag(in, "form", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) {
                // form
                Map<String, String> values = new HashMap<String, String>();
                c = Support.scanValues(in, values);
                if (c == -1) break;
                inForm = true;
                added = valid = false;
                String action = values.get("action"), method = values.get("method");
                if (action != null && method != null && context != null)
                    try {
                        form = new Form(new URL(context, action), method, values.get("encType"));
                        fields = form.getFields();
                        if (info) {
                            out.write('\n');
                            info = false;
                        }
                        formList.addElement(form);
                        Support.writeBytes(out, "><F" + (nbs ? "": "*") + formRefNum++ + "\n", null);
                        valid = true;
                    } catch (MalformedURLException ex) {}
                setName = setType = null;
            } else if ((c = Support.scanTag(in, "/form", true)) <= 0) {
                // form ends
                if (nbs && valid) break;
                inForm = valid = false;
            } else if ((c = Support.scanTag(in, "img", true)) == Support.TAG_WITH_VALUES) {
                // img
                Map<String, String> values = new HashMap<String, String>();
                c = Support.scanValues(in, values);
                String s = values.get("alt");
                if (s != null) label = s;
                else if ((s = values.get("src")) != null) label = getLabel(s);
                if (!inLink && s != null && !(s = s.trim()).equals("")) {
                    if (info) out.write(' ');
                    Support.writeBytes(out, "/" + s + "/", null);
                    info = true;
                }
            } else if ((c = Support.scanTag(in, "script", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) {
                // script
                while (c > -1 && c != '>') c = in.read();
                if (c == -1) break;
                while ((c = in.read()) != -1)
                    if (c == '<') {
                        if ((c = Support.scanTag(in, "/script", false)) <= 0) break;
                        while (c > -1 && c != '>') c = in.read();
                        if (c == -1) break;
                    }
                if (c == -1) break;
            } else if ((c = Support.scanTag(in, "!--", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) {
                // comment
                for (;;) {
                    in.mark(3);
                    if ((c = Support.scanTag(in, "--", false)) < 0) break;
                    in.reset();
                    if ((c = in.read()) == -1) break;
                }
                if (c == -1) break;
                continue;
            } else if ((c = Support.scanTag(in, "title", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) {
                if (c == -1) break;
                while ((c = in.read()) != -1)
                    if (c == '<' && (c = Support.scanTag(in, "/title", false)) <= 0) break;
            } else if ((c = Support.scanTag(in, "ttmlaction", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) {
                while (c > -1 && c != '>') c = in.read();
                if (c == -1) break;
                StringBuffer sb = new StringBuffer();
                while ((c = in.read()) != -1 && c != '<') sb.append((char)c);
                if (c == -1) break;
                if (info) {
                    out.write('\n');
                    info = false;
                }
                Support.writeBytes(out, "><A " + sb.toString() + "><a\n", null);
            } else if (valid)
		if ((c = Support.scanTag(in, "input", true)) == Support.TAG_WITH_VALUES) {
                    // input
                    Map<String, String> values = new HashMap<String, String>();
                    c = Support.scanValues(in, values);
                    String type = values.get("type");
                    boolean multiple;
                    if (type == null || type.equals("text") || type.equals("password")) {
                        String name = values.get("name");
                        if (name != null) {
                            if (info) {
                                out.write('\n');
                                info = false;
                            }
                            Support.writeBytes(out, (values.containsKey("digitentry") ? "<<" : "<") + modify(name) + ":" + (values.containsKey("value") ? modify(values.get("value")) : "") + "\n", null);
                            fields.addElement(name);
                            added = true;
                        }
                    } else if (type.equals("hidden")) fields.addElement(new Field(
                                                                                  values.containsKey("name") ? values.get("name") : "",
                                                                                  values.containsKey("value") ? values.get("value") : "", true));
                    else if ((multiple = type.equals("checkbox")) || type.equals("radio")) {
                        String name = values.get("name");
                        if (name != null) {
                            String value = values.get("value");
                            if (info) {
                                out.write('\n');
                                info = false;
                            }
                            if (setName == null || !setName.equals(name) || !type.equals(setType)) {
                                Support.writeBytes(out, (multiple ? "<." : ">.") + modify(name) + "\n", null);
                                setName = name;
                                setType = type;
                            }
                            Support.writeBytes(out, ".>" + (values.containsKey("checked") ? " " : multiple ? "#" : "*") + (value != null ? value : "on") + "\n", null);
                            fields.addElement(name);
                            added = true;
                        }
                    } else if (type.equals("submit") && values.containsKey("value")) submit = values.get("value");
		} else if ((c = Support.scanTag(in, "select", true)) == Support.TAG_WITH_VALUES) {
                    // select
                    Map<String, String> values = new HashMap<String, String>();
                    c = Support.scanValues(in, values);
                    if (c == -1) break;
                    boolean multiple = values.containsKey("multiple");
                    String firstValue = null, name = values.get("name");
                    if (name != null) {
                        if (info) {
                            out.write('\n');
                            info = false;
                        }
                        Support.writeBytes(out, (multiple ? "<." : ">.") + modify(name) + "\n", null);
                    }
                    String s;
                    boolean empty = multiple || (s = values.get("size")) != null && !s.equals("1");
                    StringBuffer sb = new StringBuffer();
                    values = null;
                    while ((c = in.read()) != -1)
			if (c == '<') {
                            if (name != null && values != null) {
                                String value = values.get("value");
                                if (value == null) value = sb.toString();
                                Support.writeBytes(out, ".>" + (values.containsKey("selected") ? " " : multiple ? "#" : "*") + modify(value) + "\n", null);
                                if (firstValue == null) firstValue = value;
                                values = null;
                            }
                            if ((c = Support.scanTag(in, "option", false)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) {
                                values = new HashMap<String, String>();
                                if (c == 0) c = Support.scanValues(in, values);
                            } else if ((c = Support.scanTag(in, "/select", true)) <= 0) break;
                            while (c > -1 && c != '>') c = in.read();
                            if (c == -1) break;
                            sb.setLength(0);
			} else if (c >= 32) sb.append((char)c);
                    if (name != null) {
                        if (empty) fields.addElement(name);
                        else if (firstValue != null) fields.addElement(new Field(name, firstValue, false));
                    }
                    added = true;
		} else if ((c = Support.scanTag(in, "textarea", true)) == Support.TAG_WITH_VALUES) {
                    // textarea
                    Map<String, String> values = new HashMap<String, String>();
                    c = Support.scanValues(in, values);
                    String name = values.get("name");
                    StringBuffer sb = new StringBuffer();
                    while ((c = in.read()) != -1)
			if (c == '<') {
                            if ((c = Support.scanTag(in, "/textarea", false)) <= 0) break;
                            while (c > -1 && c != '>') c = in.read();
                            if (c == -1) break;
			} else sb.append((char)c);
                    if (name != null) {
                        if (info) {
                            out.write('\n');
                            info = false;
                        }
                        Support.writeBytes(out, "<" + modify(name) + ":" + modify(sb.toString()) + "\n", null);
                        fields.addElement(name);
                        added = true;
                    }
		} while (c > -1 && c != '>') c = in.read();
            if (c == -1) break;
            continue;
	}
	if (linkStore == null) {
            out0.flush();
            return !nbs || !valid;
	}
	if (!linkList.isEmpty()) {
            added = false;
            footNum = 1;
            Enumeration linkEnum = linkList.elements();
            while (linkEnum.hasMoreElements()) {
                Link link = (Link)linkEnum.nextElement();
                linkStore.saveLink(link);
                if (info) {
                    out0.write('\n');
                    info = false;
                }
                if (!added) Support.writeBytes(out0, ".<\n", null);
                added = true;
                Support.writeBytes(out0, "<>*" + Long.toString(link.getNumber(), Character.MAX_RADIX) + "-" + footNum + "\n", null);
                footNum++;
            }
	}
	if (!formList.isEmpty()) {
            formRefNum = 1;
            Enumeration formEnum = formList.elements();
            while (formEnum.hasMoreElements()) {
                form = (Form)formEnum.nextElement();
                linkStore.saveLink(form);
                if (info) {
                    out0.write('\n');
                    info = false;
                }
                Support.writeBytes(out0, "><-" + Long.toString(form.getNumber(), Character.MAX_RADIX) + "-" + formRefNum + "\n", null);
                formRefNum++;
            }
            if (info) out0.write('\n');
            Support.writeBytes(out0, "><\n", null);
            if (nbs) Support.writeBytes(out0, "><R" + (submit != null ? submit : "Get answer") + "\n", null);
	}
	out0.flush();
	return !nbs || !valid;
    }

    /** Extend TTML input stream to HTTP query. */
    public static Link extend(LinkStore linkStore, InputStream in, Vector<Object> fields) throws IOException, NamingException, ParseException {
	Map<String, Object> formLinks = new HashMap<String, Object>();
	String set = null, formRef = null;
	boolean multiple = false;
	int c, fragNum = -1;
	long linkNumber = -1L;
	while ((c = in.read()) != -1)
            if (c == '<') {
		if ((c = in.read()) == -1) break;
		if (c == '\n') continue;
		if (c == '>') {
                    // global hyperlink
                    if ((c = in.read()) == -1) break;
                    if (c == '\n') continue;
                    if (c != '*') {
                        // selected
                        StringBuffer sb = new StringBuffer();
                        while (c == ' ') c = in.read();
                        if (c != -1 && c != '\n')
                            do sb.append((char)c);
                            while ((c = in.read()) != -1 && c != '-' && c != '\n');
                        in.skip(in.available());
                        linkNumber = Long.parseLong(sb.toString(), Character.MAX_RADIX);
                        break;
                    } while ((c = in.read()) != -1 && c != '\n');
                    if (c == -1) break;
                    continue;
		} else if (c == '.') {
                    // name of multiple selection list
                    StringBuffer sb = new StringBuffer();
                    while ((c = in.read()) != -1 && c != '\n') sb.append((char)c);
                    if (c == -1) break;
                    set = restore(sb.toString());
                    multiple = true;
                    continue;
		} else if (c == '<') {
                    // digit entry field
                    if ((c = in.read()) == -1) break;
                    if (c == '\n') continue;
		}
                // entry field
		StringBuffer sb = new StringBuffer();
		do sb.append((char)c);
		while ((c = in.read()) != -1 && c != ':' && c != '\n');
		if (c == -1) break;
		if (c == '\n') continue;
		String name = restore(sb.toString());
		sb.setLength(0);
		while ((c = in.read()) != -1 && c == ' ' && c != '\n');
		if (c != -1 && c != '\n')
                    do sb.append((char)c);
                    while ((c = in.read()) != -1 && c != '\n');
		fields.addElement(new Field(name, restore(sb.toString()), false));
		if (c == -1) break;
            } else if (c == '>') {
		if ((c = in.read()) == -1) break;
		if (c == '\n') continue;
		if (c == '>') {
                    // local hyperlink
                    if ((c = in.read()) == -1) break;
                    if (c == '\n') continue;
                    if (c != '*') {
                        // selected
                        StringBuffer sb = new StringBuffer();
                        while (c == ' ') c = in.read();
                        if (c != -1 && c != '\n')
                            do sb.append((char)c);
                            while ((c = in.read()) != -1 && c != '\n');
                        if (c == -1) break;
                        continue;
                    }
		} else if (c == '<') {
                    // extension
                    if ((c = in.read()) == -1) break;
                    if (c == 'F') {
                        // form
                        if ((c = in.read()) == -1) break;
                        if (c == '\n') continue;
                        if (c != '*') {
                            // selected
                            StringBuffer sb = new StringBuffer();
                            do sb.append((char)c);
                            while ((c = in.read()) != -1 && c != '\n');
                            if ((formRef = sb.toString().trim()).equals("")) formRef = null;
                            continue;
                        }
                    } else if (c == '-') {
                        // form reference
                        StringBuffer sb = new StringBuffer();
                        while ((c = in.read()) != -1 && c != '-' && c != '\n') sb.append((char)c);
                        if (c == -1) break;
                        if (c == '\n') continue;
                        Long value = new Long(Long.parseLong(sb.toString(), Character.MAX_RADIX));
                        sb.setLength(0);
                        while ((c = in.read()) != -1 && c != '\n') sb.append((char)c);
                        formLinks.put(sb.toString(), value);
                        if (c == -1) break;
                        continue;
                    } else if (c == '\n') {
                        // end of message sequence
                        in.skip(in.available());
                        break;
                    }
		} else if (c == '.') {
                    // name of single selection list
                    StringBuffer sb = new StringBuffer();
                    while ((c = in.read()) != -1 && c != '\n') sb.append((char)c);
                    if (c == -1) break;
                    set = restore(sb.toString());
                    multiple = false;
                    continue;
		} while ((c = in.read()) != -1 && c != '\n');
		if (c == -1) break;
		continue;
            } else if (c == '.') {
		if ((c = in.read()) == -1) break;
		if (c == '\n') continue;
		if (c != '>' || set == null) {
                    while ((c = in.read()) != -1 && c != '\n');
                    if (c == -1) break;
                    continue;
		}
                // selection list item
		if ((c = in.read()) == -1) break;
		if (c == '\n') continue;
		if (c == '#' || c == '*') {
                    // not selected
                    while ((c = in.read()) != -1 && c != '\n');
                    if (c == -1) break;
                    continue;
		}
		StringBuffer sb = new StringBuffer();
		while (c == ' ') c = in.read();
		if (c != -1 && c != '\n')
                    do sb.append((char)c);
                    while ((c = in.read()) != -1 && c != '\n');
		fields.addElement(new Field(set, restore(sb.toString()), false));
		if (c == -1) break;
		if (!multiple) set = null;
            } else while (c != '\n') if ((c = in.read()) == -1) break;
	if (linkNumber == -1L && formRef == null) return null;
	long ctm = System.currentTimeMillis();
	if (linkNumber != -1L) return linkStore.loadLink(linkNumber, false);
	if (formRef != null) {
            Long value;
            if ((value = (Long)formLinks.get(formRef)) == null) throw new ParseException("Failed to find form link", 0);
            return linkStore.loadLink(value.longValue(), true);
	}
	return null;
    }

    public static void main(String argv[]) {
	try {
            if (argv.length > 0) {
                java.net.URLConnection uc = new URL(argv[0]).openConnection();
                uc.setUseCaches(false);
                String ct;
                filter(null, uc.getInputStream(), System.out, uc.getURL(), false,
                       (ct = uc.getContentType()) != null && Support.getParameters(ct, null).equalsIgnoreCase(Support.htmlType));
            } else {
                Vector<Object> fields = new Vector<Object>();
                extend(null, System.in, fields);
                System.out.println("fields="+fields);
            }
            System.out.flush();
	} catch (Exception ex) {
            ex.printStackTrace();
	}
    }

}
