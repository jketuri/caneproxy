
package FI.realitymodeler.cellular;

import FI.realitymodeler.common.*;
import java.io.*;
import java.util.*;

/** Class whose unnamed instances contain overloaded methods for
    encoding and decoding specific header field and parameter types. */
class Encoder {
    Hashtable<String, Parameter> parameterTable = null;
    Vector<Object> parameterVector = null;

    public void encode(OutputStream out, int code) throws IOException {
        if (code != 0) out.write(code);
    }

    /** This method should encode the header field or parameter value in
        ASCII-format to binary format writing it to output stream after writing
        given code associated with specific header field or parameter.

        @param out output stream where encoded value should be written
        @param code code identifying specific header field or parameter
        @param value value in ASCII-format */
    public void encodeUnit(OutputStream out, int code, String value) throws IOException, EncodingException {
        encode(out, code);
        HeaderEncoding.encodeValue(out, value, this.parameterTable);
    }

    /** This method can be overloaded to encode parameter value so that
        distinction between quoted and unquoted string is significant.
        Assignment-class contains flag indicating if the value was originally
        between double quotes.

        @param out output stream where encoded value should be written
        @param code code identifying specific header field or parameter
        @param assignment class containing value in ASCII-format and flag indicating if it was originally quoted
        @see FI.realitymodeler.common.Assignment */
    public void encodeUnit(OutputStream out, int code, Assignment assignment) throws IOException, EncodingException {
        encodeUnit(out, code, assignment.value);
    }

    /** This method supposes that value is individual and if needed, can be
        overloaded by a method which handles list of separate values.

        @param out output stream where encoded value should be written
        @param code code identifying specific header field or parameter
        @param value value in ASCII-format */
    public void encode(OutputStream out, int code, String value) throws IOException, EncodingException {
        encodeUnit(out, code, value);
    }

    /** If header field value contains list of separate elements, they
        should be encoded distinctly and prefixed individually with the code
        value with this method. */
    public void encodeList(OutputStream out, int code, String value) throws IOException, EncodingException {
        StreamTokenizer sTok = new StreamTokenizer(new StringReader(value));
        sTok.resetSyntax();
        sTok.wordChars(0, 255);
        sTok.ordinaryChar(',');
        sTok.quoteChar('"');
        while (sTok.nextToken() != sTok.TT_EOF) {
            String s = sTok.sval;
            if (sTok.nextToken() != ',' && sTok.ttype != sTok.TT_EOF) {
                StringBuffer sb = new StringBuffer(s);
                do if (sTok.ttype == '"') sb.append('"').append(sTok.sval).append('"');
                    else sb.append(sTok.sval);
                while (sTok.nextToken() != ',' && sTok.ttype != sTok.TT_EOF);
                encodeUnit(out, code, sb.toString().trim());
            } else encodeUnit(out, code, s.trim());
            if (sTok.ttype == sTok.TT_EOF) break;
        }
    }

    /** This method should decode the header field or parameter value in
        binary format from input stream appending it in ASCII-format to given
        string buffer.

        @param in input stream where encoded value is read
        @param sb string buffer where decoded value is appended
        @param dataLen length of data body whose headers are decoded */
    public void decodeUnit(InputStream in, StringBuffer sb, int dataLen) throws IOException, DecodingException {
        sb.append(HeaderEncoding.decodeValue(in, this.parameterVector));
    }

    /** This method can be overloaded to decode parameter value so that
        distinction between quoted and unquoted string is significant.

        @param in input stream where encoded value is read
        @param sb string buffer where decoded value is appended
        @param quoting flag indicating if quoting is significant */
    public void decodeUnit(InputStream in, StringBuffer sb, boolean quoting) throws IOException, DecodingException {
        decodeUnit(in, sb, 0);
    }

    /** This method supposes that value is individual, and if needed, can be
        overloaded by a method which handles list of separate values.

        @param in input stream where encoded value is read
        @param code code which can be checked against succeeding character when decoding list of values
        @param sb string buffer where decoded value is appended
        @param dataLen length of data body whose headers are decoded
        @return character after value */
    public int decode(InputStream in, int code, StringBuffer sb, int dataLen) throws IOException, DecodingException {
        decodeUnit(in, sb, dataLen);
        return in.read();
    }

    /** This method reads the character after binary entity and checks if
        it's same as given code, and if this is the case, decodes list of
        separate elements and adds them to string buffer. Last character should
        be returned from method.

        @param in input stream where encoded values are read
        @param code code which can be checked against succeeding character when decoding list of values
        @param sb string buffer where decoded value is appended
        @param dataLen length of data body whose headers are decoded
        @return character after value */
    public int decodeList(InputStream in, int code, StringBuffer sb, int dataLen) throws IOException, DecodingException {
        for (;;) {
            decodeUnit(in, sb, dataLen);
            int c = in.read();
            if (c != code) return c;
            sb.append(", ");
        }
    }

}

/** Class derived from Encoder-class which supposes in it's methods that
    header field or parameter values contain lists of separate elements.

    @see Encoder */
class ListEncoder extends Encoder {

    /** This method supposes that value may contain multiple separate
        elements. It calls encodeList-method from super class.

        @param out output stream where encoded value should be written
        @param code code identifying specific header field or parameter
        @param value value in ASCII-format
        @see Encoder */
    public void encode(OutputStream out, int code, String value) throws IOException, EncodingException {
        encodeList(out, code, value);
    }

    /** This method supposes that value may be list of separate elements. It
        calls decodeList-method from super class.

        @param in input stream where encoded value is read
        @param code code which is checked against succeeding character when decoding list of values
        @param sb string buffer where decoded values are appended
        @param dataLen length of data body whose headers are decoded
        @see Encoder */
    public int decode(InputStream in, int code, StringBuffer sb, int dataLen) throws IOException, DecodingException {
        return decodeList(in, code, sb, dataLen);
    }

}

/** Class which contains name of specific header field, it's associated
    encoder and decoder methods and assigned code page and code. */
class HeaderField {
    String name;
    Encoder encoder;
    int codePage;
    int code;

    HeaderField(String name) {

        this.name = name;
    }

}

/** Class which contains name of specific parameter, it's associated
    encoder and decoder methods and assigned code. */
class Parameter {
    String name;
    Encoder encoder;
    boolean obligatory;
    int code;

    Parameter(String name) {
        this.name = name;
    }

}

/** Class which implements HTTP-headers encoding and decoding in
    WAP-binary format. */
public class HeaderEncoding {
    static final int SHIFT_DELIMETER = 127;
    static final int LENGTH_QUOTE = 31;
    static final int QUOTE = 127;
    static final int BYTE_RANGE = 128;
    static final int SUFFIX_BYTE_RANGE = 129;
    static final int ABSOLUTE_TIME = 128;
    static final int RELATIVE_TIME = 129;

    static String code_out_of_range = "Code out of range";
    static String invalid_format = "Invalid format";
    static String unknown_code = "Unknown code";
    static String unexp_token = "Unexpected token";
    static HeaderField contentTypeHeaderField;
    /** Vector which translates header field codes to header fields. First index is code page and second the code. */
    static Vector<Vector<HeaderField>> headerFieldVector;
    /** Vector which translates parameter codes to strings. */
    static Vector<Object> parameterVector;
    /** Vector which translates content type codes to strings. */
    static Vector<Object> contentTypeVector;
    /** Vector which translates charset codes to strings. */
    static Vector<Object> charsetVector;
    /** Vector which translates language codes to strings. */
    static Vector<Object> languageVector;
    /** Vector which translates method codes to strings. */
    static Vector<Object> methodVector;
    /** Hashtable which maps header field names to header fields. */
    static Hashtable<String, HeaderField> headerFieldTable;
    /** Hashtable which maps parameter names to parameters. */
    static Hashtable<String, Parameter> parameterTable;
    /** Hashtable which maps content type names to codes. */
    static Hashtable<String, Parameter> contentTypeTable;
    /** Hashtable which maps charset names to codes. */
    static Hashtable<String, Parameter> charsetTable;
    /** Hashtable which maps language names to codes. */
    static Hashtable<String, Parameter> languageTable;
    /** Hashtable which maps method names to codes. */
    static Hashtable<String, Parameter> methodTable;
    /** Hashtable which contains unnamed class instances containing methods
        for encoding and decoding header fields and parameters. */
    static Hashtable<String, Encoder> encoders = new Hashtable<String, Encoder>();
    static {
        encoders.put("general", new Encoder() {
                public void encodeUnit(OutputStream out, int code, String value) {
                }

                public void decodeUnit(InputStream in, StringBuffer sb, int dataLen) throws IOException {
                }
            });
        encoders.put("ignore", new Encoder() {
                public void encodeUnit(OutputStream out, int code, String value) {
                }

                public void decodeUnit(InputStream in, StringBuffer sb, int dataLen) throws IOException {
                }
            });
        encoders.put("none", new Encoder() {
                public void encodeUnit(OutputStream out, int code, String value) throws IOException, EncodingException {
                    super.encode(out, code);
                    if (!value.equals("")) throw new EncodingException("No value allowed");
                }

                public void decodeUnit(InputStream in, StringBuffer sb, int dataLen) throws DecodingException {
                    throw new DecodingException("No value allowed");
                }
            });
        encoders.put("integer", new Encoder() {
                public void encodeUnit(OutputStream out, int code, String value) throws IOException {
                    super.encode(out, code);
                    encodeInteger(out, Long.parseLong(value));
                }

                public void decodeUnit(InputStream in, StringBuffer sb, int dataLen) throws IOException, DecodingException {
                    sb.append(decodeInteger(in));
                }
            });
        encoders.put("short_integer", new Encoder() {
                public void encodeUnit(OutputStream out, int code, String value) throws IOException, EncodingException {
                    super.encode(out, code);
                    encodeShortInteger(out, Integer.parseInt(value));
                }

                public void decodeUnit(InputStream in, StringBuffer sb, int dataLen) throws IOException, DecodingException {
                    sb.append(decodeShortInteger(in));
                }
            });
        encoders.put("long_integer", new Encoder() {
                public void encodeUnit(OutputStream out, int code, String value) throws IOException {
                    super.encode(out, code);
                    encodeLongInteger(out, Long.parseLong(value));
                }

                public void decodeUnit(InputStream in, StringBuffer sb, int dataLen) throws IOException, DecodingException {
                    sb.append(decodeLongInteger(in));
                }
            });
        encoders.put("variable_length_integer", new Encoder() {
                public void encodeUnit(OutputStream out, int code, String value) throws IOException {
                    super.encode(out, code);
                    encodeUIntVar(out, Long.parseLong(value));
                }

                public void decodeUnit(InputStream in, StringBuffer sb, int dataLen) throws IOException {
                    sb.append(decodeUIntVar(in));
                }
            });
        encoders.put("date", new Encoder() {
                public void encodeUnit(OutputStream out, int code, String value) throws IOException {
                    super.encode(out, code);
                    encodeDate(out, value);
                }

                public void decodeUnit(InputStream in, StringBuffer sb, int dataLen) throws IOException, DecodingException {
                    sb.append(Support.dateFormat.format(decodeDate(in)));
                }
            });
        encoders.put("string", new Encoder() {
                public void encodeUnit(OutputStream out, int code, String value) throws IOException {
                    super.encode(out, code);
                    encodeString(out, value);
                }

                public void encodeUnit(OutputStream out, int code, Assignment assignment) throws IOException {
                    super.encode(out, code);
                    encodeString(out, assignment.value, assignment.quoted, true);
                }

                public void decodeUnit(InputStream in, StringBuffer sb, int dataLen) throws IOException {
                    sb.append(decodeString(in));
                }

                public void decodeUnit(InputStream in, StringBuffer sb, boolean quoting) throws IOException {
                    sb.append(decodeString(in, quoting));
                }
            });
        encoders.put("comment_string", new Encoder() {
                public void encodeUnit(OutputStream out, int code, String value) throws IOException {
                    super.encode(out, code);
                    encodeString(out, Support.uncommentString(value));
                }

                public void decodeUnit(InputStream in, StringBuffer sb, int dataLen) throws IOException {
                    sb.append(decodeString(in));
                }
            });
        encoders.put("quoted_string", new Encoder() {
                public void encodeUnit(OutputStream out, int code, String value) throws IOException {
                    super.encode(out, code);
                    encodeString(out, Support.unquoteString(value));
                }

                public void decodeUnit(InputStream in, StringBuffer sb, int dataLen) throws IOException {
                    sb.append('"').append(decodeString(in)).append('"');
                }
            });
        encoders.put("authenticate", new Encoder() {
                public void encodeUnit(OutputStream out, int code, String value) throws IOException, EncodingException {
                    ByteArrayOutputStream bout = new ByteArrayOutputStream();
                    Map<String, String> args = new HashMap<String, String>();
                    Vector<Assignment> vector = new Vector<Assignment>();
                    String realm, scheme = Support.getArguments(value, args, vector);
                    if (scheme == null || vector.isEmpty() ||
                        (realm = args.get("realm")) == null)
                        throw new EncodingException("Invalid challenge " + value);
                    encodeValue(bout, scheme, this.parameterTable);
                    encodeString(bout, realm);
                    for (int i = 0; i < vector.size(); i++)
                        if ((vector.elementAt(i)).name.equalsIgnoreCase("realm")) {
                            vector.removeElementAt(i);
                            break;
                        }
                    encodeParameters(bout, vector);
                    super.encode(out, code);
                    encodeValueLength(out, bout.size());
                    bout.writeTo(out);
                }

                public void decodeUnit(InputStream in, StringBuffer sb, int dataLen) throws IOException, DecodingException {
                    LimitInputStream limitIn = new LimitInputStream(in, decodeValueLength(in));
                    sb.append(decodeValue(limitIn, this.parameterVector));
                    sb.append(" realm=\"").append(decodeString(limitIn)).append('"');
                    decodeParameters(limitIn, sb, ", ");
                }
            });
        encoders.put("authorization", new Encoder() {
                public void encodeUnit(OutputStream out, int code, String value) throws IOException, EncodingException {
                    ByteArrayOutputStream bout = new ByteArrayOutputStream();
                    StringTokenizer st = new StringTokenizer(value);
                    if (!st.hasMoreTokens()) throw new EncodingException("Invalid authorization " + value);
                    String scheme = st.nextToken();
                    encodeValue(bout, scheme, this.parameterTable);
                    if (st.hasMoreTokens()) {
                        Vector<Assignment> vector = new Vector<Assignment>();
                        String credentials = Support.getArguments(st.nextToken("").substring(1), null, vector);
                        if (scheme.equalsIgnoreCase("basic")) {
                            StringTokenizer credSt = new StringTokenizer(new String(new BASE64Decoder().decodeStream(credentials)), ":");
                            if (credSt.hasMoreTokens()) {
                                encodeString(bout, credSt.nextToken());
                                if (credSt.hasMoreTokens()) encodeString(bout, credSt.nextToken());
                                else bout.write(0);
                            } else {
                                bout.write(0);
                                bout.write(0);
                            }
                        } else {
                            encodeString(bout, credentials);
                            encodeParameters(bout, vector);
                        }
                    }
                    super.encode(out, code);
                    encodeValueLength(out, bout.size());
                    bout.writeTo(out);
                }

                public void decodeUnit(InputStream in, StringBuffer sb, int dataLen) throws IOException, DecodingException {
                    LimitInputStream limitIn = new LimitInputStream(in, decodeValueLength(in));
                    Object obj = decodeStart(limitIn, this.parameterVector, false);
                    String scheme = obj instanceof Parameter ? ((Parameter)obj).name : (String)obj;
                    sb.append(scheme);
                    if (limitIn.getRemaining() > 0) {
                        sb.append(' ');
                        if (scheme.equalsIgnoreCase("basic")) sb.append(new BASE64Encoder().encode((decodeString(limitIn) + ":" + decodeString(limitIn)).getBytes()));
                        else decodeParameters(limitIn, sb, ", ");
                    }
                }
            });
        encoders.put("content_types", new ListEncoder() {
                public void encodeUnit(OutputStream out, int code, String value) throws IOException, EncodingException {
                    super.encode(out, code);
                    encodeValueWithParameters(out, value, contentTypeTable);
                }

                public void decodeUnit(InputStream in, StringBuffer sb, int dataLen) throws IOException, DecodingException {
                    sb.append(decodeValueWithParameters(in, contentTypeVector));
                }
            });
        encoders.put("accept_charset", new ListEncoder() {
                public void encodeUnit(OutputStream out, int code, String value) throws IOException, EncodingException {
                    super.encode(out, code);
                    encodeValueWithQFactor(out, value, charsetTable);
                }

                public void decodeUnit(InputStream in, StringBuffer sb, int dataLen) throws IOException, DecodingException {
                    sb.append(decodeValueWithQFactor(in, charsetVector));
                }
            });
        encoders.put("language", new ListEncoder() {
                public void encodeUnit(OutputStream out, int code, String value) throws IOException, EncodingException {
                    super.encode(out, code);
                    encodeValueWithQFactor(out, value, languageTable);
                }

                public void decodeUnit(InputStream in, StringBuffer sb, int dataLen) throws IOException, DecodingException {
                    sb.append(decodeValueWithQFactor(in, languageVector));
                }
            });
        encoders.put("methods", new ListEncoder() {
                public void encodeUnit(OutputStream out, int code, String value) throws IOException {
                    super.encode(out, code);
                    encodeValue(out, value, methodTable);
                }

                public void decodeUnit(InputStream in, StringBuffer sb, int dataLen) throws IOException, DecodingException {
                    sb.append(decodeValue(in, methodVector));
                }
            });
        encoders.put("ranges", new ListEncoder());
        encoders.put("cache_control", new ListEncoder() {
                public void encodeUnit(OutputStream out, int code, String value) throws IOException, EncodingException {
                    super.encode(out, code);
                    encodeSingleParameter(out, value, this.parameterTable, false);
                }

                public void decodeUnit(InputStream in, StringBuffer sb, int dataLen) throws IOException, DecodingException {
                    decodeSingleParameter(in, sb, this.parameterVector, false);
                }
            });
        encoders.put("connection", new ListEncoder());
        encoders.put("content_encoding", new ListEncoder());
        encoders.put("md5", new Encoder() {
                public void encodeUnit(OutputStream out, int code, String value) throws IOException {
                    super.encode(out, code);
                    byte digest[] = new BASE64Decoder().decodeStream(value);
                    encodeValueLength(out, digest.length);
                    out.write(digest);
                }

                public void decodeUnit(InputStream in, StringBuffer sb, int dataLen) throws IOException, DecodingException {
                    byte digestBytes[] = new byte[decodeValueLength(in)];
                    in.read(digestBytes);
                    sb.append(new BASE64Encoder().encode(digestBytes));
                }
            });
        encoders.put("content_range", new Encoder() {
                public void encodeUnit(OutputStream out, int code, String value) throws IOException, EncodingException {
                    super.encode(out, code);
                    StringTokenizer st = new StringTokenizer(value, "\t -/", true);
                    if (!st.hasMoreTokens() || !st.nextToken().toLowerCase().equals("bytes") ||
                        !st.hasMoreTokens() || !st.nextToken().trim().equals("") || !st.hasMoreTokens())
                        throw new EncodingException("Invalid content range");
                    long firstBytePos = Long.parseLong(st.nextToken());
                    if (!st.hasMoreTokens() || !st.nextToken().trim().equals("-") || !st.hasMoreTokens())
                        throw new EncodingException("Invalid content range, dash missing");
                    long lastBytePos = Long.parseLong(st.nextToken());
                    if (!st.hasMoreTokens() || !st.nextToken().trim().equals("/") || !st.hasMoreTokens())
                        throw new EncodingException("Invalid content range, slash missing");
                    long entityLength = Long.parseLong(st.nextToken());
                    ByteArrayOutputStream bout = new ByteArrayOutputStream();
                    encodeUIntVar(bout, firstBytePos);
                    encodeUIntVar(bout, entityLength);
                    encodeValueLength(out, bout.size());
                    bout.writeTo(out);
                }

                public void decodeUnit(InputStream in, StringBuffer sb, int dataLen) throws IOException, DecodingException {
                    if (dataLen == -1) throw new DecodingException("Data body length is not specified");
                    decodeValueLength(in);
                    long firstBytePos = decodeUIntVar(in),
                        entityLength = decodeUIntVar(in);
                    sb.append("bytes ").append(firstBytePos).append('-').append(firstBytePos + dataLen - 1).append('/').append(entityLength);
                }
            });
        encoders.put("content_type", new Encoder() {
                public void encodeUnit(OutputStream out, int code, String value) throws IOException, EncodingException {
                    super.encode(out, code);
                    encodeValueWithParameters(out, value, contentTypeTable);
                }

                public void decodeUnit(InputStream in, StringBuffer sb, int dataLen) throws IOException, DecodingException {
                    sb.append(decodeValueWithParameters(in, contentTypeVector));
                }
            });
        encoders.put("if_range", new Encoder() {
                public void encodeUnit(OutputStream out, int code, String value) throws IOException {
                    super.encode(out, code);
                    value = value.trim();
                    if (value.startsWith("W/") || value.startsWith("w/") || value.startsWith("\"")) {
                        encodeString(out, value);
                        return;
                    }
                    encodeDate(out, value);
                }

                public void decodeUnit(InputStream in, StringBuffer sb, int dataLen) throws IOException, DecodingException {
                    int c = in.read();
                    if (c == -1) throw new EOFException();
                    if (c < 31) sb.append(decodeDate(in, c));
                    else sb.append(decodeString(in, c, false));
                }
            });
        encoders.put("pragma", new ListEncoder() {
                public void encodeUnit(OutputStream out, int code, String value) throws IOException, EncodingException {
                    super.encode(out, code);
                    encodeSingleParameter(out, value, this.parameterTable, true);
                }

                public void decodeUnit(InputStream in, StringBuffer sb, int dataLen) throws IOException, DecodingException {
                    decodeSingleParameter(in, sb, this.parameterVector, true);
                }
            });
        encoders.put("range", new ListEncoder() {
                public void encodeUnit(OutputStream out, int code, String value) throws IOException, EncodingException {
                    StringTokenizer st = new StringTokenizer(value, "=-", true);
                    if (!st.hasMoreTokens() || !st.nextToken().equalsIgnoreCase("bytes") ||
                        !st.hasMoreTokens() || !st.nextToken().equals("=")) throw new EncodingException("Invalid range field " + value);
                    ByteArrayOutputStream bout = new ByteArrayOutputStream();
                    while (st.hasMoreTokens()) {
                        String s = st.nextToken();
                        if (s.equals("-")) {
                            if (!st.hasMoreTokens()) throw new EncodingException("Invalid range field " + value);
                            bout.write(SUFFIX_BYTE_RANGE);
                            encodeUIntVar(bout, Long.parseLong(st.nextToken()));
                        } else {
                            if (!st.hasMoreTokens() || !st.nextToken().equals("-")) throw new EncodingException("Invalid range field " + value);
                            bout.write(BYTE_RANGE);
                            encodeUIntVar(bout, Long.parseLong(s));
                            if (st.hasMoreTokens()) encodeUIntVar(bout, Long.parseLong(st.nextToken()));
                        }
                    }
                    if (bout.size() > 0) {
                        super.encode(out, code);
                        encodeValueLength(out, bout.size());
                        bout.writeTo(out);
                    }
                }

                public void decodeUnit(InputStream in, StringBuffer sb, int dataLen) throws IOException, DecodingException {
                    LimitInputStream limitIn = new LimitInputStream(in, decodeValueLength(in));
                    int c = limitIn.read();
                    if (c == -1) throw new EOFException();
                    sb.append("bytes=");
                    if (c == BYTE_RANGE) {
                        sb.append(decodeUIntVar(limitIn)).append('-');
                        if (limitIn.getRemaining() > 0) sb.append(decodeUIntVar(limitIn));
                    } else if (c == SUFFIX_BYTE_RANGE) sb.append('-').append(decodeUIntVar(limitIn));
                    else throw new DecodingException(unknown_code);
                }
            });
        encoders.put("retry_after", new Encoder() {
                public void encodeUnit(OutputStream out, int code, String value) throws IOException {
                    super.encode(out, code);
                    ByteArrayOutputStream bout = new ByteArrayOutputStream();
                    try {
                        int deltaSeconds = Integer.parseInt(value);
                        bout.write(RELATIVE_TIME);
                        encodeInteger(bout, deltaSeconds);
                    } catch (NumberFormatException ex) {
                        bout.write(ABSOLUTE_TIME);
                        encodeDate(bout, value);
                    }
                    encodeValueLength(out, bout.size());
                    bout.writeTo(out);
                }

                public void decodeUnit(InputStream in, StringBuffer sb, int dataLen) throws IOException, DecodingException {
                    decodeValueLength(in);
                    int c = in.read();
                    if (c == -1) throw new EOFException();
                    if (c == RELATIVE_TIME) sb.append(decodeInteger(in));
                    else if (c == ABSOLUTE_TIME) sb.append(decodeDate(in));
                    else throw new DecodingException(unknown_code);
                }
            });
        encoders.put("transfer_encoding", new ListEncoder());
        encoders.put("warning", new ListEncoder() {
                public void encodeUnit(OutputStream out, int code, String value) throws IOException, EncodingException {
                    StringTokenizer st = new StringTokenizer(value);
                    if (!st.hasMoreTokens()) return;
                    int warningCode = Integer.parseInt(st.nextToken());
                    super.encode(out, code);
                    if (!st.hasMoreTokens()) {
                        encodeShortInteger(out, warningCode);
                        return;
                    }
                    ByteArrayOutputStream bout = new ByteArrayOutputStream();
                    encodeShortInteger(bout, warningCode);
                    encodeString(bout, st.nextToken());
                    if (st.hasMoreTokens()) {
                        String text = st.nextToken("").substring(1).trim();
                        encodeString(bout, Support.unquoteString(text), text.startsWith("\""), true);
                    } else bout.write(0);
                    encodeValueLength(out, bout.size());
                    bout.writeTo(out);
                }

                public void decodeUnit(InputStream in, StringBuffer sb, int dataLen) throws IOException, DecodingException {
                    int c = in.read();
                    if (c == -1) throw new EOFException();
                    if ((c & 0x80) == 0) {
                        LimitInputStream limitIn = new LimitInputStream(in, decodeValueLength(in, c));
                        sb.append(decodeShortInteger(limitIn));
                        while (limitIn.getRemaining() > 0) sb.append(' ').append(decodeString(limitIn, true));
                    } else sb.append(decodeShortInteger(in, c));
                }
            });
        encoders.put("content_disposition", new Encoder() {
                public void encodeUnit(OutputStream out, int code, String value) throws IOException, EncodingException {
                    Vector<Assignment> vector = new Vector<Assignment>();
                    String type = Support.getParameters(value, null, true, vector);
                    if (type == null) throw new EncodingException("Invalid content-disposition " + value);
                    Parameter parameter = this.parameterTable.get(type);
                    if (parameter == null) throw new EncodingException("Invalid content-disposition name " + type);
                    ByteArrayOutputStream bout = new ByteArrayOutputStream();
                    encodeInteger(bout, parameter.code);
                    encodeParameters(bout, vector);
                    super.encode(out, code);
                    encodeValueLength(out, bout.size());
                    bout.writeTo(out);
                }

                public void decodeUnit(InputStream in, StringBuffer sb, int dataLen) throws IOException, DecodingException {
                    sb.append(decodeValueWithParameters(in, this.parameterVector));
                }
            });
        encoders.put("charset", new ListEncoder() {
                public void encodeUnit(OutputStream out, int code, String value) throws IOException, EncodingException {
                    super.encode(out, code);
                    encodeValue(out, value, charsetTable);
                }

                public void decodeUnit(InputStream in, StringBuffer sb, int dataLen) throws IOException, DecodingException {
                    sb.append(decodeValue(in, charsetVector));
                }
            });
        encoders.put("q_value", new Encoder() {
                public void encodeUnit(OutputStream out, int code, String value) throws IOException, EncodingException {
                    int qValue = getQValue(value);
                    if (qValue == -1) return;
                    super.encode(out, code);
                    encodeUIntVar(out, qValue);
                }

                public void decodeUnit(InputStream in, StringBuffer sb, int dataLen) throws IOException, DecodingException {
                    sb.append(decodeQValue(in));
                }
            });
        encoders.put("untyped_value", new Encoder() {
                public void encodeUnit(OutputStream out, int code, String value) throws IOException, EncodingException {
                    super.encode(out, code);
                    encodeUntypedValue(out, value, false, false);
                }

                public void encodeUnit(OutputStream out, int code, Assignment assignment) throws IOException, EncodingException {
                    super.encode(out, code);
                    encodeUntypedValue(out, assignment.value, assignment.quoted, true);
                }

                public void decodeUnit(InputStream in, StringBuffer sb, int dataLen) throws IOException, DecodingException {
                    sb.append(decodeUntypedValue(in, false));
                }

                public void decodeUnit(InputStream in, StringBuffer sb, boolean quoting) throws IOException, DecodingException {
                    sb.append(decodeUntypedValue(in, quoting));
                }
            });
        encoders.put("version", new Encoder() {
                public void encodeUnit(OutputStream out, int code, String value) throws IOException, EncodingException {
                    super.encode(out, code);
                    encodeVersion(out, value);
                }

                public void decodeUnit(InputStream in, StringBuffer sb, int dataLen) throws IOException, DecodingException {
                    sb.append(decodeVersion(in));
                }
            });
        encoders.put("field_names", new Encoder() {
                public void encodeUnit(OutputStream out, int code, String value) throws IOException, EncodingException {
                    StringTokenizer st = new StringTokenizer(Support.unquoteSimpleString(value), ",");
                    if (!st.hasMoreTokens()) return;
                    super.encode(out, code);
                    while (st.hasMoreTokens()) encodeFieldName(out, st.nextToken().trim());
                }

                public void decodeUnit(InputStream in, StringBuffer sb, int dataLen) throws IOException, DecodingException {
                    if (!(in instanceof LimitInputStream)) throw new DecodingException("Encoder called illegally");
                    LimitInputStream limitIn = (LimitInputStream)in;
                    sb.append('"');
                    if (limitIn.getRemaining() > 0)
                        for (;;) {
                            sb.append(decodeFieldName(limitIn));
                            if (limitIn.getRemaining() <= 0) break;
                            sb.append(',');
                        }
                    sb.append('"');
                }
            });
        encoders.put("field_name", new Encoder() {
                public void encodeUnit(OutputStream out, int code, String value) throws IOException, EncodingException {
                    super.encode(out, code);
                    encodeFieldName(out, value.trim());
                }

                public void decodeUnit(InputStream in, StringBuffer sb, int dataLen) throws IOException, DecodingException {
                    sb.append(decodeFieldName(in));
                }
            });
    }

    /** Parses integer number of format NNN (decimal) or 0xHHH (hexadecimal).
        @param s number in ASCII-format
        @return number in binary format */
    public static int parseNumber(String s) {
        s = s.trim();
        if ((s = s.toLowerCase()).startsWith("0x")) return Integer.parseInt(s.substring(2), 16);
        return Integer.parseInt(s);
    }

    /** Initializes this class using parameter files in specified directory.
        Comments can be included in parameter files preceding them with hash
        character (#). Strings can be put between double quotation marks (")
        where characters itself can be quoted with backslash character (\). All
        number values can be expressed either in decimal notation or in hex
        notation in the format 0xHH. In general various codes can have multiple
        names but name which is listed last is used when decoding headers. Names
        must be written in format which is wanted in decoded headers. Free
        combination of upper- and lower case letters are allowed. When encoding
        headers case of letters is ignored. Parameter files are the
        following:<br>

        <a name="header_field.table">
        <i>header_field.table</i></a> which is of the following format:<br>

        <table>
        <caption>Header field</caption>
        <tr><th>&lt;header name&gt;</th><th>=</th><th>&lt;code page number&gt;</th><th>&lt;code&gt;</th><th>&lt;value type&gt;</th></tr>

        </table>

        .<br>
        .<br>
        .<br>

        <p>

        &lt;header name&gt; is standard MIME-header name in ASCII-format e.g.
        <i>Accept</i> or <i>Content-Type</i>.<p>

        &lt;code page number&gt; is header code page number from 1 to 256.<p>

        &lt;code&gt; is code number from 0 to 127.<p>

        &lt;value type&gt; is header value type name and can be one of the
        following (Refer to 8.4.2 in [WAP] for corresponding definitions):<p>

        <i>ignore</i> specifies that header value is ignored. Code page must be
        set to 0.<p>

        <i>integer</i> specifies that header has integer value which is encoded
        in short integer or long integer format depending on the magnitude. It
        can be WAP-types Short-integer or Long-integer.<p>

        <i>short_integer</i> specifies that header has short integer value
        (under 128) which is encoded as byte value whose high order bit is set.
        It is same as WAP-type Short-integer.<p>

        <i>long_integer</i> specifies that header has long integer value (0x0 -
        0xffffffff) which is encoded as length of integer in one byte followed
        with actual bytes in high order bit first format.  It is same as
        WAP-type Long-integer.<p>

        <i>variable_length_integer</i> specifies that header has variable length
        integer value (0x0 - 0xffffffff) which is encoded as byte pieces whose
        high order bit signifies that number continues. It is same as WAP-type
        Uintvar-integer.<p>

        <i>date</i> specifies that header has date value which is encoded as
        long integer with number of seconds from time offset.  It is same as
        WAP-type Date-Value.<p>

        <i>string</i> specifies that header has string value which is encoded as
        byte string of characters followed by zero and preceded by 127 if string
        starts with a character greater than or equal to 127 or less than 32.
        It is same as WAP-type Text-string. When this type is used with <a
        href="#parameter.table">parameter definitions</a> encoding can also
        start with double quote character (") which indicates that string is
        enclosed in double quotes. In this case this can be WAP-types
        Text-string or Quoted-string.<p>

        <i>comment_string</i> specifies that header has string as value which
        can include also comments enclosed in parentheses as e.g. 'Server',
        'User-Agent' and 'Via'-field values as defined in [RFC2068]. In
        encodings comments are stripped away.<p>

        <i>quoted_string</i> specifies that header has quoted string as value.
        It's decoded with quotes and encoded always with quotes. It is same as
        WAP-type Quoted-string.<p>

        <i>content_types</i> specifies that header has content types (with
        possible parameters) in comma separated list as value like in
        'Accept-Header'-field value as defined in [RFC2068] and in 8.4.2.7 in
        [WAP]. Content types are specified in file

        <a href="#content_type.table"><i>content_type.table</i></a>.<p>
        Parameter names are specified in file

        <a href="#parameter.table"><i>parameter.table</i></a>.<p>

        <i>charset</i> specifies that header has character set string value with
        possible quality factor, like iso-8559-1 or iso-8559-1; q=0.5. Refer to
        8.4.2.8 in [WAP]. Character sets are specified in parameter file

        <a href="#charset.table"><i>charset.table</i></a>.<p>

        <i>language</i> specifies that header has language name string value
        with possible quality factor, like en or en; q=0.5. Refer to 8.4.2.10 in
        [WAP]. Languages are specified in parameter file

        <a href="#language.table"><i>language.table</i></a>.<p>

        <i>methods</i> specifies that header has method name list as value, like
        'GET, POST, PUT' as in 'Allow'-field defined in [RFC2068]. Refer to
        8.4.2.13 in [WAP].  Method names are specified in file

        <a href="#method.table"><i>method.table</i></a>.<p>

        <i>ranges</i> specifies that header has ranges name string value. It can
        be <i>none</i> or <i>bytes</i>.  It is like in 'Accept-ranges'-field
        defined in [RFC2068]. Refer to 8.4.2.11 in [WAP].<p>

        <i>cache_control</i> specifies that header has cache control string
        value. Refer to 8.4.2.15 in [WAP]. It can be

        <i>no-cache</i>, <i>no-store</i>, <i>max-age</i>, <i>max-stale</i>,
        <i>max-fresh</i>, <i>only-if-cached</i>, <i>public</i>, <i>private</i>,
        <i>no-transform</i>, <i>must-revalidate</i> or
        <i>proxy-revalidate</i>.<p>

        <i>connection</i> specifies that header has connection string value.
        Refer to 8.4.2.16 in [WAP]. It can be <i>close</i>.<p>

        <i>content_encoding</i> specifies that header has content encoding
        string value.  Refer to 8.4.2.18 in [WAP]. It can be <i>gzip</i>,
        <i>compress</i> or <i>deflate</i>.<p>

        <i>md5</i> specifies that header has md5 digest value. It is base64
        encoded byte sequence.  Refer to 8.4.2.22 in [WAP].<p>

        <i>content_range</i> specifies that header has 'Content-Range'-field
        value list as defined in [RFC2068].  Refer to 8.4.2.23 in [WAP].<p>

        <i>content_type</i> specifies that header has mime content type value
        with possible parameters.  Refer to 8.4.2.24 in [WAP]. Content types are
        specified in file

        <a href="#content_type.table"><i>content_type.table</i></a>.<p>

        Parameter names are specified in file

        <a href="#parameter.table"><i>parameter.table</i></a>.<p>

        <i>if_range</i> specifies that header has 'If-Range'-field value as
        defined in [RFC2068].  Rerfer to 8.4.2.33 in [WAP].<p>

        <i>pragma</i> specifies that header has pragma string value. Refer to
        8.4.2.38 in [WAP]. It can be <i>no-cache</i>.<p>

        <i>range</i> specifies that header has 'Range'-field header value as
        defined in [RFC2068].  Refer to 8.4.2.42 in [WAP].<p>

        <i>retry_after</i> specifies that header has 'Retry-After'-field value
        as defined in [RFC2068].  Refer to 8.4.2.44 in [WAP].<p>

        <i>transfer_encoding</i> specifies that header has transfer encoding
        string value. Refer to 8.4.2.46 in [WAP]. It can be <i>chunked</i>.<p>

        <i>warning</i> specifies that header has 'Warning'-field value as
        defined in [RFC2068].  Refer to 8.4.2.51 in [WAP].<p>

        <i>authenticate</i> specifies that header has 'WWW-Authenticate'-field
        value as defined in [RFC2068].  Refer to 8.4.2.52 in [WAP].<p>

        <i>authorization</i> specifies that header has 'Authorization'-field
        value as defined in [RFC2068].  Refer to 8.4.2.14 in [WAP].<p>

        <p>

        <a name="content_type.table">
        <i>content_type.table</i></a> which is of the following format:<p>

        <table>
        <caption>Content type</caption>
        <tr><th>&lt;content type&gt;</th><th>=</th><th>&lt;code&gt;</th></tr>

        </table>

        .<br>
        .<br>
        .<br>

        <p>

        &lt;content type&gt; is standard MIME-content type in ASCII-format e.g.
        <i>text/html</i>.<p>

        &lt;code&gt; is code number from 0 to 127.<p>

        <p>

        <a name="charset.table">
        <i>charset.table</i></a> which is of the following format:<p>

        <table>
        <caption>Charset</caption>
        <tr><th>&lt;charset name&gt;</th><th>=</th><th>&lt;code&gt;</th></tr>

        </table>

        .<br>
        .<br>
        .<br>

        <p>

        &lt;charset name&gt; is standard character set name e.g.
        <i>iso-8859-1</i>.<p>

        &lt;code&gt; is code number which can be also two byte value but whose
        least significant byte must differentiate it from others.<p>

        <p>

        <a name="language.table">
        <i>language.table</i></a> which is of the following format:<p>

        <table>
        <caption>Language</caption>
        <tr><th>&lt;language name&gt;</th><th>=</th><th>&lt;code&gt;</th></tr>

        </table>

        .<br>
        .<br>
        .<br>

        <p>

        &lt;language name&gt; is standard language name e.g. <i>English</i> or <i>en</i>.<p>

        &lt;code&gt; is code number from 0 to 255.<p>

        <p>

        <a name="method.table">
        <i>method.table</i></a> which is of the following format:<p>

        <table>
        <caption>Method</caption>
        <tr><th>&lt;method name&gt;</th><th>=</th><th>&lt;code&gt;</th></tr>

        </table>

        .<br>
        .<br>
        .<br>

        <p>

        &lt;method name&gt; is standard HTTP-method name e.g. <i>GET</i> or
        <i>PUT</i>.<p>

        &lt;code&gt; is code number from 0 to 255.<p>

        <p>

        <a name="parameter.table">
        <i>parameter.table</i></a> which is of the following format:<p>

        <table>
        <caption>Parameter</caption>
        <tr><th>&lt;parameter name&gt;</th><th>=</th><th>&lt;code&gt;</th><th>&lt;value type&gt;</th><th>&lt;condition&gt;</th><th>&lt;context&gt;</th></tr>

        </table>

        .<br>
        .<br>
        .<br>

        <p>

        &lt;parameter name&gt; is standard header value parameter name e.g.
        <i>q</i> or <i>charset</i>.<p>

        &lt;code&gt; is code number from 0 to 127.<p>

        &lt;condition&gt; specifies whether parameter has <i>obligatory</i> or
        <i>optional</i> value.

        &lt;context&gt; is type of value where this parameter can be. If context
        is <i>general</i> this parameter can be in any value type. Value types
        are listed below.

        &lt;value type&gt; is parameter value type name and can be one of the
        following:<p>

        <i>q_value</i> specifies that parameter value is quality factor value.
        This is same as WAP-type Q-value defined in 8.4.2.3 of [WAP].<p>

        <i>charset</i> is same type as in

        <a href="#header_field.table">header field table.</a>

        <i>version</i> specifes that parameter value is version value. This is
        same as WAP-type Version-value defined in 8.4.2.3 of [WAP].<p>

        <i>integer</i> is same type as in

        <a href="#header_field.table">header field table.</a><p>

        <i>string</i> is same type as in

        <a href="#header_field.table">header field table.</a><p>

        <i>short_integer</i> is same type as in

        <a href="#header_field.table">header field table.</a><p>

        <i>untyped_value</i> is integer or string depending of format.<p>

        <i>field_name</i> is name of header.<p>

        <i>field_names</i> is list of header names separated with comma and
        enclosed in double quotation marks.

        <i>none</i> specifies that parameter has no value.<p>

        <p>

        @param parameterDir directory where parameter files reside or null in which case they are searched from Java resources from sub package 'resources' of the current package of this class
        @exception java.io.IOException if reading error encountered
        @exception InitializationException if parameter file is not found or it contains error */
    public static void initialize(String parameterDir) throws IOException, InitializationException {
        String path = null;
        StreamTokenizer sTok = null;
        try {
            headerFieldVector = new Vector<Vector<HeaderField>>();
            headerFieldVector.setSize(16);
            headerFieldTable = new Hashtable<String, HeaderField>();
            String resourcePath = "FI/realitymodeler/cellular/resources/";
            Reader reader = new BufferedReader(parameterDir != null ? new FileReader(new File(parameterDir, path = "header_field.table")) :
                                               new InputStreamReader(ClassLoader.getSystemResourceAsStream(path = resourcePath + "header_field.table")));
            sTok = new StreamTokenizer(reader);
            sTok.resetSyntax();
            sTok.whitespaceChars(0, 32);
            sTok.wordChars(33, 255);
            sTok.ordinaryChar('=');
            sTok.commentChar('#');
            sTok.quoteChar('"');
            while (sTok.nextToken() != sTok.TT_EOF) {
                if (sTok.ttype != sTok.TT_WORD && sTok.ttype != '"') throw new InitializationException(unexp_token);
                String name = sTok.sval;
                if (sTok.nextToken() != '=') throw new InitializationException(unexp_token);
                HeaderField headerField = new HeaderField(name);
                if (sTok.nextToken() != sTok.TT_WORD) throw new InitializationException(unexp_token);
                headerField.codePage = parseNumber(sTok.sval);
                if (sTok.nextToken() != sTok.TT_WORD) throw new InitializationException(unexp_token);
                headerField.code = parseNumber(sTok.sval);
                if (headerField.codePage > 0) {
                    Vector<HeaderField> vector = headerFieldVector.elementAt(headerField.codePage - 1);
                    if (vector == null) headerFieldVector.setElementAt(vector = new Vector<HeaderField>(), headerField.codePage - 1);
                    if (vector.size() <= headerField.code) vector.setSize(headerField.code + 1);
                    vector.setElementAt(headerField, headerField.code);
                }
                if (sTok.nextToken() != sTok.TT_WORD && sTok.ttype != '"') throw new InitializationException(unexp_token);
                headerField.encoder = encoders.get(sTok.sval.toLowerCase());
                if (headerField.encoder == null) throw new InitializationException("No encoding type " + sTok.sval + " found");
                headerFieldTable.put(name.toLowerCase(), headerField);
            }
            contentTypeHeaderField = headerFieldTable.get("content-type");
            reader = new BufferedReader(parameterDir != null ? new FileReader(new File(parameterDir, path = "parameter.table")) :
                                        new InputStreamReader(ClassLoader.getSystemResourceAsStream(path = resourcePath + "parameter.table")));
            sTok = new StreamTokenizer(reader);
            sTok.resetSyntax();
            sTok.whitespaceChars(0, 32);
            sTok.wordChars(33, 255);
            sTok.ordinaryChar('=');
            sTok.commentChar('#');
            sTok.quoteChar('"');
            while (sTok.nextToken() != sTok.TT_EOF) {
                if (sTok.ttype != sTok.TT_WORD && sTok.ttype != '"') throw new InitializationException(unexp_token);
                String name = sTok.sval;
                if (sTok.nextToken() != '=') throw new InitializationException(unexp_token);
                Parameter parameter = new Parameter(name);
                if (sTok.nextToken() != sTok.TT_WORD) throw new InitializationException(unexp_token);
                parameter.code = parseNumber(sTok.sval);
                if (sTok.nextToken() != sTok.TT_WORD && sTok.ttype != '"') throw new InitializationException(unexp_token);
                parameter.encoder = encoders.get(sTok.sval.toLowerCase());
                if (parameter.encoder == null) throw new InitializationException("No encoding type " + sTok.sval + " found");
                if (sTok.nextToken() != sTok.TT_WORD) throw new InitializationException(unexp_token);
                if (!(parameter.obligatory = sTok.sval.equalsIgnoreCase("obligatory")) && !sTok.sval.equalsIgnoreCase("optional"))
                    throw new InitializationException("Invalid condition (must be obligatory or optional)");
                if (sTok.nextToken() != sTok.TT_WORD) throw new InitializationException(unexp_token);
                Encoder encoder = encoders.get(sTok.sval.toLowerCase());
                if (encoder == null) throw new InitializationException("No context encoding type " + sTok.sval + " found");
                if (encoder.parameterTable == null) encoder.parameterTable = new Hashtable<String, Parameter>();
                encoder.parameterTable.put(name.toLowerCase(), parameter);
                if (encoder.parameterVector == null) encoder.parameterVector = new Vector<Object>();
                if (encoder.parameterVector.size() <= parameter.code) encoder.parameterVector.setSize(parameter.code + 1);
                encoder.parameterVector.setElementAt(parameter, parameter.code);
            }
            Encoder encoder = encoders.get("general");
            parameterTable = encoder.parameterTable;
            parameterVector = encoder.parameterVector;
            contentTypeVector = new Vector<Object>();
            contentTypeTable = new Hashtable<String, Parameter>();
            reader = new BufferedReader(parameterDir != null ? new FileReader(new File(parameterDir, path = "content_type.table")) :
                                        new InputStreamReader(ClassLoader.getSystemResourceAsStream(path = resourcePath + "content_type.table")));
            sTok = new StreamTokenizer(reader);
            sTok.resetSyntax();
            sTok.whitespaceChars(0, 32);
            sTok.wordChars(33, 255);
            sTok.ordinaryChar('=');
            sTok.commentChar('#');
            sTok.quoteChar('"');
            while (sTok.nextToken() != sTok.TT_EOF) {
                if (sTok.ttype != sTok.TT_WORD && sTok.ttype != '"') throw new InitializationException(unexp_token);
                String name = sTok.sval;
                if (sTok.nextToken() != '=') throw new InitializationException(unexp_token);
                if (sTok.nextToken() != sTok.TT_WORD) throw new InitializationException(unexp_token);
                Parameter parameter = new Parameter(name);
                parameter.code = parseNumber(sTok.sval);
                contentTypeTable.put(name.toLowerCase(), parameter);
                if (contentTypeVector.size() <= parameter.code) contentTypeVector.setSize(parameter.code + 1);
                contentTypeVector.setElementAt(name, parameter.code);
            }
            charsetVector = new Vector<Object>();
            charsetTable = new Hashtable<String, Parameter>();
            reader = new BufferedReader(parameterDir != null ? new FileReader(new File(parameterDir, path = "charset.table")) :
                                        new InputStreamReader(ClassLoader.getSystemResourceAsStream(path = resourcePath + "charset.table")));
            sTok = new StreamTokenizer(reader);
            sTok.resetSyntax();
            sTok.whitespaceChars(0, 32);
            sTok.wordChars(33, 255);
            sTok.ordinaryChar('=');
            sTok.commentChar('#');
            sTok.quoteChar('"');
            while (sTok.nextToken() != sTok.TT_EOF) {
                if (sTok.ttype != sTok.TT_WORD && sTok.ttype != '"') throw new InitializationException(unexp_token);
                String name = sTok.sval;
                if (sTok.nextToken() != '=') throw new InitializationException(unexp_token);
                if (sTok.nextToken() != sTok.TT_WORD) throw new InitializationException(unexp_token);
                Parameter parameter = new Parameter(name);
                parameter.code = parseNumber(sTok.sval);
                charsetTable.put(name.toLowerCase(), parameter);
                if (charsetVector.size() <= (parameter.code & 0xff)) charsetVector.setSize((parameter.code & 0xff) + 1);
                charsetVector.setElementAt(name, parameter.code & 0xff);
            }
            languageVector = new Vector<Object>();
            languageTable = new Hashtable<String, Parameter>();
            reader = new BufferedReader(parameterDir != null ? new FileReader(new File(parameterDir, path = "language.table")) :
                                        new InputStreamReader(ClassLoader.getSystemResourceAsStream(path = resourcePath + "language.table")));
            sTok = new StreamTokenizer(reader);
            sTok.resetSyntax();
            sTok.whitespaceChars(0, 32);
            sTok.wordChars(33, 255);
            sTok.ordinaryChar('=');
            sTok.commentChar('#');
            sTok.quoteChar('"');
            while (sTok.nextToken() != sTok.TT_EOF) {
                if (sTok.ttype != sTok.TT_WORD && sTok.ttype != '"') throw new InitializationException(unexp_token);
                String name = sTok.sval;
                if (sTok.nextToken() != '=') throw new InitializationException(unexp_token);
                if (sTok.nextToken() != sTok.TT_WORD) throw new InitializationException(unexp_token);
                Parameter parameter = new Parameter(name);
                parameter.code = parseNumber(sTok.sval);
                languageTable.put(name.toLowerCase(), parameter);
                if (languageVector.size() <= parameter.code) languageVector.setSize(parameter.code + 1);
                languageVector.setElementAt(name, parameter.code);
            }
            methodVector = new Vector<Object>();
            methodTable = new Hashtable<String, Parameter>();
            reader = new BufferedReader(parameterDir != null ? new FileReader(new File(parameterDir, path = "method.table")) :
                                        new InputStreamReader(ClassLoader.getSystemResourceAsStream(path = resourcePath + "method.table")));
            sTok = new StreamTokenizer(reader);
            sTok.resetSyntax();
            sTok.whitespaceChars(0, 32);
            sTok.wordChars(33, 255);
            sTok.ordinaryChar('=');
            sTok.commentChar('#');
            sTok.quoteChar('"');
            while (sTok.nextToken() != sTok.TT_EOF) {
                if (sTok.ttype != sTok.TT_WORD && sTok.ttype != '"') throw new InitializationException(unexp_token);
                String name = sTok.sval;
                if (sTok.nextToken() != '=') throw new InitializationException(unexp_token);
                if (sTok.nextToken() != sTok.TT_WORD) throw new InitializationException(unexp_token);
                Parameter parameter = new Parameter(name);
                parameter.code = parseNumber(sTok.sval);
                methodTable.put(name.toLowerCase(), parameter);
                if (methodVector.size() <= parameter.code) methodVector.setSize(parameter.code + 1);
                methodVector.setElementAt(name, parameter.code);
            }
        } catch (InitializationException ex) {
            if (path != null && sTok != null) throw new InitializationException("Error in file " + path + " in line " +
                                                                                sTok.lineno() + "\n" + Support.stackTrace(ex));
        }
    }

    /** Encodes value to variable length unsigned integer in output stream.

        @param out output stream where variable length integer is written
        @param value integer value to be encoded */
    public static void encodeUIntVar(OutputStream out, long value) throws IOException {
        if (value == 0L) {
            out.write(0);
            return;
        }
        byte bytes[] = new byte[5];
        int i = 0;
        do bytes[i++] = (byte)(value & 0x7f);
        while ((value >>= 7) != 0L);
        while (i > 1) out.write(bytes[--i] | 0x80);
        out.write(bytes[0]);
    }

    /** Encodes value to long integer in output stream.

        @param out output stream where long integer is written
        @param value integer value to be encoded */
    public static void encodeLongInteger(OutputStream out, long value) throws IOException {
        if (value == 0L) {
            out.write(1);
            out.write(0);
            return;
        }
        byte bytes[] = new byte[10];
        int i = 0;
        do bytes[i++] = (byte)(value & 0xff);
        while ((value >>= 8) != 0L);
        out.write(i);
        do out.write(bytes[--i]);
        while (i > 0);
    }

    /** Encodes value to one of integer formats (short integer or long
        integer) depending on it's magnitude.

        @param out output stream where integer is written
        @param value integer value to be encoded */
    public static void encodeInteger(OutputStream out, long value) throws IOException {
        if (value < 128L) {
            out.write((int)value | 0x80);
            return;
        }
        encodeLongInteger(out, value);
    }

    /** Encodes value to short integer and checks for overflow.

        @param out output stream where integer is written
        @param value integer value to be encoded */
    public static void encodeShortInteger(OutputStream out, int value) throws IOException, EncodingException {
        if (value > 127) throw new EncodingException("Overflow");
        out.write(value | 0x80);
    }

    /** Encodes value length to output stream.

        @param out output stream where value length is written
        @param length value length to be encoded */
    public static void encodeValueLength(OutputStream out, int length) throws IOException {
        if (length < 31) {
            out.write(length);
            return;
        }
        out.write(LENGTH_QUOTE);
        encodeUIntVar(out, (long)length);
    }

    /** Encodes date value to output stream.

        @param out output stream where date value is written
        @param value date value in standard HTTP-ASCII-format */
    public static void encodeDate(OutputStream out, String value) throws IOException {
        Date date = Support.parse(value);
        encodeLongInteger(out, date != null ? (int)(date.getTime() / 1000L) : 0);
    }

    /** Encodes string to output stream.

        @param out output stream where string is written.
        @param value string value to be encoded
        @param quoted specifies that string was originally quoted
        @param quoting indicates that quoting is significant */
    public static void encodeString(OutputStream out, String value, boolean quoted, boolean quoting) throws IOException {
        if (quoted) out.write('"');
        else if (value.length() > 0 && (value.charAt(0) < 32 || value.charAt(0) >= 127 ||
                                        quoting &&  value.charAt(0) == '"')) out.write(QUOTE);
        Support.writeBytes(out, value, null);
        out.write(0);
    }

    public static void encodeString(OutputStream out, String value) throws IOException {
        encodeString(out, value, false, false);
    }

    public static void encodeFieldName(OutputStream out, String fieldName) throws IOException, EncodingException {
        HeaderField headerField = headerFieldTable.get(fieldName.toLowerCase());
        if (headerField != null) {
            encodeInteger(out, headerField.code);
            return;
        }
        encodeString(out, fieldName);
    }

    /** Gets decimal part of quality factor and checks if quality factor is
        1.0 or invalid.

        @param q quality factor in ASCII-format (e.g 0.5)
        @return decimal part of quality factor. If quality factor is found to be 1.0 -1 is returned. */
    public static int getQValue(String q) throws EncodingException {
        StringTokenizer st = new StringTokenizer(q, ".");
        if (!st.hasMoreTokens()) throw new EncodingException("Invalid quality factor value " + q);
        int wholePart = Integer.parseInt(st.nextToken()), qValue;
        if (st.hasMoreTokens()) {
            String s = st.nextToken().trim();
            int i = s.length() - 1;
            for (; i >= 0 && s.charAt(i) == '0'; i--);
            int n;
            if (i < 2) {
                qValue = 1;
                n = i == 0 ? 10 : 1;
            } else if (i == 2) {
                qValue = 100;
                n = 1;
            } else throw new EncodingException("Bad quality factor " + q);
            for (; i >= 0; i--) {
                qValue += Character.digit(s.charAt(i), 10) * n;
                n *= 10;
            }
        } else qValue = 0;
        if (wholePart < 0 || wholePart > 1 || wholePart == 1 && qValue != 0 || qValue < 0 || qValue > 1099)
            throw new EncodingException("Invalid quality factor " + q);
        if (wholePart == 1) return -1;
        return qValue;
    }

    /** Encodes version number to output stream.

        @param out output stream where version number value is written
        @param version version number in ASCII-format (e.g 1.2) */
    public static void encodeVersion(OutputStream out, String version) throws IOException {
        StringTokenizer st = new StringTokenizer(version, ".");
        int majorNumber = Integer.parseInt(st.nextToken());
        if (majorNumber < 1 || majorNumber > 7) {
            encodeString(out, version);
            return;
        }
        int minorNumber;
        if (st.hasMoreTokens()) {
            minorNumber = Integer.parseInt(st.nextToken());
            if (minorNumber < 0 || minorNumber > 14) {
                encodeString(out, version);
                return;
            }
        } else minorNumber = 15;
        out.write(majorNumber << 4 | minorNumber | 0x80);
    }

    /** Encodes value as code or string using codes in codeTable.

        @param out output stream where value is written
        @param value name value
        @param parameterTable table containing code values of keys
        @return true if code was found */
    public static boolean encodeValue(OutputStream out, String value, Hashtable<String, Parameter> parameterTable)
        throws IOException {
        String name = Support.getParameters(value, null);
        Parameter parameter = parameterTable.get(name.toLowerCase());
        if (parameter != null) {
            encodeInteger(out, parameter.code);
            return true;
        }
        encodeString(out, name);
        return false;
    }

    public static void encodeUntypedValue(OutputStream out, String value, boolean quoted, boolean quoting) throws IOException {
        if (!quoted)
            try {
                encodeInteger(out, Long.parseLong(value));
                return;
            } catch (NumberFormatException ex) {}
        encodeString(out, value, quoted, quoting);
    }

    public static void encodeSingleParameter(OutputStream out, String value, Hashtable<String, Parameter> parameterTable, boolean secondary)
        throws IOException, EncodingException {
        StringTokenizer st = new StringTokenizer(value, "=");
        String name = st.nextToken().trim(), key = name.toLowerCase();
        Parameter parameter = parameterTable.get(key);
        if (secondary && parameter == null) parameter = HeaderEncoding.parameterTable.get(key);
        else secondary = false;
        value = null;
        if (st.hasMoreTokens()) value = st.nextToken("").substring(1).trim();
        else if (parameter == null) {
            encodeString(out, name);
            return;
        } else if (parameter.code < 128 && !secondary) {
            encodeShortInteger(out, parameter.code);
            return;
        }
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        if (value != null) {
            Assignment assignment = new Assignment(name, Support.unquoteString(value), value.startsWith("\""));
            if (parameter == null) {
                encodeString(bout, assignment.name);
                encodeUntypedValue(bout, assignment.value, assignment.quoted, true);
            } else if (parameter.obligatory && assignment.value.equals(""))
                throw new EncodingException("Obligatory value of parameter " + name + " is missing");
            else parameter.encoder.encodeUnit(bout, parameter.code | 0x80, assignment);
        } else if (parameter == null) encodeString(bout, name);
        else if (parameter.obligatory) throw new EncodingException("Obligatory value of parameter " + name + " is missing");
        else encodeInteger(bout, parameter.code);
        encodeValueLength(out, bout.size());
        bout.writeTo(out);
    }

    /** Encodes value as code with possible quality factor value in compact or general format.

        @param out output stream where value is written
        @param value value string
        @param parameterTable table containing code values of keys */
    public static void encodeValueWithQFactor(OutputStream out, String value, Hashtable<String, Parameter> parameterTable)
        throws IOException, EncodingException {
        Map<String, String> params = new HashMap<String, String>();
        String name = Support.getParameters(value, params);
        Parameter parameter = parameterTable.get(name.toLowerCase());
        String q = params.get("q");
        int qValue = q != null ? getQValue(q) : -1;
        if (qValue == -1)
            if (parameter == null) {
                encodeString(out, name);
                return;
            } else if (parameter.code < 128) {
                encodeShortInteger(out, parameter.code);
                return;
            }
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        if (parameter != null) encodeInteger(bout, parameter.code);
        else encodeString(bout, name);
        if (qValue != -1) encodeUIntVar(bout, qValue);
        encodeValueLength(out, bout.size());
        bout.writeTo(out);
    }

    /** Encodes parameters. */
    public static void encodeParameters(OutputStream out, Vector vector)
        throws IOException, EncodingException {
        Enumeration vectorElements = vector.elements();
        while (vectorElements.hasMoreElements()) {
            Assignment assignment = (Assignment)vectorElements.nextElement();
            Parameter parameter = parameterTable.get(assignment.name);
            if (parameter != null) {
                if (parameter.obligatory && assignment.value.equals(""))
                    throw new EncodingException("Obligatory value of parameter " + assignment.name + " is missing");
                parameter.encoder.encodeUnit(out, parameter.code | 0x80, assignment);
                continue;
            }
            if (assignment.name.equals("")) continue;
            encodeString(out, assignment.name);
            encodeUntypedValue(out, assignment.value, assignment.quoted, true);
        }
    }

    /** Encodes value with parameters in compact or general format.

        @param out output stream where encoded value with possible parameters are written
        @param value string value
        @param parameterTable table containing code values of keys
        @throws IOException if writing error encountered
        @exception EncodingException if encoding error encountered */
    public static void encodeValueWithParameters(OutputStream out, String value, Hashtable<String, Parameter> parameterTable)
        throws IOException, EncodingException {
        Vector<Assignment> vector = new Vector<Assignment>();
        String name = Support.getParameters(value, null, false, vector).toLowerCase();
        Parameter parameter = parameterTable.get(name);
        if (vector.isEmpty())
            if (parameter == null) {
                encodeString(out, name);
                return;
            } else if (parameter.code < 128) {
                encodeShortInteger(out, parameter.code);
                return;
            }
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        if (parameter != null) encodeInteger(bout, parameter.code);
        else encodeString(bout, name);
        encodeParameters(bout, vector);
        encodeValueLength(out, bout.size());
        bout.writeTo(out);
    }

    public static long decodeUIntVar(InputStream in, int c) throws IOException {
        long value = c & 0x7f;
        while ((c & 0x80) != 0) {
            if ((c = in.read()) == -1) throw new EOFException();
            value = (value << 7) | (c & 0x7f);
        }
        return value;
    }

    /** Decodes variable length unsigned integer from input stream.

        @param in input stream where variable length integer is read from
        @return integer in binary format
        @exception java.io.EOFException if unexpected end of file is encountered */
    public static long decodeUIntVar(InputStream in) throws IOException {
        int c = in.read();
        if (c == -1) throw new EOFException();
        return decodeUIntVar(in, c);
    }

    public static long decodeLongInteger(InputStream in, int n) throws IOException, DecodingException {
        long value = 0L;
        if (n == 0) throw new DecodingException("Zero length long integer");
        while (--n >= 0) {
            int c = in.read();
            if (c == -1) throw new EOFException();
            value <<= 8;
            value |= c;
        }
        return value;
    }

    /** Decodes long integer integer from input stream.

        @param in input stream where long integer is read from
        @return integer in binary format
        @exception java.io.EOFException if unexpected end of file is encountered */
    public static long decodeLongInteger(InputStream in) throws IOException, DecodingException {
        int n = in.read();
        if (n == -1) throw new EOFException();
        return decodeLongInteger(in, n);
    }

    public static long decodeInteger(InputStream in, int c) throws IOException, DecodingException {
        if ((c & 0x80) != 0) return c & 0x7f;
        return decodeLongInteger(in, c);
    }

    /** Decodes integer in short or long format from input stream.

        @param in input stream where integer is read from
        @return integer in binary format
        @exception java.io.EOFException if unexpected end of file is encountered */
    public static long decodeInteger(InputStream in) throws IOException, DecodingException {
        int c = in.read();
        if (c == -1) throw new EOFException();
        return decodeInteger(in, c);
    }

    public static int decodeShortInteger(InputStream in, int c) throws IOException, DecodingException {
        if ((c & 0x80) == 0) throw new DecodingException(invalid_format);
        return c & 0x7f;
    }

    /** Decodes integer in short format from input stream.

        @param in input stream where integer is read from
        @return integer in binary format
        @exception java.io.EOFException if unexpected end of file is encountered */
    public static int decodeShortInteger(InputStream in) throws IOException, DecodingException {
        int c = in.read();
        if (c == -1) throw new EOFException();
        return decodeShortInteger(in, c);
    }

    public static int decodeValueLength(InputStream in, int c) throws IOException {
        if (c != LENGTH_QUOTE) return c;
        return (int)decodeUIntVar(in);
    }

    /** Decodes value length from input stream.

        @param in input stream where integer is read from
        @return value length in binary format
        @exception java.io.EOFException if unexpected end of file is encountered */
    public static int decodeValueLength(InputStream in) throws IOException {
        int c = in.read();
        if (c == -1) throw new EOFException();
        return decodeValueLength(in, c);
    }

    public static Date decodeDate(InputStream in, int c) throws IOException, DecodingException {
        long value = decodeLongInteger(in, c);
        return new Date(value * 1000L);
    }

    /** Decodes date from input stream

        @param in input stream where integer is read from
        @return value date in it's own format
        @exception java.io.EOFException if unexpected end of file is encountered */
    public static Date decodeDate(InputStream in) throws IOException, DecodingException {
        int c = in.read();
        if (c == -1) throw new EOFException();
        return decodeDate(in, c);
    }

    public static String decodeString(InputStream in, int c, boolean quoting) throws IOException {
        boolean mustQuote;
        if (quoting && c == '"') {
            mustQuote = true;
            if ((c = in.read()) == -1) throw new EOFException();
        } else {
            mustQuote = false;
            if (c == QUOTE && (c = in.read()) == -1) throw new EOFException();
        }
        StringBuffer sb = new StringBuffer();
        if (quoting)
            for (;c > 0; c = in.read()) {
                if (c == '"' || c == '\\') {
                    mustQuote = true;
                    sb.append('\\');
                }
                sb.append((char)c);
            } else for (;c > 0; c = in.read()) sb.append((char)c);
        if (c == -1) throw new EOFException();
        if (mustQuote) {
            sb.insert(0, '"');
            sb.append('"');
        }
        return sb.toString();
    }

    /** Decodes string from input stream.

        @param in input stream where string is read from
        @param quoting indicates that quoting is significant
        @return result string
        @exception java.io.EOFException if unexpected end of file is encountered */
    public static String decodeString(InputStream in, boolean quoting) throws IOException {
        int c = in.read();
        if (c == -1) throw new EOFException();
        return decodeString(in, c, quoting);
    }

    public static String decodeString(InputStream in) throws IOException {
        return decodeString(in, false);
    }

    /** Decodes quality factor from input stream.

        @param in input stream where quality factor is read from
        @return quality factor in ASCII-format (e.g 0.5)
        @exception java.io.EOFException if unexpected end of file is encountered */
    public static String decodeQValue(InputStream in) throws IOException, DecodingException {
        long qValue = decodeUIntVar(in);
        if (qValue <= 1L || qValue > 1099L) throw new DecodingException("Invalid quality factor");
        StringBuffer sb;
        if (qValue > 100L) {
            sb = new StringBuffer(String.valueOf(qValue - 100L));
            while (sb.length() < 3) sb.insert(0, '0');
        } else sb = new StringBuffer(String.valueOf(qValue - 1L));
        while (sb.charAt(sb.length() - 1) == '0') sb.setLength(sb.length() - 1);
        sb.insert(0, "0.");
        return sb.toString();
    }

    /** Decodes version value from input stream.

        @param in input stream where version value is read from
        @return quality factor in ASCII-format (e.g 0.5)
        @exception java.io.EOFException if unexpected end of file is encountered */
    public static String decodeVersion(InputStream in) throws IOException {
        int c = in.read();
        if (c == -1) throw new EOFException();
        if ((c & 0x80) == 0) return decodeString(in, c, false);
        int majorNumber = c >> 4 & 0x7, minorNumber = c & 0xf;
        return minorNumber != 15 ? majorNumber + "." + minorNumber : String.valueOf(majorNumber);
    }

    public static Object decodeStart(InputStream in, Vector<Object> parameterVector, boolean maybeValueLength, int c) throws IOException, DecodingException {
        if (c != 0)
            if ((c & 0x80) != 0 || !maybeValueLength && c < 31) {
                int index = (int)decodeInteger(in, c);
                if (parameterVector == charsetVector) index &= 0xff;
                if (index < 0 || index >= parameterVector.size()) throw new DecodingException(code_out_of_range);
                if (parameterVector.elementAt(index) == null) throw new DecodingException(unknown_code);
                return parameterVector.elementAt(index);
            } else if (maybeValueLength) {
                int length = -1;
                if (c < LENGTH_QUOTE) length = c;
                else if (c == LENGTH_QUOTE) length = (int)decodeUIntVar(in);
                if (length != -1) return new LimitInputStream(in, length);
            }
        return decodeString(in, c, false);
    }

    /** Decodes name encoded as code or string, or value length if value was
        encoded in general format, returning a limit input stream where limit is
        set to length of the value.

        @param in input stream where element is read from
        @param parameterVector vector containing names associated with a code
        @param maybeValueLength element maybe also value length
        @return name string or if maybeValueLength was specified, limit input stream with value length set as limit
        @exception java.io.EOFException if unexpected end of file is encountered
        @see FI.realitymodeler.common.LimitInputStream */
    public static Object decodeStart(InputStream in, Vector<Object> parameterVector, boolean maybeValueLength) throws IOException, DecodingException {
        int c = in.read();
        if (c == -1) throw new EOFException();
        return decodeStart(in, parameterVector, maybeValueLength, c);
    }

    /** Decodes name with possible quality factor.

        @param in input stream where element is read from
        @param parameterVector vector containing names associated with a code
        @return value string
        @exception java.io.EOFException if unexpected end of file is encountered */
    public static String decodeValueWithQFactor(InputStream in, Vector<Object> parameterVector) throws IOException, DecodingException {
        Object obj = decodeStart(in, parameterVector, true);
        if (obj instanceof Parameter) return ((Parameter)obj).name;
        if (obj instanceof String) return (String)obj;
        LimitInputStream limitIn = (LimitInputStream)obj;
        obj = decodeStart(limitIn, parameterVector, false);
        StringBuffer sb = new StringBuffer();
        sb.append(obj instanceof Parameter ? ((Parameter)obj).name : obj);
        if (limitIn.getRemaining() > 0) sb.append("; q=").append(decodeQValue(limitIn));
        return sb.toString();
    }

    public static String decodeValue(InputStream in, Vector<Object> parameterVector) throws IOException, DecodingException {
        Object obj = decodeStart(in, parameterVector, false);
        return obj instanceof Parameter ? ((Parameter)obj).name : (String)obj;
    }

    public static void decodeSingleParameter(InputStream in, StringBuffer sb, Vector<Object> parameterVector, boolean secondary) throws IOException, DecodingException {
        Object obj = decodeStart(in, parameterVector, true);
        if (obj instanceof Parameter) {
            Parameter parameter = (Parameter)obj;
            if (parameter.obligatory) throw new DecodingException("Obligatory value of parameter " + parameter.name + " is missing");
            sb.append(parameter.name);
            return;
        }
        if (obj instanceof String) {
            sb.append(obj);
            return;
        }
        LimitInputStream limitIn = (LimitInputStream)obj;
        obj = decodeStart(limitIn, secondary ? HeaderEncoding.parameterVector : parameterVector, false);
        if (obj instanceof Parameter) {
            Parameter parameter = (Parameter)obj;
            sb.append(parameter.name);
            if (limitIn.getRemaining() > 0) {
                sb.append('=');
                parameter.encoder.decodeUnit(limitIn, sb, true);
            } else if (parameter.obligatory) throw new DecodingException("Obligatory value of parameter " + parameter.name + " is missing");
            return;
        }
        sb.append(obj);
        if (limitIn.getRemaining() > 0) sb.append('=').append(decodeUntypedValue(limitIn, true));
    }

    public static String decodeUntypedValue(InputStream in, boolean quoting) throws IOException, DecodingException {
        int c = in.read();
        if (c == -1) throw new EOFException();
        if (c < 32 || c >= 127) return String.valueOf(decodeInteger(in, c));
        return decodeString(in, c, quoting);
    }

    /** Decodes parameters. */
    public static void decodeParameters(InputStream in, StringBuffer sb, String punct) throws IOException, DecodingException {
        int c;
        while ((c = in.read()) != -1) {
            sb.append(punct);
            Object obj = decodeStart(in, parameterVector, false, c);
            if (obj instanceof Parameter) {
                Parameter parameter = (Parameter)obj;
                sb.append(parameter.name).append('=');
                parameter.encoder.decodeUnit(in, sb, true);
                continue;
            }
            sb.append(obj);
            String value = decodeUntypedValue(in, true);
            if (value.equals("")) continue;
            sb.append('=').append(value);
        }
    }

    /** Decodes name with possible parameters.

        @param in input stream where element is read from
        @param parameterVector vector containing names associated with a code
        @return value string
        @exception java.io.EOFException if unexpected end of file is encountered */
    public static String decodeValueWithParameters(InputStream in, Vector<Object> parameterVector) throws IOException, DecodingException {
        Object obj = decodeStart(in, parameterVector, true);
        if (obj instanceof Parameter) return ((Parameter)obj).name;
        if (obj instanceof String) return (String)obj;
        LimitInputStream limitIn = (LimitInputStream)obj;
        obj = decodeStart(limitIn, parameterVector, false);
        StringBuffer sb = new StringBuffer();
        sb.append(obj instanceof Parameter ? ((Parameter)obj).name : obj);
        decodeParameters(limitIn, sb, "; ");
        return sb.toString();
    }

    public static String decodeFieldName(InputStream in) throws IOException, DecodingException {
        Vector<HeaderField> vector = headerFieldVector.firstElement();
        int c = in.read();
        if (c == -1) throw new EOFException();
        if (c >= 32 && c < 127) return decodeString(in, c, false);
        int code = (int)decodeInteger(in, c);
        if (code < 0 || code >= vector.size() || vector.elementAt(code) == null) throw new DecodingException(unknown_code);
        return vector.elementAt(code).name;
    }

    /** Encodes HTTP-headers to output stream.

        @param headerList message header to be encoded
        @param out output stream where encoded headers is written
        @param onlyValueOfContentType specifies that only value of content type header field is written. If no content type is found, zero is written.
        @exception java.io.IOException if reading or writing error encountered
        @exception EncodingException if encoding error encountered
        @see FI.realitymodeler.common.HeaderList */
    public static void encode(HeaderList headerList, OutputStream out, boolean onlyValueOfContentType) throws IOException, EncodingException {
        String contentTypeValue = null;
        Vector<Object> vector = new Vector<Object>();
        Iterator headerItems = headerList.iterator();
        while (headerItems.hasNext()) {
            Header header = (Header)headerItems.next();
            String name = header.getName().trim(), value = header.getValue().trim();
            if (value.equals("")) continue;
            HeaderField headerField = headerFieldTable.get(name.toLowerCase());
            if (headerField != null) {
                if (headerField == contentTypeHeaderField) {
                    contentTypeValue = value;
                    continue;
                }
                int index;
                for (index = vector.size() - 1; index >= 0; index--) {
                    Object obj = vector.elementAt(index);
                    if (obj instanceof Object[] &&
                        ((HeaderField)((Object[])obj)[0]).codePage <= headerField.codePage &&
                        ((HeaderField)((Object[])obj)[0]).code <= headerField.code) break;
                }
                vector.insertElementAt(new Object[] {headerField, value}, index + 1);
            } else vector.addElement(header);
        }
        if (onlyValueOfContentType)
            if (contentTypeValue != null) contentTypeHeaderField.encoder.encode(out, 0, contentTypeValue);
            else out.write(0);
        else if (contentTypeValue != null) contentTypeHeaderField.encoder.encode(out, contentTypeHeaderField.code | 0x80, contentTypeValue);
        int codePage = 1;
        Enumeration vectorElements = vector.elements();
        while (vectorElements.hasMoreElements()) {
            Object obj = vectorElements.nextElement();
            if (obj instanceof Object[]) {
                HeaderField headerField = (HeaderField)((Object[])obj)[0];
                if (headerField.codePage == 0) continue;
                if (headerField.codePage != codePage) {
                    if (headerField.codePage > 31) out.write(SHIFT_DELIMETER);
                    out.write(codePage = headerField.codePage);
                }
                headerField.encoder.encode(out, headerField.code | 0x80, (String)((Object[])obj)[1]);
                continue;
            }
            encodeString(out, ((Header)obj).getName());
            encodeString(out, ((Header)obj).getValue());
        }
    }

    /** Encodes HTTP-headers to output stream.

        @param headerList message header to be encoded
        @param out output stream where encoded header is written
        @exception java.io.IOException if reading or writing error encountered
        @exception EncodingException if encoding error encountered
        @see FI.realitymodeler.common.HeaderList */
    public static void encode(HeaderList headerList, OutputStream out) throws IOException, EncodingException {
        encode(headerList, out, false);
    }

    /** Decodes HTTP-headers from input stream.

        @param in input stream where headers are read from
        @param dataLen length of data body whose headers are decoded
        @param onlyValueOfContentType specifies that only value of content type header is encoded
        @return decoded headers
        @exception java.io.IOException if reading error encountered
        @exception java.io.EOFException if unexpected end of file is encountered
        @exception DecodingException if decoding exception encountered
        @see FI.realitymodeler.common.HeaderList */
    public static HeaderList decode(InputStream in, int dataLen, boolean onlyValueOfContentType) throws IOException, DecodingException {
        StringBuffer sb = new StringBuffer();
        HeaderList headerList = new HeaderList();
        int c = onlyValueOfContentType ? contentTypeHeaderField.code | 0x80 : in.read(), codePageIndex = 0;
        while (c != -1) {
            if ((c & 0x80) != 0) {
                int codeIndex = c & 0x7f;
                if (codePageIndex > 15) throw new DecodingException("Code page out of range");
                Vector<HeaderField> vector = headerFieldVector.elementAt(codePageIndex);
                if (vector == null || codeIndex < 0 || codeIndex >= vector.size()) throw new DecodingException(code_out_of_range);
                HeaderField headerField = vector.elementAt(codeIndex);
                if (headerField == null) throw new DecodingException(unknown_code);
                c = headerField.encoder.decode(in, c, sb, dataLen);
                headerList.append(new Header(headerField.name, sb.toString()));
                sb.setLength(0);
                continue;
            }
            if (c <= 31) codePageIndex = c - 1;
            else if (c == SHIFT_DELIMETER) {
                if ((c = in.read()) == -1) throw new EOFException();
                codePageIndex = c - 1;
            } else headerList.append(new Header(decodeString(in, c, false), decodeString(in)));
            c = in.read();
        }
        return headerList;
    }

    /** Decodes HTTP-headers from input stream.

        @param in input stream where headers are read from
        @exception java.io.IOException if reading error encountered
        @exception java.io.EOFException if unexpected end of file is encountered
        @exception DecodingException if decoding exception encountered
        @see FI.realitymodeler.common.HeaderList */
    public static HeaderList decode(InputStream in, int dataLen) throws IOException, DecodingException {
        return decode(in, dataLen, false);
    }

}
