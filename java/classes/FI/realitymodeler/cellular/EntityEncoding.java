
package FI.realitymodeler.cellular;

import FI.realitymodeler.common.*;

import java.io.*;
import java.text.*;
import java.util.*;

/** Class which implements HTTP-entity encoding and decoding in
    WAP-format and multipart entity construction.
    EntityEncoding entityEncoding = new EntityEncoding();
    try {
    ByteArrayOutputStream bout = new ByteArrayOutputStream();
    entityEncoding.encode(response.getHeaderList(), response.getInputStream(), bout, false);
    response.setHeaders(bout.toByteArray());
    entityEncoding.encode(response.getOutputStream());
    return;
    } finally {
    entityEncoding.reset();
    }
    EntityEncoding entityEncoding = new EntityEncoding();
    try {
    ByteArrayOutputStream bout = new ByteArrayOutputStream();
    HeaderList headerList = entityEncoding.decode(new ByteArrayInputStream(request.getHeaders()), request.getContentLength());
    entityEncoding.decode(request.getInputStream(), headerList);
    request.setHeaderList(headerList);
    entityEncoding.decode(request.getOutputStream());
    return;
    } finally {
    entityEncoding.reset();
    }
*/
public class EntityEncoding extends HeaderEncoding {
    static boolean useFiles = false;

    EntityMemory entity = useFiles ? new EntityCache() : new EntityMemory();
    String boundary = "\r\n--_";
    String encoding = "8859_1";
    boolean multipart = false;

    /** Initializes this class to use cache files in the specified location.

        @param parameterDir directory where parameter files reside or null in which case they are search from Java resources from sub package 'resources' of the current package of this class
        @param cacheDir directory where message entities are stored temporarily during encoding and decoding or null if memory is used
        @param clean specifies that cache directory is cleaned (all files are removed) after e.g. system crash
        @exception java.io.IOException if reading error encountered
        @exception InitializationException if parameter file is not found or it contains error */
    public static void initialize(String parameterDir, String cacheDir, boolean clean) throws IOException, InitializationException {
        HeaderEncoding.initialize(parameterDir);
        if (useFiles = cacheDir != null) EntityCache.initialize(cacheDir, clean);
    }

    /** Encodes HTTP-entity from input stream and writes headers part to output stream.<br>
        Headers part is encoded as follows:<br>
        Headers Encoded headers with Content-Type first (without code if onlyValueOfContentType-flag is set)<br>

        @param headerList entity headers to be encoded or null if it should be read from input stream
        @param in input stream where entity is read from or null when only entity headers are encoded
        @param out output stream where encoded headers are written or null when entity is included in multipart entity to be written afterwards
        @param onlyValueOfContentType specifies that only value of content type is written
        @return header list which was given in parameter or read from input stream
        @exception java.io.IOException if reading or writing error encountered
        @exception EncodingException if encoding error encountered
        @see FI.realitymodeler.common.HeaderList */
    public synchronized HeaderList encode(HeaderList headerList, InputStream in, OutputStream out, boolean onlyValueOfContentType) throws IOException, EncodingException, ParseException {
        if (headerList != null && in == null) {
            encode(headerList, out, onlyValueOfContentType);
            return headerList;
        }
        byte b[] = new byte[Support.bufferLength];
        if (in != null) {
            if (headerList == null) headerList = new HeaderList(in);
            HeaderList modifiedHeaderList = new HeaderList();
            Decoder decoder = Support.getHeader(headerList, modifiedHeaderList);
            Support.decodeBody(in, null, null, headerList, modifiedHeaderList, decoder, encoding, null, entity, false, false, false, null);
        }
        if (out == null) return headerList;
        String subType = "mixed";
        if (headerList != null) {
            String ct = headerList.getHeaderValue("content-type");
            if ((ct == null ||
                 !(ct = Support.getParameters(ct, null)).toLowerCase().startsWith("multipart/") &&
                 !ct.equals("message/rfc822")) && getNumberOfParts() == 1) {
                encode(headerList, out, onlyValueOfContentType);
                return headerList;
            }
            if (ct != null) {
                String s = ct.substring(ct.indexOf('/') + 1);
                if (contentTypeTable.containsKey("application/vnd.wap.multipart/" + s)) subType = s;
            }
        } else headerList = new HeaderList();
        headerList.replace(new Header("Content-Type", "application/vnd.wap.multipart/" + subType));
        encode(headerList, out, onlyValueOfContentType);
        multipart = true;
        return headerList;
    }

    /** Encodes HTTP-entity from input stream and writes headers part to output stream.<br>

        @param in input stream where entity is read from or null
        @param out output stream where encoded headers are written
        @param onlyValueOfContentType specifies that only value of content type is written
        @return header list which was read from input stream
        @exception java.io.IOException if reading or writing error encountered
        @exception EncodingException if encoding error encountered
        @exception ParseException if parsing error encountered */
    public HeaderList encode(InputStream in, OutputStream out, boolean onlyValueOfContentType) throws IOException, EncodingException, ParseException {
        return encode(null, in, out, onlyValueOfContentType);
    }

    /** Includes HTTP-entity in multipart entity to be written afterwards.

        @param headerList entity headers to be encoded or null if it should be read from input stream
        @param in input stream where entity is read from or null
        @return header list which was given in parameter or read from input stream
        @exception java.io.IOException if reading error encountered
        @exception EncodingException if encoding error encountered
        @exception ParseException if parsing error encountered
        @see FI.realitymodeler.common.HeaderList */
    public HeaderList encode(HeaderList headerList, InputStream in) throws IOException, EncodingException, ParseException {
        return encode(headerList, in, null, false);
    }

    /** Includes HTTP-entity in multipart entity to be written afterwards.

        @param in input stream where entity is read from
        @return header list which was read from input stream
        @exception java.io.IOException if reading error encountered
        @exception EncodingException if encoding error encountered
        @exception ParseException if parsing error encountered */
    public HeaderList encode(InputStream in) throws IOException, EncodingException, ParseException {
        return encode(null, in, null, false);
    }

    /** Writes body part of encoded HTTP-entity to output stream.<br>
        Body part is encoded as follows:<br>
        Data Raw data or application/vnd.wap.multipart structure<br>

        @param out output stream where encoded body part is written
        @exception java.io.IOException if reading or writing error encountered
        @exception EncodingException if encoding error encountered */
    public synchronized void encode(OutputStream out) throws IOException, EncodingException, ParseException {
        byte b[] = new byte[Support.bufferLength];
        if (!multipart) {
            InputStream entityIn = entity.load(String.valueOf(1), new HeaderList());
            try {
                for (int n; (n = entityIn.read(b)) > 0;) out.write(b, 0, n);
            } finally {
                entity.closeStream(entityIn);
            }
            return;
        }
        int number = getNumberOfParts();
        encodeUIntVar(out, number);
        HeaderList entityHeaderList;
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        for (int index = 1; index <= number; index++) {
            InputStream entityIn = entity.load(String.valueOf(index), entityHeaderList = new HeaderList());
            try {
                Support.readHeaderList(entityIn, entityHeaderList);
                encode(entityHeaderList, bout, true);
                encodeUIntVar(out, bout.size());
                encodeUIntVar(out, entityIn.available());
                bout.writeTo(out);
                bout.reset();
                for (int n; (n = entityIn.read(b)) > 0;) out.write(b, 0, n);
            } finally {
                entity.closeStream(entityIn);
            }
        }
        out.flush();
    }

    /** Decodes HTTP-body part from input stream.

        @param in input stream where encoded body part is read from
        @param headerList header list decoded beforehand
        @exception java.io.IOException if reading error encountered
        @exception java.io.EOFException if unexpected end of file is encountered
        @exception DecodingException if decoding exception encountered
        @see FI.realitymodeler.common.HeaderList */
    public synchronized HeaderList decode(InputStream in, HeaderList headerList) throws IOException, DecodingException, ParseException {
        byte b[] = new byte[Support.bufferLength];
        // This input stream finds unique boundary string for message body
        ConvertInputStream boundaryIn = new ConvertInputStream(in, boundary);
        String ct = headerList.getHeaderValue("content-type"), subType = "mixed";
        if (ct == null || !(ct = Support.getParameters(ct, null)).toLowerCase().startsWith("application/vnd.wap.multipart/")) {
            OutputStream entityOut = entity.save("1", headerList);
            try {
                for (int n; (n = boundaryIn.read(b)) > 0;) entityOut.write(b, 0, n);
            } finally {
                entity.closeStream(entityOut);
            }
            multipart = getNumberOfParts() > 1;
        } else {
            int number = (int)decodeUIntVar(in);
            for (int i = 1; i <= number; i++) {
                LimitInputStream limitIn = new LimitInputStream(in, (int)decodeUIntVar(in));
                int dataLen = (int)decodeUIntVar(in);
                HeaderList entityHeaderList = decode(limitIn, dataLen, true);
                limitIn = new LimitInputStream(boundaryIn, dataLen);
                OutputStream entityOut = entity.save(String.valueOf(i), entityHeaderList);
                try {
                    for (int n; (n = limitIn.read(b)) > 0;) entityOut.write(b, 0, n);
                } finally {
                    entity.closeStream(entityOut);
                }
            }
            String s = ct.substring(ct.indexOf('/') + 1);
            if (contentTypeTable.containsKey("multipart/" + s)) subType = s;
            multipart = true;
        }
        boundary = boundaryIn.getScan();
        if (multipart) headerList.replace(new Header("Content-Type", "multipart/" + subType + "; boundary=\"" + boundary.substring(4) + '"'));
        return headerList;
    }

    /** Writes decoded HTTP-entity to output stream.

        @param out output stream where decoded entity is written
        @param headerList header list to be written to output stream or null if only body part of entity is written
        @exception java.io.IOException if writing error encountered
        @see FI.realitymodeler.common.HeaderList */
    public synchronized void decode(OutputStream out, HeaderList headerList) throws IOException, DecodingException, ParseException {
        byte b[] = new byte[Support.bufferLength];
        if (!multipart) {
            InputStream entityIn = entity.load(String.valueOf(1), new HeaderList());
            try {
                if (headerList != null) {
                    if (headerList.hasHeader("content-length")) headerList.replace(new Header("Content-Length", String.valueOf(entityIn.available())));
                    Support.sendHeaderList(out, headerList);
                }
                for (int n; (n = entityIn.read(b)) > 0;) out.write(b, 0, n);
            } finally {
                entity.closeStream(entityIn);
            }
            return;
        }
        if (headerList != null) Support.sendHeaderList(out, headerList);
        Support.writeBytes(out, boundary, null);
        HeaderList entityHeaderList;
        int number = getNumberOfParts();
        for (int index = 1; index <= number; index++) {
            InputStream entityIn = entity.load(String.valueOf(index), entityHeaderList = new HeaderList());
            try {
                entityHeaderList.replace(new Header("Content-Length", String.valueOf(entityIn.available())));
                Support.writeBytes(out, "\r\n", null);
                Support.sendHeaderList(out, entityHeaderList);
                for (int n; (n = entityIn.read(b)) > 0;) out.write(b, 0, n);
                Support.writeBytes(out, boundary, null);
            } finally {
                entity.closeStream(entityIn);
            }
        }
        Support.writeBytes(out, "--\r\n", null);
        out.flush();
    }

    /** Writes decoded HTTP-entity data body to output stream.

        @param out output stream where decoded entity data body is written
        @exception java.io.IOException if writing error encountered
        @see FI.realitymodeler.common.HeaderList */
    public synchronized void decode(OutputStream out) throws IOException, DecodingException, ParseException {
        decode(out, null);
    }

    /** Returns number of parts in encoded or decoded multipart entity. */
    public int getNumberOfParts() throws IOException {
        return entity.getNumberOfElements();
    }

    /** Gets input stream of one part of multipart entity. Input stream
        should be closed after use.

        @param index index of the part whose input stream is required (from 1 to number of parts)
        @param headerList headers of the entity part are stored here
        @see FI.realitymodeler.common.HeaderList */
    public InputStream getEntityPart(int index, HeaderList headerList) throws IOException, ParseException {
        InputStream entityIn = entity.load(String.valueOf(index));
        Support.readHeaderList(entityIn, headerList);
        return entityIn;
    }

    /** Resets class instance so that all temporary files belonging to this
        instance are removed and all parts included in multipart entity are
        discarded. This method must be called always when reusing or discarding
        class instance. It is called also during instance finalization during
        garbage collection.

    */
    public synchronized void reset() throws IOException {
        boundary = "\r\n--_";
        multipart = false;
        entity.reset();
    }

    public void finalize() throws IOException {
        reset();
    }

}
