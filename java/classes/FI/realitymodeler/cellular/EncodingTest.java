
package FI.realitymodeler.cellular;

import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;

public class EncodingTest {

    /** Tests HTTP-message entity encoding and decoding of WAP-format. If
        first argument is '-encode' message entity is read from URL given in
        second argument and dumped in text and binary format to standard output.
        If only option is given standard input is used. If URL points to a local
        file or standard input is used, message header is read from it's
        beginning. If first argument is '-decode' operation is done to other
        direction. If third argument is a file name result is written also there
        in binary format and can be used as input to this program. If first
        argument is '-html' HTML-link extracting is tested. If it's '-wml'
        WML-link extracting is tested.  If no option is given, first argument or
        standard input is considered to contain source in WAP WSP/B 1.0 header
        encoding test case format. */

    public static void main(String argv[]) throws Exception {
	EntityEncoding.initialize(null, null, false);
	EntityEncoding encoding = new EntityEncoding();
	try {
            URL url;
            InputStream in;
            URLConnection urlConn = null;
            if (argv.length == 0 || argv[0].startsWith("-") && argv.length < 2) {
                url = new URL("file:unnamed");
                in = System.in;
            } else {
                url = new URL(argv[0].startsWith("-") ? argv[1] : argv[0]);
                urlConn = url.openConnection();
                in = urlConn.getInputStream();
            }
            String arg = argv.length > 0 ? argv[0].toLowerCase() : "";
            if (arg.equals("-encode")) {
                ByteArrayOutputStream bout = new ByteArrayOutputStream();
                HeaderList headerList = new HeaderList();
                if (!url.getHost().equals("")) {
                    String key;
                    for (int i = 0; (key = urlConn.getHeaderFieldKey(i)) != null || i == 0; i++)
                        if (key != null) headerList.append(new Header(key, urlConn.getHeaderField(i)));
                } else Support.readHeaderList(in, headerList);
                Support.sendHeaderList(bout, headerList);
                int size = bout.size();
                bout.reset();
                headerList.dump(System.out);
                encoding.encode(headerList, in, bout, false);
                int compressedSize = bout.size();
                encoding.encode(bout);
                if (argv.length > 2) {
                    OutputStream out = new BufferedOutputStream(new FileOutputStream(argv[2]));
                    encoding.encodeUIntVar(out, compressedSize);
                    bout.writeTo(out);
                    out.close();
                }
                byte bytes[] = bout.toByteArray();
                new HexDumpEncoder().encodeStream(bytes, System.out);
                System.out.println();
                encoding.reset();
                ByteArrayInputStream bin = new ByteArrayInputStream(bytes);
                encoding.decode(System.out, encoding.decode(bin, encoding.decode(new LimitInputStream(bin, compressedSize), bin.available() - compressedSize, false)));
                System.out.println();
                System.out.println("Compressed size of headers = " + compressedSize +
                                   " (from original = " + (double)compressedSize / (double)size * 100.0 + "%)");
                System.out.println();
            } else if (arg.equals("-decode")) {
                ByteArrayOutputStream bout = new ByteArrayOutputStream();
                byte b[] = new byte[Support.bufferLength];
                for (int n; (n = in.read(b)) > 0;) bout.write(b, 0, n);
                ByteArrayInputStream bin = new ByteArrayInputStream(bout.toByteArray());
                int headersLen = (int)encoding.decodeUIntVar(bin);
                encoding.decode(bout, encoding.decode(bin, encoding.decode(new LimitInputStream(bin, headersLen), bin.available() - headersLen, false)));
                byte bytes[] = bout.toByteArray();
                bout.reset();
                encoding.reset();
                encoding.encode(new ByteArrayInputStream(bytes), bout, true);
                encoding.encode(bout);
                if (argv.length > 2) {
                    OutputStream out = new BufferedOutputStream(new FileOutputStream(argv[2]));
                    bout.writeTo(out);
                    out.close();
                }
                new HexDumpEncoder().encodeStream(bout.toByteArray(), System.out);
            } else if (arg.equals("-html")) {
                Vector<URL> foundURLs = new Vector<URL>();
                HTMLExtractor.extract(in, url, foundURLs, new Hashtable<URL, Boolean>());
                System.out.println(foundURLs);
            } else if (arg.equals("-wml")) {
                Vector<URL> foundURLs = new Vector<URL>();
                WMLExtractor.extract(in, url, foundURLs, new Hashtable<URL, Boolean>());
                System.out.println(foundURLs);
            } else {
                ByteArrayOutputStream bout = new ByteArrayOutputStream();
                StreamTokenizer sTok = new StreamTokenizer(new InputStreamReader(in));
                sTok.resetSyntax();
                sTok.wordChars(0, 255);
                sTok.whitespaceChars('\r', '\r');
                sTok.whitespaceChars('\n', '\n');
                sTok.ordinaryChar('%');
                sTok.commentChar('#');
                int failures = 0;
                while (sTok.nextToken() != sTok.TT_EOF)
                    if (sTok.ttype == '%') {
                        if (sTok.nextToken() != sTok.TT_WORD) {
                            sTok.pushBack();
                            continue;
                        }
                        System.out.println(sTok.sval);
                        HeaderList headerList = new HeaderList();
                        char testType;
                        StringBuffer sb = new StringBuffer();
                        for (;;) {
                            if (sTok.nextToken() != sTok.TT_WORD) throw new ParseException("Header line list is invalid", sTok.lineno());
                            if (sTok.sval.trim().length() == 1 && "><=!-".indexOf(testType = sTok.sval.charAt(0)) != -1) break;
                            sb.append(sTok.sval + "\r\n");
                        }
                        headerList.setHeaders(sb.append("\r\n").toString());
                        headerList.dump(System.out);
                        if (sTok.nextToken() != sTok.TT_WORD) throw new ParseException("Encoding byte list is missing", sTok.lineno());
                        StringTokenizer st = new StringTokenizer(sTok.sval, ":");
                        if (!st.hasMoreTokens()) throw new ParseException("Length of encoding is missing", sTok.lineno());
                        StringTokenizer st1 = new StringTokenizer(st.nextToken(), "/");
                        int lengthOfEncoding = Integer.parseInt(st1.nextToken().trim()),
                            dataBodyLength = st1.hasMoreTokens() ? Integer.parseInt(st1.nextToken().trim()) : -1;
                        st = new StringTokenizer(st.nextToken());
                        while (st.hasMoreTokens()) bout.write(Integer.parseInt(st.nextToken(), 16));
                        byte testBytes[] = bout.toByteArray();
                        bout.reset();
                        EntityEncoding.encode(headerList, bout);
                        byte generatedBytes[] = bout.toByteArray();
                        bout.reset();
                        boolean exception = false;
                        Exception ex = null;
                        try {
                            HeaderList decodedHeaderList = EntityEncoding.decode(new ByteArrayInputStream(generatedBytes), dataBodyLength);
                            System.out.println("Decoded header list from generated bytes=");
                            decodedHeaderList.dump(System.out);
                        } catch (IOException ex1) {
                            ex = ex1;
                        } catch (DecodingException ex1) {
                            ex = ex1;
                        }
                        if (exception = ex != null) ex.printStackTrace(System.out);
                        HeaderList decodedHeaderList = null;
                        ex = null;
                        try {
                            decodedHeaderList = EntityEncoding.decode(new ByteArrayInputStream(testBytes), dataBodyLength);
                            System.out.println("Decoded header list from test bytes=");
                            decodedHeaderList.dump(System.out);
                        } catch (IOException ex1) {
                            ex = ex1;
                        } catch (DecodingException ex1) {
                            ex = ex1;
                        }
                        if (ex != null) {
                            exception = true;
                            ex.printStackTrace(System.out);
                        }
                        if (testType != '-' &&
                            (testType != '!' || !exception) &&
                            (testType != '<' || decodedHeaderList == null ||
                             !headerListsEquals(headerList, decodedHeaderList)) &&
                            (exception || Support.compare(testBytes, generatedBytes) != 0)) {
                            System.out.println("Expected encoding=");
                            System.out.print(lengthOfEncoding + ": ");
                            new HexDumpEncoder().encodeStream(testBytes, System.out);
                            System.out.println("Generated encoding=");
                            System.out.print(generatedBytes.length + ": ");
                            new HexDumpEncoder().encodeStream(generatedBytes, System.out);
                            failures++;
                        }
                        System.out.println();
                    }
                if (failures > 0) System.out.println(failures + " failures");
            }
	} catch (ParseException ex) {
            System.out.println("Parsing failed: " + ex.toString() + " in line " + ex.getErrorOffset());
	} finally {
            encoding.reset();
	}
    }

    static boolean headerListsEquals(HeaderList a, HeaderList b) {
	if (a.size() != b.size()) return false;
	HeaderList aClone = (HeaderList)a.clone();
	Iterator items = b.iterator();
	while (items.hasNext())
            if (!aClone.remove((Header)items.next())) return false;
	return true;
    }

}
