
package FI.realitymodeler.cellular;

import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.util.*;

public abstract class LinkExtractor {

    protected static void extract(InputStream in, URL baseURL, Vector<URL> foundURLs, Hashtable<URL, Boolean> doneURLs, Hashtable<String, String> names)
        throws IOException {
	int c;
	StringBuffer sb = new StringBuffer();
	while ((c = in.read()) != -1)
            if (c == '<') {
		do if ((c = in.read()) == -1) return;
		while (Support.whites.indexOf(c) != -1);
		do {
                    sb.append((char)c);
                    if ((c = in.read()) == -1) return;
		} while (Support.whites.indexOf(c) == -1);
		String name = names.get(sb.toString().toLowerCase());
		sb.setLength(0);
		if (name != null) {
                    Map<String, String> values = new HashMap<String, String>();
                    c = Support.scanValues(in, values);
                    String link = values.get(name);
                    if (link != null)
			try {
                            URL foundURL = baseURL != null ? new URL(baseURL, link) : new URL(link);
                            if (doneURLs == null) foundURLs.addElement(foundURL);
                            else if (!doneURLs.containsKey(foundURL)) {
                                doneURLs.put(foundURL, Boolean.TRUE);
                                foundURLs.addElement(foundURL);
                            }
			} catch (MalformedURLException ex) {} while (c > -1 && c != '>') c = in.read();
                    if (c == -1) return;
		}
            }
    }

    public static void extract(InputStream in, URL baseURL, Vector<URL> foundURLs, Hashtable<URL, Boolean> doneURLs)
        throws IOException {
    }

}
