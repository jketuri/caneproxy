
package FI.realitymodeler.cellular;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import FI.realitymodeler.server.*;
import java.io.*;
import java.util.*;
import javax.naming.*;
import javax.naming.directory.*;
import javax.naming.ldap.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** HTML to TTML filter servlet. */
public class Html2Ttml extends HttpServlet {
    static final long serialVersionUID = 0L;

    public void service(HttpServletRequest req, HttpServletResponse res)
        throws IOException, ServletException {
	W3Request w3r = (W3Request)req;
	String ct = req.getContentType();
	res.setContentType("text/x-ttml");
	LdapContext ldapContext[] = new LdapContext[1];
	LinkStore linkStore = null;
	try {
            linkStore = new LinkStore(ldapContext);
            if (!CellularEngine.filter(w3r instanceof CellularRequest ? ((CellularRequest)w3r).linkStore : null,
                                       req.getInputStream(), res.getOutputStream(), w3r.context, w3r.socket.getPort() != CellularSocket.TEXT_PORT,
                                       ct != null && Support.getParameters(ct, null).equalsIgnoreCase(Support.htmlType)))
		throw new IOException("Input stream not exhausted");
	} catch (NamingException ex) {
            throw new ServletException(Support.stackTrace(ex));
	} finally {
            if (linkStore != null) linkStore.close(ldapContext);
	}
    }

}

