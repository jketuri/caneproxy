
package FI.realitymodeler.cellular;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import FI.realitymodeler.server.*;
import java.io.*;
import java.net.*;
import java.util.*;
import javax.naming.*;
import javax.naming.directory.*;
import javax.naming.ldap.*;

public class CellularRequest extends W3Request {
    // WSP PDU Types
    static final int WSP_Connect = 0x01;
    static final int WSP_ConnectReply = 0x02;
    static final int WSP_Redirect = 0x03;
    static final int WSP_Reply = 0x04;
    static final int WSP_Disconnect = 0x05;
    static final int WSP_Push = 0x06;
    static final int WSP_ConfirmedPush = 0x07;
    static final int WSP_Suspend = 0x08;
    static final int WSP_Resume = 0x09;
    static final int WSP_Get = 0x40;
    static final int WSP_Options = 0x41;
    static final int WSP_Head = 0x42;
    static final int WSP_Post = 0x60;
    static final int WSP_Put = 0x61;

    CellularEngine engine;
    BASE64Decoder b64d = new BASE64Decoder();
    LinkStore linkStore = null;
    boolean isWap;

    CellularRequest(CellularEngine engine) {
        super(engine);
        this.engine = engine;
        store = new Store();
        mobile = true;
    }

    public Object clone() {
        return new CellularRequest(engine);
    }

    public String getRemoteAddr() {
        return socket.toString();
    }

    public String getRemoteHost() {
        return socket.toString();
    }

    public void complete()
        throws IOException {
        super.complete();
        if (!isWap) {
            Support.sendMessage(out, responseHeaderList);
            return;
        }
        out.write((status < 500 ? status / 100 : status / 100 + 1) << 4 | (status % 100));
        try {
            HeaderEncoding.encode(responseHeaderList, out, true);
        } catch (EncodingException ex) {
            throw new IOException(Support.stackTrace(ex));
        }
    }

    void setHeaders(String cookieSet) {
        store.headerList = new HeaderList();
        store.headerList.append(new Header("", store.request));
        store.headerList.append(new Header("Accept", "text/x-ttml"));
        store.headerList.append(new Header("User-Agent", "Mobile"));
        if (cookieSet != null) store.headerList.append(new Header("Set-Cookie", cookieSet));
    }

    String checkBoundary(Enumeration fieldElements, String boundary) {
        while (fieldElements.hasMoreElements()) {
            Object obj = fieldElements.nextElement();
            if (obj instanceof Field) {
                Field field = (Field)obj;
                while (field.getName().indexOf(boundary) != -1 || field.getValue().indexOf(boundary) != -1)
                    boundary += (char)(33 + (int)(Math.random() * 14.0));
            } else while (((String)obj).indexOf(boundary) != -1)
                       boundary += (char)(33 + (int)(Math.random() * 14.0));
        }
        return boundary;
    }

    public void run() {
        LdapContext ldapContext[] = new LdapContext[1];
        FI.realitymodeler.server.CookieStore cookieStore = null;
        try {
            linkStore = new LinkStore(ldapContext);
            cookieStore = new FI.realitymodeler.server.CookieStore(ldapContext);
            while (!isInterrupted())
                try {
                    initialize();
                    out = socket.getOutputStream();
                    InputStream in0 = new BufferedInputStream(socket.getInputStream());
                    for (;;) {
                        in = in0;
                        int c;
                        in.mark(1);
                        try {
                            if ((c = in.read()) == -1) break;
                        } catch (IOException ex) {
                            if (isInterrupted()) mustStop = true;
                            else if (ex instanceof InterruptedIOException) continue;
                            break;
                        }
                        in.reset();
                        parameterKeys = new Vector<String>();
                        store.request = "";
                        store.method = "GET";
                        setHeaders(null);
                        try {
                            begin();
                            String cookieSet = null;
                            if (isWap = ((CellularSocket)socket).isWap()) {
                                // Request is supposedly done through Wireless Application Protocol
                                int tid = in.read(), type = in.read();
                                System.out.println("iswap");
                                switch (type) {
                                case WSP_Get: {
                                    System.out.println("wspget");
                                    long uriLen = HeaderEncoding.decodeUIntVar(in);
                                    StringBuffer sb = new StringBuffer();
                                    while (--uriLen >= 0L) sb.append((char)in.read());
                                    store.request = sb.toString();
                                    System.out.println("request="+store.request);
                                    EntityEncoding decoding = new EntityEncoding();
                                    HeaderEncoding.decode(in, 0, false);
                                    decoding.decode(in, store.headerList);
                                    System.out.println("headerList="+store.headerList);
                                    store.requestURI = store.request.startsWith("/") || store.request.indexOf(':') != -1 ? store.request : "/" + store.request;
                                    System.out.println("requestURI="+store.requestURI);
                                    url = new URL(engine.defaultContext, store.requestURI);
                                    System.out.println("url="+url);
                                    break;
                                }
                                }
                            } else {
                                String encoding = "8859_1";
                                byte b[] = new byte[in.available()];
                                in.mark(b.length);
                                int n, cl = -1;
                                for (int i = 0; i < b.length; i += n) n = in.read(b, i, b.length - i);
                                String request = new String(b), ct = null;
                                if (w3server.verbose) w3server.log(request);
                                if (request.startsWith("<") ||
                                    request.startsWith(">") ||
                                    request.startsWith(".") ||
                                    request.indexOf("\n<") != -1 ||
                                    request.indexOf("\n>") != -1 ||
                                    request.indexOf("\n.") != -1) {
                                    // Request is form sent back from client
                                    if (w3server.verbose) w3server.log("Client sent back form");
                                    in.reset();
                                    Vector<Object> fields = new Vector<Object>();
                                    Link item = CellularEngine.extend(linkStore, in, fields);
                                    if (item == null) throw new W3Exception(HttpURLConnection.HTTP_BAD_REQUEST, "Bad request: No selected items found");
                                    if (w3server.verbose) w3server.log("Form parsed, constructing query");
                                    store.request = store.requestURI = item.getUrl();
                                    url = new URL(store.requestURI);
                                    if (item instanceof Form) {
                                        Form form = (Form)item;
                                        store.method = form.getMethod();
                                        Enumeration formFieldElements = form.getFields().elements(), fieldElements = fields.elements();
                                        Field field1 = fieldElements.hasMoreElements() ? (Field)fieldElements.nextElement() : null;
                                        byte encTypeCode = form.getEncTypeCode();
                                        if (encTypeCode == Form.FORM_DATA) {
                                            String boundary = checkBoundary(fields.elements(), checkBoundary(form.getFields().elements(), "\r\n--_"));
                                            ByteArrayOutputStream bout = new ByteArrayOutputStream();
                                            Support.writeBytes(bout, boundary, null);
                                            while (formFieldElements.hasMoreElements()) {
                                                Support.writeBytes(bout, "\r\n", null);
                                                String name;
                                                Object obj = formFieldElements.nextElement();
                                                if (obj instanceof Field) {
                                                    Field field = (Field)obj;
                                                    name = field.getName();
                                                    if (field.isHidden() || field1 == null || !field1.getName().equals(name)) {
                                                        Support.writeBytes(bout, "\r\nContent-Disposition: form-data; name=\"" + name + "\"\r\n\r\n", null);
                                                        Support.writeBytes(bout, field.getValue(), null);
                                                        Support.writeBytes(bout, boundary, null);
                                                        continue;
                                                    }
                                                } else name = (String)obj;
                                                if (field1 == null) continue;
                                                while (field1.getName().equals(name)) {
                                                    Support.writeBytes(bout, "\r\nContent-Disposition: form-data; name=\"" + name + "\"\r\n\r\n", null);
                                                    Support.writeBytes(bout, field1.getValue(), null);
                                                    Support.writeBytes(bout, boundary, null);
                                                    if (!fieldElements.hasMoreElements()) {
                                                        field1 = null;
                                                        break;
                                                    }
                                                    field1 = (Field)fieldElements.nextElement();
                                                }
                                            }
                                            Support.writeBytes(bout, "--", null);
                                            cl = bout.size();
                                            ct = form.getEncType();
                                            in = new ByteArrayInputStream(bout.toByteArray());
                                        } else {
                                            StringBuffer sb = new StringBuffer();
                                            while (formFieldElements.hasMoreElements()) {
                                                String name;
                                                Object obj = formFieldElements.nextElement();
                                                if (obj instanceof Field) {
                                                    Field field = (Field)obj;
                                                    name = field.getName();
                                                    if (field.isHidden() || field1 == null || !field1.getName().equals(name)) {
                                                        if (sb.length() > 0) sb.append('&');
                                                        sb.append(URLEncoder.encode(name, encoding)).append('=').append(URLEncoder.encode(field.getValue(), encoding));
                                                        continue;
                                                    }
                                                } else name = (String)obj;
                                                if (field1 == null) continue;
                                                while (field1.getName().equals(name)) {
                                                    if (sb.length() > 0) sb.append('&');
                                                    sb.append(URLEncoder.encode(name, encoding)).append('=').append(URLEncoder.encode(field1.getValue(), encoding));
                                                    if (!fieldElements.hasMoreElements()) {
                                                        field1 = null;
                                                        break;
                                                    }
                                                    field1 = (Field)fieldElements.nextElement();
                                                }
                                            }
                                            String query = sb.toString();
                                            if (w3server.verbose) w3server.log("Constructed query is " + query);
                                            if (store.method.equals("POST")) {
                                                cl = query.length();
                                                ct = form.getEncType();
                                                in = new ByteArrayInputStream(query.getBytes());
                                            } else store.requestURI += "?" + query;
                                        }
                                    }
                                    store.request = store.requestURI;
                                } else {
                                    store.request = request;
                                    String keyword = null;
                                    Decoder decoder = null;
                                    StreamTokenizer sTok = new StreamTokenizer(new StringReader(request));
                                    sTok.resetSyntax();
                                    sTok.wordChars(0, 255);
                                    if (request.startsWith("//WSMS")) {
                                        sTok.whitespaceChars('¤', '¤');
                                        if (sTok.nextToken() == sTok.TT_EOF || sTok.nextToken() == sTok.TT_EOF ||
                                            !sTok.sval.equals("I")) throw new W3Exception(HttpURLConnection.HTTP_BAD_REQUEST, "Bad request: Unsupported content type");
                                        while (sTok.nextToken() != sTok.TT_EOF) {
                                            if (sTok.sval.startsWith("L") || sTok.sval.startsWith("N")) keyword = sTok.sval.substring(1).trim();
                                            else if (sTok.sval.startsWith("E")) {
                                                String s;
                                                if (!(s = sTok.sval.substring(1)).equalsIgnoreCase("b6")) throw new W3Exception(HttpURLConnection.HTTP_BAD_REQUEST, "Bad request: Unsupported document encoding");
                                                decoder = b64d;
                                            } else if (sTok.sval.startsWith("D")) break;
                                        }
                                    } else {
                                        sTok.whitespaceChars(0, 32);
                                        sTok.quoteChar('"');
                                        if (sTok.nextToken() != sTok.TT_EOF &&
                                            (keyword = sTok.sval).startsWith("*#*#*#") &&
                                            (keyword = keyword.substring(6).trim()).equals("")) keyword = CellularEngine.language;
                                    }
                                    if (keyword == null) throw new W3Exception(HttpURLConnection.HTTP_BAD_REQUEST, "Bad request: No keyword");
                                    if (keyword.startsWith("$")) {
                                        int i = keyword.indexOf('/');
                                        if (i != -1) {
                                            String username, password;
                                            int j = keyword.lastIndexOf(':', i);
                                            if (j != -1) {
                                                username = keyword.substring(1, j);
                                                password = keyword.substring(j + 1, i);
                                            } else {
                                                username = keyword.substring(1, i);
                                                password = "";
                                            }
                                            keyword = keyword.substring(i + 1);
                                            if (w3server.auth.getServlet() instanceof AuthServlet)
                                                cookieSet = W3URLConnection.cookieSetHeader(((AuthServlet)w3server.auth.getServlet()).makeCookie(keyword, username, password));
                                        }
                                    }
                                    store.requestURI = keyword.startsWith("/") || keyword.indexOf(':') != -1 ? keyword : "/" + keyword;
                                    url = new URL(engine.defaultContext, store.requestURI);
                                    Map<String, Object> parameterValues = new HashMap<String, Object>();
                                    while (sTok.nextToken() != sTok.TT_EOF) {
                                        String name, value = sTok.sval;
                                        int i = value.indexOf('=');
                                        if (i != -1) {
                                            name  = value.substring(0, i);
                                            value = value.substring(i + 1);
                                        } else name = "";
                                        if (decoder != null) value = new String(decoder.decodeStream(value));
                                        Object obj = parameterValues.get(name);
                                        if (obj != null) {
                                            Vector<String> vector;
                                            if (obj instanceof String) {
                                                vector = new Vector<String>();
                                                vector.addElement((String)obj);
                                                parameterValues.put(name, vector);
                                            } else vector = (Vector<String>)obj;
                                            vector.addElement(value);
                                        } else parameterValues.put(name, value);
                                        parameterKeys.addElement(name);
                                    }
                                    W3Request.setParameters(queryParameters, parameterValues);
                                }
                                setHeaders(cookieSet);
                                if (cl != -1) store.headerList.append(new Header("Content-Length", String.valueOf(cl)));
                                if (ct != null) store.headerList.append(new Header("Content-Type", ct));
                            }
                            // checks if total number of requests exceeds the limit
                            if (engine.maxRequests >= 0 && activeCount() - engine.pool.poolCount() > engine.maxRequests) {
                                sender.sendError(HttpURLConnection.HTTP_UNAVAILABLE, "Service Unavailable");
                                return;
                            }
                            for (int redirectCount = 0; redirectCount < 2; redirectCount++) {
                                String clientHost = InetAddress.getByName(url.getHost()).getHostAddress(), clientPath = url.getFile().trim();
                                int i = clientPath.indexOf('?');
                                if (i != -1) clientPath = clientPath.substring(0, i).trim();
                                cookieStore.setCookieHeaderValues(store.headerList, getRemoteAddr(), clientHost, clientPath);
                                begin();
                                boolean redirect = false;
                                try {
                                    handle(store.requestURI);
                                } catch (W3Exception ex) {
                                    if (ex.getStatus() == HttpURLConnection.HTTP_MOVED_PERM ||
                                        ex.getStatus() == HttpURLConnection.HTTP_MOVED_TEMP) redirect = true;
                                    else throw ex;
                                }
                                cookieStore.getCookieHeaderValues(responseHeaderList, getRemoteAddr(), clientHost, clientPath);
                                if (redirect) {
                                    String location = getProperty("location");
                                    if (location != null) {
                                        url = new URL(url, location);
                                        store.request = store.requestURI = engine.isLocal(url.getHost()) ? url.getFile() : url.toExternalForm();
                                        setHeaders(cookieSet);
                                        continue;
                                    }
                                }
                                break;
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            if (ex instanceof InterruptedException) throw ex;
                            handle(ex);
                        }
                        end(store);
                    }
                    if (mustStop) break;
                    LinkStore.checkCleaning(w3context);
                    FI.realitymodeler.server.CookieStore.checkCleaning(w3context);
                } catch (Exception ex) {
                    if (ex instanceof InterruptedException) mustStop = true;
                    else w3server.handle(ex);
                } finally {
                    finish();
                    if (mustStop || engine.release(this)) break;
                }
        } catch (Exception ex) {
            if (!(ex instanceof InterruptedException)) w3server.handle(ex);
        } finally {
            try {
                if (cookieStore != null) cookieStore.close(null);
            } finally {
                if (linkStore != null) linkStore.close(ldapContext);
            }
        }
    }

    protected void finalize() {
        if (isAlive()) interrupt();
    }

}
