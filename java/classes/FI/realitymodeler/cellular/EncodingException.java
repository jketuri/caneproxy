
package FI.realitymodeler.cellular;

public class EncodingException extends Exception {
    static final long serialVersionUID = 0L;

    public EncodingException(String msg) {
	super(msg);
    }

}
