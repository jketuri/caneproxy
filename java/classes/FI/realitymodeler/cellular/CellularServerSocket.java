
package FI.realitymodeler.cellular;

import FI.realitymodeler.*;
import java.io.*;
import java.net.*;

public class CellularServerSocket extends ServerSocket {
    CellularSocket cellular;
    byte address[];
    int maxMsgSize;
    int port;
    int type;

    public CellularServerSocket(byte address[], int port, int type) throws IOException {
	super(0);
	cellular = new CellularSocket(address, port, type);
	this.address = address;
	this.port = port;
	this.type = type;
	maxMsgSize = cellular.getMaxMsgSize();
    }

    public Socket accept() throws IOException {
	for (;;) {
            CellularSocket acceptedCellular = cellular.acceptDatagram();
            if (acceptedCellular != null) return acceptedCellular;
	}
        /*
          try {
          Thread.sleep(pollingInterval);
          } catch (InterruptedException ex) {}
        */
    }

}
