
package FI.realitymodeler.cellular;

public class InitializationException extends Exception {
    static final long serialVersionUID = 0L;

    public InitializationException(String msg) {
	super(msg);
    }

}
