
package FI.realitymodeler.cellular;

public class DecodingException extends Exception {
    static final long serialVersionUID = 0L;

    public DecodingException(String msg) {
	super(msg);
    }

}
