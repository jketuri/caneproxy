
package FI.realitymodeler.cellular;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import FI.realitymodeler.server.*;
import java.io.*;
import java.net.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.naming.*;

public class Html2Wml extends HttpServlet {
    static final long serialVersionUID = 0L;

    public String getLabel(String s) {
	int i = s.lastIndexOf('?');
	if (i != -1) s = s.substring(0, i);
	return s.substring(s.lastIndexOf('/') + 1).trim();
    }

    public String saveLink(LinkStore linkStore, URL url)
        throws MalformedURLException, ServletException, UnsupportedEncodingException {
	try {
            Link link = new Link(url);
            linkStore.saveLink(link);
            return Long.toString(link.getNumber(), Character.MAX_RADIX);
	} catch (NamingException ex) {
            throw new ServletException(Support.stackTrace(ex));
	}
    }

    void writeSelectionList(PrintWriter writer, String selectName, boolean multiple, Vector<String> choices, Vector<String> defaultChoices, Form form)
        throws IOException {
	writer.print(selectName + ":<select name=\"");
	Support.writeXmlString(writer, selectName);
	writer.print("\"");
	if (multiple) writer.print(" multiple=\"true\"");
	if (defaultChoices.size() > 0) {
            writer.print(" value=\"");
            Enumeration defaultChoiceElements = defaultChoices.elements();
            if (defaultChoiceElements.hasMoreElements())
		for (;;) {
                    Support.writeXmlString(writer, (String)defaultChoiceElements.nextElement(), true);
                    if (!defaultChoiceElements.hasMoreElements()) break;
                    writer.print(";");
		}
            writer.print("\"");
	}
	writer.println(">");
	if (choices.size() == 1 && multiple) {
            Enumeration choiceElements = choices.elements();
            while (choiceElements.hasMoreElements()) {
                writer.print("<option value=\"");
                Support.writeXmlString(writer, selectName);
                writer.println("\"/>");
            }
            form.addField(new Field("$(" + selectName + ")", "", false));
	} else if (choices.size() > 0) {
            Enumeration choiceElements = choices.elements();
            while (choiceElements.hasMoreElements()) {
                writer.print("<option value=\"");
                Support.writeXmlString(writer, (String)choiceElements.nextElement());
                writer.println("\"/>");
            }
            form.addField(new Field(selectName, null, false));
	}
	writer.println("</select>");
    }

    public void service(HttpServletRequest req, HttpServletResponse res)
        throws ServletException, IOException {
	Form form = null;
	String selectName = null, selectType = null, submit = null;
	URL context = new URL(req.getRequestURL().toString());
	Vector<String> choices = null, defaultChoices = null;
	boolean inLink = false, inForm = false, multiple = false, preFormatted = false, rowStarted = false, valid = false, wasChar = false, eof[] = new boolean[1];
	LinkStore linkStore = null;
	char spaceChar = ' ';
	InputStream in = req.getInputStream();
	String ct = req.getContentType();
	res.setContentType("text/vnd.wap.wml");
	boolean html = ct != null && Support.getParameters(ct, null).equalsIgnoreCase(Support.htmlType);
	PrintWriter writer = res.getWriter();
	writer.println("<?xml version=\"1.0\"?>");
	writer.println("<!DOCTYPE wml PUBLIC \"-//WAPFORUM//DTD WML 1.1//EN\"");
	writer.println("\"http://www.wapforum.org/DTD/wml_1.1.xml\">");
	writer.println("<wml><card id=\"a\"><p>");
	int c;
	while ((c = in.read()) != -1) {
            if (!html || c != '<') {
                // coded character
                if (html && c == '&' && (c = Support.convert(in, eof)) == -1 || inForm)
                    if (eof[0]) break;
                    else continue;
                // character
                boolean isChar = !Character.isWhitespace((char)c);
                if (isChar) {
                    if (!wasChar)
                        if (preFormatted && spaceChar == '\n') writer.println("<br/>");
                        else writer.write(spaceChar);
                    spaceChar = ' ';
                    switch (c) {
                    case '&':
                        writer.print(Support.ampSign);
                        break;
                    case '<':
                        writer.print(Support.leftSign);
                        break;
                    case '>':
                        writer.print(Support.rightSign);
                        break;
                    default:
                        writer.write(c);
                    }
                } else spaceChar = c == '\n' ? '\n' : ' ';
                wasChar = isChar;
                if (eof[0]) break;
                continue;
            }
            // tag
            wasChar = false;
            if ((c = Support.scanTag(in, "a", false)) == Support.TAG_WITH_VALUES) {
                // a
                Map<String, String> values = new HashMap<String, String>();
                c = Support.scanValues(in, values);
                if (c == -1) break;
                String href = values.get("href");
                if (href != null)
                    try {
                        if (linkStore != null) href = saveLink(linkStore, new URL(context, href));
                        if (!wasChar)
                            if (preFormatted && spaceChar == '\n') writer.println("<br/>");
                            else writer.write(spaceChar);
                        spaceChar = ' ';
                        writer.print("<a href=\"");
                        Support.writeXmlString(writer, href);
                        writer.print("\">");
                        wasChar = true;
                        inLink = true;
                    } catch (MalformedURLException ex) {}
            } else if ((c = Support.scanTag(in, "/a", true)) <= 0) {
                if (inLink) writer.print("</a>");
            } else if ((c = Support.scanTag(in, "form", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES)
		try {
                    // form
                    Map<String, String> values = new HashMap<String, String>();
                    c = Support.scanValues(in, values);
                    if (c == -1) break;
                    inForm = true;
                    valid = false;
                    String action = values.get("action"), method = values.get("method");
                    form = action != null && method != null && context != null ? new Form(new URL(context, action), method, values.get("encType")) : null;
                    valid = form != null;
		} catch (MalformedURLException ex) {}
            else if ((c = Support.scanTag(in, "/form", true)) <= 0) {
                // form ends
                if (selectName != null) {
                    writeSelectionList(writer, selectName, multiple, choices, defaultChoices, form);
                    selectName = null;
                }
                inForm = valid = false;
                if (form != null) {
                    writer.println("<do type=\"accept\">");
                    writer.println("<go method=\"" + form.getMethod().toLowerCase() + "\" href=\"" + form.getUrl() + "\">");
                    Enumeration formFieldElements = form.getFields().elements();
                    while (formFieldElements.hasMoreElements()) {
                        Field field = (Field)formFieldElements.nextElement();
                        writer.print("<postfield name=\"");
                        Support.writeXmlString(writer, field.getName());
                        writer.print("\" value=\"");
                        if (field.getValue() == null) {
                            writer.print("$(");
                            Support.writeXmlString(writer, field.getName());
                            writer.print(")");
                        } else Support.writeXmlString(writer, field.getValue());
                        writer.print("\"/>");
                    }
                    writer.println("</go></do>");
                    spaceChar = ' ';
                    wasChar = true;
                }
            } else if ((c = Support.scanTag(in, "img", true)) == Support.TAG_WITH_VALUES) {
                // img
                Map<String, String> values = new HashMap<String, String>();
                c = Support.scanValues(in, values);
                String alt = values.get("alt"), src = values.get("src");
                if (alt != null)
                    try {
                        if (!wasChar)
                            if (preFormatted && spaceChar == '\n') writer.println("<br/>");
                            else writer.write(spaceChar);
                        spaceChar = ' ';
                        Support.writeXmlString(writer, alt);
                        wasChar = true;
                        /*
                          if (src != null)
                          try {
                          if (linkStore != null) src = saveLink(linkStore, new URL(context, src));
                          if (!wasChar)
                          if (preFormatted && spaceChar == '\n') writer.println("<br/>");
                          else writer.write(spaceChar);
                          spaceChar = ' ';
                          writer.print("<img src=\"");
                          Support.writeXmlString(writer, src);
                          writer.print("\"");
                          if (alt != null) {
                          writer.print(" alt=\"");
                          Support.writeXmlString(writer, alt);
                          writer.print("\"");
                          }
                          writer.print("/>");
                          wasChar = true;
                        */
                    } catch (MalformedURLException ex) {}
            } else if ((c = Support.scanTag(in, "script", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) {
                // script
                while (c > -1 && c != '>') c = in.read();
                if (c == -1) break;
                while ((c = in.read()) != -1)
                    if (c == '<') {
                        if ((c = Support.scanTag(in, "/script", false)) <= 0) break;
                        while (c > -1 && c != '>') c = in.read();
                        if (c == -1) break;
                    }
                if (c == -1) break;
            } else if ((c = Support.scanTag(in, "!--", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) {
                // comment
                for (;;) {
                    in.mark(3);
                    if ((c = Support.scanTag(in, "--", false)) < 0) break;
                    in.reset();
                    if ((c = in.read()) == -1) break;
                }
                if (c == -1) break;
                continue;
            } else if ((c = Support.scanTag(in, "title", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) {
                if (c == -1) break;
                while ((c = in.read()) != -1)
                    if (c == '<' && (c = Support.scanTag(in, "/title", false)) <= 0) break;
            } else if ((c = Support.scanTag(in, "b", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) writer.print("<b>");
            else if ((c = Support.scanTag(in, "big", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) writer.print("<big>");
            else if ((c = Support.scanTag(in, "em", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) writer.print("<em>");
            else if ((c = Support.scanTag(in, "i", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) writer.print("<i>");
            else if ((c = Support.scanTag(in, "small", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) writer.print("<small>");
            else if ((c = Support.scanTag(in, "strong", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) writer.print("<strong>");
            else if ((c = Support.scanTag(in, "u", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) writer.print("<u>");
            else if ((c = Support.scanTag(in, "br", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) writer.print("<br/>");
            else if ((c = Support.scanTag(in, "pre", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) preFormatted = true;
            //		else if ((c = Support.scanTag(in, "td", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES || (c = Support.scanTag(in, "th", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) writer.print("<td>");
            //		else if ((c = Support.scanTag(in, "tr", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) writer.print("<tr>");
            //		else if ((c = Support.scanTag(in, "table", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) writer.print("<table>");
            else if ((c = Support.scanTag(in, "/b", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) writer.print("</b>");
            else if ((c = Support.scanTag(in, "/big", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) writer.print("</big>");
            else if ((c = Support.scanTag(in, "/em", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) writer.print("</em>");
            else if ((c = Support.scanTag(in, "/i", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) writer.print("</i>");
            else if ((c = Support.scanTag(in, "/small", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) writer.print("</small>");
            else if ((c = Support.scanTag(in, "/strong", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) writer.print("</strong>");
            else if ((c = Support.scanTag(in, "/u", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) writer.print("</u>");
            else if ((c = Support.scanTag(in, "/br", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) writer.print("<br/>");
            else if ((c = Support.scanTag(in, "/pre", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) preFormatted = true;
            //		else if ((c = Support.scanTag(in, "/td", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES || (c = Support.scanTag(in, "/th", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) writer.print("</td>");
            //		else if ((c = Support.scanTag(in, "/tr", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) writer.print("</tr>");
            //		else if ((c = Support.scanTag(in, "/table", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) writer.print("</table>");
            else if (valid)
		if ((c = Support.scanTag(in, "input", true)) == Support.TAG_WITH_VALUES) {
                    // input
                    Map<String, String> values = new HashMap<String, String>();
                    c = Support.scanValues(in, values);
                    String name = values.get("name"), type = values.get("type"), value = values.get("value");
                    if (name != null) {
                        boolean password = false;
                        if (type == null || (type = type.toLowerCase()).equals("text") || (password = type.equals("password"))) {
                            if (selectName != null) {
                                writeSelectionList(writer, selectName, multiple, choices, defaultChoices, form);
                                selectName = null;
                            }
                            if (name != null) {
                                writer.print(name + ":<input name=\"");
                                Support.writeXmlString(writer, name);
                                writer.print("\"");
                                if (password) writer.print(" type=\"password\"");
                                String maxLength = values.get("maxlength");
                                if (maxLength != null) writer.print(" maxlength=\"" + maxLength + "\"");
                                if (value != null) {
                                    writer.print(" value=\"");
                                    Support.writeXmlString(writer, value);
                                    writer.print("\"");
                                }
                                writer.println("/>");
                                form.addField(new Field(name, null, false));
                                spaceChar = ' ';
                                wasChar = true;
                            }
                        } else if (type.equals("hidden")) form.addField(new Field(name, value != null ? value : "", true));
                        else if ((multiple = type.equals("checkbox")) || type.equals("radio")) {
                            if (selectName == null || !selectName.equals(name) || selectType == null || !selectType.equals(name)) {
                                if (selectName != null) writeSelectionList(writer, selectName, multiple, choices, defaultChoices, form);
                                selectName = name;
                                selectType = type;
                                choices = new Vector<String>();
                                defaultChoices = new Vector<String>();
                            }
                            if (value == null) value = "";
                            if (values.containsKey("checked")) defaultChoices.addElement(value);
                            choices.addElement(value);
                        }
                        spaceChar = ' ';
                        wasChar = true;
                    } else if (type.equals("submit") && values.containsKey("value")) submit = values.get("value");
		} else if ((c = Support.scanTag(in, "select", true)) == Support.TAG_WITH_VALUES) {
                    // select
                    if (selectName != null) {
                        writeSelectionList(writer, selectName, multiple, choices, defaultChoices, form);
                        selectName = null;
                    }
                    Map<String, String> values = new HashMap<String, String>();
                    c = Support.scanValues(in, values);
                    if (c == -1) break;
                    selectName = values.get("name");
                    selectType = null;
                    multiple = values.containsKey("multiple");
                    StringBuffer sb = new StringBuffer();
                    choices = new Vector<String>();
                    defaultChoices = new Vector<String>();
                    values = null;
                    while ((c = in.read()) != -1)
			if (c == '<') {
                            if (selectName != null && values != null) {
                                String value = values.get("value");
                                if (value == null) value = sb.toString();
                                if (values.containsKey("selected")) defaultChoices.addElement(value);
                                choices.addElement(value);
                                values = null;
                            }
                            if ((c = Support.scanTag(in, "option", false)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) {
                                values = new HashMap<String, String>();
                                if (c == 0) c = Support.scanValues(in, values);
                            } else if ((c = Support.scanTag(in, "/select", true)) <= 0) break;
                            while (c > -1 && c != '>') c = in.read();
                            if (c == -1) break;
                            sb.setLength(0);
			} else if (c >= 32) sb.append((char)c);
                    if (selectName != null)
			if (choices.size() > 1) {
                            form.addField(new Field(selectName, null, false));
                            writeSelectionList(writer, selectName, multiple, choices, defaultChoices, form);
                            selectName = null;
                            spaceChar = ' ';
                            wasChar = true;
			} else form.addField(new Field(selectName, choices.isEmpty() ? "" : choices.firstElement(), true));
		} else if ((c = Support.scanTag(in, "textarea", true)) == Support.TAG_WITH_VALUES) {
                    // textarea
                    if (selectName != null) {
                        writeSelectionList(writer, selectName, multiple, choices, defaultChoices, form);
                        selectName = null;
                    }
                    Map<String, String> values = new HashMap<String, String>();
                    c = Support.scanValues(in, values);
                    StringBuffer sb = new StringBuffer();
                    while ((c = in.read()) != -1)
			if (c == '<') {
                            if ((c = Support.scanTag(in, "/textarea", false)) <= 0) break;
                            while (c > -1 && c != '>') c = in.read();
                            if (c == -1) break;
			} else sb.append((char)c);
                    String name = values.get("name");
                    if (name != null) {
                        writer.print(name + ":<input key=\"");
                        Support.writeXmlString(writer, name);
                        writer.print("\"");
                        if (sb.length() > 0) {
                            writer.print(" value=\"");
                            Support.writeXmlString(writer, sb.toString());
                            writer.print("\"");
                        }
                        writer.println(">");
                        form.addField(new Field(name, null, false));
                        spaceChar = ' ';
                        wasChar = true;
                    }
		} while (c > -1 && c != '>') c = in.read();
            if (c == -1) break;
	}
	if (selectName != null) writeSelectionList(writer, selectName, multiple, choices, defaultChoices, form);
	writer.println("</p></card></wml>");
    }

}
