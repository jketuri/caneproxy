
package FI.realitymodeler;

import java.io.*;
import java.util.*;

public class W3CopyRequester extends W3Requester implements Cloneable {
    protected String response = null;
    protected int lastReplyCode;

    public Object clone() {
        return copy(new W3CopyRequester());
    }

    public int readResponse()
        throws IOException {
        StringBuffer replyBuffer = new StringBuffer(), responseBuffer = new StringBuffer();
        int continuingCode = -1, code = -1;
        for (;;) {
            int c0 = 0;
            for (;;) {
                int c;
                if ((c = input.read()) == -1) throw new EOFException();
                if (c0 == '\r' && c == '\n') break;
                replyBuffer.append((char)c);
                c0 = c;
            }
            if (responseBuffer.length() > 0) responseBuffer.append('\n');
            String reply = replyBuffer.toString();
            replyBuffer.setLength(0);
            if (reply.length() > 3 && Character.isDigit(reply.charAt(0))) {
                try {
                    code = Integer.parseInt(reply.substring(0, 3));
                    responseBuffer.append(reply.substring(4));
                    if (continuingCode != -1 && code != continuingCode || reply.charAt(3) == '-') {
                        continuingCode = code;
                        continue;
                    }
                    break;
                } catch (NumberFormatException ex) {}
            }
            responseBuffer.append(reply);
        }
        response = responseBuffer.toString();
        return lastReplyCode = code;
    }

    public String getResponse() {
        return response;
    }

    public W3CopyRequester(String host, int port)
        throws IOException {
        super(host, port);
    }

    public W3CopyRequester() {}

}
