
package FI.realitymodeler.sqlbean;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.math.*;
import java.sql.*;
import java.text.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

interface SqlConversion
{

public int getType();

public Object convert(String parameter);

}

class SqlBitConversion implements SqlConversion
{

public int getType()
{
	return Types.BIT;
}

public Object convert(String parameter)
{
	return new Boolean(parameter);
}

}

class SqlTinyIntConversion implements SqlConversion
{

public int getType()
{
	return Types.TINYINT;
}

public Object convert(String parameter)
{
	return new Byte(parameter);
}

}

class SqlSmallIntConversion implements SqlConversion
{

public int getType()
{
	return Types.SMALLINT;
}

public Object convert(String parameter)
{
	return new Short(parameter);
}

}

class SqlIntegerConversion implements SqlConversion
{

public int getType()
{
	return Types.INTEGER;
}

public Object convert(String parameter)
{
	return new Integer(parameter);
}

}

class SqlBigIntConversion implements SqlConversion
{

public int getType()
{
	return Types.BIGINT;
}

public Object convert(String parameter)
{
	return new Long(parameter);
}

}

class SqlFloatConversion implements SqlConversion
{

public int getType()
{
	return Types.FLOAT;
}

public Object convert(String parameter)
{
	return new Float(parameter);
}

}

class SqlRealConversion implements SqlConversion
{

public int getType()
{
	return Types.REAL;
}

public Object convert(String parameter)
{
	return new Float(parameter);
}

}

class SqlDoubleConversion implements SqlConversion
{

public int getType()
{
	return Types.DOUBLE;
}

public Object convert(String parameter)
{
	return new Double(parameter);
}

}

class SqlNumericConversion implements SqlConversion
{

public int getType()
{
	return Types.NUMERIC;
}

public Object convert(String parameter)
{
	return new BigDecimal(parameter);
}

}

class SqlDecimalConversion implements SqlConversion
{

public int getType()
{
	return Types.DECIMAL;
}

public Object convert(String parameter)
{
	return new BigDecimal(parameter);
}

}

class SqlCharConversion implements SqlConversion
{

public int getType()
{
	return Types.CHAR;
}

public Object convert(String parameter)
{
	return parameter;
}

}

class SqlVarCharConversion implements SqlConversion
{

public int getType()
{
	return Types.VARCHAR;
}

public Object convert(String parameter)
{
	return parameter;
}

}

class SqlLongVarCharConversion implements SqlConversion
{

public int getType()
{
	return Types.LONGVARCHAR;
}

public Object convert(String parameter)
{
	return parameter;
}

}

class SqlDateConversion implements SqlConversion
{

public int getType()
{
	return Types.DATE;
}

public Object convert(String parameter)
{
	return java.sql.Date.valueOf(parameter);
}

}

class SqlTimeConversion implements SqlConversion
{

public int getType()
{
	return Types.TIME;
}

public Object convert(String parameter)
{
	return java.sql.Time.valueOf(parameter);
}

}

class SqlTimestampConversion implements SqlConversion
{

public int getType()
{
	return Types.TIMESTAMP;
}

public Object convert(String parameter)
{
	return Timestamp.valueOf(parameter.indexOf('.') != -1 ? parameter : parameter + ".000000000");
}

}

class SqlBinaryConversion implements SqlConversion
{

public int getType()
{
	return Types.BINARY;
}

public Object convert(String parameter)
{
	return parameter.getBytes();
}

}

class SqlVarBinaryConversion implements SqlConversion
{

public int getType()
{
	return Types.VARBINARY;
}

public Object convert(String parameter)
{
	return parameter.getBytes();
}

}

class SqlLongVarBinaryConversion implements SqlConversion
{

public int getType()
{
	return Types.LONGVARBINARY;
}

public Object convert(String parameter)
{
	return parameter.getBytes();
}

}

class SqlNullConversion implements SqlConversion
{

public int getType()
{
	return Types.NULL;
}

public Object convert(String parameter)
{
	return null;
}

}

class SqlParameter
{
	String name;
	String defaultValue;
	String checkStatement;
	SqlConversion conversion;

SqlParameter(String name, SqlConversion conversion, String defaultValue, String checkStatement)
{
	this.name = name;
	this.conversion = conversion;
	this.defaultValue = defaultValue;
	this.checkStatement = checkStatement;
}

}

class SqlStatement
{
    private static HashMap<String,SqlConversion> conversions = new HashMap<String,SqlConversion>();
	static
	{
		try
		{
			conversions.put("bit", new SqlBitConversion());
			conversions.put("tinyint", new SqlTinyIntConversion());
			conversions.put("smallint", new SqlSmallIntConversion());
			conversions.put("integer", new SqlIntegerConversion());
			conversions.put("bigint", new SqlBigIntConversion());
			conversions.put("float", new SqlFloatConversion());
			conversions.put("real", new SqlRealConversion());
			conversions.put("double", new SqlDoubleConversion());
			conversions.put("numeric", new SqlNumericConversion());
			conversions.put("decimal", new SqlDecimalConversion());
			conversions.put("char", new SqlCharConversion());
			conversions.put("varchar", new SqlVarCharConversion());
			conversions.put("longvarchar", new SqlLongVarCharConversion());
			conversions.put("date", new SqlDateConversion());
			conversions.put("time", new SqlTimeConversion());
			conversions.put("timestamp", new SqlTimestampConversion());
			conversions.put("binary", new SqlBinaryConversion());
			conversions.put("varbinary", new SqlVarBinaryConversion());
			conversions.put("longvarbinary", new SqlLongVarBinaryConversion());
			conversions.put("null", new SqlNullConversion());
		}
		catch (Exception ex)
		{
			throw new Error(ex.toString());
		}
	}

	String statement;
	Vector<SqlParameter> parameters = new Vector<SqlParameter>();

SqlStatement(String sql) throws ServletException
{
	int i = 0, j;
	StringBuffer sb = new StringBuffer();
	while ((j = sql.indexOf('?', i)) != -1)
	{
		sb.append(sql.substring(i, ++j));
		int k;
		if ((k = sql.indexOf('?', j)) == -1) throw new ServletException("Invalid statement format " + sql);
		StringTokenizer st = new StringTokenizer(sql.substring(j, k), ",");
		if (!st.hasMoreTokens()) throw new ServletException("Invalid statement format " + sql);
		String name = st.nextToken().trim();
		if (!st.hasMoreTokens()) throw new ServletException("Invalid statement format " + sql);
		String type = st.nextToken().trim();
		SqlConversion conversion = (SqlConversion)conversions.get(type);
		if (conversion == null) throw new ServletException("Unknown type " + type);
		boolean hadMoreTokens = st.hasMoreTokens();
		String defaultValue = hadMoreTokens ? st.nextToken().trim() : null;
		if (defaultValue != null && defaultValue.equals("")) defaultValue = null;
		parameters.addElement(new SqlParameter(name, conversion, defaultValue, hadMoreTokens && st.hasMoreTokens() ? st.nextToken() : null));
		i = k + 1;
	}
	statement = sb.append(sql.substring(i)).toString();
}

}

public class SqlBean
{
    static HashMap<ServletConfig,SqlBean> sqlBeans = new HashMap<ServletConfig,SqlBean>();
    static HashMap<String,Boolean> databases = new HashMap<String,Boolean>();
    static HashMap<String,SqlStatement> statements = new HashMap<String,SqlStatement>();

	Connection connection = null;
    HashMap<String,Vector<String>> customLists = new HashMap<String,Vector<String>>();
    HashMap<String,SqlResult> results = new HashMap<String,SqlResult>();
	HttpServletRequest req;
	Properties info = new Properties();
	ServletConfig servletConfig = null;
	ServletContext servletContext = null;
	SqlBean sqlBean = null;
	String action;
	String database;
	String dbInitFilename;
	String url;
	String view = "";
	boolean returnEmpty = false;
	boolean update;
	boolean verbose = false;
	int loopCount = 0;
	int maxRows = -1;

public void setAction(String action)
{
	this.action = action;
}

public String getAction()
{
	return action;
}

public void setCustomList(String name, String listValues)
{
	Vector<String> list = new Vector<String>();
	StringTokenizer st = new StringTokenizer(listValues, ",");
	while (st.hasMoreTokens()) list.addElement(st.nextToken().trim());
	customLists.put(name, list);
}

public String getCustomList(String name, int index)
{
	Vector list = (Vector)customLists.get(name);
	return list != null && index < list.size() ? (String)list.elementAt(index) :
	sqlBean != null ? sqlBean.getCustomList(name, index) : null;
}

public void setDatabase(String database)
{
	this.database = database;
}

public String getDatabase()
{
	return database != null ? database : sqlBean != null ? sqlBean.getDatabase() : null;
}

public void setDbInitFilename(String dbInitFilename)
{
	this.dbInitFilename = dbInitFilename;
}

public String getDbInitFilename()
{
	return dbInitFilename != null ? dbInitFilename : sqlBean != null ? sqlBean.getDbInitFilename() : null;
}

public void setDriverClasses(String driverClasses) throws ClassNotFoundException, InstantiationException, IllegalAccessException
{
	StringTokenizer st = new StringTokenizer(driverClasses, ",");
	while (st.hasMoreTokens()) Class.forName(st.nextToken().trim()).newInstance();
}

public void setInfo(String name, String value)
{
	info.put(name, value);
}

public String getInfo(String name)
{
	String value = (String)info.get(name);
	return value != null ? value : sqlBean != null ? sqlBean.getInfo(name) : null;
}

public void setLoopCount(int loopCount)
{
	this.loopCount = loopCount;
}

public int getLoopCount()
{
	return loopCount;
}

public void setMaxRows(int maxRows)
{
	this.maxRows = maxRows;
}

public int getMaxRows()
{
	return maxRows;
}

public void setReturnEmpty(boolean returnEmpty)
{
	this.returnEmpty = returnEmpty;
}

public boolean getReturnEmpty()
{
	return returnEmpty;
}

public synchronized void setStatement(String name, String sql) throws ServletException
{
	statements.put(name, new SqlStatement(sql));
}

public void setUrl(String url)
{
	this.url = url;
}

public String getUrl()
{
	return url != null ? url : sqlBean != null ? sqlBean.getUrl() : null;
}

public void setVerbose(boolean verbose)
{
	this.verbose = verbose;
	if (verbose) DriverManager.setLogWriter(new PrintWriter(new OutputStreamWriter(System.out)));
}

public boolean getVerbose()
{
	return verbose;
}

long year()
{
	Calendar cal = Calendar.getInstance();
	cal.setTime(new java.util.Date());
	cal.set(cal.DAY_OF_MONTH, 1);
	cal.set(cal.MONTH, cal.JANUARY);
	return cal.getTime().getTime();
}

void setParameters(PreparedStatement preparedStatement, Vector parameters) throws ServletException, SQLException
{
	for (int i = parameters.size(); i > 0; i--)
	{
		SqlParameter parameter = (SqlParameter)parameters.elementAt(i - 1);
		if (parameter.checkStatement != null)
		{
			SqlResult sqlResult = getResults(parameter.checkStatement, 0);
			if (sqlResult.result == null) throw new ServletException(parameter.name + " must have already entry");
		}
		String values[] = req.getParameterValues(parameter.name), value;
		if (values == null || values.length == 0 || values[0].equals(""))
		if (parameter.defaultValue == null)
		{
			preparedStatement.setNull(i, parameter.conversion.getType());
			if (verbose) servletContext.log("Setting parameter " + i + " to null");
			continue;
		}
		else value = parameter.defaultValue;
		else value = values[0];
		if (verbose) servletContext.log("Setting parameter " + i + " to " + value);
		switch (parameter.conversion.getType()) {
		case Types.BIT:
			preparedStatement.setBoolean(i, new Boolean(value).booleanValue());
			break;
		case Types.TINYINT:
			preparedStatement.setByte(i, new Byte(value).byteValue());
			break;
		case Types.SMALLINT:
			preparedStatement.setShort(i, new Short(value).shortValue());
			break;
		case Types.INTEGER:
			preparedStatement.setInt(i, new Integer(value).intValue());
			break;
		case Types.BIGINT:
			preparedStatement.setLong(i, new Long(value).longValue());
			break;
		case Types.FLOAT:
		case Types.REAL:
			preparedStatement.setFloat(i, new Float(value).floatValue());
			break;
		case Types.DOUBLE:
			preparedStatement.setDouble(i, new Double(value).doubleValue());
			break;
		case Types.NUMERIC:
		case Types.DECIMAL:
			preparedStatement.setBigDecimal(i, new BigDecimal(value));
			break;
		case Types.CHAR:
		case Types.VARCHAR:
		case Types.LONGVARCHAR:
			if (values.length > 1)
			{
				StringBuffer sb = new StringBuffer();
				for (int j = 0; j < values.length; j++)
				{
					if (sb.length() > 0) sb.append(',');
					sb.append(values[j]);
				}
				preparedStatement.setString(i, sb.toString());
			}
			else preparedStatement.setString(i, value);
			break;
		case Types.DATE:
			preparedStatement.setDate(i, value.equals("") ? new java.sql.Date(0L) :
			value.equalsIgnoreCase("$now") ? new java.sql.Date(System.currentTimeMillis()) :
			value.equalsIgnoreCase("$year") ? new java.sql.Date(year()) :
			java.sql.Date.valueOf(value));
			break;
		case Types.TIME:
			preparedStatement.setTime(i, value.equals("") ? new java.sql.Time(0L) :
			value.equalsIgnoreCase("$now") ? new java.sql.Time(System.currentTimeMillis()) :
			value.equalsIgnoreCase("$year") ? new java.sql.Time(year()) :
			java.sql.Time.valueOf(value));
			break;
		case Types.TIMESTAMP:
			preparedStatement.setTimestamp(i, value.equals("") ? new java.sql.Timestamp(0L) :
			value.equalsIgnoreCase("$now") ? new java.sql.Timestamp(System.currentTimeMillis()) :
			value.equalsIgnoreCase("$year") ? new java.sql.Timestamp(year()) :
			java.sql.Timestamp.valueOf(value.indexOf('.') != -1 ? value : value + ".000000000"));
			break;
		case Types.BINARY:
		case Types.VARBINARY:
		case Types.LONGVARBINARY:
			preparedStatement.setBytes(i, value.getBytes());
			break;
		case Types.NULL:
			preparedStatement.setNull(i, Types.NULL);
			break;
		}
	}
}

SqlResult executeStatement(String statement, SqlResult sqlResult) throws ServletException, SQLException
{
	SqlStatement sqlStatement = (SqlStatement)statements.get(statement);
	if (sqlStatement == null && (sqlStatement = sqlBean != null ? (SqlStatement)sqlBean.statements.get(statement) : null) == null)
	throw new ServletException("Unknown statement " + statement);
	int i = 0, j;
	StringBuffer sb = new StringBuffer();
	while ((j = sqlStatement.statement.indexOf('{', i)) != -1)
	try
	{
		sb.append(sqlStatement.statement.substring(i, j++));
		int k;
		if ((k = sqlStatement.statement.indexOf('}', j)) == -1) throw new ServletException("Invalid statement format " + sqlStatement.statement);
		StringTokenizer st = new StringTokenizer(sqlStatement.statement.substring(j, k), ",");
		String name = st.nextToken().trim(), columnName = st.nextToken().trim();
		if ((sqlResult = (SqlResult)results.get(name)) == null) throw new ServletException("No results for " + name);
		sb.append("'").append(Support.replace((Character.isDigit(columnName.charAt(0)) ? sqlResult.getColumns(Integer.parseInt(columnName)) : sqlResult.getColumnByName(columnName)).toString(), "'", "''")).append("'");
		i = k + 1;
	}
	catch (SQLException ex)
	{
		if (verbose) servletContext.log(ex.toString(), ex);
		if (sqlResult != null) sqlResult.result = null;
		return null;
	}
	String sql = sb.append(sqlStatement.statement.substring(i)).toString();
	if (verbose) servletContext.log("Executing statement " + sql);
	sqlResult = new SqlResult();
	if (sql.startsWith("call "))
	    sqlResult.statement = connection.prepareCall(sql);
	else sqlResult.statement = connection.prepareStatement(sql);
	if (maxRows > -1) sqlResult.statement.setMaxRows(maxRows);
	setParameters(sqlResult.statement, sqlStatement.parameters);
	try
	{
		if (sqlStatement.statement.regionMatches(true, 0, "select", 0, "select".length()))
		{
			sqlResult.result = sqlResult.statement.executeQuery();
			ResultSetMetaData metaData = ((ResultSet)sqlResult.result).getMetaData();
			sqlResult.columnCount = metaData.getColumnCount();
		}
		else sqlResult.result = String.valueOf(sqlResult.statement.executeUpdate());
	}
	catch (SQLException ex)
	{
		try
		{
			sqlResult.statement.close();
		}
		catch (AbstractMethodError er) {}
		catch (UnsupportedOperationException ex1) {}
		catch (SQLException ex1) {}
		throw ex;
	}
	return sqlResult;
}

public SqlResult getResults(String statement, int rowIndex) throws ServletException, SQLException
{
	if (connection == null) return rowIndex == 0 && returnEmpty ? new SqlResult() : null;
	if (verbose) servletContext.log("Getting results of " + statement);
	String resultName;
	int x = statement.lastIndexOf('/');
	if (x != -1)
	{
		resultName = statement.substring(x + 1).trim();
		statement = statement.substring(0, x).trim();
	}
	else resultName = statement;
	SqlResult sqlResult;
	if (resultName.equals(""))
	{
// Refresh results
		sqlResult = null;
		resultName = statement;
	}
	else sqlResult = (SqlResult)results.get(resultName);
	if (sqlResult == null || rowIndex == 0)
	{
		sqlResult = executeStatement(statement, sqlResult);
		results.put(resultName, sqlResult);
	}
	else
	{
		if (sqlResult.result == null) return null;
		if (rowIndex == -1) return sqlResult;
	}
	if (sqlResult.result instanceof ResultSet)
	if (!((ResultSet)sqlResult.result).next())
	{
		try
		{
			if (sqlResult.statement != null)
			{
				sqlResult.statement.close();
				sqlResult.statement = null;
			}
		}
		catch (AbstractMethodError er) {}
		catch (UnsupportedOperationException ex) {}
		catch (SQLException ex) {}
		finally
		{
			try
			{
				((ResultSet)sqlResult.result).close();
			}
			catch (AbstractMethodError er) {}
			catch (UnsupportedOperationException ex) {}
			catch (SQLException ex) {}
			sqlResult.result = null;
			results.remove(resultName);
			sqlResult = null;
		}
		if (rowIndex == 0 && returnEmpty) sqlResult = new SqlResult();
	}
	if (verbose) servletContext.log("Returning result " + sqlResult + " with row " + rowIndex);
	return sqlResult;
}

public SqlResult getResult(String statement, int columnIndex) throws ServletException, SQLException
{
	return getResults(statement, columnIndex == 0 ? 0 : -1);
}

public String getStringResults(String statement, int columnIndex) throws ServletException, SQLException
{
	SqlResult sqlResult = getResult(statement, columnIndex);
	return sqlResult != null ? sqlResult.getStringColumns(columnIndex == -1 ? 0 : columnIndex) : null;
}

public String getBooleanResults(String statement, int columnIndex) throws ServletException, SQLException
{
	SqlResult sqlResult = getResult(statement, columnIndex);
	return sqlResult != null ? sqlResult.getBooleanColumns(columnIndex == -1 ? 0 : columnIndex) : null;
}

public String getByteResults(String statement, int columnIndex) throws ServletException, SQLException
{
	SqlResult sqlResult = getResult(statement, columnIndex);
	return sqlResult != null ? sqlResult.getByteColumns(columnIndex == -1 ? 0 : columnIndex) : null;
}

public String getShortResults(String statement, int columnIndex) throws ServletException, SQLException
{
	SqlResult sqlResult = getResult(statement, columnIndex);
	return sqlResult != null ? sqlResult.getShortColumns(columnIndex == -1 ? 0 : columnIndex) : null;
}

public String getIntResults(String statement, int columnIndex) throws ServletException, SQLException
{
	SqlResult sqlResult = getResult(statement, columnIndex);
	return sqlResult != null ? sqlResult.getIntColumns(columnIndex == -1 ? 0 : columnIndex) : null;
}

public String getLongResults(String statement, int columnIndex) throws ServletException, SQLException
{
	SqlResult sqlResult = getResult(statement, columnIndex);
	return sqlResult != null ? sqlResult.getLongColumns(columnIndex == -1 ? 0 : columnIndex) : null;
}

public String getFloatResults(String statement, int columnIndex) throws ServletException, SQLException
{
	SqlResult sqlResult = getResult(statement, columnIndex);
	return sqlResult != null ? sqlResult.getFloatColumns(columnIndex == -1 ? 0 : columnIndex) : null;
}

public String getDoubleResults(String statement, int columnIndex) throws ServletException, SQLException
{
	SqlResult sqlResult = getResult(statement, columnIndex);
	return sqlResult != null ? sqlResult.getDoubleColumns(columnIndex == -1 ? 0 : columnIndex) : null;
}

public String getBigDecimalResults(String statement, int columnIndex) throws ServletException, SQLException
{
	SqlResult sqlResult = getResult(statement, columnIndex);
	return sqlResult != null ? sqlResult.getBigDecimalColumns(columnIndex == -1 ? 0 : columnIndex) : null;
}

public String getBytesResults(String statement, int columnIndex) throws ServletException, SQLException
{
	SqlResult sqlResult = getResult(statement, columnIndex);
	return sqlResult != null ? sqlResult.getByteColumns(columnIndex == -1 ? 0 : columnIndex) : null;
}

public String getDateResults(String statement, int columnIndex) throws ServletException, SQLException
{
	SqlResult sqlResult = getResult(statement, columnIndex);
	return sqlResult != null ? sqlResult.getDateColumns(columnIndex == -1 ? 0 : columnIndex) : null;
}

public String getTimeResults(String statement, int columnIndex) throws ServletException, SQLException
{
	SqlResult sqlResult = getResult(statement, columnIndex);
	return sqlResult != null ? sqlResult.getTimeColumns(columnIndex == -1 ? 0 : columnIndex) : null;
}

public String getTimestampResults(String statement, int columnIndex) throws ServletException, SQLException
{
	SqlResult sqlResult = getResult(statement, columnIndex);
	return sqlResult != null ? sqlResult.getTimestampColumns(columnIndex == -1 ? 0 : columnIndex) : null;
}

public String getColumnResults(String statement, int columnIndex) throws ServletException, SQLException
{
	SqlResult sqlResult = getResult(statement, columnIndex);
	return sqlResult != null ? sqlResult.getColumns(columnIndex == -1 ? 0 : columnIndex) : null;
}

public String getStringResult(String statement) throws ServletException, SQLException
{
	return getStringResults(statement, -1);
}

public String getBooleanResult(String statement) throws ServletException, SQLException
{
	return getBooleanResults(statement, -1);
}

public String getByteResult(String statement) throws ServletException, SQLException
{
	return getByteResults(statement, -1);
}

public String getShortResult(String statement) throws ServletException, SQLException
{
	return getShortResults(statement, -1);
}

public String getIntResult(String statement) throws ServletException, SQLException
{
	return getIntResults(statement, -1);
}

public String getLongResult(String statement) throws ServletException, SQLException
{
	return getLongResults(statement, -1);
}

public String getFloatResult(String statement) throws ServletException, SQLException
{
	return getFloatResults(statement, -1);
}

public String getDoubleResult(String statement) throws ServletException, SQLException
{
	return getDoubleResults(statement, -1);
}

public String getBigDecimalResult(String statement) throws ServletException, SQLException
{
	return getBigDecimalResults(statement, -1);
}

public String getBytesResult(String statement) throws ServletException, SQLException
{
	return getBytesResults(statement, -1);
}

public String getDateResult(String statement) throws ServletException, SQLException
{
	return getDateResults(statement, -1);
}

public String getTimeResult(String statement) throws ServletException, SQLException
{
	return getTimeResults(statement, -1);
}

public String getTimestampResult(String statement) throws ServletException, SQLException
{
	return getTimestampResults(statement, -1);
}

public String getColumnResult(String statement) throws ServletException, SQLException
{
	return getColumnResults(statement, -1);
}

public void setView(String view)
{
	this.view = view;
}

public String getView()
{
	return view;
}

public void setServlet(String property, String value) throws ServletException, ClassNotFoundException, InstantiationException, IllegalAccessException
{
	StringTokenizer st = new StringTokenizer(property, ":");
	if (!st.hasMoreTokens() || !st.nextToken().equalsIgnoreCase("sql")) return;
	value = Support.decode(value);
	if (verbose) servletContext.log("setServlet " + property + " to " + value);
	String name = st.nextToken().trim();
	if (name.equals("customList")) setCustomList(st.nextToken(), value);
	else if (name.equals("database")) setDatabase(value);
	else if (name.equals("dbInitFilename")) setDbInitFilename(value);
	else if (name.equals("driverClasses")) setDriverClasses(value);
	else if (name.equals("info")) setInfo(st.nextToken(), value);
	else if (name.equals("statement")) setStatement(st.nextToken(), value);
	else if (name.equals("url")) setUrl(value);
	else throw new ServletException("Unknown sql bean servlet parameter " + name);
}

public void setServletConfig(ServletConfig servletConfig) throws ServletException, ClassNotFoundException, ParseException, InstantiationException, IllegalAccessException
{
	if (servletConfig == null) return;
	servletContext = servletConfig.getServletContext();
	if (Support.isTrue(servletConfig.getInitParameter("verbose"), "verbose") ||
	servletContext.getAttribute("FI.realitymodeler.server.W3Context/verbose") == Boolean.TRUE) setVerbose(true);
	if ((sqlBean = (SqlBean)sqlBeans.get(servletConfig)) != null) return;
	sqlBean = new SqlBean();
	sqlBean.servletContext = servletContext;
	if (getVerbose()) sqlBean.setVerbose(true);
	Enumeration initParameters = servletConfig.getInitParameterNames();
	while (initParameters.hasMoreElements())
	{
		String name = (String)initParameters.nextElement();
		sqlBean.setServlet(name, servletConfig.getInitParameter(name));
	}
	sqlBeans.put(servletConfig, sqlBean);
}

public void processRequest(HttpServletRequest req) throws IOException, ServletException, SQLException, ParseException
{
	if (!req.getParameterNames().hasMoreElements()) return;
	this.req = req;
	setAction(req.getParameter("action"));
	String s = req.getParameter("view");
	if (s != null) setView(s);
	Properties info = this.info.isEmpty() && sqlBean != null ? sqlBean.info : this.info;
	if (verbose) servletContext.log("Getting connection to " + getUrl() + " with info " + info);
	connection = DriverManager.getConnection(getUrl(), info);
	String database = req.getParameter("database");
	if (database == null && (database = getDatabase()) == null) throw new ServletException("Database not specified");
	if (databases.containsKey(database)) return;
	databases.put(database, Boolean.TRUE);
	Reader reader = null;
	try
	{
		reader = new BufferedReader(new FileReader(getDbInitFilename()));
		Support.initializeDatabase(connection, reader, getDbInitFilename(), servletContext, null, verbose);
	}
	finally
	{
		if (reader != null) reader.close();
	}
}

public void close()
{
	try
	{
		Iterator iter = results.values().iterator();
		while (iter.hasNext())
		{
			SqlResult sqlResult = (SqlResult)iter.next();
			try
			{
				if (sqlResult.statement != null) sqlResult.statement.close();
			}
			catch (AbstractMethodError er) {}
			catch (UnsupportedOperationException ex) {}
			catch (SQLException ex) {}
			finally
			{
				try
				{
					if (sqlResult.result instanceof ResultSet) ((ResultSet)sqlResult.result).close();
				}
				catch (AbstractMethodError er) {}
				catch (UnsupportedOperationException ex) {}
				catch (SQLException ex) {}
			}
		}
	}
	finally
	{
		if (connection != null)
		try
		{
			connection.close();
		}
		catch (SQLException ex) {}
	}
}

public static void main(String argv[]) throws Exception
{
	Class.forName("jdbc.odbc.JdbcOdbcDriver").newInstance();
	Enumeration drivers = DriverManager.getDrivers();
	while (drivers.hasMoreElements()) System.out.println(drivers.nextElement());
}

}
