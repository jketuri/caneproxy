
package FI.realitymodeler.sqlbean;

import java.sql.*;

public class SqlResult
{
    Object result = null;
    PreparedStatement statement = null;
    int columnCount = 0;

public String toString()
{
	return result != null ? result.toString() : "<empty>";
}

final String toString(Object object)
{
	return object != null ? object.toString() : "";
}

public String getStringByName(String columnName) throws SQLException
{
	return result instanceof ResultSet ? ((ResultSet)result).getString(columnName) : null;
}

public String getBooleanByName(String columnName) throws SQLException
{
	return result instanceof ResultSet ? String.valueOf(((ResultSet)result).getBoolean(columnName)) : null;
}

public String getByteByName(String columnName) throws SQLException
{
	return result instanceof ResultSet ? String.valueOf(((ResultSet)result).getByte(columnName)) : null;
}

public String getShortByName(String columnName) throws SQLException
{
	return result instanceof ResultSet ? String.valueOf(((ResultSet)result).getShort(columnName)) : null;
}

public String getIntByName(String columnName) throws SQLException
{
	return result instanceof ResultSet ? String.valueOf(((ResultSet)result).getInt(columnName)) : columnName.equals("") ? (String)result : null;
}

public String getLongByName(String columnName) throws SQLException
{
	return result instanceof ResultSet ? String.valueOf(((ResultSet)result).getLong(columnName)) : null;
}

public String getFloatByName(String columnName) throws SQLException
{
	return result instanceof ResultSet ? String.valueOf(((ResultSet)result).getFloat(columnName)) : null;
}

public String getDoubleByName(String columnName) throws SQLException
{
	return result instanceof ResultSet ? String.valueOf(((ResultSet)result).getDouble(columnName)) : null;
}

public String getBigDecimalByName(String columnName) throws SQLException
{
	return result instanceof ResultSet ? String.valueOf(((ResultSet)result).getBigDecimal(columnName)) : null;
}

public String getBytesByName(String columnName) throws SQLException
{
	return result instanceof ResultSet ? new String(((ResultSet)result).getBytes(columnName)) : null;
}

public String getDateByName(String columnName) throws SQLException
{
	return result instanceof ResultSet ? toString(((ResultSet)result).getDate(columnName)) : null;
}

public String getTimeByName(String columnName) throws SQLException
{
	return result instanceof ResultSet ? toString(((ResultSet)result).getTime(columnName)) : null;
}

public String getTimestampByName(String columnName) throws SQLException
{
	return result instanceof ResultSet ? toString(((ResultSet)result).getTimestamp(columnName)) : null;
}

public String getColumnByName(String columnName) throws SQLException
{
	return result instanceof ResultSet ? toString(((ResultSet)result).getObject(columnName)) : columnName.equals("") ? (String)result : null;
}

public String getStringColumns(int columnIndex) throws SQLException
{
	return result instanceof ResultSet && columnIndex < columnCount ? ((ResultSet)result).getString(columnIndex + 1) : null;
}

public String getBooleanColumns(int columnIndex) throws SQLException
{
	return result instanceof ResultSet && columnIndex < columnCount ? String.valueOf(((ResultSet)result).getBoolean(columnIndex + 1)) : null;
}

public String getByteColumns(int columnIndex) throws SQLException
{
	return result instanceof ResultSet && columnIndex < columnCount ? String.valueOf(((ResultSet)result).getByte(columnIndex + 1)) : null;
}

public String getShortColumns(int columnIndex) throws SQLException
{
	return result instanceof ResultSet && columnIndex < columnCount ? String.valueOf(((ResultSet)result).getShort(columnIndex + 1)) : null;
}

public String getIntColumns(int columnIndex) throws SQLException
{
	return result instanceof ResultSet && columnIndex < columnCount ? String.valueOf(((ResultSet)result).getInt(columnIndex + 1)) : columnIndex == 0 ? (String)result : null;
}

public String getLongColumns(int columnIndex) throws SQLException
{
	return result instanceof ResultSet && columnIndex < columnCount ? String.valueOf(((ResultSet)result).getLong(columnIndex + 1)) : null;
}

public String getFloatColumns(int columnIndex) throws SQLException
{
	return result instanceof ResultSet && columnIndex < columnCount ? String.valueOf(((ResultSet)result).getFloat(columnIndex + 1)) : null;
}

public String getDoubleColumns(int columnIndex) throws SQLException
{
	return result instanceof ResultSet && columnIndex < columnCount ? String.valueOf(((ResultSet)result).getDouble(columnIndex + 1)) : null;
}

public String getBigDecimalColumns(int columnIndex) throws SQLException
{
	return result instanceof ResultSet && columnIndex < columnCount ? String.valueOf(((ResultSet)result).getBigDecimal(columnIndex + 1)) : null;
}

public String getBytesColumns(int columnIndex) throws SQLException
{
	return result instanceof ResultSet && columnIndex < columnCount ? new String(((ResultSet)result).getBytes(columnIndex + 1)) : null;
}

public String getDateColumns(int columnIndex) throws SQLException
{
	return result instanceof ResultSet && columnIndex < columnCount ? toString(((ResultSet)result).getDate(columnIndex + 1)) : null;
}

public String getTimeColumns(int columnIndex) throws SQLException
{
	return result instanceof ResultSet && columnIndex < columnCount ? toString(((ResultSet)result).getTime(columnIndex + 1)) : null;
}

public String getTimestampColumns(int columnIndex) throws SQLException
{
	return result instanceof ResultSet && columnIndex < columnCount ? toString(((ResultSet)result).getTimestamp(columnIndex + 1)) : null;
}

public String getColumns(int columnIndex) throws SQLException
{
	return result instanceof ResultSet && columnIndex < columnCount ? toString(((ResultSet)result).getObject(columnIndex + 1)) : columnIndex == 0 ? (String)result : null;
}

}
