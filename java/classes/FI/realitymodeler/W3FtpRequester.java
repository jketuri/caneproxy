
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.util.*;

/** Client for File Transfer Protocol servers. Refer to RFC 959.
 */
public class W3FtpRequester extends W3CopyRequester {
    public static final boolean DEBUG = false;
    public static final int FTP_PORT = 21;

    static final int FTP_SUCCESS = 1;
    static final int FTP_TRY_AGAIN = 2;
    static final int FTP_ERROR = 3;
    static final int DESCRIPTOR_EOR = 128;
    static final int DESCRIPTOR_EOF = 64;
    static final int DESCRIPTOR_SUSPECT = 32;
    static final int DESCRIPTOR_MARKER = 16;

    private boolean active = false;
    private boolean binaryMode = false;
    private boolean blockMode = false;
    private boolean eof = false;
    private boolean replyPending = false;
    private byte marker[] = null;
    private long filePos = 0L;
    private long markerPos = 0L;

    String user = null, password = null, command = null;

    public String welcomeMsg;

    class BlockInputStream extends FilterInputStream {
        int byteCount = 0;

        BlockInputStream(InputStream in) {
            super(in);
        }

        public int read()
            throws IOException {
            for (;;) {
                if (--byteCount >= 0) {
                    int c = in.read();
                    if (c == -1) throw new EOFException();
                    filePos++;
                    return c;
                }
                if (eof) return -1;
                int descriptor = in.read();
                if (descriptor == -1) throw new EOFException();
                int c = in.read();
                if (c == -1) throw new EOFException();
                byteCount = in.read();
                if (byteCount == -1) throw new EOFException();
                byteCount |= c << 8;
                if ((descriptor & DESCRIPTOR_EOF) != 0) eof = true;
                if ((descriptor & DESCRIPTOR_MARKER) != 0) {
                    markerPos = filePos;
                    marker = new byte[byteCount];
                    for (int i = 0; i < marker.length; i++)
                        if ((c = in.read()) == -1) throw new EOFException();
                        else marker[i] = (byte)c;
                    byteCount = 0;
                }
            }
        }

    }

    class BlockOutputStream extends FilterOutputStream {
        byte b[] = new byte[Support.bufferLength];
        int byteCount = 0;

        BlockOutputStream(OutputStream out) {
            super(out);
        }

        public void flush(int descriptor)
            throws IOException {
            out.write(descriptor);
            out.write(byteCount >>> 8);
            out.write(byteCount & 0xff);
            if (byteCount > 0) {
                out.write(b, 0, byteCount);
                byteCount = 0;
            }
            out.flush();
        }

        public void flush()
            throws IOException {
            flush(0);
        }

        public void write(int c)
            throws IOException {
            if (byteCount == b.length) flush(0);
            b[byteCount++] = (byte)c;
        }

        public void close()
            throws IOException {
            flush(DESCRIPTOR_EOF);
            super.close();
        }

    }

    public W3FtpRequester(String host)
        throws IOException {
        open(host);
    }

    public W3FtpRequester(String host, int port)
        throws IOException {
        open(host, port);
    }

    public W3FtpRequester(String host, int port, InetAddress localAddress, int localPort)
        throws IOException {
        open(host, port, localAddress, localPort);
    }

    public W3FtpRequester() {}

    public Object clone() {
        W3FtpRequester ftpRequester = (W3FtpRequester)copy(new W3FtpRequester());
        ftpRequester.welcomeMsg = welcomeMsg;
        ftpRequester.user = user;
        ftpRequester.password = password;
        return ftpRequester;
    }

    public int send(String command, boolean login)
        throws IOException {
        command = command;
        if (replyPending) readReply();
        int reply;
        do {
            super.send(command + "\r\n");
            reply = readReply();
        } while (reply == FTP_TRY_AGAIN);
        if (reply != FTP_SUCCESS) throw login ? new LoginException(command, makeRealm()) : new IOException(command);
        return reply;
    }

    public final void send(String command)
        throws IOException {
        send(command, false);
    }

    @SuppressWarnings("fallthrough")
    protected int readReply()
        throws IOException {
        readResponse();
        replyPending = false;
        switch (lastReplyCode / 100) {
        case 1:
            replyPending = true;
        case 2:
        case 3:
            return FTP_SUCCESS;
        case 5:
            if (lastReplyCode == 550) throw new FileNotFoundException(command + ": " + lastReplyCode + " " + getResponse());
            else if (lastReplyCode == 530 && user == null) throw new LoginException("Not logged in: " + lastReplyCode + " " + getResponse(), makeRealm());
        }
        return FTP_ERROR;
    }

    protected Socket openDataConnection(String command0, String command)
        throws IOException {
        eof = false;
        marker = null;
        if (!active)
            try {
                send("PASV");
                String response = getResponse().trim();
                int x = 0;
                while (x < response.length() && !Character.isDigit(response.charAt(x))) x++;
                int y = response.length();
                while (y > 0 && !Character.isDigit(response.charAt(y - 1))) y--;
                if (y > x) {
                    StringTokenizer st = new StringTokenizer(response.substring(x, y), ",");
                    StringBuffer sb = new StringBuffer();
                    for (int i = 0; i < 4; i++) {
                        if (sb.length() > 0) sb.append('.');
                        sb.append(st.nextToken().trim());
                    }
                    InetAddress address = InetAddress.getByName(sb.toString());
                    int port = (Integer.parseInt(st.nextToken().trim()) << 8) | Integer.parseInt(st.nextToken().trim());
                    if (DEBUG) System.out.println("Using passive mode");
                    Socket socket = null;
                    try {
                        socket = new W3Socket(address, port);
                        if (command0 != null) send(command0);
                        if (command != null) send(command);
                    } catch (IOException ex) {
                        if (socket != null)
                            try {
                                socket.close();
                            } catch (IOException ex1) {}
                        throw ex;
                    }
                    return socket;
                }
            } catch (IOException ex) {
                active = true;
                try {
                    send("ABOR");
                } catch (IOException ex1) {}
                if (DEBUG) System.out.println("falling back to active");
            }
        ServerSocket portSocket = null;
        try {
            portSocket = new W3ServerSocket(0, 1, socket.getLocalAddress());
            byte addr[] = portSocket.getInetAddress().getAddress();
            StringBuffer sb = new StringBuffer("PORT ");
            for (int i = 0; i < addr.length; i++) sb.append(addr[i] & 0xFF).append(',');
            sb.append((portSocket.getLocalPort() >>> 8) & 0xff).append(',').append(portSocket.getLocalPort() & 0xff);
            send(sb.toString());
            if (command0 != null) send(command0);
            if (command != null) send(command);
            return portSocket.accept();
        } finally {
            if (portSocket != null) {
                portSocket.close();
                portSocket = null;
            }
        }
    }

    protected Socket openDataConnection(String command0, String command, String filename)
        throws IOException {
        if (filename == null) return openDataConnection(command0, command);
        try {
            return openDataConnection(command0, command + " " + filename);
        } catch (IOException ex) {
            if (filename.indexOf(' ') == -1 || filename.startsWith("\"")) throw ex;
            return openDataConnection(command0, command + " \"" + filename + "\"");
        }
    }

    public void open(String host, int port, InetAddress localAddress, int localPort)
        throws IOException {
        if (port == -1) port = FTP_PORT;
        super.open(host, port, W3Socket.NORMAL, localAddress, localPort);
        if (readReply() == FTP_ERROR) throw new IOException("Welcome message");
        try {
            send("MODE B");
            blockMode = true;
            if (DEBUG) System.out.println("Using block mode");
        } catch (IOException ex) {}
    }

    public void open(String host, int port)
        throws IOException {
        open(host, port, null, 0);
    }

    public void open(String host)
        throws IOException {
        open(host, FTP_PORT);
    }

    public void close()
        throws IOException {
        if (isOpen())
            try {
                super.send("quit\r\n");
            } finally {
                super.close();
            }
    }

    public void login(String user, String password)
        throws IOException {
        if (!isOpen()) throw new IOException("Not connected to host");
        this.user = user;
        this.password = password;
        if (user != null) send("USER " + user, true);
        if (password != null) send("PASS " + password, true);
        welcomeMsg = getResponse();
    }

    public InputStream get(String filename, String command0)
        throws IOException {
        InputStream in = openDataConnection(command0, "RETR", filename).getInputStream();
        if (blockMode) in = new BlockInputStream(in);
        replyPending = true;
        return new BufferedInputStream(binaryMode ? in : Support.getTelnetInputStream(in));
    }

    public InputStream get(String filename)
        throws IOException {
        return get(filename, null);
    }

    public InputStream get(String filename, long filePos)
        throws IOException {
        return get(filename, "REST " + filePos);
    }

    public OutputStream put(String filename, String command0)
        throws IOException {
        OutputStream out = openDataConnection(command0, "STOR", filename).getOutputStream();
        if (blockMode) out = new BlockOutputStream(out);
        replyPending = true;
        return new BufferedOutputStream(binaryMode ? out : Support.getTelnetOutputStream(out));
    }

    public OutputStream put(String filename)
        throws IOException {
        return put(filename, null);
    }

    public OutputStream put(String filename, long filePos)
        throws IOException {
        return put(filename, "REST " + filePos);
    }

    public InputStream list()
        throws IOException {
        return list(null);
    }

    public InputStream list(String filename)
        throws IOException {
        InputStream in = openDataConnection(null, "LIST", filename).getInputStream();
        replyPending = true;
        return new BufferedInputStream(Support.getTelnetInputStream(in));
    }

    public void cd(String remoteDirectory)
        throws IOException {
        send("CWD " + remoteDirectory);
    }

    public String pwd()
        throws IOException {
        send("PWD");
        return getResponse();
    }

    public void binary()
        throws IOException {
        send("TYPE I");
        binaryMode = true;
    }

    public void text()
        throws IOException {
        send("TYPE A");
        binaryMode = false;
    }

    public void delete(String filename)
        throws IOException {
        send("DELE " + filename);
    }

    public void rename(String fromFilename, String toFilename)
        throws IOException {
        send("RNFR " + fromFilename);
        send("FNTO " + toFilename);
    }

    public long restart()
        throws IOException {
        if (marker == null) return -1;
        send("REST " + new String(marker));
        return filePos = markerPos;
    }

    public boolean hasBlockMode() {
        return blockMode;
    }

}
