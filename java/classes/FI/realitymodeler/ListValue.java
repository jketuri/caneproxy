
package FI.realitymodeler;

import java.util.*;

public class ListValue {
    public String name;
    public Map<String, String> params;
    public float qualityValue;

    ListValue(String name, Map<String, String> params, float qualityValue) {
        this.name = name;
        this.params = params;
        this.qualityValue = qualityValue;
    }

    public boolean equals(Object obj) {
        return name.equals(((ListValue)obj).name);
    }

    public String toString() {
        return name + "=" + params;
    }

}
