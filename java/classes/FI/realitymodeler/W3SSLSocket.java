
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.util.zip.*;
import javax.net.ssl.*;

public class W3SSLSocket extends W3Socket {

    protected W3SSLSocket() throws SocketException {
        W3SocketImpl impl = getSocketImpl();
        impl.secure = true;
        type = SECURE;
    }

    protected W3SSLSocket(SocketImpl impl) throws SocketException {
        super(impl);
        ((W3SocketImpl)impl).secure = true;
        type = SECURE;
    }

    public W3SSLSocket(String host, int port) throws UnknownHostException, IOException {
        super(InetAddress.getByName(host), port, null, 0, true, SECURE);
    }

    public W3SSLSocket(InetAddress address, int port) throws IOException {
        super(address, port, null, 0, true, SECURE);
    }

    public W3SSLSocket(String host, int port, InetAddress localAddress, int localPort) throws IOException {
        super(InetAddress.getByName(host), port, localAddress, localPort, true, SECURE);
    }

    public W3SSLSocket(InetAddress address, int port, InetAddress localAddress, int localPort) throws IOException {
        super(address, port, localAddress, localPort, true, SECURE);
    }

    public W3SSLSocket(String host, int port, boolean stream) throws IOException {
        super(InetAddress.getByName(host), port, null, 0, stream, SECURE);
    }

    public W3SSLSocket(InetAddress address, int port, boolean stream) throws IOException {
        super(address, port, null, 0, stream, SECURE);
    }

    public W3SSLSocket(String host, int port, int type) throws UnknownHostException, IOException {
        super(InetAddress.getByName(host), port, null, 0, true, type);
    }

    public W3SSLSocket(InetAddress address, int port, int type) throws IOException {
        super(address, port, null, 0, true, type);
    }

    public W3SSLSocket(String host, int port, InetAddress localAddress, int localPort, int type) throws IOException {
        super(InetAddress.getByName(host), port, localAddress, localPort, true, type);
    }

    public W3SSLSocket(InetAddress address, int port, InetAddress localAddress, int localPort, int type) throws IOException {
        super(address, port, localAddress, localPort, true, type);
    }

    public W3SSLSocket(String host, int port, boolean stream, int type) throws IOException {
        super(InetAddress.getByName(host), port, null, 0, stream, type);
    }

    public W3SSLSocket(InetAddress address, int port, boolean stream, int type) throws IOException {
        super(address, port, null, 0, stream, type);
    }

    public W3SSLSocket(Socket socket) throws IOException {
        super(null);
        type = SECURE;
    }

    public String[] getSupportedCipherSuites() {
        return null;
    }

    public String[] getEnabledCipherSuites() {
        return null;
    }

    public void setEnabledCipherSuites(String suites[]) {
    }

    public String[] getSupportedProtocols() {
        return null;
    }

    public String[] getEnabledProtocols() {
        return null;
    }

    public void setEnabledProtocols(String protocols[]) {
    }

    public SSLSession getSession() {
        return null;
    }

    public void addHandshakeCompletedListener(HandshakeCompletedListener listener) {
    }

    public void removeHandshakeCompletedListener(HandshakeCompletedListener listener) {
    }

    public void startHandshake() {
    }

    public void setUseClientMode(boolean mode) {
    }

    public boolean getUseClientMode() {
        return false;
    }

    public void setNeedClientAuth(boolean need) {
    }

    public boolean getNeedClientAuth() {
        return false;
    }

    public void setWantClientAuth(boolean want) {
    }

    public boolean getWantClientAuth() {
        return false;
    }

    public void setEnableSessionCreate(boolean flag) {
    }

    public boolean getEnableSessionCreate() {
        return false;
    }

}
