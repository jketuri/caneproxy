
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;

public abstract class W3SocketImpl extends SocketImpl
{
    boolean secure = false;
    byte addr[] = null;
    byte localAddr[] = null;

    public W3SocketImpl()
    {
    }

    public abstract void create(boolean stream)
        throws IOException;

    public abstract void create(boolean stream, int af)
        throws IOException;

    public abstract void connect(String host, int port)
        throws IOException;

    public abstract void connect(InetAddress address, int port)
        throws IOException;

    public abstract void connect(SocketAddress address, int timeout)
        throws IOException;

    public abstract void connect(byte addr[], int scopeId, int port)
        throws IOException;

    public abstract void connect(byte addr[], int scopeId, int port, int timeout)
        throws IOException;

    public abstract void bind(InetAddress address, int port)
        throws IOException;

    public abstract void bind(byte addr[], int scopeId, int port)
        throws IOException;

    public abstract void listen(int backlog)
        throws IOException;

    public abstract void accept(SocketImpl socketImpl)
        throws IOException;

    public abstract InputStream getInputStream()
        throws IOException;

    public abstract OutputStream getOutputStream()
        throws IOException;

    public abstract int read(byte buf[], int off, int len)
        throws IOException;

    public abstract void write(byte buf[], int off, int len)
        throws IOException;

    public abstract void receive(MultiDatagramPacket pack)
        throws IOException;

    public abstract void send(MultiDatagramPacket pack)
        throws IOException;

    public abstract int available()
        throws IOException;

    public abstract void flush()
        throws IOException;

    public abstract void close()
        throws IOException;

    public abstract void shutdownInput()
        throws SocketException;

    public abstract void shutdownOutput()
        throws SocketException;

    public FileDescriptor getFileDescriptor() {
        return fd;
    }

    public InetAddress getInetAddress() {
        return address;
    }

    public byte[] getAddress() {
        return addr;
    }

    public byte[] getLocalByteAddress() {
        return localAddr;
    }

    public int getPort() {
        return port;
    }

    public boolean supportsUrgentData() {
        return super.supportsUrgentData();
    }

    public abstract void sendUrgentData(int data)
        throws IOException;

    public int getLocalPort() {
        return localport;
    }

    public String toString() {
        return "W3SocketImpl[address="+ Support.toHexString(getAddress()) + ", port=" + getPort() + ", localport=" + getLocalPort() + "]";
    }

    public abstract void setOption(int optID, Object value)
        throws SocketException;

    public abstract Object getOption(int optID)
        throws SocketException;

    public void setIOControl(int code, int value)
        throws SocketException {
    }

    public int getIOControl(int code)
        throws SocketException {
        return 0;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setLocalPort(int localport) {
        this.localport = localport;
    }

    public void setAddress(InetAddress address) {
        this.address = address;
    }

    public abstract boolean isClosed();

    public abstract boolean isConnected();

}
