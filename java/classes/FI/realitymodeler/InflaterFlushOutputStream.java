
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import java.io.*;
import java.util.zip.*;

public class InflaterFlushOutputStream extends FilterOutputStream {
    Inflater inf;
    byte result[] = new byte[Support.bufferLength];

    public InflaterFlushOutputStream(OutputStream out, Inflater inf) throws IOException {
        super(out);
        this.inf = inf;
    }

    public void write(byte b[], int off, int len) throws IOException {
        try {
            inf.setInput(b, off, len);
            int n = inf.inflate(result);
            write(result, 0, n);
            for (;;) {
                n = inf.getRemaining();
                inf.reset();
                if (n <= 0) break;
                inf.setInput(b, off + len - n, n);
                n = inf.inflate(result);
                write(result, 0, n);
            }
        } catch (DataFormatException ex) {
            throw new IOException(Support.stackTrace(ex));
        }
    }

    public void close() throws IOException {
        out.close();
        inf.end();
    }

}
