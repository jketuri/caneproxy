
package FI.realitymodeler;

import java.io.*;
import java.net.*;

public class W3SocketOutputStream extends OutputStream {
    private W3SocketImpl impl;
    private byte temp[] = new byte[1];

    W3SocketOutputStream(W3SocketImpl socketImpl)
        throws IOException {
        impl = socketImpl;
    }

    public final void write(int b)
        throws IOException {
        if (impl.isClosed()) throw new SocketException("Socket is closed");
        temp[0] = (byte)b;
        impl.write(temp, 0, 1);
    }

    public final void write(byte buf[])
        throws IOException {
        write(buf, 0, buf.length);
    }

    public final void write(byte buf[], int off, int len)
        throws IOException {
        if (impl.isClosed()) throw new SocketException("Socket is closed");
        impl.write(buf, off, len);
    }

    public final void flush()
        throws IOException {
        if (impl.isClosed()) throw new SocketException("Socket is closed");
        impl.flush();
    }

    public final void close()
        throws IOException {
        impl.shutdownOutput();
    }

}
