
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import java.io.*;
import java.lang.reflect.*;
import java.net.*;

/** Multi socket implementation which can use any of WinSock2 Service Providers.
 */
public class MultiSocketImpl extends W3SocketImpl {
    static Field fdField, fd0Field, localportField;
    static {
        try {
            Support.loadLibrary("FI_realitymodeler");
            MultiSocketImpl.initialize();
            localportField = SocketImpl.class.getDeclaredField("localport");
            localportField.setAccessible(true);
            fdField = SocketImpl.class.getDeclaredField("fd");
            fdField.setAccessible(true);
            fd0Field = FileDescriptor.class.getDeclaredField("fd");
            fd0Field.setAccessible(true);
        } catch (Exception ex) {
            throw new Error(ex.toString());
        }
    }

    public static final int AF_UNSPEC = 0;		/* unspecified */
    public static final int AF_UNIX = 1;		/* local to host (pipes, portals) */
    public static final int AF_INET = 2;		/* internetwork: UDP, TCP, etc. */
    public static final int AF_IMPLINK = 3;		/* arpanet imp addresses */
    public static final int AF_PUP = 4;			/* pup protocols: e.g. BSP */
    public static final int AF_CHAOS = 5;		/* mit CHAOS protocols */
    public static final int AF_NS = 6;			/* XEROX NS protocols */
    public static final int AF_IPX = AF_NS;		/* IPX protocols: IPX, SPX, etc. */
    public static final int AF_ISO = 7;			/* ISO protocols */
    public static final int AF_OSI = AF_ISO;	/* OSI is ISO */
    public static final int AF_ECMA = 8;		/* european computer manufacturers */
    public static final int AF_DATAKIT = 9;		/* datakit protocols */
    public static final int AF_CCITT = 10;		/* CCITT protocols, X.25 etc */
    public static final int AF_SNA = 11;		/* IBM SNA */
    public static final int AF_DECnet = 12;		/* DECnet */
    public static final int AF_DLI = 13;		/* Direct data link interface */
    public static final int AF_LAT = 14;		/* LAT */
    public static final int AF_HYLINK = 15;		/* NSC Hyperchannel */
    public static final int AF_APPLETALK = 16;	/* AppleTalk */
    public static final int AF_NETBIOS = 17;	/* NetBios-style addresses */
    public static final int AF_VOICEVIEW = 18;	/* VoiceView */
    public static final int AF_FIREFOX = 19;	/* Protocols from Firefox */
    public static final int AF_UNKNOWN1 = 20;	/* Somebody is using this! */
    public static final int AF_BAN = 21;		/* Banyan */
    public static final int AF_ATM = 22;		/* Native ATM Services */
    public static final int AF_MAX = 24;
    public static final int AF_INET6 = 30;		/* Internetwork Version 6 */
    public static final int AF_GSMSMS = AF_UNKNOWN1;/* Computer Interface to Message Distribution */
    public static final int AF_NBS = 31;		/* Narrow Band Socket */
    public static final int SOCK_STREAM = 1;	/* stream socket */
    public static final int SOCK_DGRAM = 2;		/* datagram socket */
    public static final int SOCK_RAW = 3;		/* raw-protocol interface */
    public static final int SOCK_RDM = 4;		/* reliably-delivered message */
    public static final int SOCK_SEQPACKET = 5;	/* sequenced packet stream */
    public static final int IPPROTO_IP = 0;		/* dummy for IP */
    public static final int IPPROTO_ICMP = 1;	/* control message protocol */
    public static final int IPPROTO_IGMP = 2;	/* internet group management protocol */
    public static final int IPPROTO_GGP = 3;	/* gateway^2 (deprecated) */
    public static final int IPPROTO_TCP = 6;	/* tcp */
    public static final int IPPROTO_PUP = 12;	/* pup */
    public static final int IPPROTO_UDP = 17;	/* user datagram protocol */
    public static final int IPPROTO_IDP = 22;	/* xns idp */
    public static final int IPPROTO_ND = 77;	/* UNOFFICIAL net disk proto */
    public static final int IPPROTO_RAW = 255;	/* raw IP packet */
    public static final int IPPROTO_MAX = 256;
    public static final int NBS_SMSWLESS = 1;
    public static final int NBS_SMSINET = 2;
    public static final int NBS_SMSNOTIF = 3;
    public static final int NBS_PACT = 4;
    public static final int NBS_CDMA = 5;
    public static final int SG_UNCONSTRAINED_GROUP = 0x01;
    public static final int SG_CONSTRAINED_GROUP = 0x02;
    public static final int WSA_FLAG_OVERLAPPED = 0x01;
    public static final int WSA_FLAG_MULTIPOINT_C_ROOT = 0x02;
    public static final int WSA_FLAG_MULTIPOINT_C_LEAF = 0x04;
    public static final int WSA_FLAG_MULTIPOINT_D_ROOT = 0x08;
    public static final int WSA_FLAG_MULTIPOINT_D_LEAF = 0x10;
    public static final int FROM_PROTOCOL_INFO = -1;
    public static final String socksServerProp = "socksProxyHost";
    public static final String socksPortProp = "socksProxyPort";
    public static final String socksDefaultPortStr = "1080";

    private static final int SOCKS_PROTO_VERS = 4;
    private static final int SOCKS_REPLY_VERS = 4;
    private static final int COMMAND_CONNECT = 1;
    private static final int COMMAND_BIND = 2;
    private static final int REQUEST_GRANTED = 90;
    private static final int REQUEST_REJECTED = 91;
    private static final int REQUEST_REJECTED_NO_IDENTD = 92;
    private static final int REQUEST_REJECTED_DIFF_IDENTS = 93;
    private static final int IOC_UNIX = 0x0000000;
    private static final int IOC_WS2 = 0x0800000;
    private static final int IOC_PROTOCOL = 0x10000000;
    private static final int IOC_VENDOR = 0x1800000;
    private static final int IOC_VOID = 0x20000000;
    private static final int IOC_OUT = 0x40000000;
    private static final int IOC_IN = 0x80000000;
    private static final int IOC_INOUT = IOC_IN | IOC_OUT;

    static final int NBS_NOTIFYMODE = IOC_IN | IOC_PROTOCOL | AF_NBS << 16 | 1;
    static final int NBS_OPERATIONSTATUS = IOC_INOUT | IOC_PROTOCOL | AF_NBS << 16 | 2;
    static final int NBS_ORDERED = IOC_IN | IOC_PROTOCOL | AF_NBS << 16 | 3;
    static final int NBS_DATACODING = IOC_IN | IOC_PROTOCOL | AF_NBS << 16 | 4;
    static final int NBS_VALIDITY = IOC_INOUT | IOC_PROTOCOL | AF_NBS << 16 | 5;
    static final int NBS_GET_PARAMETERS = IOC_INOUT | IOC_PROTOCOL | AF_NBS << 16 | 6;
    static final int NBS_SET_PARAMETERS = IOC_IN | IOC_PROTOCOL | AF_NBS << 16 | 7;
    static final int NBS_CONCATINFO = IOC_OUT | IOC_PROTOCOL | AF_NBS << 16 | 8;
    static final int NBS_SETPARTIALRECV = IOC_IN | IOC_PROTOCOL | AF_NBS << 16 | 9;
    static final int NBS_SETHEADERCODING = IOC_IN | IOC_PROTOCOL | AF_NBS << 16 | 10;
    static final int NBS_GETHEADERCODING = IOC_OUT | IOC_PROTOCOL | AF_NBS << 16 | 11;
    static final int NBS_SET_PROTSPECIFIC = IOC_IN | IOC_PROTOCOL | AF_NBS << 16 | 12;
    static final int NBS_GET_PROTSPECIFIC = IOC_OUT | IOC_PROTOCOL | AF_NBS << 16 | 13;
    static final int NBS_REFNUM = IOC_OUT | IOC_PROTOCOL | AF_NBS << 16 | 14;

    InetAddress localAddress = null;
    SecureSocketsLayer ssl = null;
    int fd0 = 0;
    int af = 0;
    int type = 0;
    int protocol = 0;
    int group = 0;
    int flags = 0;
    int timeout = 0;
    long timeval = 0;
    long protocolInfo = 0;
    boolean stream = true;

    public static native String getHostName();

    private static native void initialize();
    private static native long getTimeval(long timeval, int millis);
    private static native long getProtocolInfo0(String protocolName)
        throws ProtocolException;
    private static native Object[] getHostByAddr0(byte addr[])
        throws IOException;
    private static native Object[] getHostByName0(int af, String name)
        throws IOException;
    private static native int getDefaultAddressFamily0();
    private static native byte[] getLoopbackAddress0(int af);
    private static native byte[] getAnyLocalAddress0(int af);
    private static native int select0(long readFds, long writeFds, long exceptFds, int sec, int usec)
        throws IOException;

    private native int create0(int af, int type, int protocol, long protocolInfo, int group, int flags)
        throws IOException;
    private native void connect0(int fd0, int af, byte addr[], int scopeId, int port, int timeout)
        throws IOException;
    private native void bind0(int fd0, int af, byte addr[], int scopeId, int port)
        throws IOException;
    private native void listen0(int fd0, int backlog)
        throws IOException;
    private native int accept0(int fd0, int af, long timeval, SocketImpl impl)
        throws IOException;
    private native int available0(int fd0)
        throws IOException;
    private native void close0(int fd0, long timeval)
        throws IOException;
    private native void shutdownInput0(int fd0)
        throws SocketException;
    private native void shutdownOutput0(int fd0)
        throws SocketException;
    private native void setOption0(int fd0, int option, boolean flag, int value)
        throws SocketException;
    private native int getOption0(int fd0, int option)
        throws SocketException;
    private native int read0(int fd0, long timeval, byte buf[], int off, int len)
        throws IOException;
    private native void write0(int fd0, byte buf[], int off, int len)
        throws IOException;
    private native void receive0(int fd0, int af, long timeval, MultiDatagramPacket mdp)
        throws IOException;
    private native void send0(int fd0, int af, MultiDatagramPacket mdp)
        throws IOException;
    private native void setIOControl0(int fd0, int af, int code, int value)
        throws SocketException;
    private native int getIOControl0(int fd0, int af, int code)
        throws SocketException;
    private native int getLocalPort(int fd0)
        throws IOException;
    private native boolean isConnected0(int fd0);

    public static long getProtocolInfo(String protocolName)
        throws ProtocolException {
        return MultiSocketImpl.getProtocolInfo0(protocolName);
    }

    public static Object[] getHostByAddr(byte addr[])
        throws IOException {
        return MultiSocketImpl.getHostByAddr0(addr);
    }

    public static Object[] getHostByName(int af, String name)
        throws IOException, ProtocolException {
        if ("localhost".equalsIgnoreCase(name) || "loopback".equalsIgnoreCase(name) || "127.0.0.1".equals(name))
            return new Object[] {new String[] {"localhost"}, new byte[][] {MultiSocketImpl.getLoopbackAddress(af)}, new int[] {0}};
        return MultiSocketImpl.getHostByName0(af, name);
    }

    public static int getDefaultAddressFamily() {
        return MultiSocketImpl.getDefaultAddressFamily0();
    }

    public static byte[] getLoopbackAddress(int af) {
        return MultiSocketImpl.getLoopbackAddress0(af);
    }

    public static byte[] getAnyLocalAddress(int af) {
        return MultiSocketImpl.getAnyLocalAddress0(af);
    }

    public static int select(SocketSet readSS, SocketSet writeSS, SocketSet exceptSS, long millis)
        throws IOException {
        int sec = 0, usec = 0;
        if (millis != 0L) {
            sec = (int)(millis / 1000L);
            usec = (int)(millis % 1000L) * 1000;
        }
        return MultiSocketImpl.select0(readSS != null ? readSS.fdSet : 0,
                                       writeSS != null ? writeSS.fdSet : 0,
                                       exceptSS != null ? exceptSS.fdSet : 0, sec, usec);
    }

    /** Construct socket by specific parameters. */
    public MultiSocketImpl(int af, int type, int protocol, int group, int flags) {
        this.af = af;
        this.type = type;
        this.protocol = protocol;
        this.group = group;
        this.flags = flags;
    }

    /** Construct socket by protocol name. */
    public MultiSocketImpl(String protocolName, int group, int flags)
        throws ProtocolException {
        protocolInfo = MultiSocketImpl.getProtocolInfo(protocolName);
        af = FROM_PROTOCOL_INFO;
        type = FROM_PROTOCOL_INFO;
        protocol = FROM_PROTOCOL_INFO;
        this.group = group;
        this.flags = flags;
    }

    /** Construct socket by specific parameters and protocol info. */
    public MultiSocketImpl(int af, int type, int protocol, long protocolInfo, int group, int flags) {
        this(af, type, protocol, group, flags);
        this.protocolInfo = protocolInfo;
    }

    /** Construct socket by protocol info. */
    public MultiSocketImpl(long protocolInfo, int group, int flags) {
        this(FROM_PROTOCOL_INFO, FROM_PROTOCOL_INFO, FROM_PROTOCOL_INFO, group, flags);
        this.protocolInfo = protocolInfo;
    }

    /** Construct IP-socket. */
    public MultiSocketImpl(int group, int flags) {
        this(AF_INET, SOCK_STREAM, IPPROTO_IP, group, flags);
    }

    public final int getFd0()
        throws SocketException {
        try {
            return fd0Field.getInt(fd);
        } catch (IllegalAccessException ex) {
            throw new SocketException(Support.stackTrace(ex));
        }
    }

    public synchronized void create(boolean stream)
        throws IOException {
        create(stream, -1);
    }

    public synchronized void create(boolean stream, int af)
        throws IOException {
        if (af != -1) this.af = af;
        else af = this.af;
        if (af == -1 && protocolInfo == -1L) return;
        this.stream = stream;
        if (type == 0) type = stream ? SOCK_STREAM : SOCK_DGRAM;
        fd = new FileDescriptor();
        fd0 = create0(af, type, protocol, protocolInfo, group, flags);
        try {
            fd0Field.setInt(fd, fd0);
        } catch (IllegalAccessException ex) {
            throw new IOException(Support.stackTrace(ex));
        }
    }

    public void connect(String host, int port)
        throws UnknownHostException, IOException {
        connect(host, port, 0);
    }

    public void connect(String host, int port, int timeout)
        throws UnknownHostException, IOException {
        if (af == -1) return;
        try {
            Object[] addressArrays = MultiSocketImpl.getHostByName(af, host);
            byte[][] addrArray = (byte[][])addressArrays[1];
            int[] scopeIdArray = (int[])addressArrays[2];
            addr = addrArray[0];
            int scopeId = scopeIdArray[0];
            connectToAddress(addr, scopeId, port, timeout);
        } catch (IOException e) {
            close();
            throw e;
        }
    }

    public void connect(InetAddress address, int port)
        throws IOException {
        connect(address, port, 0);
    }

    public void connect(InetAddress address, int port, int timeout)
        throws IOException {
        if (af == -1) return;
        try {
            addr = address.getAddress();
            int scopeId = address instanceof Inet6Address ? ((Inet6Address)address).getScopeId() : 0;
            connectToAddress(addr, scopeId, port, timeout);
        } catch (IOException e) {
            close();
            throw e;
        }
    }

    public void connect(SocketAddress address, int timeout)
        throws IOException {
        InetSocketAddress inetAddress = (InetSocketAddress)address;
        if (inetAddress.isUnresolved()) connect(inetAddress.getHostName(), inetAddress.getPort(), timeout);
        else connect(inetAddress.getAddress(), inetAddress.getPort(), timeout);
    }

    public void connect(byte addr[], int scopeId, int port)
        throws IOException {
        connect(addr, port, 0);
    }

    public void connect(byte addr[], int scopeId, int port, int timeout)
        throws IOException {
        if (af == -1) return;
        this.addr = addr;
        this.port = port;
        try {
            connectToAddress(addr, scopeId, port, timeout);
        } catch (IOException e) {
            close();
            throw e;
        }
    }

    private void connectToAddress(byte addr[], int scopeId, int port, int timeout)
        throws IOException {
        if (usingSocks()) doSOCKSConnect(addr, scopeId, port, timeout);
        else doConnect(addr, scopeId, port, timeout);
    }

    public void setOption(int option, Object object)
        throws SocketException {
        boolean on = true;
        int value = 0;
        switch (option) {
        case SO_REUSEADDR:
        case TCP_NODELAY:
        case SO_KEEPALIVE:
            if (object == null || !(object instanceof Boolean))
                throw new SocketException("Bad parameter for option");
            on = ((Boolean)object).booleanValue();
            value = on ? 1 : 0;
            break;
        case SO_BINDADDR:
            throw new SocketException("Cannot re-bind socket");
        case SO_LINGER:
            if (object == null || !(object instanceof Integer) && !(object instanceof Boolean))
                throw new SocketException("Bad parameter for option");
            if (object instanceof Boolean) on = false;
            else value = ((Integer)object).intValue();
            break;
        case SO_SNDBUF:
        case SO_RCVBUF:
            if (object == null || !(object instanceof Integer))
                throw new SocketException("Bad parameter for option");
            value = ((Integer)object).intValue();
            break;
        case SO_TIMEOUT:
            if (object == null || !(object instanceof Integer))
                throw new SocketException("Bad parameter for option");
            value = ((Integer)object).intValue();
            if (value < 0) throw new IllegalArgumentException("timeout < 0");
            timeval = MultiSocketImpl.getTimeval(timeval, timeout = value);
            return;
        default:
            throw new SocketException("unrecognized TCP option: " + option);
        }
        try {
            setOption0(fd0, option, on, value);
        } catch (SocketException ex) {
            throw new SocketException(ex.toString()
                                      + ": setOption option=" + option
                                      + ", object=" + object + ", on=" + on);
        }
    }

    @SuppressWarnings("fallthrough")
    public Object getOption(int option)
        throws SocketException {
        if (option == SO_TIMEOUT) return new Integer(timeout);
        int value = getOption0(fd0, option);
        switch (option) {
        case TCP_NODELAY:
        case SO_KEEPALIVE:
            return new Boolean(value != 0 ? true : false);
        case SO_BINDADDR:
            return localAddress;
        case SO_LINGER:
            if (value == -1) return new Boolean(false);
        case SO_SNDBUF:
        case SO_RCVBUF:
            return new Integer(value);
        default:
            return null;
        }
    }

    private void doSOCKSConnect(byte addr[], int scopeId, int port, int timeout)
        throws IOException {
        connectToSocksServer(timeout);
        sendSOCKSCommandPacket(COMMAND_CONNECT, addr, port);
        switch (getSOCKSReply()) {
        case REQUEST_GRANTED:
            return;
        case REQUEST_REJECTED:
        case REQUEST_REJECTED_NO_IDENTD:
            throw new SocketException("SOCKS server cannot connect to identd");
        case REQUEST_REJECTED_DIFF_IDENTS:
            throw new SocketException("User name does not match identd name");
        }
    }

    private int getSOCKSReply()
        throws IOException {
        InputStream in = getInputStream();
        byte response[] = new byte[8];
        int code;
        if ((code = in.read(response)) != response.length) throw new SocketException("Malformed reply from SOCKS server");
        if (response[0] != 0) throw new SocketException("Malformed reply from SOCKS server");
        return response[1];
    }

    private void connectToSocksServer(int timeout)
        throws IOException {
        String socksProxyHost = System.getProperty("socksProxyHost");
        if (socksProxyHost == null) return;
        InetAddress address = InetAddress.getByName(socksProxyHost);
        byte addr[] = address.getAddress();
        int scopeId =  address instanceof Inet6Address ? ((Inet6Address)address).getScopeId() : 0;
        int port;
        try {
            port = Integer.parseInt(System.getProperty("socksProxyPort", "1080"));
        } catch (NumberFormatException e) {
            throw new SocketException("Bad port number format");
        }
        doConnect(addr, scopeId, port, timeout);
    }

    private void doConnect(byte addr[], int scopeId, int port, int timeout)
        throws IOException {
        if (af == AF_INET6) address = Inet6Address.getByAddress(null, addr, scopeId);
        else address = InetAddress.getByAddress(addr);
        for (int i = 0;;)
            try {
                connect0(fd0, af, addr, scopeId, port, timeout);
                this.addr = addr;
                this.port = port;
                if (localport == 0) localport = getLocalPort(fd0);
                if (secure) {
                    ssl = new SecureSocketsLayer();
                    ssl.setSocket(this);
                    ssl.connect();
                }
                return;
            } catch (IllegalAccessException ex) {
                throw new SocketException(Support.stackTrace(ex));
            } catch (ProtocolException ex) {
                close();
                if (++i < 3) create(stream);
                else throw ex;
            } catch (IOException ex) {
                close();
                throw ex;
            }
    }

    private void sendSOCKSCommandPacket(int command, byte addr[], int port)
        throws IOException {
        byte commandPacket[] = makeCommandPacket(command, addr, port);
        OutputStream out = getOutputStream();
        out.write(commandPacket);
    }

    private byte[] makeCommandPacket(int command, byte addr[], int port) {
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream(8 + 1);
        byteStream.write(SOCKS_PROTO_VERS);
        byteStream.write(command);
        byteStream.write((port >> 8) & 0xff);
        byteStream.write((port >> 0) & 0xff);
        byteStream.write(addr, 0, addr.length);
        byte usernameBytes[] = System.getProperty("user.name").getBytes();
        byteStream.write(usernameBytes, 0, usernameBytes.length);
        byteStream.write(0);
        return byteStream.toByteArray();
    }

    private boolean usingSocks() {
        return System.getProperty("socksProxyHost") != null;
    }

    public synchronized void bind(InetAddress address, int port)
        throws IOException {
        if (address != null && !address.isAnyLocalAddress()) {
            Object[] addressArrays = MultiSocketImpl.getHostByName(af, address.getHostAddress());
            byte[][] addrArray = (byte[][])addressArrays[1];
            int[] scopeIdArray = (int[])addressArrays[2];
            byte[] addr = addrArray[0];
            int scopeId = scopeIdArray[0];
            bind(addr, scopeId, port);
        } else bind(null, 0, port);
    }

    public synchronized void bind(byte addr[], int scopeId, int port)
        throws IOException {
        if (af == -1) return;
        if (addr == null) addr = MultiSocketImpl.getAnyLocalAddress(af);
        bind0(fd0, af, addr, scopeId, port);
        localAddr = addr;
        localport = port != 0 ? port : getLocalPort(fd0);
        if (af == AF_INET6)
            localAddress = Inet6Address.getByAddress(null, localAddr, scopeId);
        else localAddress = InetAddress.getByAddress(localAddr);
    }

    public synchronized void listen(int count)
        throws IOException {
        if (af == -1) return;
        listen0(fd0, count);
    }

    public synchronized void accept(SocketImpl impl)
        throws IOException {
        if (af == -1) return;
        FileDescriptor fd = new FileDescriptor();
        try {
            int fd1 = accept0(fd0, af, timeval, impl);
            if (impl instanceof MultiSocketImpl)
                ((MultiSocketImpl)impl).fd0 = fd1;
            fd0Field.setInt(fd, fd1);
            fdField.set(impl, fd);
            localportField.setInt(impl, localport);
            if (secure) {
                ssl = new SecureSocketsLayer();
                ssl.setSocket(impl);
                ssl.connect();
            }
        } catch (IllegalAccessException ex) {
            throw new IOException(Support.stackTrace(ex));
        }
    }

    public synchronized InputStream getInputStream()
        throws IOException {
        return secure ? ssl.getInputStream() : new W3SocketInputStream(this);
    }

    public synchronized OutputStream getOutputStream()
        throws IOException {
        return secure ? ssl.getOutputStream() : new W3SocketOutputStream(this);
    }

    public int read(byte buf[], int off, int len)
        throws IOException {
        return read0(fd0, timeval, buf, off, len);
    }

    public void write(byte buf[], int off, int len)
        throws IOException {
        write0(fd0, buf, off, len);
    }

    public void receive(MultiDatagramPacket mdp)
        throws IOException {
        receive0(fd0, af, timeval, mdp);
    }

    public void send(MultiDatagramPacket mdp)
        throws IOException {
        send0(fd0, af, mdp);
    }

    public void flush() {
    }

    public int available()
        throws IOException {
        return available0(fd0);
    }

    public void setIOControl(int code, int value)
        throws SocketException {
        setIOControl0(fd0, af, code, value);
    }

    public int getIOControl(int code)
        throws SocketException {
        return getIOControl0(fd0, af, code);
    }

    public void close()
        throws IOException {
        if (af == -1 || fd0 == -1) return;
        close0(fd0, timeval);
        if (ssl != null) ssl.close();
        fd0 = -1;
    }

    public void shutdownInput()
        throws SocketException {
        shutdownInput0(fd0);
    }

    public void shutdownOutput()
        throws SocketException {
        shutdownOutput0(fd0);
    }

    public void sendUrgentData(int data)
        throws IOException {
    }

    public String toString() {
        return "MultiSocketImpl[address="+ Support.toHexString(getAddress()) + ", port=" + getPort() + ", localport=" + getLocalPort() + ", fd=" + fd0 + "]";
    }

    public boolean isClosed() {
        return fd0 == -1;
    }

    public boolean isConnected() {
        return isConnected0(fd0);
    }

    protected void finalize() {
        try {
            close();
        } catch (IOException ex) {}
    }

}
