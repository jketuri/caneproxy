
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.security.*;
import java.util.*;
import javax.security.auth.*;
import javax.security.auth.callback.*;
import javax.security.auth.login.*;
import javax.security.sasl.*;

public class W3SmtpRequester extends W3CopyRequester {
    public static final boolean DEBUG = false;
    public static final int SMTP_PORT = 25;
    public static final int SMTP_SECURE_PORT = 465;

    String host, mechanisms[] = null;

    static {
        FI.realitymodeler.sasl.ClientFactory.initialize();
    }

    class LoginHandler implements CallbackHandler, PrivilegedAction<Object> {
        SaslClient sasl = null;
        String username = null, password = null;

        LoginHandler(String username, String password)
            throws GeneralSecurityException, IOException {
            this.username = username;
            this.password = password;
            sasl = Sasl.createSaslClient(mechanisms, username, "smtp", host, new Hashtable<String,Object>(), this);
            if (sasl == null) return;
            Object result = null;
            if (sasl.getMechanismName().equals("GSSAPI")) {
                LoginContext loginContext = new LoginContext("JaasSample", this);
                loginContext.login();
                result = Subject.doAs(loginContext.getSubject(), this);
            } else result = run();
            if (result != null) throw (IOException)result;
        }

        public void handle(Callback callback[]) {
            for (int i = 0; i < callback.length; i++)
                if (callback[i] instanceof NameCallback)
                    ((NameCallback)callback[i]).setName(username);
                else if (callback[i] instanceof PasswordCallback) {
                    char chars[] = new char[password.length()];
                    password.getChars(0, password.length(), chars, 0);
                    ((PasswordCallback)callback[i]).setPassword(chars);
                } else if (callback[i] instanceof RealmChoiceCallback)
                    ((RealmChoiceCallback)callback[i]).setSelectedIndex(((RealmChoiceCallback)callback[i]).getDefaultChoice());
        }

        public Object run() {
            try {
                byte response[] = sasl.hasInitialResponse() ? sasl.evaluateChallenge(new byte[0]) : null;
                BASE64Encoder base64 = new BASE64Encoder(0);
                if (send("AUTH " + sasl.getMechanismName() + (response != null ? " " + base64.encode(response) : ""), 334, 235) != 235)
                    while (!sasl.isComplete() && send(base64.encode(sasl.evaluateChallenge(getResponse().getBytes())), 334, 235) != 235);
                return null;
            } catch (IOException ex) {
                return ex;
            }
        }

    }

    class MessageOutputStream extends FilterOutputStream {
        boolean closed = false;

        MessageOutputStream(ConvertOutputStream out) {
            super(out);
        }

        public void close()
            throws IOException {
            if (closed) return;
            if (((ConvertOutputStream)out).getLastChar() != '\n') {
                out.write('\r');
                out.write('\n');
            }
            out.flush();
            send(".", 250);
            closed = true;
        }

    }

    public W3SmtpRequester(String host, int port, InetAddress localAddress, int localPort, boolean secure)
        throws IOException {
        if (host != null)
            try {
                open(host, port, localAddress, localPort, secure);
                return;
            } catch (IOException ex) {}
        try {
            String s = System.getProperty("mail.host");
            if (s != null) {
                open(s, port, localAddress, localPort, secure);
                return;
            }
        } catch (IOException ex) {}
        try {
            open("localhost", port, localAddress, localPort, secure);
        }
        catch(IOException ex) {
            open("mailhost", port, localAddress, localPort, secure);
        }
    }

    public W3SmtpRequester(String host)
        throws IOException {
        this(host, SMTP_PORT, null, 0, false);
    }

    public W3SmtpRequester() {
    }

    public Object clone() {
        return copy(new W3SmtpRequester());
    }

    public void open(String host, int port, InetAddress localAddress, int localPort, boolean secure)
        throws IOException {
        if (port == -1) port = SMTP_PORT;
        super.open(host, port, secure ? W3Socket.SECURE : W3Socket.NORMAL, localAddress, localPort);
        try {
            send("EHLO " + socket.getLocalAddress().getHostName(), 250);
            StringTokenizer st = new StringTokenizer(getResponse(), "\n");
            if (st.hasMoreTokens()) {
                st.nextToken();
                while (st.hasMoreTokens()) {
                    StringTokenizer st1 = new StringTokenizer(st.nextToken());
                    if (!st1.hasMoreTokens()) continue;
                    String keyword = st1.nextToken();
                    if (keyword.equalsIgnoreCase("auth")) {
                        mechanisms = new String[st1.countTokens()];
                        for (int i = 0; i < mechanisms.length; i++) mechanisms[i] = st1.nextToken();
                        break;
                    }
                }
            }
        } catch (IOException ex) {
            send("HELO " + socket.getLocalAddress().getHostName(), 250);
        }
    }

    public void open(String host)
        throws IOException {
        open(host, SMTP_PORT, null, 0, false);
    }

    public void close()
        throws IOException {
        if (isOpen())
            try {
                super.send("QUIT\r\n");
            } finally {
                super.close();
            }
    }

    int send(String command, int expect1, int expect2)
        throws IOException {
        if (DEBUG) System.out.println("send " + command + ", expect1=" + expect1 + ", expect2=" + expect2);
        super.send(command + "\r\n");
        int reply;
        while ((reply = readResponse()) != expect1 && (expect2 == 0 || reply != expect2))
            if (reply == 473 || reply == 505 || reply == 530) {
                throw new LoginException(command + ": " + reply + " " + getResponse(), makeRealm());
            }
            else if (reply != 220) throw new IOException(command + ": " + reply + " " + getResponse());
        if (DEBUG) System.out.println("serverResponse: " + getResponse());
        return reply;
    }

    int send(String command, int expect)
        throws IOException {
        return send(command, expect, 0);
    }

    protected void toCanonical(String s)
        throws IOException {
        send("RCPT TO: <" + s + ">", 250);
    }

    public void login(String user, String password)
        throws GeneralSecurityException, IOException {
        new LoginHandler(user, password);
    }

    public void to(String s)
        throws IOException {
        int st = 0;
        int limit = s.length();
        int pos = 0;
        int lastnonsp = 0;
        int parendepth = 0;
        boolean ignore = false;
        while (pos < limit) {
            int c = s.charAt(pos);
            if (parendepth > 0) {
                if (c == '(') parendepth++;
                else if (c == ')')
                    parendepth--;
                if (parendepth == 0)
                    if (lastnonsp > st) ignore = true;
                    else st = pos + 1;
            } else if (c == '(') parendepth++;
            else if (c == '<') st = lastnonsp = pos + 1;
            else if (c == '>') ignore = true;
            else if (c == ',') {
                if (lastnonsp > st) toCanonical(s.substring(st, lastnonsp));
                st = pos + 1;
                ignore = false;
            } else {
                if (c > ' ' && !ignore) lastnonsp = pos + 1;
                else if (st == pos) st++;
            }
            pos++;
        }
        if (lastnonsp > st) toCanonical(s.substring(st, lastnonsp));
    }

    public void from(String s)
        throws IOException {
        send("MAIL FROM: " + s, 250);
    }

    public OutputStream startMessage()
        throws IOException {
        send("DATA", 354);
        return new MessageOutputStream(Support.getDotOutputStream(output));
    }

}
