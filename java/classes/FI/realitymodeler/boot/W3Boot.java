
package FI.realitymodeler.boot;

import java.io.*;
import java.net.*;
import java.lang.reflect.*;

class JarFileFilter implements FileFilter {

    public boolean accept(File file) {
        return file.getName().toLowerCase().endsWith(".jar");
    }

}

public class W3Boot {

    public static void main(String argv[]) {
        File errorFile = new File(File.separatorChar == '/' ? System.getProperty("user.home") : "/", "KeppiProxyError");
        Class<?> w3serverClass = null;
	Object w3 = null;
	try {
            ClassLoader bootClassLoader = null;
            File serverLibDir = new File("server/lib");
            if (serverLibDir.exists()) {
                File serverLibFiles[] = serverLibDir.listFiles(new JarFileFilter());
                URL urls[] = new URL[serverLibFiles.length];
                for (int i = 0; i < serverLibFiles.length; i++) {
                    urls[i] = new URL("file:" + serverLibFiles[i].getPath());
                }
                bootClassLoader = URLClassLoader.newInstance(urls, ClassLoader.getSystemClassLoader());
            } else bootClassLoader = ClassLoader.getSystemClassLoader();
            Thread.currentThread().setContextClassLoader(bootClassLoader);
            w3serverClass = bootClassLoader.loadClass("FI.realitymodeler.server.W3Server");
            errorFile.delete();
            Constructor<?> constructor = w3serverClass.getConstructor(new Class<?>[] {Class.forName("[Ljava.lang.String;"), String.class});
            w3 = constructor.newInstance(new Object[] {argv, "KeppiProxyOptions"});
            Method waitForEndMethod = w3serverClass.getMethod("waitForEnd", new Class[] {w3serverClass});
            waitForEndMethod.invoke(w3, new Object[] {w3});
            System.exit(0);
	} catch (Throwable th) {
            try {
                try {
                    if (w3serverClass != null) {
                        Method shutdownMethod = w3serverClass.getMethod("shutdown", (Class[])null);
                        shutdownMethod.invoke(null, (Object[])null);
                    }
                } finally {
                    th.printStackTrace(System.out);
                    System.out.flush();
                    PrintWriter pw = new PrintWriter(new FileOutputStream(errorFile));
                    th.printStackTrace(pw);
                    pw.close();
                }
            } catch (Throwable th1) {}
            finally {
                System.exit(1);
            }
	}
    }

}
