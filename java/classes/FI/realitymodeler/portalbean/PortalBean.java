
package FI.realitymodeler.portalbean;

import FI.realitymodeler.common.*;
import FI.realitymodeler.xml.*;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;
import javax.naming.*;
import javax.naming.directory.*;
import javax.naming.ldap.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.w3c.dom.*;
import org.xml.sax.*;
import org.xml.sax.helpers.*;

/** This component is a common entry point for various services needing
    authentication and includes it's independent session handling, which
    operates also with browsers not having cookie-support. */

public class PortalBean implements Runnable {
    static Map<ServletConfig, PortalBean> portalBeans = new HashMap<ServletConfig, PortalBean>();
    static Map<String, Session> sessions = new HashMap<String, Session>();

    Map<String, String> userParameters = new HashMap<String, String>();
    Map<String, String> parameterAliases = new HashMap<String, String>();
    Hashtable<String, String> environment = new Hashtable<String, String>();
    HttpSession httpSession = null;
    PortalBean portalBean = null;
    ServletConfig servletConfig = null;
    ServletContext servletContext = null;
    String dnPattern = null;
    String filter = null;
    String state = "login";
    boolean verbose = false;
    int maxInactiveInterval = 60 * 1000;

    public class Session implements HttpSession {
        Hashtable<String,Object> values = new Hashtable<String,Object>();
        String id;
        boolean invalidated = false, isNew = true;
        int maxInactiveInterval = PortalBean.this.maxInactiveInterval;
        int n_bindings = 1;
        long creationTime;
        long lastAccessedTime;

        public Session(String id) {
            this.id = id;
            creationTime = lastAccessedTime = System.currentTimeMillis();
        }

        public long getCreationTime() {
            if (invalidated) throw new IllegalStateException();
            return creationTime;
        }

        public String getId() {
            if (invalidated) throw new IllegalStateException();
            return id;
        }

        public long getLastAccessedTime() {
            if (invalidated) throw new IllegalStateException();
            return lastAccessedTime;
        }

        public ServletContext getServletContext() {
            return null;
        }

        public void setMaxInactiveInterval(int interval) {
            if (invalidated) throw new IllegalStateException();
            maxInactiveInterval = interval;
        }

        public int getMaxInactiveInterval() {
            if (invalidated) throw new IllegalStateException();
            return maxInactiveInterval;
        }

        @Deprecated
        public HttpSessionContext getSessionContext() {
            if (invalidated) throw new IllegalStateException();
            return null;
        }

        public Object getAttribute(String name) {
            if (invalidated) throw new IllegalStateException();
            return values.get(name);
        }

        @Deprecated
        public Object getValue(String name) {
            return getAttribute(name);
        }

        public Enumeration getAttributeNames() {
            if (invalidated) throw new IllegalStateException();
            return values.keys();
        }

        @Deprecated
        public String[] getValueNames() {
            if (invalidated) throw new IllegalStateException();
            String valueNames[] = new String[values.size()];
            Enumeration keyElements = values.keys();
            for (int i = 0; i < valueNames.length && keyElements.hasMoreElements(); i++) valueNames[i] = (String)keyElements.nextElement();
            return valueNames;
        }

        public void setAttribute(String name, Object value) {
            if (invalidated) throw new IllegalStateException();
            removeAttribute(name);
            values.put(name, value);
            if (value instanceof HttpSessionBindingListener) ((HttpSessionBindingListener)value).valueBound(new HttpSessionBindingEvent(this, name));
        }

        @Deprecated
        public void putValue(String name, Object value) {
            setAttribute(name, value);
        }

        public void removeAttribute(String name) {
            if (invalidated) throw new IllegalStateException();
            Object value = values.remove(name);
            if (value instanceof HttpSessionBindingListener) ((HttpSessionBindingListener)value).valueUnbound(new HttpSessionBindingEvent(this, name));
        }

        @Deprecated
        public void removeValue(String name) {
            removeAttribute(name);
        }

        public void invalidate() {
            if (invalidated) throw new IllegalStateException();
            invalidated = true;
        }

        public boolean isNew() {
            if (invalidated) throw new IllegalStateException();
            return isNew;
        }

    }

    public void run() {
        try {
            for (;;) {
                cleanSessions(System.currentTimeMillis());
                Thread.sleep(maxInactiveInterval);
            }
        } catch (InterruptedException ex) {}
    }

    public HttpSession continueSession(String sessionId) {
        Session session = null;
        if (sessionId != null) {
            synchronized (sessions) {
                if ((session = sessions.get(sessionId)) != null && !session.invalidated) {
                    session.n_bindings++;
                    session.isNew = false;
                    return session;
                }
            }
            return null;
        }
        String id = Long.toString(System.currentTimeMillis(), Character.MAX_RADIX);
        long decimal = 0L;
        while (sessions.containsKey(sessionId = id + (decimal > 0L ? "." + Long.toString(decimal, Character.MAX_RADIX) : ""))) decimal++;
        sessions.put(sessionId, session = new Session(sessionId));
        return session;
    }

    public void pauseSession(HttpSession session) {
        synchronized (sessions) {
            ((Session)session).n_bindings--;
        }
    }

    public synchronized void cleanSessions(long ctm) {
        if (verbose) servletContext.log("Cleaning sessions");
        Vector<Session> sessionsToRemove = new Vector<Session>();
        Iterator sessionIter = sessions.values().iterator();
        while (sessionIter.hasNext()) {
            Session session = (Session)sessionIter.next();
            if (!session.invalidated && (session.n_bindings > 0 || session.lastAccessedTime > ctm - session.maxInactiveInterval)) continue;
            sessionsToRemove.addElement(session);
        }
        Iterator removeIter = sessionsToRemove.iterator();
        while (removeIter.hasNext()) {
            Session session = (Session)removeIter.next();
            synchronized (sessions) {
                if (!session.invalidated && (session.n_bindings > 0 || session.lastAccessedTime > ctm - session.maxInactiveInterval)) continue;
                sessions.remove(session.id);
            }
            if (verbose) servletContext.log("Removed session " + session.id);
            Iterator valueIter = session.values.entrySet().iterator();
            while (valueIter.hasNext()) {
                Map.Entry entry = (Map.Entry)valueIter.next();
                valueIter.remove();
                if (entry.getValue() instanceof HttpSessionBindingListener) ((HttpSessionBindingListener)entry.getValue()).valueUnbound(new HttpSessionBindingEvent(session, (String)entry.getKey()));
            }
        }
    }

    public static void requestLogin(HttpServletRequest req)
        throws MalformedURLException, RedirectException {
        throw new RedirectException(new URL(new URL(HttpUtils.getRequestURL(req).toString()), "portal.html").toString());
    }

    public static PortalEntry searchPortalEntry(HttpServletRequest req, ServletConfig servletConfig)
        throws MalformedURLException, RedirectException {
        PortalEntry portalEntry = null;
        PortalBean portalBean = portalBeans.get(servletConfig);
        if (portalBean != null) {
            HttpSession httpSession = null;
            String id = portalBean.getParameter(req, "id");
            if (id != null) httpSession = portalBean.continueSession(id);
            if (httpSession != null) portalEntry = (PortalEntry)httpSession.getAttribute("FI.realitymodeler.portalbean.PortalEntry");
        }
        if (portalEntry == null) {
            requestLogin(req);
            return null;
        }
        return portalEntry;
    }

    public void setDnPattern(String dnPattern) {
        this.dnPattern = dnPattern;
    }

    public String getDnPattern() {
        return dnPattern != null ? dnPattern : portalBean != null ? portalBean.getDnPattern() : null;
    }

    /** Sets environment property which is used when obtaining the initial
        context to directory. 

        @param propertyName name of the property

        @param value value of the property. */

    public void setEnvironment(String propertyName, String value) {
        environment.put(propertyName, value);
    }

    public String getEnvironment(String propertyName) {
        String property = environment.get(propertyName);
        return property != null ? property : portalBean != null ? portalBean.getEnvironment(propertyName) : null;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public String getFilter() {
        return filter != null ? filter : portalBean != null ? portalBean.getFilter() : null;
    }

    public String getParameterAlias(String parameterName) {
        String alias = parameterAliases.get(parameterName);
        return alias != null ? alias : portalBean != null ? portalBean.getParameterAlias(parameterName) : parameterName;
    }

    public void setParameterAlias(String parameterAlias, String parameterName) {
        parameterAliases.put(parameterName, parameterAlias);
    }

    public String getSessionId() {
        return httpSession.getId();
    }

    public String getSessionIdLink() {
        return "?id=" + httpSession.getId();
    }

    public String getState() {
        return state;
    }

    public String getUserParameter(String name) {
        return userParameters.get(name);
    }

    public boolean getVerbose() {
        return verbose;
    }

    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    private String getParameter(HttpServletRequest req, String name) {
        String parameter = req.getParameter(name = getParameterAlias(name));
        if (parameter != null && (parameter = parameter.trim()).equals("")) parameter = null;
        if (parameter != null) userParameters.put(name, parameter);
        return parameter;
    }

    public void setServlet(String property, String value)
        throws ServletException, ParseException {
        StringTokenizer st = new StringTokenizer(property, ":");
        if (!st.hasMoreTokens() || !st.nextToken().equalsIgnoreCase("portal")) return;
        value = Support.decode(value);
        if (verbose) servletContext.log("setServlet " + property + " to " + value);
        String name = st.nextToken().trim();
        if (name.equals("dnPattern")) setDnPattern(value);
        else if (name.equals("environment")) setEnvironment(st.nextToken(), value);
        else if (name.equals("filter")) setFilter(value);
        else throw new ServletException("Unknown portal bean servlet parameter " + name);
    }

    public void setServletConfig(ServletConfig servletConfig)
        throws ServletException, ParseException {
        if (servletConfig == null) return;
        this.servletConfig = servletConfig;
        servletContext = servletConfig.getServletContext();
        if (Support.isTrue(servletConfig.getInitParameter("verbose"), "verbose") ||
            servletContext.getAttribute("FI.realitymodeler.server.W3Server/verbose") == Boolean.TRUE) setVerbose(true);
        if ((portalBean = portalBeans.get(servletConfig)) != null) return;
        portalBean = new PortalBean();
        portalBean.servletContext = servletContext;
        if (getVerbose()) portalBean.setVerbose(true);
        Enumeration initParameterNames = servletConfig.getInitParameterNames();
        while (initParameterNames.hasMoreElements()) {
            String name = (String)initParameterNames.nextElement();
            portalBean.setServlet(name, servletConfig.getInitParameter(name));
        }
        portalBeans.put(servletConfig, portalBean);
        new Thread(portalBean).start();
    }

    public void processRequest(HttpServletRequest req)
        throws IOException, NamingException, ServletException {
        String id = portalBean.getParameter(req, "id");
        if (id != null) {
            httpSession = portalBean.continueSession(id);
            if (httpSession != null && httpSession.getAttribute("FI.realitymodeler.portalbean.PortalEntry") != null) {
                state = "entered";
                return;
            }
        }
        String username = getParameter(req, "u"), password = getParameter(req, "p");
        if (username == null) return;
        if (password == null) password = "";
        Hashtable<String,String> dirEnv = new Hashtable<String,String>();
        if (portalBean != null) dirEnv.putAll(portalBean.environment);
        if (!dirEnv.containsKey(Context.INITIAL_CONTEXT_FACTORY) && System.getProperty(Context.INITIAL_CONTEXT_FACTORY) == null)
            dirEnv.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        if (!dirEnv.containsKey(Context.REFERRAL) && System.getProperty(Context.REFERRAL) == null)
            dirEnv.put(Context.REFERRAL, "follow");
        String dn = Support.replace(getDnPattern(), "{}", username);
        dirEnv.put(Context.SECURITY_PRINCIPAL, dn);
        dirEnv.put(Context.SECURITY_CREDENTIALS, password);
        if (!dirEnv.containsKey(Context.SECURITY_AUTHENTICATION) &&
            System.getProperty(Context.SECURITY_AUTHENTICATION) == null) dirEnv.put(Context.SECURITY_AUTHENTICATION, "simple");
        String mail = null, mailhost = null;
        InitialLdapContext ldapContext = null;
        try {
            if (verbose) servletContext.log("getting initial context with environment " + dirEnv);
            ldapContext = new InitialLdapContext(dirEnv, null);
            NamingEnumeration namingEnumeration = null;
            String filter = getFilter();
            try {
                if (verbose) servletContext.log("search " + dn + " with filter " + filter);
                namingEnumeration = ldapContext.search(dn, filter,
                                                       new SearchControls(SearchControls.OBJECT_SCOPE, 1, 0, null, false, true));
                try {
                    SearchResult searchResult = (SearchResult)namingEnumeration.next();
                    javax.naming.directory.Attributes attributes = searchResult.getAttributes();
                    Attribute attribute = attributes.get("mail");
                    if (attribute != null) mail = (String)attribute.get();
                    attribute = attributes.get("mailhost");
                    if (attribute != null) mailhost = (String)attribute.get();
                } catch (NoSuchElementException ex) {
                    return;
                }
            } finally {
                if (namingEnumeration != null) namingEnumeration.close();
            }
        } catch (AuthenticationException ex) {
            return;
        } finally {
            if (ldapContext != null) ldapContext.close();
        }
        httpSession = portalBean.continueSession(null);
        PortalEntry portalEntry = new PortalEntry();
        portalEntry.credentials = "Basic " + new BASE64Encoder().encode((username + ":" + password).getBytes());
        portalEntry.username = username;
        portalEntry.password = password;
        portalEntry.mailhost = mailhost;
        portalEntry.email = mail;
        portalEntry.httpSession = httpSession;
        httpSession.setAttribute(portalEntry.getClass().getName(), portalEntry);
        state = "entered";
    }

}
