
package FI.realitymodeler.portalbean;

import javax.servlet.http.*;

public class PortalEntry {
    public String credentials;
    public String username;
    public String password;
    public String mailhost;
    public String email;
    public HttpSession httpSession;
}