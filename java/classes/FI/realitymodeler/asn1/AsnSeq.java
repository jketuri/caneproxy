
package FI.realitymodeler.asn1;

import java.io.*;
import java.util.*;

public class AsnSeq extends AsnAny {
    protected Vector<AsnAny> value;

    public AsnSeq(Vector<AsnAny> value) {
        this.value = value;
    }

    public AsnSeq() {
    }

    public Object clone() {
        return new AsnSeq(value);
    }

    public final Vector<AsnAny> getValue() {
        return value;
    }

    public void set(Vector<AsnAny> value) {
        this.value = value;
    }

    public void set(AsnSeq seq) {
        value = seq.value;
    }

    public int bEncContent(AsnBuf buf)
        throws Exception {
        int l = bEncEoc(buf);
        for (int i = value.size() - 1; i >= 0; i--) l += value.elementAt(i).bEnc(buf);
        return l;
    }

    public int bDecContent(InputStream in, int tagId, int elmtLen)
        throws Exception {
        int bd = bytesDecoded;
        if (elmtLen != INDEFINITE_LEN) elmtLen += bytesDecoded;
        value = new Vector<AsnAny>();
        while (bytesDecoded < elmtLen || elmtLen == INDEFINITE_LEN) {
            AsnAny aa = bDecAny(in);
            if (aa == null) break;
            value.addElement(aa);
            bytesDecoded += aa.bytesDecoded;
        }
        return bytesDecoded - bd;
    }

    public int bEnc(AsnBuf buf)
        throws Exception {
        int l = bEncContent(buf);
        l += bEncConsLen(buf, l);
        l += bEncTag(buf, UNIV, CONS, SEQ_TAG_CODE);
        return l;
    }

    public int bDec(InputStream in)
        throws Exception {
        int bd = bytesDecoded, tag = bDecTag(in);
        if (tag != makeTagId(UNIV, CONS, SEQ_TAG_CODE)) throw new Exception("Wrong tag");
        int elmtLen = bDecLen(in);
        bDecContent(in, tag, elmtLen);
        return bytesDecoded - bd;
    }

    public int bEncContent(AsnBuf buf, OutputStream out)
        throws Exception {
        int l = 0;
        buf.init(buf.data);
        if (indefiniteLength) {
            int n = bEncEoc(buf);
            out.write(buf.data, 0, n);
            l += n;
        }
        for (int i = value.size() - 1; i >= 0; i--) {
            buf.init(buf.data);
            int n = value.elementAt(i).bEnc(buf);
            out.write(buf.data, 0, n);
            l += n;
        }
        return l;
    }

    public int bEnc(AsnBuf buf, OutputStream out)
        throws Exception {
        int l = bEncContent(buf, out);
        l += bEncConsLen(buf, l);
        l += bEncTag(buf, UNIV, CONS, SEQ_TAG_CODE);
        return l;
    }

    public void print(PrintStream p) {
        p.println("{ -- SEQUENCE --");
        for (int i = 0; i < value.size(); i++) {
            value.elementAt(i).print(p);
            if (i < value.size() - 1) p.println(",");
        }
        p.print("\n}");
    }

}       
