
package FI.realitymodeler.asn1;

import java.io.*;

public class AsnInt extends AsnAny {
    protected int forceLen, value;

    public AsnInt() {
    }

    public AsnInt(int value, int forceLen) {
        this.value = value;
        this.forceLen = forceLen;
    }

    public AsnInt(int value) {
        this(value, 0);
    }

    public Object clone() {
        return new AsnInt(value);
    }

    public final int getValue() {
        return value;
    }

    /** Encodes BER content of this AsnInt to the given buffer.
        Returns the number of octets written to the buffer. */
    public int bEncContent(AsnBuf buf) {
        int i, len, dataCpy = value, mask = 0x7f80 << 16;
        // calculate encoded length of the integer (content)
        if (dataCpy < 0)
            for (len = 4; len > 1; --len)
                if ((dataCpy & mask) == mask) mask >>= 8;
                else break;
        else for (len = 4; len > 1; --len)
                 if ((dataCpy & mask) == 0) mask >>= 8;
                 else break;
        if (forceLen > len) len = forceLen;
        // write the BER integer
        for (i = 0; i < len; i++) {
            buf.putByteRvs(dataCpy);
            dataCpy >>= 8;
        }
        return len;
    }

    /** Decodes the content of a BER INTEGER from the given input stream.
        The value is placed in this object.  tagId is ignored. */
    public int bDecContent(InputStream in, int tagId, int elmtLen)
        throws Exception {
        if (elmtLen > 4) throw new Exception("Integer is too big to decode.");
        // look at integer value
        int c =  getByte(in);
        // top bit of first byte is sign bit
        if ((c & 0x80) != 0) value = (-1 << 8) | c;
        else value = c;
        // write from input stream into AsnIntType
        for (int i = 1; i < elmtLen; i++) value = (value << 8) | getByte(in);
        return elmtLen;
    }

    public int bEnc(AsnBuf buf) {
        int l = bEncContent(buf);
        buf.putByteRvs(l);
        l++;
        l += bEncTag(buf, UNIV, PRIM, INTEGER_TAG_CODE);
        return l;
    }

    public int bDec(InputStream in)
        throws Exception {
        int bd = bytesDecoded;
        if (bDecTag(in) != makeTagId(UNIV, PRIM, INTEGER_TAG_CODE))
            throw new Exception("Tag on INTEGER is wrong.");
        int elmtLen = bDecLen(in);
        bDecContent(in, makeTagId(UNIV, PRIM, INTEGER_TAG_CODE), elmtLen);
        return bytesDecoded - bd;
    }

    public void print(PrintStream p) {
        p.print(value);
    }

}
