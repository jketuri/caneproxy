
package FI.realitymodeler.asn1;

import FI.realitymodeler.common.*;
import java.io.*;

public class AsnOcts extends AsnAny {
    protected int code = OCTETSTRING_TAG_CODE;
    protected byte octs[];

    public AsnOcts() {
    }

    public AsnOcts(byte octs[]) {
        set(octs);
    }

    public AsnOcts(String str) {
        set(str);
    }

    public AsnOcts(AsnOcts o) {
        set(o);
    }

    public AsnOcts(int code) {
        this.code = code;
    }

    public AsnOcts(int code, byte octs[]) {
        set(code, octs);
    }

    public AsnOcts(int code, String str) {
        set(code, str);
    }

    public AsnOcts(int code, AsnOcts o) {
        set(code, o);
    }

    public Object clone() {
        return new AsnOcts(octs);
    }

    public final int getCode() {
        return code;
    }

    public final byte[] getOcts() {
        return octs;
    }

    /** Initialize the AsnOcts with octs. */
    public void set(byte octs[]) {
        this.octs = octs;
    }

    /** Initialize the AsnOcts from another AsnOcts. */
    public void set(AsnOcts o) {
        octs = o.octs;
    }

    /** Initialize the AsnOcts from a string. */
    public void set(String str) {
        octs = str.getBytes();
    }

    /** Initialize the AsnOcts with octs. */
    public void set(int code, byte octs[]) {
        this.code = code;
        set(octs);
    }

    /** Initialize the AsnOcts from another AsnOcts. */
    public void set(int code, AsnOcts o) {
        this.code = code;
        set(o);
    }

    /** Initialize the AsnOcts from a string. */
    public void set(int code, String str) {
        this.code = code;
        set(str);
    }

    public int bEncContent(AsnBuf buf) {
        buf.putSegRvs(octs);
        return octs.length;
    }

    /** Decodes a BER OCTET STRING value and puts it in this object.
        Constructed OCTET STRINGs are always concatenated into primitive ones. */
    public int bDecContent(InputStream in, int tagId, int elmtLen)
        throws Exception {
        // tagId is encoded tag shifted into long int.
        // if CONS bit is set then constructed octet string
        if ((tagId & 0x20000000) != 0) return bDecConsOcts (in, elmtLen);
        // primitive octet string
        octs = new byte[elmtLen];
        copySeg(in, octs);
        return elmtLen;
    }

    public int bEnc(AsnBuf buf) {
        int l = bEncContent(buf);
        l += bEncDefLen(buf, l);
        l += bEncTag(buf, UNIV, PRIM, code);
        return l;
    }

    public int bDec(InputStream in)
        throws Exception {
        int bd = bytesDecoded, tag = bDecTag(in);
        if (tag != makeTagId(UNIV, PRIM, code) && tag != makeTagId(UNIV, CONS, code))
            throw new Exception("Tag is wrong.");
        int elmtLen = bDecLen(in);
        bDecContent(in, tag, elmtLen);
        return bytesDecoded - bd;
    }

    /** Used for concatenating constructed OCTET STRING values when decoding */
    public void fill(InputStream in, int elmtLen0, ByteArrayOutputStream bout)
        throws Exception {
        if (elmtLen0 != INDEFINITE_LEN) elmtLen0 += bytesDecoded;
        while (bytesDecoded < elmtLen0 || elmtLen0 == INDEFINITE_LEN) {
            int tagId1 = bDecTag(in);
            if (tagId1 == EOC_TAG_ID && elmtLen0 == INDEFINITE_LEN) {
                bDec2ndEocOctet(in);
                break;
            }
            int elmtLen1 = bDecLen(in);
            if (tagId1 == makeTagId(UNIV, PRIM, code)) {
                // primitive part of string
                if (elmtLen1 != 0) {
                    byte b[] = new byte[elmtLen1];
                    copySeg(in, b);
                    bout.write(b);
                }
            } else if (tagId1 == makeTagId(UNIV, CONS, code)) fill(in, elmtLen1, bout);
            else throw new Exception("Decoded tag inside a constructed is not same");
        }
    }

    /** Decodes a seq of universally tagged octets until either EOC is
        encountered or the given len decoded.  Return them in a single concatenated octet string */
    public int bDecConsOcts(InputStream in, int elmtLen)
        throws Exception {
        int bd = bytesDecoded;
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        fill(in, elmtLen, bout);
        // alloc single str long enough for combined octetstring
        octs = bout.toByteArray();
        return bytesDecoded - bd;
    }

    public boolean octsEquiv(AsnOcts o) {
        return Support.compare(o.octs, octs) == 0;
    }

    /** Prints the AsnOcts to the given ostream in Value Notation. */
    public void print(PrintStream p) {
        int i;
        p.print("'");
        for (i = 0; i < octs.length; i++) p.print(Integer.toHexString(octs[i]));
        p.print("'H  -- \"");
        // put printable parts in ASN.1 comment
        for (i = 0; i < octs.length; i++)
            // newlines->space (so don't screw up ASN.1 comment)
            if (Character.isWhitespace((char)octs[i])) p.print(" ");
            else if (Character.isLetterOrDigit((char)octs[i])) p.print((char)octs[i]);
            else p.print(".");
        p.print("\" --");
    }

}
