
package FI.realitymodeler.asn1;

public class AsnBuf {
    public byte data[];
    public int dataPos;

    public AsnBuf(byte data[]) {
        this.data = data;
        dataPos = data.length;
    }

    public AsnBuf() {
    }

    public final void init(byte data[]) {
        this.data = data;
        dataPos = data.length;
    }

    public final void init() {
        dataPos = data.length;
    }

    public final int dataLen() {
        return data.length - dataPos;
    }

    public final void putSegRvs(byte src[]) {
        dataPos -= src.length;
        System.arraycopy(src, 0, data, dataPos, src.length);
    }

    public final void putByteRvs(int c) {
        data[--dataPos] = (byte)c;
    }

}
