
package FI.realitymodeler.asn1;

import FI.realitymodeler.common.*;
import java.io.*;

public class AsnBits extends AsnAny {
    protected byte bits[];
    protected int bitLen;

    public AsnBits(int numBits) {
        set(numBits);
    }

    public AsnBits(byte bits[], int numBits) {
        set(bits, numBits);
    }

    public AsnBits(AsnBits b) {
        set(b);
    }

    public AsnBits() {
    }

    public Object clone() {
        return new AsnBits(this);
    }

    public final int bitLen() {
        return bitLen;
    }

    /** Initializes the bits string with a bit string numBits in length.
        All bits are zeroed. */
    public void set(int bitLen) {
        this.bitLen = bitLen;
        bits = new byte[(bitLen + 7) / 8];
    }

    /** Initializes a BIT STRING with the given string and bit length
        Copies the bits from bitsOcts. */
    public void set(byte bits[], int bitLen) {
        this.bitLen = bitLen;
        this.bits = bits;
    }

    /** Initializes a BIT STRING by copying another BIT STRING's bits */
    public void set(AsnBits asnBits) {
        bitLen = asnBits.bitLen;
        bits = asnBits.bits;
    }

    /** Returns true if the given BIT STRING is the same as this one */
    public boolean bitsEquiv(AsnBits asnBits) {
        int octetsLessOne = (bitLen - 1) / 8,octetBits = 7 - (bitLen % 8);
        if (bitLen == 0 && asnBits.bitLen == 0) return true;
        // trailing bits may not be significant
        return bitLen == asnBits.bitLen &&
            Support.compare(bits, asnBits.bits, 0, 0, octetsLessOne) == 0 &&
            (bits[octetsLessOne] & (0xFF << octetBits)) ==
            (asnBits.bits[octetsLessOne] & (0xFF << octetBits));
    }

    /** Set given bit to 1. Most significant bit is bit 0, least significant bit is bitLen - 1. */
    public void setBit(int bit) {
        int octet = bit / 8;
        int octetsBit = 7 - (bit % 8);  // bit zero is first/most sig bit in octet
        bits[octet] |= 1 << octetsBit;
    }

    /** Clear bit. Most significant bit is bit 0, least significant bit is bitLen - 1. */
    public void clrBit(int bit) {
        int octet = bit / 8;
        int octetsBit = 7 - (bit % 8);  // bit zero is first/most sig bit in octet
        bits[octet] &= ~(1 << octetsBit);
    }

    /** Returns given bit. Most significant bit is bit 0, least significant bit is bitLen - 1. */
    public boolean getBit(int bit) {
        int octet = bit / 8;
        int octetsBit = 7 - (bit % 8);  // bit zero is first/most sig bit in octet
        return (bits[octet] & (1 << octetsBit)) != 0;
    }

    /** Encode the content (included unused bits octet) of the BIT STRING
        to the given output stream. */
    public int bEncContent(AsnBuf buf) {
        int byteLen = (bitLen + 7) / 8;
        buf.putSegRvs(bits);
        int unusedBits = (bitLen % 8);
        if (unusedBits != 0) unusedBits = 8 - unusedBits;
        buf.putByteRvs(unusedBits);
        return byteLen + 1;
    }

    /** Decodes a BER BIT STRING from the given input stream and stores
        the value in this object. */
    public int bDecContent(InputStream in, int tagId, int elmtLen)
        throws Exception {
        // tagId is encoded tag shifted into long int.
        // if CONS bit is set then constructed bit string
        if ((tagId & 0x20000000) != 0) return bDecConsBits(in, elmtLen);
        // primitive octet string
        elmtLen--;
        bitLen = (elmtLen * 8) - (getByte(in) & 0xff);
        bits = new byte[elmtLen];
        copySeg(in, bits);
        return elmtLen + 1;
    }

    public int bEnc(AsnBuf buf) {
        int l =  bEncContent (buf);
        l += bEncDefLen (buf, l);
        l += bEncTag(buf, UNIV, PRIM, BITSTRING_TAG_CODE);
        return l;
    }

    public int bDec(InputStream in)
        throws Exception {
        int bd = bytesDecoded, tag = bDecTag(in);
        if (tag != makeTagId(UNIV, PRIM, BITSTRING_TAG_CODE)
            && tag != makeTagId(UNIV, CONS, BITSTRING_TAG_CODE))
            throw new Exception("Tag on BIT STRING is wrong.");
        int elmtLen = bDecLen(in);
        bDecContent(in, tag, elmtLen);
        return bytesDecoded - bd;
    }

    /** Used to concatenate constructed bit strings when decoding. */
    public int fill(InputStream in, int elmtLen0, int unusedBits, ByteArrayOutputStream bout)
        throws Exception {
        if (elmtLen0 != INDEFINITE_LEN) elmtLen0 += bytesDecoded;
        while (bytesDecoded < elmtLen0 || elmtLen0 == INDEFINITE_LEN) {
            int tagId1 = bDecTag(in);
            if (tagId1 == EOC_TAG_ID && elmtLen0 == INDEFINITE_LEN) {
                bDec2ndEocOctet(in);
                break;
            }
            int elmtLen1 = bDecLen(in);
            if (tagId1 == makeTagId(UNIV, PRIM, BITSTRING_TAG_CODE)) {
                if (unusedBits != 0) throw new Exception("A component of a constructed" +
                                                         " BIT STRING that is not the last has non-zero unused bits");
                if (elmtLen1 > 0) {
                    unusedBits = getByte(in);
                    byte b[] = new byte[elmtLen1 - 1];
                    copySeg(in, b);
                    bout.write(b);
                }
            } else if (tagId1 == makeTagId(UNIV, CONS, BITSTRING_TAG_CODE))
                fill(in, elmtLen1, unusedBits, bout);
            else throw new Exception("Decoded non-BIT STRING tag inside a constructed BIT STRING");
        }
        return unusedBits;
    }


    /** Decodes a seq of universally tagged bits until either EOC is
        encountered or the given len decoded.  Return them in a
        single concatenated bit string */
    public int bDecConsBits(InputStream in, int elmtLen)
        throws Exception {
        int bd = bytesDecoded;
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        int unusedBits = fill(in, elmtLen, 0, bout);
        bits = bout.toByteArray();
        bitLen = bits.length * 8 - unusedBits;
        return bytesDecoded - bd;
    }

    /** Prints the BIT STRING to the given print stream. */
    public void print(PrintStream p) {
        int octetLen = (bitLen + 7) / 8;
        p.print("\"");
        for (int i = 0; i < octetLen; i++) p.print(Integer.toHexString(bits[i]));
        p.print("'H  -- BIT STRING bitlen = " + bitLen + " --");
    }

}
