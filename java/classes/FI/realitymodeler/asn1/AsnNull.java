
package FI.realitymodeler.asn1;

import java.io.*;

public class AsnNull extends AsnAny {
        
    public AsnNull() {
    }

    public Object clone() {
        return new AsnNull();
    }

    public int bEncContent(AsnBuf buf) {
        return 0;
    }

    public int bDecContent(InputStream in, int tagId, int elmtLen)
        throws Exception {
        if (elmtLen != 0) throw new Exception("NULL values len is non-zero");
        return 0;
    }

    public int bEnc(AsnBuf buf)
        throws Exception {
        int l = 0;
        buf.putByteRvs(l);
        l++;
        l += bEncTag(buf, UNIV, PRIM, NULLTYPE_TAG_CODE);
        return l;
    }

    public int bDec(InputStream in)
        throws Exception {
        int bd = bytesDecoded;
        if (bDecTag(in) != makeTagId(UNIV, PRIM, NULLTYPE_TAG_CODE))
            throw new Exception("Tag on NULL is wrong.");
        int elmtLen = bDecLen(in);
        bDecContent(in, makeTagId(UNIV, PRIM, NULLTYPE_TAG_CODE), elmtLen);
        return bytesDecoded - bd;
    }

    public void print(PrintStream p) {
        p.print("NULL");
    }

}
