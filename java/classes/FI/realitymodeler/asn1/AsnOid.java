
package FI.realitymodeler.asn1;

import FI.realitymodeler.common.*;
import java.io.*;

public class AsnOid extends AsnAny {
    protected byte oid[];

    public AsnOid() {
    }

    public AsnOid(byte oid[]) {
        this.oid = oid;
    }

    public AsnOid(AsnOid o) {
        oid = o.oid;
    }

    public AsnOid(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11) {
        set(a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11);
    }

    public AsnOid(int a1, int a2) {
        set(a1, a2, -1, -1, -1, -1, -1, -1, -1, -1, -1);
    }

    public Object clone() {
        return new AsnOid(oid);
    }

    public final byte[] getOid() {
        return oid;
    }

    /** Initializes an AsnOid with a string and it's length.
        The string should hold the encoded OID. */
    public void set(byte oid[]) {
        this.oid = oid;
    }

    /** Inits an AsnOid from another OID.
        The oid string is copied. */
    public void set(AsnOid o) {
        oid = o.oid;
    }

    /** Given some arc numbers, an AsnOid is built.
        set(1, 2, 3, 4, 5, -1, -1, -1, -1, -1, -1) results in
        oid { 1 2 3 4 5 }.  The first negative arc number represents
        the end of the arc numbers - at least 2 are required. */
    public void set(int a1, int a2, int a3, int a4, int a5, int a6, int a7, int a8, int a9, int a10, int a11) {
        int arcNumArr[] = new int[11], totalLen, elmtLen, tmpArcNum, headArcNum;
        ByteArrayOutputStream bout = new ByteArrayOutputStream();

        arcNumArr[0] = a1;
        arcNumArr[1] = a2;
        arcNumArr[2] = a3;
        arcNumArr[3] = a4;
        arcNumArr[4] = a5;
        arcNumArr[5] = a6;
        arcNumArr[6] = a7;
        arcNumArr[7] = a8;
        arcNumArr[8] = a9;
        arcNumArr[9] = a10;
        arcNumArr[10] = a11;

        // munge together first oid arc numbers
        headArcNum = tmpArcNum = (arcNumArr[0] * 40) + arcNumArr[1];

        // figure encoded length for this arc number
        for (elmtLen = 1; (tmpArcNum >>= 7) != 0; elmtLen++);
        // write bytes except the last/least significant of the head arc number
        // more bit is on
        totalLen = elmtLen;
        int k = 0;
        for (int i = 1; i < elmtLen; i++)
            bout.write(0x80 | (headArcNum >> ((elmtLen - i) * 7)));
        // write least significant (more bit is off)
        bout.write(0x7f & headArcNum);
        // repeat for the rest of the arc numbers
        for (int i = 2; (i < 11) && (arcNumArr[i] > 0); i++) {
            tmpArcNum = arcNumArr[i];
            for (elmtLen = 1; (tmpArcNum >>= 7) != 0; elmtLen++);
            totalLen += elmtLen;
            tmpArcNum = arcNumArr[i];
            for (int j = 1; j < elmtLen; j++)
                bout.write(0x80 | (tmpArcNum >> ((elmtLen - j) * 7)));
        }
        bout.write(0x7f & tmpArcNum);
        oid = bout.toByteArray();
    }

    /** Returns the number of arc numbers in the OID value */
    public int numArcs() {
        int i, numArcs = 0;

        for (i = 0; i < oid.length; ) {
            // skip octets in this arc num with the 'more' bit set
            for (; i < oid.length && (oid[i] & 0x80) != 0; i++);
            // skip last octet in this arc num (no more bit)
            i++;

            numArcs++;
        }

        // add one to return value because the first two arcs are
        // crunched together into a single one.
        return numArcs +1;
    }

    public boolean oidEquiv (AsnOid o) {
        return Support.compare(o.oid, oid) == 0;
    }

    public int bEncContent(AsnBuf buf) {
        buf.putSegRvs(oid);
        return oid.length;
    }

    /** Decodes the content of a BER OBJECT IDENTIFIER value and puts
        the results in this AsnOid object. */
    public int bDecContent(InputStream in, int tagId, int elmtLen)
        throws Exception {
        // treat like primitive octet string
        oid = new byte[elmtLen];
        copySeg(in, oid);
        return elmtLen;
    }

    public int bEnc(AsnBuf buf) {
        int l = bEncContent(buf);
        l += bEncDefLen(buf, l);
        l += bEncTag(buf, UNIV, PRIM, OID_TAG_CODE);
        return l;
    }

    public int bDec(InputStream in)
        throws Exception {
        int bd = bytesDecoded;
        if (bDecTag(in) != makeTagId(UNIV, PRIM, OID_TAG_CODE))
            throw new Exception("Tag on OBJECT IDENTIFIER is wrong.");
        int elmtLen = bDecLen(in);
        bDecContent(in, makeTagId(UNIV, PRIM, OID_TAG_CODE), elmtLen);
        return bytesDecoded - bd;
    }

    /** Prints an AsnOid in ASN.1 Value Notation.
        Decodes the oid to get the individual arc numbers. */
    public void print(PrintStream p) {
        int firstArcNum, arcNum, i;
        // print oid in
        p.print("{");
        if (oid != null) {
            // un-munge first two arc numbers
            arcNum = 0;
            for (i = 0; i < oid.length && (oid[i] & 0x80) != 0; i++)
                arcNum = (arcNum << 7) + (oid[i] & 0x7f);

            arcNum = (arcNum << 7) + (oid[i] & 0x7f);
            i++;
            firstArcNum = arcNum / 40;
            if (firstArcNum > 2) firstArcNum = 2;

            p.print(firstArcNum + " " + (arcNum - (firstArcNum * 40)));

            for (; i < oid.length; ) {
                for (arcNum = 0; (i < oid.length) && (oid[i] & 0x80) != 0; i++)
                    arcNum = (arcNum << 7) + (oid[i] & 0x7f);

                arcNum = (arcNum << 7) + (oid[i] & 0x7f);
                i++;
                p.print(" " + arcNum);
            }
        } else p.print("-- void --");
        p.print("}");
    }

}
