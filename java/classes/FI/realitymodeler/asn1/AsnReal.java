
package FI.realitymodeler.asn1;

import java.io.*;

public class AsnReal extends AsnAny {
    public static final int ENC_PLUS_INFINITY = 0x40;
    public static final int ENC_MINUS_INFINITY = 0x41;
    public static final int REAL_BINARY = 0x80;
    public static final int REAL_SIGN = 0x40;
    public static final int REAL_EXPLEN_MASK = 0x03;
    public static final int REAL_EXPLEN_1 = 0x00;
    public static final int REAL_EXPLEN_2 = 0x01;
    public static final int REAL_EXPLEN_3 = 0x02;
    public static final int REAL_EXPLEN_LONG = 0x03;
    public static final int REAL_FACTOR_MASK = 0x0c;
    public static final int REAL_BASE_MASK = 0x30;
    public static final int REAL_BASE_2 = 0x00;
    public static final int REAL_BASE_8 = 0x10;
    public static final int REAL_BASE_16 = 0x20;

    protected double value;

    public AsnReal() {
    }

    public AsnReal(double value) {
        this.value = value;
    }

    public Object clone() {
        return new AsnReal(value);
    }

    public final double getValue() {
        return value;
    }

    /** Returns the smallest octet length needed to hold the given long int value */
    int signedIntOctetLen(int val) {
        int mask = 0x7f80 << 16, retVal = 4;
        if (val < 0) val = val ^ (~0);

        while (retVal > 1 && (val & mask) == 0) {
            mask >>= 8;
            retVal--;
        }
        return retVal;
    }

    /** Use this routine if you system/compiler represents doubles in the IEEE format. */
    public int bEncContent(AsnBuf buf) {
        // no contents for 0.0 reals
        if (value == 0.0) return 0;
        long val = Double.doubleToLongBits(value);
        boolean isNeg = ((val >> 63) & 1) != 0;
        // special real values for +/- oo
        if (Double.isInfinite(value)) {
            if (isNeg) buf.putByteRvs(ENC_MINUS_INFINITY);
            else buf.putByteRvs(ENC_PLUS_INFINITY);
            return 1;
        }
        // encode a binary real value
        int exponent = (int)((val >> 52) & 0x7ff);
        int mantissa = (int)((val & 0xfffffffffffffL) | 0x10000000000000L);

        for (int i = 0; i < 7; i++) {
            buf.putByteRvs(mantissa & 0xff);
            mantissa >>= 8;
        }
        exponent -= (1023 + 52);

        // write the exponent
        buf.putByteRvs (exponent & 0xff);
        buf.putByteRvs (exponent >> 8);
        // write format octet */
        // bb is 00 since base is 2 so do nothing */
        // ff is 00 since no other shifting is nec */
        if (isNeg) buf.putByteRvs (REAL_BINARY | REAL_EXPLEN_2 | REAL_SIGN);
        else buf.putByteRvs (REAL_BINARY | REAL_EXPLEN_2);
        return 10;
    }

    /** Decode a REAL value's content from the given input stream. Places the result in this object. */
    public int bDecContent(InputStream in, int tagId, int elmtLen)
        throws Exception {
        byte firstOctet, firstExpOctet;
        int i, expLen;
        double mantissa;
        short base;
        int exponent = 0;
        double tmpBase;
        double tmpExp;

        if (elmtLen == 0) {
            value = 0.0;
            return 0;
        }

        firstOctet = (byte)getByte(in);
        if (elmtLen == 1) {
            if (firstOctet == ENC_PLUS_INFINITY) value = Double.POSITIVE_INFINITY;
            else if (firstOctet == ENC_MINUS_INFINITY) value = Double.NEGATIVE_INFINITY;
            else throw new Exception("Unrecognized 1 octet length real number");
        } else if ((firstOctet & REAL_BINARY) != 0) {
            firstExpOctet = (byte)getByte(in);
            if ((firstExpOctet & 0x80) != 0) exponent = -1;
            switch (firstOctet & REAL_EXPLEN_MASK) {
            case REAL_EXPLEN_1:
                expLen = 1;
                exponent =  (exponent << 8) | firstExpOctet;
                break;

            case REAL_EXPLEN_2:
                expLen = 2;
                exponent =  (exponent << 16) | (firstExpOctet << 8) | getByte(in);
                break;

            case REAL_EXPLEN_3:
                expLen = 3;
                exponent =  (exponent << 16) | (firstExpOctet << 8) | getByte(in);
                exponent =  (exponent << 8) | getByte(in);
                break;

            default: // long form
                expLen = firstExpOctet +1;
                i = firstExpOctet - 1;
                firstExpOctet = (byte)getByte(in);
                if ((firstExpOctet & 0x80) != 0)
                    exponent = (-1 << 8) | firstExpOctet;
                else exponent = firstExpOctet;
                for (;i > 0; firstExpOctet--)
                    exponent = (exponent << 8) | getByte(in);
                break;
            }

            mantissa = 0.0;
            for (i = 1 + expLen; i < elmtLen; i++) {
                mantissa *= 1 << 8;
                mantissa += getByte(in);
            }

            // adjust N by scaling factor
            mantissa *= (1 << ((firstOctet & REAL_FACTOR_MASK) >> 2));

            switch (firstOctet & REAL_BASE_MASK) {
            case REAL_BASE_2:
                base = 2;
                break;

            case REAL_BASE_8:
                base = 8;
                break;

            case REAL_BASE_16:
                base = 16;
                break;

            default:
                throw new Exception("Unsupported base for a binary real number.");
            }
            tmpBase = base;
            tmpExp = exponent;
            value =  mantissa * Math.pow((double)base, (double)exponent);
            if ((firstOctet & REAL_SIGN) != 0) value = -value;
        } else throw new Exception("Decimal REAL form is not currently supported");
        return elmtLen;
    }

    public int bEnc(AsnBuf buf) {
        int l = bEncContent(buf);
        l += bEncDefLen(buf, l);
        l += bEncTag(buf, UNIV, PRIM, REAL_TAG_CODE);
        return l;
    }

    public int bDec(InputStream in)
        throws Exception {
        int bd = bytesDecoded;
        if (bDecTag(in) != makeTagId(UNIV, PRIM, REAL_TAG_CODE))
            throw new Exception("Tag on REAL is wrong.");
        int elmtLen = bDecLen(in);
        bDecContent(in, makeTagId(UNIV, PRIM, REAL_TAG_CODE), elmtLen);
        return bytesDecoded - bd;
    }

    public void print(PrintStream p) {
        p.print(value);
    }

}
