
package FI.realitymodeler.asn1;

import java.io.*;

public abstract class AsnAny implements Cloneable {
    public static final int ANY_CLASS = -2,
        NULL_CLASS = -1,
        UNIV = 0,
        APPL = 1 << 1,
        CNTX = 2 << 1,
        PRIV = 3 << 1;
    public static final int ANY_FORM = -2,
        NULL_FORM = -1,
        PRIM = 0,
        CONS = 1;
    public static final int NO_TAG_CODE = 0,
        BOOLEAN_TAG_CODE = 1,
        INTEGER_TAG_CODE = 2,
        BITSTRING_TAG_CODE = 3,
        OCTETSTRING_TAG_CODE = 4,
        NULLTYPE_TAG_CODE = 5,
        OID_TAG_CODE = 6,
        OD_TAG_CODE = 7,
        EXTERNAL_TAG_CODE = 8,
        REAL_TAG_CODE = 9,
        ENUM_TAG_CODE = 10,
        SEQ_TAG_CODE =  16,
        SET_TAG_CODE = 17,
        NUMERICSTRING_TAG_CODE = 18,
        PRINTABLESTRING_TAG_CODE = 19,
        TELETEXSTRING_TAG_CODE = 20,
        VIDEOTEXSTRING_TAG_CODE = 21,
        IA5STRING_TAG_CODE = 22,
        UTCTIME_TAG_CODE = 23,
        GENERALIZEDTIME_TAG_CODE = 24,
        GRAPHICSTRING_TAG_CODE = 25,
        VISIBLESTRING_TAG_CODE = 27,
        GENERALSTRING_TAG_CODE = 28,
        TT61STRING_TAG_CODE = TELETEXSTRING_TAG_CODE,
        ISO646STRING_TAG_CODE = VISIBLESTRING_TAG_CODE;
    public static final int EOC_TAG_ID = 0;
    public static final int INDEFINITE_LEN = ~0;
    public static final int BooleanTag = makeTagId(UNIV, PRIM, BOOLEAN_TAG_CODE),
        IntegerTag = makeTagId(UNIV, PRIM, INTEGER_TAG_CODE),
        BitStringTag = makeTagId(UNIV, PRIM, BITSTRING_TAG_CODE),
        OctetStringTag = makeTagId(UNIV, PRIM, OCTETSTRING_TAG_CODE),
        NullTypeTag = makeTagId(UNIV, PRIM, NULLTYPE_TAG_CODE),
        OidTag = makeTagId(UNIV, PRIM, OID_TAG_CODE),
        RealTag = makeTagId(UNIV, PRIM, REAL_TAG_CODE),
        EnumTag = makeTagId(UNIV, PRIM, ENUM_TAG_CODE),
        SeqTag = makeTagId(UNIV, CONS, SEQ_TAG_CODE),
        NumericStringTag = makeTagId(UNIV, PRIM, NUMERICSTRING_TAG_CODE),
        PrintableStringTag = makeTagId(UNIV, PRIM, PRINTABLESTRING_TAG_CODE),
        TeletexStringTag = makeTagId(UNIV, PRIM, TELETEXSTRING_TAG_CODE),
        VideotexStringTag = makeTagId(UNIV, PRIM, VIDEOTEXSTRING_TAG_CODE),
        IA5StringTag = makeTagId(UNIV, PRIM, IA5STRING_TAG_CODE),
        UTCTimeTag = makeTagId(UNIV, PRIM, UTCTIME_TAG_CODE),
        GeneralizedTimeTag = makeTagId(UNIV, PRIM, GENERALIZEDTIME_TAG_CODE),
        GraphicStringTag = makeTagId(UNIV, PRIM, GRAPHICSTRING_TAG_CODE),
        VisibleStringTag = makeTagId(UNIV, PRIM, VISIBLESTRING_TAG_CODE),
        GeneralStringTag = makeTagId(UNIV, PRIM, GENERALSTRING_TAG_CODE),
        EocTag = makeTagId(UNIV, PRIM, EOC_TAG_ID);

    public static boolean indefiniteLength = false;

    public int bytesDecoded = 0;

    public static final int makeTagId(int cl, int fm, int cd) {
        return cl | fm | cd << 3;
    }

    public static final int tagIdClass(int tid) {
        return tid & 0x3 << 1;
    }

    public static final int tagIdForm(int tid) {
        return tid & 0x1;
    }

    public static final int tagIdCode(int tid) {
        return tid >> 3;
    }

    /** Evaluates to true if the given AsnTag type tag has the constructed bit set. */
    public static final boolean tagIsCons(int tid) {
        return (tid & CONS) != 0;
    }

    public final int getByte(InputStream in)
        throws Exception {
        int c = in.read();
        if (c == -1) throw new Exception("End of stream");
        bytesDecoded++;
        return c;
    }

    public final int copySeg(InputStream in, byte dst[])
        throws Exception {
        int i = 0, n;
        while (i < dst.length && (n = in.read(dst, i, dst.length - i)) >= 0) i += n;
        if (i < dst.length) throw new Exception("End of stream");
        bytesDecoded += i;
        return i;
    }

    public int bEncTag(AsnBuf buf, int cl, int fm, int cd) {
        if (cd < 31) {
            buf.putByteRvs((cl | fm) << 5 | cd);
            return 1;
        }
        int dp = buf.dataPos;
        buf.putByteRvs(cd & 0x7f);
        while ((cd >>= 7) != 0) buf.putByteRvs(0x80 | cd & 0x7f);
        buf.putByteRvs((cl | fm) << 5 | 0x1f);
        return dp - buf.dataPos;
    }

    public int bEncTag(AsnBuf buf, int tid) {
        return bEncTag(buf, tagIdClass(tid), tagIdForm(tid), tagIdCode(tid));
    }

    public int bDecTag(InputStream in)
        throws Exception {
        int t = getByte(in);
        if ((t & 0x1f) != 0x1f) return (t & 0x1f) << 3 | t >> 5;
        int c, v = 0;
        for (;;) {
            c = getByte(in);
            v |= c & 0x7f;
            if ((c & 0x80) == 0) break;
            v <<= 7;
        }
        return v << 3 | t >> 5;
    }

    public int bEncDefLen(AsnBuf buf, int len) {
        if (len < 128) {
            buf.putByteRvs(len);
            return 1;
        }
        int n = 0;
        while (len != 0) {
            buf.putByteRvs(len & 0xff);
            len >>= 8;
            n++;
        }
        buf.putByteRvs(0x80 + n);
        return n + 1;
    }

    public final int bEncIndefLen(AsnBuf buf) {
        buf.putByteRvs(0x80);
        return 1;
    }

    public final int bEncConsLen(AsnBuf buf, int len) {
        // include len for EOC
        return indefiniteLength ? 2 + bEncIndefLen(buf) : bEncDefLen(buf, len);
    }

    /** Decode a BER length from the given input stream. */
    public int bDecLen(InputStream in)
        throws Exception {
        int len, lenBytes;
        int c = getByte(in);
        if (c < 128) return c;
        if (c == 0x080) return INDEFINITE_LEN;
        // strip high bit to get # bytes left in len
        lenBytes = c & 0x7f;
        if (lenBytes > 4) throw new Exception("Length overflow");
        for (len = 0; lenBytes > 0; lenBytes--)
            len = (len << 8) | getByte(in);
        return len;
    }

    /** Encodes an End of Contents (EOC) to the given buffer. Returns the encoded length. */
    public final int bEncEoc(AsnBuf buf) {
        if (!indefiniteLength) return 0;
        buf.putByteRvs(0);
        buf.putByteRvs(0);
        return 2;
    }

    /** Decodes an EOC from the given input stream. */
    public final int bDecEoc(InputStream in)
        throws Exception {
        if (!indefiniteLength) return 0;
        if (getByte(in) != 0 || getByte(in) != 0) throw new Exception("Non zero byte in EOC");
        return 2;
    }

    public final int bDec2ndEocOctet(InputStream in)
        throws Exception {
        if (!indefiniteLength) return 0;
        if (getByte(in) != 0) throw new Exception("Non zero byte in EOC");
        return 1;
    }

    public AsnAny bDecAny(InputStream in)
        throws Exception {
        AsnAny aa;
        int tag = bDecTag(in), len = bDecLen(in);
        if (tag == BooleanTag) aa = new AsnBool();
        else if (tag == IntegerTag || tag == EnumTag) aa = new AsnInt();
        else if (tag == BitStringTag) aa = new AsnBits();
        else if (tag == OctetStringTag) aa = new AsnOcts();
        else if (tag == NullTypeTag) aa = new AsnNull();
        else if (tag == OidTag) aa = new AsnOid();
        else if (tag == RealTag) aa = new AsnReal();
        else if (tag == SeqTag) aa = new AsnSeq();
        else if (tag == NumericStringTag) aa = new AsnOcts(NUMERICSTRING_TAG_CODE);
        else if (tag == PrintableStringTag) aa = new AsnOcts(PRINTABLESTRING_TAG_CODE);
        else if (tag == TeletexStringTag) aa = new AsnOcts(TELETEXSTRING_TAG_CODE);
        else if (tag == VideotexStringTag) aa = new AsnOcts(VIDEOTEXSTRING_TAG_CODE);
        else if (tag == IA5StringTag) aa = new AsnOcts(IA5STRING_TAG_CODE);
        else if (tag == UTCTimeTag) aa = new AsnOcts(UTCTIME_TAG_CODE);
        else if (tag == GeneralizedTimeTag) aa = new AsnOcts(GENERALIZEDTIME_TAG_CODE);
        else if (tag == GraphicStringTag) aa = new AsnOcts(GRAPHICSTRING_TAG_CODE);
        else if (tag == GeneralStringTag) aa = new AsnOcts(GENERALSTRING_TAG_CODE);
        else if (tag == EocTag) return null;
        else throw new Exception("Unsupported tag");
        aa.bDecContent(in, tag, len);
        return aa;
    }

    public abstract Object clone();

    public abstract int bEncContent(AsnBuf buf)
        throws Exception;

    public abstract int bDecContent(InputStream in, int tagId, int elmtLen)
        throws Exception;

    public abstract int bEnc(AsnBuf buf)
        throws Exception;

    public abstract int bDec(InputStream in)
        throws Exception;

    public abstract void print(PrintStream p);

}
