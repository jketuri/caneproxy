
package FI.realitymodeler.asn1;

import java.io.*;

public class AsnBool extends AsnAny {
    protected boolean value;

    public AsnBool(boolean value) {
        this.value = value;
    }

    public AsnBool() {
    }

    public Object clone() {
        return new AsnBool(value);
    }

    boolean getValue() {
        return value;
    }

    public int bEnc(AsnBuf buf) {
        int l = bEncContent(buf);
        buf.putByteRvs(l);
        l++;
        l += bEncTag(buf, UNIV, PRIM, BOOLEAN_TAG_CODE);
        return l;
    }

    public int bDec(InputStream in)
        throws Exception {
        int bd = bytesDecoded;
        if (bDecTag(in) != makeTagId(UNIV, PRIM, BOOLEAN_TAG_CODE))
            throw new Exception("Tag on BOOLEAN wrong.");
        int elmtLen = bDecLen(in);
        bDecContent(in, makeTagId(UNIV, PRIM, BOOLEAN_TAG_CODE), elmtLen);
        return bytesDecoded - bd;
    }

    /** Decodes the content of a BOOLEAN and sets this object's value
        to the decoded value. Flags an error if the length is wrong
        or a read error occurs. */
    public int bDecContent(InputStream in, int tagId, int elmtLen)
        throws Exception {
        if (elmtLen != 1) throw new Exception("Boolean value too long.");
        value = (getByte(in) != 0);
        return 1;
    }

    public int bEncContent(AsnBuf buf) {
        buf.putByteRvs(value ? 0xFF : 0);
        return 1;
    }

    /** print the BOOLEAN's value in ASN.1 value notation to the given ostream */
    public void print(PrintStream p) {
        p.print(value ? "TRUE" : "FALSE");
    }

}
