
package FI.realitymodeler.asn1;

import java.io.*;

public class AsnChoice extends AsnAny {
    protected int tag;
    protected AsnAny value;

    public AsnChoice(int tag, AsnAny value) {
        set(tag, value);
    }

    public AsnChoice() {
    }

    public Object clone() {
        return new AsnChoice(tag, value);
    }

    public final int getTag() {
        return tag;
    }

    public final AsnAny getValue() {
        return value;
    }

    public void set(int tag, AsnAny value) {
        this.tag = tag;
        this.value = value;
    }

    public void set(AsnChoice choice) {
        set(choice.tag, choice.value);
    }

    public int bEncContent(AsnBuf buf)
        throws Exception {
        int l = bEncEoc(buf);
        l += value.bEnc(buf);
        return l;
    }

    public int bDecContent(InputStream in, int tagId, int elmtLen)
        throws Exception {
        int bd = bytesDecoded;
        value = bDecAny(in);
        if (value == null) throw new Exception("No choice value");
        bytesDecoded += value.bytesDecoded;
        bDecEoc(in);
        return bytesDecoded - bd;
    }

    public int bEnc(AsnBuf buf)
        throws Exception {
        int l = bEncContent(buf);
        l += bEncConsLen(buf, l);
        l += bEncTag(buf, tag);
        return l;
    }

    public int bDec(InputStream in)
        throws Exception {
        int bd = bytesDecoded;
        tag = bDecTag(in);
        int elmtLen = bDecLen(in);
        bDecContent(in, tag, elmtLen);
        return bytesDecoded - bd;
    }

    public void print(PrintStream p) {
        p.print(tagIdCode(tag) + " ");
        value.print(p);
    }

}
