
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;

/** Implements URL connection to current class loader. URL must be of form:<br>
    resource:path
*/
public class W3ResourceURLConnection extends W3FtpURLConnection {

    public W3ResourceURLConnection(URL url) {
        super(url);
    }

    public void doConnect() {
    }

    public InputStream getInputStream() throws IOException {
        if (!url.getHost().equals("")) return super.getInputStream();
        if (inputDone) return in;
        mayKeepAlive = true;
        inputDone = true;
        if (doOutput) {
            headerFields = new HeaderList();
            headerFields.append(new Header("", protocol_1_1 + " " + HTTP_NO_CONTENT));
            return in = null;
        }
        String name = url.getFile();
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        if (classLoader != null) in = classLoader.getResourceAsStream(name);
        if (in == null) in = getClass().getResourceAsStream(name);
        if (in == null) throw new IOException("Resource " + name + " not found");
        return in;
    }

    public OutputStream getOutputStream() throws IOException {
        if (!url.getHost().equals("")) return super.getOutputStream();
        doOutput = true;
        return sink = null;
    }

}
