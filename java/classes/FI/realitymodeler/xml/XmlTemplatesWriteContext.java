
package FI.realitymodeler.xml;

import FI.realitymodeler.beans.*;
import org.apache.crimson.parser.*;
import org.apache.crimson.tree.*;
import java.io.*;
import java.util.*;
import javax.servlet.http.*;

public class XmlTemplatesWriteContext extends XmlWriteContext
{
	BeanContainer beanContainers[];
	HttpServletRequest request;
	String suffix;
	XmlTemplates xmlTemplates;
	int charCount;

public XmlTemplatesWriteContext(Writer writer, BeanContainer beanContainers[], HttpServletRequest request, XmlTemplates xmlTemplates, String suffix, int charCount)
{
	super(writer);
	this.beanContainers = beanContainers;
	this.request = request;
	this.xmlTemplates = xmlTemplates;
	this.suffix = suffix;
	this.charCount = charCount;
}

}