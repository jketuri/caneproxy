
package FI.realitymodeler.xml;

import java.io.*;

public class RedirectException extends IOException
{

public RedirectException(String location)
{
	super(location);
}

public String getLocation()
{
	return getMessage();
}

}