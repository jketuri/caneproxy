
package FI.realitymodeler.xml;

import FI.realitymodeler.beans.*;
import FI.realitymodeler.common.*;
import org.apache.crimson.parser.*;
import org.apache.crimson.tree.*;
import java.io.*;
import java.lang.reflect.*;
import java.text.*;
import java.util.*;
import org.w3c.dom.*;

public class XmlTemplatesSetPropertyElementNode extends XmlTemplatesContainerElementNode
{

public void writeXml(XmlWriteContext context) throws IOException
{
	String index = null, name = null, param = null, property = null, value = null;
	NamedNodeMap attributes = getAttributes();
	for (int i = attributes.getLength() - 1; i >= 0; i--)
	{
		Node attribute = attributes.item(i);
		String attributeName = attribute.getNodeName(), attributeValue = attribute.getNodeValue();
		if (attributeName.equals("index")) index = attributeValue;
		else if (attributeName.equals("name")) name = attributeValue;
		else if (attributeName.equals("param")) param = attributeValue;
		else if (attributeName.equals("property")) property = attributeValue;
		else if (attributeName.equals("value")) value = attributeValue;
		else throw new IOException("Unknown attribute " + attributeName + " in tag " + getTagName());
	}
	if (property == null || property.equals("")) throw new IOException("Attribute 'property' is missing from tag '" + getTagName() + "'");
	if (name == null || name.equals(""))
	{
		StringTokenizer st = new StringTokenizer(property, ":");
		name = st.nextToken().trim();
		property = st.hasMoreTokens() ? st.nextToken("").substring(1) : null;
	}
	if (index != null && index.equals("")) index = null;
	if (param != null && param.equals("")) param = null;
	Object bean = BeanContainer.getBean(name, ((XmlTemplatesWriteContext)context).beanContainers);
	if (bean == null) throw new IOException("Bean " + name + " not found in tag " + getTagName());
	Exception ex = null;
	try
	{
		if (value != null)
		{
			if (index != null) BeanContainer.setBeanProperty(bean, name, property, Integer.parseInt(index), value);
			else BeanContainer.setBeanProperty(bean, name, property, value);
			return;
		}
		if (property.equals("*"))
		{
			Enumeration parameterNames = ((XmlTemplatesWriteContext)context).request.getParameterNames();
			while (parameterNames.hasMoreElements())
			{
				param = (String)parameterNames.nextElement();
				BeanContainer.setBeanProperty(bean, name, "request:" + param, ((XmlTemplatesWriteContext)context).request.getParameterValues(param));
			}
		}
		else if (index != null) BeanContainer.setBeanProperty(bean, name, property, Integer.parseInt(index), ((XmlTemplatesWriteContext)context).request.getParameterValues(param != null ? param : property));
		else BeanContainer.setBeanProperty(bean, name, property, ((XmlTemplatesWriteContext)context).request.getParameterValues(param != null ? param : property));
	}
	catch (IllegalAccessException ex1)
	{
		ex = ex1;
	}
	catch (InstantiationException ex1)
	{
		ex = ex1;
	}
	catch (InvocationTargetException ex1)
	{
		ex = ex1;
	}
	catch (ParseException ex1)
	{
		ex = ex1;
	}
	if (ex != null) throw new IOException(Support.stackTrace(ex));
}

}
