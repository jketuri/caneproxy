
package FI.realitymodeler.xml;

import FI.realitymodeler.beans.*;
import FI.realitymodeler.common.*;
import org.apache.crimson.parser.*;
import org.apache.crimson.tree.*;
import java.beans.*;
import java.io.*;
import java.lang.reflect.*;
import java.text.*;
import java.util.*;
import javax.servlet.*;
import org.w3c.dom.*;
import org.xml.sax.*;

public class XmlTemplatesUseBeanElementNode extends XmlTemplatesContainerElementNode
{

public void writeXml(XmlWriteContext context) throws IOException
{
	String beanName = null, clazz = null, id = null, scope = null, type = null;
	NamedNodeMap attributes = getAttributes();
	for (int i = attributes.getLength() - 1; i >= 0; i--)
	{
		Node attribute = attributes.item(i);
		String attributeName = attribute.getNodeName(), attributeValue = attribute.getNodeValue();
		if (attributeName.equals("beanName")) beanName = attributeValue;
		else if (attributeName.equals("class")) clazz = attributeValue;
		else if (attributeName.equals("id")) id = attributeValue;
		else if (attributeName.equals("scope")) scope = attributeValue;
		else if (attributeName.equals("type")) type = attributeValue;
		else throw new IOException("Unknown attribute " + attributeName + " in tag " + getTagName());
	}
	if (id == null || id.equals("")) throw new IOException("Attribute 'id' is missing from tag '" + getTagName() + "'");
	BeanContainer beanContainer;
	if (scope == null || scope.equals("") || scope.equals("page")) beanContainer = ((XmlTemplatesWriteContext)context).beanContainers[XmlTemplates.PAGE_SCOPE];
	else if (scope.equals("request")) beanContainer = ((XmlTemplatesWriteContext)context).beanContainers[XmlTemplates.REQUEST_SCOPE];
	else if (scope.equals("session")) beanContainer = ((XmlTemplatesWriteContext)context).beanContainers[XmlTemplates.SESSION_SCOPE];
	else if (scope.equals("session")) beanContainer = ((XmlTemplatesWriteContext)context).beanContainers[XmlTemplates.APPLICATION_SCOPE];
	else throw new IOException("Unknown scope " + scope + " in tag " + getTagName());
	Exception ex = null;
	try
	{
		Object bean;
		synchronized (beanContainer)
		{
			if (!beanContainer.containsBean(id))
			{
				if ((beanName == null || beanName.equals("")) &&
				(beanName = clazz) == null || beanName.equals("")) throw new IOException("Both attributes beanName and class are missing from tag " + getTagName());
				bean = beanContainer.createBean(id, beanName);
			}
			else bean = null;
		}
		if (bean == null) return;
		try
		{
			beanContainer.setBeanProperty(bean, beanName, "servletConfig", ((XmlTemplatesWriteContext)context).xmlTemplates.getServletConfig());
		}
		catch (ParseException ex1) {}
		super.writeChildrenXml(context);
		beanContainer.invokeMethod(bean, "processRequest", new Object[] {((XmlTemplatesWriteContext)context).request});
	}
	catch (ClassNotFoundException ex1)
	{
		ex = ex1;
	}
	catch (IntrospectionException ex1)
	{
		ex = ex1;
	}
	catch (InstantiationException ex1)
	{
		ex = ex1;
	}
	catch (IllegalAccessException ex1)
	{
		ex = ex1;
	}
	catch (InvocationTargetException ex1)
	{
		Throwable th = ex1.getTargetException();
		if (th instanceof RedirectException) throw (RedirectException)th;
		ex = ex1;
	}
	catch (ParseException ex1)
	{
		ex = ex1;
	}
	if (ex != null) throw new IOException(Support.stackTrace(ex));
}

}