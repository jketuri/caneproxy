
package FI.realitymodeler.xml;

import FI.realitymodeler.*;
import FI.realitymodeler.beans.*;
import FI.realitymodeler.common.*;
import FI.realitymodeler.server.*;
import org.apache.crimson.parser.*;
import org.apache.crimson.tree.*;
import org.apache.xalan.processor.*;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import org.w3c.dom.*;
import org.xml.sax.*;
import org.xml.sax.helpers.*;

class XmlTemplate
{
	XmlDocument xmlDocument;
	long lastModified;
	int charCount = 0;

XmlTemplate(XmlDocument xmlDocument, long lastModified)
{
	this.xmlDocument = xmlDocument;
	this.lastModified = lastModified;
}

}

class TransformerThread extends Thread
{
	Exception ex = null;
    Transformer transformer;
    Source source;
    Result result;

    TransformerThread(Transformer transformer, Source source, Result result)
{
    this.transformer = transformer;
    this.source = source;
    this.result = result;
}

public void run()
{
	try
	{
	    transformer.transform(source, result);
	}
	catch (Exception ex1)
	{
		ex = ex1;
	}
}

}

public class XmlTemplates extends HttpServlet implements ErrorHandler, EntityResolver
{
	public static final int PAGE_SCOPE = 0, REQUEST_SCOPE = 1, SESSION_SCOPE = 2, APPLICATION_SCOPE = 3;
	public static SimpleElementFactory xmlTemplatesElementFactory = null;
	public static HashMap xmlTemplates = new HashMap();
    static DocumentBuilderFactory documentBuilderFactory = null;
	static
	{
		try
		{
			BeanContainer.prepare();
			xmlTemplatesElementFactory = new SimpleElementFactory();
			java.util.Hashtable mappings = new java.util.Hashtable();
			mappings.put("xml:container", Class.forName("FI.realitymodeler.xml.XmlTemplatesContainerElementNode"));
			mappings.put("xml:display", Class.forName("FI.realitymodeler.xml.XmlTemplatesDisplayElementNode"));
			mappings.put("xml:excludeIf", Class.forName("FI.realitymodeler.xml.XmlTemplatesExcludeIfElementNode"));
			mappings.put("xml:getProperty", Class.forName("FI.realitymodeler.xml.XmlTemplatesGetPropertyElementNode"));
			mappings.put("xml:includeIf", Class.forName("FI.realitymodeler.xml.XmlTemplatesIncludeIfElementNode"));
			mappings.put("xml:loop", Class.forName("FI.realitymodeler.xml.XmlTemplatesLoopElementNode"));
			mappings.put("xml:setProperty", Class.forName("FI.realitymodeler.xml.XmlTemplatesSetPropertyElementNode"));
			mappings.put("xml:useBean", Class.forName("FI.realitymodeler.xml.XmlTemplatesUseBeanElementNode"));
			xmlTemplatesElementFactory.addMapping(mappings, null);
			xmlTemplatesElementFactory.setDefaultNamespace("xml");
            documentBuilderFactory = DocumentBuilderFactory.newInstance();
            documentBuilderFactory.setNamespaceAware(true);
            documentBuilderFactory.setIgnoringElementContentWhitespace(false);
		}
		catch (Exception ex)
		{
			throw new Error(ex.toString());
		}
	}

    TransformerFactory transformerFactory = null;
    FileNameMap fileNameMap = null;
    String defaultTemplate = null;
    String directory = null;
    String transletPrefix = null;
    boolean getContentTypeFromServletContext = false;
    boolean useTranslets = false;
    boolean verbose = false;

public void warning(SAXParseException exception)
{
	if (verbose) log(Support.stackTrace(exception));
}

public void error(SAXParseException exception)
{
	if (verbose) log(Support.stackTrace(exception));
}

public void fatalError(SAXParseException exception) throws SAXParseException
{
	throw exception;
}

public InputSource resolveEntity(String publicId, String systemId)
{
	return new InputSource(new ByteArrayInputStream(new byte[0]));
}

public static int countChars(org.w3c.dom.Node node)
{
	if (node == null) return 0;
	int count = 0;
	if (node instanceof Text)
	{
		String value = node.getNodeValue();
		if (value != null)
		if (value.length() > 0 && Character.isWhitespace(value.charAt(0)) &&
		(node.getPreviousSibling() instanceof XmlTemplatesContainerElementNode &&
		!(node.getPreviousSibling() instanceof XmlTemplatesDisplayElementNode) ||
		node.getPreviousSibling() == null))
		{
			int i = 1, l = value.length();
			while (i < l && Character.isWhitespace(value.charAt(i))) i++;
			node.setNodeValue(value.substring(i));
		}
		else count += value.length();
	}
	else if (node instanceof Element && !(node instanceof XmlTemplatesContainerElementNode))
	{
		count += 3;
		NamedNodeMap attributes = node.getAttributes();
		if (attributes != null)
		{
			int l = attributes.getLength();
			for (int i = 0; i < l; i++)
			{
				count += 2;
				String value = attributes.item(i).getNodeValue();
				if (value != null) count += value.length();
			}
		}
	}
	NodeList childNodes = node.getChildNodes();
	if (childNodes != null)
	{
		int l = childNodes.getLength();
		for (int i = 0; i < l; i++) count += countChars((org.w3c.dom.Node)childNodes.item(i));
	}
	if (node instanceof XmlTemplatesLoopElementNode)
	{
		((XmlTemplatesLoopElementNode)node).charCount = count;
		return 0;
	}
	return count;
}

public void init(ServletConfig config) throws ServletException
{
	super.init(config);
	Exception ex = null;
	try
	{
		Enumeration initParameterNames = config.getInitParameterNames();
		while (initParameterNames.hasMoreElements())
		{
			String name = (String)initParameterNames.nextElement(), value = config.getInitParameter(name);
			if (name.equals("directory")) directory = Support.decode(value);
			else if (name.equals("defaultTemplate")) defaultTemplate = Support.decode(value);
			else if (name.equals("verbose"))
			{
				if (Support.isTrue(value, "verbose")) verbose = true;
			}
			else if (name.equals("getContentTypeFromServletContext"))
			{
				if (Support.isTrue(value, "getContentTypeFromServletContext")) getContentTypeFromServletContext = true;
			}
			else if (name.equals("transletPrefix")) transletPrefix = value;
			else if (name.equals("useTranslets"))
			{
				if (Support.isTrue(value, "useTranslets")) useTranslets = true;
			}
			else if (name.indexOf(':') == -1) throw new ServletException("Unknown initialization parameter " + name);
		}
		if (defaultTemplate != null && (defaultTemplate = defaultTemplate.trim()).equals("")) throw new ServletException("Parameter 'defaultTemplate' is empty");
		if (directory == null) throw new ServletException("Parameter 'directory' is not specified");
		if (config.getServletContext().getAttribute("FI.realitymodeler.server.W3Context/verbose") == Boolean.TRUE) verbose = true;
		fileNameMap = MimeMap.readMimeMap(new File(directory, "mime_types").getCanonicalPath(), null);
	}
	catch (IOException ex1)
	{
		ex = ex1;
	}
	catch (ParseException ex1)
	{
		ex = ex1;
	}
	if (ex != null) throw new ServletException(Support.stackTrace(ex));
	if (!useTranslets) return;
}

public void service(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException
{
	String templateName = req.getPathInfo();
	if ((templateName == null || (templateName = templateName.trim()).equals("") || templateName.equals("/")) &&
	(defaultTemplate == null || (templateName = "/" + defaultTemplate).equals("/"))) throw new ServletException("Template name not specified");
	String fileName = templateName;
	if (useTranslets) fileName = fileName.substring(0, fileName.lastIndexOf('.') + 1) + "xml";
	File file = new File(directory, fileName);
	XmlTemplate xmlTemplate = (XmlTemplate)xmlTemplates.get(file.getCanonicalPath());
	if (xmlTemplate == null || xmlTemplate.lastModified < file.lastModified())
	{
		Exception ex = null;
		try
		{
			if (verbose) log("Parsing file " + file.getCanonicalPath());
			InputSource inputSource = Resolver.createInputSource(file);
			inputSource.setSystemId(req.getRequestURI());
			XMLReader xmlReader = XMLReaderFactory.createXMLReader();
			XmlDocumentBuilder xmlDocumentBuilder = new XmlDocumentBuilder();
			xmlReader.setContentHandler(xmlDocumentBuilder);
			xmlReader.setErrorHandler(this);
			xmlReader.setEntityResolver(this);
			xmlDocumentBuilder.setElementFactory(xmlTemplatesElementFactory);
			xmlDocumentBuilder.setIgnoringLexicalInfo(false);
			xmlDocumentBuilder.setDisableNamespaces(false);
			xmlReader.parse(inputSource);
			xmlTemplate = new XmlTemplate(xmlDocumentBuilder.getDocument(), file.lastModified());
			xmlTemplate.charCount = countChars(xmlTemplate.xmlDocument.getDocumentElement());
		}
		catch (SAXException ex1)
		{
			ex = ex1;
			Exception embeddedException = ex1.getException();
			if (ex instanceof SAXParseException) throw new ServletException(Support.stackTrace(ex) +
			"column = " + ((SAXParseException)ex).getColumnNumber() +
			", line = " + ((SAXParseException)ex).getLineNumber() +
			", publicId = " + ((SAXParseException)ex).getPublicId() +
			", systemId = " + ((SAXParseException)ex).getSystemId() +
			", file = " + file.getCanonicalPath() + "\n" +
			(embeddedException != null ? Support.stackTrace(embeddedException) : ""));
			throw new ServletException(Support.stackTrace(ex) +
			(embeddedException != null ? Support.stackTrace(embeddedException) : ""));
		}
		if (ex != null) throw new ServletException(Support.stackTrace(ex));
		xmlTemplates.put(file.getCanonicalPath(), xmlTemplate);
	}
	BeanContainer pageBeanContainer = new BeanContainer();
	try
	{
		String contentType = getContentTypeFromServletContext ? getServletConfig().getServletContext().getMimeType(templateName) : fileNameMap.getContentTypeFor(templateName);
		res.setStatus(res.SC_OK);
		res.setContentType(contentType != null ? contentType : Support.unknownType);
		res.setHeader("Cache-Control", "no-cache");
		res.setHeader("Pragma", "no-cache");
		String suffix = templateName.substring(templateName.lastIndexOf('.') + 1).toLowerCase();
// If xml-output is requested, translets are not used
		if (useTranslets && !suffix.equals("xml"))
		{
		    if (verbose) log("Transforming file " + file.getCanonicalPath());
// XSLT-Translet class is searched from xml-file-directory specified
// in initialization parameter 'directory'. Class name is prefixed
// with 'transletPrefix' servlet initialization parameter followed with
// character 2 and target file extension
		    transformerFactory = new TransformerFactoryImpl();
		    transformerFactory.setAttribute("translet-name", transletPrefix + "2" + suffix);
		    transformerFactory.setAttribute("destination-directory", directory);
		    Templates templates = transformerFactory.newTemplates(null);
		    Transformer transformer = templates.newTransformer();
		    
// XML-output is piped to XSLT-DOM parser and after that resulting DOM-tree
// is transformed through translet and delivered to servlet output stream.
		    PipedOutputStream pout = new PipedOutputStream();
		    PipedInputStream pin = new PipedInputStream(pout);
		    XmlTemplatesWriteContext context = new XmlTemplatesWriteContext(new OutputStreamWriter(pout), new BeanContainer[] {pageBeanContainer, pageBeanContainer, pageBeanContainer, BeanContainer.instance}, req, this, suffix, xmlTemplate.charCount);
		    InputSource inputSource = Resolver.createInputSource("text/xml;charset=iso-8859-1", pin, false, "http");
		    inputSource.setSystemId(req.getRequestURI());
		    DOMImplementationImpl dom = new DOMImplementationImpl();
		    XMLReader xmlReader = XMLReaderFactory.createXMLReader();
		    XmlDocumentBuilder xmlDocumentBuilder = new XmlDocumentBuilder();
		    xmlReader.setContentHandler(xmlDocumentBuilder);
		    xmlReader.setErrorHandler(this);
		    xmlReader.setEntityResolver(this);
		    TransformerThread transformerThread = new TransformerThread(transformer, new StreamSource(pin), new StreamResult(res.getOutputStream()));
		    transformerThread.start();
		    xmlTemplate.xmlDocument.writeXml(context);
		    context.getWriter().close();
		    transformerThread.join();
		    if (transformerThread.ex != null) throw transformerThread.ex;
		} else {
		    XmlTemplatesWriteContext context = new XmlTemplatesWriteContext(res.getWriter(), new BeanContainer[] {pageBeanContainer, pageBeanContainer, pageBeanContainer, BeanContainer.instance},
			req, this, suffix, xmlTemplate.charCount);
		    if (contentType != null &&
			(contentType = Support.getParameters(contentType, null).toLowerCase()).startsWith("text/vnd.wap.") ||
			contentType.equals("text/xml")) xmlTemplate.xmlDocument.writeXml(context);
		    else xmlTemplate.xmlDocument.writeChildrenXml(context);
		    context.getWriter().flush();
		    if (verbose) log("charCount=" + context.charCount);
		}
	}
	catch (Exception ex)
	{
		if (ex instanceof RedirectException)
		{
			res.sendRedirect(((RedirectException)ex).getLocation());
			return;
		}
		log(Support.stackTrace(ex));
		res.reset();
		res.setStatus(res.SC_INTERNAL_SERVER_ERROR);
		res.setContentType("text/html");
		PrintWriter writer = res.getWriter();
		writer.print(Support.htmlString(Support.stackTrace(ex)));
		writer.close();
	}
	finally
	{
		pageBeanContainer.close();
	}
}

public String getServletInfo()
{
	return getClass().getName();
}

}
