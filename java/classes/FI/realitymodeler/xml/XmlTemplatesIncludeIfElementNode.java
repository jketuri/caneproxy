
package FI.realitymodeler.xml;

import FI.realitymodeler.beans.*;
import FI.realitymodeler.common.*;
import org.apache.crimson.parser.*;
import org.apache.crimson.tree.*;
import java.io.*;
import java.lang.reflect.*;
import java.text.*;
import java.util.*;
import org.w3c.dom.*;

public class XmlTemplatesIncludeIfElementNode extends XmlTemplatesLoopElementNode
{

protected void writeIfConditionSatisfies(XmlTemplatesWriteContext xmlTemplatesWriteContext, boolean not) throws IOException
{
	String caseAttr = null, match = null, name = null, property = null, value = null, valueName = null, valueProperty = null;
	NamedNodeMap attributes = getAttributes();
	for (int i = attributes.getLength() - 1; i >= 0; i--)
	{
		Node attribute = attributes.item(i);
		String attributeName = attribute.getNodeName(), attributeValue = attribute.getNodeValue();
		if (attributeName.equals("caseAttr")) caseAttr = attributeValue;
		else if (attributeName.equals("match")) match = attributeValue;
		else if (attributeName.equals("name")) name = attributeValue;
		else if (attributeName.equals("property")) property = attributeValue;
		else if (attributeName.equals("value")) value = attributeValue;
		else if (attributeName.equals("valueName")) valueName = attributeValue;
		else if (attributeName.equals("valueProperty")) valueProperty = attributeValue;
		else throw new IOException("Unknown attribute " + attributeName + " in tag " + getTagName());
	}
	if (property == null || property.equals("")) throw new IOException("Attribute 'property' is missing from tag '" + getTagName() + "'");
	if (name == null || name.equals(""))
	{
		StringTokenizer st = new StringTokenizer(property, ":");
		name = st.nextToken().trim();
		property = st.hasMoreTokens() ? st.nextToken("").substring(1) : null;
	}
	Exception ex = null;
	try
	{
		if (valueName != null && !valueName.equals("") ||
		valueProperty != null && !valueProperty.equals(""))
		{
			if (valueName == null || valueName.equals(""))
			{
				StringTokenizer st = new StringTokenizer(valueProperty, ":");
				valueName = st.nextToken().trim();
				valueProperty = st.hasMoreTokens() ? st.nextToken("").substring(1) : null;
			}
			Object valueBean = BeanContainer.getBean(valueName, xmlTemplatesWriteContext.beanContainers),
			propertyValue = BeanContainer.getBeanProperty(valueBean, valueName, valueProperty);
			if (propertyValue != null) value = propertyValue instanceof String ? (String)propertyValue : propertyValue.toString();
		}
		Object bean = BeanContainer.getBean(name, xmlTemplatesWriteContext.beanContainers);
		Object propertyValue = BeanContainer.getBeanProperty(bean, name, property);
		if (propertyValue != null && !(propertyValue instanceof String)) propertyValue = propertyValue.toString();
		if (caseAttr == null || caseAttr.equals("") || caseAttr.equals("insensitive"))
		{
			if (value != null) value = value.toLowerCase();
			if (propertyValue != null) propertyValue = ((String)propertyValue).toLowerCase();
		}
		else if (!caseAttr.equals("sensitive")) throw new IOException("case-attribute has unknown value " + caseAttr + " in " + getTagName() + "-tag");
		boolean satisfies = false;
		if (match != null && match.equals("null")) satisfies = propertyValue == null;
		else if (value == null) throw new IOException("Attribute 'value' is missing from tag '" + getTagName() + "'");
		else if (propertyValue != null)
		if (match == null || match.equals("") || match.equals("exact")) satisfies = ((String)propertyValue).equals(value);
		else if (match.equals("contains")) satisfies = ((String)propertyValue).indexOf(value) != -1;
		else if (match.equals("isMember"))
		{
			StringTokenizer st = new StringTokenizer((String)propertyValue, ",");
			while (st.hasMoreTokens())
			if (st.nextToken().trim().equals(value))
			{
				satisfies = true;
				break;
			}
		}
		else if (match.equals("startsWith")) satisfies = ((String)propertyValue).startsWith(value);
		else if (match.equals("endsWith")) satisfies = ((String)propertyValue).endsWith(value);
		else throw new IOException("Attribute 'match' has unknown value in tag '" + getTagName() + "'");
		if (satisfies ^ not)
		{
			super.writeChildrenXml(xmlTemplatesWriteContext);
			xmlTemplatesWriteContext.charCount += charCount;
		}
	}
	catch (IllegalAccessException ex1)
	{
		ex = ex1;
	}
	catch (InvocationTargetException ex1)
	{
		ex = ex1;
	}
	catch (ParseException ex1)
	{
		ex = ex1;
	}
	if (ex != null) throw new IOException(Support.stackTrace(ex));
}

public void writeXml(XmlWriteContext context) throws IOException
{
	writeIfConditionSatisfies((XmlTemplatesWriteContext)context, false);
}

}