
package FI.realitymodeler.xml;

import FI.realitymodeler.common.*;
import org.apache.crimson.parser.*;
import org.apache.crimson.tree.*;
import java.io.*;
import org.w3c.dom.*;

public class XmlTemplatesDisplayElementNode extends XmlTemplatesContainerElementNode
{

public void writeXml(XmlWriteContext context) throws IOException
{
	String value = null;
	NamedNodeMap attributes = getAttributes();
	for (int i = attributes.getLength() - 1; i >= 0; i--)
	{
		Node attribute = attributes.item(i);
		String attributeName = attribute.getNodeName(), attributeValue = attribute.getNodeValue();
		if (attributeName.equals("value")) value = attributeValue;
		else throw new IOException("Unknown attribute " + attributeName + " in tag " + getTagName());
	}
	if (value == null || value.equals("")) throw new IOException("Attribute 'value is missing from tag '" + getTagName() + "'");
	context.getWriter().write(value);
}

}