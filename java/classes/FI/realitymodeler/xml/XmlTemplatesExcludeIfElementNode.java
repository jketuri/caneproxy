
package FI.realitymodeler.xml;

import org.apache.crimson.parser.*;
import org.apache.crimson.tree.*;
import java.io.*;

public class XmlTemplatesExcludeIfElementNode extends XmlTemplatesIncludeIfElementNode
{

public void writeXml(XmlWriteContext context) throws IOException
{
	writeIfConditionSatisfies((XmlTemplatesWriteContext)context, true);
}

}