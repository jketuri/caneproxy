
package FI.realitymodeler.xml;

import FI.realitymodeler.beans.*;
import FI.realitymodeler.common.*;
import org.apache.crimson.parser.*;
import org.apache.crimson.tree.*;
import java.beans.*;
import java.io.*;
import java.lang.reflect.*;
import java.text.*;
import java.util.*;
import org.w3c.dom.*;
import org.xml.sax.*;

public class XmlTemplatesLoopElementNode extends XmlTemplatesContainerElementNode
{
	int charCount = 0;

public void writeXml(XmlWriteContext context) throws IOException
{
	String charLimit = null, endIndex = null, name = null, property = null, propertyElement = null, startIndex = null;
	NamedNodeMap attributes = getAttributes();
	for (int i = attributes.getLength() - 1; i >= 0; i--)
	{
		Node attribute = attributes.item(i);
		String attributeName = attribute.getNodeName(), attributeValue = attribute.getNodeValue();
		if (attributeName.equals("charLimit")) charLimit = attributeValue;
		else if (attributeName.equals("endIndex")) endIndex = attributeValue;
		else if (attributeName.equals("name")) name = attributeValue;
		else if (attributeName.equals("property")) property = attributeValue;
		else if (attributeName.equals("propertyElement")) propertyElement = attributeValue;
		else if (attributeName.equals("startIndex")) startIndex = attributeValue;
		else throw new IOException("Unknown attribute " + attributeName + " in tag " + getTagName());
	}
	if (property == null || property.equals("")) throw new IOException("Attribute 'property' is missing from tag " + getTagName() + "'");
	if (name == null || name.equals(""))
	{
		StringTokenizer st = new StringTokenizer(property, ":");
		name = st.nextToken().trim();
		property = st.hasMoreTokens() ? st.nextToken("").substring(1) : null;
	}
	if (propertyElement == null || propertyElement.equals("")) throw new IOException("Attribute 'propertyElement' is missing from tag '" + getTagName() + "'");
// Ignore char count limit if not generating wml-output
	int charCountLimit = !((XmlTemplatesWriteContext)context).suffix.equals("wml") ||
	charLimit == null || (charLimit = charLimit.trim()).equals("") ? 0 : Integer.parseInt(charLimit),
	start = startIndex == null || (startIndex = startIndex.trim()).equals("") ? 0 : Integer.parseInt(startIndex),
	end = endIndex == null || (endIndex = endIndex.trim()).equals("") ? Integer.MAX_VALUE : Integer.parseInt(endIndex);
	Exception ex = null;
	try
	{
		Object bean = BeanContainer.getBean(name, ((XmlTemplatesWriteContext)context).beanContainers);
		BeanFeature beanFeature = BeanContainer.getBeanFeature(bean, name, property, false);
		Method method = beanFeature.featureDescriptor instanceof IndexedPropertyDescriptor ?
		((IndexedPropertyDescriptor)beanFeature.featureDescriptor).getIndexedReadMethod() :
		beanFeature.featureDescriptor instanceof PropertyDescriptor ?
		((PropertyDescriptor)beanFeature.featureDescriptor).getReadMethod() :
		((MethodDescriptor)beanFeature.featureDescriptor).getMethod();
		Class parameterTypes[] = method.getParameterTypes();
		int index = 0;
		if (parameterTypes.length == 0 || parameterTypes[parameterTypes.length - 1] != Integer.TYPE)
		{
			index += start;
			BeanFeature element = new BeanFeature(null, null);
			((XmlTemplatesWriteContext)context).beanContainers[XmlTemplates.PAGE_SCOPE].beans.put(propertyElement, element);
			Object array = method.invoke(beanFeature.bean, beanFeature.parameter == null ?
			new Object[0] : new Object[] {beanFeature.parameter});
			if (array instanceof boolean[])
			{
				boolean booleanArray[] = (boolean[])array;
				end = Math.min(end, booleanArray.length);
				while (index < end)
				{
					element.bean = new Boolean(booleanArray[index]);
					writeChildrenXml(context);
					index++;
					((XmlTemplatesWriteContext)context).charCount += charCount;
					if (charCountLimit > 0 && ((XmlTemplatesWriteContext)context).charCount >= charCountLimit) break;
				}
			}
			else if (array instanceof byte[])
			{
				byte byteArray[] = (byte[])array;
				end = Math.min(end, byteArray.length);
				while (index < end)
				{
					element.bean = new Byte(byteArray[index]);
					writeChildrenXml(context);
					index++;
					((XmlTemplatesWriteContext)context).charCount += charCount;
					if (charCountLimit > 0 && ((XmlTemplatesWriteContext)context).charCount >= charCountLimit) break;
				}
			}
			else if (array instanceof char[])
			{
				char charArray[] = (char[])array;
				end = Math.min(end, charArray.length);
				while (index < end)
				{
					element.bean = new Character(charArray[index]);
					writeChildrenXml(context);
					index++;
					((XmlTemplatesWriteContext)context).charCount += charCount;
					if (charCountLimit > 0 && ((XmlTemplatesWriteContext)context).charCount >= charCountLimit) break;
				}
			}
			else if (array instanceof double[])
			{
				double doubleArray[] = (double[])array;
				end = Math.min(end, doubleArray.length);
				while (index < end)
				{
					element.bean = new Double(doubleArray[index]);
					writeChildrenXml(context);
					index++;
					((XmlTemplatesWriteContext)context).charCount += charCount;
					if (charCountLimit > 0 && ((XmlTemplatesWriteContext)context).charCount >= charCountLimit) break;
				}
			}
			else if (array instanceof float[])
			{
				float floatArray[] = (float[])array;
				end = Math.min(end, floatArray.length);
				while (index < end)
				{
					element.bean = new Float(floatArray[index]);
					writeChildrenXml(context);
					index++;
					((XmlTemplatesWriteContext)context).charCount += charCount;
					if (charCountLimit > 0 && ((XmlTemplatesWriteContext)context).charCount >= charCountLimit) break;
				}
			}
			else if (array instanceof int[])
			{
				int intArray[] = (int[])array;
				end = Math.min(end, intArray.length);
				while (index < end)
				{
					element.bean = new Integer(intArray[index]);
					writeChildrenXml(context);
					index++;
					((XmlTemplatesWriteContext)context).charCount += charCount;
					if (charCountLimit > 0 && ((XmlTemplatesWriteContext)context).charCount >= charCountLimit) break;
				}
			}
			else if (array instanceof long[])
			{
				long longArray[] = (long[])array;
				end = Math.min(end, longArray.length);
				while (index < end)
				{
					element.bean = new Long(longArray[index]);
					writeChildrenXml(context);
					index++;
					((XmlTemplatesWriteContext)context).charCount += charCount;
					if (charCountLimit > 0 && ((XmlTemplatesWriteContext)context).charCount >= charCountLimit) break;
				}
			}
			else
			{
				Object objectArray[] = (Object[])array;
				end = Math.min(end, objectArray.length);
				while (index < end)
				{
					element.bean = objectArray[index];
					writeChildrenXml(context);
					index++;
					((XmlTemplatesWriteContext)context).charCount += charCount;
					if (charCountLimit > 0 && ((XmlTemplatesWriteContext)context).charCount >= charCountLimit) break;
				}
			}
		}
		else
		{
			Method closeMethod = null;
			boolean endLoop = false;
			while (index < end)
			{
				Object value = method.invoke(beanFeature.bean, beanFeature.parameter == null ?
				new Object[] {new Integer(index)} : new Object[] {beanFeature.parameter, new Integer(index)});
				try
				{
					if (endLoop)
					{
						if (value == null) end = index;
						break;
					}
					index++;
					if (value == null)
					{
						end = index;
						break;
					}
					if (index == 1)
					{
                                            Map map = BeanContainer.examine(value);
						closeMethod = BeanContainer.getCloseMethod(map);
					}
					if (index > start)
					{
						((XmlTemplatesWriteContext)context).beanContainers[XmlTemplates.PAGE_SCOPE].beans.put(propertyElement, value);
						writeChildrenXml(context);
						((XmlTemplatesWriteContext)context).charCount += charCount;
						if (charCountLimit > 0 && ((XmlTemplatesWriteContext)context).charCount >= charCountLimit) endLoop = true;
					}
				}
				finally
				{
					if (closeMethod != null) BeanContainer.callCloseMethod(value, closeMethod);
				}
			}
		}
		try
		{
			((XmlTemplatesWriteContext)context).beanContainers[0].setBeanProperty(beanFeature.bean, name, "loopCount", new Integer(index - start));
		}
		catch (ParseException ex1) {}
		try
		{
			((XmlTemplatesWriteContext)context).beanContainers[0].setBeanProperty(beanFeature.bean, name, "loopEnded", new Boolean(index == end));
		}
		catch (ParseException ex1) {}
		((XmlTemplatesWriteContext)context).beanContainers[XmlTemplates.PAGE_SCOPE].beans.remove(propertyElement);
	}
	catch (IllegalAccessException ex1)
	{
		ex = ex1;
	}
	catch (IntrospectionException ex1)
	{
		ex = ex1;
	}
	catch (InvocationTargetException ex1)
	{
		ex = ex1;
	}
	catch (ParseException ex1)
	{
		ex = ex1;
	}
	catch (InstantiationException ex1)
	{
		ex = ex1;
	}
	if (ex != null) throw new IOException(Support.stackTrace(ex));
}

}