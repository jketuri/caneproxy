
package FI.realitymodeler.xml;

import FI.realitymodeler.beans.*;
import FI.realitymodeler.common.*;
import org.apache.crimson.parser.*;
import org.apache.crimson.tree.*;
import java.io.*;
import java.lang.reflect.*;
import java.text.*;
import java.util.*;
import org.w3c.dom.*;

public class XmlTemplatesGetPropertyElementNode extends XmlTemplatesDisplayElementNode
{

public int writeValue(Writer writer, String value, String search, String replace) throws IOException
{
	int charCount = 0;
	if (search == null || search.equals(""))
	{
		Support.writeXmlString(writer, value);
		charCount += value.length();
		return charCount;
	}
	int i = 0, j;
	while ((j = value.indexOf(search, i)) != -1)
	{
		Support.writeXmlString(writer, value.substring(i, j));
		charCount += j - i + 1;
		writer.write(replace);
		charCount += replace.length();
		i = j + search.length();
	}
	if (i < value.length())
	{
		Support.writeXmlString(writer, value.substring(i));
		charCount += value.length() - i;
	}
	return charCount;
}

public void writeXml(XmlWriteContext context) throws IOException
{
	String charLimit = null, index = null, name = null, property = null, placeHolder = null, pattern = null, remove = null, search = null, replace = null;
	NamedNodeMap attributes = getAttributes();
	for (int i = attributes.getLength() - 1; i >= 0; i--)
	{
		Node attribute = attributes.item(i);
		String attributeName = attribute.getNodeName(), attributeValue = attribute.getNodeValue();
		if (attributeName.equals("charLimit")) charLimit = attributeValue;
		else if (attributeName.equals("index")) index = attributeValue;
		else if (attributeName.equals("name")) name = attributeValue;
		else if (attributeName.equals("pattern")) pattern = attributeValue;
		else if (attributeName.equals("placeholder")) placeHolder = attributeValue;
		else if (attributeName.equals("property")) property = attributeValue;
		else if (attributeName.equals("remove")) remove = attributeValue;
		else if (attributeName.equals("search")) search = attributeValue;
		else if (attributeName.equals("replace")) replace = attributeValue;
		else throw new IOException("Unknown attribute " + attributeName + " in tag " + getTagName());
	}
	if (property == null || property.equals("")) throw new IOException("Attribute 'property' is missing from tag '" + getTagName() + "'");
	if (name == null || name.equals(""))
	{
		StringTokenizer st = new StringTokenizer(property, ":");
		name = st.nextToken().trim();
		property = st.hasMoreTokens() ? st.nextToken("").substring(1) : null;
	}
	if (index != null && index.trim().equals("")) index = null;
	if (pattern != null && pattern.trim().equals("")) pattern = null;
	if (charLimit != null && charLimit.trim().equals("")) charLimit = null;
	int charCountLimit = !((XmlTemplatesWriteContext)context).suffix.equals("wml") ||
	charLimit == null ? 0 : Integer.parseInt(charLimit);
	Exception ex = null;
	try
	{
		Object bean = BeanContainer.getBean(name, ((XmlTemplatesWriteContext)context).beanContainers);
		if (bean == null) throw new IOException("Bean " + name + " not found");
		BeanFeature beanFeature = BeanContainer.getBeanFeature(bean, name, property, false);
		Object value = index != null ? BeanContainer.getProperty(beanFeature, Integer.parseInt(index)) :
		BeanContainer.getProperty(beanFeature);
		if (value == null) value = placeHolder;
		else if (!(value instanceof String)) value = value.toString();
		if (value == null) value = "";
		if (remove != null && !remove.equals("")) value = Support.replace((String)value, remove, "");
		Writer writer = charCountLimit > 0 ? new StringWriter() : context.getWriter();
		int charCount = 0;
		if (pattern != null)
		{
			int i = 0, j;
			while ((j = pattern.indexOf("{}", i)) != -1)
			{
				writer.write(pattern.substring(i, j));
				charCount += j - i + 1 + writeValue(writer, (String)value, search, replace);
				i = j + 2;
			}
			if (i < pattern.length())
			{
				writer.write(pattern.substring(i));
				charCount += pattern.length() - i;
			}
		}
		else charCount = writeValue(writer, (String)value, search, replace);
		if (charCountLimit > 0)
		{
			Integer charCountInteger = (Integer)((XmlTemplatesWriteContext)context).beanContainers[0].getBeanProperty(bean, name, "charCount");
			charCount = charCountInteger != null ? charCountInteger.intValue() : 0;
			String string = ((StringWriter)writer).toString().substring(charCount);
			int stringLength = string.length();
			if (((XmlTemplatesWriteContext)context).charCount + stringLength > charCountLimit)
			{
				int n = charCountLimit - ((XmlTemplatesWriteContext)context).charCount, i = string.lastIndexOf(' ', n);
				if (i != -1) string = string.substring(0, i);
				else string = string.substring(0, n);
			}
			context.getWriter().write(string);
			((XmlTemplatesWriteContext)context).beanContainers[0].setBeanProperty(bean, name, "charCount", new Integer(charCount + string.length()));
			((XmlTemplatesWriteContext)context).beanContainers[0].setBeanProperty(bean, name, "charsEnded", new Boolean(string.length() == stringLength));
			charCount = string.length();
		}
		else if (charLimit != null) ((XmlTemplatesWriteContext)context).beanContainers[0].setBeanProperty(bean, name, "charsEnded", Boolean.TRUE);
		((XmlTemplatesWriteContext)context).charCount += charCount;
	}
	catch (IllegalAccessException ex1)
	{
		ex = ex1;
	}
	catch (InvocationTargetException ex1)
	{
		ex = ex1;
	}
	catch (ParseException ex1)
	{
		ex = ex1;
	}
	catch (InstantiationException ex1)
	{
		ex = ex1;
	}
	if (ex != null) throw new IOException(Support.stackTrace(ex));
}

}
