
package FI.realitymodeler;

import FI.realitymodeler.common.*;

import java.util.*;

public class MsgItem {
    HeaderList headerList;
    String msgName;
    String msgId;
    int index;
    long time = 0L;
    long uid = -1L;

    public MsgItem() {
    }

    public MsgItem(HeaderList headerList, String msgName, String msgId, int index) {
        this.headerList = headerList;
        this.msgName = msgName;
        this.msgId = msgId;
        this.index = index;
        if (headerList == null) return;
        if (msgId == null) this.msgId = headerList.getHeaderValue("message-id");
        String uidValue = headerList.getHeaderValue("-");
        if (uidValue != null) uid = Long.parseLong(uidValue);
        Date date = Support.parse(headerList.getHeaderValue("date"));
        time = date != null ? date.getTime() : 0L;
    }

}
