
package FI.realitymodeler.msgbean;

import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

public class MsgItem {
    static SimpleDateFormat listDateFormat;
    static {
        try {
            listDateFormat = new SimpleDateFormat("EEE, dd MMM yy HH:mm:ss", Locale.US);
        } catch (Exception ex) {
            throw new Error(Support.stackTrace(ex));
        }
    }

    Message message;
    MsgBean msgBean;

    MsgItem(Message message, MsgBean msgBean)
        throws MessagingException {
        this.message = message;
        this.msgBean = msgBean;
    }

    /** Returns a link, which can be used to receive a message composition
        form addressed to sender of current message. */
    public String getComposeLink()
        throws MessagingException {
        //      String href = "?id=" + URLEncoder.encode(((com.sun.mail.imap.IMAPMessage)message).getMessageID()) + "&f=";
        String href = "?id=" + msgBean.getSessionId() + "&amp;nr=" + message.getMessageNumber() +  "&amp;f=";
        return href;
    }

    public String getDate()
        throws MessagingException {
        Date date = message.getReceivedDate();
        return date != null ? listDateFormat.format(date) : null;
    }

    /** Returns a link, which can be used to display the body of the current
        message. */
    public String getDisplayLink()
        throws MessagingException {
        //      String href = "?id=" + URLEncoder.encode(((com.sun.mail.imap.IMAPMessage)message).getMessageID());
        String href = "?id=" + msgBean.getSessionId() + "&amp;nr=" + message.getMessageNumber();
        return href;
    }

    public String getFrom()
        throws MessagingException {
        Address from[] = message.getFrom();
        if (from == null || from.length == 0 || !(from[0] instanceof InternetAddress)) return null;
        String personal = ((InternetAddress)from[0]).getPersonal();
        if (personal == null) personal = ((InternetAddress)from[0]).getAddress();
        return personal;
    }

    public String getFromAddress()
        throws MessagingException {
        Address from[] = message.getFrom();
        if (from == null || from.length == 0 || !(from[0] instanceof InternetAddress)) return null;
        String address = ((InternetAddress)from[0]).getAddress(),
            personal = ((InternetAddress)from[0]).getPersonal();
        if (address != null && personal != null) address += " (" + personal + ")";
        return address;
    }

    public String getSubject()
        throws MessagingException {
        String subject = message.getSubject();
        if (subject == null || subject.trim().equals("")) subject = "(Untitled)";
        return subject;
    }

    public StringBuffer appendText(Part part, StringBuffer buffer)
        throws MessagingException, IOException {
        Object content = part.getContent();
        if (part.isMimeType("text/plain") && content instanceof String) {
            if (buffer.length() > 0) buffer.append('\n');
            buffer.append(content);
            return buffer;
        }
        if (content instanceof Multipart) {
            Multipart multipart = (Multipart)content;
            int count = multipart.getCount();
            for (int i = 0; i < count; i++) appendText((Part)multipart.getBodyPart(i), buffer);
        }
        return buffer;
    }

    public String getText()
        throws MessagingException, IOException {
        StringBuffer buffer = new StringBuffer();
        return appendText(message, buffer).toString();
    }

}