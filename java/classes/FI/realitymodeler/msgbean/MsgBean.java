
package FI.realitymodeler.msgbean;

import FI.realitymodeler.common.*;
import FI.realitymodeler.portalbean.*;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.mail.search.*;
import javax.naming.*;
import javax.naming.directory.*;
import javax.naming.ldap.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.w3c.dom.*;
import org.xml.sax.*;
import org.xml.sax.helpers.*;

/** Visual component, which can be used in XML Template pages for
    accessing IMAP4-, NNTP- and SMTP-servers for asynchronous Internet
    messaging. */
public class MsgBean {
    // This hash map contains request parameters mapping to Active Directory attribute names
    public static HashMap<String,Object> propertyFields = new HashMap<String,Object>();
    // This hash map will contain default initialization parameters per servlet instance
    static HashMap<ServletConfig,MsgBean> msgBeans = new HashMap<ServletConfig,MsgBean>();
    static {
        try {
            // Property fields define mapping of user parameters to directory properties.
            // If property field is string array, it contains property name with default value
            // If property field is object array, it contains sub content class possibly with other property fields
            propertyFields.put("cmt", "D:comment");

            propertyFields.put("af", "H:attachmentfilename");
            propertyFields.put("bc", "H:bcc");
            propertyFields.put("cr", "H:calendar");
            propertyFields.put("cc", "H:cc");
            propertyFields.put("cs", "H:contacts");
            propertyFields.put("ce", "H:contact-disposition-type");
            propertyFields.put("cm", "H:content-media-type");
            propertyFields.put("de", "H:date");
            propertyFields.put("dr", "H:datereceived");
            propertyFields.put("di", "H:deleteditems");
            propertyFields.put("db", "H:displaybcc");
            propertyFields.put("dc", "H:displaycc");
            propertyFields.put("dt", "H:displayto");
            propertyFields.put("ds", "H:drafts");
            propertyFields.put("ed", "H:expiry-date");
            propertyFields.put("fc", "H:flagcompleted");
            propertyFields.put("fm", "H:from");
            propertyFields.put("fe", "H:fromemail");
            propertyFields.put("fn", "H:fromname");
            propertyFields.put("ha", "H:hasattachment");
            propertyFields.put("hd", "H:htmldescription");
            propertyFields.put("ie", "H:importance");
            propertyFields.put("ix", "H:inbox");
            propertyFields.put("jn", "H:journal");
            propertyFields.put("mf", "H:messageflag");
            propertyFields.put("mt", "H:msgfolderroot");
            propertyFields.put("ns", "H:normalizedsubject");
            propertyFields.put("nt", "H:notes");
            propertyFields.put("ox", "H:outbox");
            propertyFields.put("py", "H:priority");
            propertyFields.put("rd", "H:read");
            propertyFields.put("rb", "H:reply-by");
            propertyFields.put("rt", "H:reply-to");
            propertyFields.put("sd", "H:savedestination");
            propertyFields.put("si", "H:saveinsert");
            propertyFields.put("sr", "H:sender");
            propertyFields.put("sl", "H:senderemail");
            propertyFields.put("sm", "H:sendername");
            propertyFields.put("sg", "H:sendmsg");
            propertyFields.put("ss", "H:sentitems");
            propertyFields.put("sj", "H:subject");
            propertyFields.put("sb", "H:submitted");
            propertyFields.put("ts", "H:tasks");
            propertyFields.put("td", "H:textdescription");
            propertyFields.put("tt", "H:thread-topic");
            propertyFields.put("to", "H:to");
            propertyFields.put("ut", "H:unreadcount");

            propertyFields.put("ae", "C:alldayevent");
            propertyFields.put("ar", "C:attendeerole");
            propertyFields.put("as", "C:attendeestatus");
            propertyFields.put("bs", "C:busystatus");
            propertyFields.put("ca", "C:contact");
            propertyFields.put("cu", "C:contacturl");
            propertyFields.put("cd", "C:created");
            propertyFields.put("dd", "C:dtend");
            propertyFields.put("dp", "C:dtstamp");
            propertyFields.put("da", "C:dtstart");
            propertyFields.put("dn", "C:duration");
            propertyFields.put("ee", "C:exdate");
            propertyFields.put("er", "C:exrule");
            propertyFields.put("fu", "C:fburl");
            propertyFields.put("ga", "C:geolatitude");
            propertyFields.put("go", "C:geolongitude");
            propertyFields.put("it", "C:instancetype");
            propertyFields.put("io", "C:isorganizer");
            propertyFields.put("lm", "C:lastmodifiedtime");
            propertyFields.put("lc", "C:location");
            propertyFields.put("lu", "C:locationurl");
            propertyFields.put("ms", "C:meetingstatus");
            propertyFields.put("md", "C:method");
            propertyFields.put("pi", "C:prodid");
            propertyFields.put("re", "C:rdate");
            propertyFields.put("ri", "C:recurrenceid");
            propertyFields.put("rc", "C:recurrenceidrange");
            propertyFields.put("ro", "C:reminderoffset");
            propertyFields.put("rm", "C:replytime");
            propertyFields.put("rs", "C:resources");
            propertyFields.put("rr", "C:responserequested");
            propertyFields.put("ru", "C:rrule");
            propertyFields.put("rp", "C:rsvp");
            propertyFields.put("sq", "C:sequence");
            propertyFields.put("tz", "C:timezone");
            propertyFields.put("tp", "C:transparent");
            propertyFields.put("ud", "C:uid");
            propertyFields.put("vn", "C:version");

            propertyFields.put("at", "T:account");
            propertyFields.put("ag", "T:authoring");
            propertyFields.put("by", "T:bday");
            propertyFields.put("bi", "T:billinginformation");
            propertyFields.put("bp", "T:businesshomepage");
            propertyFields.put("c", "T:c");
            propertyFields.put("cp", "T:callbackphone");
            propertyFields.put("ch", "T:childrensnames");
            propertyFields.put("cn", "T:cn");
            propertyFields.put("co", "T:co");
            propertyFields.put("cw", "T:computernetworkname");
            propertyFields.put("ct", "T:contact");
            propertyFields.put("ci", "T:customerid");
            propertyFields.put("dm", "T:department");
            propertyFields.put("e1", "T:email1");
            propertyFields.put("e2", "T:email2");
            propertyFields.put("e3", "T:email2");
            propertyFields.put("en", "T:employeenumber");
            propertyFields.put("ft", "T:facsimiletelephonenumber");
            propertyFields.put("fa", "T:fileas");
            propertyFields.put("fi", "T:fileasid");
            propertyFields.put("fs", "T:ftpsite");
            propertyFields.put("gr", "T:gender");
            propertyFields.put("gn", "T:givenname");
            propertyFields.put("gi", "T:governmentid");
            propertyFields.put("hs", "T:hobbies");
            propertyFields.put("hc", "T:homecity");
            propertyFields.put("hy", "T:homecountry");
            propertyFields.put("hf", "T:homefax");
            propertyFields.put("la", "T:homelatitude");
            propertyFields.put("lo", "T:homelongitude");
            propertyFields.put("hp", "T:homephone");
            propertyFields.put("h2", "T:homephone2");
            propertyFields.put("hl", "T:homepostaladdress");
            propertyFields.put("ho", "T:homepostalcode");
            propertyFields.put("hb", "T:homepostofficebox");
            propertyFields.put("ht", "T:homestate");
            propertyFields.put("hr", "T:homestreet");
            propertyFields.put("hz", "T:hometimezone");
            propertyFields.put("is", "T:initials");
            propertyFields.put("in", "T:internationalisdnnumber");
            propertyFields.put("l", "T:l");
            propertyFields.put("le", "T:language");
            propertyFields.put("ln", "T:location");
            propertyFields.put("mi", "T:mailingaddressid");
            propertyFields.put("mt", "T:mailingcity");
            propertyFields.put("my", "T:mailingcountry");
            propertyFields.put("ma", "T:mailingpostaladdress");
            propertyFields.put("mp", "T:mailingpostalcode");
            propertyFields.put("mo", "T:mailingpostofficebox");
            propertyFields.put("ml", "T:mailingstate");
            propertyFields.put("mr", "T:mailingstreet");
            propertyFields.put("mg", "T:manager");
            propertyFields.put("mc", "T:managercn");
            propertyFields.put("mu", "T:mapurl");
            propertyFields.put("mm", "T:memberaddresses");
            propertyFields.put("mb", "T:members");
            propertyFields.put("mn", "T:middlename");
            propertyFields.put("me", "T:mobile");
            propertyFields.put("nx", "T:namesuffix");
            propertyFields.put("o", "T:o");
            propertyFields.put("oc", "T:othercity");
            propertyFields.put("oy", "T:othercountry");
            propertyFields.put("oe", "T:othercountrycode");
            propertyFields.put("of", "T:otherfacsimiletelephonenumber");
            propertyFields.put("or", "T:otherfax");
            propertyFields.put("om", "T:othermobile");
            propertyFields.put("op", "T:otherpager");
            propertyFields.put("oa", "T:otherpostaladdress");
            propertyFields.put("od", "T:otherpostalcode");
            propertyFields.put("ob", "T:otherpostofficebox");
            propertyFields.put("os", "T:otherstate");
            propertyFields.put("ot", "T:otherstreet");
            propertyFields.put("oh", "T:othertelephone");
            propertyFields.put("oz", "T:othertimezone");
            propertyFields.put("pr", "T:pager");
            propertyFields.put("ph", "T:personalhomepage");
            propertyFields.put("pt", "T:personaltitle");
            propertyFields.put("pc", "T:postalcode");
            propertyFields.put("pb", "T:postalofficebox");
            propertyFields.put("pn", "T:profession");
            propertyFields.put("ps", "T:protocolsettings");
            propertyFields.put("pa", "T:proxyaddresses");
            propertyFields.put("rn", "T:roomnuber");
            propertyFields.put("sy", "T:secretary");
            propertyFields.put("tc", "T:secretarycn");
            propertyFields.put("tu", "T:secretaryurl");
            propertyFields.put("sn", "T:sn");
            propertyFields.put("su", "T:sourceurl");
            propertyFields.put("sc", "T:spousecn");
            propertyFields.put("st", "T:st");
            propertyFields.put("se", "T:street");
            propertyFields.put("so", "T:submissioncontlength");
            propertyFields.put("tn", "T:telephonenumber");
            propertyFields.put("t2", "T:telephonenumber2");
            propertyFields.put("tx", "T:telexnumber");
            propertyFields.put("te", "T:title");
            propertyFields.put("ua", "T:unauthoring");
            propertyFields.put("uc", "T:usercertificate");
            propertyFields.put("wg", "T:weddinganniversary");
            propertyFields.put("wa", "T:workaddress");

            propertyFields.put("ad", new Object[] {"S:attendees", new Object[] {"S:attendee", "C:address"}});
        } catch (Exception ex) {
            throw new Error(Support.stackTrace(ex));
        }
    }
    Boolean activeDirectory = null;
    HashMap<String,String> parameterAliases = new HashMap<String,String>();
    HashMap<String,String> userParameters = null;
    MsgBean msgBean = null;
    MsgFolder msgFolder = null;
    MsgItem msgItem = null;
    PortalEntry portalEntry = null;
    ServletConfig servletConfig = null;
    ServletContext servletContext = null;
    String category = null;
    String host = null;
    String keywords = null;
    String smtpHost = null;
    String state = "started";
    boolean charsEnded = false;
    boolean loopEnded = false;
    boolean verbose = false;
    int charCount = 0;
    int loopCount = 0;
    int messageNumber = 0;
    int skipNumber = 0;

    class MsgFolder implements HttpSessionBindingListener {
        Session session = null;
        Folder folder = null;
        HashMap messageMap = new HashMap();
        Message messages[] = null;
        Store store = null;

        public void valueBound(HttpSessionBindingEvent event) {
        }

        public void valueUnbound(HttpSessionBindingEvent event) {
            try {
                if (folder != null)
                    try {
                        folder.close(true);
                    } catch (MessagingException ex) {}
            } finally {
                if (store != null)
                    try {
                        store.close();
                    } catch (MessagingException ex) {}
            }
        }

    }

    public boolean getActiveDirectory() {
        return activeDirectory != null ? activeDirectory.booleanValue() : msgBean != null ? msgBean.getActiveDirectory() : false;
    }

    /** This flag indicates that MS Active Directory is used as LDAP-server. */
    public void setActiveDirectory(boolean activeDirectory) {
        this.activeDirectory = new Boolean(activeDirectory);
    }

    public String getCategory() {
        return category != null ? category : msgBean != null ? msgBean.getCategory() : null;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setCharCount(int charCount) {
        this.charCount = charCount;
    }

    public int getCharCount() {
        return charCount;
    }

    public void setCharsEnded(boolean charsEnded) {
        this.charsEnded = charsEnded;
    }

    public boolean getCharsEnded() {
        return charsEnded;
    }

    /** Returns link, which will respond with message composition form
        prefilled for replying to current message. */
    public String getComposeLink()
        throws MessagingException {
        return msgItem.getComposeLink();
    }

    /** Returns date of current message. */
    public String getDate()
        throws MessagingException {
        return msgItem != null ? msgItem.getDate() : null;
    }

    /** Returns link, which will display current message. */
    public String getDisplayLink()
        throws MessagingException {
        return msgItem.getDisplayLink();
    }

    /** Returns from-name of current message. */
    public String getFrom()
        throws MessagingException {
        return msgItem != null ? msgItem.getFrom() : null;
    }

    /** Returns from-address of current message. */
    public String getFromAddress()
        throws MessagingException {
        return msgItem != null ? msgItem.getFromAddress() : null;
    }

    public boolean getIsFirstPage() {
        return charCount == 0;
    }

    public void setLoopCount(int loopCount) {
        this.loopCount = loopCount;
    }

    public int getLoopCount() {
        return loopCount;
    }

    public void setLoopEnded(boolean loopEnded) {
        this.loopEnded = loopEnded;
    }

    public boolean getLoopEnded() {
        return loopEnded;
    }

    public boolean getLoopStarted() {
        return skipNumber == 0;
    }

    public String getHost() {
        return host != null ? host : msgBean != null ? msgBean.getHost() : null;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getKeywords() {
        return keywords != null ? keywords : msgBean != null ? msgBean.getKeywords() : null;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public int getMessageNumber() {
        return messageNumber;
    }

    public String getMoreLink() {
        return "?id=" + getSessionId() + "&amp;nr=" + getMessageNumber() + "&amp;sh=" + getCharCount();
    }

    public MsgItem getMsgItem(int index)
        throws MessagingException {
        if ((index += skipNumber) >= msgFolder.messages.length) return null;
        Message message = msgFolder.messages[msgFolder.messages.length - index - 1];
        return new MsgItem(message, this);
    }

    public int getNewSkipNumber() {
        return skipNumber + loopCount;
    }

    public String getNextLink() {
        return "?id=" + getSessionId() + "&amp;a=Get list&amp;s=" + getNewSkipNumber();
    }

    public String getSessionId() {
        return portalEntry.httpSession.getId();
    }

    public String getState() {
        return state;
    }

    public String getSmtpHost() {
        return smtpHost != null ? smtpHost : msgBean != null ? msgBean.getSmtpHost() : null;
    }

    public void setSmtpHost(String smtpHost) {
        this.smtpHost = smtpHost;
    }

    public String getSubject()
        throws MessagingException {
        return msgItem != null ? msgItem.getSubject() : null;
    }

    public String getText()
        throws MessagingException, IOException {
        return msgItem != null ? msgItem.getText() : null;
    }

    public void setParameterAlias(String parameterAlias, String parameterName) {
        parameterAliases.put(parameterName, parameterAlias);
    }

    public String getParameterAlias(String parameterName) {
        String alias = parameterAliases.get(parameterName);
        return alias != null ? alias : msgBean != null ? msgBean.getParameterAlias(parameterName) : parameterName;
    }

    public void setServlet(String property, String value)
        throws ServletException, java.text.ParseException, MalformedURLException {
        StringTokenizer st = new StringTokenizer(property, ":");
        if (!st.hasMoreTokens() || !st.nextToken().equalsIgnoreCase("msg")) return;
        value = Support.decode(value);
        if (verbose) servletContext.log("setServlet " + property + " to " + value);
        String name = st.nextToken().trim();
        if (name.equals("activeDirectory")) setActiveDirectory(Support.isTrue(value, name));
        else if (name.equals("host")) setHost(value);
        else if (name.equals("parameterAlias")) setParameterAlias(st.nextToken(), value);
        else if (name.equals("smtpHost")) setSmtpHost(value);
        else throw new ServletException("Unknown msg bean servlet parameter " + name);
    }

    public void setServletConfig(ServletConfig servletConfig)
        throws ServletException, java.text.ParseException, MalformedURLException {
        if (servletConfig == null) return;
        this.servletConfig = servletConfig;
        servletContext = servletConfig.getServletContext();
        if (Support.isTrue(servletConfig.getInitParameter("verbose"), "verbose") ||
            servletContext.getAttribute("FI.realitymodeler.server.W3Server/verbose") == Boolean.TRUE) setVerbose(true);
        if ((msgBean = msgBeans.get(servletConfig)) != null) return;
        msgBean = new MsgBean();
        msgBean.servletContext = servletContext;
        if (getVerbose()) msgBean.setVerbose(true);
        Enumeration initParameterNames = servletConfig.getInitParameterNames();
        while (initParameterNames.hasMoreElements()) {
            String name = (String)initParameterNames.nextElement();
            msgBean.setServlet(name, servletConfig.getInitParameter(name));
        }
        msgBeans.put(servletConfig, msgBean);
    }

    /** Returns parameter value which was given in request with name
        which is mapped to actual parameter name.

        @param name name which is mapped to parameter name.

        @return parameter value or null if not found. */
    public String getUserParameter(String name) {
        return userParameters.get(name);
    }

    public boolean getVerbose() {
        return verbose;
    }

    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    private boolean parameterExists(HttpServletRequest req, String name) {
        return req.getParameter(name = getParameterAlias(name)) != null;
    }

    private String getParameter(HttpServletRequest req, String name) {
        String parameter = req.getParameter(name = getParameterAlias(name));
        if (parameter != null && (parameter = parameter.trim()).equals("")) parameter = null;
        return parameter;
    }

    /** Responses with a message list from IMAP-server. Http request may
        contain following parameters:

        host - name of the IMAP-server host

        keywords - keywords separated with a comma to be searched from messages
        Subject- and From-headers

        category - IMAP folder name whose messages are requested to be listed. If
        this is not specified, it defaults to INBOX. 

    */
    public void respondMsgList(HttpServletRequest req, PortalEntry portalEntry)
        throws IOException, NamingException, ServletException, NoSuchProviderException, MessagingException {
        if (portalEntry.mailhost != null && !portalEntry.mailhost.trim().equals("")) setHost(portalEntry.mailhost);
        else if (getHost() == null) setHost("127.0.0.1");
        String keywords = getParameter(req, "ks");
        if (keywords != null && !keywords.trim().equals("")) setKeywords(keywords);
        else keywords = getKeywords();
        String category = getParameter(req, "cy");
        if (category != null && !category.trim().equals("")) setCategory(category);
        else category = getCategory();
        msgFolder.store.connect(getHost(), 143, portalEntry.username, portalEntry.password);
        msgFolder.folder = msgFolder.store.getFolder(category != null ? category : "INBOX");
        msgFolder.folder.open(Folder.READ_WRITE);
        msgFolder.messages = null;
        if (keywords != null) {
            StringTokenizer st = new StringTokenizer(keywords, ",");
            SearchTerm searchTerms[] = new SearchTerm[st.countTokens() * 2];
            for (int i = 0; i < searchTerms.length; i += 2) {
                String keyword = st.nextToken();
                searchTerms[i] = new FromStringTerm(keyword);
                searchTerms[i + 1] = new SubjectTerm(keyword);
            }
            if (searchTerms.length > 0) msgFolder.messages = msgFolder.folder.search(new OrTerm(searchTerms));
        }
        if (msgFolder.messages == null) msgFolder.messages = msgFolder.folder.getMessages();
        String s = getParameter(req, "s");
        skipNumber = s != null ? Integer.parseInt(s) : 0;
        state = "listing";
    }

    private InternetAddress[] makeAddressArray(String addressList)
        throws AddressException {
        StringTokenizer st = new StringTokenizer(addressList, ",");
        InternetAddress addressArray[] = new InternetAddress[st.countTokens()];
        for (int i = 0; i < addressArray.length; i++) addressArray[i] = new InternetAddress(st.nextToken());
        return addressArray;
    }

    public void processRequest(HttpServletRequest req)
        throws IOException, NamingException, ServletException, NoSuchProviderException, MessagingException {
        userParameters = new HashMap<String,String>();
        String pathInfo = req.getPathInfo();
        portalEntry = PortalBean.searchPortalEntry(req, servletConfig);
        if (portalEntry == null) return;
        msgFolder = (MsgFolder)portalEntry.httpSession.getAttribute(getClass().getName() + "/msgFolder");
        String number = getParameter(req, "nr");
        if (number != null) {
            // Number of message found in request, user wants to display specific message
            // or reply to specific message
            Message message = msgFolder.folder.getMessage(messageNumber = Integer.parseInt(number));
            msgItem = new MsgItem(message, this);
            if (parameterExists(req, "d")) {
                message.setFlag(Flags.Flag.DELETED, true);
                return;
            }
            if (parameterExists(req, "f")) {
                state = "composing";
                return;
            }
            message.setFlag(Flags.Flag.SEEN, true);
            String skipCharacters = getParameter(req, "sh");
            if (skipCharacters != null) setCharCount(Integer.parseInt(skipCharacters));
            state = "displaying";
            return;
        }
        if (msgFolder == null) {
            msgFolder = new MsgFolder();
            portalEntry.httpSession.setAttribute(getClass().getName() + "/msgFolder", msgFolder);
            msgFolder.session = Session.getDefaultInstance(System.getProperties(), null);
            msgFolder.store = msgFolder.session.getStore("imap");
        }
        if (parameterExists(req, "t")) {
            String text = getParameter(req, "t");
            if (text == null) text = "";
            // Text found in request, user wants to send a message
            MimeMessage message = new MimeMessage(msgFolder.session);
            String to = getParameter(req, "to"),
                cc = getParameter(req, "cc"),
                bcc = getParameter(req, "bc");
            if (to != null) message.setRecipients(Message.RecipientType.TO, makeAddressArray(to));
            if (cc != null) message.setRecipients(Message.RecipientType.CC, makeAddressArray(cc));
            if (bcc != null) message.setRecipients(Message.RecipientType.BCC, makeAddressArray(bcc));
            if (portalEntry.email != null) message.setFrom(new InternetAddress(portalEntry.email));
            String subject = getParameter(req, "sj");
            if (subject != null) message.setSubject(subject);
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            PrintStream eout = new PrintStream(MimeUtility.encode(bout, "quoted-printable"));
            eout.print(text);
            message.setText(bout.toString(), "iso-8859-1");
            message.setHeader("Content-Transfer-Encoding", "quoted-printable");
            if (System.getProperty("mail.smtp.host") == null && getSmtpHost() != null) System.setProperty("mail.smtp.host", getSmtpHost());
            Transport.send(message);
            return;
        }
        String action = Support.getParameterValue(req.getParameterValues(getParameterAlias("a")));
        if (action == null) return;
        if (action.equals("Get list")) {
            // Get list of messages
            respondMsgList(req, portalEntry);
            return;
        }
        if (action.equals("Compose")) {
            // Compose new message
            state = "composing";
            return;
        }
    }

}
