
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.util.*;

/** Implements URL connection to Short Message System. URL must be of form:<br>
    sms:to[:port][/data coding/datagram type/nbs headers/from]<br>
    To-field can have many msisdn-numbers separated with commas.<br>
    Data coding can be 'ascii', 'unicode' or 'binary'.<br>
    Datagram type can be 'cimd', 'nbs', 'mobile' or 'udp' (default).<br>
    If nbs headers is 'nbs' appropriate headers are used. Only output is supported.
*/
public class W3SmsURLConnection extends W3URLConnection {
    public static boolean useProxy = false;
    public static String proxyHost = null;
    public static int proxyPort = 80;
    public static String fromAddr = null;

    CellularSocket cellulars[] = null;

    public W3SmsURLConnection(URL url) {
        super(url);
    }

    public void open() throws IOException {
        if (useProxy) {
            if (requester != null) return;
            proxyConnect(proxyHost, proxyPort);
        }
    }

    public void close() {
        if (cellulars != null) {
            for (int i = 0; i < cellulars.length; i++)
                if (cellulars[i] != null)
                    try {
                        cellulars[i].close();
                    } catch (IOException ex) {}
            cellulars = null;
        }
        super.close();
    }

    public void doConnect()
        throws IOException {
        open();
    }

    public InputStream getInputStream() throws IOException {
        if (inputDone) return in;
        if (doOutput) {
            closeStreams();
            inputDone = true;
            useCaches = false;
            if (useProxy) {
                if (requester == null) throw new IOException("Not connected");
                in = requester.input;
                checkInput();
                return in;
            }
            mayKeepAlive = true;
            headerFields = new HeaderList();
            headerFields.append(new Header("", protocol_1_1 + " " + HTTP_NO_CONTENT));
            return in = null;
        }
        setRequestMethod();
        setRequestFields();
        return super.getInputStream();
    }

    public OutputStream getOutputStream() throws IOException {
        connect();
        doOutput = true;
        setRequestMethod();
        setRequestFields();
        if (useProxy) {
            Support.sendMessage(requester.output, requestHeaderFields);
            return requester.output;
        }
        String dataCoding = null, from = null, to = null, datagramType = null, nbsHeaders = null;
        StringTokenizer st = new StringTokenizer(path, "/");
        if (st.hasMoreTokens()) {
            to = st.nextToken().trim();
            if (st.hasMoreTokens()) {
                dataCoding = st.nextToken().trim().toLowerCase();
                if (st.hasMoreTokens()) {
                    datagramType = st.nextToken().trim().toLowerCase();
                    if (st.hasMoreTokens()) {
                        nbsHeaders = st.nextToken().trim().toLowerCase();
                        if (st.hasMoreTokens()) from = st.nextToken().trim();
                    }
                }
            }
        }
        if (from == null && (from = getRequestProperty("from")) == null &&
            (from = fromAddr) == null) from = System.getProperty("sms.fromaddr");
        if (from == null) throw new IOException("From-address not specified");
        byte msisdn[] = from.getBytes();
        int type = CellularSocket.UDP;
        if (datagramType != null) {
            if (datagramType.equals("cimd")) type = CellularSocket.CIMD;
            else if (datagramType.equals("nbs")) type = CellularSocket.NBS;
            else if (datagramType.equals("mobile")) type = CellularSocket.MOBILE;
            else if (!datagramType.equals("udp")) throw new IOException("Unknown datagram type " + datagramType);
        }
        st = new StringTokenizer(to, ",");
        int tc = st.countTokens();
        cellulars = new CellularSocket[tc];
        OutputStream outs[] = new OutputStream[tc];
        boolean nbsHeadersValue = nbsHeaders != null && nbsHeaders.equals("nbs");
        int dataCodingValue = dataCoding != null && dataCoding.equals("binary") ? 0 : 1,
            defaultPort = nbsHeadersValue ? CellularSocket.TTML_PORT : CellularSocket.TEXT_PORT;
        for (int i = 0; st.hasMoreTokens(); i++) {
            int port, x = (to = st.nextToken().trim()).indexOf(':');
            if (x != -1) {
                port = Integer.parseInt(to.substring(x + 1).trim());
                to = to.substring(0, x).trim();
            } else port = defaultPort;
            cellulars[i] = new CellularSocket(msisdn, port, to.getBytes(), port, dataCodingValue, nbsHeadersValue, type);
            outs[i] = cellulars[i].getOutputStream();
        }
        return sink = new CloneOutputStream(outs);
    }

    public boolean usingProxy() {
        return useProxy;
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public int getProxyPort() {
        return proxyPort;
    }

}
