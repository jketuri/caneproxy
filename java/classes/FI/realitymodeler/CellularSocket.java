
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.util.*;

/** Cellular socket supporting User Datagram Protocol, Narrow Band Socket Protocol,
    Computer Interface for Message Distribution and Mobile phone serial interface.
*/
public class CellularSocket extends W3Socket {
    public static final boolean cimd = false;
    // Cellular Socket Types
    public static final int UDP = 0;
    public static final int NBS = 1;
    public static final int CIMD = 2;
    public static final int MOBILE = 3;

    public static final int WAP_CL_PORT = 9200;
    public static final int WAP_CO_PORT = 9201;
    public static final int TEXT_PORT = 10;
    public static final int TTML_PORT = 5580;
    public static final int SMS_MSG_SIZE = 160;
    public static final int PLAIN_MSG_FREE = SMS_MSG_SIZE - 14;
    public static final int NBS_MSG_FREE = SMS_MSG_SIZE - 21;
    public static final int NO_HEADERS = 0;
    public static final int BINARY_HEADERS = 1;
    public static final int TEXT_HEADERS = 2;
    public static final int ASCII_DATA_CODING = 0;
    public static final int UNICODE_DATA_CODING = 1;
    public static final int BINARY_DATA_CODING = 2;
    public static final byte dots[] = "...".getBytes();
    public static final byte zeros[] = new byte[SMS_MSG_SIZE];
    public static final String reserved = ".<>";
    // WTP PDU Types
    public static final int WTP_Invoke = 0x01;
    public static final int WTP_Result = 0x02;
    public static final int WTP_Ack = 0x03;
    public static final int WTP_Abort = 0x04;
    public static final int WTP_Segmented_Invoke = 0x05;
    public static final int WTP_Segmented_Result = 0x06;
    public static final int WTP_Negative_Ack = 0x07;
    // WTP TPI Identities
    public static final int TPI_Error = 0x00;
    public static final int TPI_Info = 0x01;
    public static final int TPI_Option = 0x02;
    public static final int TPI_Packet_Sequence_Number = 0x03;
    // WTP Option TPI Identities
    public static final int Option_TPI_Maximum_Receive_Unit = 0x01;
    public static final int Option_TPI_Total_Message_Size = 0x02;
    public static final int Option_TPI_Delay_Transmission_Time = 0x03;
    public static final int Option_TPI_Maximum_Group = 0x04;
    public static final int Option_TPI_Current_TID = 0x05;
    public static final int Option_TPI_No_Cached_TID = 0x06;

    public static int plainMaxCount = PLAIN_MSG_FREE * 10;
    public static int nbsMaxCount = NBS_MSG_FREE * 3;
    public static int udpMaxCount = 4500;
    public static long cellularSocketInfo;

    static Map<String, CellularSocket> instances = new WeakHashMap<String, CellularSocket>();
    static Map<String, Object> codes = new HashMap<String, Object>();
    static {
        try {
            if (cimd) cellularSocketInfo = MultiSocketImpl.getProtocolInfo("CIMD LAYERED");
            codes.put("Oa", new Character('@'));
            codes.put("$ ", new Character('\44'));
            codes.put("!!", new Character('\241'));
            codes.put("L-", new Character('\243'));
            codes.put("so", new Character('\247'));
            codes.put("??", new Character('\277'));
            codes.put("A\"", new Character('\304'));
            codes.put("A*", new Character('\305'));
            codes.put("AE", new Character('\306'));
            codes.put("C,", new Character('\307'));
            codes.put("E´", new Character('\311'));
            codes.put("N~", new Character('\321'));
            codes.put("O\"", new Character('\326'));
            codes.put("O/", new Character('\330'));
            codes.put("U\"", new Character('\334'));
            codes.put("ss", new Character('\337'));
            codes.put("a`", new Character('\340'));
            codes.put("a´", new Character('\341'));
            codes.put("a\"", new Character('\344'));
            codes.put("a*", new Character('\345'));
            codes.put("ae", new Character('\346'));
            codes.put("e`", new Character('\350'));
            codes.put("e´", new Character('\351'));
            codes.put("i`", new Character('\354'));
            codes.put("i´", new Character('\355'));
            codes.put("n~", new Character('\361'));
            codes.put("o`", new Character('\362'));
            codes.put("o´", new Character('\363'));
            codes.put("o\"", new Character('\366'));
            codes.put("o/", new Character('\370'));
            codes.put("u`", new Character('\371'));
            codes.put("u´", new Character('\372'));
            codes.put("u\"", new Character('\374'));
            codes.put("gd", "delta");
            codes.put("gf", "phi");
            codes.put("gg", "gamma");
            codes.put("gl", "lambda");
            codes.put("go", "omega");
            codes.put("gp", "pi");
            codes.put("gi", "psi");
            codes.put("gs", "sigma");
            codes.put("gt", "theta");
            codes.put("gx", "xi");
        } catch (Exception ex) {
            throw new Error(ex.toString());
        }
    }

    CellularDatagram datagram;
    CellularInputStream in = null;
    CellularOutputStream out = null;
    String key;
    TreeMap<CellularDatagram,Boolean> datagramMap = null;
    boolean nbsHeaders = false;
    boolean isWap = false;
    int destPort, origPort;
    int maxCount;
    int maxMsgSize = 0;
    int numAvailable = 0;
    int refNum, refNumOut = 0;
    int type = 0;

    class CellularDatagram implements Comparable {
        byte data[];
        int refNum;
        int fragNum;

        CellularDatagram(byte data[], int num, int pos, int refNum, int fragNum) {
            this.data = new byte[num - pos];
            System.arraycopy(data, pos, this.data, 0, this.data.length);
            this.refNum = refNum;
            this.fragNum = fragNum;
        }

        public int compareTo(Object o) {
            return refNum - ((CellularDatagram)o).refNum;
        }

    }

    class CellularInputStream extends InputStream {
        CellularDatagram datagram;
        Iterator iter;
        boolean locked = true;
        byte data[] = null;
        int pos = 0, index = 0, markedPos = 0, markedIndex = 0, markedNumAvailable = 0;
        /*
          final synchronized void lock() throws IOException {
          if (locked)
          try {
          W3SocketImpl impl = getSocketImpl();
          Integer timeout = (Integer)impl.getOption(SocketOptions.SO_TIMEOUT);
          if (timeout != null) {
          wait(timeout.longValue());
          if (locked) throw new InterruptedIOException("read time out");
          } else wait();
          } catch (InterruptedException ex) {
          if (locked) {
          Thread.currentThread().interrupt();
          throw new InterruptedIOException("interrupt");
          }
          }
          locked = true;
          }

          final synchronized void unlock() {
          locked = false;
          notifyAll();
          }
        */
        CellularInputStream()
            throws IOException {
            iter = datagramMap.keySet().iterator();
            getMore();
        }

        final boolean getMore() {
            if (!iter.hasNext()) {
                data = null;
                return true;
            }
            datagram = (CellularDatagram)iter.next();
            data = datagram.data;
            pos = 0;
            index++;
            return false;
        }

        public int read()
            throws IOException {
            if (data == null || data.length <= pos && getMore()) return -1;
            numAvailable--;
            return data[pos++];
        }

        public int read(byte b[], int off, int len)
            throws IOException {
            if (data == null || data.length <= pos && getMore()) return -1;
            int n = Math.min(data.length - pos, len - off);
            System.arraycopy(data, pos, b, off, n);
            numAvailable -= n;
            pos += n;
            return n;
        }

        public long skip(long n) {
            for (long m = n = Math.min(n, available()); m > 0;) {
                if (data == null || data.length <= pos && getMore()) return n - m;
                int l = (int)Math.min((long)(data.length - pos), m);
                pos += l;
                numAvailable -= l;
            }
            return n;
        }

        public int available() {
            return numAvailable;
        }

        public synchronized void mark(int readlimit) {
            in.mark(readlimit);
            markedNumAvailable = numAvailable;
            markedIndex = index;
            markedPos = pos;
        }

        public synchronized void reset()
            throws IOException {
            in.reset();
            numAvailable = markedNumAvailable;
            index = markedIndex;
            if (index != markedIndex) {
                iter = datagramMap.keySet().iterator();
                for (index = 0; index < markedIndex; index++) datagram = (CellularDatagram)iter.next();
                data = datagram.data;
            }
            pos = markedPos;
        }

    }

    class CellularOutputStream extends OutputStream {
        CellularSocket cellular;
        MultiDatagramPacket mdp;
        ByteOutputStream out = new ByteOutputStream();
        boolean exceed = false;

        class ByteOutputStream extends ByteArrayOutputStream {

            byte[] getBuf() {
                return buf;
            }

        }

        final String pad(String s, int n) {
            if (s.length() == n) return s;
            StringBuffer sb = new StringBuffer(s);
            while (sb.length() < n) sb.insert(0, '0');
            return sb.toString();
        }

        final String hex(int i, int n) {
            return pad(Integer.toHexString(i).toUpperCase(), n);
        }

        CellularOutputStream(byte address[], int port, CellularSocket cellular) {
            this.cellular = cellular;
            mdp = new MultiDatagramPacket(type == NBS ? null : new byte[maxMsgSize], maxMsgSize, address, port);
        }

        public void write(int b)
            throws IOException {
            if (out.size() >= maxCount) {
                exceed = true;
                throw new IOException("Maximum count exceeded");
            }
            out.write(b);
        }

        public void write(byte b[])
            throws IOException {
            write(b, 0, b.length);
        }

        public void write(byte b[], int off, int len)
            throws IOException {
            if (out.size() + len > maxCount) {
                exceed = true;
                if (out.size() < maxCount) out.write(b, off, maxCount - out.size());
                throw new IOException("Maximum count exceeded");
            }
            out.write(b, off, len);
        }

        public void close()
            throws IOException {
            try {
                out.flush();
                int count = out.size();
                if (count == 0) return;
                byte buf[] = out.getBuf();
                if (exceed) System.arraycopy(dots, 0, buf, count - dots.length, dots.length);
                if (type == CIMD) {
                    W3SocketImpl impl = getSocketImpl();
                    mdp.setPort(impl.getPort());
                    int msgFree = nbsHeaders ? NBS_MSG_FREE : PLAIN_MSG_FREE, total = (count + msgFree - 1) / msgFree;
                    String title =  nbsHeaders ? "//SCKL" + hex(mdp.getPort(), 4) + hex(getLocalPort(), 4) + hex(getRefNumOut(), 2) + hex(total, 2) :
                        "><" + pad(String.valueOf(getRefNumOut()), 3) + "-" + pad(String.valueOf(total), 3) + "-";
                    byte data[] = mdp.getData();
                    boolean putWaitSign = false;
                    if (nbsHeaders && mdp.getPort() == TTML_PORT && count + 2 <= maxCount) {
                        boolean inLine = false;
                        for (int i = 0; i < count; i++)
                            if (!inLine && reserved.indexOf(buf[i]) != -1) {
                                putWaitSign = true;
                                break;
                            } else inLine = buf[i] != '\n';
                    }
                    int num = 1;
                    for (int off = 0; off < count; num++) {
                        StringBuffer sb = new StringBuffer();
                        sb.append(title);
                        sb.append(nbsHeaders ? hex(num, 2) : pad(String.valueOf(num), 3));
                        sb.append('\n');
                        if (putWaitSign) {
                            sb.append("..\n");
                            putWaitSign = false;
                        }
                        String header = sb.toString();
                        int l = header.length(), n;
                        System.arraycopy(header.getBytes(), 0, data, 0, l);
                        System.arraycopy(buf, off, data, l, n = Math.min(SMS_MSG_SIZE - l, count - off));
                        System.arraycopy(zeros, 0, data, l += n, SMS_MSG_SIZE - l);
                        mdp.setLength(l);
                        //              cellular.send(mdp);
                        try {
                            cellular.send(mdp);
                        } catch (SocketException ex) {}
                        off += n;
                    }
                } else {
                    mdp.setData(buf);
                    mdp.setLength(count);
                    cellular.send(mdp);
                }
            } finally {
                exceed = false;
                out.reset();
            }
        }

    }

    protected CellularSocket(int type)
        throws SocketException {
        super(type == MOBILE ? (SocketImpl)new MobileSocketImpl() :
              (SocketImpl)new MultiSocketImpl(type == UDP ? MultiSocketImpl.AF_INET :
                                              type == NBS ? MultiSocketImpl.AF_NBS : MultiSocketImpl.AF_GSMSMS,
                                              MultiSocketImpl.SOCK_DGRAM, 0, type == UDP ? MultiSocketImpl.IPPROTO_IP : cellularSocketInfo, 0, 0));
        this.type = type;
    }

    /** Constructs bound socket. */
    public CellularSocket(byte address[], int port, int type)
        throws IOException {
        this(type);
        W3SocketImpl impl = getSocketImpl();
        impl.create(false);
        impl.bind(address, 0, port);
        System.out.println("cellular socket of type " + type + " bound to port " + port);
        in = null;
        out = null;
        key = null;
        nbsHeaders = false;
        maxMsgSize = type == UDP ? ((Integer)impl.getOption(SocketOptions.SO_SNDBUF)).intValue() : SMS_MSG_SIZE;
        System.out.println("maxMsgSize="+maxMsgSize);
    }

    /** Constructs output socket.
        @param address address to bind
        @param port local port to bind
        @param address1 destination address
        @param port1 destination port
        @param dataCoding data coding to be used with nbs
        @param nbsHeaders use nbs headers
        @param type socket type, must be UDP, NBS, CIMD or MOBILE */
    public CellularSocket(byte address[], int port, byte address1[], int port1, int dataCoding, boolean nbsHeaders, int type)
        throws IOException {
        this(address, port, type);
        W3SocketImpl impl = getSocketImpl();
        if (type == NBS) {
            impl.setIOControl(MultiSocketImpl.NBS_SETHEADERCODING, nbsHeaders ? BINARY_HEADERS : NO_HEADERS);
            if (dataCoding != -1) impl.setIOControl(MultiSocketImpl.NBS_DATACODING, dataCoding);
        } else this.nbsHeaders = nbsHeaders;
        impl.addr = address1;
        impl.setPort(port1);
        maxCount = type == UDP ? udpMaxCount : nbsHeaders ? nbsMaxCount : plainMaxCount;
        out = new CellularOutputStream(address1, port1, this);
        CellularSocket cellular = instances.get(new String(address1).trim());
        if (cellular != null) refNumOut = cellular.getRefNumOut();
        instances.put(key, this);
        this.key = key;
    }

    /** Constructs input/output socket.
        @param address address to bind
        @param port local port to bind
        @param mdp gives destination address */
    public CellularSocket(byte address[], int port, MultiDatagramPacket mdp, int type, String key)
        throws IOException {
        this(address, port, mdp.getAddress(), mdp.getPort(), 0, false, type);
        this.key = key;
        datagramMap = new TreeMap<CellularDatagram,Boolean>();
    }

    public InputStream getInputStream()
        throws IOException {
        if (in != null) return in;
        if (datagramMap == null) throw new IOException("Input not supported");
        return in = new CellularInputStream();
    }

    public OutputStream getOutputStream()
        throws IOException {
        if (out == null) throw new IOException("Output not supported");
        return out;
    }

    public int getType() {
        return type;
    }

    public void close()
        throws IOException {
        try {
            if (out != null) out.flush();
        } catch (IOException ex) {} finally {
            if (key != null) instances.remove(key);
            super.close();
        }
    }

    public String toString() {
        W3SocketImpl impl = getSocketImpl();
        return new String(impl.addr);
    }

    protected void finalize() {
        try {
            close();
        } catch (IOException ex) {}
    }

    protected synchronized int getRefNumOut() {
        return refNumOut <= 255 ? refNumOut++ : (refNumOut = 0);
    }

    public int getRefNum()
        throws SocketException {
        return refNum;
    }

    public int getMaxMsgSize() {
        return maxMsgSize;
    }

    public boolean isWap() {
        return isWap;
    }

    public int outSize() {
        if (out == null) return -1;
        return out.out.size();
    }

    /** Sets maximum number of plain messages to be sent in succession. */
    public static void setMaxPlainMsgs(int maxPlainMsgs) {
        plainMaxCount = maxPlainMsgs * PLAIN_MSG_FREE;
    }

    /** Sets maximum number of NBS-messages to be sent in succession. */
    public static void setMaxNbsMsgs(int maxNbsMsgs) {
        nbsMaxCount = maxNbsMsgs * NBS_MSG_FREE;
    }

    /** Sets maximum number of bytes to be sent in succession for UDP-connections. */
    public static void setUdpMaxCount(int udpMaxCount) {
        CellularSocket.udpMaxCount = udpMaxCount;
    }

    /** Returns new cellular socket whose all datagrams have arrived,
        or null if no complete input streams available. */
    public CellularSocket acceptDatagram()
        throws IOException {
        System.out.println("waiting for datagram");
        MultiDatagramPacket mdp = new MultiDatagramPacket(new byte[maxMsgSize], maxMsgSize);
        receive(mdp);
        System.out.println("received datagram with length " + mdp.getLength());
        new HexDumpEncoder().encodeStream(new ByteArrayInputStream(mdp.getData(), 0, mdp.getLength()), System.out);
        String servAccAddr, smscAddr;
        boolean wap = false;
        byte data[] = mdp.getData();
        int num = mdp.getLength(), pos = 0;
        int refNum = 0, fragNum = 0, totalNum = -1;
        if (type == NBS) {
            W3SocketImpl impl = getSocketImpl();
            refNum = impl.getIOControl(MultiSocketImpl.NBS_REFNUM);
            totalNum = impl.getIOControl(MultiSocketImpl.NBS_CONCATINFO);
        } else {
            boolean nonWtp = false;
            // Fixed header part
            // Continue Flag
            boolean con = (data[pos] & 0x80) != 0;
            // PDU Type
            int pduType = (data[pos] >> 3) & 0xf;
            switch (pduType) {
            case WTP_Invoke: {
                // Group Trailer Flag
                boolean gtr = (data[pos] & 0x4) != 0,
                    // Transmission Trailer Flag
                    ttr = (data[pos] & 0x2) != 0,
                    // Re-transmission Indicator
                    rid = (data[pos] & 0x1) != 0;
                int tid = (data[++pos] & 0xff) << 16 | data[++pos],
                    // Version number
                    version = (data[++pos] >> 6) & 0x3;
                // TIDnew flag
                boolean tidNew = (data[pos] & 0x20) != 0,
                    // User acknowledgement flag
                    u_p = (data[pos] & 0x10) != 0;
                // Transaction Class
                int tcl = data[pos] & 0x3;
                refNum = tid;
                break;
            }
            case WTP_Result:
                break;
            case WTP_Ack:
                break;
            case WTP_Abort:
                break;
            case WTP_Segmented_Invoke: {
                // Group Trailer Flag
                boolean gtr = (data[pos] & 0x4) != 0,
                    // Transmission Trailer Flag
                    ttr = (data[pos] & 0x2) != 0,
                    // Re-transmission Indicator
                    rid = (data[pos] & 0x1) != 0;
                int tid = (data[++pos] & 0xff) << 16 | data[++pos],
                    packetSequenceNumber = data[++pos] & 0xff;
                break;
            }
            case WTP_Segmented_Result:
                break;
            case WTP_Negative_Ack:
                break;
            default:
                nonWtp = true;
            }
            if (!nonWtp) {
                wap = true;
                // Variable header part
                while (con) {
                    // Continue Flag
                    con = (data[++pos] & 0x80) != 0;
                    // Transport Information Item Identity
                    int tpiIdentity = (data[pos] >> 3) & 0xf;
                    // Long Transport Information Item Flag
                    boolean longTpi = (data[pos] & 0x4) != 0;
                    // Transport Information Item Length
                    int tpiLength = longTpi ? (data[++pos] & 0xff) : (data[pos] & 0x3),
                        lastPos = pos + tpiLength;
                    switch (tpiIdentity) {
                        // Error Transport Information Item
                    case TPI_Error: {
                        // Error Code
                        int errorCode = (data[++pos] >> 4) & 0xf,
                            // Bad TPI Identity
                            badTPIIdentity = data[pos] & 0xf;
                        if (errorCode == 0x02) {
                            // Known TPI, unknown content
                            int firstOctetOfTPI = data[++pos] & 0xff;
                        }
                        break;
                    }
                    case TPI_Info: {
                        // Info Transport Information Item
                        break;
                    }
                    case TPI_Option: {
                        // Option Transport Information Item
                        int optionIdentity = data[++pos] & 0xff;
                        break;
                    }
                    case TPI_Packet_Sequence_Number: {
                        // Packet Sequence Number Transport Information Item
                        int packetSequenceNumber = data[++pos] & 0xff;
                        break;
                    }
                    } while (pos <= lastPos) pos++;
                }
                if (type == UDP) maxCount = udpMaxCount;
                else maxCount = nbsMaxCount;
            } else {
                boolean rawData = false;
                refNum = -1;
                if (type == UDP && getLocalPort() == WAP_CO_PORT) wap = rawData = true;
                else for (int i = num; i >= 0; i--)
                         if (data[i] < ' ') {
                             wap = rawData = true;
                             break;
                         }
                if (type == UDP) maxCount = udpMaxCount;
                else if (rawData) maxCount = plainMaxCount;
                else if (num > 1 && data[0] == '/' && data[1] == '/') {
                    nbsHeaders = true;
                    maxCount = nbsMaxCount;
                } else {
                    nbsHeaders = false;
                    maxCount = plainMaxCount;
                    if (num < 3 || data[0] != '>' || data[1] != '<' || !Character.isDigit((char)data[2])) {
                        mdp.setPort(TEXT_PORT);
                        rawData = true;
                    }
                }
                if (!rawData) {
                    StringBuffer sb = new StringBuffer();
                    while (pos < num && Support.whites.indexOf(data[pos]) == -1) sb.append((char)data[pos++]);
                    while (pos < num && Support.whites.indexOf(data[pos]) != -1) pos++;
                    String header = sb.toString().toUpperCase();
                    if (nbsHeaders) {
                        int i, j, k;
                        for (i = 2; i < header.length(); i = j + 2) {
                            if ((j = header.indexOf("//", i)) == -1) j = header.length();
                            if (header.regionMatches(i, "CAT", 0, 3)) k = i + 3;
                            else if (header.regionMatches(i, "SCKL", 0, 4)) {
                                // long port number format
                                destPort = Integer.parseInt(header.substring(i + 4, i + 8), 16);
                                if (j > i + 8) {
                                    // full header
                                    origPort = Integer.parseInt(header.substring(i + 8, i + 12), 16);
                                    k = i + 12;
                                } else continue;
                            } else if (header.regionMatches(i, "SCK", 0, 3)) {
                                // short port number format
                                destPort = Integer.parseInt(header.substring(i + 3, i + 5), 16);
                                if (j > i + 5) {
                                    // full header
                                    origPort = Integer.parseInt(header.substring(i + 5, i + 7), 16);
                                    k = i + 7;
                                } else continue;
                            } else if (header.regionMatches(i, "SAP", 0, 3)) {
                                servAccAddr = header.substring(3, 7);
                                smscAddr = header.substring(8, 15);
                                continue;
                            } else continue;
                            // concatenated SM reference number
                            refNum = Integer.parseInt(header.substring(k, k + 2), 16);
                            // total number of fragments
                            totalNum = Integer.parseInt(header.substring(k + 2, k + 4), 16);
                            // fragment number
                            fragNum = Integer.parseInt(header.substring(k + 4, k + 6), 16);
                        }
                    } else {
                        // Cellular Socket custom headers
                        StringTokenizer st = new StringTokenizer(header.substring(2), "-");
                        if (st.hasMoreTokens()) {
                            refNum = Integer.parseInt(st.nextToken());
                            if (st.hasMoreTokens()) {
                                totalNum = Integer.parseInt(st.nextToken());
                                if (st.hasMoreTokens()) fragNum = Integer.parseInt(st.nextToken());
                            }
                        }
                    }
                    mdp.setPort(origPort != 0 ? origPort : destPort != 0 ? destPort : TEXT_PORT);
                }
            }
        }
        key = new String(mdp.getAddress()).trim() + "/" + refNum;
        CellularSocket cellular = instances.get(key);
        if (cellular == null) cellular = new CellularSocket(getLocalByteAddress(), getLocalPort(), mdp, getType(), key);
        cellular.datagramMap.put(new CellularDatagram(data, num, pos, refNum, fragNum), Boolean.TRUE);
        cellular.numAvailable += num - pos;
        cellular.isWap = wap;
        System.out.println("totalNum="+totalNum+",size="+cellular.datagramMap.size());
        System.out.println("mdp.port="+mdp.getPort()+",iswap="+isWap);
        if (totalNum == -1 || cellular.datagramMap.size() == totalNum) return cellular;
        //  cellular.in.unlock();
        return null;
    }

}
