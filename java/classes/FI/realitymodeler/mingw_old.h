
#define __int64 long long
#define int64_t discard_int64_t
#define uint64_t discard_uint64_t
#include <typedefs_md.h>
#include <_g_config.h>
#define __int64 __int64
#define int64_t int64_t
#define uint64_t uint64_t
typedef _G_int64_t __int64;
typedef _G_int64_t int64_t;
typedef _G_uint64_t uint64_t;
#include <jni_md.h>
#define JNICALL
#define JNIEXPORT
#define JNIIMPORT
#define __declspec(dummy)
