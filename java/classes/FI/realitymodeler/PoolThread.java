
package FI.realitymodeler;

/** Base class for threads using thread pool.
    @see FI.realitymodeler.ThreadPool */
public abstract class PoolThread extends Thread implements Cloneable {
    ThreadPool pool;
    volatile boolean locked = true, released = false;

    synchronized boolean lock(long timeout) throws InterruptedException {
        if (locked)
            if (timeout > 0L) wait(timeout);
            else wait();
        return locked ? false : (locked = true);
    }

    synchronized void unlock() {
        locked = false;
        notifyAll();
    }

    public PoolThread() {
    }

    public PoolThread(Runnable target) {
        super(target);
    }

    public PoolThread(ThreadGroup group, Runnable target) {
        super(group, target);
    }

    public PoolThread(String name) {
        super(name);
    }

    public PoolThread(ThreadGroup group, String name) {
        super(group, name);
    }

    public PoolThread(ThreadGroup group, Runnable target, String name) {
        super(group, target, name);
    }

    public abstract Object clone();

    public abstract void run();

    protected void finalize() {
        interrupt();
    }

}
