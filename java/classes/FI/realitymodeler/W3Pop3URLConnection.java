
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.mail.search.*;
import javax.servlet.*;

/** Implements URL connection to Post Office Protocol 3 -servers. URL must be of form:<br>
    When getting input stream of message list:<br>
    pop3://host[/from]<br>
    [?auto= : quote original automatically<br>
    &amp;contents= : show in list also message contents<br>
    &amp;date=messages newer than date are listed<br>
    &amp;from=reply email address<br>
    &amp;only=only messages newer than cookie value or date are listed]<br>
    When getting input stream of specified message:<br>
    pop3://host/message number/message id<br>
    [?delete= : message is only deleted<br>
    &amp;forward= : get message as raw text<br>
    &amp;header= : show message header<br>
    &amp;plain= : only plain text is returned from message<br>
    &amp;remove= : removes message after reading]<br>
    When getting output stream of unique id list of messages to be moved to some category:<br>
    pop3://host<br>
    Basic credentials must be in request property Authorization.<br>
*/
public class W3Pop3URLConnection extends W3MsgURLConnection implements Cloneable, Comparator<MsgItem> {
    static boolean useClient = false;

    public static long cacheCleaningInterval = 24L * 60L * 60L * 1000L, cacheCleaningTime = 0L;
    public static String cacheRoot = "cache/pop3/";
    public static boolean defaultUseCaches = false, useProxy = false;
    public static String proxyHost = null;
    public static int proxyPort = 80;
    public static String pop3ServerHost = null;
    public static String version = "FI.realitymodeler.W3Pop3URLConnection/2000-5-10";

    boolean secure = false;

    class Category {
        W3URLConnection ucIn = null, ucOut = null;
        String category, from;
        byte b[] = null;
    }

    public Object clone() {
        W3Pop3URLConnection uc = new W3Pop3URLConnection(url);
        uc.requester = requester;
        uc.msgs = msgs;
        uc.cachePath = cachePath;
        uc.host = host;
        uc.msgVec = msgVec;
        uc.loggedIn = loggedIn;
        return uc;
    }

    public String getCacheRoot() {
        return cacheRoot;
    }

    public synchronized HeaderList getHeader(W3Requester requester, MsgItem msgItem) throws IOException, java.text.ParseException {
            return (HeaderList)msgItem.headerList.clone();
        }

    public synchronized InputStream getEntity(W3Requester requester, MsgItem msgItem) throws IOException, java.text.ParseException {
            if (useClient)
                try {
                    SearchTerm messageIDTerm = new MessageIDTerm(msgItem.msgId);
                    Message messages[] = ((W3Pop3Requester)requester).folder.search(messageIDTerm);
                    if (messages == null || messages.length == 0) throw new IOException("Message " + msgItem.msgId + " not found");
                    Message message = messages[0];
                    /*
                      message.setFlag(Flags.Flag.SEEN, true);
                      msgItem.headerList.remove("X-Unread");
                    */
                    return ((MimeMessage)message).getRawInputStream();
                } catch (MessagingException ex) {
                    throw new IOException(Support.stackTrace(ex));
                }
            requester.send("RETR " + msgItem.index + "\r\n");
            checkOK(requester);
            HeaderList headerList = new HeaderList();
            headerList.setHeaders(requester.input);
            msgItem.headerList.remove("X-Unread");
            return Support.getDotInputStream(requester.input);
        }

    void delete(MsgPipingThread piping) throws IOException {
        MsgItem msgItem = piping.msgs.get(piping.uniqueID);
        if (msgItem == null) return;
        if (useClient) {
            try {
                Message message = ((W3Pop3Requester)requester).folder.getMessage(msgItem.index);
                message.setFlag(Flags.Flag.DELETED, true);
            } catch (MessagingException ex) {
                throw new IOException(Support.stackTrace(ex));
            }
        } else {
            piping.requester.send("DELE " + msgItem.index + "\r\n");
            try {
                checkOK(piping.requester);
            } catch (IOException ex) {
                if (ex instanceof SocketException) throw ex;
            }
        }
        piping.msgs.remove(piping.uniqueID);
        new EntityCache(piping.cachePath + Support.encode(piping.uniqueID, null)).reset();
    }

    Object openCategoryObject(String category, String from) throws IOException {
        if (useClient)
            try {
                if (category.equalsIgnoreCase("destruction")) return null;
                Folder folder = ((W3Pop3Requester)requester).store.getFolder(category);
                if (!folder.exists()) folder.create(folder.HOLDS_FOLDERS | folder.HOLDS_MESSAGES);
                folder.open(folder.READ_WRITE);
                return folder;
            } catch (MessagingException ex) {
                throw new IOException(Support.stackTrace(ex));
            }
        Category categoryObject = new Category();
        categoryObject.category = category;
        categoryObject.from = from;
        categoryObject.b = category.equalsIgnoreCase("destruction") ? null : new byte[Support.bufferLength];
        return categoryObject;
    }

    void move(MsgPipingThread piping, Object categoryObject) throws IOException {
        MsgItem msgItem = piping.msgs.get(piping.uniqueID);
        if (msgItem == null) return;
        if (useClient) {
            try {
                Message message = ((W3Pop3Requester)requester).folder.getMessage(msgItem.index);
                ((W3Pop3Requester)requester).folder.copyMessages(new Message[] {message}, (Folder)categoryObject);
                message.setFlag(Flags.Flag.DELETED, true);
            } catch (MessagingException ex) {
                throw new IOException(Support.stackTrace(ex));
            }
            piping.msgs.remove(piping.uniqueID);
            new EntityCache(piping.cachePath + Support.encode(piping.uniqueID, null)).reset();
            return;
        }
        URL url = new URL("pop3://" + host + "/" + msgItem.index + "/" + Support.encode(piping.uniqueID, null) + "?forward=on&remove=on");
        if (((Category)categoryObject).ucIn == null) ((Category)categoryObject).ucIn = (W3URLConnection)clone();
        ((Category)categoryObject).ucIn.set(url);
        if (piping.authorization != null) ((Category)categoryObject).ucIn.setRequestProperty("Authorization", piping.authorization);
        url = new URL("mailto:" + ((Category)categoryObject).from);
        if (((Category)categoryObject).ucOut == null || !((Category)categoryObject).ucOut.mayKeepAlive() || !((Category)categoryObject).ucOut.check(url)) {
            if (((Category)categoryObject).ucOut != null) {
                ((Category)categoryObject).ucOut.disconnect();
                ((Category)categoryObject).ucOut = null;
            }
            ((Category)categoryObject).ucOut = openConnection(url);
            ((Category)categoryObject).ucOut.setServletContext(servletContext);
        } else ((Category)categoryObject).ucOut.set(url);
        InputStream in = ((Category)categoryObject).ucIn.getInputStream();
        if (((Category)categoryObject).ucIn.getResponseCode() != HTTP_OK) throw new IOException("Wrong status " + ((Category)categoryObject).ucIn.getResponseCode());
        String key;
        for (int i = 1; (key = ((Category)categoryObject).ucIn.getHeaderFieldKey(i)) != null; i++)
            ((Category)categoryObject).ucOut.addRequestProperty(key, ((Category)categoryObject).ucIn.getHeaderField(i));
        ((Category)categoryObject).ucOut.setRequestProperty("X-Category", ((Category)categoryObject).category);
        OutputStream out = ((Category)categoryObject).ucOut.getOutputStream();
        byte b[] = ((Category)categoryObject).b;
        for (int n; (n = in.read(b)) > 0;) out.write(b, 0, n);
        out.close();
        ((Category)categoryObject).ucIn.closeStreams();
        in = ((Category)categoryObject).ucOut.getInputStream();
        if (((Category)categoryObject).ucOut.getResponseCode() != HTTP_OK && ((Category)categoryObject).ucOut.getResponseCode() != HTTP_NO_CONTENT)
            throw new IOException("Wrong status " + ((Category)categoryObject).ucOut.getResponseCode());
        if (in != null) for (int n; (n = in.read(b)) > 0;);
        ((Category)categoryObject).ucOut.closeStreams();
        ((Category)categoryObject).ucIn.complete();
    }

    void closeCategoryObject(Object categoryObject) {
        if (useClient) {
            if (categoryObject != null)
                try {
                    ((Folder)categoryObject).close(true);
                } catch (MessagingException ex) {}
            return;
        }
        if (categoryObject == null || ((Category)categoryObject).ucOut == null) return;
        ((Category)categoryObject).ucOut.disconnect();
        ((Category)categoryObject).ucOut = null;
    }

    public static void cleanCache(ServletContext servletContext) {
        cacheCleaningTime = cleanCache(servletContext, cacheRoot, cacheCleaningInterval);
    }

    public W3Pop3URLConnection(URL url) {
        super(url);
        secure = url.getProtocol().equalsIgnoreCase("pop3s");
    }

    public String getVersion() {
        return version;
    }

    public void checkCacheCleaning() {
        if (cacheCleaningInterval >= 0L &&
            System.currentTimeMillis() - cacheCleaningTime > cacheCleaningInterval) cleanCache(servletContext);
    }

    public void open() throws IOException, java.text.ParseException {
        if (requester != null || username.equals("") || password.equals("")) return;
        if (useProxy) {
            proxyConnect(proxyHost, proxyPort);
            return;
        }
        if (useClient)
            try {
                String category = getRequestProperty("x-category");
                requester = new W3Pop3Requester(host, url.getPort() != -1 ? url.getPort() : -1, username, password, category, secure);
                if (timeout > 0) requester.setTimeout(timeout);
                loggedIn = true;
                if (list()) return;
                first = Integer.MAX_VALUE;
                last = 0;
                Message messages[] = ((W3Pop3Requester)requester).folder.getMessages();
                for (int i = 0; i < messages.length; i++) {
                    MimeMessage message = (MimeMessage)messages[i];
                    if (msgs.containsKey(message.getMessageID())) continue;
                    int n = message.getMessageNumber();
                    HeaderList headerList = new HeaderList();
                    Enumeration allHeaders = message.getAllHeaders();
                    while (allHeaders.hasMoreElements()) {
                        javax.mail.Header header = (javax.mail.Header)allHeaders.nextElement();
                        headerList.append(new FI.realitymodeler.common.Header(header.getName(), header.getValue()));
                    }
                    MsgItem msgItem = new MsgItem(headerList, null, null, n);
                    msgItem.msgId = ((W3Pop3Requester)requester).folder.getUID(message);
                    if (useCaches && msgItem.msgId != null) {
                        String idEncoded = Support.encode(msgItem.msgId, null);
                        W3File file = new W3File(cachePath + idEncoded + "_");
                        if (!file.exists()) {
                            BufferedOutputStream fout = new BufferedOutputStream(new FileOutputStream(file));
                            Support.sendHeaderList(fout, headerList);
                            fout.close();
                        }
                    }
                    if (!message.isSet(Flags.Flag.SEEN)) headerList.replace(new FI.realitymodeler.common.Header("X-Unread", "true"));
                    msgVec.addElement(msgItem);
                    if (msgItem.msgId != null) msgs.put(msgItem.msgId, msgItem);
                    first = Math.min(first, n);
                    last = Math.max(last, n);
                }
                Collections.sort(msgVec, (Comparator<MsgItem>)this);
                saveSession();
                return;
            } catch (MessagingException ex) {
                if (ex instanceof AuthenticationFailedException) return;
                throw new IOException(Support.stackTrace(ex));
            }
        requester = new W3Requester(host, url.getPort() != -1 ? url.getPort() : secure ? W3Pop3Requester.POP3_SECURE_PORT : W3Pop3Requester.POP3_PORT, secure ? W3Socket.SECURE : W3Socket.NORMAL, localAddress, localPort);
        if (timeout > 0) requester.setTimeout(timeout);
        checkOK(requester);
        try {
            requester.send("USER " + username + "\r\n");
            checkOK(requester);
            requester.send("PASS " + password + "\r\n");
            checkOK(requester);
            loggedIn = true;
        } catch (IOException ex) {
            if (ex instanceof SocketException) throw ex;
            return;
        }
        if (list()) return;
        List<Integer> msgNumbers = new ArrayList<Integer>();
        first = Integer.MAX_VALUE;
        last = 0;
        String s;
        requester.send("LIST\r\n");
        checkOK(requester);
        InputStream source = Support.getDotInputStream(requester.input);
        while ((s = Support.readLine(source)) != null) {
            StringTokenizer st = new StringTokenizer(s);
            int n = Integer.parseInt(st.nextToken());
            msgNumbers.add(new Integer(n));
            first = Math.min(first, n);
            last = Math.max(last, n);
        }
        int length = msgNumbers.size();
        for (int i = 0; i < length; i++) {
            int n = msgNumbers.get(i).intValue();
            requester.send("TOP " + n + " 0\r\n");
            try {
                checkOK(requester);
                HeaderList headerList = new HeaderList();
                source = Support.getDotInputStream(requester.input);
                headerList.setHeaders(source);
                while (source.read() != -1);
                MsgItem msgItem = new MsgItem(headerList, null, null, n);
                if (useCaches && msgItem.msgId != null) {
                    String idEncoded = Support.encode(msgItem.msgId, null);
                    W3File file = new W3File(cachePath + idEncoded + "_");
                    if (!file.exists()) {
                        BufferedOutputStream fout = new BufferedOutputStream(new FileOutputStream(file));
                        Support.sendHeaderList(fout, headerList);
                        fout.close();
                    }
                }
                msgVec.addElement(msgItem);
                if (msgItem.msgId != null) msgs.put(msgItem.msgId, msgItem);
            } catch (IOException ex) {}
        }
        Collections.sort(msgVec, (Comparator<MsgItem>)this);
        saveSession();
    }

    public void close() {
        if (useClient) return;
        if (requester != null && !useProxy) {
            try {
                requester.send("QUIT\r\n");
                checkOK(requester);
            } catch (IOException ex) {}
        }
        super.close();
    }

    public void doConnect()
        throws IOException {
        if ((host = url.getHost()).equals("") && (host = pop3ServerHost) == null &&
            (host = System.getProperty("pop3.host")) == null) throw new IOException("No host");
        getBasicCredentials();
        if (username == null || username.equals("")) {
            in = origin = getUnauthorizedStream("Messaging authorization required");
            inputDone = true;
            return;
        }
        String category = getRequestProperty("x-category");
        cachePath = cacheRoot + host.toLowerCase() + "/" + username + "/" + (category != null && !(category = category.trim()).equals("") ? category + "/" : "");
        W3File dir = new W3File(cachePath.replace('/', File.separatorChar));
        if (!dir.exists()) dir.mkdirs();
        StringTokenizer st = new StringTokenizer(path, "/");
        int tc = st.countTokens();
        if (tc > 2) {
            // With attachments cache must be used always
            st.nextToken();
            setCheckCaches(true);
            if (checkCacheFile(cachePath + st.nextToken(), st.nextToken())) return;
        }
        try {
            open();
        } catch (java.text.ParseException ex) {
            throw new IOException(Support.stackTrace(ex));
        }
    }

    final void checkOK(W3Requester requester) throws IOException {
        String s;
        if ((s = Support.readLine(requester.input)) == null || !s.startsWith("+OK")) throw new IOException(s);
    }

    public void setDefaultUseCaches(boolean useCaches) {
        W3Pop3URLConnection.defaultUseCaches = useCaches;
    }

    public boolean getDefaultUseCaches() {
        return W3Pop3URLConnection.defaultUseCaches;
    }

    public int getDefaultPort() {
        return W3Pop3Requester.POP3_PORT;
    }

    public boolean usingProxy() {
        return useProxy;
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public int getProxyPort() {
        return proxyPort;
    }

    public void setCacheCleaningInterval(long cacheCleaningInterval) {
        W3Pop3URLConnection.cacheCleaningInterval = cacheCleaningInterval;
    }

}
