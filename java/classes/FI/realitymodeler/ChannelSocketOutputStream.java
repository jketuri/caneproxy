
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.nio.*;
import java.nio.channels.*;
import java.nio.channels.spi.*;

public class ChannelSocketOutputStream extends OutputStream {
    SocketChannel socketChannel = null;
    SelectionKey selectionKey = null;
    Selector selector = null;
    ByteBuffer buffer = null;

    public ChannelSocketOutputStream(SocketChannel socketChannel)
        throws IOException {
        this.socketChannel = socketChannel;
        selector = Selector.open();
        selectionKey = socketChannel.register(selector, SelectionKey.OP_WRITE);
        buffer = ByteBuffer.allocateDirect(Support.bufferLength);
        buffer.clear();
    }

    private boolean demandWrite() throws IOException {
        selector.select();
        /*
        SelectionKey selectionKey = socketChannel.keyFor(selector);
        int interestOps = selectionKey.interestOps();
        interestOps |= SelectionKey.OP_WRITE;
        selectionKey.interestOps(interestOps);
        selector.wakeup();
        try {
            synchronized (this) {
                wait();
            }
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
            return true;
        }
        */
        writeBuffer();
        return false;
    }

    private void leaveWrite() {
        SelectionKey selectionKey = socketChannel.keyFor(selector);
        int interestOps = selectionKey.interestOps();
        interestOps &= 0xffffffff ^ SelectionKey.OP_WRITE;
        selectionKey.interestOps(interestOps);
    }

    private void writeBuffer() throws IOException {
        buffer.flip();
        socketChannel.write(buffer);
        buffer.clear();
    }

    void signal() {
        synchronized (this) {
            notifyAll();
        }
    }

    public final void write(int b) throws IOException {
        buffer.put((byte)b);
        demandWrite();
    }

    public final void write(byte buf[]) throws IOException {
        write(buf, 0, buf.length);
    }

    public final void write(byte buf[], int off, int len) throws IOException {
        while (len > 0) {
            int length = Math.min(buffer.limit(), len);
            buffer.put(buf, off, length);
            demandWrite();
            off += length;
            len -= length;
        }
    }

    public final void flush() throws IOException {
    }

    public final void close() throws IOException {
    }

}
