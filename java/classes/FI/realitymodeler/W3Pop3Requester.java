
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import com.sun.mail.pop3.*;
import java.io.*;
import java.util.*;
import javax.mail.*;

public class W3Pop3Requester extends W3Requester {
    public static boolean DEBUG = false;
    public static final int POP3_PORT = 110;
    public static final int POP3_SECURE_PORT = 995;

    Store store = null;
    POP3Folder folder = null;

    private W3Pop3Requester() {
    }

    public W3Pop3Requester(String host, int port, String user, String password, String category, boolean secure) throws IOException, NoSuchProviderException, MessagingException, LoginException {
        open(host, port, user, password, category, secure);
    }

    public W3Pop3Requester(String host, String user, String password, String category) throws IOException, NoSuchProviderException, MessagingException, LoginException {
        open(host, user, password, category);
    }

    public W3Pop3Requester(String host, int port, String user, String password) throws IOException, NoSuchProviderException, MessagingException, LoginException {
        open(host, port, user, password, null, false);
    }

    public W3Pop3Requester(String host, String user, String password) throws IOException, NoSuchProviderException, MessagingException, LoginException {
        open(host, user, password, null);
    }

    public Object clone() {
        W3Pop3Requester pop3Requester = new W3Pop3Requester();
        pop3Requester.store = store;
        pop3Requester.folder = folder;
        return pop3Requester;
    }

    public void open(String host, int port, String user, String password, String category, boolean secure) throws IOException, NoSuchProviderException, MessagingException, LoginException {
        if (store != null) close();
        if (port == -1) port = secure ? POP3_SECURE_PORT : POP3_PORT;
        this.host = host;
        this.port = port;
        Properties properties = new Properties();
        Session session = Session.getInstance(properties,  null);
        if (DEBUG) session.setDebug(true);
        store = session.getStore("pop3" + (secure ? "s" : ""));
        try {
            store.connect(host, port, user, password);
        } catch (AuthenticationFailedException ex) {
            throw new LoginException(ex.getMessage(), makeRealm());
        }
        folder = (POP3Folder)store.getFolder(category != null && !(category = category.trim()).equals("") ? category : "INBOX");
        if (!folder.exists()) folder.create(folder.HOLDS_FOLDERS | folder.HOLDS_MESSAGES);
        folder.open(Folder.READ_WRITE);
    }

    public void open(String host, String user, String password, String category) throws IOException, NoSuchProviderException, MessagingException, LoginException {
        open(host, -1, user, password, category, false);
    }

    public void close() throws IOException {
        try {
            if (folder != null) {
                folder.close(true);
                folder = null;
            }
        } catch (MessagingException ex) {
            throw new IOException(Support.stackTrace(ex));
        } finally {
            try {
                if (store != null) {
                    store.close();
                    store = null;
                }
            } catch (MessagingException ex) {
                throw new IOException(Support.stackTrace(ex));
            }
        }
    }

}
