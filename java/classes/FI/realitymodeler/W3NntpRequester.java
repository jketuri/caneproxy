
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;

/** Client for network news transfer protocol servers. */
public class W3NntpRequester extends W3CopyRequester implements Cloneable {
    public static final int NNTP_PORT = 119;
    public static SimpleDateFormat newsDateFormat = new SimpleDateFormat("yyMMdd HHmmss 'GMT'");

    ConvertOutputStream articleStream = null;
    InetAddress localAddress;
    String host;
    int localPort;
    int port;

    class ArticleOutputStream extends FilterOutputStream {
        boolean closed = false;

        ArticleOutputStream(ConvertOutputStream out) {
            super(out);
        }

        public void close()
            throws IOException {
            if (closed) return;
            if (((ConvertOutputStream)out).getLastChar() != '\n') {
                out.write('\r');
                out.write('\n');
            }
            out.flush();
            send(".", 240, false, false, null);
            closed = true;
        }

    }

    public W3NntpRequester() {
    }

    public W3NntpRequester(String host)
        throws IOException {
        open(host);
    }

    public W3NntpRequester(String host, int port)
        throws IOException {
        open(host, port);
    }

    public W3NntpRequester(String host, int port, InetAddress localAddress, int localPort)
        throws IOException {
        open(host, port, localAddress, localPort);
    }

    public Object clone() {
        W3NntpRequester nntpRequester = (W3NntpRequester)copy(new W3NntpRequester());
        nntpRequester.host = this.host;
        nntpRequester.port = this.port;
        return nntpRequester;
    }

    public void open(String host, int port, InetAddress localAddress, int localPort)
        throws IOException {
        if (port == -1) port = NNTP_PORT;
        this.host = host;
        this.port = port;
        this.localAddress = localAddress;
        this.localPort = localPort;
        super.open(host, port, W3Socket.NORMAL, localAddress, localPort);
        if (readResponse() >= 300) throw new IOException("Welcome message");
    }

    public void open(String host, int port)
        throws IOException {
        open(host, port, null, 0);
    }

    public void open(String host)
        throws IOException {
        open(host, NNTP_PORT);
    }

    public void close()
        throws IOException {
        if (isOpen())
            try {
                super.send("QUIT\r\n");
            } finally {
                super.close();
            }
    }

    InputStream send(String cmd, int reply, boolean retry, boolean stream, HeaderList headerList)
        throws IOException {
        int response = 503;
        for (int tries = 3; --tries >= 0;) {
            try {
                super.send(cmd + "\r\n");
                response = readResponse();
                if (response < 500) break;
            } catch (IOException ex) {
                if (!retry) throw ex;
            }
            if (!retry) break;
            try {
                close();
            } catch (IOException ex) {}
            open(host, port, localAddress, localPort);
        }
        if (response != reply) throw new IOException(cmd + ": " + response + " " + getResponse());
        if (!stream && headerList == null) return null;
        InputStream source = Support.getDotInputStream(input);
        if (headerList != null) {
            try {
                headerList.setHeaders(source);
            } catch (ParseException ex) {
                throw new IOException(ex.toString());
            }
            if (!stream) while (source.read() != -1);
        }
        return stream ? source : null;
    }

    public W3Newsgroup getGroup(String name)
        throws IOException {
        send("GROUP " + name, 211, true, false, null);
        StringTokenizer tokens = new StringTokenizer(getResponse());
        return new W3Newsgroup(name,
                               Integer.parseInt(tokens.nextToken()),
                               Integer.parseInt(tokens.nextToken()),
                               Integer.parseInt(tokens.nextToken()));
    }

    public void setGroup(String name)
        throws IOException {
        send("GROUP " + name, 211, true, false, null);
    }

    public InputStream getArticle(HeaderList headerList)
        throws IOException {
        return send("ARTICLE", 220, true, true, headerList);
    }

    public InputStream getArticle(int index, HeaderList headerList)
        throws IOException {
        return send("ARTICLE " + index, 220, true, true, headerList);
    }

    public InputStream getArticle(String id, HeaderList headerList)
        throws IOException {
        if (id.charAt(0) != '<') id = "<" + id + ">";
        return send("ARTICLE " + id, 220, true, true, headerList);
    }

    public void getHeader(HeaderList headerList)
        throws IOException {
        send("HEAD", 221, true, false, headerList);
    }

    public void getHeader(int index, HeaderList headerList)
        throws IOException {
        send("HEAD " + index, 221, true, false, headerList);
    }

    public void getHeader(String id, HeaderList headerList)
        throws IOException {
        if (id.charAt(0) != '<') id = "<" + id + ">";
        send("HEAD " + id + " ", 221, true, false, headerList);
    }

    public InputStream getBody()
        throws IOException {
        return send("BODY", 222, true, true, null);
    }

    public InputStream getBody(int index)
        throws IOException {
        return send("BODY " + index, 222, true, true, null);
    }

    public InputStream getBody(String id)
        throws IOException {
        if (id.charAt(0) != '<') id = "<" + id + ">";
        return send("BODY " + id + " ", 222, true, true, null);
    }

    public void setStatus(int n)
        throws IOException {
        send("STAT " + n, 223, true, false, null);
    }

    public void getLast()
        throws IOException {
        send("LAST", 223, true, false, null);
    }

    public void getNext()
        throws IOException {
        send("NEXT", 223, true, false, null);
    }

    public InputStream getList()
        throws IOException {
        return send("LIST", 215, true, true, null);
    }

    public InputStream getNewGroups(Date date)
        throws IOException {
        return send("NEWGROUPS " + newsDateFormat.format(date), 231, true, true, null);
    }

    public InputStream getNewNews(String newsgroups, Date date)
        throws IOException {
        return send("NEWNEWS " + newsgroups + " " + newsDateFormat.format(date), 230, true, true, null);
    }

    public OutputStream startArticle()
        throws IOException {
        send("POST", 340, true, false, null);
        return new ArticleOutputStream(Support.getDotOutputStream(output));
    }

}
