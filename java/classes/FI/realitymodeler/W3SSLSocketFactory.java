
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.util.*;
import javax.net.*;
import javax.net.ssl.*;

public class W3SSLSocketFactory extends SSLSocketFactory {
    public static W3SSLSocketFactory instance = null;

    public static synchronized void prepare() {
        if (instance != null) return;
        Support.prepare();
        instance = new W3SSLSocketFactory();
    }

    public static SocketFactory getDefault() {
        return instance;
    }

    public Socket createSocket() throws IOException {
        return new W3SSLSocket();
    }

    public Socket createSocket(String host, int port) throws IOException {
        return new W3SSLSocket(host, port);
    }

    public Socket createSocket(String host, int port, InetAddress localHost, int localPort) throws IOException {
        return new W3SSLSocket(host, port, localHost, localPort);
    }

    public Socket createSocket(InetAddress host, int port) throws IOException {
        return new W3SSLSocket(host, port);
    }

    public Socket createSocket(InetAddress address, int port, InetAddress localAddress, int localPort) throws IOException {
        return new W3SSLSocket(address, port, localAddress, localPort);
    }

    public String[] getDefaultCipherSuites() {
        return null;
    }

    public String[] getSupportedCipherSuites() {
        return null;
    }

    public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException {
        return new W3SSLSocket(socket);
    }

}
