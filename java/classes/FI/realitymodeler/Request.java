
package FI.realitymodeler;

import java.net.*;

/** General socket server request. Handles coming requests in socket variable.
    Override run-method with service specific handler. Close the socket in the end. */
public abstract class Request extends PoolThread {
    static volatile long number = 0;

    public Socket socket = null;

    /** Request constructor
        @param group thread group where this request thread will belong */
    public Request(ThreadGroup group) {
        super(group, String.valueOf(Request.number++));
    }

}
