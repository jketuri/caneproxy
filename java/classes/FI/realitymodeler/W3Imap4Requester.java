
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import com.sun.mail.imap.*;
import java.io.*;
import java.util.*;
import javax.mail.*;

public class W3Imap4Requester extends W3Requester
{
    public static boolean DEBUG = false;
    public static final int IMAP4_PORT = 143;
    public static final int IMAP4_SECURE_PORT = 993;

    Store store = null;
    IMAPFolder folder = null;

    private W3Imap4Requester()
    {
    }

    public W3Imap4Requester(String host, int port, String user, String password, String category, boolean secure)
        throws IOException, NoSuchProviderException, MessagingException, LoginException
    {
        open(host, port, user, password, category, secure);
    }

    public W3Imap4Requester(String host, String user, String password, String category)
        throws IOException, NoSuchProviderException, MessagingException, LoginException
    {
        open(host, user, password, category);
    }

    public W3Imap4Requester(String host, int port, String user, String password)
        throws IOException, NoSuchProviderException, MessagingException, LoginException
    {
        open(host, port, user, password, null, false);
    }

    public W3Imap4Requester(String host, String user, String password)
        throws IOException, NoSuchProviderException, MessagingException, LoginException
    {
        open(host, user, password, null);
    }

    public Object clone()
    {
        W3Imap4Requester imap4Requester = new W3Imap4Requester();
        imap4Requester.store = store;
        imap4Requester.folder = folder;
        return imap4Requester;
    }

    public void open(String host, int port, String user, String password, String category, boolean secure)
        throws IOException, NoSuchProviderException, MessagingException, LoginException
    {
        if (store != null) close();
        if (port == -1) port = secure ? IMAP4_SECURE_PORT : IMAP4_PORT;
        this.host = host;
        this.port = port;
        Properties properties = new Properties();
        Session session = Session.getInstance(properties,  null);
        if (DEBUG) session.setDebug(true);
        store = session.getStore("imap" + (secure ? "s" : ""));
        try {
            if (!store.isConnected())
                store.connect(host, port, user, password);
        }
        catch (AuthenticationFailedException ex) {
            throw new LoginException(ex.getMessage(), makeRealm());
        }
        folder = (IMAPFolder)store.getFolder(category != null && !(category = category.trim()).equals("") ? category : "INBOX");
        if (!folder.exists()) folder.create(folder.HOLDS_FOLDERS | folder.HOLDS_MESSAGES);
        folder.open(Folder.READ_WRITE);
    }

    public void open(String host, String user, String password, String category)
        throws IOException, NoSuchProviderException, MessagingException, LoginException
    {
        open(host, -1, user, password, category, false);
    }

    public void close()
        throws IOException
    {
        try {
            if (folder != null) {
                folder.close(true);
                folder = null;
            }
        }
        catch (MessagingException ex) {
            throw new IOException(Support.stackTrace(ex));
        }
        finally {
            try {
                if (store != null) {
                    store.close();
                    store = null;
                }
            }
            catch (MessagingException ex) {
                throw new IOException(Support.stackTrace(ex));
            }
        }
    }

}
