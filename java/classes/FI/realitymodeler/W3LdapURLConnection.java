
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;
import javax.naming.*;
import javax.naming.directory.*;
import javax.naming.ldap.*;
import javax.servlet.*;

class LdapPipingThread extends CacheFillingThread {
    EntityMemory entity = null;
    NamingEnumeration searchResultEnum;
    OutputStream entityOut = null;
    String dnPath = null, from = null, path = null, query = null, target = null;
    boolean mobile = false, servlet = false;

    LdapPipingThread(W3LdapURLConnection uc) {
        super(uc);
    }

    public void run() {
        try {
            OutputStream htmlOut = Support.getHtmlOutputStream(out, from != null ? uc.servletPathTable : null, uc.charsetName);
            writeBytes("<html><head><title>" + dnPath + "</title></head><body>\n");
            while (searchResultEnum.hasMoreElements()) {
                SearchResult searchResult;
                try {
                    searchResult = (SearchResult)searchResultEnum.next();
                } catch (NoSuchElementException ex) {
                    break;
                }
                Attributes attributes = searchResult.getAttributes();
                String name = searchResult.getName();
                try {
                    writeBytes("<a href=\"" + path + Support.encodePath(searchResult.isRelative() ? name : new URL(Support.unquoteString(name)).getFile().substring(1)) + "?" + query + "\"><h3>" + Support.htmlString(name) + "</h3></a>\n");
                } catch (MalformedURLException ex) {
                    writeBytes("<h3>" + Support.htmlString(name) + "</h3>\n");
                }
                writeBytes("<table>\n");
                NamingEnumeration attributeEnum = searchResult.getAttributes().getAll();
                try {
                    while (attributeEnum.hasMore()) {
                        Attribute attribute = (Attribute)attributeEnum.next();
                        String id = attribute.getID().trim();
                        NamingEnumeration valueEnum = attribute.getAll();
                        try {
                            for (boolean started = false; valueEnum.hasMore();) {
                                Object value = valueEnum.next();
                                if (!(value instanceof String) || (value = ((String)value).trim()).equals("")) continue;
                                writeBytes("<tr><th>" + (started ? "" : id + ":") + "</th><td>");
                                if (!id.equals("")) {
                                    out.flush();
                                    if (id.equals("mail") && !((String)value).startsWith("mailto:"))
                                        Support.writeBytes(htmlOut, "mailto:" + value, uc.charsetName);
                                    else Support.writeBytes(htmlOut, (String)value, uc.charsetName);
                                    htmlOut.write('\0');
                                    htmlOut.flush();
                                } else writeBytes((String)value);
                                writeBytes("</td></tr>\n");
                                started = true;
                            }
                        } finally {
                            valueEnum.close();
                        }
                    }
                } finally {
                    attributeEnum.close();
                }
                writeBytes("</table><hr>\n");
            }
            writeBytes("</body></html>");
            out.close();
            if (entity != null) entity.setReadOnly();
        } catch (Exception ex) {
            try {
                if (in != null)
                    synchronized (in) {
                        in.close();
                        in.notifyAll();
                    }
            } catch (IOException ex1) {} finally {
                if (entity != null)
                    try {
                        entityOut.close();
                        entity.delete("");
                    } catch (IOException ex1) {}
                uc.log(getClass().getName(), ex);
            }
        } finally {
            try {
                searchResultEnum.close();
            } catch (NamingException ex) {}
        }
    }

}

/** Implements URL connection to Lightweight Directory Access Servers. URL must be of form:<br>
    ldap://host[:port][/dn[?[attributes][?[scope][?[filter][?extensions]]]]]<br>
    Refer to rfc2255.
*/
public class W3LdapURLConnection extends W3URLConnection {
    public static final int LDAP_PORT = 389;
    public static long cacheCleaningInterval = 24L * 60L * 60L * 1000L, cacheCleaningTime = 0L;
    public static String cacheRoot = "cache/ldap/";
    public static boolean defaultUseCaches = false, useProxy = false;
    public static String proxyHost = null;
    public static int proxyPort = 80;
    public static String ldapServerHost = null;
    public static String version = "FI.realitymodeler.W3LdapURLConnection/2000-5-10";

    InitialLdapContext ldapContext = null;
    LdapPipingThread piping = null;
    String attributeArray[] = null, from = null, host = null, scope = null, filter = null;
    long count = 0L;

    public static void cleanCache(ServletContext servletContext) {
        cacheCleaningTime = cleanCache(servletContext, cacheRoot, cacheCleaningInterval);
    }

    public W3LdapURLConnection(URL url) {
        super(url);
    }

    public void checkCacheCleaning() {
        if (cacheCleaningInterval >= 0L &&
            System.currentTimeMillis() - cacheCleaningTime > cacheCleaningInterval) cleanCache(servletContext);
    }

    public String getVersion() {
        return version;
    }

    public void open() throws IOException {
        if (useProxy) {
            if (requester != null) return;
            proxyConnect(proxyHost, proxyPort);
            return;
        }
        if (ldapContext != null) return;
        attributeArray = null;
        scope = "";
        filter = "";
        String bindname = null;
        boolean criticalBindname = false;
        if (query != null)
            for (;;) {
                StringTokenizer st = new StringTokenizer(query, "?", true);
                if (!st.hasMoreTokens()) break;
                String s = st.nextToken();
                if (!s.equals("?")) {
                    StringTokenizer st1 = new StringTokenizer(s, ",");
                    attributeArray = new String[st1.countTokens()];
                    for (int i = 0; i < attributeArray.length; i++) attributeArray[i] = java.net.URLDecoder.decode(st1.nextToken().trim(), charsetName);
                    if (attributeArray.length == 0) attributeArray = null;
                    if (!st.hasMoreTokens()) break;
                    st.nextToken();
                }
                if (!st.hasMoreTokens()) break;
                s = st.nextToken();
                if (!s.equals("?")) {
                    scope = java.net.URLDecoder.decode(s, charsetName).toLowerCase().trim();
                    if (!st.hasMoreTokens()) break;
                    st.nextToken();
                }
                if (!st.hasMoreTokens()) break;
                s = st.nextToken();
                if (!s.equals("?")) {
                    filter = java.net.URLDecoder.decode(s, charsetName).trim();
                    if (!st.hasMoreTokens()) break;
                    st.nextToken();
                }
                if (!st.hasMoreTokens()) break;
                s = st.nextToken();
                if (!s.equals("?")) {
                    StringTokenizer st1 = new StringTokenizer(s, ",");
                    while (st1.hasMoreTokens()) {
                        StringTokenizer st2 = new StringTokenizer(st1.nextToken(), "=");
                        if (!st2.hasMoreTokens()) continue;
                        String name = java.net.URLDecoder.decode(st2.nextToken(), charsetName).toLowerCase();
                        boolean critical;
                        if (name.startsWith("!")) {
                            name = name.substring(1);
                            critical = true;
                        } else critical = false;
                        if (name.equals("bindname")) {
                            if (st2.hasMoreTokens()) bindname = java.net.URLDecoder.decode(st2.nextToken("").substring(1), charsetName).trim();
                            if ((criticalBindname = critical) && (password == null || password.equals(""))) {
                                inputDone = true;
                                in = getUnauthorizedStream("Critical bindname");
                                return;
                            }
                        } else if (critical) throw new IOException("Unknown critical extension");
                    }
                    if (!st.hasMoreTokens()) break;
                    st.nextToken();
                }
                if (!st.hasMoreTokens()) break;
                s = st.nextToken();
                if (s.equals("?")) break;
                HashMap<String,Object> queryPars = new HashMap<String,Object>();
                parseQuery(s, queryPars);
                s = (String)queryPars.get("count");
                if (s != null) count = Long.parseLong(s);
                if ((s = (String)queryPars.get("from")) != null) from = s;
                break;
            }
        Hashtable<String,Object> dirEnv = new Hashtable<String,Object>();
        dirEnv.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        if (host != null) dirEnv.put(Context.PROVIDER_URL, "ldap://" + host + (url.getPort() != -1 ? ":" + url.getPort() : ""));
        dirEnv.put(Context.REFERRAL, "follow");
        if (username == null || username.equals("")) username = bindname;
        if (username != null && !username.equals("")) {
            dirEnv.put(Context.SECURITY_PRINCIPAL, username);
            if (password != null) dirEnv.put(Context.SECURITY_CREDENTIALS, password);
            if (System.getProperty(Context.SECURITY_AUTHENTICATION) == null) dirEnv.put(Context.SECURITY_AUTHENTICATION, "simple");
        }
        for (;;)
            try {
                ldapContext = new InitialLdapContext(dirEnv, new Control[0]);
                return;
            } catch (NamingException ex) {
                if (dirEnv.containsKey(Context.SECURITY_PRINCIPAL)) {
                    if (!criticalBindname) {
                        dirEnv.put(Context.SECURITY_AUTHENTICATION, "none");
                        dirEnv.remove(Context.SECURITY_PRINCIPAL);
                        dirEnv.remove(Context.SECURITY_CREDENTIALS);
                        continue;
                    }
                }
                if (ex instanceof AuthenticationException) {
                    inputDone = true;
                    in = getUnauthorizedStream(ex.toString());
                    return;
                }
                throw new IOException(Support.stackTrace(ex));
            }
    }

    public void doConnect()
        throws IOException {
        if ((host = url.getHost()).equals("") && (host = ldapServerHost) == null) host = System.getProperty("ldap.host");
        if (host != null) host = Support.decodePath(host);
        getBasicCredentials();
        String cachePath = cacheRoot + (url.getPort() != -1 && url.getPort() != LDAP_PORT ?
                                        host + "#" + url.getPort() : host).toLowerCase() + "/";
        StringTokenizer st = new StringTokenizer(path, "/");
        int tc = st.countTokens();
        if (tc > 1 && checkCacheFile(cachePath, st.nextToken("").substring(1))) return;
        open();
    }

    public void close() {
        if (ldapContext != null)
            try {
                ldapContext.close();
            } catch (NamingException ex) {} finally {
                ldapContext = null;
            }
        super.close();
    }

    public InputStream getInputStream() throws IOException {
        if (inputDone) return in;
        if (doOutput) {
            closeStreams();
            inputDone = true;
            useCaches = false;
            if (requester == null) throw new IOException("Not connected");
            if (useProxy && mayKeepAlive) {
                in = requester.input;
                checkInput();
                return in;
            }
            mayKeepAlive = true;
            headerFields = new HeaderList();
            headerFields.append(new Header("", protocol_1_1 + " " + HTTP_NO_CONTENT));
            return in = null;
        }
        connect();
        if (inputDone) return in;
        inputDone = true;
        setRequestMethod();
        setRequestFields();
        if (useProxy) return getInput(false);
        mayKeepAlive = true;
        filling = piping = new LdapPipingThread(this);
        piping.mobile = isMobile();
        piping.query = query != null ? query : "";
        piping.from = from;
        checkCacheCleaning();
        SearchControls cons = new SearchControls(scope.equals("one") ? SearchControls.ONELEVEL_SCOPE :
                                                 scope.equals("sub") ? SearchControls.SUBTREE_SCOPE : SearchControls.OBJECT_SCOPE,
                                                 count > 0L ? count : 0L, timeout > 0 ? timeout : 0, attributeArray, false, true);
        piping.dnPath = Support.decodePath(path);
        StringBuffer sb = new StringBuffer();
        StringTokenizer st = new StringTokenizer(piping.dnPath, "/");
        while (st.hasMoreTokens()) {
            if (sb.length() > 0) sb.insert(0, ',');
            sb.insert(0, st.nextToken());
        }
        try {
            piping.searchResultEnum = ldapContext.search(sb.toString(), filter.equals("") ? "(objectClass=*)" : filter, null, cons);
        } catch (NamingException ex) {
            throw new IOException(Support.stackTrace(ex));
        }
        headerFields = new HeaderList();
        headerFields.append(new Header("", protocol_1_1 + " " + HTTP_OK + " OK"));
        headerFields.append(new Header("Content-Type", Support.htmlType));
        headerFields.append(new Header("Cache-Control", "no-cache"));
        headerFields.append(new Header("Pragma", "no-cache"));
        Support.copyHeaderList(headerFields, headerList = new HeaderList());
        if (params != null) piping.target = params.get("target");
        piping.servlet = requestRoot != null;
        piping.path = piping.servlet ? requestRoot : path.endsWith("/") ? "" : path.substring(path.lastIndexOf('/') + 1) + "/";
        piping.setup(this);
        in = new BufferedInputStream(piping.in = new PipedInputStream(piping.out = new PipedOutputStream()));
        piping.start();
        checkMessage(true);
        return in;
    }

    public OutputStream getOutputStream() throws IOException {
        doOutput = true;
        connect();
        setRequestMethod();
        if (useProxy) {
            Support.sendMessage(requester.output, requestHeaderFields);
            return requester.output;
        }
        path = Support.decodePath(path);
        return null;
    }

    public void setDefaultUseCaches(boolean useCaches) {
        W3LdapURLConnection.defaultUseCaches = useCaches;
    }

    public boolean getDefaultUseCaches() {
        return W3LdapURLConnection.defaultUseCaches;
    }

    public int getDefaultPort() {
        return LDAP_PORT;
    }

    public boolean usingProxy() {
        return useProxy;
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public int getProxyPort() {
        return proxyPort;
    }

}
