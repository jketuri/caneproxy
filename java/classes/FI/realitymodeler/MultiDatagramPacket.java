
package FI.realitymodeler;

public class MultiDatagramPacket {
    byte data[];
    int length;
    byte address[];
    int scopeId;
    int port;

    public MultiDatagramPacket(byte data[], int length) {
        this.data = data;
        this.length = length;
    }

    public MultiDatagramPacket(byte data[], int length, byte address[], int port) {
        this.data = data;
        this.length = length;
        this.address = address;
        this.port = port;
    }

    public MultiDatagramPacket(byte data[], int length, byte address[], int scopeId, int port) {
        this.data = data;
        this.length = length;
        this.address = address;
        this.scopeId = scopeId;
        this.port = port;
    }

    public synchronized byte[] getAddress() {
        return address;
    }

    public synchronized int getScopeId() {
        return scopeId;
    }

    public synchronized int getPort() {
        return port;
    }

    public synchronized byte[] getData() {
        return data;
    }

    public synchronized int getLength() {
        return length;
    }

    public synchronized void setAddress(byte address[]) {
        this.address = address;
    }

    public synchronized void setScopeId(int scopeId) {
        this.scopeId = scopeId;
    }

    public synchronized void setPort(int port) {
        this.port = port;
    }

    public synchronized void setData(byte data[]) {
        this.data = data;
    }

    public synchronized void setLength(int length) {
        this.length = length;
    }

}
