
package FI.realitymodeler.image;

import FI.realitymodeler.common.*;
import java.awt.*;
import java.awt.color.*;
import java.awt.image.*;
import java.io.*;
import java.util.*;

/** Class which represents group of TIFF images. */
public class GroupImage extends TiffImage implements ImageProducer, Runnable {
    Hashtable<ImageConsumer,Boolean> ics = new Hashtable<ImageConsumer,Boolean>();

    public Object getProperty(String name, ImageObserver observer) {
        return UndefinedProperty;
    }

    public Object getProperty(String name) {
        return null;
    }

    public BufferedImage getSubimage(int x, int y, int w, int h) {
        if (DEBUG) System.out.println("getSubimage(" + x + "," + y + "," + w + "," + h + "),xOffset="+sampleModel.xOffset+",yOffset="+sampleModel.yOffset);
        try {
            GroupImage groupImage = new GroupImage(sampleModel.xOffset + x, sampleModel.yOffset + y, w, h, ((GroupSampleModel)sampleModel).images, ((GroupSampleModel)sampleModel).rectangles);
            groupImage.ics = ics;
            return groupImage;
        } catch (IOException ex) {
            throw new ImagingOpException(Support.stackTrace(ex));
        }
    }

    public String[] getPropertyNames() {
        return null;
    }

    public int getNumXTiles() {
        return 1;
    }

    public int getNumYTiles() {
        return 1;
    }

    public int getMinTileX() {
        return 0;
    }

    public int getMinTileY() {
        return 0;
    }

    public int getTileWidth() {
        return 0;
    }

    public int getTileHeight() {
        return 0;
    }

    public int getTileGridXOffset() {
        return 0;
    }

    public int getTileGridYOffset() {
        return 0;
    }

    public Raster getTile(int tileX, int tileY) {
        if (tileX != 0 || tileY != 0) throw new ArrayIndexOutOfBoundsException();
        return getData();
    }

    public Raster getData(Rectangle rect) {
        if (DEBUG) System.out.println("getData(" + rect + ")");
        try {
            GroupSampleModel sampleModel1 = new GroupSampleModel(sampleModel.xOffset + rect.x, sampleModel.yOffset + rect.y, rect.width, rect.height, ((GroupSampleModel)sampleModel).images, ((GroupSampleModel)sampleModel).rectangles);
            return Raster.createRaster(sampleModel1, new TiffDataBuffer(sampleModel1), null);
        } catch (IOException ex) {
            throw new ImagingOpException(Support.stackTrace(ex));
        }
    }

    public void addConsumer(ImageConsumer ic) {
        ics.put(ic, Boolean.TRUE);
    }

    public boolean isConsumer(ImageConsumer ic) {
        return ics.containsKey(ic);
    }

    public void removeConsumer(ImageConsumer ic) {
        ics.remove(ic);
    }

    public void requestTopDownLeftRightResend(ImageConsumer ic) {
    }

    void error() {
        ImageConsumer ic;
        Enumeration icElements = ics.keys();
        while (icElements.hasMoreElements()) {
            ic = (ImageConsumer)icElements.nextElement();
            ic.imageComplete(ImageConsumer.IMAGEERROR);
        }
    }

    public void setPixels(int x, int y, int w, int h, ColorModel model, int pixels[], int off, int scansize) {
        Enumeration icElements = ics.keys();
        while (icElements.hasMoreElements()) {
            ImageConsumer ic = (ImageConsumer)icElements.nextElement();
            if (model instanceof DirectColorModel) {
                int pixel[] = new int[1];
                int m = scansize - w, n = off, x1 = x + w, y1 = y + h;
                for (int y0 = y; y0 < y1; y0++)
                    for (int x0 = x; x0 < x1; x0++) {
                        pixel[0] = pixels[n++];
                        ic.setPixels(x0, y0, 1, 1, model, pixel, 0, scansize);
                        n += m;
                    }
            } else ic.setPixels(x, y, w, h, model, pixels, off, scansize);
        }
    }

    public void produce()
        throws IOException {
        if (ics.isEmpty()) return;
        int width = getWidth(), height = getHeight();
        ColorModel colorModel = getColorModel();
        Enumeration icElements = ics.keys();
        while (icElements.hasMoreElements()) {
            ImageConsumer ic = (ImageConsumer)icElements.nextElement();
            ic.setDimensions(width, height);
            ic.setColorModel(colorModel);
            ic.setHints(ImageConsumer.SINGLEPASS | ImageConsumer.SINGLEFRAME);
        }
        int pixels[] = new int[width];
        for (int y = 0; y < height; y++) {
            sampleModel.getDataElements(0, y, width, 1, pixels, null);
            setPixels(0, y, width, 1, colorModel, pixels, 0, width);
        }
        icElements = ics.keys();
        while (icElements.hasMoreElements()) {
            ImageConsumer ic = (ImageConsumer)icElements.nextElement();
            ic.imageComplete(ImageConsumer.STATICIMAGEDONE);
        }
    }

    public void run() {
        try {
            produce();
        } catch (Exception ex) {
            ex.printStackTrace();
            error();
        }
    }

    public void startProduction(ImageConsumer ic) {
        addConsumer(ic);
        new Thread(this).start();
    }

    protected GroupImage(int x, int y, int width, int height, TiffImage images[], Rectangle rectangles[])
        throws IOException {
        super(images[0].getColorModel(), new GroupSampleModel(x, y, width, height, images, rectangles), null);
        sampleModel = (GroupSampleModel)getSuperRaster().getSampleModel();
    }

    public GroupImage(int width, int height, TiffImage images[], Rectangle rectangles[])
        throws IOException {
        this(0, 0, width, height, images, rectangles);
    }

    public static void main(String argv[])
        throws Exception {
    }

}
