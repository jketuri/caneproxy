
package FI.realitymodeler.image;

import java.awt.image.*;
import java.util.*;

public class TiffHeader {
    public int magic = 0;
    public int imagewidth = -1, imagelength = -1, imagedepth = 1;
    public int bitspersample = 1;
    public int sampleformat = TiffImage.SAMPLEFORMAT_VOID;
    public Decompression decompression = null;
    public int photometric = -1;
    public int threshholding = TiffImage.THRESHHOLD_BILEVEL;
    public int fillorder = TiffImage.FILLORDER_MSB2LSB;
    public int orientation = TiffImage.ORIENTATION_TOPLEFT;
    public int samplesperpixel = 1;
    public int predictor = 1;
    public int rowsperstrip = -1;
    public int planarconfig = TiffImage.PLANARCONFIG_CONTIG;
    public int colormap[] = null;
    public int extrasamples = 0, sampleinfo[] = null;
    public int stripsperimage = -1;
    public int stripoffset[] = null;
    public int stripbytecount[] = null;
    public float ycbcrcoeffs[] = null;
    public int ycbcrsubsampling[] = {2, 2};
    public int ycbcrpositioning = TiffImage.YCBCRPOSITION_CENTERED;
    public int jpegproc = -1;
    public int jpegrestartinterval = -1;
    public short qtab[][] = null;
    public short dctab[][] = null;
    public short actab[][] = null;
    public float whitepoint[] = null;
    public float primarychromas[] = null;
    public float refblackwhite[] = null;
    public int transferfunction[] = null;
    public int inkset = TiffImage.INKSET_CMYK;
    public int dotrange[] = null;
    public int tilewidth = -1;
    public int tilelength = -1;
    public int tiledepth = 1;
    public int stride;
    public int rowsize;
    public int size = 0;
    public boolean istiled = false;
    public Hashtable<ImageConsumer,Boolean> ics = new Hashtable<ImageConsumer,Boolean>();
    public Hashtable<String,Object> properties = new Hashtable<String,Object>();
    public byte red[] = null;
    public byte green[] = null;
    public byte blue[] = null;
    public int bytespersample;
    public int bytesperpixel;
    public int offset1, offset2, mask;
    public int samplesperbyte;
    public ColorModel colorModel = null;
    public String fileName = null;
    public WeakHashMap<Number,int[]> pixelsCache = new WeakHashMap<Number,int[]>();

    public TiffHeader(String fileName) {
        this.fileName = fileName;
    }

}
