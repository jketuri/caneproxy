
package FI.realitymodeler.image;

import java.awt.*;
import java.awt.image.*;

public class TiffDataBuffer extends DataBuffer {
    TiffSampleModel sampleModel;

    public TiffDataBuffer(int width, int height) {
        super(TYPE_INT, height * width);
    }

    public TiffDataBuffer(TiffSampleModel sampleModel) {
        this(sampleModel.getWidth(), sampleModel.getHeight());
        this.sampleModel = sampleModel;
    }

    public final int getElem(int i) {
        return ((int[])sampleModel.getDataElements(i % sampleModel.getWidth(), i / sampleModel.getWidth(), null, null))[0];
    }

    public final int getElem(int bank, int i) {
        return getElem(i);
    }

    public final void setElem(int bank, int i, int val) {
    }

}
