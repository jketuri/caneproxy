
package FI.realitymodeler.image;

import java.awt.image.*;

public class TiffColorModel extends DirectColorModel {

    public TiffColorModel() {
        super(24, 0xff0000, 0xff00, 0xff);
    }

    public final boolean isCompatibleRaster(Raster raster) {
        return true;
    }

    public final boolean isCompatibleSampleModel(SampleModel sm) {
        return sm instanceof TiffSampleModel;
    }

}