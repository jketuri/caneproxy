
package FI.realitymodeler.image;

import java.io.*;

class Code {
    int nextx;
    int length;
    byte value;
    byte firstchar;
}

public class LZWDecompression extends Decompression {
    static final int BITS_MIN = 9;
    static final int BITS_MAX = 12;
    static final int CODE_CLEAR = 256;
    static final int CODE_EOI = 257;
    static final int CODE_FIRST = 258;
    static final int CODE_MAX = (1 << BITS_MAX) - 1;
    static final int CSIZE = CODE_MAX + 1024;

    int dx;
    int nbits;
    int nextdata;
    int nextbits;
    int nbitsmask;
    int code;
    Code codetab[];

    public LZWDecompression() {
        codetab = new Code[CSIZE];
        for (code = 0; code < CSIZE; code++) codetab[code] = new Code();
        for (code = 0; code < 256; code++) {
            codetab[code].value = (byte)code;
            codetab[code].firstchar = (byte)code;
            codetab[code].length = 1;
            codetab[code].nextx = -1;
        }
    }

    final void nextCode(RandomAccessFile raf)
        throws IOException {
        nextdata = nextdata << 8 | raf.read() & 0xff;
        nextbits += 8;
        if (nextbits < nbits) {
            nextdata = nextdata << 8 | raf.read() & 0xff;
            nextbits += 8;
        }
        code = nextdata >> nextbits - nbits & nbitsmask;
        nextbits -= nbits;
    }

    public int decode(RandomAccessFile raf, byte buf[])
        throws IOException {
        int len, tx, ox = 0, length = buf.length, codex, oldcodex = -1, free_entx = CODE_FIRST, maxcodex;
        nbits = BITS_MIN;
        nextbits = 0;
        nextdata = 0;
        nbitsmask = (1 << BITS_MIN) - 1;
        maxcodex = nbitsmask - 1;
        dx = 0;
        while (length > 0) {
            nextCode(raf);
            if (code == CODE_EOI) break;
            if (code == CODE_CLEAR) {
                free_entx = CODE_FIRST;
                nbits = BITS_MIN;
                nbitsmask = (1 << BITS_MIN) - 1;
                maxcodex = nbitsmask - 1;
                nextCode(raf);
                if (code == CODE_EOI) break;
                buf[ox++] = (byte)code;
                length--;
                oldcodex = code;
                continue;
            }
            codex = code;
            codetab[free_entx].nextx = oldcodex;
            codetab[free_entx].firstchar = codetab[codetab[free_entx].nextx].firstchar;
            codetab[free_entx].length = codetab[codetab[free_entx].nextx].length + 1;
            codetab[free_entx].value = codex < free_entx ? codetab[codex].firstchar :
                codetab[free_entx].firstchar;
            if (++free_entx > maxcodex) {
                if (++nbits > BITS_MAX) nbits = BITS_MAX;
                nbitsmask = (1 << nbits) - 1;
                maxcodex = nbitsmask - 1;
            }
            oldcodex = codex;
            if (code >= 256) {
                if (codetab[codex].length > length) return -1;
                len = codetab[codex].length;
                tx = ox + len;
                do buf[--tx] = codetab[codex].value;
                while ((codex = codetab[codex].nextx) != -1 && tx > ox);
                if (codex != -1) return -1;
                ox += len;
                length -= len;
            } else {
                buf[ox++] = (byte)code;
                length--;
            }
        }
        return buf.length - length;
    }

}
