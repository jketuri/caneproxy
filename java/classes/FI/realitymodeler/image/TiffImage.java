
package FI.realitymodeler.image;

import FI.realitymodeler.common.*;
import java.awt.*;
import java.awt.color.*;
import java.awt.image.*;
import java.io.*;
import java.util.*;

/** Class which represents TIFF images. */
public class TiffImage extends BufferedImage implements ImageProducer, Runnable {
    public static final boolean DEBUG = false;

    static final int TAG = 0;
    static final int TYPE = 1;
    static final int COUNT = 2;
    static final int OFFSET = 3;
    static final int READCOUNT = 1;
    static final int WRITECOUNT = 2;
    static final int BIT = 3;
    static final int CANCHANGE = 4;
    static final int NAME = 5;
    static final int VERSION = 42;
    static final int BIGENDIAN = 0x4d4d;
    static final int LITTLEENDIAN = 0x4949;
    static final int BYTE = 1;
    static final int ASCII = 2;
    static final int SHORT = 3;
    static final int LONG = 4;
    static final int RATIONAL = 5;
    static final int SBYTE = 6;
    static final int UNDEFINED = 7;
    static final int SSHORT = 8;
    static final int SLONG = 9;
    static final int SRATIONAL = 10;
    static final int FLOAT = 11;
    static final int DOUBLE = 12;
    static final int SUBFILETYPE = 254;
    static final int FILETYPE_REDUCEDIMAGE = 0x1;
    static final int FILETYPE_PAGE = 0x2;
    static final int FILETYPE_MASK = 0x4;
    static final int OSUBFILETYPE = 255;
    static final int OFILETYPE_IMAGE = 1;
    static final int OFILETYPE_REDUCEDIMAGE = 2;
    static final int OFILETYPE_PAGE = 3;
    static final int IMAGEWIDTH = 256;
    static final int IMAGELENGTH = 257;
    static final int BITSPERSAMPLE = 258;
    static final int COMPRESSION = 259;
    static final int COMPRESSION_NONE = 1;
    static final int COMPRESSION_CCITTRLE = 2;
    static final int COMPRESSION_CCITTFAX3 = 3;
    static final int COMPRESSION_CCITTFAX4 = 4;
    static final int COMPRESSION_LZW = 5;
    static final int COMPRESSION_JPEG = 6;
    static final int COMPRESSION_NEXT = 32766;
    static final int COMPRESSION_CCITTRLEW = 32771;
    static final int COMPRESSION_PACKBITS = 32773;
    static final int COMPRESSION_THUNDERSCAN = 32809;
    static final int PHOTOMETRIC = 262;
    static final int PHOTOMETRIC_MINISWHITE = 0;
    static final int PHOTOMETRIC_MINISBLACK = 1;
    static final int PHOTOMETRIC_RGB = 2;
    static final int PHOTOMETRIC_PALETTE = 3;
    static final int PHOTOMETRIC_MASK = 4;
    static final int PHOTOMETRIC_SEPARATED = 5;
    static final int PHOTOMETRIC_YCBCR = 6;
    static final int PHOTOMETRIC_CIELAB = 8;
    static final int THRESHHOLDING = 263;
    static final int THRESHHOLD_BILEVEL = 1;
    static final int THRESHHOLD_HALFTONE = 2;
    static final int THRESHHOLD_ERRORDIFFUSE = 3;
    static final int CELLWIDTH = 264;
    static final int CELLLENGTH = 265;
    static final int FILLORDER = 266;
    static final int FILLORDER_MSB2LSB = 1;
    static final int FILLORDER_LSB2MSB = 2;
    static final int DOCUMENTNAME = 269;
    static final int IMAGEDESCRIPTION = 270;
    static final int MAKE = 271;
    static final int MODEL = 272;
    static final int STRIPOFFSETS = 273;
    static final int ORIENTATION = 274;
    static final int ORIENTATION_TOPLEFT = 1;
    static final int ORIENTATION_TOPRIGHT = 2;
    static final int ORIENTATION_BOTRIGHT = 3;
    static final int ORIENTATION_BOTLEFT = 4;
    static final int ORIENTATION_LEFTTOP = 5;
    static final int ORIENTATION_RIGHTTOP = 6;
    static final int ORIENTATION_RIGHTBOT = 7;
    static final int ORIENTATION_LEFTBOT = 8;
    static final int SAMPLESPERPIXEL = 277;
    static final int ROWSPERSTRIP = 278;
    static final int STRIPBYTECOUNTS = 279;
    static final int MINSAMPLEVALUE = 280;
    static final int MAXSAMPLEVALUE = 281;
    static final int XRESOLUTION = 282;
    static final int YRESOLUTION = 283;
    static final int PLANARCONFIG = 284;
    static final int PLANARCONFIG_CONTIG = 1;
    static final int PLANARCONFIG_SEPARATE = 2;
    static final int PAGENAME = 285;
    static final int XPOSITION = 286;
    static final int YPOSITION = 287;
    static final int FREEOFFSETS = 288;
    static final int FREEBYTECOUNTS = 289;
    static final int GRAYRESPONSEUNIT = 290;
    static final int GRAYRESPONSEUNIT_10S = 1;
    static final int GRAYRESPONSEUNIT_100S = 2;
    static final int GRAYRESPONSEUNIT_1000S = 3;
    static final int GRAYRESPONSEUNIT_10000S = 4;
    static final int GRAYRESPONSEUNIT_100000S = 5;
    static final int GRAYRESPONSECURVE = 291;
    static final int GROUP3OPTIONS = 292;
    static final int GROUP3OPT_2DENCODING = 0x1;
    static final int GROUP3OPT_UNCOMPRESSED = 0x2;
    static final int GROUP3OPT_FILLBITS = 0x4;
    static final int GROUP4OPTIONS = 293;
    static final int GROUP4OPT_UNCOMPRESSED = 0x2;
    static final int RESOLUTIONUNIT = 296;
    static final int RESUNIT_NONE = 1;
    static final int RESUNIT_INCH = 2;
    static final int RESUNIT_CENTIMETER = 3;
    static final int PAGENUMBER = 297;
    static final int COLORRESPONSEUNIT = 300;
    static final int COLORRESPONSEUNIT_10S = 1;
    static final int COLORRESPONSEUNIT_100S = 2;
    static final int COLORRESPONSEUNIT_1000S = 3;
    static final int COLORRESPONSEUNIT_10000S = 4;
    static final int COLORRESPONSEUNIT_100000S = 5;
    static final int TRANSFERFUNCTION = 301;
    static final int SOFTWARE = 305;
    static final int DATETIME = 306;
    static final int ARTIST = 315;
    static final int HOSTCOMPUTER = 316;
    static final int PREDICTOR = 317;
    static final int WHITEPOINT = 318;
    static final int PRIMARYCHROMATICITIES = 319;
    static final int COLORMAP = 320;
    static final int HALFTONEHINTS = 321;
    static final int TILEWIDTH = 322;
    static final int TILELENGTH = 323;
    static final int TILEOFFSETS = 324;
    static final int TILEBYTECOUNTS = 325;
    static final int BADFAXLINES = 326;
    static final int CLEANFAXDATA = 327;
    static final int CLEANFAXDATA_CLEAN = 0;
    static final int CLEANFAXDATA_REGENERATED = 1;
    static final int CLEANFAXDATA_UNCLEAN = 2;
    static final int CONSECUTIVEBADFAXLINES = 328;
    static final int INKSET = 332;
    static final int INKSET_CMYK = 1;
    static final int INKNAMES = 333;
    static final int DOTRANGE = 336;
    static final int TARGETPRINTER = 337;
    static final int EXTRASAMPLES = 338;
    static final int EXTRASAMPLE_UNSPECIFIED = 0;
    static final int EXTRASAMPLE_ASSOCALPHA = 1;
    static final int EXTRASAMPLE_UNASSALPHA = 2;
    static final int SAMPLEFORMAT =  339;
    static final int SAMPLEFORMAT_UINT = 1;
    static final int SAMPLEFORMAT_INT = 2;
    static final int SAMPLEFORMAT_IEEEFP = 3;
    static final int SAMPLEFORMAT_VOID = 4;
    static final int SMINSAMPLEVALUE = 340;
    static final int SMAXSAMPLEVALUE = 341;
    static final int JPEGPROC = 512;
    static final int JPEGPROC_BASELINE = 1;
    static final int JPEGPROC_LOSSLESS = 14;
    static final int JPEGIFOFFSET = 513;
    static final int JPEGIFBYTECOUNT = 514;
    static final int JPEGRESTARTINTERVAL = 515;
    static final int JPEGLOSSLESSPREDICTORS = 517;
    static final int JPEGPOINTTRANSFORM = 518;
    static final int JPEGQTABLES = 519;
    static final int JPEGDCTABLES = 520;
    static final int JPEGACTABLES = 521;
    static final int YCBCRCOEFFICIENTS = 529;
    static final int YCBCRSUBSAMPLING = 530;
    static final int YCBCRPOSITIONING = 531;
    static final int YCBCRPOSITION_CENTERED = 1;
    static final int YCBCRPOSITION_COSITED = 2;
    static final int REFERENCEBLACKWHITE = 532;
    static final int REFPTS = 32953;
    static final int REGIONTACKPOINT = 32954;
    static final int REGIONWARPCORNERS = 32955;
    static final int REGIONAFFINE = 32956;
    static final int MATTEING = 32995;
    static final int DATATYPE = 32996;
    static final int IMAGEDEPTH = 32997;
    static final int TILEDEPTH = 32998;
    static int zigzag[] = {
        0, 1, 5, 6, 14, 15, 27, 28,
        2, 4, 7, 13, 16, 26, 29, 42,
        3, 8, 12, 17, 25, 30, 41, 43,
        9, 11, 18, 24, 31, 40, 44, 53,
        10, 19, 23, 32, 39, 45, 52, 54,
        20, 22, 33, 38, 46, 51, 55, 60,
        21, 34, 37, 47, 50, 56, 59, 61,
        35, 36, 48, 49, 57, 58, 62, 63};

    static TiffColorModel tiffColorModel = new TiffColorModel();

    public TiffHeader header;
    public TiffSampleModel sampleModel = null;

    public WritableRaster getSuperRaster() {
        return super.getRaster();
    }

    public WritableRaster getRaster() {
        if (DEBUG) System.out.println("getRaster()");
        return Raster.createWritableRaster(sampleModel, new TiffDataBuffer(sampleModel), null);
    }

    public WritableRaster getAlphaRaster() {
        if (DEBUG) System.out.println("getAlphaRaster()");
        return null;
    }

    public int getRGB(int x, int y) {
        return ((int[])sampleModel.getDataElements(x, y, null, null))[0];
    }

    public int[] getRGB(int startX, int startY, int w, int h, int rgbArray[], int offset, int scansize) {
        int h1;
        if (rgbArray == null) {
            scansize = w;
            h1 = h;
        } else h1 = rgbArray.length / scansize;
        return (int[])sampleModel.getDataElements(startX, startY, w, h, rgbArray, scansize, h1, offset, null);
    }

    public void setRGB(int x, int y, int rgb) {
        sampleModel.setDataElements(x, y, new int[] {rgb}, null);
    }

    public void setRGB(int startX, int startY, int w, int h, int rgbArray[], int offset, int scansize) {
        sampleModel.setDataElements(startX, startY, w, h, rgbArray, scansize, rgbArray.length / scansize, offset, null);
    }

    public int getWidth() {
        return sampleModel.getWidth();
    }

    public int getHeight() {
        return sampleModel.getHeight();
    }

    public int getWidth(ImageObserver observer) {
        return getWidth();
    }

    public int getHeight(ImageObserver observer) {
        return getHeight();
    }

    public ImageProducer getSource() {
        return this;
    }

    public Object getProperty(String name, ImageObserver observer) {
        Object value = header.properties.get(name);
        return value != null ? value : UndefinedProperty;
    }

    public Object getProperty(String name) {
        return header.properties.get(name);
    }

    public void flush() {
    }

    public BufferedImage getSubimage(int x, int y, int w, int h) {
        if (DEBUG) System.out.println("getSubimage(" + x + "," + y + "," + w + "," + h + "),xOffset="+sampleModel.xOffset+",yOffset="+sampleModel.yOffset);
        try {
            return new TiffImage(sampleModel.xOffset + x, sampleModel.yOffset + y, w, h, header);
        } catch (IOException ex) {
            throw new ImagingOpException(Support.stackTrace(ex));
        }
    }

    public boolean isAlphaPremultiplied() {
        return false;
    }

    public String[] getPropertyNames() {
        Vector<String> names = new Vector<String>();
        Enumeration keyElements = header.properties.keys();
        while (keyElements.hasMoreElements())
            names.addElement((String)keyElements.nextElement());
        String propertyNames[] = new String[names.size()];
        for (int i = 0; i < propertyNames.length; i++) propertyNames[i] = names.elementAt(i);
        return propertyNames;
    }

    public SampleModel getSampleModel() {
        return sampleModel;
    }

    public int getNumXTiles() {
        return header.imagewidth / header.tilewidth;
    }

    public int getNumYTiles() {
        return header.imagelength / header.tilelength;
    }

    public int getMinTileX() {
        return 0;
    }

    public int getMinTileY() {
        return 0;
    }

    public int getTileWidth() {
        return header.tilewidth;
    }

    public int getTileHeight() {
        return header.tilelength;
    }

    public int getTileGridXOffset() {
        return 0;
    }

    public int getTileGridYOffset() {
        return 0;
    }

    public Raster getTile(int tileX, int tileY) {
        try {
            int pixels[];
            int x = tileX * header.tilewidth, y = tileY * header.tilelength;
            if (header.planarconfig == PLANARCONFIG_SEPARATE && header.samplesperpixel - header.extrasamples > 1)
                if (header.istiled) pixels = sampleModel.getTileSeparate(x, y, new Rectangle());
                else pixels = sampleModel.getStripSeparate(x, y, new Rectangle());
            else if (header.istiled) pixels = sampleModel.getTileContig(x, y, new Rectangle());
            else pixels = sampleModel.getStripContig(x, y, new Rectangle());
            TiffSampleModel sampleModel1 = new TiffSampleModel(x, y, header.tilewidth, header.tilelength, this);
            return Raster.createRaster(sampleModel1, new TiffDataBuffer(sampleModel1), null);
        } catch (IOException ex) {
            throw new ImagingOpException(Support.stackTrace(ex));
        }
    }

    public Raster getData() {
        if (DEBUG) System.out.println("getData()");
        return Raster.createWritableRaster(sampleModel, new TiffDataBuffer(sampleModel), null);
    }

    public Raster getData(Rectangle rect) {
        if (DEBUG) System.out.println("getData(" + rect + ")");
        try {
            TiffSampleModel sampleModel1 = new TiffSampleModel(sampleModel.xOffset + rect.x, sampleModel.yOffset + rect.y, rect.width, rect.height, this);
            return Raster.createRaster(sampleModel1, new TiffDataBuffer(sampleModel1), null);
        } catch (IOException ex) {
            throw new ImagingOpException(Support.stackTrace(ex));
        }
    }

    public void addConsumer(ImageConsumer ic) {
        header.ics.put(ic, Boolean.TRUE);
    }

    public boolean isConsumer(ImageConsumer ic) {
        return header.ics.containsKey(ic);
    }

    public void removeConsumer(ImageConsumer ic) {
        header.ics.remove(ic);
    }

    public void requestTopDownLeftRightResend(ImageConsumer ic) {
    }

    void error(String msg)
        throws IOException {
        ImageConsumer ic;
        Enumeration keyElements = header.ics.keys();
        while (keyElements.hasMoreElements()) {
            ic = (ImageConsumer)keyElements.nextElement();
            ic.imageComplete(ImageConsumer.IMAGEERROR);
        }
        throw new IOException(msg);
    }

    void readPart(RandomAccessFile raf, byte buf[], int n)
        throws IOException {
        raf.seek(header.stripoffset[n]);
        if (header.decompression != null) {
            int l = header.decompression.decode(raf, buf);
            if (header.predictor == 2)
                if (header.bitspersample == 8)
                    for (int i = 0; i < l; i += header.rowsize) {
                        int m = header.rowsize + i;
                        for (int j = i + header.stride; j < m; j++) buf[j] += buf[j - header.stride];
                    }
                else
                    for (int i = 0; i < l; i += header.rowsize) {
                        int m = header.rowsize + i;
                        for (int j = i + header.stride; j < m; j += 2) {
                            int v = ((buf[j] & 0xff) << 8 | (buf[j + 1] & 0xff)) +
                                ((buf[j - header.stride] & 0xff) << 8 | (buf[j - header.stride + 1] & 0xff));
                            buf[j] = (byte)(v >> 8 & 0xff);
                            buf[j + 1] = (byte)(v & 0xff);
                        }
                    }
        } else raf.readFully(buf, 0, header.stripbytecount[n]);
    }

    final int readTile(RandomAccessFile raf, int x, int y, int z, int s, byte buf[])
        throws IOException {
        int tile;
        if (header.imagedepth == 1) z = 0;
        if (header.tilewidth != 0 && header.tilelength != 0 && header.tiledepth != 0) {
            int xpt = (header.imagewidth + header.tilewidth - 1) / header.tilewidth;
            int ypt = (header.imagelength + header.tilelength - 1) / header.tilelength;
            int zpt = (header.imagedepth + header.tiledepth - 1) / header.tiledepth;
            if (header.planarconfig == PLANARCONFIG_SEPARATE)
                tile = (xpt * ypt * zpt) * s + (xpt * ypt) * (z / header.tiledepth) + xpt * (y / header.tilelength) + x / header.tilewidth;
            else tile = (xpt * ypt) * (z / header.tiledepth) + xpt * (y / header.tilelength) + x / header.tilewidth + s;
        } else tile = 1;
        readPart(raf, buf, tile);
        return tile;
    }

    final int readStrip(RandomAccessFile raf, int r, int s, byte buf[])
        throws IOException {
        int strip = r / header.rowsperstrip;
        if (header.planarconfig == PLANARCONFIG_SEPARATE) strip += s * header.stripsperimage;
        readPart(raf, buf, strip);
        return strip;
    }

    final void putSeparate(byte buf[], int pixels[], int w, int h, int l) {
        int i = 0, k = header.imagewidth - w, n = 0;
        while (h > 0) {
            int m = n + w;
            while (n < m) {
                if (l == 0) pixels[n] = 0;
                pixels[n++] |= (buf[i] & 0xff) << l;
                i += header.bytespersample;
            }
            n += k;
            h--;
        }
    }

    final void putContig(byte buf[], int pixels[], int w, int h) {
        int i = 0, k = header.imagewidth - w, n = 0;
        if (header.photometric == PHOTOMETRIC_RGB) {
            while (h > 0) {
                int m = n + w;
                while (n < m) {
                    pixels[n++] = (buf[i] & 0xff) | (buf[i + header.offset1] & 0xff) << 8 |
                        (buf[i + header.offset2] & 0xff) << 16;
                    i += header.bytesperpixel;
                }
                n += k;
                h--;
            }
            return;
        }
        while (h > 0) {
            int m = n + w;
            while (n < m) {
                for (int t = (header.samplesperbyte - 1) * header.bitspersample; t >= 0 && n < m; t -= header.bitspersample)
                    pixels[n++] = (buf[i] & 0xff) >> t & header.mask;
                i++;
            }
            n += k;
            h--;
        }
    }

    public final int readShort(RandomAccessFile raf)
        throws IOException {
        return (raf.readByte() & 0xff) | (raf.readByte() & 0xff) << 8;
    }

    public final int readInt(RandomAccessFile raf)
        throws IOException {
        return (raf.readByte() & 0xff) | (raf.readByte() & 0xff) << 8 | (raf.readByte() & 0xff) <<  16 | (raf.readByte() & 0xff) << 24;
    }

    public void setPixels(int x, int y, int w, int h, ColorModel model, int pixels[], int off, int scansize) {
        Enumeration keyElements = header.ics.keys();
        while (keyElements.hasMoreElements()) {
            ImageConsumer ic = (ImageConsumer)keyElements.nextElement();
            if (model instanceof DirectColorModel) {
                int pixel[] = new int[1];
                int m = scansize - w, n = off, x1 = x + w, y1 = y + h;
                for (int y0 = y; y0 < y1; y0++)
                    for (int x0 = x; x0 < x1; x0++) {
                        pixel[0] = pixels[n++];
                        ic.setPixels(x0, y0, 1, 1, model, pixel, 0, scansize);
                        n += m;
                    }
            } else ic.setPixels(x, y, w, h, model, pixels, off, scansize);
        }
    }

    @SuppressWarnings("fallthrough")
    public void produce()
        throws IOException {
        RandomAccessFile raf = new RandomAccessFile(header.fileName, "r");
        if (header.magic == 0) {
            header.magic = raf.readShort();
            if (header.magic != BIGENDIAN && header.magic != LITTLEENDIAN) error("Invalid magic number");
            int version, diroff, dircount;
            if (header.magic == BIGENDIAN) {
                version = raf.readShort();
                diroff = raf.readInt();
            } else {
                version = readShort(raf);
                diroff = readInt(raf);
            }
            if (version != VERSION) error("Invalid version number");
            raf.seek(diroff);
            if (header.magic == BIGENDIAN) dircount = raf.readShort();
            else dircount = readShort(raf);
            int i, n = dircount << 2, dir[] = new int[n];
            if (header.magic == BIGENDIAN)
                for (i = 0; i < n; i += 4) {
                    dir[i + TAG] = raf.readShort();
                    dir[i + TYPE] = raf.readShort();
                    dir[i + COUNT] = raf.readInt();
                    dir[i + OFFSET] = raf.readInt();
                } else for (i = 0; i < n; i += 4) {
                    dir[i + TAG] = readShort(raf);
                    dir[i + TYPE] = readShort(raf);
                    dir[i + COUNT] = readInt(raf);
                    dir[i + OFFSET] = readInt(raf);
                }
            int tag, type, count, offset, h, j, t, u;
            Object v = null;
            for (i = 0; i < n; i += 4) {
                tag = dir[i + TAG];
                type = dir[i + TYPE];
                count = dir[i + COUNT];
                offset = dir[i + OFFSET];
                switch (type) {
                case BYTE:
                case SBYTE: {
                    short a[] = new short[count];
                    v = a;
                    if (count > 4) {
                        raf.seek(offset);
                        for (u = 0; u < count; u++) a[u] = (short)raf.readByte();
                    } else if (header.magic == BIGENDIAN)
                        switch (count) {
                        case 4: a[3] = (short)(offset & 0xff);
                        case 3: a[2] = (short)(offset >> 8 & 0xff);
                        case 2: a[1] = (short)(offset >> 16 & 0xff);
                        case 1: a[0] = (short)(offset >> 24 & 0xff); }
                    else
                        switch (count) {
                        case 4: a[3] = (short)(offset >> 24 & 0xff);
                        case 3: a[2] = (short)(offset >> 16 & 0xff);
                        case 2: a[1] = (short)(offset >> 8 & 0xff);
                        case 1: a[0] = (short)(offset & 0xff); }
                    break;
                }
                case SHORT:
                case SSHORT: {
                    int a[] = new int[count];
                    v = a;
                    if (count > 2) {
                        raf.seek(offset);
                        if (header.magic == BIGENDIAN)
                            for (u = 0; u < count; u++) a[u] = (int)raf.readShort();
                        else
                            for (u = 0; u < count; u++) a[u] = readShort(raf);
                    } else if (header.magic == BIGENDIAN)
                        switch (count) {
                        case 2: a[1] = offset & 0xffff;
                        case 1: a[0] = offset >> 16 & 0xffff; }
                    else
                        switch (count) {
                        case 2: a[1] = offset >> 16 & 0xffff;
                        case 1: a[0] = offset & 0xffff; }
                    break;
                }
                case LONG:
                case SLONG: {
                    int a[] = new int[count];
                    v = a;
                    if (count > 1) {
                        raf.seek(offset);
                        if (header.magic == BIGENDIAN)
                            for (u = 0; u < count; u++) a[u] = raf.readInt();
                        else
                            for (u = 0; u < count; u++) a[u] = readInt(raf);
                    } else a[0] = offset;
                    break;
                }
                case RATIONAL:
                case SRATIONAL: {
                    float a[] = new float[count];
                    v = a;
                    raf.seek(offset);
                    if (header.magic == BIGENDIAN)
                        for (u = 0; u < count; u++) a[u] = (float)raf.readInt() / (float)raf.readInt();
                    else
                        for (u = 0; u < count; u++) a[u] = (float)readInt(raf) / (float)readInt(raf);
                    break;
                }
                case FLOAT: {
                    float a[] = new float[count];
                    v = a;
                    if (count > 1) {
                        raf.seek(offset);
                        for (u = 0; u < count; u++) a[u] = raf.readFloat();
                    } else {
                        byte b[] = new byte[4];
                        if (header.magic == BIGENDIAN) {
                            b[3] = (byte)(offset & 0xff);
                            b[2] = (byte)(offset >> 8 & 0xff);
                            b[1] = (byte)(offset >> 16 & 0xff);
                            b[0] = (byte)(offset >> 24 & 0xff);
                        } else {
                            b[3] = (byte)(offset >> 24 & 0xff);
                            b[2] = (byte)(offset >> 16 & 0xff);
                            b[1] = (byte)(offset >> 8 & 0xff);
                            b[0] = (byte)(offset & 0xff);
                        }
                        a[0] = new DataInputStream(new ByteArrayInputStream(b)).readFloat();
                    }
                    break;
                }
                case ASCII: {
                    byte a[] = new byte[count];
                    if (count > 4) {
                        raf.seek(offset);
                        raf.readFully(a);
                    } else if (header.magic == BIGENDIAN)
                        switch (count) {
                        case 4: a[3] = (byte)(offset & 0xff);
                        case 3: a[2] = (byte)(offset >> 8 & 0xff);
                        case 2: a[1] = (byte)(offset >> 16 & 0xff);
                        case 1: a[0] = (byte)(offset >> 24 & 0xff); }
                    else
                        switch (count) {
                        case 4: a[3] = (byte)(offset >> 24 & 0xff);
                        case 3: a[2] = (byte)(offset >> 16 & 0xff);
                        case 2: a[1] = (byte)(offset >> 8 & 0xff);
                        case 1: a[0] = (byte)(offset & 0xff); }
                    v = new String(a);
                    break;
                }
                default:
                    continue; }
                switch (tag) {
                case SUBFILETYPE:
                    header.properties.put("SUBFILETYPE", new Integer(((int[])v)[0]));
                    break;
                case OSUBFILETYPE:
                    switch (((int[])v)[0]) {
                    case OFILETYPE_REDUCEDIMAGE:
                        header.properties.put("SUBFILETYPE", new Integer(FILETYPE_REDUCEDIMAGE));
                        break;
                    case OFILETYPE_PAGE:
                        header.properties.put("SUBFILETYPE", new Integer(FILETYPE_PAGE));
                        break;
                    default:
                        continue; }
                    break;
                case IMAGEWIDTH:
                    header.imagewidth = ((int[])v)[0];
                    header.properties.put("IMAGEWIDTH", new Integer(header.imagewidth));
                    sampleModel.setWidth(header.imagewidth);
                    break;
                case IMAGELENGTH:
                    header.imagelength = ((int[])v)[0];
                    header.properties.put("IMAGELENGTH", new Integer(header.imagelength));
                    sampleModel.setHeight(header.imagelength);
                    break;
                case BITSPERSAMPLE:
                    header.bitspersample = ((int[])v)[0];
                    header.properties.put("BITSPERSAMPLE", new Integer(header.bitspersample));
                    break;
                case COMPRESSION:
                    switch (((int[])v)[0] & 0xffff) {
                    case COMPRESSION_NONE:
                        header.decompression = null;
                        break;
                    case COMPRESSION_LZW:
                        header.decompression = new LZWDecompression();
                        header.properties.put("COMPRESSION", header.decompression);
                        break;
                    default:
                        error("Unsupported compression");
                    }
                    break;
                case PHOTOMETRIC:
                    header.photometric = ((int[])v)[0];
                    header.properties.put("PHOTOMETRIC", new Integer(header.photometric));
                    break;
                case THRESHHOLDING:
                    header.threshholding = ((int[])v)[0];
                    header.properties.put("THRESHHOLDING", new Integer(header.threshholding));
                    break;
                case FILLORDER:
                    t = ((int[])v)[0];
                    if (t != FILLORDER_LSB2MSB && t != FILLORDER_MSB2LSB) continue;
                    header.fillorder = t;
                    header.properties.put("FILLORDER", new Integer(header.fillorder));
                    break;
                case DOCUMENTNAME:
                    header.properties.put("DOCUMENTNAME", v);
                    break;
                case ARTIST:
                    header.properties.put("ARTIST", v);
                    break;
                case DATETIME:
                    header.properties.put("DATETIME", v);
                    break;
                case HOSTCOMPUTER:
                    header.properties.put("HOSTCOMPUTER", v);
                    break;
                case IMAGEDESCRIPTION:
                    header.properties.put("IMAGEDESCRIPTION", v);
                    break;
                case MAKE:
                    header.properties.put("MAKE", v);
                    break;
                case MODEL:
                    header.properties.put("MODEL", v);
                    break;
                case SOFTWARE:
                    header.properties.put("SOFTWARE", v);
                    break;
                case STRIPOFFSETS:
                case TILEOFFSETS:
                    header.stripoffset = (int[])v;
                    header.properties.put("STRIPOFFSETS", v);
                    header.properties.put("TILEOFFSETS", v);
                    break;
                case STRIPBYTECOUNTS:
                case TILEBYTECOUNTS:
                    header.stripbytecount = (int[])v;
                    header.properties.put("STRIPBYTECOUNTS", v);
                    header.properties.put("TILEBYTECOUNTS", v);
                    break;
                case ORIENTATION:
                    t = ((int[])v)[0];
                    if (t < ORIENTATION_TOPLEFT && ORIENTATION_LEFTBOT < t) continue;
                    header.orientation = t;
                    header.properties.put("ORIENTATION", new Integer(header.orientation));
                    break;
                case SAMPLESPERPIXEL:
                    t = ((int[])v)[0];
                    if (t == 0) continue;
                    header.samplesperpixel = t;
                    header.properties.put("SAMPLESPERPIXEL", new Integer(t));
                    break;
                case ROWSPERSTRIP:
                    t = ((int[])v)[0];
                    if (t == 0) continue;
                    header.rowsperstrip = t;
                    header.tilelength = t;
                    header.tilewidth = header.imagewidth;
                    header.properties.put("ROWSPERSTRIP", new Integer(header.rowsperstrip));
                    break;
                case MINSAMPLEVALUE:
                    header.properties.put("MINSAMPLEVALUE", new Integer(((int[])v)[0] & 0xffff));
                    break;
                case MAXSAMPLEVALUE:
                    header.properties.put("MAXSAMPLEVALUE", new Integer(((int[])v)[0] & 0xffff));
                    break;
                case XRESOLUTION:
                    header.properties.put("XRESOLUTION", new Float(((float[])v)[0]));
                    break;
                case YRESOLUTION:
                    header.properties.put("YRESOLUTION", new Float(((float[])v)[0]));
                    break;
                case PLANARCONFIG:
                    t = ((int[])v)[0];
                    if (t != PLANARCONFIG_CONTIG && t != PLANARCONFIG_SEPARATE) continue;
                    header.planarconfig = t;
                    header.properties.put("PLANARCONFIG", new Integer(t));
                    break;
                case PAGENAME:
                    header.properties.put("PAGENAME", v);
                    break;
                case XPOSITION:
                    header.properties.put("XPOSITION", new Float(((float[])v)[0]));
                    break;
                case YPOSITION:
                    header.properties.put("YPOSITION", new Float(((float[])v)[0]));
                    break;
                case GROUP3OPTIONS:
                    header.properties.put("GROUP3OPTIONS", new Integer(((int[])v)[0]));
                    break;
                case GROUP4OPTIONS:
                    header.properties.put("GROUP4OPTIONS", new Integer(((int[])v)[0]));
                    break;
                case RESOLUTIONUNIT:
                    t = ((int[])v)[0];
                    if (t < RESUNIT_NONE || RESUNIT_CENTIMETER < t) continue;
                    header.properties.put("RESOLUTIONUNIT", new Integer(t));
                    break;
                case PAGENUMBER:
                    header.properties.put("PAGENUMBER", (int[])v);
                    break;
                case HALFTONEHINTS:
                    header.properties.put("HALFTONEHINTS", (int[])v);
                    break;
                case COLORMAP:
                case TRANSFERFUNCTION:
                    t = 1 << header.bitspersample;
                    if ((tag == COLORMAP || count != t) && count != t * 3) continue;
                    if (tag == COLORMAP) header.colormap = (int[])v;
                    else header.transferfunction = (int[])v;
                    header.properties.put(tag == COLORMAP ? "COLORMAP" : "TRANSFERFUNCTION", v);
                    break;
                case PREDICTOR:
                    header.predictor = ((int[])v)[0];
                    switch (header.predictor) {
                    case 1:
                    case 2:
                        break;
                    default:
                        error("Unsupported predictor");
                    }
                    header.properties.put("PREDICTOR", new Integer(header.predictor));
                    break;
                case EXTRASAMPLES:
                    t = ((int[])v)[0];
                    if (t > header.samplesperpixel) continue;
                    for (u = 1; u <= t; u++) if (((int[])v)[u] > EXTRASAMPLE_UNASSALPHA) break;
                    if (u <= t) continue;
                    header.extrasamples = t;
                    header.sampleinfo = new int[t];
                    for (u = 1; u <= t; u++) header.sampleinfo[u-1] = ((int[])v)[u];
                    header.properties.put("EXTRASAMPLES", new Integer(t));
                    break;
                case MATTEING:
                    header.extrasamples = ((int[])v)[0];
                    if (header.extrasamples != 0) {
                        header.sampleinfo = new int[1];
                        header.sampleinfo[0] = EXTRASAMPLE_ASSOCALPHA;
                    }
                    header.properties.put("MATTEING", new Integer(header.extrasamples));
                    break;
                case BADFAXLINES:
                    header.properties.put("BADFAXLINES", new Integer(((int[])v)[0]));
                    break;
                case CLEANFAXDATA:
                    header.properties.put("CLEANFAXDATA", new Integer(((int[])v)[0]));
                    break;
                case CONSECUTIVEBADFAXLINES:
                    header.properties.put("CONSECUTIVEBADFAXLINES", new Integer(((int[])v)[0]));
                    break;
                case TILEWIDTH:
                    t = ((int[])v)[0];
                    if (t % 16 != 0) continue;
                    header.tilewidth = t;
                    header.istiled = true;
                    header.properties.put("TILEWIDTH", new Integer(t));
                    break;
                case TILELENGTH:
                    t = ((int[])v)[0];
                    if (t % 16 != 0) continue;
                    header.tilelength = t;
                    header.istiled = true;
                    header.properties.put("TILELENGTH", new Integer(t));
                    break;
                case TILEDEPTH:
                    t = ((int[])v)[0];
                    if (t == 0) continue;
                    header.tiledepth = t;
                    header.properties.put("TILEDEPTH", new Integer(t));
                    break;
                case DATATYPE:
                case SAMPLEFORMAT:
                    t = ((int[])v)[0];
                    if (tag == DATATYPE && t == 0) t = SAMPLEFORMAT_VOID;
                    if (t < SAMPLEFORMAT_UINT || SAMPLEFORMAT_VOID < t) continue;
                    header.sampleformat = t;
                    header.properties.put(tag == DATATYPE ? "DATATYPE" : "SAMPLEFORMAT", new Integer(t));
                    break;
                case IMAGEDEPTH:
                    header.imagedepth = ((int[])v)[0];
                    header.properties.put("header.imagedepth", new Integer(header.imagedepth));
                    break;
                case YCBCRCOEFFICIENTS:
                    header.ycbcrcoeffs = (float[])v;
                    header.properties.put("YCBCRCOEFFICIENTS", header.ycbcrcoeffs);
                    break;
                case YCBCRPOSITIONING:
                    header.ycbcrpositioning = ((int[])v)[0];
                    header.properties.put("YCBCRPOSITIONING", new Integer(header.ycbcrpositioning));
                    break;
                case YCBCRSUBSAMPLING:
                    header.ycbcrsubsampling = (int[])v;
                    header.properties.put("YCBCRSUBSAMPLING", header.ycbcrsubsampling);
                    break;
                case JPEGPROC:
                    header.jpegproc = ((int[])v)[0];
                    header.properties.put("JPEGPROC", new Integer(header.jpegproc));
                    break;
                case JPEGRESTARTINTERVAL:
                    header.jpegrestartinterval = ((int[])v)[0];
                    header.properties.put("JPEGRESTARTINTERVAL", new Integer(header.jpegrestartinterval));
                    break;
                case JPEGQTABLES:
                    header.qtab = new short[header.samplesperpixel][];
                    for (h = 0, j = 0; j < header.samplesperpixel; j++) {
                        header.qtab[j] = new short[64];
                        for (u = 0; u < 64; u++, h++) header.qtab[j][zigzag[u]] = ((short[])v)[h];
                    }
                    header.properties.put("JPEGQTABLES", header.qtab);
                    break;
                case JPEGDCTABLES:
                case JPEGACTABLES:
                    short tab[][] = new short[header.samplesperpixel][];
                    for (h = 0, j = 0; j < header.samplesperpixel; j++) {
                        for (t = 16, u = 0; u < 16; u++, h++) t += ((short[])v)[h];
                        tab[j] = new short[t];
                        for (h -= 16, u = 0; u < t; u++, h++) tab[j][u] = ((short[])v)[h];
                    }
                    if (tag == JPEGDCTABLES) header.dctab = tab;
                    else header.actab = tab;
                    header.properties.put(tag == JPEGDCTABLES ? "JPEGDCTABLES" : "JPEGACTABLES", tab);
                    break;
                case WHITEPOINT:
                    header.whitepoint = (float[])v;
                    header.properties.put("WHITEPOINT", header.whitepoint);
                    break;
                case PRIMARYCHROMATICITIES:
                    header.primarychromas = new float[6];
                    for (j = 0; j < 6; j++) header.primarychromas[j] = ((int[])v)[j];
                    header.properties.put("PRIMARYCHROMATICITIES", header.primarychromas);
                    break;
                case REFERENCEBLACKWHITE:
                    if (type == LONG || type == SLONG) {
                        header.refblackwhite = new float[count];
                        for (u = 0; u < count; u++) header.refblackwhite[u] = ((int[])v)[u];
                    } else header.refblackwhite = (float[])v;
                    header.properties.put("REFERENCEBLACKWHITE", header.refblackwhite);
                    break;
                case INKSET:
                    header.inkset = ((int[])v)[0];
                    header.properties.put("INKSET", new Integer(header.inkset));
                    break;
                case DOTRANGE:
                    header.dotrange = (int[])v;
                    header.properties.put("DOTRANGE", header.dotrange);
                    break;
                case INKNAMES:
                    header.properties.put("INKNAMES", v);
                    break;
                case TARGETPRINTER:
                    header.properties.put("TARGETPRINTER", v);
                    break;
                default:
                    continue; }
            }
            if (header.imagelength == -1 || header.stripoffset == null) error("No imagelength nor strips");
            if (header.bitspersample == -1) header.bitspersample = 8;
            switch (header.bitspersample) {
            case 1:
            case 2:
            case 4:
            case 8:
            case 16:
                break;
            default:
                error("Invalid number of bits per sample " + header.bitspersample);
            }
            if (header.samplesperpixel > 4) error("Invalid number of samples per pixel " + header.samplesperpixel);
            switch (header.samplesperpixel - header.extrasamples) {
            case 3:
                break;
            case 1: case 4:
                if (!(header.extrasamples == 1 && header.sampleinfo[0] == EXTRASAMPLE_ASSOCALPHA) ||
                    header.planarconfig != PLANARCONFIG_CONTIG) break;
            default:
                error("Invalid number of extra samples " + header.extrasamples);
            }
            if (header.photometric == -1)
                switch (header.samplesperpixel) {
                case 1:
                    header.photometric = PHOTOMETRIC_MINISBLACK;
                    break;
                case 3:
                case 4:
                    header.photometric = PHOTOMETRIC_RGB;
                    break;
                default:
                    error("Invalid number of samples per pixel " + header.samplesperpixel);
                }
            switch (header.photometric) {
            case PHOTOMETRIC_MINISWHITE:
            case PHOTOMETRIC_MINISBLACK:
            case PHOTOMETRIC_RGB:
            case PHOTOMETRIC_PALETTE:
            case PHOTOMETRIC_YCBCR:
                break;
            case PHOTOMETRIC_SEPARATED:
                if (header.inkset == INKSET_CMYK) break;
                break;
            default:
                error("Invalid photometric " + header.photometric);
            }
            if (header.rowsperstrip == -1) header.rowsperstrip = header.imagelength;
            if (header.predictor == 2) {
                header.stride = header.planarconfig == PLANARCONFIG_CONTIG ? header.samplesperpixel : 1;
                if (header.bitspersample == 16) header.stride <<= 1;
            }
            short map[] = null;
            float D1, D2, D3, D4;
            switch (header.photometric) {
            case PHOTOMETRIC_YCBCR:
                D1 = 2 - 2 * header.ycbcrcoeffs[0];
                D2 = D1 * header.ycbcrcoeffs[0] / header.ycbcrcoeffs[1];
                D3 = 2 - 2 * header.ycbcrcoeffs[2];
                D4 = D3 * header.ycbcrcoeffs[2] / header.ycbcrcoeffs[1];
                break;
            case PHOTOMETRIC_MINISBLACK:
            case PHOTOMETRIC_MINISWHITE:
                n = 1 << header.bitspersample;
                header.colormap = new int[n * 3];
                if (header.photometric==PHOTOMETRIC_MINISWHITE)
                    for (i = 0; i < n; i++)
                        header.colormap[i] = header.colormap[i + n] = header.colormap[i + 2 * n] = (n - i) * 255 / n;
                for (i = 0; i < n; i++)
                    header.colormap[i] = header.colormap[i + n] = header.colormap[i + 2 * n] = i * 255 / n;
                break;
            case PHOTOMETRIC_PALETTE:
                if (header.colormap == null) error("No colormap specified");
                n = (1 << header.bitspersample) * 3;
                for (i = 0; i < n; i++)
                    if (header.colormap[i] >= 256) {
                        for (i = 0; i < n; i++)
                            header.colormap[i] = (header.colormap[i] * 255) / ((1 << 16) - 1);
                        break;
                    }
                break;
            }
        }
        if (header.istiled) {
            if (header.tilewidth == -1) header.tilewidth = header.imagewidth;
            if (header.tilelength == -1) header.tilelength = header.imagelength;
            if (header.tiledepth == -1) header.tiledepth = header.imagedepth;
            header.stripsperimage = (header.tilewidth != 0 && header.tilelength != 0 && header.tiledepth != 0) ?
                ((header.imagewidth + header.tilewidth - 1) / header.tilewidth) *
                ((header.imagelength + header.tilelength - 1) / header.tilelength) *
                ((header.imagedepth + header.tiledepth - 1) / header.tiledepth) : 0;
            if (header.planarconfig == PLANARCONFIG_SEPARATE) header.stripsperimage *= header.samplesperpixel;
            if (header.planarconfig == PLANARCONFIG_CONTIG && header.photometric == PHOTOMETRIC_YCBCR) {
                int w = (header.tilewidth + header.ycbcrsubsampling[0] - 1) / header.ycbcrsubsampling[0] * header.ycbcrsubsampling[0];
                int samplingarea = header.ycbcrsubsampling[0] * header.ycbcrsubsampling[1];
                header.rowsize = (w * header.bitspersample + 7) / 8;
                w = (header.tilelength + header.ycbcrsubsampling[1] - 1) / header.ycbcrsubsampling[1] * header.ycbcrsubsampling[1];
                header.size = w * header.rowsize + 2 * (w * header.rowsize / samplingarea);
            } else {
                header.rowsize = header.bitspersample * header.tilewidth;
                if (header.planarconfig == PLANARCONFIG_CONTIG) header.rowsize *= header.samplesperpixel;
                header.rowsize = (header.rowsize + 7) / 8;
                header.size = header.tilelength * header.rowsize;
            }
            header.size *= header.tiledepth;
        } else {
            header.stripsperimage = (header.imagelength + header.rowsperstrip - 1) / header.rowsperstrip;
            header.tilewidth = header.imagewidth;
            header.tilelength = header.rowsperstrip;
            header.tiledepth = header.imagedepth;
            if (header.planarconfig == PLANARCONFIG_CONTIG && header.photometric == PHOTOMETRIC_YCBCR) {
                int w = (header.imagewidth + header.ycbcrsubsampling[0] - 1) / header.ycbcrsubsampling[0] * header.ycbcrsubsampling[0];
                int samplingarea = header.ycbcrsubsampling[0] * header.ycbcrsubsampling[1];
                header.rowsize = (w * header.bitspersample + 7) / 8;
                w = (header.rowsperstrip + header.ycbcrsubsampling[1] - 1) / header.ycbcrsubsampling[1] * header.ycbcrsubsampling[1];
                header.size = w * header.rowsize + 2 * (w * header.rowsize / samplingarea);
            } else {
                header.rowsize = header.bitspersample * header.imagewidth;
                if (header.planarconfig == PLANARCONFIG_CONTIG) header.rowsize *= header.samplesperpixel;
                header.rowsize = (header.rowsize + 7) / 8;
                header.size = header.rowsperstrip * header.rowsize;
            }
        }
        if (header.size == 0) error("Strip size not specified");
        byte buf[] = new byte[header.size];
        int col, row;
        int pixels[] = new int[header.tilelength * header.tilewidth];
        header.bytespersample = header.bitspersample / 8;
        header.bytesperpixel = header.bytespersample * header.samplesperpixel;
        header.offset1 = header.bytespersample;
        header.offset2 = 2 * header.bytespersample;
        header.samplesperbyte = 8 / header.bitspersample;
        header.mask = (1 << header.bitspersample) - 1;
        switch (header.photometric) {
        case PHOTOMETRIC_YCBCR:
        case PHOTOMETRIC_RGB:
        case PHOTOMETRIC_SEPARATED:
            header.colorModel = new DirectColorModel(24, 0xff, 0xff00, 0xff0000);
            break;
        default: {
            int n = 1 << header.bitspersample;
            header.red = new byte[n];
            header.green = new byte[n];
            header.blue = new byte[n];
            int h = n << 1;
            for (int i = 0; i < n; i++) {
                header.red[i] = (byte)header.colormap[i];
                header.green[i] = (byte)header.colormap[i + n];
                header.blue[i] = (byte)header.colormap[i + h];
            }
            header.colorModel = new IndexColorModel(header.bitspersample, 1 << header.bitspersample, header.red, header.green, header.blue);
        }
        }
        if (DEBUG) System.out.println("colorModel=" + header.colorModel);
        int y;
        switch (header.orientation) {
        case ORIENTATION_BOTRIGHT:
        case ORIENTATION_RIGHTBOT:
        case ORIENTATION_LEFTBOT:
            header.orientation = ORIENTATION_BOTLEFT;
        case ORIENTATION_BOTLEFT:
            y = header.imagelength - 1;
            break;
        case ORIENTATION_TOPRIGHT:
        case ORIENTATION_RIGHTTOP:
        case ORIENTATION_LEFTTOP:
        default:
            header.orientation = ORIENTATION_TOPLEFT;
        case ORIENTATION_TOPLEFT:
            y = 0;
            break;
        }
        if (header.ics.isEmpty()) return;
        Enumeration keyElements = header.ics.keys();
        while (keyElements.hasMoreElements()) {
            ImageConsumer ic = (ImageConsumer)keyElements.nextElement();
            ic.setDimensions(header.imagewidth, header.imagelength);
            ic.setProperties(header.properties);
            ic.setColorModel(header.colorModel);
            ic.setHints(ImageConsumer.SINGLEPASS | ImageConsumer.SINGLEFRAME);
        }
        if (header.planarconfig == PLANARCONFIG_SEPARATE && header.samplesperpixel - header.extrasamples > 1)
            if (header.istiled)
                for (row = 0; row < header.imagelength; row += header.tilelength) {
                    int h = row + header.tilelength > header.imagelength ? header.imagelength - row : header.tilelength;
                    for (col = 0; col < header.imagewidth; col += header.tilewidth) {
                        int w = col + header.tilewidth > header.imagewidth ? header.imagewidth - col : header.tilewidth;
                        for (int i = 0; i < 3; i++) {
                            readTile(raf, col, row, 0, i, buf);
                            putSeparate(buf, pixels, w, h, i << 3);
                        }
                        setPixels(col, y, w, h, header.colorModel, pixels, 0, w);
                    }
                    y += (header.orientation == ORIENTATION_TOPLEFT ? header.tilelength : -header.tilelength);
                } else for (row = 0; row < header.imagelength; row += header.rowsperstrip) {
                    int h = row + header.rowsperstrip > header.imagelength ? header.imagelength - row : header.rowsperstrip;
                    for (int i = 0; i < 3; i++) {
                        readStrip(raf, row, i, buf);
                        putSeparate(buf, pixels, header.imagewidth, h, i << 3);
                    }
                    setPixels(0, y, header.imagewidth, h, header.colorModel, pixels, 0, header.imagewidth);
                    y += header.orientation == ORIENTATION_TOPLEFT ? header.rowsperstrip : -header.rowsperstrip;
                } else if (header.istiled)
            for (row = 0; row < header.imagelength; row += header.tilelength) {
                int h = row + header.tilelength > header.imagelength ? header.imagelength - row : header.tilelength;
                for (col = 0; col < header.imagewidth; col += header.tilewidth) {
                    readTile(raf, col, row, 0, 0, buf);
                    int w = col + header.tilewidth > header.imagewidth ? header.imagewidth - col : header.tilewidth;
                    putContig(buf, pixels, w, h);
                    setPixels(col, y, w, h, header.colorModel, pixels, 0, w);
                }
                y += (header.orientation == ORIENTATION_TOPLEFT ? header.tilelength : -header.tilelength);
            } else for (row = 0; row < header.imagelength; row += header.rowsperstrip) {
                int h = row + header.rowsperstrip > header.imagelength ? header.imagelength - row : header.rowsperstrip;
                readStrip(raf, row, 0, buf);
                putContig(buf, pixels, header.imagewidth, h);
                setPixels(0, y, header.imagewidth, h, header.colorModel, pixels, 0, header.imagewidth);
                y += (header.orientation == ORIENTATION_TOPLEFT ? header.rowsperstrip : -header.rowsperstrip);
            }
        keyElements = header.ics.keys();
        while (keyElements.hasMoreElements()) {
            ImageConsumer ic = (ImageConsumer)keyElements.nextElement();
            ic.imageComplete(ImageConsumer.STATICIMAGEDONE);
        }
    }

    public void run() {
        try {
            produce();
        } catch (Exception ex) {
            if (DEBUG) ex.printStackTrace();
            try {
                error(Support.stackTrace(ex));
            } catch (IOException ex1) {}
        }
    }

    public void startProduction(ImageConsumer ic) {
        addConsumer(ic);
        new Thread(this).start();
    }

    protected TiffImage(ColorModel colorModel, TiffSampleModel sampleModel, TiffHeader header)
        throws IOException {
        super(colorModel, Raster.createWritableRaster(sampleModel, new TiffDataBuffer(sampleModel.getWidth(), sampleModel.getHeight()), null), false, null);
        this.sampleModel = (TiffSampleModel)getSuperRaster().getSampleModel();
        this.sampleModel.image = this;
        TiffDataBuffer dataBuffer = (TiffDataBuffer)getSuperRaster().getDataBuffer();
        dataBuffer.sampleModel = sampleModel;
        this.header = header;
        sampleModel.setFile();
    }

    protected TiffImage(int x, int y, int width, int height, TiffHeader header)

        throws IOException {
        this(tiffColorModel, new TiffSampleModel(x, y, width, height, null), header);
    }

    public TiffImage(String fileName, int width, int height)
        throws IOException {
        this(0, 0, width, height, new TiffHeader(fileName));
        produce();
        if (DEBUG) System.out.println("properties=" + header.properties);
    }

    public static void main(String argv[])
        throws Exception {
    }

}
