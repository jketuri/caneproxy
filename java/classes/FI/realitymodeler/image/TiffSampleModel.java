
package FI.realitymodeler.image;

import FI.realitymodeler.common.*;
import java.awt.*;
import java.awt.image.*;
import java.io.*;

public class TiffSampleModel extends SampleModel {
    TiffImage image;
    RandomAccessFile raf = null;
    int xOffset = 0, yOffset = 0;

    TiffSampleModel(int xOffset, int yOffset, int width, int height, TiffImage image)
        throws IOException {
        super(DataBuffer.TYPE_INT, width, height, 3);
        this.xOffset = xOffset;
        this.yOffset = yOffset;
        this.image = image;
        setFile();
    }

    TiffSampleModel(int width, int height, TiffImage image)
        throws IOException {
        this(0, 0, width, height, image);
    }

    void setFile()
        throws IOException {
        if (image == null || image.header == null) return;
        raf = new RandomAccessFile(image.header.fileName, "r");
    }

    public int getNumDataElements() {
        return 1;
    }

    public Object getDataElements(int x, int y, Object obj, DataBuffer data) {
        return getDataElements(x, y, 1, 1, obj, data);
    }

    int[] getTileSeparate(int x1, int y1, Rectangle rectangle1)
        throws IOException {
        Long key = new Long((long)(y1 / image.header.tilelength) * (long)image.getNumXTiles() + (long)(x1 / image.header.tilewidth));
        rectangle1.setBounds(x1 - x1 % image.header.tilewidth, y1 - y1 % image.header.tilelength, image.header.tilewidth, image.header.tilelength);
        int pixels[] = image.header.pixelsCache.get(key);
        if (pixels != null) return pixels;
        byte buf[] = new byte[image.header.size];
        pixels = new int[image.header.tilelength * image.header.tilewidth];
        int col = x1 - x1 % image.header.tilewidth,
            row = image.header.orientation == TiffImage.ORIENTATION_TOPLEFT ? rectangle1.y : image.header.imagelength - 1 - rectangle1.y,
            h1 = row + image.header.tilelength > image.header.imagelength ? image.header.imagelength - row : image.header.tilelength,
            w1 = col + image.header.tilewidth > image.header.imagewidth ? image.header.imagewidth - col : image.header.tilewidth;
        for (int i = 0; i < 3; i++) {
            image.readTile(raf, col, row, 0, i, buf);
            image.putSeparate(buf, pixels, w1, h1, i << 3);
        }
        for (int i = 0; i < pixels.length; i++) pixels[i] = image.header.colorModel.getRGB(pixels[i]);
        image.header.pixelsCache.put(key, pixels);
        return pixels;
    }

    int[] getStripSeparate(int x1, int y1, Rectangle rectangle1)
        throws IOException {
        Integer key = new Integer(y1 / image.header.rowsperstrip);
        rectangle1.setBounds(0, y1 - y1 % image.header.rowsperstrip, image.header.imagewidth, image.header.rowsperstrip);
        int pixels[] = image.header.pixelsCache.get(key);
        if (pixels != null) return pixels;
        byte buf[] = new byte[image.header.size];
        pixels = new int[image.header.tilelength * image.header.tilewidth];
        int row = image.header.orientation == TiffImage.ORIENTATION_TOPLEFT ? rectangle1.y : image.header.imagelength - 1 - rectangle1.y,
            h1 = row + image.header.rowsperstrip > image.header.imagelength ? image.header.imagelength - row : image.header.rowsperstrip;
        for (int i = 0; i < 3; i++) {
            image.readStrip(raf, row, i, buf);
            image.putSeparate(buf, pixels, image.header.imagewidth, h1, i << 3);
        }
        for (int i = 0; i < pixels.length; i++) pixels[i] = image.header.colorModel.getRGB(pixels[i]);
        image.header.pixelsCache.put(key, pixels);
        return pixels;
    }

    int[] getTileContig(int x1, int y1, Rectangle rectangle1)
        throws IOException {
        Long key = new Long((long)(y1 / image.header.tilelength) * (long)image.getNumXTiles() + (long)(x1 / image.header.tilewidth));
        rectangle1.setBounds(x1 - x1 % image.header.tilewidth, y1 - y1 % image.header.tilelength, image.header.tilewidth, image.header.tilelength);
        int pixels[] = image.header.pixelsCache.get(key);
        if (pixels != null) return pixels;
        byte buf[] = new byte[image.header.size];
        pixels = new int[image.header.tilelength * image.header.tilewidth];
        int col = x1 - x1 % image.header.tilewidth,
            row = image.header.orientation == TiffImage.ORIENTATION_TOPLEFT ? rectangle1.y : image.header.imagelength - 1 - rectangle1.y,
            h1 = row + image.header.tilelength > image.header.imagelength ? image.header.imagelength - row : image.header.tilelength,
            w1 = col + image.header.tilewidth > image.header.imagewidth ? image.header.imagewidth - col : image.header.tilewidth;
        image.readTile(raf, col, row, 0, 0, buf);
        image.putContig(buf, pixels, w1, h1);
        for (int i = 0; i < pixels.length; i++) pixels[i] = image.header.colorModel.getRGB(pixels[i]);
        image.header.pixelsCache.put(key, pixels);
        return pixels;
    }

    int[] getStripContig(int x1, int y1, Rectangle rectangle1)
        throws IOException {
        Integer key = new Integer(y1 / image.header.rowsperstrip);
        rectangle1.setBounds(0, y1 - y1 % image.header.rowsperstrip, image.header.imagewidth, image.header.rowsperstrip);
        int pixels[] = image.header.pixelsCache.get(key);
        if (pixels != null) return pixels;
        byte buf[] = new byte[image.header.size];
        pixels = new int[image.header.tilelength * image.header.tilewidth];
        int row = image.header.orientation == TiffImage.ORIENTATION_TOPLEFT ? rectangle1.y : image.header.imagelength - 1 - rectangle1.y,
            h1 = row + image.header.rowsperstrip > image.header.imagelength ? image.header.imagelength - row : image.header.rowsperstrip;
        image.readStrip(raf, row, 0, buf);
        image.putContig(buf, pixels, image.header.imagewidth, h1);
        for (int i = 0; i < pixels.length; i++) pixels[i] = image.header.colorModel.getRGB(pixels[i]);
        image.header.pixelsCache.put(key, pixels);
        return pixels;
    }

    public Object getDataElements(int x, int y, int w, int h, Object obj, DataBuffer data) {
        return getDataElements(x, y, w, h, obj, w, h, 0, data);
    }

    public Object getDataElements(int x, int y, int w, int h, Object obj, int w1, int h1, int offset, DataBuffer data) {
        //      if (TiffImage.DEBUG && x == 0) System.out.println("y=" + y);
        Rectangle rectangle = new Rectangle(xOffset + x, yOffset + y, w, h),
            rectangle0 = new Rectangle(xOffset, yOffset, image.header.imagewidth - xOffset, image.header.imagelength - yOffset).intersection(rectangle);
        int x1 = rectangle0.x, y1 = rectangle0.y, x2 = rectangle0.x + rectangle0.width - 1, y2 = rectangle0.y + rectangle0.height - 1;
        if (obj == null) obj = new int[h1 * w1];
        Rectangle rectangle1 = new Rectangle();
        try {
            if (image.header.planarconfig == TiffImage.PLANARCONFIG_SEPARATE && image.header.samplesperpixel - image.header.extrasamples > 1)
                if (image.header.istiled)
                    while (x1 <= x2 && y1 <= y2) {
                        int pixels[] = getTileSeparate(x1, y1, rectangle1);
                        Rectangle rectangle2 = rectangle.intersection(rectangle1);
                        int j = offset + (rectangle2.y - rectangle.y) * w1 + rectangle2.x - rectangle.x,
                            k = (rectangle2.y + rectangle2.height - rectangle1.y - 1) * image.header.tilewidth + rectangle2.x + rectangle2.width - rectangle1.x - 1;
                        for (int i = (rectangle2.y - rectangle1.y) * image.header.tilewidth + rectangle2.x - rectangle1.x; i <= k; i += image.header.tilewidth) {
                            int a = i, b = j, c = i + rectangle2.width - 1;
                            while (a <= c) ((int[])obj)[b++] = pixels[a++];
                            j += w1;
                        }
                        if ((x1 += rectangle1.width) >= x2) {
                            x1 = rectangle0.x;
                            y1 += rectangle1.height;
                        }
                    }
                else
                    while (x1 <= x2 && y1 <= y2) {
                        int pixels[] = getStripSeparate(x1, y1, rectangle1);
                        Rectangle rectangle2 = rectangle.intersection(rectangle1);
                        int j = offset + (rectangle2.y - rectangle.y) * w1 + rectangle2.x - rectangle.x,
                            k = (rectangle2.y + rectangle2.height - rectangle1.y - 1) * image.header.imagewidth + rectangle2.x + rectangle2.width - rectangle1.x - 1;
                        for (int i = (rectangle2.y - rectangle1.y) * image.header.imagewidth + rectangle2.x - rectangle1.x; i <= k; i += image.header.imagewidth) {
                            int a = i, b = j, c = i + rectangle2.width - 1;
                            while (a <= c) ((int[])obj)[b++] = pixels[a++];
                            j += w1;
                        }
                        if ((x1 += rectangle1.width) >= x2) {
                            x1 = rectangle0.x;
                            y1 += rectangle1.height;
                        }
                    } else if (image.header.istiled)
                while (x1 <= x2 && y1 <= y2) {
                    int pixels[] = getTileContig(x1, y1, rectangle1);
                    Rectangle rectangle2 = rectangle.intersection(rectangle1);
                    int j = offset + (rectangle2.y - rectangle.y) * w1 + rectangle2.x - rectangle.x,
                        k = (rectangle2.y + rectangle2.height - rectangle1.y - 1) * image.header.tilewidth + rectangle2.x + rectangle2.width - rectangle1.x - 1;
                    for (int i = (rectangle2.y - rectangle1.y) * image.header.tilewidth + rectangle2.x - rectangle1.x; i <= k; i += image.header.tilewidth) {
                        int a = i, b = j, c = i + rectangle2.width - 1;
                        while (a <= c) ((int[])obj)[b++] = pixels[a++];
                        j += w1;
                    }
                    if ((x1 += rectangle1.width) >= x2) {
                        x1 = rectangle0.x;
                        y1 += rectangle1.height;
                    }
                }
            else
                while (x1 <= x2 && y1 <= y2) {
                    int pixels[] = getStripContig(x1, y1, rectangle1);
                    Rectangle rectangle2 = rectangle.intersection(rectangle1);
                    int j = offset + (rectangle2.y - rectangle.y) * w1 + rectangle2.x - rectangle.x,
                        k = (rectangle2.y + rectangle2.height - rectangle1.y - 1) * image.header.imagewidth + rectangle2.x + rectangle2.width - rectangle1.x - 1;
                    for (int i = (rectangle2.y - rectangle1.y) * image.header.imagewidth + rectangle2.x - rectangle1.x; i <= k; i += image.header.imagewidth) {
                        int a = i, b = j, c = i + rectangle2.width - 1;
                        while (a <= c) ((int[])obj)[b++] = pixels[a++];
                        j += w1;
                    }
                    if ((x1 += rectangle1.width) >= x2) {
                        x1 = rectangle0.x;
                        y1 += rectangle1.height;
                    }
                }
        } catch (IOException ex) {
            throw new ImagingOpException(Support.stackTrace(ex));
        }
        return obj;
    }

    public void setDataElements(int x, int y, Object obj, DataBuffer data) {
        setDataElements(x, y, 1, 1, obj, data);
    }

    public void setDataElements(int x, int y, int w, int h, Object obj, DataBuffer data) {
        setDataElements(x, y, w, h, obj, w, h, 0, data);
    }

    public void setDataElements(int x, int y, int w, int h, Object obj, int w1, int h1, int offset, DataBuffer data) {
    }

    public int getSample(int x, int y, int b, DataBuffer data) {
        int pixels[] = new int[1];
        getSamples(x, y, 1, 1, b, pixels, data);
        return pixels[0];
    }

    public int[] getSamples(int x, int y, int w, int h, int b, int iArray[], DataBuffer data) {
        int pixels[] = (int[])getDataElements(x, y, w, h, iArray, data);
        for (int i = 0; i < pixels.length; i++)
            switch (b) {
            case 0:
                iArray[i] = image.tiffColorModel.getRed(pixels[i]);
                continue;
            case 1:
                iArray[i] = image.tiffColorModel.getGreen(pixels[i]);
                continue;
            case 2:
                iArray[i] = image.tiffColorModel.getBlue(pixels[i]);
                continue;
            case 3:
                iArray[i] = image.tiffColorModel.getAlpha(pixels[i]);
                continue;
            }
        return pixels;
    }

    public void setSample(int x, int y, int b, int s, DataBuffer data) {
        setSamples(x, y, 1, 1, b, new int[] {s}, data);
    }

    public void setSamples(int x, int y, int w, int h, int b, int iArray[], DataBuffer data) {
    }

    public SampleModel createCompatibleSampleModel(int w, int h) {
        try {
            return new TiffSampleModel(xOffset, yOffset, w, h, image);
        } catch (IOException ex) {
            throw new ImagingOpException(Support.stackTrace(ex));
        }
    }

    public SampleModel createSubsetSampleModel(int bands[]) {
        try {
            return new TiffSampleModel(xOffset, yOffset, width, height, image);
        } catch (IOException ex) {
            throw new ImagingOpException(Support.stackTrace(ex));
        }
    }

    public DataBuffer createDataBuffer() {
        if (TiffImage.DEBUG) System.out.println("createDataBuffer()");
        return new TiffDataBuffer(this);
    }

    public int[] getSampleSize() {
        return new int[] {8, 8, 8};
    }

    public int getSampleSize(int band) {
        return 8;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void finalize()
        throws IOException {
        if (raf != null) raf.close();
    }

}