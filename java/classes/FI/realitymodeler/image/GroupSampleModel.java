
package FI.realitymodeler.image;

import FI.realitymodeler.common.*;
import java.awt.*;
import java.awt.image.*;
import java.io.*;

/** Rectangles specify areas images occupy in group image. */
public class GroupSampleModel extends TiffSampleModel {
    TiffImage images[] = null;
    Rectangle rectangles[] = null;

    GroupSampleModel(int xOffset, int yOffset, int width, int height, TiffImage images[], Rectangle rectangles[])
        throws IOException {
        super(xOffset, yOffset, width, height, null);
        this.images = images;
        this.rectangles = rectangles;
    }

    public Object getDataElements(int x, int y, int w, int h, Object obj, int w1, int h1, int offset, DataBuffer data) {
        //      if (TiffImage.DEBUG && x == 0) System.out.println("GroupSampleModel y=" + y);
        Rectangle rectangle = new Rectangle(xOffset + x, yOffset + y, w, h);
        if (obj == null) obj = new int[h1 * w1];
        for (int member = 0; member < images.length; member++) {
            TiffImage memberImage = images[member];
            Rectangle memberRectangle = rectangles[member];
            Rectangle rectangle0 = memberRectangle.intersection(rectangle);
            if (rectangle0.width > 0 && rectangle0.height > 0)
                memberImage.sampleModel.getDataElements(rectangle0.x - memberRectangle.x, rectangle0.y - memberRectangle.y,
                                                        rectangle0.width, rectangle0.height, obj, w1, h1, offset + (rectangle0.y - rectangle.y) * w1 + rectangle0.x - rectangle.x, data);
        }
        return obj;
    }

    public void setDataElements(int x, int y, int w, int h, Object obj, int w1, int h1, int offset, DataBuffer data) {
    }

    public SampleModel createCompatibleSampleModel(int w, int h) {
        try {
            return new GroupSampleModel(xOffset, yOffset, w, h, images, rectangles);
        } catch (IOException ex) {
            throw new ImagingOpException(Support.stackTrace(ex));
        }
    }

    public SampleModel createSubsetSampleModel(int bands[]) {
        try {
            return new GroupSampleModel(xOffset, yOffset, width, height, images, rectangles);
        } catch (IOException ex) {
            throw new ImagingOpException(Support.stackTrace(ex));
        }
    }

}