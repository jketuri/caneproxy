
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;

public class W3ServerSocket extends ServerSocket {
    int type = 0;

    public W3ServerSocket(int port, int backlog, InetAddress bindAddr, int type) throws IOException {
        super(port, backlog, bindAddr);
        this.type = type;
    }

    public W3ServerSocket(int port) throws IOException {
        super(port);
    }

    public W3ServerSocket(int port, int backlog) throws IOException {
        super(port, backlog);
    }

    public W3ServerSocket(int port, int backlog, InetAddress bindAddr) throws IOException {
        super(port, backlog, bindAddr);
    }

    public Socket accept() throws IOException {
        W3Socket socket = new W3Socket();
        implAccept(socket);
        socket.type = type;
        return socket;
    }

}
