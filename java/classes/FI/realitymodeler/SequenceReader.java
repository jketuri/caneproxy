
package FI.realitymodeler;

import java.io.*;
import java.util.*;

public class SequenceReader extends Reader {
    Enumeration sequence;
    Reader reader = null;
    int c = 0;

    public SequenceReader(Enumeration sequence) {
        this.sequence = sequence;
    }

    public int read() throws IOException {
        if (c == -1) return -1;
        for (;;) {
            if (reader == null) {
                if (!sequence.hasMoreElements()) {
                    sequence = null;
                    return c = -1;
                }
                reader = (Reader)sequence.nextElement();
            }
            if ((c = reader.read()) == -1) {
                reader.close();
                reader = null;
                continue;
            }
            return c;
        }
    }

    public int read(char cbuf[], int off, int len) throws IOException {
        if (c == -1) return -1;
        for (int total = 0;;) {
            if (reader == null) {
                if (!sequence.hasMoreElements()) {
                    sequence = null;
                    return c = -1;
                }
                reader = (Reader)sequence.nextElement();
            }
            int n = reader.read(cbuf, off, len);
            if (n <= 0) {
                reader.close();
                reader = null;
                continue;
            }
            return n;
        }
    }

    public void close() throws IOException {
        if (sequence == null) return;
        while (sequence.hasMoreElements())
            ((Reader)sequence.nextElement()).close();
    }

}
