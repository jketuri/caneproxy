
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;
import javax.servlet.*;

/** Implements cacheable URL connection to http-servers. URL must be of form:<br>
    http://host[:port]/path
*/
public class W3HttpURLConnection extends W3URLConnection {
    public static long cacheCleaningInterval = 7L * 24L * 60L * 60L * 1000L, cacheRefreshingInterval =  48L * 60L * 60L * 1000L, cacheCleaningTime = 0L;
    public static String cacheRoot = "cache/http/";
    public static boolean defaultUseCaches = false, useProxy = false;
    public static String proxyHost = null;
    public static int proxyPort = 80;

    boolean secure = false;

    public static void cleanCache(ServletContext servletContext) {
        cacheCleaningTime = cleanCache(servletContext, cacheRoot, cacheCleaningInterval);
    }

    public W3HttpURLConnection(URL url) {
        super(url);
        secure = url.getProtocol().equalsIgnoreCase("https");
    }

    public W3HttpURLConnection(URL url, boolean secure) {
        super(url);
        this.secure = secure;
    }

    public void checkCacheCleaning() {
        if (cacheCleaningInterval >= 0L &&
            System.currentTimeMillis() - cacheCleaningTime > cacheCleaningInterval) cleanCache(servletContext);
    }

    public void open()
        throws IOException {
        if (requester != null) return;
        if (useProxy) {
            if (secure)
                for (int tries = 1; tries <= numberOfTries; tries++) {
                    proxyConnect(proxyHost, proxyPort);
                    Support.writeBytes(requester.output, "CONNECT " + url.getHost() + ":" + (url.getPort() != -1 ? url.getPort() : 443) + " " + protocol_1_0 + "\r\n\r\n", null);
                    if (check(requester.input, "HTTP/") == 0) {
                        try {
                            headerFields = new HeaderList(requester.input);
                        } catch (ParseException ex) {
                            throw new IOException(ex.toString());
                        }
                        if (getResponseCode() == 200) {
                            requester.setStreams(W3Socket.SECURE);
                            return;
                        }
                    }
                    if (tries == numberOfTries) throw new IOException("SSL Proxy connection refused");
                    disconnect();
                } else proxyConnect(proxyHost, proxyPort);
            return;
        }
        requester = new W3Requester();
        if (connectTimeout > 0) requester.setConnectTimeout(connectTimeout);
        if (timeout > 0) requester.setTimeout(timeout);
        requester.open(url.getHost(), url.getPort() != -1 ? url.getPort() :
                       secure ? 443 : 80, secure ? W3Socket.SECURE : 0, localAddress, localPort);
    }

    public void doConnect()
        throws IOException {
        if (connected) return;
        setRequestMethod();
        setRequestFields();
        if (checkCacheFile(cacheRoot, 80)) return;
        super.doConnect();
    }

    public InputStream getInputStream()
        throws IOException {
        if (inputDone) return in;
        cached = false;
        if (doOutput) {
            if (requester == null) throw new IOException("Not connected");
            in = requester.input;
            return checkInput();
        }
        return getInput(false);
    }

    public OutputStream getOutputStream()
        throws IOException {
        sink = null;
        doOutput = true;
        return getOutput();
    }

    public void setDefaultUseCaches(boolean useCaches) {
        W3HttpURLConnection.defaultUseCaches = useCaches;
    }

    public boolean getDefaultUseCaches() {
        return W3HttpURLConnection.defaultUseCaches;
    }

    public int getDefaultPort() {
        return 80;
    }

    public boolean usingProxy() {
        return useProxy;
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public int getProxyPort() {
        return proxyPort;
    }

    public void setCacheCleaningInterval(long cacheCleaningInterval) {
        W3HttpURLConnection.cacheCleaningInterval = cacheCleaningInterval;
    }

    public void setCacheRefreshingInterval(long cacheRefreshingInterval) {
        W3HttpURLConnection.cacheRefreshingInterval = cacheRefreshingInterval;
    }

    public long getCacheRefreshingInterval() {
        return cacheRefreshingInterval;
    }

}
