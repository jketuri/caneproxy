
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import java.io.*;
import java.lang.reflect.*;
import java.math.*;
import java.net.*;
import java.security.*;
import java.security.cert.*;
import java.util.*;

import javax.crypto.*;

class SSLCertificate extends X509Certificate {
    static final long serialVersionUID = 0L;

    protected long instance = 0;

    SSLCertificate(long instance) {
        this.instance = instance;
    }

    public boolean hasUnsupportedCriticalExtension() {
        return false;
    }
	
    public Set<String> getCriticalExtensionOIDs() {
        return null;
    }

    public Set<String> getNonCriticalExtensionOIDs() {
        return null;
    }

    public byte[] getExtensionValue(String oid) {
        return null;
    }

    public byte[] getEncoded() {
        return null;
    }

    public void verify(PublicKey key) {
    }

    public void verify(PublicKey key, String sigProvider) {
    }

    public String toString() {
        return getClass().getName();
    }

    public PublicKey getPublicKey() {
        return null;
    }

    public void checkValidity() {
    }

    public void checkValidity(Date date) {
    }

    public int getVersion() {
        return 0;
    }

    public BigInteger getSerialNumber() {
        return null;
    }

    public Principal getIssuerDN() {
        return null;
    }

    public Principal getSubjectDN() {
        return null;
    }

    public Date getNotBefore() {
        return null;
    }

    public Date getNotAfter() {
        return null;
    }

    public byte[] getTBSCertificate() {
        return null;
    }

    public byte[] getSignature() {
        return null;
    }

    public String getSigAlgName() {
        return null;
    }

    public String getSigAlgOID() {
        return null;
    }

    public byte[] getSigAlgParams() {
        return null;
    }

    public boolean[] getIssuerUniqueID() {
        return null;
    }

    public boolean[] getSubjectUniqueID() {
        return null;
    }

    public boolean[] getKeyUsage() {
        return null;
    }

    public int getBasicConstraints() {
        return 0;
    }

    protected void finalize() throws Throwable {
        try {
            if (instance != 0) SecureSocketsLayer.freeCertificate0(instance);
        } finally {
            super.finalize();
        }
    }

}

class CertificateStack {
    protected long instance = 0;

    CertificateStack(long instance) {
        this.instance = instance;
    }

    public void push(X509Certificate certificate) throws IOException {
        SecureSocketsLayer.certificateStackPush0(instance, ((SSLCertificate)certificate).instance);
    }

    protected void finalize() throws Throwable {
        try {
            if (instance != 0) SecureSocketsLayer.freeCertificateStack0(instance);
        } finally {
            super.finalize();
        }
    }

}

class SSLCipher extends Cipher {
    protected long instance = 0;

    SSLCipher(long instance) {
        super(null, null, null);
        this.instance = instance;
    }

}

class SSLPrivateKey implements PrivateKey {
    static final long serialVersionUID = 0L;

    protected long instance = 0;

    SSLPrivateKey(long instance) {
        this.instance = instance;
    }

    public String getAlgorithm() {
        return null;
    }

    public byte[] getEncoded() {
        return null;
    }

    public String getFormat() {
        return null;
    }

}

/** Secure Sockets Layer which can be used as socket implementation or as standalone utility with any socket. */
public class SecureSocketsLayer extends MultiSocketImpl {
    static Field implField, fdFIeld, fd0Field;
    static {
        try {
            Support.loadLibrary("FI_realitymodeler_ssl");
            implField = Socket.class.getDeclaredField("impl");
            implField.setAccessible(true);
            fdField = SocketImpl.class.getDeclaredField("fd");
            fdField.setAccessible(true);
            fd0Field = FileDescriptor.class.getDeclaredField("fd");
            fd0Field.setAccessible(true);
            initialize();
        } catch (Exception ex) {
            throw new Error(ex.toString());
        }
    }

    public static final int FILETYPE_PEM = 1;
    public static final int FILETYPE_ASN1 = 2;
    public static final int VERIFY_NONE = 0;
    public static final int VERIFY_PEER = 1;
    public static final int VERIFY_FAIL_IF_NO_PEER_CERT = 2;

    protected long instance = 0;

    private static native void initialize();

    public static native void flushSessions(int tm);
    public static native void setDefaultReadAhead(boolean yes);
    public static native void setDefaultTimeout(int secs);
    public static native void setDefaultVerify(int mode);
    public static native void loadVerifyLocations(String caFile, String caPath) throws IOException;
    public static native void setCertificateChainFile(String file) throws IOException;
    public static native void setCertificateFile(String file, int type) throws IOException;
    public static native void setRSAPrivateKeyFile(String file, int type, byte password[]) throws IOException;
    public static native long getDigestByName(String name) throws IOException;

    protected static native long getCipherByName0(String name) throws IOException;
    protected static native long getPrivateKey0(byte bytes[], byte password[]) throws IOException;
    protected static native long getCertificate0(byte bytes[], byte password[]) throws IOException;
    protected static native int freeCertificate0(long certificate) throws IOException;
    protected static native long getCertificateStack0() throws IOException;
    protected static native int certificateStackPush0(long certificateStack, long certificate) throws IOException;
    protected static native int freeCertificateStack0(long certificateStack) throws IOException;
    protected static native void pkcs7Encrypt0(long certificateStack, InputStream in, OutputStream out, byte inBuffer[], byte outBuffer[], long cipher, int flags) throws IOException;
    protected static native void pkcs7Decrypt0(InputStream in, byte inBuffer[], long key, long certificate, OutputStream out, byte outBuffer[], int flags);

    private native long construct();
    private native void destroy(long instance);
    private native void accept0(long instance) throws IOException;
    private native void clear0(long instance);
    private native void connect0(long instance) throws IOException;
    private native String getCipher0(long instance);
    private native String getCipherList0(long instance, int n);
    private native String getSharedCiphers0(long instance, byte buf[]);
    private native int getReadAhead0(long instance);
    private native int isInitFinished0(long instance);
    private native int pending0(long instance);
    private native void setCipherList0(long instance, String str) throws IOException;
    private native void setReadAhead0(long instance, boolean yes);
    private native void setSocket0(long instance, int socket);
    private native void setVerify0(long instance, int mode);
    private native void useCertificateFile0(long instance, String file, int type) throws IOException;
    private native void useRSAPrivateKeyFile0(long instance, String file, int type) throws IOException;
    private native int read0(long instance, byte buf[], int off, int len) throws IOException;
    private native void write0(long instance, byte buf[], int off, int len) throws IOException;

    public static Cipher getCipherByName(String name) throws IOException {
        return new SSLCipher(getCipherByName0(name));
    }

    public static PrivateKey getPrivateKey(byte bytes[], byte password[]) throws IOException {
        return new SSLPrivateKey(getPrivateKey0(bytes, password));
    }

    public static X509Certificate getCertificate(byte bytes[], byte password[]) throws IOException {
        return new SSLCertificate(getCertificate0(bytes, password));
    }

    public static CertificateStack getCertificateStack() throws IOException {
        return new CertificateStack(getCertificateStack0());
    }

    public static void pkcs7Encrypt(CertificateStack certificateStack, InputStream in, OutputStream out, Cipher cipher, int flags) throws IOException {
        byte inBuffer[] = new byte[Support.bufferLength], outBuffer[] = new byte[Support.bufferLength];
        pkcs7Encrypt0(certificateStack.instance, in, out, inBuffer, outBuffer, ((SSLCipher)cipher).instance, flags);
    }

    public static void pkcs7Decrypt(InputStream in, PrivateKey key, java.security.cert.Certificate certificate, OutputStream out, int flags) {
        byte inBuffer[] = new byte[Support.bufferLength], outBuffer[] = new byte[Support.bufferLength];
        pkcs7Decrypt0(in, inBuffer, ((SSLPrivateKey)key).instance, ((SSLCertificate)certificate).instance, out, outBuffer, flags);
    }

    public SecureSocketsLayer() {
        super(-1, 0, 0);
        instance = construct();
    }

    public SecureSocketsLayer(int af, int type, int protocol, int protocolInfo, int group, int flags) {
        super(af, type, protocol, protocolInfo, group, flags);
        instance = construct();
    }

    public final void accept() throws IOException {
        accept0(instance);
    }

    public final void clear() {
        clear0(instance);
    }

    public final void connect() throws IOException {
        connect0(instance);
    }

    public final String getCipher() {
        return getCipher0(instance);
    }

    public final String getCipherList(int n) {
        return getCipherList0(instance, n);
    }

    public final int getReadAhead() {
        return getReadAhead0(instance);
    }

    public final int isInitFinished() {
        return isInitFinished0(instance);
    }

    public final void setCipherList(String str) throws IOException {
        setCipherList0(instance, str);
    }

    public final void setReadAhead(boolean yes) {
        setReadAhead0(instance, yes);
    }

    public void setSocket(SocketImpl socketImpl) throws IllegalAccessException {
        FileDescriptor fd = (FileDescriptor)fdField.get(socketImpl);
        setSocket0(instance, fd0Field.getInt(fd));
    }

    public void setSocket(Socket socket) throws IllegalAccessException {
        setSocket((SocketImpl)implField.get(socket));
    }

    public final void setVerify(int mode) {
        setVerify0(instance, mode);
    }

    public final void useCertificateFile(String file, int type) throws IOException {
        useCertificateFile0(instance, new File(file).getCanonicalPath(), type);
    }

    public final void useRSAPrivateKeyFile(String file, int type) throws IOException {
        useRSAPrivateKeyFile0(instance, new File(file).getCanonicalPath(), type);
    }

    public void connect(String host, int port) throws IOException {
        super.connect(host, port);
        connect();
    }

    public void connect(InetAddress address, int port) throws IOException {
        super.connect(address, port);
        connect();
    }

    public void connect(byte addr[], int scopeId, int port) throws IOException {
        super.connect(addr, scopeId, port);
        connect();
    }

    public synchronized void accept(SocketImpl socketImpl) throws IOException {
        super.accept(socketImpl);
        accept();
    }

    public int read(byte buf[], int off, int len) throws IOException {
        return read0(instance, buf, off, len);
    }

    public void write(byte buf[], int off, int len) throws IOException {
        write0(instance, buf, off, len);
    }

    public int available() throws IOException {
        return pending0(instance);
    }

    protected void finalize() {
        try {
            if (instance != 0) destroy(instance);
        } finally {
            super.finalize();
        }
    }

    public static void main(String argv[]) {
        SecureSocketsLayer ssl = new SecureSocketsLayer();
    }

}
