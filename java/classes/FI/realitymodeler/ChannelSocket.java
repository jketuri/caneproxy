
package FI.realitymodeler;

import java.io.*;
import java.net.*;
import java.nio.*;
import java.nio.channels.*;
import java.nio.channels.spi.*;
import java.util.*;

public class ChannelSocket extends Socket {
    ChannelSocketInputStream in = null;
    ChannelSocketOutputStream out = null;
    SocketChannel socketChannel = null;
    boolean suspended = false;

    public ChannelSocket(SocketChannel socketChannel)
        throws IOException {
        this.socketChannel = socketChannel;
        in = new ChannelSocketInputStream(socketChannel);
        out = new ChannelSocketOutputStream(socketChannel);
    }

    public Socket getSocket() {
        return socketChannel.socket();
    }

    public void setSuspended(boolean suspended) {
        this.suspended = suspended;
    }

    public InputStream getInputStream() {
        return in;
    }

    public OutputStream getOutputStream() {
        return out;
    }

}

