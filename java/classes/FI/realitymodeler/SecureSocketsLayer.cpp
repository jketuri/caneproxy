
/* Interface to Secure Sockets Layer Library */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef _WIN32
#include <windows.h>
#include "openssl\err.h"
#include "openssl\evp.h"
#include "openssl\ssl.h"
#else
#include "openssl/err.h"
#include "openssl/evp.h"
#include "openssl/ssl.h"
#include "sys/param.h"
#define min MIN
#endif
#include "FI_realitymodeler_SecureSocketsLayer.h"

typedef struct
{
    char *password;
    int len;
} PASSWORD_DATA;

typedef struct
{
    JNIEnv *env;
    jobject in;
    jobject out;
    jbyteArray inBuffer;
    jbyteArray outBuffer;
    jbyte *inBuf;
} STREAM_DATA;

static jmethodID appendID, stringBufferID, toStringID, ioExceptionID, readID, writeID;
static jclass arrayIndexOutOfBoundsException, nullPointerException, ioException, runtimeException, stringBuffer;
static SSL_CTX *ctx;

static int stream_write(BIO *bio, const char *buf, int num);
static int stream_read(BIO *bio, char *buf, int size);
static int stream_puts(BIO *bio, const char *str);
static long stream_ctrl(BIO *bio, int cmd, long arg1, void *arg2);
static int stream_new(BIO *bio);
static int stream_free(BIO *data);

static BIO_METHOD stream_methods = {
    BIO_TYPE_SOCKET,
    "stream",
    stream_write,
    stream_read,
    stream_puts,
    NULL,
    stream_ctrl,
    stream_new,
    stream_free,
    NULL
};

static int stream_new(BIO *bio)
{
    memset(bio, 0, sizeof(BIO));
    return 0;
}

static int stream_free(BIO *bio)
{
    if (!bio) return 0;
    return 1;
}

static int stream_read(BIO *bio, char *buf, int size)
{
    STREAM_DATA *streamData = (STREAM_DATA *)BIO_get_app_data(bio);
    int n = streamData->env->CallIntMethod(streamData->in, readID, streamData->inBuffer, 0, size);
    memcpy(buf, streamData->inBuf, n);
    return n;
}

static int stream_write(BIO *bio, const char *buf, int num)
{
    STREAM_DATA *streamData = (STREAM_DATA *)BIO_get_app_data(bio);
    streamData->env->SetByteArrayRegion(streamData->outBuffer, 0, num, (const jbyte *)buf);
    streamData->env->CallVoidMethod(streamData->out, writeID, streamData->outBuffer, 0, num);
    if (streamData->env->ExceptionOccurred()) return 0;
    return 1;
}

static long stream_ctrl(BIO *bio, int cmd, long arg1, void *arg2)
{
    return 1;
}

static int stream_puts(BIO *bio, const char *str)
{
    return stream_write(bio, str, (int)strlen(str));
}

static void throwEx(JNIEnv *env)
{
    jobject sb = env->NewObject(stringBuffer, stringBufferID);
    if (!sb) return;
    jstring s = env->NewStringUTF("SSL error");
    if (!s) return;
    env->CallObjectMethod(sb, appendID, s);
    if (env->ExceptionOccurred()) return;
    int n;
    while ((n = ERR_get_error())) {
        if (!(s = env->NewStringUTF("\n"))) return;
        env->CallObjectMethod(sb, appendID, s);
        if (env->ExceptionOccurred()) return;
        if (!(s = env->NewStringUTF(ERR_error_string(n, NULL)))) return;
        env->CallObjectMethod(sb, appendID, s);
        if (env->ExceptionOccurred()) return;
    }
    s = (jstring)env->CallObjectMethod(sb, toStringID);
    if (env->ExceptionOccurred()) return;
    jthrowable ex = (jthrowable)env->NewObject(ioException, ioExceptionID, s);
    if (!ex) return;
    env->Throw(ex);
}

static int password_callback(char *buf, int bufSize, int verify, void *pwData)
{
    PASSWORD_DATA *passwordData = (PASSWORD_DATA *)pwData;
    int len = min(bufSize, passwordData->len);
    memcpy(buf, passwordData->password, passwordData->len);
    return len;
}

static jclass getClass(JNIEnv *env, char *name)
{
    jclass clazz;
    if ((clazz = env->FindClass(name))) clazz = (jclass)env->NewGlobalRef(clazz);
    return clazz;
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_SecureSocketsLayer_initialize(JNIEnv *env, jclass clazz)
{
    if (!(arrayIndexOutOfBoundsException = getClass(env, (char *)"java/lang/ArrayIndexOutOfBoundsException")) ||
        !(nullPointerException = getClass(env, (char *)"java/lang/NullPointerException")) ||
        !(ioException = getClass(env, (char *)"java/io/IOException")) ||
        !(runtimeException = getClass(env, (char *)"java/lang/RuntimeException")) ||
        !(stringBuffer = getClass(env, (char *)"java/lang/StringBuffer")) ||
        !(stringBufferID = env->GetMethodID(stringBuffer, "<init>", "()V")) ||
        !(appendID = env->GetMethodID(stringBuffer, "append", "(Ljava/lang/String;)Ljava/lang/StringBuffer;")) ||
        !(toStringID = env->GetMethodID(stringBuffer, "toString", "()Ljava/lang/String;")) ||
        !(ioExceptionID = env->GetMethodID(ioException, "<init>", "(Ljava/lang/String;)V")) ||
        !(clazz = getClass(env, (char *)"java/io/InputStream")) ||
        !(readID = env->GetMethodID(clazz, "read", "([BII)I")) ||
        !(clazz = getClass(env, (char *)"java/io/OutputStream")) ||
        !(writeID = env->GetMethodID(clazz, "write", "([BII)V"))) return;
    SSL_load_error_strings();
    SSLeay_add_ssl_algorithms();
    if (!(ctx = SSL_CTX_new(SSLv23_method()))) {
        env->ThrowNew(runtimeException, "Out of memory");
        return;
    }
    //SSL_CTX_set_options(ctx, SSL_OP_ALL);
    /*
    SSL_CTX_set_options(ctx, SSL_OP_MICROSOFT_SESS_ID_BUG
                        | SSL_OP_ALLOW_UNSAFE_LEGACY_RENEGOTIATION);
    */
    /*
    SSL_CTX_set_options(ctx, 
                        SSL_OP_MICROSOFT_SESS_ID_BUG
                        //SSL_OP_NETSCAPE_CHALLENGE_BUG
                        | SSL_OP_NETSCAPE_REUSE_CIPHER_CHANGE_BUG
                        | SSL_OP_SSLREF2_REUSE_CERT_TYPE_BUG
                        //| SSL_OP_MICROSOFT_BIG_SSLV3_BUFFER
                        | SSL_OP_SSLEAY_080_CLIENT_DH_BUG
                        | SSL_OP_TLS_D5_BUG
                        | SSL_OP_TLS_BLOCK_PADDING_BUG
                        | SSL_OP_DONT_INSERT_EMPTY_FRAGMENTS
                        | SSL_OP_TLS_ROLLBACK_BUG
                        | SSL_OP_SINGLE_DH_USE
                        //| SSL_OP_EPHEMERAL_RSA
                        | SSL_OP_CIPHER_SERVER_PREFERENCE
                        | SSL_OP_PKCS1_CHECK_1
                        | SSL_OP_PKCS1_CHECK_2
                        | SSL_OP_NETSCAPE_CA_DN_BUG
                        //| SSL_OP_NETSCAPE_DEMO_CIPHER_CHANGE_BUG
                        | SSL_OP_NO_SESSION_RESUMPTION_ON_RENEGOTIATION
                        //| SSL_OP_NO_TICKET
                        | SSL_OP_NO_COMPRESSION
                        | SSL_OP_ALLOW_UNSAFE_LEGACY_RENEGOTIATION);
    */
}

int checkCtx(JNIEnv *env)
{
    if (ctx) return 0;
    env->ThrowNew(ioException, "Library is not initialized");
    return 1;
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_SecureSocketsLayer_flushSessions(JNIEnv *env, jclass clazz, jint tm)
{
    if (checkCtx(env)) return;
    SSL_CTX_flush_sessions(ctx, (long)tm);
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_SecureSocketsLayer_setDefaultReadAhead(JNIEnv *env, jclass clazz, jboolean yes)
{
    if (checkCtx(env)) return;
    SSL_CTX_set_default_read_ahead(ctx, yes);
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_SecureSocketsLayer_setDefaultTimeout(JNIEnv *env, jclass clazz, jint secs)
{
    if (checkCtx(env)) return;
    SSL_CTX_set_timeout(ctx, (long)secs);
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_SecureSocketsLayer_setDefaultVerify(JNIEnv *env, jclass clazz, jint mode)
{
    if (checkCtx(env)) return;
    SSL_CTX_set_verify(ctx, mode, NULL);
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_SecureSocketsLayer_loadVerifyLocations(JNIEnv *env, jclass clazz, jstring caFile, jstring caPath)
{
    if (checkCtx(env)) return;
    const char *caFileString = NULL;
    if (caFile) {
        caFileString = env->GetStringUTFChars(caFile, NULL);
        if (!caFileString) return;
    }
    const char *caPathString = NULL;
    if (caPath) {
        caPathString = env->GetStringUTFChars(caPath, NULL);
        if (!caPathString) {
            env->ReleaseStringUTFChars(caFile, caFileString);
            return;
        }
    }
    if (SSL_CTX_load_verify_locations(ctx, (char *)caFileString, (char *)caPathString) <= 0) throwEx(env);
    if (caFile)
        env->ReleaseStringUTFChars(caFile, caFileString);
    if (caPath)
        env->ReleaseStringUTFChars(caPath, caPathString);
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_SecureSocketsLayer_setCertificateChainFile(JNIEnv *env, jclass clazz, jstring file)
{
    if (checkCtx(env)) return;
    const char *s = env->GetStringUTFChars(file, NULL);
    if (!s) return;
    if (SSL_CTX_use_certificate_chain_file(ctx, (char *)s) <= 0) throwEx(env);
    env->ReleaseStringUTFChars(file, s);
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_SecureSocketsLayer_setCertificateFile(JNIEnv *env, jclass clazz, jstring file, jint type)
{
    if (checkCtx(env)) return;
    const char *s = env->GetStringUTFChars(file, NULL);
    if (!s) return;
    if (SSL_CTX_use_certificate_file(ctx, (char *)s, type) <= 0) throwEx(env);
    env->ReleaseStringUTFChars(file, s);
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_SecureSocketsLayer_setRSAPrivateKeyFile(JNIEnv *env, jclass clazz, jstring file, jint type, jbyteArray password)
{
    if (checkCtx(env)) return;
    jbyte *pw = env->GetByteArrayElements(password, NULL);
    if (!pw) return;
    PASSWORD_DATA passwordData;
    passwordData.password = (char *)pw;
    passwordData.len = env->GetArrayLength(password);
    const char *s = env->GetStringUTFChars(file, NULL);
    if (!s) {
        env->ReleaseByteArrayElements(password, pw, JNI_ABORT);
        return;
    }
    SSL_CTX_set_default_passwd_cb(ctx, password_callback);
    SSL_CTX_set_default_passwd_cb_userdata(ctx, &passwordData);
    if (SSL_CTX_use_RSAPrivateKey_file(ctx, (char *)s, type) <= 0) throwEx(env);
    env->ReleaseByteArrayElements(password, pw, JNI_ABORT);
    env->ReleaseStringUTFChars(file, s);
}

extern "C" JNIEXPORT jlong JNICALL Java_FI_realitymodeler_SecureSocketsLayer_getCipherByName0(JNIEnv *env, jclass clazz, jstring name)
{
    if (checkCtx(env)) return 0;
    const char *s = env->GetStringUTFChars(name, NULL);
    if (!s) return 0;
    const EVP_CIPHER *cipher = EVP_get_cipherbyname(s);
    if (!cipher) throwEx(env);
    env->ReleaseStringUTFChars(name, s);
    return (jlong)cipher;
}

extern "C" JNIEXPORT jlong JNICALL Java_FI_realitymodeler_SecureSocketsLayer_getDigestByName(JNIEnv *env, jclass clazz, jstring name)
{
    if (checkCtx(env)) return 0;
    const char *s = env->GetStringUTFChars(name, NULL);
    if (!s) return 0;
    const EVP_MD *digest = EVP_get_digestbyname(s);
    if (!digest) throwEx(env);
    env->ReleaseStringUTFChars(name, s);
    return (jlong)digest;
}

extern "C" JNIEXPORT jlong JNICALL Java_FI_realitymodeler_SecureSocketsLayer_getPrivateKey0(JNIEnv *env, jclass clazz, jbyteArray bytes, jbyteArray password)
{
    jbyte *pw = env->GetByteArrayElements(password, NULL);
    if (!pw) return 0;
    jbyte *b = env->GetByteArrayElements(bytes, NULL);
    if (!b) {
        env->ReleaseByteArrayElements(password, pw, JNI_ABORT);
        return 0;
    }
    PASSWORD_DATA passwordData;
    passwordData.password = (char *)pw;
    passwordData.len = env->GetArrayLength(password);
    BIO *bio = BIO_new(BIO_s_mem());
    BUF_MEM *bufMem = BUF_MEM_new();
    int len = env->GetArrayLength(bytes);
    BUF_MEM_grow(bufMem, len);
    memcpy(bufMem->data, b, len);
    BIO_set_mem_buf(bio, bufMem, 0);
    EVP_PKEY *privateKey = PEM_read_bio_PrivateKey(bio, NULL, password_callback, &passwordData);
    if (!privateKey) env->ThrowNew(ioException, "Unable to load private key");
    BUF_MEM_free(bufMem);
    env->ReleaseByteArrayElements(bytes, b, JNI_ABORT);
    env->ReleaseByteArrayElements(password, pw, JNI_ABORT);
    return (jlong)privateKey;
}

extern "C" JNIEXPORT jlong JNICALL Java_FI_realitymodeler_SecureSocketsLayer_getCertificate0(JNIEnv *env, jclass clazz, jbyteArray bytes, jbyteArray password)
{
    jbyte *pw = env->GetByteArrayElements(password, NULL);
    if (!pw) return 0;
    jbyte *b = env->GetByteArrayElements(bytes, NULL);
    if (!b) {
        env->ReleaseByteArrayElements(password, pw, JNI_ABORT);
        return 0;
    }
    PASSWORD_DATA passwordData;
    passwordData.password = (char *)pw;
    passwordData.len = env->GetArrayLength(password);
    BIO *bio = BIO_new(BIO_s_mem());
    BUF_MEM *bufMem = BUF_MEM_new();
    int len = env->GetArrayLength(bytes);
    BUF_MEM_grow(bufMem, len);
    memcpy(bufMem->data, b, len);
    BIO_set_mem_buf(bio, bufMem, 0);
    X509 *certificate = PEM_read_bio_X509_AUX(bio, NULL, password_callback, &passwordData);
    if (!certificate) env->ThrowNew(ioException, "Unable to load certificate");
    BUF_MEM_free(bufMem);
    env->ReleaseByteArrayElements(bytes, b, JNI_ABORT);
    env->ReleaseByteArrayElements(password, pw, JNI_ABORT);
    return (jlong)certificate;
}

extern "C" JNIEXPORT jint JNICALL Java_FI_realitymodeler_SecureSocketsLayer_freeCertificate0(JNIEnv *env, jclass clazz, jlong certificate)
{
    X509_free((X509 *)certificate);
    return 0;
}

extern "C" JNIEXPORT jlong JNICALL Java_FI_realitymodeler_SecureSocketsLayer_getCertificateStack0(JNIEnv *env, jclass clazz)
{
    return (jlong)sk_X509_new_null();
}

extern "C" JNIEXPORT jint JNICALL Java_FI_realitymodeler_SecureSocketsLayer_certificateStackPush0(JNIEnv *env, jclass clazz, jlong certificateStack, jlong certificate)
{
    sk_X509_push((STACK_OF(X509) *)certificateStack, (X509 *)certificate);
    return 0;
}

extern "C" JNIEXPORT jint JNICALL Java_FI_realitymodeler_SecureSocketsLayer_freeCertificateStack0(JNIEnv *env, jclass clazz, jlong certificateStack)
{
    sk_X509_pop_free((STACK_OF(X509) *)certificateStack, X509_free);
    return 0;
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_SecureSocketsLayer_pkcs7Encrypt0(JNIEnv *env, jclass clazz, jlong certificateStack, jobject in, jobject out, jbyteArray inBuffer, jbyteArray outBuffer, jlong cipher, jint flags)
{
    if (!in || !out || !inBuffer || !outBuffer) {
        env->ThrowNew(nullPointerException, "");
        return;
    }
    STREAM_DATA streamData;
    streamData.env = env;
    streamData.in = in;
    streamData.out = out;
    streamData.inBuffer = inBuffer;
    streamData.outBuffer = outBuffer;
    streamData.inBuf = env->GetByteArrayElements(inBuffer, NULL);
    BIO *bio = BIO_new(&stream_methods);
    BIO_set_app_data(bio, &streamData);
    PKCS7 *pkcs7 = PKCS7_encrypt((STACK_OF(X509) *)certificateStack, bio, (EVP_CIPHER *)cipher, flags);
    if (pkcs7) {
        PEM_write_bio_PKCS7(bio, pkcs7);
        PKCS7_free(pkcs7);
    }
    else env->ThrowNew(ioException, "Error reading S/MIME message");
    BIO_free(bio);
    env->ReleaseByteArrayElements(inBuffer, streamData.inBuf, JNI_ABORT);
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_SecureSocketsLayer_pkcs7Decrypt0(JNIEnv *env, jclass clazz, jobject in, jbyteArray inBuffer, jlong key, jlong certificate, jobject out, jbyteArray outBuffer, jint flags)
{
    if (!in || !out || !inBuffer || !outBuffer) {
        env->ThrowNew(nullPointerException, "");
        return;
    }
    STREAM_DATA streamData;
    streamData.env = env;
    streamData.in = in;
    streamData.out = out;
    streamData.inBuffer = inBuffer;
    streamData.outBuffer = outBuffer;
    streamData.inBuf = env->GetByteArrayElements(inBuffer, NULL);
    BIO *bio = BIO_new(&stream_methods);
    BIO_set_app_data(bio, &streamData);
    BIO *inData = NULL;
    PKCS7 *pkcs7 = SMIME_read_PKCS7(bio, &inData);
    if (!pkcs7) env->ThrowNew(ioException, "Error reading S/MIME message");
    if (!PKCS7_decrypt(pkcs7, (EVP_PKEY *)key, (X509 *)certificate, bio, flags))
        env->ThrowNew(ioException, "Error decrypting PKCS#7 structure");
    BIO_free(bio);
    if (inData) BIO_free(inData);
    env->ReleaseByteArrayElements(inBuffer, streamData.inBuf, JNI_ABORT);
}

extern "C" JNIEXPORT jlong JNICALL Java_FI_realitymodeler_SecureSocketsLayer_construct(JNIEnv *env, jobject obj)
{
    if (checkCtx(env)) return 0;
    jlong instance = (jlong)SSL_new(ctx);
    if (!instance) env->ThrowNew(runtimeException, "Out of memory");
    return instance;
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_SecureSocketsLayer_destroy(JNIEnv *env, jobject obj, jlong instance)
{
    SSL_free((SSL *)instance);
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_SecureSocketsLayer_accept0(JNIEnv *env, jobject obj, jlong instance)
{
    if (SSL_accept((SSL *)instance) <= 0) throwEx(env);
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_SecureSocketsLayer_clear0(JNIEnv *env, jobject obj, jlong instance)
{
    SSL_clear((SSL *)instance);
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_SecureSocketsLayer_connect0(JNIEnv *env, jobject obj, jlong instance)
{
    if (SSL_connect((SSL *)instance) <= 0) throwEx(env);
}

extern "C" JNIEXPORT jstring JNICALL Java_FI_realitymodeler_SecureSocketsLayer_getCipher0(JNIEnv *env, jobject obj, jlong instance)
{
    const char *s = SSL_get_cipher((SSL *)instance);
    return s ? env->NewStringUTF(s) : (jstring)NULL;
}

extern "C" JNIEXPORT jstring JNICALL Java_FI_realitymodeler_SecureSocketsLayer_getCipherList0(JNIEnv *env, jobject obj, jlong instance, jint n)
{
    const char *s = SSL_get_cipher_list((SSL *)instance, n);
    return s ? env->NewStringUTF(s) : (jstring)NULL;
}

extern "C" JNIEXPORT jstring JNICALL Java_FI_realitymodeler_SecureSocketsLayer_getSharedCiphers0(JNIEnv *env, jobject obj, jlong instance, jbyteArray buf)
{
    jbyte *b = env->GetByteArrayElements(buf, NULL);
    if (!b) return NULL;
    char *s = SSL_get_shared_ciphers((SSL *)instance, (char *)b, env->GetArrayLength(buf));
    env->ReleaseByteArrayElements(buf, b, JNI_ABORT);
    return s ? env->NewStringUTF(s) : (jstring)NULL;
}

extern "C" JNIEXPORT jint JNICALL Java_FI_realitymodeler_SecureSocketsLayer_getReadAhead0(JNIEnv *env, jobject obj, jlong instance)
{
    return SSL_get_read_ahead((SSL *)instance);
}

extern "C" JNIEXPORT jint JNICALL Java_FI_realitymodeler_SecureSocketsLayer_isInitFinished0(JNIEnv *env, jobject obj, jlong instance)
{
    return SSL_is_init_finished((SSL *)instance);
}

extern "C" JNIEXPORT jint JNICALL Java_FI_realitymodeler_SecureSocketsLayer_pending0(JNIEnv *env, jobject obj, jlong instance)
{
    return SSL_pending((SSL *)instance);
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_SecureSocketsLayer_setCipherList0(JNIEnv *env, jobject obj, jlong instance, jstring str)
{
    if (!str) {
        env->ThrowNew(nullPointerException, "");
        return;
    }
    const char *s = env->GetStringUTFChars(str, NULL);
    if (SSL_set_cipher_list((SSL *)instance, (char *)s) <= 0) throwEx(env);
    env->ReleaseStringUTFChars(str, s);
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_SecureSocketsLayer_setReadAhead0(JNIEnv *env, jobject obj, jlong instance, jboolean yes)
{
    SSL_set_read_ahead((SSL *)instance, yes);
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_SecureSocketsLayer_setSocket0(JNIEnv *env, jobject obj, jlong instance, jint socket)
{
    if (!socket) {
        env->ThrowNew(nullPointerException, "");
        return;
    }
    SSL_set_fd((SSL *)instance, socket);
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_SecureSocketsLayer_setVerify0(JNIEnv *env, jobject obj, jlong instance, jint mode)
{
    SSL_set_verify((SSL *)instance, mode, NULL);
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_SecureSocketsLayer_useCertificateFile0(JNIEnv *env, jobject obj, jlong instance, jstring file, jint type)
{
    const char *s = env->GetStringUTFChars(file, NULL);
    if (!s) return;
    if (SSL_use_certificate_file((SSL *)instance, (char *)s, type) <= 0) throwEx(env);
    env->ReleaseStringUTFChars(file, s);
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_SecureSocketsLayer_useRSAPrivateKeyFile0(JNIEnv *env, jobject obj, jlong instance, jstring file, jint type)
{
    const char *s = env->GetStringUTFChars(file, NULL);
    if (!s) return;
    if (SSL_use_RSAPrivateKey_file((SSL *)instance, (char *)s, type) <= 0) throwEx(env);
    env->ReleaseStringUTFChars(file, s);
}

extern "C" JNIEXPORT jint JNICALL Java_FI_realitymodeler_SecureSocketsLayer_read0(JNIEnv *env, jobject obj, jlong instance, jbyteArray buf, jint off, jint len)
{
    if (len <= 0) return 0;
    if (!buf) {
        env->ThrowNew(nullPointerException, "");
        return 0;
    }
    if (off < 0 || off + len > env->GetArrayLength(buf)) {
        env->ThrowNew(arrayIndexOutOfBoundsException, "");
        return 0;
    }
    jbyte *b = env->GetByteArrayElements(buf, NULL);
    if (!b) return 0;
    int n = SSL_read((SSL *)instance, (char *)b + off, len);
    env->ReleaseByteArrayElements(buf, b, 0);
    if (n < 0) throwEx(env);
    return n ? n : -1;
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_SecureSocketsLayer_write0(JNIEnv *env, jobject obj, jlong instance, jbyteArray buf, jint off, jint len)
{
    if (len <= 0) return;
    if (!buf) {
        env->ThrowNew(nullPointerException, "");
        return;
    }
    if (off < 0 || off + len > env->GetArrayLength(buf)) {
        env->ThrowNew(arrayIndexOutOfBoundsException, "");
        return;
    }
    jbyte *b = env->GetByteArrayElements(buf, NULL);
    if (!b) return;
    if (SSL_write((SSL *)instance, (char *)b + off, len) != len) throwEx(env);
    env->ReleaseByteArrayElements(buf, b, JNI_ABORT);
}
