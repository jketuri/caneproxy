
package FI.realitymodeler.common;

import java.text.*;
import java.util.*;

/**
 * Class that implements a request/response header, which
 * consists of the header name and value.
 */
public class Header {
    private String m_name = null;
    private String m_value = null;

    /**
     * Constructs a header with the specified name and value.
     *
     * @param name name of the header
     * @param value value of the header
     */
    public Header(String name, String value) {
        m_name = name;
        m_value = value;
    }

    /**
     * Constructs a header with the specified name and integer value.
     *
     * @param name name of the header
     * @param value value of the header as an int
     */
    public Header(String name, int value) {
        m_name = name;
        m_value = String.valueOf(value);
    }

    public Header(String name, long value) {
        m_name = name;
        m_value = String.valueOf(value);
    }

    /**
     * Constructs a header with the specified name and Date value.
     *
     * @param name name of the header
     * @param value value of the header as a Date
     */
    public Header(String name, Date value) {
        m_name = name;
        setDateValue(value);
    }

    /**
     * Constructs a header that is a copy of the given header.
     *
     * @param hdr the header to copy
     */
    public Header(Header hdr) {
        m_name = hdr.getName();
        m_value = hdr.getValue();
    }

    /**
     * Gets the name of the header.
     *
     * @return the name of the header
     */
    public String getName() {
        return m_name;
    }

    /**
     * Gets the value of the header as a string.
     *
     * @return the value of the header as a string
     */
    public String getValue() {
        return m_value;
    }

    /**
     * Get the value of the header as an int.
     *
     * @return the value of the header as an int
     * @exception NumberFormatException if the value can't be
     * coverted to integer
     */
    public int getIntValue() throws NumberFormatException {
        return Integer.parseInt(m_value);
    }

    /**
     * Get the value of the header as a Date.
     *
     * @return the value of the header as a Date
     * @exception IllegalArgumentException if the value can't be
     * converted to Date
     */
    public Date getDateValue() throws IllegalArgumentException {
        Date date = Support.parse(m_value);
        if (date == null) throw new IllegalArgumentException("Invalid date format");
        return date;
    }

    /**
     * Sets the value of the header.
     *
     * @param value new value of the header as a string
     */
    public void setValue(String value) {
        m_value = value;
    }

    /**
     * Sets the value of the header from an int.
     *
     * @param value new value of the header as an int
     */
    public void setIntValue(int value) {
        m_value = String.valueOf(value);
    }

    /**
     * Sets the value of the header from a Date.
     *
     * @param value new value of the header as a Date.
     */
    public void setDateValue(Date value) {
        m_value = Support.dateFormat.format(value);
    }

    /**
     * Tests equality of this header and another object. Overrides 
     * the equals method in class Object.
     *
     * @param obj the object to test for equality
     * @return true if the given object is this header itself, or
     * another header object with the same name and value as this.
     * Otherwise returns false.
     */
    public boolean equals(Object obj) {
        // null or not instance of Header
        if (!(obj instanceof Header)) return false;
        // this object itself
        if (obj == this) return true;
        // another Header object
        Header hdr = (Header)obj;
        return (m_name.equalsIgnoreCase(hdr.getName()) &&
                m_value.equalsIgnoreCase(hdr.getValue()));
    }

}
