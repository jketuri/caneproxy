
package FI.realitymodeler.common;

import java.io.*;

public class EncodingConversionInputStream extends PipedInputStream implements Runnable {
    Reader reader;
    Writer writer;

    public void run() {
        try {
            char b[] = new char[Support.bufferLength];
            for (int n; (n = reader.read(b)) > 0;) writer.write(b, 0, n);
            writer.close();
        } catch (IOException ex) {
            try {
                close();
            } catch (IOException ex1) {}
        }
    }

    public EncodingConversionInputStream(Reader reader, String encoding) throws IOException {
        this.reader = reader;
        writer = new OutputStreamWriter(new PipedOutputStream(this), encoding);
        new Thread(this).start();
    }

}
