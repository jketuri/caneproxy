
package FI.realitymodeler.common;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.EOFException;
import java.io.IOException;

/**
 */

public abstract class Decoder {

    protected void decodeStreamPrefix(InputStream in, OutputStream out)
        throws IOException {
    }

    protected void decodeStreamSuffix(InputStream in, OutputStream out)
        throws IOException {
    }

    /**
     */
    protected int decodeSequencePrefix(InputStream in, OutputStream out)
        throws IOException {
        return bytesInSequence();
    }

    /**
     */
    protected void decodeSequenceSuffix(InputStream in, OutputStream out)
        throws IOException {
    }

    /**
     */
    protected void decodeToken(InputStream in, OutputStream out, int length)
        throws IOException {
        throw new EOFException();
    }

    /**
     */
    protected int readFully(InputStream in, byte buffer[], int offset, int length)
        throws IOException {
        int offset0 = offset;
        length += offset;
        while (offset < length) {
            int n = in.read(buffer, offset, length - offset);
            if (n <= 0) break;
            offset += n;
        }
        return offset - offset0;
    }

    /**
     */
    public void decodeStream(InputStream in, OutputStream out)
        throws IOException {
        int i;
        int totalBytes = 0;

        decodeStreamPrefix(in, out);
        for (;;) {
            int length;
            try {
                length = decodeSequencePrefix(in, out);
                for (i = 0; (i + bytesInToken()) < length; i += bytesInToken()) {
                    decodeToken(in, out, bytesInToken());
                    totalBytes += bytesInToken();
                }
                if ((i + bytesInToken()) == length) {
                    decodeToken(in, out, bytesInToken());
                    totalBytes += bytesInToken();
                } else {
                    decodeToken(in, out, length - i);
                    totalBytes += (length - i);
                }
                decodeSequenceSuffix(in, out);
            } catch (NextInputStreamException ex) {
            } catch (EOFException ex) {
                break;
            }
        }
        decodeStreamSuffix(in, out);
    }

    /**
     */
    public byte[] decodeStream(String inputString)
        throws IOException {
        ByteArrayInputStream in;
        ByteArrayOutputStream out;
        byte inputBuffer[] = inputString.getBytes();
        in = new ByteArrayInputStream(inputBuffer);
        out = new ByteArrayOutputStream();
        decodeStream(in, out);
        return out.toByteArray();
    }

    /**
     */
    public byte[] decodeStream(InputStream in)
        throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        decodeStream(in, out);
        return out.toByteArray();
    }

    abstract protected int bytesInToken();

    abstract protected int bytesInSequence();

}
