
package FI.realitymodeler.common;

import java.io.*;

/** General string conversion filter input stream. */
public class ConvertInputStream extends FilterInputStream {
    protected String feed = null, scan = null, replace = null, left = null, markedFeed = null;
    protected int c, length = -1, feedNum = 0, feedPos = 0, scanPos = 0,
        markedChar = 0, markedLength = -1, markedFeedNum = 0, markedFeedPos = 0;
    protected boolean anchored = false, boundary = false, character = false,
        stop = false, unread = false, markedUnread = false, needCR = false, wasCR = true;

    /** Constructs conversion input stream.
        @param in input stream to read from
        @param scan string to scan (skipped when replace string is null and stops reading when stop flag is set)
        @param replace string to replace scan strings or null (if scan string is null this is sent in the end)
        @param left characters scanned strings must follow or null
        @param length number of characters to read before stream ends or -1 if not used
        @param anchored scan string must occur in the beginning of line if this flag is set
        @param character individual characters are scanned and corresponding character is replaced
        @param stop stream ends when scan string is encountered when this flag is set */
    public ConvertInputStream(InputStream in, String scan, String replace, String left, int length, boolean anchored, boolean boundary, boolean character, boolean stop) {
        super(in);
        this.scan = scan;
        this.replace = replace;
        this.left = left;
        this.length = length;
        this.anchored = anchored;
        this.boundary = boundary;
        this.character = character;
        this.stop = stop;
        needCR = anchored && left != null && left.equals("\r");
        c = anchored ? '\n' : left != null ? left.charAt(0) : 0;
    }

    public ConvertInputStream(InputStream in, String scan, String replace, String left, boolean anchored) {
        this(in, scan, replace, left, -1, anchored, false, false, false);
    }

    public ConvertInputStream(InputStream in, String scan, String replace, boolean anchored) {
        this(in, scan, replace, null, -1, anchored, false, false, false);
    }

    public ConvertInputStream(InputStream in, String scan, String left, boolean anchored, boolean stop) {
        this(in, scan, null, left, -1, anchored, false, false, stop);
    }

    public ConvertInputStream(InputStream in, String scan, boolean anchored, boolean stop) {
        this(in, scan, null, null, -1, anchored, false, false, stop);
    }

    public ConvertInputStream(InputStream in, String scan, String replace) {
        this(in, scan, replace, null, -1, false, false, false, false);
    }

    public ConvertInputStream(InputStream in, String scan, int length) {
        this(in, scan, null, null, length, false, false, false, false);
    }

    public ConvertInputStream(InputStream in, String scan, boolean boundary) {
        this(in, scan, null, null, -1, false, boundary, false, false);
    }

    public ConvertInputStream(InputStream in, String scan) {
        this(in, scan, null, null, -1, false, false, false, false);
    }

    public ConvertInputStream(InputStream in, int length) {
        this(in, null, null, null, length, false, false, false, false);
    }

    public int getLength() {
        return length;
    }

    public String convert(String scan, String replace) throws IOException {
        return replace;
    }

    public String convert(char scan, char replace) throws IOException {
        return String.valueOf(replace);
    }

    public int read() throws IOException {
        if (feedPos < feedNum) return feed.charAt(feedPos++);
        if (c == -1) return -1;
        if (unread) {
            unread = false;
            return c;
        }
        if (anchored ? c != '\n' || needCR && !wasCR : left != null && left.indexOf(c) == -1) {
            wasCR = c == '\r';
            return c = length == -1 || --length >= 0 ? in.read() : -1;
        }
        wasCR = c == '\r';
        while ((length == -1 || --length >= 0) && (c = in.read()) != -1) {
            if (scan == null) return c;
            if (character) {
                int index = scan.indexOf(c);
                if (index != -1) {
                    if (stop) c = -1;
                    if (replace != null) {
                        feed = convert(scan.charAt(index), replace.charAt(index));
                        feedPos = 1;
                        feedNum = feed.length();
                        return feed.charAt(0);
                    }
                    if (stop) return c;
                }
                return c;
            }
            if (scan.charAt(scanPos) == c) {
                if (++scanPos == scan.length()) {
                    scanPos = 0;
                    if (stop) c = -1;
                    if (replace != null || boundary) {
                        if (boundary) {
                            feed = scan;
                            char b;
                            while ((b = (char)(33 + (int)Math.random() * 14.0)) == c);
                            scan += b;
                        } else feed = convert(scan, replace);
                        feedPos = 1;
                        feedNum = feed.length();
                        return feed.charAt(0);
                    }
                    if (stop) return c;
                }
                wasCR = c == '\r';
                continue;
            }
            if (!anchored && left == null)
                for (int i = 1; i < scanPos; i++)
                    if (scan.regionMatches(0, scan, i, scanPos - i) && scan.charAt(scanPos - i) == c) {
                        feed = scan;
                        feedPos = 1;
                        feedNum = i;
                        scanPos -= i - 1;
                        return feed.charAt(0);
                    }
            if (scanPos > 0) {
                feed = scan;
                feedPos = 1;
                feedNum = scanPos;
                if (anchored || scan.charAt(0) != c) {
                    scanPos = 0;
                    unread = true;
                } else scanPos = 1;
                return feed.charAt(0);
            }
            return c;
        }
        if (replace != null && scan == null) {
            c = -1;
            feed = replace;
            feedPos = 1;
            feedNum = feed.length();
            return feed.charAt(0);
        }
        return c = -1;
    }

    public int read(byte b[], int off, int len) throws IOException {
        int c = 0, i = off;
        len += off;
        while (i < len && (c = read()) != -1) b[i++] = (byte)c;
        return (i -= off) > 0 ? i : c == -1 ? -1 : 0;
    }

    public int available() {
        return feedNum - feedPos;
    }

    public synchronized void mark(int readlimit) {
        if (isEnded()) return;
        in.mark(readlimit);
        markedChar = c;
        markedLength = length;
        markedFeed = feed;
        markedFeedNum = feedNum;
        markedFeedPos = feedPos;
        markedUnread = unread;
    }

    public synchronized void reset() throws IOException {
        if (isEnded()) throw new EOFException();
        in.reset();
        c = markedChar;
        length = markedLength;
        feed = markedFeed;
        feedNum = markedFeedNum;
        feedPos = markedFeedPos;
        unread = markedUnread;
    }

    public String getScan() {
        return scan;
    }

    public boolean isEnded() {
        return c == -1;
    }

}
