
package FI.realitymodeler.common;

import java.io.*;
import java.text.*;
import java.util.*;

public class EntityMemory {
    HashMap<String,OutputStream> entities = new HashMap<String,OutputStream>();
    String entryName = null;
    long entryHeaderSize = 0L, entrySize = 0L;
    int entryNumber = 0;

    public boolean exists(String entryName)
        throws IOException {
        return entities.containsKey(entryName);
    }

    public InputStream load(String entryName, HeaderList headerList)
        throws IOException, ParseException {
        ByteArrayOutputStream bout = (ByteArrayOutputStream)entities.get(entryName);
        if (bout == null) return null;
        InputStream in = new ByteArrayInputStream(bout.toByteArray());
        if (headerList != null) Support.readHeaderList(in, headerList);
        return in;
    }

    public InputStream load(String entryName)
        throws IOException, ParseException {
        return load(entryName, null);
    }

    public OutputStream save(String entryName, HeaderList headerList, boolean check)
        throws IOException, ParseException {
        entryNumber++;
        if (entryName == null) entryName = String.valueOf(entryNumber);
        this.entryName = entryName;
        OutputStream out = new ByteArrayOutputStream();
        if (headerList != null) Support.sendHeaderList(out, headerList);
        entryHeaderSize = ((ByteArrayOutputStream)out).size();
        entities.put(entryName, out);
        return out;
    }

    public OutputStream save(String entryName, HeaderList headerList)
        throws IOException, ParseException {
        return save(entryName, headerList, false);
    }

    public OutputStream save(String entryName, boolean check)
        throws IOException, ParseException {
        return save(entryName, null, check);
    }

    public OutputStream save(String entryName)
        throws IOException, ParseException {
        return save(entryName, false);
    }

    public String getEntryName() {
        return entryName;
    }

    public long getEntrySize() {
        return entrySize;
    }

    public void closeStream(OutputStream out)
        throws IOException {
        out.close();
        entrySize = ((ByteArrayOutputStream)out).size() - entryHeaderSize;
    }

    public void closeStream(InputStream in)
        throws IOException {
        in.close();
    }

    public int getNumberOfElements()
        throws IOException {
        return entities.size();
    }

    public String getFilePath()
        throws IOException {
        return null;
    }

    public boolean delete(String entryName)
        throws IOException {
        entryNumber--;
        return entities.remove(entryName) != null;
    }

    public void setReadOnly()
        throws IOException {
    }

    public void close()
        throws IOException {
    }

    public synchronized void reset()
        throws IOException {
        entities.clear();
        entryNumber = 0;
    }

}
