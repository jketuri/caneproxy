
package FI.realitymodeler.common;

import java.io.*;

public class WriterOutputStream extends PipedOutputStream implements Runnable {
    Thread thread;
    Reader reader;
    Writer writer;

    public void run() {
        try {
            char ch[] = new char[Support.bufferLength];
            for (int n; (n = reader.read(ch)) > 0;) writer.write(ch, 0, n);
            writer.flush();
        } catch (IOException ex) {}
    }

    public WriterOutputStream(Writer writer, String encoding) throws IOException {
        this.writer = writer;
        reader = new InputStreamReader(new PipedInputStream(this), encoding);
        (thread = new Thread(this)).start();
    }

    public void close() throws IOException {
        super.close();
        try {
            thread.join();
        } catch (InterruptedException ex) {}
    }

}
