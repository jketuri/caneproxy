
package FI.realitymodeler.common;

import java.io.*;

public class HtmlOutputStream
    extends FilterOutputStream {
    private String encoding;

    public HtmlOutputStream(OutputStream out, String encoding) {
        super(out);
        if (encoding == null) encoding = "UTF-8";
        this.encoding = encoding;
    }

    public void write(int c) throws IOException {
        if (c == 34 || c == 38 || c == 60 || c == 62) {
            out.write('&');
            out.write('#');
            out.write(String.valueOf(c).getBytes(encoding));
            out.write(';');
        } else out.write(c);
    }

    public void write(byte buf[], int off, int len)
        throws IOException {
        for (int i = off; i < len; i++) write(buf[i] & 0xff);
    }

}
