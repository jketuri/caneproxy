
package FI.realitymodeler.common;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;

public class QPEncoder extends Encoder {
    boolean isEncodedWord;

    public QPEncoder(boolean isEncodedWord) {
        this.isEncodedWord = isEncodedWord;
    }

    public QPEncoder() {
        this(false);
    }

    protected void encodeToken(OutputStream out, byte buffer[], int offset, int length) {
    }

    public void encode(InputStream in, OutputStream out)
        throws IOException {
        int c, n = 0;
        for (int b = 0; (c = in.read()) != -1; b = c)
            if (c <= 32 && (isEncodedWord || Support.whites.indexOf(c) == -1) || c == '=' || c > 126) {
                if ((n += 3) > 76) {
                    out.write('=');
                    out.write('\r');
                    out.write('\n');
                    n = 0;
                }
                out.write('=');
                out.write(Character.forDigit(c >> 4 & 0xf, 16));
                out.write(Character.forDigit(c & 0xf, 16));
            } else if (c == '\n') {
                n = 0;
                if (b != '\r') out.write('\r');
                out.write(c);
            } else {
                if (++n > 76) {
                    out.write('=');
                    out.write('\r');
                    out.write('\n');
                    n = 0;
                }
                out.write(c);
            }
    }

    public void encodeStream(InputStream in, OutputStream out)
        throws IOException {
        encode(in, out);
        out.write('\r');
        out.write('\n');
    }

    protected int bytesInToken() {
        return 1;
    }

    protected int bytesInSequence() {
        return 1;
    }

}
