
package FI.realitymodeler.common;

import java.io.*;
import java.lang.reflect.*;
import java.net.*;
import java.text.*;
import java.sql.*;
import java.text.*;
import java.util.*;
import java.util.jar.*;
import javax.servlet.*;

class LinkOutputStream extends ConvertOutputStream {
    static final String scan1 = Support.whites + "!\"'()*,";
    static final String left1 = scan1 + "-./:;=?@$";

    ByteArrayOutputStream bout = new ByteArrayOutputStream();
    OutputStream out1 = null;
    String schemeSpec = null, schemeKey = null, servletPath = null;
    boolean guessFromName = false;

    LinkOutputStream(OutputStream out, String schemeKey, String charsetName, Map<String, String> servletPathTable) {
        super(out, schemeKey, schemeKey, left1, null, false, false);
        this.schemeKey = schemeKey;
        servletPath = servletPathTable != null ? servletPathTable.get(schemeKey) : null;
        setCharsetName(charsetName);
    }

    LinkOutputStream(OutputStream out, String schemeKey, String schemeSpec, String charsetName) {
        super(out, schemeKey, schemeKey, Support.whites, null, false, false);
        this.schemeKey = schemeKey;
        this.schemeSpec = schemeSpec;
        servletPath = null;
        guessFromName = true;
        setCharsetName(charsetName);
    }

    public String convert(String scan, String replace)
        throws IOException {
        out.flush();
        out1 = out;
        out = bout;
        this.scan = this.replace = scan1;
        left = null;
        character = true;
        return "";
    }

    public String convert(char scan, char replace)
        throws IOException {
        out.flush();
        out = out1;
        this.scan = this.replace = schemeKey;
        left = guessFromName ? Support.whites : left1;
        character = wasChar = false;
        if (bout.size() > 0) {
            String s = bout.toString();
            bout.reset();
            if (guessFromName) Support.writeBytes(out, schemeSpec + schemeKey + s, charsetName);
            else if (s.indexOf('>') == -1) {
                int i = s.indexOf("&#62;"),
                    j = s.indexOf("&#34;");
                if (j != -1)
                    i = i != -1 ? Math.min(i, j) : j;
                String r = "";
                if (i != -1) {
                    r = s.substring(i);
                    s = s.substring(0, i);
                }
                String link = (s = schemeKey + s);
                if (servletPath != null)
                    try {
                        URL url = new URL(link);
                        StringBuffer sb = new StringBuffer(servletPath);
                        String path = url.getFile();
                        if ((i = path.indexOf('?')) != -1) {
                            sb.append(path.substring(i)).append('&');
                            path = path.substring(0, i);
                        } else sb.append('?');
                        if (url.getHost() != null && !url.getHost().equals("")) sb.append("host=").append(url.getHost()).append('&');
                        StringTokenizer st = new StringTokenizer(url.getFile(), "/");
                        String name = st.hasMoreTokens() ? st.nextToken() : null;
                        if (schemeKey.equals("mailto:")) {
                            sb.append("form=");
                            if (name != null) sb.append("&to=").append(name);
                        } else if (schemeKey.equals("news:")) {
                            sb.append("new=");
                            if (name != null) sb.append("&group=").append(name);
                        } else if (schemeKey.equals("wais:")) {
                            sb.append("new=");
                            if (name != null) sb.append("&databases=").append(name);
                        }
                        link = sb.toString();
                    } catch (MalformedURLException ex) {} else link = Support.replace(link, Support.ampSign, "&");
                Support.writeBytes(out, "<a href=\"" + link + "\">" + s + "</a>" + r, charsetName);
            } else Support.writeBytes(out, schemeKey + s, charsetName);
        } else Support.writeBytes(out, schemeKey, charsetName);
        if (replace != 0) out.write(replace);
        return "";
    }

}

class StringComparator implements Comparator<String> {

    public int compare(String o1, String o2) {
        return o1.compareTo(o2);
    }

    public boolean equals(Object obj) {
        return obj instanceof StringComparator;
    }

}

public abstract class Support {
    public static final String whites = "\0\t\n\r ";
    public static final String plainType = "text/plain";
    public static final String htmlType = "text/html";
    public static final String shtmlType = "text/shtml";
    public static final String octetStreamType = "application/octet-stream";
    public static final String partialType = "message/partial";
    public static final String unknownType = "content/unknown";
    public static final String defaultCharsetName = "=?ISO-8859-1?q?";
    public static final String ampSign = "&amp;", leftSign = "&lt;", rightSign = "&gt;", quotSign = "&quot;";
    public static final String vCardTokens = ".;=,:";
    public static final byte CANONIZE = 0, ENCODE = 1, DECODE = 2;
    public static final byte NORMAL = 0, ESCAPE = 1, UNWISE = 2;
    public static final int TAG_WITHOUT_VALUES = -2, TAG_WITH_VALUES = 0;
    public static Object emptyArray = new Object[0];
    public static Comparator<String> stringComparator = new StringComparator();
    public static Map<String, String> characterEncodings = new HashMap<String, String>();
    public static Map<String, Character> codeTable = new HashMap<String, Character>();
    public static SimpleDateFormat dateFormats[], dateFormat, generalizedTimeFormat;
    public static byte uriCharType[];
    public static int bufferLength = 1024 << 2;

    static String filterChars = "\0()*\\";
    static String dnChars = "\"+,/;<=>\\";
    static String dnChars1 = " #" + dnChars;
    static String dnChars2 = " " + dnChars;
    static {
        try {
            String escapeChars = "\"#%&+,/:;<=>?@", unwiseChars = "[\\]{|}";
            uriCharType = new byte[0x7e - 0x20];
            for (int i = escapeChars.length() - 1; i >= 0; i--) uriCharType[escapeChars.charAt(i) - 0x21] = ESCAPE;
            for (int i = unwiseChars.length() - 1; i >= 0; i--) uriCharType[unwiseChars.charAt(i) - 0x21] = UNWISE;
            codeTable.put("nbsp", new Character('\040'));
            codeTable.put("quot", new Character('\042'));
            codeTable.put("amp", new Character('\046'));
            codeTable.put("lt", new Character('\074'));
            codeTable.put("gt", new Character('\076'));
            codeTable.put("Agrave", new Character('\300'));
            codeTable.put("Aacute", new Character('\301'));
            codeTable.put("Acirc", new Character('\302'));
            codeTable.put("Atilde", new Character('\303'));
            codeTable.put("Auml", new Character('\304'));
            codeTable.put("Aring", new Character('\305'));
            codeTable.put("AElig", new Character('\306'));
            codeTable.put("Ccedil", new Character('\307'));
            codeTable.put("Egrave", new Character('\310'));
            codeTable.put("Eacute", new Character('\311'));
            codeTable.put("Ecirc", new Character('\312'));
            codeTable.put("Euml", new Character('\313'));
            codeTable.put("Igrave", new Character('\314'));
            codeTable.put("Iacute", new Character('\315'));
            codeTable.put("Icirc", new Character('\316'));
            codeTable.put("Iuml", new Character('\317'));
            codeTable.put("ETH", new Character('\320'));
            codeTable.put("Ntilde", new Character('\321'));
            codeTable.put("Ograve", new Character('\322'));
            codeTable.put("Oacute", new Character('\323'));
            codeTable.put("Ocirc", new Character('\324'));
            codeTable.put("Otilde", new Character('\325'));
            codeTable.put("Ouml", new Character('\326'));
            codeTable.put("Oslash", new Character('\330'));
            codeTable.put("Ugrave", new Character('\331'));
            codeTable.put("Uacute", new Character('\332'));
            codeTable.put("Ucirc", new Character('\333'));
            codeTable.put("Uuml", new Character('\334'));
            codeTable.put("Yacute", new Character('\335'));
            codeTable.put("thorn", new Character('\336'));
            codeTable.put("szlig", new Character('\337'));
            codeTable.put("agrave", new Character('\340'));
            codeTable.put("aacute", new Character('\341'));
            codeTable.put("acirc", new Character('\342'));
            codeTable.put("atilde", new Character('\343'));
            codeTable.put("auml", new Character('\344'));
            codeTable.put("aring", new Character('\345'));
            codeTable.put("aelig", new Character('\346'));
            codeTable.put("ccedil", new Character('\347'));
            codeTable.put("egrave", new Character('\350'));
            codeTable.put("eacute", new Character('\351'));
            codeTable.put("ecirc", new Character('\352'));
            codeTable.put("euml", new Character('\353'));
            codeTable.put("igrave", new Character('\354'));
            codeTable.put("iacute", new Character('\355'));
            codeTable.put("icirc", new Character('\356'));
            codeTable.put("iuml", new Character('\357'));
            codeTable.put("ieth", new Character('\360'));
            codeTable.put("ntilde", new Character('\361'));
            codeTable.put("ograve", new Character('\362'));
            codeTable.put("oacute", new Character('\363'));
            codeTable.put("ocirc", new Character('\364'));
            codeTable.put("otilde", new Character('\365'));
            codeTable.put("ouml", new Character('\366'));
            codeTable.put("oslash", new Character('\370'));
            codeTable.put("ugrave", new Character('\371'));
            codeTable.put("uacute", new Character('\372'));
            codeTable.put("ucirc", new Character('\373'));
            codeTable.put("uuml", new Character('\374'));
            codeTable.put("yacute", new Character('\375'));
            codeTable.put("THORN", new Character('\376'));
            codeTable.put("yuml", new Character('\377'));

            characterEncodings.put("UTF-8", "UTF8");
            characterEncodings.put("US-ASCII", "ISO-8859-1");
            characterEncodings.put("ISO-2022-JP", "JIS");
            characterEncodings.put("SHIFT_JIS", "SJIS");
            characterEncodings.put("EUC-JP", "EUCJIS");
            characterEncodings.put("BIG5", "Big5");

            int i = 0;

            dateFormats = new SimpleDateFormat[4 * 4];

            dateFormats[i++] = new SimpleDateFormat("EEE, d MMM yy HH:mm:ss z", Locale.US);
            dateFormats[i++] = new SimpleDateFormat("EEEE, dd-MMM-yy HH:mm:ss z", Locale.US);
            dateFormats[i++] = new SimpleDateFormat("EEE MMM d HH:mm:ss z yy", Locale.US);
            dateFormats[i++] = new SimpleDateFormat("d MMM yy HH:mm:ss z", Locale.US);

            dateFormats[i++] = new SimpleDateFormat("EEE, d MMM yy HH:mm:ss", Locale.US);
            dateFormats[i++] = new SimpleDateFormat("EEEE, dd-MMM-yy HH:mm:ss", Locale.US);
            dateFormats[i++] = new SimpleDateFormat("EEE MMM d HH:mm:ss yy", Locale.US);
            dateFormats[i++] = new SimpleDateFormat("d MMM yy HH:mm:ss", Locale.US);

            dateFormats[i++] = new SimpleDateFormat("EEE, d MMM yy HH:mm z", Locale.US);
            dateFormats[i++] = new SimpleDateFormat("EEEE, dd-MMM-yy HH:mm z", Locale.US);
            dateFormats[i++] = new SimpleDateFormat("EEE MMM d HH:mm z yy", Locale.US);
            dateFormats[i++] = new SimpleDateFormat("d MMM yy HH:mm z", Locale.US);

            dateFormats[i++] = new SimpleDateFormat("EEE, d MMM yy HH:mm", Locale.US);
            dateFormats[i++] = new SimpleDateFormat("EEEE, dd-MMM-yy HH:mm", Locale.US);
            dateFormats[i++] = new SimpleDateFormat("EEE MMM d HH:mm yy", Locale.US);
            dateFormats[i++] = new SimpleDateFormat("d MMM yy HH:mm", Locale.US);

            dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss 'GMT'", Locale.US);

            generalizedTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss'Z'", Locale.US);

            TimeZone tz = new SimpleTimeZone(0, "GMT");
            for (i = 0; i < dateFormats.length; i++) {
                dateFormats[i].setLenient(false);
                dateFormats[i].setTimeZone(tz);
            }
            dateFormat.setTimeZone(tz);
            generalizedTimeFormat.setTimeZone(tz);
        } catch (Exception ex) {
            throw new Error(stackTrace(ex));
        }
    }

    public static void prepare() {
    }

    /** Returns stack trace of throwable as a string. */
    public static String stackTrace(Throwable throwable) {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        PrintWriter pw = new PrintWriter(bout);
        throwable.printStackTrace(pw);
        pw.close();
        return bout.toString();
    }

    public static boolean isTrue(String s, String name)
        throws ParseException {
        if (s == null) return false;
        if ((s = s.trim().toLowerCase()).equals("true")) return true;
        if (!s.equals("false")) throw new ParseException("Property '" + name + "' must be 'true' or 'false'", 0);
        return false;
    }

    /** Returns standard format of date. */
    public static final String format(java.util.Date date) {
        return date != null ? dateFormat.format(date) : null;
    }

    /** Tries to parse date string with given formats. */
    public static java.util.Date parse(String dateString, DateFormat dateFormats[]) {
        if (dateString == null) return null;
        StringBuffer sb = new StringBuffer();
        StringTokenizer st = new StringTokenizer(dateString);
        while (st.hasMoreTokens()) {
            String s = st.nextToken();
            if (sb.length() > 0 && !s.equals(",")) sb.append(' ');
            sb.append(s);
        }
        dateString = sb.toString();
        java.util.Date date;
        for (int i = 0; i < dateFormats.length; i++)
            try {
                if ((date = dateFormats[i].parse(dateString)) != null) return date;
            } catch (Exception ex) {}
        return null;
    }

    /** Tries to parse date string with multiple formats. */
    public static final java.util.Date parse(String dateString) {
        return parse(dateString, dateFormats);
    }

    public static final String generalizedTime(long timeMillis) {
        return generalizedTimeFormat.format(new java.util.Date(timeMillis));
    }

    public static final long parseGeneralizedTime(String timeString)
        throws ParseException {
        return generalizedTimeFormat.parse(timeString).getTime();
    }

    public static String escapeDn(String value) {
        StringBuffer sb = new StringBuffer();
        int n = value.length();
        if (--n >= 0) {
            if (dnChars1.indexOf(value.charAt(0)) != -1) sb.append('\\');
            sb.append(value.charAt(0));
        }
        for (int i = 1; i < n; i++) {
            if (dnChars.indexOf(value.charAt(i)) != -1) sb.append('\\');
            sb.append(value.charAt(i));
        }
        if (n >= 0) {
            if (dnChars2.indexOf(value.charAt(n)) != -1) sb.append('\\');
            sb.append(value.charAt(n));
        }
        return sb.toString();
    }

    public static final char toHex(int number) {
        return Character.toUpperCase(Character.forDigit(number, 16));
    }

    public static final String toHexString(byte bytes[]) {
        StringBuilder builder = new StringBuilder();
        for (int index = 0; index < bytes.length; index++) {
            builder.append(Support.toHex((bytes[index] & 0xf0) >>> 4)).append(Support.toHex(bytes[index] & 0xf));
        }
        return builder.toString();
    }

    public static final byte fromHex(char hex) {
        return (byte)(Character.digit(hex, 16) & 0xff);
    }

    public static final byte[] fromHexString(final String hexString) {
        final int length = hexString.length();
        final byte[] bytes = new byte[length];
        for (int index = 0; index < length; index += 2) {
            bytes[index] = (byte)(((Support.fromHex(hexString.charAt(index)) & 0xff)) << 4
                                  | (Support.fromHex(hexString.charAt(index)) & 0xf));
        }
        return bytes;
    }

    public static String escapeFilter(String value) {
        StringBuffer sb = new StringBuffer();
        int l = value.length();
        for (int i = 0; i < l; i++) {
            char c = value.charAt(i);
            if (filterChars.indexOf(c) == -1) sb.append(c);
            else sb.append('\\').append(toHex(c >> 4 & 0xf)).append(toHex(c & 0xf));
        }
        return sb.toString();
    }

    @SuppressWarnings("fallthrough")
    public static void writeXmlString(Writer writer, String string, boolean semicolon)
        throws IOException {
        int c, l = string.length();
        for (int i = 0; i < l; i++)
            switch (c = string.charAt(i)) {
            case '&':
                writer.write(ampSign);
                break;
            case '<':
                writer.write(leftSign);
                break;
            case '>':
                writer.write(rightSign);
                break;
            case '"':
                writer.write(quotSign);
                break;
            case ';':
                if (semicolon) {
                    writer.write("&#59;");
                    break;
                }
            default:
                writer.write(c);
            }
    }

    public static final void writeXmlString(Writer writer, String string)
        throws IOException {
        writeXmlString(writer, string, false);
    }

    /** Reads 8-bit character line ending with LF or CRLF.
        @return line read or null if in end of file */
    public static String readLine(InputStream in)
        throws IOException {
        StringBuffer sb = new StringBuffer();
        boolean wasCR = false;
        for (int c;;)
            switch (c = in.read()) {
            case -1:
                return sb.length() > 0 ? sb.toString() : null;
            case '\n':
                return sb.toString();
            case '\r':
                wasCR = true;
                continue;
            default:
                if (wasCR) {
                    sb.append('\r');
                    wasCR = false;
                }
                sb.append((char)c);
            }
    }

    /** Writes string as 8-bit character line. */
    public static void writeBytes(OutputStream out, String str, String charsetName)
        throws IOException {
        if (charsetName == null) charsetName = "8859_1";
        out.write(str.getBytes(charsetName));
    }

    public static void writeBytes(OutputStream out, String str)
        throws IOException {
        writeBytes(out, str, "8859_1");
    }

    public static final int compare(byte bytes1[], byte bytes2[], int off1, int off2, int num) {
        int d, m1 = bytes1.length, m2 = bytes2.length;
        if (num != -1) {
            m1 = Math.min(m1, off1 + num);
            m2 = Math.min(m2, off2 + num);
        } while (off1 < m1 && off2 < m2) {
            if ((d = (bytes1[off1] & 0xff) - (bytes2[off2] & 0xff)) != 0) return d;
            off1++;
            off2++;
        }
        return m1 - m2;
    }

    /** Compares strings and byte arrays from given offset and number.
        @param obj1 gives the first object
        @param obj2 gives the second object
        @param off1 starting offset of first object
        @param off2 starting offset of second object
        @param num number of positions to be compared or -1 if not specified
        @return an integer which indicates by it's sign if the first object is less than, equal to,
        or greater than the second. */
    public static int compare(Object obj1, Object obj2, int off1, int off2, int num) {
        if (obj1 instanceof String && obj2 instanceof String) {
            int d, m1 = ((String)obj1).length(), m2 = ((String)obj2).length();
            if (num != -1) {
                m1 = Math.min(m1, off1 + num);
                m2 = Math.min(m2, off2 + num);
            } while (off1 < m1 && off2 < m2) {
                if ((d = ((String)obj1).charAt(off1) - ((String)obj2).charAt(off2)) != 0) return d;
                off1++;
                off2++;
            }
            return m1 - m2;
        } else if (obj1 instanceof byte[] && obj2 instanceof byte[]) return compare((byte[])obj1, (byte[])obj2, off1, off2, num);
        else if (obj1 instanceof byte[] && obj2 instanceof String) {
            int d, m1 = ((byte[])obj1).length, m2 = ((String)obj2).length();
            if (num != -1) {
                m1 = Math.min(m1, off1 + num);
                m2 = Math.min(m2, off2 + num);
            } while (off1 < m1 && off2 < m2) {
                if ((d = (((byte[])obj1)[off1] & 0xff) - ((String)obj2).charAt(off2)) != 0) return d;
                off1++;
                off2++;
            }
            return m1 - m2;
        } else {
            int d, m1 = ((String)obj1).length(), m2 = ((byte[])obj2).length;
            if (num != -1) {
                m1 = Math.min(m1, off1 + num);
                m2 = Math.min(m2, off2 + num);
            } while (off1 < m1 && off2 < m2) {
                if ((d = ((String)obj1).charAt(off1) - (((byte[])obj2)[off2] & 0xff)) != 0) return d;
                off1++;
                off2++;
            }
            return m1 - m2;
        }
    }

    public static final int compare(byte bytes1[], byte bytes2[]) {
        return compare(bytes1, bytes2, 0, 0, -1);
    }

    /** Compares strings and byte arrays.
        @param obj1 gives the first object
        @param obj2 gives the second object */
    public static int compare(Object obj1, Object obj2) {
        return compare(obj1, obj2, 0, 0, -1);
    }

    /** Replaces all occurences of oldStr to newStr in str.
        @param str string to change
        @param oldStr string to search
        @param newStr string to replace
        @return new changed string. */
    public static String replace(String str, String oldStr, String newStr) {
        int i = 0;
        while (i < str.length() && (i = str.indexOf(oldStr, i)) != -1) {
            str = str.substring(0, i) + newStr + str.substring(i + oldStr.length());
            i += newStr.length();
        }
        return str;
    }

    /** Removes quotation and possible comments from string.
        String may be e.g. of format: literal"quoted\"quoted"literal (comment)
        @param s string to be stripped
        @param comments recognize comments
        @return stripped string */
    public static String stripString(String s, boolean comments) {
        StringBuffer sb = new StringBuffer();
        int commentLevel = 0, l = s.length();
        boolean lastSpace = false, quoted = false;
        for (int i = 0; i < l; i++) {
            int c = s.charAt(i);
            switch (c) {
            case '\t':
            case ' ':
                if (commentLevel == 0 && quoted) break;
                lastSpace = true;
                continue;
            case '"':
                if (commentLevel == 0) quoted = !quoted;
                continue;
            case '\\':
                if (quoted && i < l - 1) c = s.charAt(++i);
                break;
            case '(':
                if (!comments) break;
                commentLevel++;
                continue;
            case ')':
                if (!comments) break;
                if (commentLevel == 0) break;
                commentLevel--;
                continue;
            }
            if (commentLevel > 0) continue;
            if (lastSpace && sb.length() > 0) sb.append(' ');
            lastSpace = false;
            sb.append((char)c);
        }
        return sb.toString();
    }

    /** Unquotes string.
        @param s string to be unquoted
        @return unquoted string */
    public static String unquoteString(String s) {
        return stripString(s, false);
    }

    /** Uncomments string.
        @param s string to be uncommented
        @return uncommented string */
    public static String uncommentString(String s) {
        return stripString(s, true);
    }

    /** Unquotes simple quoted string. */
    public static String unquoteSimpleString(String s) {
        s = s.trim();
        int i = 0;
        while (i < s.length() && s.charAt(i) == '"') i++;
        int j = s.length() - 1;
        while (j > i && s.charAt(j) == '"') j--;
        return i > 0 || j < s.length() - 1 ? s.substring(i, j + 1) : s;
    }

    /** Parses list of form: name=value; name=value... where ; is delimeter
        @param s string to be parsed
        @param table Map where results are stored
        @param delim delimeter characters
        @param toLow lower cases Map keys
        @param simple assumes that values are quoted in simple way (only with one pairs of double quotation characters).
        @param vector store the order of name value pairs in this vector as Assignment-instances. */
    public static void parse(String s, Map<String,String> table, String delim, boolean toLow, boolean simple, Vector<Assignment> vector) {
        StringTokenizer st = new StringTokenizer(s, delim);
        while (st.hasMoreTokens()) {
            StringTokenizer st1 = new StringTokenizer(st.nextToken().trim(), "=");
            if (!st1.hasMoreTokens()) continue;
            String name = st1.nextToken().trim();
            s = st1.hasMoreTokens() ? st1.nextToken("").substring(1).trim() : "";
            String value = simple ? unquoteSimpleString(s) : unquoteString(s);
            if (table != null) table.put(toLow ? name.toLowerCase() : name, value);
            if (vector != null) vector.addElement(new Assignment(name, value, s.startsWith("\"")));
        }
    }

    public static void parse(String s, Map<String,String> table, String delim, boolean toLow, boolean simple) {
        parse(s, table, delim, toLow, simple, null);
    }

    public static void parse(String s, Map<String,String> table, String delim, boolean toLow) {
        parse(s, table, delim, toLow, false, null);
    }

    /** Gets parameters from string of form: leader; name=value; name=value...
        @param s string to be parsed
        @param table Map where parameters are returned
        @param simple assumes that values are quoted in simple way (only with one pairs of double quotation characters).
        @param vector store the order of name value pairs in this vector as Assignment-instances.
        @return first value */
    public static String getParameters(String s, Map<String, String> table, boolean simple, Vector<Assignment> vector) {
        StringTokenizer st = new StringTokenizer(s, ";");
        if (!st.hasMoreTokens()) return "";
        String leader = unquoteString(st.nextToken());
        if (st.hasMoreTokens() && (table != null || vector != null))
            parse(st.nextToken("").substring(1), table, ";", true, simple, vector);
        return leader;
    }

    public static String getParameters(String s, Map<String,String> table, boolean simple) {
        return getParameters(s, table, simple, null);
    }

    public static String getParameters(String s, Map<String,String> table) {
        return getParameters(s, table, false, null);
    }

    public static String getParameters(String s) {
        return getParameters(s, null, false, null);
    }

    /** Gets arguments from string of form: leader name=value, name=value...
        @param s string to be parsed
        @param table Map where arguments are returned
        @param vector store the order of name value pairs in this vector as Assignment-instances.
        @return first value */
    public static String getArguments(String s, Map<String, String> table, Vector<Assignment> vector) {
        StringTokenizer st = new StringTokenizer(s);
        if (!st.hasMoreTokens()) return null;
        String leader = unquoteString(st.nextToken());
        if (st.hasMoreTokens() && (table != null || vector != null))
            parse(st.nextToken("").substring(1), table, ",", true, true, vector);
        return leader;
    }

    public static String getArguments(String s, Map<String, String> table) {
        return getArguments(s, table, null);
    }

    /** Scans HTML tag from input stream.
        @param in input stream where tag is scanned
        @param tag tag which should be scanned
        @param reset if set, resets input stream before scanning to previous mark
        @return TAG_WITHOUT_VALUES (0) if tag was found and has no values, TAG_WITH_VALUES (-2) if tag was found and
        possibly has values, -1 if unexpected end of file was found, otherwise returns character which was not matched. */
    public static final int scanTag(InputStream in, String tag, boolean reset)
        throws IOException {
        if (reset) in.reset();
        int c, l = tag.length();
        in.mark(l + 1);
        for (int i = 0; i < l; i++)
            if ((c = in.read()) == -1 || c == '>') return c;
            else if (Character.toLowerCase((char)c) != tag.charAt(i)) return tag.charAt(i);
        return (c = in.read()) == -1 ? -1 : c == '>' ? TAG_WITHOUT_VALUES : whites.indexOf(c) == -1 ? c : TAG_WITH_VALUES;
    }

    /** Scans values from HTML tag after tag has been found.
        @return -1 if end of stream or character &gt; if found. */
    public static int scanValues(InputStream in, Map<String, String> values)
        throws IOException {
        StringBuffer sb = new StringBuffer();
        boolean eof[] = new boolean[1];
        int c = 0;
        for (;;) {
            while (c != -1 && whites.indexOf(c) != -1) c = in.read();
            if (c == -1 || c == '>') break;
            if (c != '=')
                do sb.append((char)c);
                while ((c = in.read()) != -1 && c != '=' && c != '>' && whites.indexOf(c) == -1);
            String name = sb.toString().toLowerCase();
            sb.setLength(0);
            while (c != -1 && whites.indexOf(c) != -1) c = in.read();
            if (c != '=') {
                values.put(name, "");
                continue;
            }
            if (c == -1 || c == '>') break;
            while ((c = in.read()) != -1 && whites.indexOf(c) != -1);
            if (c == '\'' || c == '"') {
                // Quoted value has been found
                int c1;
                while ((c1 = in.read()) != -1 && c1 != c) {
                    if (c1 == '&') c1 = convert(in, eof);
                    sb.append((char)c1);
                    if (eof[0]) return -1;
                }
                c = 0;
            }
            else
                while (c != -1 && c != '>' && whites.indexOf(c) == -1) {
                    if (c == '&') c = convert(in, eof);
                    sb.append((char)c);
                    if (eof[0]) return -1;
                    c = in.read();
                }
            values.put(name, sb.toString());
            sb.setLength(0);
        }
        return c;
    }

    /** Gets last parameter from multiple parameters by given name. */
    public static String getParameter(Map<String, Object> values, String name) {
        Object obj = values.get(name);
        if (obj instanceof Vector) {
            String value = null;
            Vector vector = (Vector)obj;
            for (int i = vector.size() - 1; i >= 0; i--)
                if (!(value = (String)vector.elementAt(i)).equals("")) return value;
            return value;
        }
        return (String)obj;
    }

    /** Gets last non-empty value from given values. */
    public static String getParameterValue(String values[]) {
        if (values == null) return null;
        for (int i = values.length - 1; i >= 0; i--)
            if (!values[i].equals("")) return values[i];
        return values[0];
    }

    /** Converts HTML-encoded characters to their plain variants. */
    public static int convert(InputStream in, boolean eof[])
        throws IOException {
        eof[0] = false;
        int c;
        in.mark(10);
        if ((c = in.read()) == -1) {
            eof[0] = true;
            return '&';
        }
        boolean decimal;
        if (c == '#') {
            decimal = true;
            if ((c = in.read()) == -1) {
                in.reset();
                return '&';
            }
        } else decimal = false;
        StringBuffer sb = new StringBuffer();
        for (int n = 0; n < 8 && c != ';' && c > 32; n++) {
            sb.append((char)c);
            c = in.read();
        }
        String s = sb.toString();
        if (c != ';') {
            in.reset();
            return '&';
        }
        eof[0] = c == -1;
        c = -1;
        if (decimal)
            try {
                c = Integer.parseInt(s);
            } catch (NumberFormatException ex) {} else {
            Character ch;
            if ((ch = codeTable.get(s)) != null) c = ch.charValue();
        }
        return c;
    }

    /** Filters HTML to plain text. */
    public static void filter(InputStream in, OutputStream out)
        throws IOException {
        int c;
        boolean eof[] = new boolean[1], preformatted = false, quoted = false;
        while ((c = in.read()) != -1)
            if (c == '<') {
                if (quoted) {
                    if ((c = scanTag(in, "/xmp", false)) == TAG_WITH_VALUES || c == TAG_WITHOUT_VALUES) {
                        while (c > -1 && c != '>') c = in.read();
                        if (c == -1) break;
                        quoted = false;
                        continue;
                    }
                    out.write('<');
                    in.reset();
                    continue;
                }
                if ((c = scanTag(in, "!--", false)) != TAG_WITH_VALUES && c != TAG_WITHOUT_VALUES) {
                    if ((c = scanTag(in, "br", true)) == TAG_WITH_VALUES || c == TAG_WITHOUT_VALUES ||
                        (c = scanTag(in, "div", true)) == TAG_WITH_VALUES || c == TAG_WITHOUT_VALUES ||
                        (c = scanTag(in, "hr", true)) == TAG_WITH_VALUES || c == TAG_WITHOUT_VALUES ||
                        (c = scanTag(in, "p", true)) == TAG_WITH_VALUES || c == TAG_WITHOUT_VALUES ||
                        (c = scanTag(in, "/p", true)) == TAG_WITH_VALUES || c == TAG_WITHOUT_VALUES ||
                        (c = scanTag(in, "title", true)) == TAG_WITH_VALUES || c == TAG_WITHOUT_VALUES ||
                        (c = scanTag(in, "/title", true)) == TAG_WITH_VALUES || c == TAG_WITHOUT_VALUES ||
                        (c = scanTag(in, "/tr", true)) == TAG_WITH_VALUES || c == TAG_WITHOUT_VALUES) out.write('\n');
                    else if ((c = scanTag(in, "pre", true)) == TAG_WITH_VALUES || c == TAG_WITHOUT_VALUES) preformatted = true;
                    else if ((c = scanTag(in, "/pre", true)) == TAG_WITH_VALUES || c == TAG_WITHOUT_VALUES) preformatted = false;
                    else if ((c = scanTag(in, "xmp", true)) == TAG_WITH_VALUES || c == TAG_WITHOUT_VALUES) quoted = true;
                    while (c > -1 && c != '>') c = in.read();
                } else while ((c = scanTag(in, "--", false)) >= 0);
                if (c == -1) break;
            } else if (quoted) out.write(c);
            else if (c == '&') {
                if ((c = convert(in, eof)) != -1) out.write(c);
                if (eof[0]) break;
            } else if (c == '\t') for (int n = 4; n > 0; n--) out.write(' ');
            else if (c >= ' ' || preformatted) out.write(c);
    }

    /** Changes string to HTML-format. */
    public static String htmlString(String string) {
        StringBuffer buffer = new StringBuffer();
        int l = string.length();
        for (int i = 0; i < l; i++) {
            int c = string.charAt(i);
            if (c == 34 || c == 38 || c == 60 || c == 62) buffer.append("&#").append(String.valueOf(c)).append(';');
            else if (c == '\n') buffer.append("<br>\n");
            else if (c == '\r') continue;
            else buffer.append((char)c);
        }
        return buffer.toString();
    }

    public static String modifyPath(String s, byte mode) {
        StringBuffer sb = null;
        int l = s.length(), n = 0;
        for (int i = 0; i < l; i++) {
            int c = s.charAt(i);
            if (c == '%' && i + 2 < l && mode != ENCODE) {
                try {
                    c = Integer.parseInt(s.substring(i + 1, i + 3), 16);
                } catch (NumberFormatException ex) {
                    continue;
                }
                if (mode < DECODE && (c <= 0x20 || c >= 0x7f || uriCharType[c - 0x21] != NORMAL)) {
                    i += 2;
                    continue;
                }
                if (sb == null) sb = new StringBuffer();
                if (i > n) sb.append(s.substring(n, i));
                sb.append((char)c);
                n = (i += 2) + 1;
            } else if (mode < DECODE && (c <= 0x20 || c >= 0x7f ||
                                         (mode == CANONIZE ? uriCharType[c - 0x21] == UNWISE :
                                          c != '/' && uriCharType[c - 0x21] != NORMAL))) {
                if (sb == null) sb = new StringBuffer();
                if (i > n) sb.append(s.substring(n, i));
                sb.append('%').append(toHex(c >> 4 & 0xf)).append(toHex(c & 0xf));
                n = i + 1;
            }
        }
        if (sb == null) return s;
        if (l > n) sb.append(s.substring(n));
        return sb.toString();
    }

    public static final String canonizePath(String s) {
        return modifyPath(s, CANONIZE);
    }

    public static final String encodePath(String s) {
        return modifyPath(s, ENCODE);
    }

    public static final String decodePath(String s) {
        return modifyPath(s, DECODE);
    }

    /** Encodes string to encoded words format (rfc2047).
        @param s string to be encoded
        @return the encoded string */
    public static String encodeWords(String s, String charset) {
        int i = 0;
        for (; i < s.length() && s.charAt(i) < 127; i++);
        if (i == s.length()) return s;
        int j = s.length() - 1;
        for (; j > i && s.charAt(j) < 127; j--);
        j++;
        if (charset == null) charset = "ISO-8859-1";
        return s.substring(0, i) + "=?" + charset + "?q?" +
            new QPEncoder(true).encode(s.substring(i, j).getBytes()) + "?=" + s.substring(j);
    }

    public static String encodeWords(String s) {
        return encodeWords(s, null);
    }

    public static String encode(String string, String chars) {
        StringBuffer sb = new StringBuffer();
        int l = string.length();
        for (int i = 0; i < l; i++) {
            char ch = string.charAt(i);
            if (chars == null ? ch < '0' || ch > '9' && ch < 'A' || ch > 'Z' && ch < 'a' || ch > 'z' : chars.indexOf(ch) != -1) sb.append('$').append(toHex(ch >> 12 & 0xf)).append(toHex(ch >> 8 & 0xf)).append(toHex(ch >> 4 & 0xf)).append(toHex(ch & 0xf));
            else sb.append(ch);
        }
        return sb.toString();
    }

    public static String decode(String string) {
        StringBuffer sb = new StringBuffer();
        int i = 0;
        for (int j; (j = string.indexOf('$', i)) != -1; i = j + 5)
            sb.append(string.substring(i, j)).append((char)Integer.parseInt(string.substring(j + 1, j + 5), 16));
        return sb.append(string.substring(i)).toString();
    }

    /** Returns dot input stream where extra dot is removed from byte stuffed lines
        starting with dot and which ends with single dot in new line. */
    public static ConvertInputStream getDotInputStream(InputStream in) {
        return new ConvertInputStream(new ConvertInputStream(in, ".\r\n", "\r", true, true), "..", ".", "\r", true);
    }

    /** Returns dot output stream extra dot is prepended to lines beginning with dots. */
    public static ConvertOutputStream getDotOutputStream(OutputStream out) {
        return new ConvertOutputStream(out, ".", "..", "\r", true);
    }

    /** Returns text input stream where CRLFs are replaced with LFs. */
    public static ConvertInputStream getTextInputStream(InputStream in) {
        return new ConvertInputStream(in, "\r\n", "\n");
    }

    /** Returns text output stream where individual LFs are replaced with CRLFs. */
    public static ConvertOutputStream getTextOutputStream(OutputStream out) {
        return new ConvertOutputStream(out, "\n", "\r\n", true);
    }

    /** Returns simple text output stream where CRLFs are replaced with LFs. */
    public static ConvertOutputStream getSimpleOutputStream(OutputStream out) {
        return new ConvertOutputStream(out, "\r\n", "\n");
    }

    /** Returns telnet input stream. */
    public static ConvertInputStream getTelnetInputStream(InputStream in) {
        return new ConvertInputStream(new ConvertInputStream(in, "\r\n", "\n"), "\r\0", "\r");
    }

    /** Returns telnet output stream. */
    public static ConvertOutputStream getTelnetOutputStream(OutputStream out) {
        return new ConvertOutputStream(new ConvertOutputStream(out, "\n", "\r\n"), "\r", "\r\0");
    }

    /** Converts text output stream to HTML-stream where possible URLs are converted to links.
        Zero byte can be used to indicate end of stream so that conversion is finished. */
    public static OutputStream getHtmlOutputStream(OutputStream out, Map<String, String> servletPathTable, String charsetName) {
        return new HtmlOutputStream(new LinkOutputStream(new LinkOutputStream(new LinkOutputStream(new LinkOutputStream(new LinkOutputStream(new LinkOutputStream(new LinkOutputStream(new LinkOutputStream(new LinkOutputStream(new LinkOutputStream(new LinkOutputStream(new LinkOutputStream(new ConvertOutputStream(new ConvertOutputStream(new ConvertOutputStream(new ConvertOutputStream(new ConvertOutputStream(out, "\0"), "\n", "<br>\n"), "\r\n", "\n"), "\t", "&nbsp;&nbsp;&nbsp;&nbsp;"), "  ", "&nbsp;"), "file:", charsetName, servletPathTable), "ftp:", charsetName, servletPathTable), "http:", charsetName, servletPathTable), "https:", charsetName, servletPathTable), "gopher:", charsetName, servletPathTable), "mailto:", charsetName, servletPathTable), "news:", charsetName, servletPathTable), "snews:", charsetName, servletPathTable), "telnet:", charsetName, servletPathTable), "wais:", charsetName, servletPathTable), "www.", "http://", charsetName), "ftp.", "ftp://", charsetName), charsetName);
    }

    @SuppressWarnings("fallthrough")
    public static void convertVCard2Html(InputStream in, OutputStream out, OutputStream htmlOut, String charsetName)
        throws IOException {
        Support.writeBytes(out, "<table border=\"1\">\n", charsetName);
        for (String line; (line = Support.readLine(in)) != null;) {
            String group = null, name = null;
            Vector<String> paramVector = null, values = null;
            Vector<Vector<String>> params = new Vector<Vector<String>>();
            StringTokenizer st = new StringTokenizer(line, vCardTokens, true);
            for (int type = 0; st.hasMoreTokens();) {
                String token = st.nextToken();
                if (token.length() > 1 || vCardTokens.indexOf(token.charAt(0)) == -1) {
                    switch (type) {
                    case '.':
                        group = name;
                        name = token;
                        break;
                    case ';':
                        if (values != null) {
                            values.addElement(token);
                            break;
                        }
                    case ',':
                        paramVector = new Vector<String>();
                        paramVector.addElement(token);
                        params.addElement(paramVector);
                        break;
                    case '=':
                        if (paramVector == null) break;
                        paramVector.addElement(token);
                        break;
                    case ':':
                        values.addElement(token);
                        break;
                    default:
                        name = token;
                    }
                    type = 0;
                } else {
                    int newType = token.charAt(0);
                    if (newType == ';') {
                        if ((type == ':' || type == ';') && values != null) values.addElement("");
                    } else if (newType == ':' && st.hasMoreTokens()) {
                        st = new StringTokenizer(st.nextToken(""), ";", true);
                        values = new Vector<String>();
                    }
                    type = newType;
                }
            }
            if (name == null || values == null) continue;
            Decoder decoder = null;
            Support.writeBytes(out, "<tr><th>", charsetName);
            Support.writeBytes(htmlOut, name + "\0\0", charsetName);
            htmlOut.flush();
            Iterator iter = params.iterator();
            while (iter.hasNext()) {
                Iterator iter1 = ((Vector)iter.next()).iterator();
                String paramName = (String)iter1.next();
                Support.writeBytes(out, "\n<br><u>", charsetName);
                Support.writeBytes(htmlOut, paramName + "\0\0", charsetName);
                htmlOut.flush();
                Support.writeBytes(out, "</u>", charsetName);
                String paramValue = null;
                if (iter1.hasNext()) paramValue = (String)iter1.next();
                if (paramName.equalsIgnoreCase("encoding")) {
                    String encoding = paramValue.toUpperCase();
                    if (encoding.equals("QUOTED-PRINTABLE")) decoder = new QPDecoder();
                    else if (encoding.equals("B")) decoder = new BASE64Decoder();
                }
                if (paramValue != null)
                    for (;;) {
                        Support.writeBytes(out, "<br>\n", charsetName);
                        Support.writeBytes(out, "<i>", charsetName);
                        Support.writeBytes(htmlOut, paramValue + "\0\0", charsetName);
                        htmlOut.flush();
                        Support.writeBytes(out, "</i>", charsetName);
                        if (!iter1.hasNext()) break;
                        paramValue = (String)iter1.next();
                    }
            }
            Support.writeBytes(out, "</th><td>", charsetName);
            boolean checkMailto = name.equalsIgnoreCase("email");
            iter = values.iterator();
            for (boolean started = false; iter.hasNext(); started = true) {
                if (started) Support.writeBytes(out, "<br>\n", charsetName);
                String value = (String)iter.next();
                if (decoder != null) {
                    byte b[] = decoder.decodeStream(value);
                    value = new String(b);
                }
                if (checkMailto && !value.regionMatches(true, 0, "mailto:", 0, 7)) value = "mailto:" + value;
                Support.writeBytes(htmlOut, value + "\0\0", charsetName);
                htmlOut.flush();
            }
            Support.writeBytes(out, "</td></tr>\n", charsetName);
        }
        Support.writeBytes(out, "</table>\n", charsetName);
    }

    public static String getCharacterEncoding(String charset) {
        charset = charset.toUpperCase();
        String characterEncoding = characterEncodings.get(charset);
        return characterEncoding != null ? characterEncoding : charset.startsWith("ISO-") ? charset.substring(4).replace('-', '_') : charset;
    }

    /** Decodes string from encoded words format (rfc2047).
        @param s string to be decoded
        @return the decoded string */
    public static String decodeWords(String s)
        throws IOException {
        BASE64Decoder b64d = new BASE64Decoder();
        QPDecoder qpd = new QPDecoder();
        StringBuffer sb = new StringBuffer();
        int i = 0, j;
        while (i < s.length() && (j = s.indexOf("=?", i)) != -1) {
            int k = j + 2, l;
            if ((l = s.indexOf('?', k)) == -1) break;
            String charset = s.substring(k, l).trim();
            if ((l = s.indexOf('?', k = l + 1)) == -1) break;
            String encoding = s.substring(k, l).trim().toUpperCase();
            if ((l = s.indexOf("?=", k = l + 1)) == -1) break;
            String encodedText = s.substring(k, l);
            sb.append(s.substring(i, j));
            i = l + 2;
            byte b[] = null;
            if (encoding.equals("B")) b = b64d.decodeStream(encodedText);
            else if (encoding.equals("Q")) b = qpd.decodeStream(encodedText);
            if (b != null) {
                String characterEncoding = getCharacterEncoding(charset), t;
                try {
                    t = new String(b, characterEncoding);
                } catch (UnsupportedEncodingException ex) {
                    t = new String(b);
                }
                sb.append(t);
            } else sb.append(new String(encodedText));
        }
        if (i < s.length()) sb.append(s.substring(i));
        s = sb.toString();
        return unquoteString(s);
    }

    public static Decoder getDecoder(HeaderList headerList, HeaderList headerList1) {
        String cte = headerList.getHeaderValue("content-transfer-encoding");
        if (cte == null) return null;
        Decoder decoder = null;
        String value = cte.trim().toLowerCase();
        if (value.equals("base64")) decoder = new BASE64Decoder();
        else if (value.equals("quoted-printable")) decoder = new QPDecoder();
        else if (value.equals("uuencode") || value.equals("x-uuencode")) decoder = new UUDecoder();
        if (decoder != null || value.equals("7bit") || value.equals("8bit") || value.equals("binary")) return decoder;
        headerList1.append(new Header("Content-Transfer-Encoding", cte));
        return decoder;
    }

    /** Gets appropriate fields from given message header and copies them in to the second. */
    public static Decoder getHeader(HeaderList headerList, HeaderList headerList1)
        throws IOException {
        Iterator items = headerList.iterator();
        while (items.hasNext()) {
            Header header = (Header)items.next();
            String name = header.getName(), value = header.getValue(), comp = name.toLowerCase();
            if (comp.equals("date")) {
                StringTokenizer st = new StringTokenizer(value, ";");
                if (st.hasMoreTokens() && (value = format(parse(st.nextToken().trim()))) != null)
                    headerList1.append(new Header("Last-Modified", value));
            } else if (!comp.equals("content-transfer-encoding") && !comp.equals("content-type") &&
                       !comp.equals("content-length") && !comp.equals("lines")) headerList1.append(header);
        }
        return getDecoder(headerList, headerList1);
    }

    public static void writeHeader(OutputStream out, OutputStream htmlOut, HeaderList headerList, String charsetName, boolean html)
        throws IOException {
        if (html) {
            writeBytes(out, "<table border=0 cellpadding=0 cellspacing=0 cols=2>\n", charsetName);
            Iterator items = headerList.iterator();
            while (items.hasNext()) {
                Header header = (Header)items.next();
                writeBytes(out, "<tr><td>", charsetName);
                if (header.getName().length() > 0) {
                    writeBytes(out, "<b>", charsetName);
                    writeBytes(htmlOut, header.getName() + "\0\0", charsetName);
                    htmlOut.flush();
                    writeBytes(out, ":</b>", charsetName);
                }
                writeBytes(out, "</td><td>", charsetName);
                writeBytes(htmlOut, decodeWords(header.getValue()) + "\0\0", charsetName);
                htmlOut.flush();
                writeBytes(out, "</td></tr>\n", charsetName);
            }
            writeBytes(out, "</table><br>\n", charsetName);
            return;
        }
        Iterator items = headerList.iterator();
        while (items.hasNext()) {
            Header header = (Header)items.next();
            String name = header.getName();
            int n = 28 - name.length();
            if (name.length() > 0) writeBytes(out, name + ":", charsetName);
            else n++;
            for (; n > 0; n--) out.write(' ');
            out.write(' ');
            writeBytes(out, decodeWords(header.getValue()) + "\n", charsetName);
        }
        out.write('\n');
    }

    static void convertContentIds(InputStream in, OutputStream out, String charsetName)
        throws IOException {
        // Searches all cid: (content ID) links from HTML-page and removes the prefix to be usable in browsers
        boolean quoted = false;
        int c;
        while ((c = in.read()) != -1) {
            if (c == '<') {
                if (!quoted) {
                    String name = null, tag = null;
                    if ((c = Support.scanTag(in, tag = "a", false)) == Support.TAG_WITH_VALUES ||
                        (c = Support.scanTag(in, tag = "area", true)) == Support.TAG_WITH_VALUES) name = "href";
                    else if ((c = Support.scanTag(in, tag = "body", true)) == Support.TAG_WITH_VALUES) name = "background";
                    else if ((c = Support.scanTag(in, tag = "img", true)) == Support.TAG_WITH_VALUES ||
                             (c = Support.scanTag(in, tag = "frame", true)) == Support.TAG_WITH_VALUES) name = "src";
                    else if ((c = Support.scanTag(in, "xmp", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) quoted = true;
                    else if ((c = Support.scanTag(in, "!--", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) {
                        // comment
                        for (;;) {
                            in.mark(3);
                            if ((c = Support.scanTag(in, "--", false)) < 0) break;
                            in.reset();
                            if ((c = in.read()) == -1) break;
                            out.write(c);
                        }
                        if (c == -1) break;
                        continue;
                    }
                    if (name != null) {
                        Map<String, String> values = new HashMap<String, String>();
                        c = Support.scanValues(in, values);
                        String link = values.get(name);
                        while (c > -1 && c != '>') c = in.read();
                        if (link != null) {
                            if (link.regionMatches(true, 0, "cid:", 0, 4)) link = encode(link.substring(4), null);
                            values.put(name, link);
                            Support.writeBytes(out, "<" + tag, charsetName);
                            Iterator iter = values.entrySet().iterator();
                            while (iter.hasNext()) {
                                Map.Entry entry = (Map.Entry)iter.next();
                                Support.writeBytes(out, " " + entry.getKey() + "=", charsetName);
                                Support.writeBytes(out, "\"" + htmlString((String)entry.getValue()) + "\"", charsetName);
                            }
                            Support.writeBytes(out, ">", charsetName);
                            if (c == -1) break;
                            continue;
                        }
                    }
                } else if ((c = Support.scanTag(in, "/xmp", false)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) quoted = false;
                in.reset();
                c = '<';
            }
            out.write(c);
        }
    }

    /** Decodes MIME message body recursively. See rfc2046.
        @param in input stream containing message body
        @param out plain output stream or null if message is saved entirely in cache
        @param htmlOut html output stream (converts ordinary text to html-text)
        @param headerList original message header
        @param modifiedHeaderList modified message header
        @param decoder character decoder or null
        @param path path to source
        @param entity cache entity
        @param html flags if message can be decoded to html
        @param showHtml html-attachments can be shown in result
        @param related flags if this message is of type multipart/related */
    public static void decodeBody(InputStream in,
                                  OutputStream out,
                                  OutputStream htmlOut,
                                  HeaderList headerList,
                                  HeaderList modifiedHeaderList,
                                  Decoder decoder,
                                  String charsetName,
                                  String path,
                                  EntityMemory entity,
                                  boolean html,
                                  boolean showHtml,
                                  boolean related,
                                  ArrayList<String> relatedHtml)
        throws IOException, ParseException {
        Map<String, String> ctParams = null;
        String ct = headerList.getHeaderValue("content-type"), ct0 = null;
        if (ct != null) {
            ct0 = getParameters(ct, ctParams = new HashMap<String, String>()).toLowerCase();
            String start = ctParams.get("start");
            if (ct0.startsWith("multipart/"))
                if (entity instanceof EntityArchive && ((EntityArchive)entity).jarStream == null) {
                    related = related || ct0.equals("multipart/related");
                    int n = entity.getNumberOfElements();
                    for (int i = 1; i <= n; i++) {
                        if ((in = entity.load(String.valueOf(i), headerList = new HeaderList())) == null) continue;
                        modifiedHeaderList = new HeaderList();
                        decoder = getHeader(headerList, modifiedHeaderList);
                        if (start != null) {
                            String contentID = modifiedHeaderList.getHeaderValue("content-id");
                            if (contentID != null && start.equals(contentID)) modifiedHeaderList.removeAll("content-id");
                        }
                        decodeBody(in, out, htmlOut, headerList, modifiedHeaderList, decoder, charsetName, path, entity, html, showHtml, related, null);
                    }
                    return;
                } else {
                    related = related || ct0.equals("multipart/related");
                    String boundary = ctParams.get("boundary");
                    if (boundary == null) throw new IOException("No boundary in multipart content");
                    boundary = "--" + boundary;
                    ConvertInputStream in1 = new ConvertInputStream(in, boundary, "\r", true, true);
                    boundary = "\r\n" + boundary;
                    while (in1.read() != -1);
                    relatedHtml = related ? new ArrayList<String>() : null;
                    for (;;) {
                        int c;
                        if ((c = in.read()) == '-' && (c = in.read()) == '-') break;
                        while (c != -1 && c != '\n') c = in.read();
                        if (c == -1) break;
                        in1 = new ConvertInputStream(in, boundary, false, true);
                        modifiedHeaderList = new HeaderList();
                        decoder = getHeader(headerList = new HeaderList(in1), modifiedHeaderList);
                        if (start != null) {
                            String contentID = modifiedHeaderList.getHeaderValue("content-id");
                            if (contentID != null && start.equals(contentID)) modifiedHeaderList.removeAll("content-id");
                        }
                        if (in1.isEnded()) continue;
                        decodeBody(in1, out, htmlOut, headerList, modifiedHeaderList, decoder, charsetName, path, entity, html, showHtml, related, relatedHtml);
                    }
                    if (relatedHtml != null) {
                        Iterator<String> relatedHtmls = relatedHtml.iterator();
                        while (relatedHtmls.hasNext()) {
                            String entryName = relatedHtmls.next();
                            InputStream entityIn = entity.load(entryName, headerList = new HeaderList());
                            if (entityIn != null)
                                convertContentIds(entityIn, out, charsetName);
                        }
                    } while (in.read() != -1);
                    return;
                }
            if (ct0.equals("message/rfc822")) {
                HeaderList headerList1 = new HeaderList();
                decoder = getHeader(headerList = new HeaderList(in), headerList1);
                if (out != null) writeHeader(out, htmlOut, headerList, charsetName, html);
                decodeBody(in, out, htmlOut, headerList, headerList1, decoder, charsetName, path, entity, html, showHtml, related, null);
                return;
            }
            if (ct0.equals("message/external-body")) {
                HeaderList headerList1 = new HeaderList();
                decoder = getHeader(headerList = new HeaderList(in), headerList1);
                decodeBody(in, null, null, headerList, headerList1, decoder, charsetName, path, entity = new EntityMemory(), false, false, related, null);
                String href = null, accessType = ctParams.get("access-type"), name = ctParams.get("name");
                if (accessType == null || name == null) return;
                accessType = accessType.trim().toLowerCase();
                if (accessType.equals("anon-ftp") || accessType.equals("ftp") || accessType.equals("tftp")) {
                    String site = ctParams.get("site"), directory = ctParams.get("directory"), mode = ctParams.get("mode");
                    if (site == null) return;
                    href = "ftp://" + site + (directory != null ? directory : "/") + encodePath(name) +
                        (mode != null ? "?mode=" + URLEncoder.encode(mode, charsetName) : "");
                } else if (accessType.equals("local-file")) href = "file:///" + encodePath(name);
                else if (accessType.equals("mail-server")) {
                    String server = ctParams.get("server"), subject = ctParams.get("subject");
                    if (server == null) return;
                    InputStream entityIn = entity.load(String.valueOf(1), headerList = new HeaderList());
                    ByteArrayOutputStream bout = new ByteArrayOutputStream();
                    byte b[] = new byte[bufferLength];
                    for (int n; (n = entityIn.read(b)) > 0;) bout.write(b, 0, n);
                    href = "mailto:listserver@" + server + "?body=" + URLEncoder.encode(new String(bout.toByteArray()), charsetName) +
                        (subject != null ? "subject=" + URLEncoder.encode(subject, charsetName) : "");
                }
                return;
            }
        }
        String item = null;
        OutputStream someOut = html ? htmlOut : out;
        String charset = ctParams != null ? ctParams.get("charset") : null,
            contentCharsetName = charset != null ? getCharacterEncoding(charset) : "ISO-8859-1";
        if (out != null && contentCharsetName != null
            && (contentCharsetName.equals("WINDOWS-1251") || contentCharsetName.equals("KOI8-R")))
            out = new WriterOutputStream(new CyrillicWriter(new OutputStreamWriter(out, contentCharsetName)), contentCharsetName);
        if (someOut != null
            && !modifiedHeaderList.hasHeader("content-transfer-encoding") && decoder == null
            && (ct0 == null || ct0.equals("text/plain") || ct0.equals("text"))) {
            for (;;) {
                String s;
                in.mark(256);
                if ((s = readLine(in)) == null) break;
                if (s.startsWith("begin "))
                    try {
                        int i;
                        Integer.parseInt(s.substring(6, i = s.indexOf(' ', 6)));
                        item = s.substring(i + 1).trim();
                        decoder = new UUDecoder();
                        in.reset();
                        ct0 = null;
                        break;
                    } catch (Exception ex) {} else if (s.startsWith("=ybegin"))
                    try {
                        decoder = new YEncodeDecoder();
                        in.reset();
                        ct0 = null;
                        break;
                    } catch (Exception ex) {}
                writeBytes(someOut, s + "\n", charsetName);
            }
        }
        if (item == null)
            if (ctParams != null && (item = ctParams.get("name")) != null) item = decodeWords(item);
            else if ((item = headerList.getHeaderValue("content-disposition")) != null) {
                Map<String, String> params = new HashMap<String, String>();
                getParameters(item, params, true);
                if ((item = params.get("filename")) != null)
                    item = decodeWords(item.substring(Math.max(item.lastIndexOf('/'), item.lastIndexOf('\\')) + 1));
            } else if (ct0 == null && (item != null || (item = headerList.getHeaderValue("subject")) != null)) {
                FileNameMap fnm = URLConnection.getFileNameMap();
                if (fnm != null) {
                    StringBuffer buffer = new StringBuffer();
                    String s = decodeWords(item);
                    int length = s.length();
                    for (int index = 0; index < length && (index = s.indexOf('.', index)) != -1; index++) {
                        while (++index < length && Character.isLetter(s.charAt(index)))
                            buffer.append(s.charAt(index));
                        String contentType = fnm.getContentTypeFor("." + buffer.toString());
                        if (contentType != null) {
                            ct0 = contentType;
                            break;
                        }
                        buffer.setLength(0);
                    }
                }
            }
        if ((ct0 == null || ct0.equals(octetStreamType) || ct0.equals(unknownType)) && item != null) {
            FileNameMap fnm = URLConnection.getFileNameMap();
            if (fnm != null) {
                String contentType;
                int i = item.lastIndexOf('.');
                if (i != -1 && (contentType = fnm.getContentTypeFor(item.substring(i))) != null) ct0 = contentType;
            }
        }
        if (decoder != null) in = new DecoderInputStream(in, decoder);
        boolean isBuffered = false;
        if (ct0 == null || ct0.equals(octetStreamType)) {
            in = new BufferedInputStream(in);
            isBuffered = true;
            String ct1 = URLConnection.guessContentTypeFromStream(in);
            if (ct1 != null) ct0 = ct1;
        }
        if (decoder == null && ct0 == null
            || ct0 != null && (ct0.toLowerCase().startsWith("text/") || ct0.equals("text"))) {
            in = getTextInputStream(in);
            if (contentCharsetName != null && !contentCharsetName.equals(charsetName))
                in = new EncodingConversionInputStream(new InputStreamReader(in, contentCharsetName), charsetName);
            isBuffered = false;
        }
        if (!isBuffered) in = new BufferedInputStream(in);
        String contentID = modifiedHeaderList.getHeaderValue("content-id");
        byte b[] = new byte[bufferLength];
        boolean isEnriched = false, isHtml = ct0 != null && ct0.equals("text/html"), isVCard = false;
        if ((related || contentID != null) && isHtml ||
            ct0 == null && decoder != null ||
            ct0 != null && !ct0.equals("text/plain") && (!showHtml || !isHtml) &&
            !(isEnriched = ct0.equals("text/enriched")) && !(isVCard = ct0.equals("text/x-vcard")) &&
            !ct0.equals("text") && !ct0.startsWith("message/") || out == null) {
            // If message part is not writable in html or has html with related attachments, it is used from cache
            if (html || out == null) {
                if (ct0 != null) {
                    int semicolon = ct != null ? ct.indexOf(';') : -1;
                    modifiedHeaderList.replace(new Header("Content-Type", ct0 + (semicolon != -1 ? ct.substring(semicolon) : "")));
                }
                modifiedHeaderList.remove("content-disposition");
                modifiedHeaderList.remove("content-length");
                modifiedHeaderList.remove("lines");
                modifiedHeaderList.remove("subject");
                String entryName = null;
                if (contentID != null) {
                    if ((entryName = decodeWords(contentID.trim())).startsWith("<") && entryName.endsWith(">")) entryName = entryName.substring(1, entryName.length() - 1);
                } else if (entity instanceof EntityArchive) entryName = item;
                if (entryName != null) entryName = encode(entryName, null);
                OutputStream entityOut = entity.save(entryName, modifiedHeaderList, true);
                if (entityOut != null) {
                    // Message part is saved to cache
                    try {
                        for (int n; (n = in.read(b)) > 0;) entityOut.write(b, 0, n);
                    } catch (IOException ex) {
                        entity.closeStream(entityOut);
                        entity.delete(entryName);
                        throw ex;
                    }
                    entity.closeStream(entityOut);
                    entity.setReadOnly();
                }
                // Message part is already in cache, data can be skipped
                else while (in.read(b) > 0);
                if (related && isHtml) relatedHtml.add(entryName);
                if (out != null && !isHtml || (!related || !showHtml)) {
                    // Reference to attachment is written to the html-message
                    String href = path + entity.getEntryName();
                    double sizeKbs = (double)(long)((double)entity.getEntrySize() / 1024.0 * 100.0) / 100.0;
                    if (item != null) href += "/" + encodePath(item);
                    if (ct0 == null || !ct0.startsWith("image/")) {
                        if (ct0 == null) ct0 = unknownType;
                        writeBytes(out, "\n<p><a href=\"" + href + "\">" + ct0 + " (" + sizeKbs + " kb)</a><br>\n", charsetName);
                    } else writeBytes(out, "\n<p><img src=\"" + href + "\"" + (item != null ? " alt=\"" + item.replace("\"", "&quot;") + "\"" : "") + "><br>\n", charsetName);
                }
            } else if (isHtml) filter(in, out);
            else while (in.read(b) > 0);
        } else {
            if (ct0 != null && ct0.equals("message/rfc822")) {
                HeaderList headerList1 = new HeaderList();
                decoder = getHeader(headerList = new HeaderList(in), headerList1);
                if (out != null) writeHeader(out, htmlOut, headerList, charsetName, html);
                decodeBody(in, out, htmlOut, headerList, headerList1, decoder, charsetName, path, entity, html, showHtml, related, null);
            } else if (isHtml)
                if (html)
                    if (related) {
                        convertContentIds(in, out, charsetName);
                        writeBytes(out, html ? "<hr>\n" : "\n", charsetName);
                        someOut = null;
                    } else someOut = out;
                else {
                    filter(in, out);
                    someOut = null;
                } else if (isEnriched)
                if (html) {
                    in = new BufferedInputStream(new Enriched2HtmlInputStream(in, charsetName));
                    someOut = out;
                } else {
                    Enriched2HtmlInputStream.filter(in, out);
                    someOut = null;
                } else if (isVCard)
                if (html) {
                    convertVCard2Html(in, out, htmlOut, charsetName);
                    someOut = null;
                } else someOut = out;
            if (someOut != null) {
                for (int n; (n = in.read(b)) > 0;) someOut.write(b, 0, n);
                if (someOut instanceof WriterOutputStream) someOut.close();
                writeBytes(out, html ? "<hr>\n" : "\n", charsetName);
            }
            out.flush();
        }
    }

    /** Copies first header to second. */
    public static final void copyHeaderList(HeaderList headerList, HeaderList headerList1) {
        Iterator items = headerList.iterator();
        while (items.hasNext()) headerList1.append((Header)items.next());
    }

    /** Sends message header to output stream. */
    public static final void sendHeaderList(OutputStream out, HeaderList headerList, int request)
        throws IOException {
        Iterator items = headerList.iterator();
        if (items.hasNext()) {
            switch (request) {
            case -1:
                items.next();
                break;
            case 1:
                writeBytes(out, ((Header)items.next()).getValue() + "\r\n", null);
                break;
            } while (items.hasNext()) {
                Header header = (Header)items.next();
                if (header.getName().equals("")) continue;
                writeBytes(out, header.getName() + ": " + header.getValue() + "\r\n", null);
            }
        }
        Support.writeBytes(out, "\r\n", null);
        out.flush();
    }

    /** Sends given message header. */
    public static final void sendHeaderList(OutputStream out, HeaderList headerList)
        throws IOException {
        sendHeaderList(out, headerList, 0);
    }

    /** Sends given message header treating first value as the request line. */
    public static final void sendMessage(OutputStream out, HeaderList headerList)
        throws IOException {
        sendHeaderList(out, headerList, 1);
    }

    public static HeaderList readHeaderList(InputStream in, HeaderList headerList)
        throws IOException, ParseException {
        ConvertInputStream cin = new ConvertInputStream(in, "\r\n\r\n", false, true);
        if (headerList == null) {
            for (int c; (c = cin.read()) != -1;);
            return null;
        }
        StringBuffer sb = new StringBuffer();
        for (int c; (c = cin.read()) != -1;) sb.append((char)c);
        headerList.setHeaders(sb.append("\r\n\r\n").toString());
        return headerList;
    }

    public static void skipHeaderList(InputStream in)
        throws IOException, ParseException {
        readHeaderList(in, null);
    }

    public static final String requestPath(String requestURI) {
        int i = requestURI.indexOf('?');
        return i != -1 ? requestURI.substring(0, i).trim() : requestURI;
    }

    public static void log(ServletContext servletContext, PrintWriter logWriter, String msg) {
        if (servletContext != null) servletContext.log(msg);
        else logWriter.println(msg);
    }

    public static boolean initializeDatabase(Connection connection, Reader reader, String dbInitName, ServletContext servletContext, PrintWriter logWriter, boolean verbose)
        throws IOException, SQLException, ParseException {
        boolean executed = false;
        Statement statement = null;
        try {
            statement = connection.createStatement();
            StreamTokenizer sTok = new StreamTokenizer(reader);
            sTok.resetSyntax();
            sTok.wordChars(0, 255);
            sTok.whitespaceChars(';', ';');
            sTok.ordinaryChar('!');
            sTok.commentChar('#');
            while (sTok.nextToken() != sTok.TT_EOF) {
                boolean not;
                if (sTok.ttype == '!') {
                    not = true;
                    sTok.nextToken();
                } else not = false;
                if (sTok.ttype != sTok.TT_WORD) throw new ParseException("Invalid token at line " + sTok.lineno() + " in file " + dbInitName, 0);
                if (sTok.sval.trim().equals("")) continue;
                if (verbose) log(servletContext, logWriter, "Executing statement " + sTok.sval);
                try {
                    statement.execute(sTok.sval);
                    if (verbose) log(servletContext, logWriter, "Statement executed");
                    if (not) break;
                    executed = true;
                } catch (SQLException ex) {
                    if (!not) throw ex;
                }
            }
        } finally {
            try {
                if (statement != null) statement.close();
            } catch (AbstractMethodError er) {} catch (UnsupportedOperationException ex) {} catch (SQLException ex) {}
        }
        return executed;
    }

    public static byte[] readBytes(InputStream in)
        throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte b[] = new byte[bufferLength];
        for (int n; (n = in.read(b)) > 0;) out.write(b, 0, n);
        in.close();
        out.close();
        return out.toByteArray();
    }

    public static String toString(Object object) {
        if (object == null) {
            return "<null>";
        }
        final StringBuffer buffer = new StringBuffer();
        final Class objectClass = object.getClass();
        if (objectClass.isArray()) {
            buffer.append(object.getClass().getComponentType().getName() + "[");
            final int length = java.lang.reflect.Array.getLength(object);
            for (int index = 0; index < length; index++) {
                Object item = java.lang.reflect.Array.get(object, index);
                if (index > 0) {
                    buffer.append(", ");
                }
                buffer.append(Support.toString(item));
            }
            buffer.append("]");
        } else if (object instanceof Iterable) {
            buffer.append(object.getClass().getName() + "{");
            final Iterator elements = ((Iterable)object).iterator();
            boolean started = false;
            while (elements.hasNext()) {
                final Object element = elements.next();
                if (started) {
                    buffer.append(", ");
                } else {
                    started = true;
                }
                buffer.append(Support.toString(element));
            }
            buffer.append("}");
        } else if (object instanceof Map) {
            buffer.append(object.getClass().getName() + "{");
            final Iterator<Map.Entry> entries = ((Map)object).entrySet().iterator();
            boolean started = false;
            while (entries.hasNext()) {
                final Map.Entry entry = entries.next();
                if (started) {
                    buffer.append(", ");
                } else {
                    started = true;
                }
                buffer.append(Support.toString(entry.getKey())).append('=').append(Support.toString(entry.getValue()));
            }
            buffer.append("}");
        } else {
            final Package objectPackage = objectClass.getPackage();
            if (objectPackage != null) {
                final String packageName = objectPackage.getName();
                if (!packageName.startsWith("org.")) {
                    return object.toString();
                }
            }
            buffer.append(object.getClass().getName() + "{");
            boolean started = false;
            final Method methods[] = object.getClass().getMethods();
            for (int index = 0; index < methods.length; index++) {
                boolean hadIs = false;
                final Method method = methods[index];
                final Class declaringClass = method.getDeclaringClass();
                final Package classPackage = declaringClass.getPackage();
                if (classPackage != null) {
                    final String packageName = classPackage.getName();
                    if (!packageName.startsWith("org.")) {
                        continue;
                    }
                }
                final String methodName = method.getName();
                if (method.getParameterTypes().length == 0 &&
                    ((methodName.length() > 3 &&
                      methodName.startsWith("get") &&
                      Character.isUpperCase(methodName.charAt(3))) ||
                     (hadIs = methodName.length() > 2 &&
                      methodName.startsWith("is") &&
                      Character.isUpperCase(methodName.charAt(2))))) {
                    final Class returnType = method.getReturnType();
                    try {
                        final Object returnValue = method.invoke(object);
                        String name = methodName.substring(hadIs ? 2 : 3);
                        name = Character.toLowerCase(name.charAt(0)) + name.substring(1);
                        if (started) buffer.append(", ");
                        buffer.append(name + "=" + Support.toString(returnValue));
                        started = true;
                    } catch (IllegalAccessException ex) {}
                    catch (InvocationTargetException ex) {}
                }
            }
            buffer.append("}");
        }
        return buffer.toString();
    }

    public static void loadLibrary(String name) {
        Error er = null;
        try {
            System.loadLibrary(name);
            return;
        } catch (UnsatisfiedLinkError ule) {
            er = ule;
        }
        String javaLibraryPath = System.getProperty("java.library.path");
        if (javaLibraryPath == null) throw er;
        StringTokenizer tokens = new StringTokenizer(javaLibraryPath, File.pathSeparator);
        while (tokens.hasMoreElements()) {
            String token = tokens.nextToken();
            File file = new File(token, "lib" + name + ".jnilib");
            if (file.exists()) {
                System.load(file.getPath());
                return;
            }
        }
        throw er;
    }

    public static boolean listContains(String list, String item) {
        list = list.trim().toLowerCase();
        int index = list.indexOf(item);
        if (index == -1) return false;
        if (index > 0) {
            char c = list.charAt(index);
            if (!Character.isWhitespace(c) && c != ',') return false;
        }
        index += item.length();
        if (index < list.length()) {
            char c = list.charAt(index);
            if (!Character.isWhitespace(c) && c != ',') return false;
        }
        return true;
    }

    public static boolean deleteDirectory(File directory) {
        if (directory.exists()) {
            File[] files = directory.listFiles();
            for (File file : files)
                if (file.isDirectory()) {
                    if (!Support.deleteDirectory(file)) return false;
                } else if (!file.delete()) return false;
            return directory.delete();
        }
        return true;
    }

}
