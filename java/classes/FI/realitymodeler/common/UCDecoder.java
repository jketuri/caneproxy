
package FI.realitymodeler.common;

import java.io.OutputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.EOFException;
import java.io.IOException;

/**
 */
public class UCDecoder extends Decoder {

    private int sequence;
    private byte tmp[] = new byte[2];
    private CRC16 crc = new CRC16();

    /**
     */
    protected void decodeToken(InputStream in, OutputStream out, int length)
        throws IOException {
        int i, p1, p2, np1, np2;
        byte a = -1, b = -1, c = -1;
        byte high_byte, low_byte;
        byte tmp[] = new byte[3];

        i = in.read(tmp);
        if (i != 3) throw new EOFException();
        for (i = 0; (i < 64) && ((a == -1) || (b == -1) || (c == -1)); i++) {
            if (tmp[0] == (byte)UCEncoder.characterArray[i]) a = (byte) i;
            if (tmp[1] == (byte)UCEncoder.characterArray[i]) b = (byte) i;
            if (tmp[2] == (byte)UCEncoder.characterArray[i]) c = (byte) i;
        }
        high_byte = (byte) (((a & 0x38) << 2) + (b & 0x1f));
        low_byte = (byte) (((a & 0x7) << 5) + (c & 0x1f));
        p1 = 0;
        p2 = 0;
        for (i = 1; i < 256; i = i * 2) {
            if ((high_byte & i) != 0) p1++;
            if ((low_byte & i) != 0) p2++;
        }
        np1 = (b & 32) / 32;
        np2 = (c & 32) / 32;
        if ((p1 & 1) != np1) throw new EncoderException("UCDecoder: High byte parity error.");
        if ((p2 & 1) != np2) throw new EncoderException("UCDecoder: Low byte parity error.");
        out.write(high_byte);
        crc.update(high_byte);
        if (length == 2) {
            out.write(low_byte);
            crc.update(low_byte);
        }
    }

    private ByteArrayOutputStream lineAndSeq = new ByteArrayOutputStream(2);

    /**
     */
    protected void decodeStreamPrefix(InputStream in, OutputStream out) {
        sequence = 0;
    }

    /**
     */
    protected int decodeSequencePrefix(InputStream in, OutputStream out)
        throws IOException {
        int i;
        int nLen, nSeq;
        byte xtmp[];
        int c;

        crc.value = 0;
        for (;;) {
            c = in.read(tmp, 0, 1);
            if (c == -1) throw new EOFException();
            if (tmp[0] == '*') break;
        }
        lineAndSeq.reset();
        decodeToken(in, lineAndSeq, 2);
        xtmp = lineAndSeq.toByteArray();
        nLen = xtmp[0] & 0xff;
        nSeq = xtmp[1] & 0xff;
        if (nSeq != sequence) throw new EncoderException("UCDecoder: Out of sequence line.");
        sequence = (sequence + 1) & 0xff;
        return nLen;
    }

    /**
     */
    protected void decodeSequenceSuffix(InputStream in, OutputStream out)
        throws IOException {
        int i;
        int lineCRC = crc.value;
        int readCRC;
        byte tmp[];

        lineAndSeq.reset();
        decodeToken(in, lineAndSeq, 2);
        tmp = lineAndSeq.toByteArray();
        readCRC = ((tmp[0] << 8) & 0xFF00) + (tmp[1] & 0xff);
        if (readCRC != lineCRC) throw new EncoderException("UCDecoder: CRC check failed.");
    }

    protected int bytesInToken() {
        return 2;
    }

    protected int bytesInSequence() {
        return 48;
    }

}
