
package FI.realitymodeler.common;

import java.util.*;

public class IteratorSequence implements Iterator
{
    Iterator iterators[] = null;
    Object element = null;
    Set<Object> passedElements = new HashSet<Object>();
    boolean areUnique = false, hasElement = false;
    int iteratorIndex = 0;

    public IteratorSequence(Iterator iterators[], boolean areUnique)
    {
        this.iterators = iterators;
        this.areUnique = areUnique;
    }
	
    public boolean hasNext()
    {
        if (hasElement) return true;
        while (iteratorIndex < iterators.length) {
            if (iterators[iteratorIndex].hasNext()) {
                element = iterators[iteratorIndex].next();
                if (areUnique) {
                    if (passedElements.contains(element)) continue;
                    passedElements.add(element);
                }
                return hasElement = true;
            }
            iteratorIndex++;
        }
        return false;
    }
	
    public Object next()
    {
        if (!hasNext()) throw new NoSuchElementException();
        hasElement = false;
        return element;
    }

    public void remove() {
        iterators[iteratorIndex].remove();
    }

}
