
package FI.realitymodeler.common;

import java.io.*;
import java.util.*;

public class CyrillicWriter extends FilterWriter {

    static HashMap<String,Character> letters = new HashMap<String,Character>();
    static {
        letters.put("4", new Character('\u0447'));
        letters.put("6", new Character('\u0448'));
        letters.put("6t", new Character('\u0449'));
        letters.put("´", new Character('\u044c'));
        letters.put("`", new Character('\u044c'));
        letters.put("'", new Character('\u044c'));
        letters.put("", new Character('\u044c'));
        letters.put("A", new Character('\u0410'));
        letters.put("B", new Character('\u0411'));
        letters.put("C", new Character('\u0426'));
        letters.put("CH", new Character('\u0427'));
        letters.put("D", new Character('\u0414'));
        letters.put("E", new Character('\u0415'));
        letters.put("F", new Character('\u0424'));
        letters.put("G", new Character('\u0413'));
        letters.put("H", new Character('\u0425'));
        letters.put("I", new Character('\u0418'));
        letters.put("IA", new Character('\u0425'));
        letters.put("IU", new Character('\u042e'));
        letters.put("J", new Character('\u0416'));
        letters.put("K", new Character('\u041a'));
        letters.put("L", new Character('\u041b'));
        letters.put("M", new Character('\u041c'));
        letters.put("N", new Character('\u041d'));
        letters.put("O", new Character('\u041e'));
        letters.put("P", new Character('\u041f'));
        letters.put("Q", new Character('\u041a'));
        letters.put("R", new Character('\u0420'));
        letters.put("S", new Character('\u0421'));
        letters.put("SH", new Character('\u0428'));
        letters.put("SHT", new Character('\u0429'));
        letters.put("T", new Character('\u0422'));
        letters.put("U", new Character('\u0423'));
        letters.put("V", new Character('\u0412'));
        letters.put("W", new Character('\u0412'));
        letters.put("X", new Character('\u0425'));
        letters.put("Y", new Character('\u042e'));
        letters.put("Z", new Character('\u0417'));
        letters.put("a", new Character('\u0430'));
        letters.put("b", new Character('\u0431'));
        letters.put("c", new Character('\u0446'));
        letters.put("ch", new Character('\u0447'));
        letters.put("d", new Character('\u0434'));
        letters.put("e", new Character('\u0435'));
        letters.put("f", new Character('\u0444'));
        letters.put("g", new Character('\u0433'));
        letters.put("h", new Character('\u0445'));
        letters.put("i", new Character('\u0438'));
        letters.put("ia", new Character('\u044f'));
        letters.put("iu", new Character('\u044e'));
        letters.put("j", new Character('\u0436'));
        letters.put("k", new Character('\u043a'));
        letters.put("l", new Character('\u043b'));
        letters.put("m", new Character('\u043c'));
        letters.put("n", new Character('\u043d'));
        letters.put("o", new Character('\u043e'));
        letters.put("p", new Character('\u043f'));
        letters.put("q", new Character('\u043a'));
        letters.put("r", new Character('\u0440'));
        letters.put("s", new Character('\u0441'));
        letters.put("sh", new Character('\u0448'));
        letters.put("sht", new Character('\u0449'));
        letters.put("t", new Character('\u0442'));
        letters.put("u", new Character('\u0443'));
        letters.put("v", new Character('\u0432'));
        letters.put("w", new Character('\u0432'));
        letters.put("x", new Character('\u0445'));
        letters.put("y", new Character('\u044e'));
        letters.put("z", new Character('\u0437'));
    }

    char ca[] = new char[3];
    int cn = 0;

    public CyrillicWriter(Writer writer) {
        super(writer);
    }

    public void write(int c) throws IOException {
        if (cn < ca.length) {
            ca[cn++] = (char)c;
            if (cn < ca.length) return;
        }
        for (int l = cn; l > 0; l--) {
            Character letter = letters.get(new String(ca, 0, l));
            if (letter != null) {
                out.write(letter.charValue());
                cn -= l;
                for (int i = 0; i < cn; i++) ca[i] = ca[i + l];
                return;
            }
        }
        out.write(ca[0]);
        cn--;
        for (int i = 0; i < cn; i++) ca[i] = ca[i + 1];
    }

    public void write(char cbuf[]) throws IOException {
        write(cbuf, 0, cbuf.length);
    }

    public void write(char cbuf[], int off, int len) throws IOException {
        for (int i = off; i < len; i++) write(cbuf[i]);
    }

    public void write(String str) throws IOException {
        write(str, 0, str.length());
    }

    public void write(String str, int off, int len) throws IOException {
        for (int i = off; i < len; i++) write(str.charAt(i));
    }

    public void flush() throws IOException {
        int l;
        for (l = cn; l > 0; l--) {
            Character letter = letters.get(new String(ca, 0, l));
            if (letter != null) {
                out.write(letter.charValue());
                break;
            }
        }
        if (l < cn) out.write(ca, l, cn);
        out.flush();
    }

    public void close() throws IOException {
        flush();
        super.close();
    }

}
