
package FI.realitymodeler.common;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;
import java.io.EOFException;

/**
 */
public class BASE64Decoder extends Decoder {
	
    /**
     */
    private final static char characterArray[] = {
        //		 0   1   2   3   4   5   6   7
        'A','B','C','D','E','F','G','H', // 0
        'I','J','K','L','M','N','O','P', // 1
        'Q','R','S','T','U','V','W','X', // 2
        'Y','Z','a','b','c','d','e','f', // 3
        'g','h','i','j','k','l','m','n', // 4
        'o','p','q','r','s','t','u','v', // 5
        'w','x','y','z','0','1','2','3', // 6
        '4','5','6','7','8','9','+','/'  // 7
    };

    private final static byte characterConvertArray[] = new byte[256];

    static {
        for (int i = 0; i < 255; i++) characterConvertArray[i] = -1;
        for (int i = 0; i < characterArray.length; i++) characterConvertArray[characterArray[i]] = (byte) i;
    }

    private byte decodeBuffer[] = new byte[4];

    /**
     */
    @SuppressWarnings("fallthrough")
    protected void decodeToken(InputStream in, OutputStream out, int length)
        throws java.io.IOException {
        int i;
        byte a = -1, b = -1, c = -1, d = -1;
        if (length < 2) throw new EncoderException("BASE64Decoder: Not enough bytes for an token.");
        do {
            i = in.read();
            if (i == -1) throw new EOFException();
        } while (i == '\n' || i == '\r');
        decodeBuffer[0] = (byte)i;
        i = readFully(in, decodeBuffer, 1, length - 1);
        if (i <= 0) throw new EOFException();
        if (length > 3 && decodeBuffer[3] == '=') length = 3;
        if (length > 2 && decodeBuffer[2] == '=') length = 2;
        switch (length) {
        case 4:
            d = characterConvertArray[decodeBuffer[3] & 0xff];
        case 3:
            c = characterConvertArray[decodeBuffer[2] & 0xff];
        case 2:
            b = characterConvertArray[decodeBuffer[1] & 0xff];
            a = characterConvertArray[decodeBuffer[0] & 0xff];
            break;
        }
        switch (length) {
        case 2:
            out.write((byte)(((a << 2) & 0xfc) | ((b >>> 4) & 3)));
            break;
        case 3:
            out.write((byte) (((a << 2) & 0xfc) | ((b >>> 4) & 3)));
            out.write((byte) (((b << 4) & 0xf0) | ((c >>> 2) & 0xf)));
            break;
        case 4:
            out.write((byte) (((a << 2) & 0xfc) | ((b >>> 4) & 3)));
            out.write((byte) (((b << 4) & 0xf0) | ((c >>> 2) & 0xf)));
            out.write((byte) (((c << 6) & 0xc0) | (d  & 0x3f)));
            break;
        }
    }

    protected int bytesInToken() {
        return 4;
    }

    protected int bytesInSequence() {
        return 72;
    }

}
