
package FI.realitymodeler.common;

import java.io.*;
import java.text.*;
import java.util.*;

/**
 * Class that implements a list of request/response headers.
 * Does not prevent duplicates (headers with the same name <b>or</b>
 * same name and value). Users must check this themselves by 
 * calling either of the <code>hasHeader</code> methods. All
 * header name and value comparisons are case-insensitive.
 */
public class HeaderList
    implements Cloneable, Map<String, List<String>> {

    class Entry
        implements Map.Entry<String, List<String>> {
        String key;
        List<String> value;

        Entry(String key, List<String> value) {
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public List<String> getValue() {
            return value;
        }

        public List<String> setValue(List<String> value) {
            List<String> oldValue = value;
            this.value = value;
            return oldValue;
        }

        public boolean equals(Object o) {
            return ((Entry)o).getKey().equals(key) &&
                ((Entry)o).getValue().equals(value);
        }

    }

    // List of headers, should be always accessed via the getList() method.
    protected List<Header> m_list = new ArrayList<Header>();

    /**
     * Constructs an empty header list.
     */
    public HeaderList() {
    }

    public HeaderList(InputStream in) throws IOException, ParseException {
        parseHeaders(in, this, false);
    }

    /**
     * Constructs a list of headers. The headers are given as a string
     * with the headers separated by CRLF. If the headers contain
     * a line with only CRLF on it, it is assumed to mark the
     * end of headers.
     *
     * @param headers a string containing all the headers
     */
    public HeaderList(String headers) throws IOException, ParseException {
        setHeaders(headers);
    }

    public Object clone() {
        HeaderList headerList = new HeaderList();
        headerList.m_list = new ArrayList<Header>();
        headerList.m_list.addAll(m_list);
        return headerList;
    }

    public String toString() {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        try {
            dump(bout);
            return new String(bout.toByteArray());
        } catch (IOException ex) {
            return null;
        } finally {
            try {
                bout.close();
            } catch (IOException ex) {}
        }
    }

    /**
     * Returns the header list member variable (m_list).
     * <strong>This method should be always used instead
     * of using the m_list member variable directly.</strong>
     */
    public final List<Header> getList() {
        return m_list;
    }

    /**
     * Returns the number of headers in this list.
     *
     * @return an integer specifying the number of headers in this list
     */
    public int size() {
        return getList().size();
    }

    public boolean isEmpty() {
        return getList().isEmpty();
    }

    public boolean containsKey(Object key) {
        Iterator items = getList().iterator();
        while (items.hasNext())
            if (((String)key).equalsIgnoreCase(((Header)items.next()).getName())) return true;
        // Not found
        return false;
    }

    public boolean containsValue(Object value) {
        Iterator items = getList().iterator();
        while (items.hasNext())
            if (((String)value).equalsIgnoreCase(((Header)items.next()).getValue())) return true;
        // Not found
        return false;
    }

    public String get(String key) {
        Iterator items = getList().iterator();
        while (items.hasNext()) {
            Header header = (Header)items.next();
            if (key.equalsIgnoreCase(header.getName())) return header.getValue();
        }
        return null;
    }

    public List<String> put(String key, List<String> values) {
        boolean endOfValues = false;
        List<String> oldValues = null;
        Iterator<Header> items = getList().iterator();
        Iterator<String> valueItems = values.iterator();
        while (items.hasNext()) {
            Header header = items.next();
            if (key.equalsIgnoreCase(header.getName())) {
                if (oldValues == null)
                    oldValues = new ArrayList<String>();
                oldValues.add(header.getValue());
                if (!endOfValues) {
                    if (valueItems.hasNext()) {
                        String value = valueItems.next();
                        header.setValue(value);
                    } else endOfValues = true;
                }
                if (endOfValues)
                    items.remove();
            }
        }
        if (!endOfValues)
            while (valueItems.hasNext())
                append(new Header(key, valueItems.next()));
        return oldValues;
    }

    public List<String> remove(Object key) {
        List<String> oldValues = null;
        Iterator<Header> items = getList().iterator();
        while (items.hasNext()) {
            Header header = items.next();
            if (((String)key).equalsIgnoreCase(header.getName())) {
                if (oldValues == null)
                    oldValues = new ArrayList<String>();
                oldValues.add(header.getValue());
                items.remove();
            }
        }
        return oldValues;
    }

    public void putAll(Map<? extends String,? extends List<String>> map) {
        Iterator items = map.entrySet().iterator();
        while (items.hasNext()) {
            Map.Entry entry = (Map.Entry)items.next();
            put((String)entry.getKey(), (List<String>)entry.getValue());
        }
    }

    /**
     * Removes all headers from this header list.
     */
    public void clear() {
        getList().clear();
    }

    public Set<String> keySet() {
        Set<String> keys = new HashSet<String>();
        Iterator items = getList().iterator();
        while (items.hasNext()) {
            Header header = (Header)items.next();
            keys.add(header.getName());
        }
        return keys;
    }

    public Collection<List<String>> values() {
        Set<String> keys = new HashSet<String>();
        Collection<List<String>> values = new ArrayList<List<String>>();
        Iterator items = getList().iterator();
        while (items.hasNext()) {
            Header header = (Header)items.next();
            if (keys.contains(header.getName())) continue;
            keys.add(header.getName());
            values.add(get((Object)header.getName()));
        }
        return values;
    }

    public Set<Map.Entry<String,List<String>>> entrySet() {
        Set<String> keys = new HashSet<String>();
        Set<Map.Entry<String,List<String>>> entries = new HashSet<Map.Entry<String,List<String>>>();
        Iterator items = getList().iterator();
        while (items.hasNext()) {
            Header header = (Header)items.next();
            if (keys.contains(header.getName())) continue;
            keys.add(header.getName());
            entries.add(new Entry(header.getName(), get((Object)header.getName())));
        }
        return entries;
    }

    public boolean contains(Object o) {
        Iterator items = getList().iterator();
        while (items.hasNext())
            if (items.next().equals(o)) return true;
        // Not found
        return false;
    }

    public void setHeaders(InputStream in) throws IOException, ParseException {
        parseHeaders(in, this, false);
    }

    /**
     * Sets the headers of this list from a string of headers.
     * The headers in the string should be separated from each
     * other by CRLF.
     */
    public void setHeaders(String headers) throws IOException, ParseException {
        parseHeaders(new ByteArrayInputStream(headers.getBytes()), this, false);
    }

    /**
     * Returns an iterator of the headers in this list.
     *
     * @return an enumeration of headers
     */
    public Iterator<Header> iterator() {
        return getList().iterator();
    }

    /**
     * Gets the first header in the list.
     *
     * @return the first header of the list or null if none
     */
    public Header getFirst() {
        return isEmpty() ? null : getList().get(0);
    }

    /**
     * Gets the last header in the list.
     *
     * @return the first header of the list or null if none
     */
    public Header getLast() {
        return isEmpty() ? null : getList().get(size() - 1);
    }

    public String getHeaderValue(String name) {
        return get(name);
    }

    public Header getHeader(int index) {
        return index < size() ? getList().get(index) : null;
    }

    public void setHeader(int index, Header header) {
        getList().set(index, header);
    }

    /**
     * Gets all headers with the specified name.
     * This method can be used to get all headers if there are
     * multiple headers with the same name.
     *
     * @param name a string stating the name of the headers to get
     * @return an array containing the headers or null if not found
     */
    public Header[] getHeaders(String name) {
        // A list of matching header values
        List<Header> list = new ArrayList<Header>();
        // Find the matching headers
        Iterator items = getList().iterator();
        while (items.hasNext()) {
            Header header = (Header)items.next();
            if (name.equalsIgnoreCase(header.getName())) list.add(header);
        }
        if (list.isEmpty()) return null;
        // Construct an array of headers
        return list.toArray(new Header[list.size()]);
    }

    /**
     * Tells whether the list contains a header with the specified
     * name. There may be more than one matching header.
     *
     * @param name string stating the name of the header to find
     * @return true if found, false if not
     */
    public boolean hasHeader(String name) {
        return containsKey(name);
    }

    /**
     * Tells whether the list contains the specified header.
     * Both the name and value of the header must match.
     *
     * @param header the header to find
     * @return true if found, false if not
     */
    public boolean hasHeader(Header header) {
        return contains(header);
    }

    /**
     * Prepends the given header to beginning of the list.
     *
     * @param newHeader the header to prepend
     */
    public void prepend(Header newHeader) {
        getList().add(0, newHeader);
    }

    /**
     * Appends the given header to end of the list.
     *
     * @param newHeader the header to append
     */
    public void append(Header newHeader) {
        getList().add(newHeader);
    }

    public void insert(Header newHeader, int index) {
        getList().add(index, newHeader);
    }

    /**
     * Replaces first header with same name as given
     * header or appends if not found.
     * @param newHeader header to replace or append
     * @return replaced header or null if not found
     */
    public Header replace(Header newHeader) {
        int size = size();
        List<Header> list = getList();
        String name = newHeader.getName();
        for (int i = 0; i < size; i++) {
            Header header = list.get(i);
            if (name.equalsIgnoreCase(header.getName())) {
                list.set(i, newHeader);
                return header;
            }
        }
        append(newHeader);
        return null;
    }

    /**
     * Inserts the given header right before the given marker header.
     *
     * @param marker the header that will follow the new header
     * in the list
     * @param newHeader the header to insert
     * @exception IllegalArgumentException if the marker header
     * is not found in the list
     */
    public void insertBefore(Header marker, Header newHeader) {
        int pos = getList().indexOf(marker);
        if (pos == -1) throw new IllegalArgumentException("Illegal marker parameter");
        getList().add(pos, newHeader);
    }

    /**
     * Inserts the given header right after the given marker header.
     *
     * @param marker the header that will precede the new header
     * in the list
     * @param newHeader the header to insert
     * @exception IllegalArgumentException if the marker header
     * is not found in the list
     */
    public void insertAfter(Header marker, Header newHeader) {
        int pos = getList().indexOf(marker);
        if (pos == -1) throw new IllegalArgumentException("Illegal marker parameter");
        getList().add(pos + 1, newHeader);
    }

    /**
     * Removes the first header from the list and returns it.
     *
     * @return the removed header, null if the list was empty
     */
    public Header removeFirst() {
        if (isEmpty()) return null;
        return remove(0);
    }

    /**
     * Removes the last header from the list and returns it.
     *
     * @return the removed header, null if the list was empty
     */
    public Header removeLast() {
        if (isEmpty()) return null;
        return remove(getList().size() - 1);
    }

    /**
     * Removes a header from the list. The contents of the <code>
     * header</code> are matched, not the object reference. Uses <code>
     * Header.equals</code> method for comparison, which means that 
     * both the header name and header value must match those of the
     * <code>header</code> argument.
     *
     * @see Header#equals
     * @param header a header object to remove
     * @return true if found, false if not
     */
    public boolean remove(Header header) {
        return getList().remove(header);
    }

    /**
     * Removes a header specified by the name. Removes only
     * the first matching header. If all headers with the
     * specified name are to be removed, use <a href="#removeAll">
     * removeAll</a> instead.
     *
     * @see #removeAll(String)
     * @param name a string stating the name of the header to remove
     * @return the removed header or <b>null</b> if not found
     */
    public Header remove(String name) {
        int size = size();
        List list = getList();
        for (int i = 0; i < size; i++) {
            Header header = (Header)list.get(i);
            if (name.equalsIgnoreCase(header.getName())) {
                list.remove(i);
                return header;
            }
        }
        return null;
    }

    public Header remove(int index) {
        if (index >= size()) return null;
        Header header = getHeader(index);
        getList().remove(index);
        return header;
    }

    public List<String> get(Object key) {
        List<String> values = new ArrayList<String>();
        Iterator items = getList().iterator();
        while (items.hasNext()) {
            Header header = (Header)items.next();
            if (((String)key).equalsIgnoreCase(header.getName()))
                values.add(header.getValue());
        }
        return values;
    }

    /**
     * Removes all headers matching the given name.
     *
     * @param name a string stating the name of the header(s) to remove
     * @return the number of removed headers
     */
    public int removeAll(String name) {
        int count = 0;
        List<Header> list = getList();
        for (int i = size() - 1; i >= 0; i--)
            if (name.equalsIgnoreCase(list.get(i).getName())) {
                list.remove(i);
                count++;
            }
        return count;
    }

    public Map<String,List<String>> getMap() {
        Map<String,List<String>> map = new LinkedHashMap<String,List<String>>();
        Iterator<String> keyItems = keySet().iterator();
        while (keyItems.hasNext()) {
            String key = keyItems.next();
            map.put(key, get((Object)key));
        }
        return map;
    }

    public void dump(OutputStream out) throws IOException {
        Iterator items = iterator();
        while (items.hasNext()) {
            Header header = (Header)items.next();
            if (!header.getName().equals("")) Support.writeBytes(out, header.getName() + ": ", null);
            Support.writeBytes(out, header.getValue() + "\n", null);
        }
        out.write('\n');
        out.flush();
    }

    /**
     * Parses the given input stream of headers into given list.
     * This is a static method. Does not prevent duplicates.
     *
     * @param in input stream of headers separated by CRLF or CR
     * @param headerList the list to receive the parsed headers (Header objects)
     * @param inValue inside value
     * @return last character read
     */
    public static int parseHeaders(InputStream in, HeaderList headerList, boolean inValue) throws IOException, ParseException {
        StringBuffer key = new StringBuffer(), value = new StringBuffer();
        boolean strictHeader0 = false, strictHeader = false;
        int c;
        for (int last = -1; (c = in.read()) != -1; last = c) {
            if (last == '\r' || !strictHeader)
                if (c == '\n') {
                    if (key.length() == 0 && value.length() == 0) return c;
                    StringBuffer whites = null;
                    if ((c = in.read()) == ' ' || c == '\t') {
                        whites = new StringBuffer();
                        whites.append((char)c);
                        while ((c = in.read()) == ' ' || c == '\t') whites.append((char)c);
                        last = c;
                    }
                    if (whites != null)
                        value.append("\r\n").append(whites.toString());
                    if (whites == null || c == -1) {
                        headerList.append(new Header(key.toString().trim(), value.toString().trim()));
                        key.setLength(0);
                        value.setLength(0);
                        inValue = false;
                        if (last == '\r')
                            if (headerList.size() == 0) strictHeader0 = true;
                            else if (strictHeader0 && headerList.size() == 1) strictHeader = true;
                    }
                    if (c == -1 || !strictHeader && c == '\n') return c;
                } else if (strictHeader)
                    if (!inValue) key.append('\r');
                    else value.append('\r');
            if (c == '\r') continue;
            if (value.length() == 0) {
                if (inValue && (c == ' ' || c == '\t')) continue;
                if (!inValue) {
                    if (c == ':') inValue = true;
                    else key.append((char)c);
                    continue;
                }
            }
            value.append((char)c);
        }
        return c;
    }

    public static void main(String argv[]) throws Exception {
        HeaderList list = new HeaderList(System.in);
        Iterator items = list.iterator();
        while (items.hasNext()) {
            Header header = (Header)items.next();
            System.out.println(header.getName() + ": " + header.getValue());
        }
    }

}
