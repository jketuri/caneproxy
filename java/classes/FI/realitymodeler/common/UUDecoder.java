
package FI.realitymodeler.common;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.EOFException;
import java.io.IOException;

/**
 */
public class UUDecoder extends Decoder
{

    /** 
     */
	public String streamName;

    /**
     */
	public int mode;

	private byte decoderBuffer[] = new byte[4];

    /**
     */
    protected void decodeToken(InputStream in, OutputStream out, int length)
        throws IOException
    {
        int i, c1, c2, c3, c4, a, b, c;
        StringBuffer x = new StringBuffer();

        for (i = 0; i < 4; i++) {
            c1 = in.read();
            if (c1 == -1) throw new EOFException();
            x.append((char)c1);
            decoderBuffer[i] = (byte) ((c1 - ' ') & 0x3f);
        }
        a = ((decoderBuffer[0] << 2) & 0xfc) | ((decoderBuffer[1] >>> 4) & 3);
        b = ((decoderBuffer[1] << 4) & 0xf0) | ((decoderBuffer[2] >>> 2) & 0xf);
        c = ((decoderBuffer[2] << 6) & 0xc0) | (decoderBuffer[3] & 0x3f);
        out.write((byte)(a & 0xff));
        if (length > 1) out.write((byte)(b & 0xff));
        if (length > 2) out.write((byte)(c & 0xff));
    }

    /**
     */
    protected void decodeStreamPrefix(InputStream in, OutputStream out)
        throws IOException
    {
        int c;
        StringBuffer q = new StringBuffer(32);
        String r;
        boolean sawNewSequence;
        sawNewSequence = true;
        for (;;)
            {
                c = in.read();
                if (c == -1) throw new EncoderException("UUDecoder: No begin line.");
                if ((c == 'b')  && sawNewSequence)
                    {
                        c = in.read();
                        if (c == 'e') break;
                    }
                sawNewSequence = (c == '\n') || (c == '\r');
            }

        while ((c != '\n') && (c != '\r'))
            {
                c = in.read();
                if (c == -1) throw new EncoderException("UUDecoder: No begin line.");
                if ((c != '\n') && (c != '\r')) q.append((char)c);
            }
        while (c != '\n' && (c = in.read()) != -1);
        if (c == -1) throw new EOFException();
        r = q.toString();
        if (r.indexOf(' ') != 3) throw new EncoderException("UUDecoder: Invalid begin line.");
        mode = Integer.parseInt(r.substring(4, 7));
        streamName = r.substring(r.indexOf(' ', 6) + 1);
    }

    /**
     */ 
    protected int decodeSequencePrefix(InputStream in, OutputStream out)
        throws IOException
    {
        int c;

        c = in.read();
        if (c == ' ')
            {
                decodeSequenceSuffix(in, out);
                throw new EOFException();
            }
        else if (c == -1) throw new EncoderException("UUDecoder: Short stream.");
        c = (c - ' ') & 0x3f;
        if (c > bytesInSequence()) throw new EncoderException("UUDecoder: Bad line length.");
        return c;
    }

    /**
     */
    protected void decodeSequenceSuffix(InputStream in, OutputStream out)
        throws IOException
    {
        int c;
        for (;;)
            {
                c = in.read();
                if (c == -1) throw new EOFException();
                if (c == '\n') break;
            }
    }

    /**
     */
    protected void decodeStreamSuffix(InputStream in, OutputStream out)
        throws IOException
    {
        int c;
        c = in.read(decoderBuffer);
        if ((decoderBuffer[0] != 'e') || (decoderBuffer[1] != 'n') || (decoderBuffer[2] != 'd'))
            throw new EncoderException("UUDecoder: Missing end line.");
    }

    protected int bytesInToken()
    {
        return 3;
    }

    protected int bytesInSequence()
    {
        return 45;
    }

}
