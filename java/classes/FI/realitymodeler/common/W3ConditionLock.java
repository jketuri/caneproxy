
package FI.realitymodeler.common;

/**
 */
public class W3ConditionLock extends W3Lock {
    private int state;

    /**
     */
    public W3ConditionLock(int initialState) {
        state = initialState;
    }

    /**
     */
    public W3ConditionLock() {
        this(0);
    }

    /**
     */
    public synchronized void lock(int requestedState, long timeout)
        throws InterruptedException {
        long ctm = System.currentTimeMillis(), delay = 0L;
        while (state != requestedState && (timeout <= 0L || (delay = System.currentTimeMillis() - ctm) < timeout)) wait(timeout - delay);
        if (state == requestedState) lock();
    }

    /**
     */
    public synchronized void lock(int requestedState)
        throws InterruptedException {
        while (state != requestedState) wait();
        super.lock();
    }

    /**
     */
    public synchronized void release(int newState) {
        state = newState;
        super.release();
    }

    public int getState() {
        return state;
    }

}
