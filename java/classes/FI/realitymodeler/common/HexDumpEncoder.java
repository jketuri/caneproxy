
package FI.realitymodeler.common;

import java.io.OutputStream;
import java.io.IOException;

/**
 */
public class HexDumpEncoder extends Encoder {

    private int offset;
    private int sequenceLength;
    private int currentByte;
    private byte sequence[] = new byte[16];

    static void hexDigit(OutputStream out, byte x)
        throws IOException {
        char c = (char) ((x >> 4) & 0xf);
        if (c > 9) c = (char) ((c - 10) + 'A');
        else c = (char)(c + '0');
        out.write(c);
        c = (char) (x & 0xf);
        if (c > 9) c = (char)((c - 10) + 'A');
        else c = (char)(c + '0');
        out.write(c);
    }

    protected void encodeStreamPrefix(OutputStream out)
        throws IOException {
        offset = 0;
        super.encodeStreamPrefix(out);
    }

    protected void encodeSequencePrefix(OutputStream out, int len)
        throws IOException {
        hexDigit(out, (byte)((offset >>> 8) & 0xff));
        hexDigit(out, (byte)(offset & 0xff));
        Support.writeBytes(out, ": ", null);
        currentByte = 0;
        sequenceLength = len;
    }

    protected void encodeToken(OutputStream out, byte buffer[], int offset, int length)
        throws IOException {
        sequence[currentByte] = buffer[offset];
        hexDigit(out, buffer[offset]);
        out.write(' ');
        currentByte++;
        if (currentByte == 8) Support.writeBytes(out, "  ", null);
    }

    protected void encodeSequenceSuffix(OutputStream out)
        throws IOException {
        if (sequenceLength < 16)
            for (int i = sequenceLength; i < 16; i++) {
                Support.writeBytes(out, "   ", null);
                if (i == 7) Support.writeBytes(out, "  ", null);
            }
        out.write(' ');
        for (int i = 0; i < sequenceLength; i++)
            if ((sequence[i] < ' ') || (sequence[i] > 'z')) out.write('.');
            else out.write(sequence[i]);
        super.encodeSequenceSuffix(out);
        offset += sequenceLength;
    }

    public static void main(String argv[])
        throws IOException {
        new HexDumpEncoder().encodeStream(System.in, System.out);
    }

    protected int bytesInToken() {
        return 1;
    }

    protected int bytesInSequence() {
        return 16;
    }

}
