
package FI.realitymodeler.common;

import java.io.*;
import java.util.*;

/** Converts content of type text/enriched as specified in rfc1896 to HTML.
 */
public class Enriched2HtmlInputStream extends PipedInputStream implements Runnable {
    PipedOutputStream out;
    InputStream in;
    String charsetName;

    /** Filters enriched type to plain text. */
    public static void filter(InputStream in, OutputStream out) throws IOException {
        int c;
        while ((c = in.read()) != -1)
            if (c == '<') {
                if ((c = Support.scanTag(in, "param", false)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) {
                    if (c == -1) break;
                    while ((c = in.read()) != -1 &&
                           (c != '<' || Support.scanTag(in, "/param", false) > 0));
                } while (c > -1 && c != '>') c = in.read();
                if (c == -1) break;
            } else if (c != '\r') out.write(c);
    }

    public static void convert(InputStream in, OutputStream out, String charsetName) throws IOException {
        int c, nofill = 0, state = 0;
        while ((c = in.read()) != -1)
            if (c == '<') {
                in.mark(1);
                if ((c = in.read()) == '<') {
                    Support.writeBytes(out, "&lt;", charsetName);
                    continue;
                }
                if (c == -1) break;
                in.reset();
                int newState = 0;
                if ((c = Support.scanTag(in, "nofill", false)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) {
                    nofill++;
                    Support.writeBytes(out, "<pre>", charsetName);
                } else if ((c = Support.scanTag(in, "/nofill", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) {
                    nofill--;
                    Support.writeBytes(out, "</pre>", charsetName);
                } else if ((c = Support.scanTag(in, "bold", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES)
                    Support.writeBytes(out, "<b>", charsetName);
                else if ((c = Support.scanTag(in, "/bold", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES)
                    Support.writeBytes(out, "</b>", charsetName);
                else if ((c = Support.scanTag(in, "center", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES)
                    Support.writeBytes(out, "<center>", charsetName);
                else if ((c = Support.scanTag(in, "/center", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES)
                    Support.writeBytes(out, "</center>", charsetName);
                else if ((c = Support.scanTag(in, "excerpt", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES)
                    Support.writeBytes(out, "<blockquote>", charsetName);
                else if ((c = Support.scanTag(in, "/excerpt", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES)
                    Support.writeBytes(out, "</blockquote>", charsetName);
                else if ((c = Support.scanTag(in, "fixed", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES)
                    Support.writeBytes(out, "<tt>", charsetName);
                else if ((c = Support.scanTag(in, "/fixed", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES)
                    Support.writeBytes(out, "</tt>", charsetName);
                else if ((c = Support.scanTag(in, "italic", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES)
                    Support.writeBytes(out, "<i>", charsetName);
                else if ((c = Support.scanTag(in, "/italic", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES)
                    Support.writeBytes(out, "</i>", charsetName);
                else if ((c = Support.scanTag(in, "underline", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES)
                    Support.writeBytes(out, "<u>", charsetName);
                else if ((c = Support.scanTag(in, "/underline", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES)
                    Support.writeBytes(out, "</u>", charsetName);
                else if ((c = Support.scanTag(in, "smaller", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES)
                    Support.writeBytes(out, "<font size=-2>", charsetName);
                else if ((c = Support.scanTag(in, "bigger", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES)
                    Support.writeBytes(out, "<font size=+2>", charsetName);
                else if ((c = Support.scanTag(in, "color", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES)
                    newState = 1;
                else if ((c = Support.scanTag(in, "fontfamily", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES)
                    newState = 2;
                else if ((c = Support.scanTag(in, "/fontfamily", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES ||
                         (c = Support.scanTag(in, "/smaller", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES ||
                         (c = Support.scanTag(in, "/bigger", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES ||
                         (c = Support.scanTag(in, "/color", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES)
                    Support.writeBytes(out, "</font>", charsetName);
                else if ((c = Support.scanTag(in, "param", true)) == Support.TAG_WITH_VALUES || c == Support.TAG_WITHOUT_VALUES) {
                    while (c > -1 && c != '>') c = in.read();
                    if (c == -1) break;
                    StringBuffer sb = new StringBuffer();
                    while ((c = in.read()) != -1 &&
                           (c != '<' || (c = Support.scanTag(in, "/param", false)) > 0)) {
                        if (c == '<') in.reset();
                        sb.append((char)c);
                    }
                    String s = sb.toString();
                    switch (state) {
                    case 1:
                        if (s.length() > 0 && Character.isDigit(s.charAt(0))) {
                            int i = 0, rgb[] = new int[3];
                            StringTokenizer st = new StringTokenizer(s, ",");
                            try {
                                while (st.hasMoreTokens() && i < 3)
                                    rgb[i++] = Integer.parseInt(st.nextToken().trim());
                            } catch (NumberFormatException ex) {}
                            if (i >= 3) {
                                sb.setLength(0);
                                for (i = 0; i < 3; i++)
                                    sb.append(Character.forDigit(rgb[i] / 16, 16)).
                                        append(Character.forDigit(rgb[i] % 16, 16));
                                Support.writeBytes(out, "<font color=\"#" + sb.toString() + "\">", charsetName);
                            }
                        } else Support.writeBytes(out, "<font color=\"" + s + "\">", charsetName);
                        break;
                    case 2:
                        Support.writeBytes(out, "<font face=\"" + s + "\">", charsetName);
                        break;
                    }
                }
                state = newState;
                while (c > -1 && c != '>') c = in.read();
                if (c == -1) break;
            } else if (c == '\n') {
                if (nofill <= 0) {
                    for (;;) {
                        in.mark(1);
                        if ((c = in.read()) == -1) break;
                        if (c == '\n') Support.writeBytes(out, "<br>\n", charsetName);
                        else if (c != '\r') break;
                    }
                    if (c == -1) break;
                    in.reset();
                }
            } else if (c == '>') Support.writeBytes(out, "&gt;", charsetName);
            else if (c != '\r') out.write(c);
    }

    public void run() {
        try {
            convert(in, out, charsetName);
            out.close();
        } catch (IOException ex) {
            try {
                close();
            } catch (IOException ex1) {}
        }
    }

    public Enriched2HtmlInputStream(InputStream in, String charsetName) throws IOException {
        this.in = in;
        this.charsetName = charsetName;
        out = new PipedOutputStream(this);
        new Thread(this).start();
    }

}
