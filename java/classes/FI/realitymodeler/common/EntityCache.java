
package FI.realitymodeler.common;

import java.io.*;
import java.text.*;

class EntityCacheFilter implements FilenameFilter {
    String prefix;

    public EntityCacheFilter(String prefix) {
        this.prefix = prefix;
    }

    public EntityCacheFilter() {
        this(null);
    }

    public boolean accept(File dir, String name) {
        return name.endsWith(".tmp");
    }

}

public class EntityCache extends EntityMemory {
    static String cacheDir = null;

    File file, headersFile = null;
    String filePath;
    boolean temporary;

    public static void initialize(String cacheDir, boolean clean) throws IOException {
        if (!cacheDir.endsWith("/")) cacheDir += "/";
        EntityCache.cacheDir = cacheDir.replace('/', File.separatorChar);
        if (clean) {
            String list[] = new File(cacheDir).list(new EntityCacheFilter());
            for (int i = 0; i < list.length; i++) {
                File file = new File(list[i]);
                if (file.isFile() && !file.isDirectory()) file.delete();
            }
        }
    }

    public EntityCache() {
        String baseName  = Long.toString(System.currentTimeMillis(), Character.MAX_RADIX);
        File file = new File((filePath = cacheDir + baseName) + "_0.tmp");
        for (int number = 0; file.exists(); number++)
            file = new File((filePath = cacheDir + baseName + "-" + Integer.toString(number, Character.MAX_RADIX)) + "_0.tmp");
        temporary = true;
    }

    public EntityCache(String filePath) {
        this.filePath = filePath.replace('/', File.separatorChar);
        temporary = false;
    }

    public String makeName(String entryName) {
        if (entryName == null) entryName = String.valueOf(entryNumber);
        this.entryName = entryName;
        return temporary ? filePath + "_" + entryName + ".tmp" : filePath + "_" + entryName;
    }

    public boolean exists(String entryName) {
        return new File(makeName(entryName)).exists();
    }

    public InputStream load(String entryName, HeaderList headerList) throws IOException, ParseException {
        if (!exists(entryName)) return null;
        File file = new File(makeName(entryName)),
            headersFile = new File(file.getPath() + "#");
        if (headerList != null) {
            InputStream in = new BufferedInputStream(new FileInputStream(headersFile));
            Support.readHeaderList(in, headerList);
            in.close();
        }
        InputStream in = new BufferedInputStream(new FileInputStream(file));
        entrySize = in.available();
        return in;
    }

    public OutputStream save(String entryName, HeaderList headerList, boolean check) throws IOException, ParseException {
        entryNumber++;
        file = new File(makeName(entryName));
        headersFile = new File(file.getPath() + "#");
        if (!file.exists()) new File(file.getParent()).mkdirs();
        else if (check) {
            load(entryName, headerList);
            return null;
        }
        if (headerList != null) {
            OutputStream out = new BufferedOutputStream(new FileOutputStream(headersFile));
            Support.sendHeaderList(out, headerList);
            out.close();
        }
        OutputStream out = new BufferedOutputStream(new FileOutputStream(file));
        return out;
    }

    public void closeStream(OutputStream out) throws IOException {
        out.close();
        entrySize = file.length();
    }

    public void closeStream(InputStream in) throws IOException {
        in.close();
    }

    public int getNumberOfElements() {
        return entryNumber;
    }

    public String getFilePath() {
        return filePath;
    }

    public boolean delete(String entryName) {
        entryNumber--;
        return new File(makeName(entryName)).delete();
    }

    public void setReadOnly() {
        file.setReadOnly();
        headersFile.setReadOnly();
    }

    public synchronized void reset() {
        File dir = new File(new File(filePath).getParent());
        String list[] = dir.list(new EntityCacheFilter(new File(filePath).getName() + "_") {
                public boolean accept(File dir, String name) {
                    return name.startsWith(prefix);
                }
            });
        if (list != null) for (int i = 0; i < list.length; i++) new File(dir, list[i]).delete();
        entryNumber = 0;
    }

    protected void finalize() {
        if (temporary) reset();
    }

}
