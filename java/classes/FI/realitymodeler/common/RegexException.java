
package FI.realitymodeler.common;

/**
 */

public class RegexException extends Exception {
    static final long serialVersionUID = 0L;

    RegexException(String s) {
        super(s);
    }
}
