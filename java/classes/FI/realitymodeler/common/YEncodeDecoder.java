
package FI.realitymodeler.common;

import ar.com.ktulu.yenc.YEncDecoder;
import ar.com.ktulu.yenc.YEncException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.EOFException;
import java.io.IOException;

public class YEncodeDecoder extends Decoder {
    YEncDecoder yEncDecoder = new YEncDecoder();

    protected void decodeToken(InputStream in, OutputStream out, int length)
        throws IOException {
    }

    public void decodeStream(InputStream in, OutputStream out)
        throws IOException {
        yEncDecoder.setOutputStream(out);
        for (boolean isNextPart = false;; isNextPart = true)
            try {
                yEncDecoder.setInputStream(in, isNextPart);
                yEncDecoder.decode();
                return;
            } catch (NextInputStreamException ex) {
                continue;
            } catch (YEncException ex) {
                throw new IOException(Support.stackTrace(ex));
            }
    }

    protected int bytesInToken() {
        return 1;
    }

    protected int bytesInSequence() {
        return 1;
    }

}
