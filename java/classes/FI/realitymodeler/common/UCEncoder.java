
package FI.realitymodeler.common;

import java.io.OutputStream;
import java.io.InputStream;
import java.io.IOException;

/**
 */
public class UCEncoder extends Encoder
{

	final static char characterArray[] =
	{
        //		 0   1   2   3   4   5   6   7
		'0','1','2','3','4','5','6','7', // 0
		'8','9','A','B','C','D','E','F', // 1
		'G','H','I','J','K','L','M','N', // 2
		'O','P','Q','R','S','T','U','V', // 3
		'W','X','Y','Z','a','b','c','d', // 4
		'e','f','g','h','i','j','k','l', // 5
		'm','n','o','p','q','r','s','t', // 6
		'u','v','w','x','y','z','(',')'  // 7
	};

	private int sequence;
	private byte tmp[] = new byte[2];
	private CRC16 crc = new CRC16();

    /**
     */  
    protected void encodeToken(OutputStream out, byte data[], int offset, int len)
        throws IOException
    {
        int i;
        int p1, p2; // parity bits
        byte a, b;

        a = data[offset];
        if (len == 2) b = data[offset+1];
        else b = 0;
        crc.update(a);
        if (len == 2) crc.update(b);
        out.write((byte)characterArray[((a >>> 2) & 0x38) + ((b >>> 5) & 0x7)]);
        p1 = 0; p2 = 0;
        for (i = 1; i < 256; i = i * 2)
            {
                if ((a & i) != 0) p1++;
                if ((b & i) != 0) p2++;
            }
        p1 = (p1 & 1) * 32;
        p2 = (p2 & 1) * 32;
        out.write((byte)characterArray[(a & 31) + p1]);
        out.write((byte)characterArray[(b & 31) + p2]);
    }

    /**
     */
    protected void encodeSequencePrefix(OutputStream out, int length)
        throws IOException
    {
        out.write('*');
        crc.value = 0;
        tmp[0] = (byte) length;
        tmp[1] = (byte) sequence;
        sequence = (sequence + 1) & 0xff;
        encodeToken(out, tmp, 0, 2);
    } 

    /**
     */
    protected void encodeSequenceSuffix(OutputStream out)
        throws IOException
    {
        tmp[0] = (byte) ((crc.value >>> 8) & 0xff);
        tmp[1] = (byte) (crc.value & 0xff);
        encodeToken(out, tmp, 0, 2);
        super.encodeSequenceSuffix(out);
    }

    /**
     */
    protected void encodeStreamPrefix(OutputStream out)
        throws IOException
    {
        sequence = 0;
        super.encodeStreamPrefix(out);
    }

    protected int bytesInToken()
    {
        return 2;
    }

    protected int bytesInSequence()
    {
        return 48;
    }

}
