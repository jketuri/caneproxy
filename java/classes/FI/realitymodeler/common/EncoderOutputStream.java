
package FI.realitymodeler.common;

import java.io.*;

public class EncoderOutputStream extends PipedOutputStream implements Runnable {
    Encoder encoder;
    PipedInputStream in;
    OutputStream out;
    Thread thread;

    public void run() {
        try {
            encoder.encodeStream(in, out);
            out.flush();
        } catch (IOException ex) {}
    }

    public EncoderOutputStream(OutputStream out, Encoder encoder) throws IOException {
        this.out = out;
        this.encoder = encoder;
        in = new PipedInputStream(this);
        (thread = new Thread(this)).start();
    }

    public void close() throws IOException {
        super.close();
        try {
            thread.join();
        } catch (InterruptedException ex) {}
    }

}
