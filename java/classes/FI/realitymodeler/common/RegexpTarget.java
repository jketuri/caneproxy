
package FI.realitymodeler.common;

/**
 */

public interface RegexpTarget {

    Object found(String remainder);
}
