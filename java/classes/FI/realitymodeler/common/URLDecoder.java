
package FI.realitymodeler.common;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.EOFException;
import java.io.IOException;

/** URL decoder. Stops reading stream when it ends or in case of query
    decoder url delimiter character is encountered (= or &amp;). Treats ?
    functionally identical to &amp; and resets return character to last
    mentioned.

    @see java.net.URLEncoder
*/
public class URLDecoder extends Decoder {
    public int c;

    boolean isQuery;

    public URLDecoder(boolean isQuery) {
        this.isQuery = isQuery;
    }

    public URLDecoder() {
        this(false);
    }

    protected void decodeToken(InputStream in, OutputStream out, int len)
        throws IOException {
    }

    public void decodeStream(InputStream in, OutputStream out)
        throws IOException {
        try {
            while ((c = in.read()) != -1 && (!isQuery || c != '=' && c != '&' && c != '?'))
                switch (c) {
                case '%': {
                    int c1;
                    if ((c = in.read()) == -1 || isQuery && (c == '=' || c == '&' || c == '?')) return;
                    if (c == '\n') continue;
                    if ((c1 = in.read()) == -1 || isQuery && (c1 == '=' || c1 == '&' || c1 == '?')) {
                        c = c1;
                        return;
                    }
                    if (c == '\r' && c1 == '\n') continue;
                    int d = Character.digit((char)c, 16), d1 = Character.digit((char)c1, 16);
                    if (d == -1 || d1 == -1) {
                        out.write('%');
                        out.write(c);
                        out.write(c1);
                        break;
                    }
                    out.write((d << 4) + d1);
                    break;
                }
                case '+':
                    out.write(' ');
                    break;
                default:
                    out.write(c);
                }
        } catch (NextInputStreamException ex) {} finally {
            if (isQuery && c == '?') c = '&';
        }
    }

    protected int bytesInToken() {
        return 1;
    }

    protected int bytesInSequence() {
        return 1;
    }

}
