
package FI.realitymodeler.common;

import java.io.*;

public class EncodingConversionOutputStream extends PipedOutputStream implements Runnable {
    Reader reader;
    Thread thread;
    Writer writer;

    public void run() {
        try {
            char b[] = new char[Support.bufferLength];
            for (int n; (n = reader.read(b)) > 0;) writer.write(b, 0, n);
            writer.flush();
        } catch (IOException ex) {
            try {
                super.close();
            } catch (IOException ex1) {}
        }
    }

    public EncodingConversionOutputStream(Writer writer, String encoding) throws IOException {
        this.writer = writer;
        reader = new InputStreamReader(new PipedInputStream(this), encoding);
        (thread = new Thread(this)).start();
    }

    public void close() throws IOException {
        super.flush();
        super.close();
        try {
            thread.join();
        } catch (InterruptedException ex) {}
    }

}
