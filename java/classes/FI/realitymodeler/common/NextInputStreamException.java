
package FI.realitymodeler.common;

import java.io.IOException;

public class NextInputStreamException extends IOException {
    static final long serialVersionUID = 0L;

    public NextInputStreamException() {
    }

    public NextInputStreamException(String s) {
        super(s);
    }

}
