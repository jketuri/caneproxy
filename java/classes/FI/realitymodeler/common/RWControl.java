
package FI.realitymodeler.common;

public class RWControl {
    W3ConditionLock readLock = new W3ConditionLock(0);
    int nReads = 0;

    public synchronized void startRead()
        throws InterruptedException {
        readLock.lock(nReads);
        readLock.release(++nReads);
    }

    public synchronized void stopRead() {
        readLock.release(--nReads);
    }

    public synchronized void startWrite()
        throws InterruptedException {
        readLock.lock(0);
    }

    public synchronized void stopWrite() {
        readLock.release(0);
    }

    public synchronized void reset() {
        readLock.release(nReads = 0);
    }

}
