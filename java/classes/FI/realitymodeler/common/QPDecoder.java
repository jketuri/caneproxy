
package FI.realitymodeler.common;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;

/** Quoted printable decoder. */
public class QPDecoder extends Decoder
{

    protected void decodeToken(InputStream in, OutputStream out, int length)
        throws IOException {
    }

    public void decodeStream(InputStream in, OutputStream out)
        throws IOException {
        int c;
        try {
            while ((c = in.read()) != -1)
                if (c == '=') {
                    int c1;
                    if ((c = in.read()) == -1) return;
                    if (c == '\n') continue;
                    if ((c1 = in.read()) == -1) return;
                    if (c == '\r' && c1 == '\n') continue;
                    int d = Character.digit((char)c, 16), d1 = Character.digit((char)c1, 16);
                    if (d == -1 || d1 == -2) {
                        out.write('=');
                        out.write(c);
                        out.write(c1);
                    }
                    else out.write((d << 4) + d1);
                }
                else out.write(c);
        } catch (NextInputStreamException ex) {}
    }

    protected int bytesInToken() {
        return 1;
    }

    protected int bytesInSequence() {
        return 1;
    }

}
