
package FI.realitymodeler.common;

import java.io.*;

public class HtmlWriter extends FilterWriter {

    public HtmlWriter(Writer writer) {
        super(writer);
    }

    public void write(int c) throws IOException {
        if (c == 34 || c == 38 || c == 60 || c == 62) {
            out.write("&#");
            out.write(String.valueOf(c));
            out.write(';');
        } else out.write(c);
    }

    public void write(char cbuf[], int off, int len) throws IOException {
        for (int i = off; i < len; i++) write(cbuf[i]);
    }

    public void write(String str, int off, int len) throws IOException {
        for (int i = off; i < len; i++) write(str.charAt(i));
    }

}
