
package FI.realitymodeler.common;

import java.io.InputStream;
import java.io.FilterInputStream;
import java.io.IOException;

/** Input stream with length fixed in advance. */
public class LimitInputStream extends FilterInputStream {
    boolean ended = false, markedEnded = false;
    int length, index, markedIndex;

    public LimitInputStream(InputStream in, int length) {
        super(in);
        this.length = length;
        index = 0;
        if (length <= 0) ended = true;
    }

    public int read()
        throws IOException {
        if (ended) return -1;
        int c = in.read();
        if (c == -1) ended = true;
        else if (++index >= length) ended = true;
        return c;
    }

    public int read(byte b[], int off, int len)
        throws IOException {
        if (ended) return -1;
        int n = in.read(b, off, length != -1 ? Math.min(len, length - index) : len);
        if (n <= 0) {
            ended = true;
            return -1;
        }
        if ((index += n) >= length) ended = true;
        return n;
    }

    public int read(byte b[])
        throws IOException {
        return read(b, 0, b.length);
    }

    public long skip(long n)
        throws IOException {
        if (ended) return -1;
        n = in.skip(length != -1 ? Math.min(n, length - index) : n);
        if ((index += n) >= length) ended = true;
        return n;
    }

    public int available()
        throws IOException {
        return length != -1 ? Math.min(in.available(), length - index) : in.available();
    }

    public synchronized void mark(int readlimit) {
        in.mark(readlimit);
        markedIndex = index;
        markedEnded = ended;
    }

    public synchronized void reset()
        throws IOException {
        in.reset();
        index = markedIndex;
        ended = markedEnded;
    }

    public int getRemaining() {
        return length == -1 ? -1 : length - index;
    }

}
