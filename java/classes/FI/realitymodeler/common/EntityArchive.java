
package FI.realitymodeler.common;

import java.io.*;
import java.text.*;
import java.util.*;
import java.util.jar.*;

public class EntityArchive extends EntityMemory {
    JarEntry jarEntry = null;
    JarFile jarFile = null;
    JarOutputStream jarStream = null;
    String name = null;

    public EntityArchive(String name) throws IOException {
        this.name = name;
    }

    public EntityArchive(String name, HeaderList headerList) throws IOException {
        this(name);
        Manifest manifest = new Manifest();
        Attributes mainAttributes = manifest.getMainAttributes();
        mainAttributes.put(Attributes.Name.MANIFEST_VERSION, "1.0");
        Iterator items = headerList.iterator();
        while (items.hasNext()) {
            Header header = (Header)items.next();
            if (header.getName().equals("")) continue;
            mainAttributes.putValue(header.getName(), header.getValue());
        }
        jarStream = new JarOutputStream(new FileOutputStream(name), manifest);
    }

    protected void openJarFile() throws IOException {
        if (jarStream == null && jarFile == null) jarFile = new JarFile(name);
    }

    protected JarEntry getEntry(String entryName) throws IOException {
        openJarFile();
        return jarFile.getJarEntry(entryName);
    }

    public boolean exists(String entryName) throws IOException {
        return getEntry(entryName) != null;
    }

    public InputStream load(String entryName, HeaderList headerList) throws IOException, ParseException {
        jarEntry = getEntry(entryName);
        if (jarEntry == null) return null;
        if (headerList != null) {
            byte extra[] = jarEntry.getExtra();
            if (extra != null) {
                ByteArrayInputStream bin = new ByteArrayInputStream(extra);
                HeaderList.parseHeaders(bin, headerList, false);
            }
        }
        entrySize = jarEntry.getSize();
        return new BufferedInputStream(jarFile.getInputStream(jarEntry));
    }

    public OutputStream save(String entryName, HeaderList headerList, boolean check) throws IOException, ParseException {
        if (jarStream == null)
            if (check) {
                load(entryName, headerList);
                return null;
            } else jarStream = new JarOutputStream(new FileOutputStream(name));
        entryNumber++;
        if (entryName == null) entryName = String.valueOf(entryNumber);
        this.entryName = entryName;
        jarEntry = new JarEntry(entryName);
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        Support.sendHeaderList(bout, headerList);
        jarEntry.setExtra(bout.toByteArray());
        jarStream.putNextEntry(jarEntry);
        return jarStream;
    }

    public void closeStream(OutputStream out) throws IOException {
        entrySize = jarEntry.getSize();
        jarStream.closeEntry();
    }

    public void closeStream(InputStream in) {
    }

    public int getNumberOfElements() throws IOException {
        openJarFile();
        return jarFile.size() - 1;
    }

    public String getFilePath() throws IOException {
        openJarFile();
        return jarFile.getName();
    }

    public boolean delete(String entryName) throws IOException {
        entryNumber--;
        return false;
    }

    public void setReadOnly() throws IOException {
    }

    public void close() throws IOException {
        if (jarStream != null) {
            jarStream.close();
            jarStream = null;
        }
        if (jarFile != null) {
            jarFile.close();
            jarFile = null;
        }
    }

    public synchronized void reset() throws IOException {
        close();
        new File(name).delete();
        entryNumber = 0;
    }

}
