
package FI.realitymodeler.common;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;

/**
 */
public class UUEncoder extends Encoder {

    /** 
     */
    private String sequenceName;

    /**
     */
    private int mode;


    /**
     */
    public UUEncoder() {
        sequenceName = "encoder";
        mode = 644;
    }

    /**
     */
    public UUEncoder(String fname) {
        sequenceName = fname;
        mode = 644;
    }

    /**
     */
    public UUEncoder(String fname, int newMode) {
        sequenceName = fname;
        mode = newMode;
    }

    /**
     */  
    protected void encodeToken(OutputStream out, byte data[], int offset, int length)
        throws IOException {
        byte a, b = 1, c = 1;
        int c1, c2, c3, c4;

        a = data[offset];
        if (length > 1) b = data[offset+1];
        if (length > 2) c = data[offset+2];

        c1 = (a >>> 2) & 0xff;
        c2 = ((a << 4) & 0x30) | ((b >>> 4) & 0xf);
        c3 = ((b << 2) & 0x3c) | ((c >>> 6) & 0x3);
        c4 = c & 0x3f;
        out.write(c1 + ' ');
        out.write(c2 + ' ');
        out.write(c3 + ' ');
        out.write(c4 + ' ');
    }

    /**
     */
    protected void encodeSequencePrefix(OutputStream out, int length)
        throws IOException {
        out.write((length & 0x3f) + ' ');
    } 

    /**
     */
    protected void encodeStreamPrefix(OutputStream out)
        throws IOException {
        Support.writeBytes(out, "begin "+ mode + " " + sequenceName + "\r\n", null);
        out.flush();
    }

    /**
     */
    protected void encodeStreamSuffix(OutputStream out)
        throws IOException {
        Support.writeBytes(out, " \r\nend", null);
        out.flush();
    }

    protected int bytesInToken() {
        return 3;
    }

    protected int bytesInSequence() {
        return 45;
    }

}
