
package FI.realitymodeler.common;

import java.util.*;

public class EnumerationSequence implements Enumeration
{
    Enumeration enumerations[] = null;
    Object element = null;
    Set passedElements = null, passElements = null;
    String passPrefix = null;
    boolean areUnique = false, hasElement = false;
    int enumerationIndex = 0;

    public EnumerationSequence(Enumeration enumerations[], boolean areUnique)
    {
        this.enumerations = enumerations;
        this.areUnique = areUnique;
        if (areUnique) passedElements = new HashSet<Object>();
    }

    public EnumerationSequence(Enumeration enumerations[], Set passElements)
    {
        this(enumerations, true);
        this.passElements = passElements;
    }

    public EnumerationSequence(Enumeration enumerations[], String passPrefix)
    {
        this(enumerations, true);
        this.passPrefix = passPrefix;
    }

    public EnumerationSequence(Enumeration enumerations[])
    {
        this(enumerations, false);
    }

    public boolean hasMoreElements()
    {
        if (hasElement) return true;
        while (enumerationIndex < enumerations.length) {
            if (enumerations[enumerationIndex].hasMoreElements()) {
                element = enumerations[enumerationIndex].nextElement();
                if (areUnique) {
                    if (passPrefix != null && element instanceof String && ((String)element).startsWith(passPrefix)) continue;
                    if (passElements != null && passElements.contains(element) || passedElements.contains(element)) continue;
                    passedElements.add(element);
                }
                return hasElement = true;
            }
            enumerationIndex++;
        }
        return false;
    }
	
    public Object nextElement()
    {
        if (!hasMoreElements()) throw new NoSuchElementException();
        hasElement = false;
        return element;
    }

}
