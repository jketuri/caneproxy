
package FI.realitymodeler.common;

import java.io.*;

public class DecoderInputStream extends PipedInputStream implements Runnable {
    Decoder decoder;
    PipedOutputStream out;
    InputStream in;
    IOException ex = null;

    public void run() {
        try {
            try {
                decoder.decodeStream(in, out);
            } catch (EncoderException ex) {}
            out.close();
        } catch (IOException ex) {
            this.ex = ex;
            try {
                close();
            } catch (IOException ex1) {}
        }
    }

    public int read() throws IOException {
        try {
            return super.read();
        } catch (IOException ex) {
            throw this.ex != null ? this.ex : ex;
        }
    }

    public DecoderInputStream(InputStream in, Decoder decoder) throws IOException {
        this.in = in;
        this.decoder = decoder;
        out = new PipedOutputStream(this);
        new Thread(this).start();
    }

}
