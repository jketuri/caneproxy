
package FI.realitymodeler.common;

/**
 */
public class W3Lock {
    private boolean locked;

    public W3Lock(boolean locked) {
        this.locked = locked;
    }

    /**
     */
    public W3Lock() {
        this(false);
    }

    /**
     */
    public final synchronized void lock(long timeout)
        throws InterruptedException {
        long ctm = System.currentTimeMillis(), delay = 0L;
        while (locked && (timeout <= 0L || (delay = System.currentTimeMillis() - ctm) < timeout)) wait(timeout - delay);
        locked = true;
    }

    /**
     */
    public final synchronized void lock()
        throws InterruptedException {
        while (locked) wait();
        locked = true;
    }

    /**
     */
    public final synchronized void release() {
        locked = false;
        notifyAll();
    }

    public boolean isLocked() {
        return locked;
    }

}
