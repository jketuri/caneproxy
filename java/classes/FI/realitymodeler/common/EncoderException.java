
package FI.realitymodeler.common;

import java.io.IOException;

public class EncoderException extends IOException {
    static final long serialVersionUID = 0L;

    public EncoderException(String s) {
        super(s);
    }

}
