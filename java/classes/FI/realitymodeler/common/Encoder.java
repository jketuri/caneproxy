
package FI.realitymodeler.common;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;

/**
 */
public abstract class Encoder {
    protected boolean noSequenceSuffix = false;

    /**
     */
    protected void encodeStreamPrefix(OutputStream out)
        throws IOException {
    }

    /**
     */
    protected void encodeStreamSuffix(OutputStream out)
        throws IOException {
    }

    /**
     */
    protected void encodeSequencePrefix(OutputStream out, int length)
        throws IOException {
    }

    /**
     */
    protected void encodeSequenceSuffix(OutputStream out)
        throws IOException {
        if (noSequenceSuffix) return;
        out.write('\r');
        out.write('\n');
    }

    abstract protected void encodeToken(OutputStream out, byte someBytes[], int anOffset, int length)
        throws IOException;

    /**
     */
    protected int readFully(InputStream in, byte buffer[])
        throws IOException {
        int offset = 0;
        while (offset < buffer.length) {
            int n = in.read(buffer, offset, buffer.length - offset);
            if (n <= 0) return offset;
            offset += n;
        }
        return offset;
    }

    /**
     */
    public void encode(InputStream in, OutputStream out)
        throws IOException {
        int j;
        int numBytes;
        byte tmpbuffer[] = new byte[bytesInSequence()];

        encodeStreamPrefix(out);

        for (;;) {
            numBytes = readFully(in, tmpbuffer);
            if (numBytes <= 0) break;
            encodeSequencePrefix(out, numBytes);
            for (j = 0; j < numBytes; j += bytesInToken())
                if ((j + bytesInToken()) <= numBytes) encodeToken(out, tmpbuffer, j, bytesInToken());
                else encodeToken(out, tmpbuffer, j, numBytes - j);
            if (numBytes < bytesInSequence()) break;
            else encodeSequenceSuffix(out);
        }
        encodeStreamSuffix(out);
    }

    /**
     */
    public void encode(byte buffer[], OutputStream out)
        throws IOException {
        ByteArrayInputStream in = new ByteArrayInputStream(buffer);
        encode(in, out);
    }

    /**
     */
    public String encode(byte buffer[]) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ByteArrayInputStream in = new ByteArrayInputStream(buffer);
        try {
            encode(in, out);
        } catch (Exception IOException) {
            // This should never happen.
            throw new Error("ChracterEncoder::encodeStream internal error");
        }
        return (out.toString());
    }

    /**
     */
    public void encodeStream(InputStream in, OutputStream out)
        throws IOException {
        int j;
        int numBytes;
        byte tmpbuffer[] = new byte[bytesInSequence()];

        encodeStreamPrefix(out);

        for (;;) {
            numBytes = readFully(in, tmpbuffer);
            if (numBytes <= 0) break;
            encodeSequencePrefix(out, numBytes);
            for (j = 0; j < numBytes; j += bytesInToken())
                if ((j + bytesInToken()) <= numBytes) encodeToken(out, tmpbuffer, j, bytesInToken());
                else encodeToken(out, tmpbuffer, j, numBytes - j);
            encodeSequenceSuffix(out);
            if (numBytes < bytesInSequence()) break;
        }
        encodeStreamSuffix(out);
    }

    /**
     */
    public void encodeStream(byte buffer[], OutputStream out)
        throws IOException {
        ByteArrayInputStream in = new ByteArrayInputStream(buffer);
        encodeStream(in, out);
    }

    /**
     */
    public String encodeStream(byte buffer[])
        throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ByteArrayInputStream in = new ByteArrayInputStream(buffer);
        encodeStream(in, out);
        return out.toString();
    }

    abstract protected int bytesInToken();

    abstract protected int bytesInSequence();

}
