
package FI.realitymodeler.common;

public class Assignment {
    public String name;
    public String value;
    public boolean quoted;

    public Assignment(String name, String value, boolean quoted) {
        this.name = name;
        this.value = value;
        this.quoted = quoted;
    }

}
