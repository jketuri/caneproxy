
package FI.realitymodeler;

import java.io.*;

public class W3Newsgroup {
    public String name;
    public int numberArticles;
    public int firstArticle;
    public int lastArticle;

    public W3Newsgroup(String name, int number, int start, int end) {
        this.name = name;
        numberArticles = number;
        firstArticle = start;
        lastArticle = end;
    }

    public String toString() {
        return "Newsgroup[name=" + name + "[" + firstArticle + ", " + lastArticle + "]";
    }

    public void reload(W3NntpRequester nntp)
        throws IOException {
        W3Newsgroup newsgroup = nntp.getGroup(name);
        this.firstArticle = newsgroup.firstArticle;
        this.lastArticle = newsgroup.lastArticle;
    }

}
