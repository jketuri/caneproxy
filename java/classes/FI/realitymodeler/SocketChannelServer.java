
package FI.realitymodeler;

import java.io.*;
import java.net.*;
import java.nio.*;
import java.nio.channels.*;
import java.nio.channels.spi.*;
import java.util.*;

/** General socket server using thread pool. */
public class SocketChannelServer extends Thread {
    public ServerSocketChannel serverSocketChannel;
    public SelectorProvider selectorProvider;
    public Request request;
    public ThreadPool pool;
    public long timeout;

    public SocketChannelServer() {
    }

    public SocketChannelServer(ServerSocketChannel serverSocketChannel, Request request, long timeout) {
        this.serverSocketChannel = serverSocketChannel;
        this.request = request;
        this.timeout = timeout;
        selectorProvider = SelectorProvider.provider();
    }

    public void handle(Throwable throwable) {
        throwable.printStackTrace();
    }

    public void run() {
        Selector selector = null;
        try {
            selector = selectorProvider.openSelector();
            serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
        } catch (Exception ex) {
            handle(ex);
            return;
        }
        request.pool = pool = new ThreadPool(timeout);
        while (!isInterrupted())
            try {
                if (selector.select() > 0) {
                    Iterator<SelectionKey> selectedKeys = selector.selectedKeys().iterator();
                    while (selectedKeys.hasNext()) {
                        SelectionKey selectionKey = selectedKeys.next();
                        selectedKeys.remove();
                        if (selectionKey.isAcceptable()) {
                            serverSocketChannel = (ServerSocketChannel)selectionKey.channel();
                            SocketChannel socketChannel = serverSocketChannel.accept();
                            socketChannel.configureBlocking(false);
                            request.socket = new ChannelSocket(socketChannel);
                            /*
                            selectionKey = socketChannel.register(selector, SelectionKey.OP_READ);
                            selectionKey.attach(request.socket);
                            */
                        }
                        /*
                        if (selectionKey.isReadable()) {
                            ChannelSocket channelSocket = (ChannelSocket)selectionKey.attachment();
                            channelSocket.in.signal();
                            if (channelSocket.suspended) {
                                channelSocket.suspended = false;
                                request.socket = channelSocket;
                            } else continue;
                        }
                        if (selectionKey.isWritable()) {
                            ChannelSocket channelSocket = (ChannelSocket)selectionKey.attachment();
                            channelSocket.out.signal();
                            continue;
                        }
                        */
                        request = (Request)pool.reserve(request);
                    }
                }
            } catch (Exception ex) {
                if (ex instanceof InterruptedException) break;
                handle(ex);
            } catch (Error er) {
                handle(er);
                throw er;
            }
    }

    public final boolean release(Request request) throws InterruptedException {
        return pool.release(request);
    }

}
