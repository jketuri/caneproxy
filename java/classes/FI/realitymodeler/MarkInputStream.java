
package FI.realitymodeler;

import java.io.*;

public class MarkInputStream extends FilterInputStream {
    ByteArrayInputStream bytesInput = null;
    ByteArrayOutputStream bytesOutput = new ByteArrayOutputStream();
    boolean ended = false, marked = false, reading = false;

    public MarkInputStream(InputStream in) {
        super(in);
    }

    public int read()
        throws IOException {
        if (reading) {
            int c = bytesInput.read();
            if (c != -1) {
                if (marked) bytesOutput.write(c);
                return c;
            }
            reading = false;
        }
        if (ended) return -1;
        int c = in.read();
        if (c == -1) ended = true;
        else if (marked) bytesOutput.write(c);
        return c;
    }

    public int read(byte b[])
        throws IOException {
        return read(b, 0, b.length);
    }

    public int read(byte b[], int off, int len)
        throws IOException {
        int n = 0;
        if (reading) {
            n = bytesInput.read(b, off, len);
            if (n >= len) {
                if (marked) bytesOutput.write(b, off, n);
                return n;
            }
            if (n > 0) {
                if (marked) bytesOutput.write(b, off, n);
                off += n;
                len -= n;
            } else n = 0;
            reading = false;
        }
        if (!ended && len > 0) {
            int n1 = in.read(b, off, len);
            if (n1 <= 0) {
                ended = true;
                return n > 0 ? n : -1;
            }
            n += n1;
            if (marked) bytesOutput.write(b, off, n1);
        }
        return n > 0 ? n : len > 0 ? -1 : n;
    }

    public int available()
        throws IOException {
        int n = ended ? 0 : in.available();
        if (reading) return bytesInput.available() + n;
        return n;
    }

    public void mark(int readLimit) {
        bytesOutput.reset();
        marked = true;
    }

    public void reset()
        throws IOException {
        bytesInput = new ByteArrayInputStream(bytesOutput.toByteArray());
        marked = false;
        reading = true;
    }

    public boolean markSupported() {
        return true;
    }

}
