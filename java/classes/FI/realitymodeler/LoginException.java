
package FI.realitymodeler;

import java.io.*;

public class LoginException extends IOException {
    static final long serialVersionUID = 0L;

    String domain;

    public LoginException(String msg, String domain) {
        super(msg);
        this.domain = domain;
    }

    public String getDomain() {
        return domain;
    }

}
