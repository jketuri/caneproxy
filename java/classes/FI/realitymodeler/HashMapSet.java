
package FI.realitymodeler;

import java.util.*;

public class HashMapSet<K,V> extends HashMap<K,V> {
    static final long serialVersionUID = 0L;

    public HashMapSet union(HashMapSet<K,V> hs) {
        Iterator<Map.Entry<K,V>> entryIter = hs.entrySet().iterator();
        while (entryIter.hasNext()) {
            Map.Entry<K,V> entry = entryIter.next();
            hs.put(entry.getKey(), entry.getValue());
        }
        return this;
    }

    public HashMapSet exclusiveUnion(HashMapSet<K,V> hs) {
        Iterator<Map.Entry<K,V>> entryIter = hs.entrySet().iterator();
        while (entryIter.hasNext()) {
            Map.Entry<K,V> entry = entryIter.next();
            if (containsKey(entry.getKey())) remove(entry.getKey());
            else put(entry.getKey(), entry.getValue());
        }
        return this;
    }

    public HashMapSet intersection(HashMapSet<K,V> hs) {
        Iterator<K> keyIter = keySet().iterator();
        while (keyIter.hasNext()) {
            K key = keyIter.next();
            if (!hs.containsKey(key)) remove(key);
        }
        return this;
    }

    public HashMapSet difference(HashMapSet<K,V> hs) {
        Iterator<K> keyIter = keySet().iterator();
        while (keyIter.hasNext()) {
            K key = keyIter.next();
            if (hs.containsKey(key)) remove(key);
        }
        return this;
    }

}

