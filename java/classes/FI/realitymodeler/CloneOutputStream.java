
package FI.realitymodeler;

import java.io.*;

public class CloneOutputStream extends OutputStream {
    public OutputStream outs[];

    public CloneOutputStream(OutputStream outs[]) {
        this.outs = outs;
    }

    public void write(int b) throws IOException {
        for (int i = 0; i < outs.length; i++) outs[i].write(b);
    }

    public void write(byte b[]) throws IOException {
        for (int i = 0; i < outs.length; i++) outs[i].write(b);
    }

    public void write(byte b[], int off, int len) throws IOException {  
        for (int i = 0; i < outs.length; i++) outs[i].write(b, off, len);
    }

    public void flush() throws IOException {
        for (int i = 0; i < outs.length; i++) outs[i].flush();
    }

    public void close() throws IOException {
        IOException ex = null;
        for (int i = 0; i < outs.length; i++)
            try {
                outs[i].close();
            } catch (IOException ex1) {
                ex = ex1;
            }
        if (ex != null) throw ex;
    }

}
