
package FI.realitymodeler;

import java.io.*;
import java.net.*;

public class W3SocketInputStream extends InputStream {
    private W3SocketImpl impl;
    private boolean eof = false;
    private byte temp[] = new byte[1];

    W3SocketInputStream(W3SocketImpl impl)
        throws IOException {
        this.impl = impl;
    }

    public final int read(byte buf[])
        throws IOException {
        return read(buf, 0, buf.length);
    }

    public final int read(byte buf[], int off, int len)
        throws IOException {
        if (impl.isClosed()) throw new SocketException("Socket is closed");
        if (eof) return -1;
        if (len == 0) return 0;
        int n = impl.read(buf, off, len);
        if (n > 0) return n;
        eof = true;
        return -1;
    }

    public final int read()
        throws IOException {
        if (impl.isClosed()) throw new SocketException("Socket is closed");
        if (eof) return -1;
        int n = impl.read(temp, 0, 1);
        if (n > 0) return temp[0] & 255;
        eof = true;
        return -1;
    }

    public final long skip(long n)
        throws IOException {
        if (n <= 0L) return n;
        byte b[] = new byte[(int)Math.min(1024, n)];
        long l = n;
        while (l > 0) {
            int k = read(b, 0, (int)Math.min(l, b.length));
            if (k < 1) break;
            l -= k;
        }
        return n - l;
    }

    public final int available()
        throws IOException {
        return impl.available();
    }

    public final void close()
        throws IOException {
        eof = true;
    }

}
