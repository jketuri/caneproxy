
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.nio.*;
import java.nio.channels.*;
import java.nio.channels.spi.*;

public class ChannelSocketInputStream extends InputStream {
    SocketChannel socketChannel = null;
    SelectionKey selectionKey = null;
    Selector selector = null;
    ByteBuffer buffer = null;
    boolean eof = false;

    public ChannelSocketInputStream(SocketChannel socketChannel)
        throws IOException {
        this.socketChannel = socketChannel;
        selector = Selector.open();
        //socketChannel.configureBlocking(false);
        selectionKey = socketChannel.register(selector, SelectionKey.OP_READ);
        //socketChannel.configureBlocking(true);
        buffer = ByteBuffer.allocateDirect(Support.bufferLength);
        buffer.flip();
    }

    private boolean demandRead() throws IOException {
        if (eof) return true;
        if (buffer.remaining() > 0) return false;
        selector.select();
        /*
        SelectionKey selectionKey = socketChannel.keyFor(selector);
        int interestOps = selectionKey.interestOps();
        interestOps |= SelectionKey.OP_READ;
        selectionKey.interestOps(interestOps);
        selector.wakeup();
        */
        /*
        try {
            synchronized (this) {
                wait();
            }
        } catch (InterruptedException ex) {
            eof = true;
            Thread.currentThread().interrupt();
            return true;
        }
        */
        readBuffer();
        return false;
    }

    private synchronized void leaveRead() {
        /*
        SelectionKey selectionKey = socketChannel.keyFor(selector);
        int interestOps = selectionKey.interestOps();
        interestOps &= 0xffffffff ^ SelectionKey.OP_READ;
        selectionKey.interestOps(interestOps);
        */
    }

    private void readBuffer() throws IOException {
        if (buffer.remaining() == 0) buffer.clear();
        if (!socketChannel.isConnected() || socketChannel.read(buffer) == -1) eof = true;
        buffer.flip();
    }

    void signal() {
        synchronized (this) {
            notifyAll();
        }
    }

    public final int read(byte buf[]) throws IOException {
	return read(buf, 0, buf.length);
    }

    public final int read(byte buf[], int off, int len) throws IOException {
        if (demandRead() || eof) return -1;
        try {
            int n = Math.min(len, buffer.remaining());
            buffer.get(buf, off, n);
            return n;
        } finally {
            leaveRead();
        }
    }

    public final int read() throws IOException {
        if (demandRead() || eof) return -1;
        try {
            return buffer.remaining() > 0 ? (int)buffer.get() : -1;
        } finally {
            leaveRead();
        }
    }

    public final long skip(long n) throws IOException {
	if (n <= 0L) return n;
	byte b[] = new byte[(int)Math.min(1024, n)];
	long l = n;
	while (l > 0) {
            int k = read(b, 0, (int)Math.min(l, b.length));
            if (k < 1) break;
            l -= k;
	}
	return n - l;
    }

    public final int available() throws IOException {
        return buffer.remaining();
    }

    public final void close() throws IOException {
        eof = true;
    }

}
