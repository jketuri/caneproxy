
package FI.realitymodeler;

import java.io.*;

public class CountInputStream extends FilterInputStream {
    long count = 0, markedCount = 0;

    public CountInputStream(InputStream in) {
        super(in);
    }

    public long getCount() {
        return count;
    }

    public int read() throws IOException {
        int c = in.read();
        if (c != -1) count++;
        return c;
    }

    public int read(byte b[], int off, int len) throws IOException {
        int n = in.read(b, off, len);
        if (n <= 0) return n;
        count += (long)n;
        return n;
    }

    public int read(byte b[]) throws IOException {
        return read(b, 0, b.length);
    }

    public long skip(long n) throws IOException {
        n = in.skip(n);
        if (n <= 0) return n;
        count += n;
        return n;
    }

    public void mark(int readLimit) {
        in.mark(readLimit);
        markedCount = count;
    }

    public void reset() throws IOException {
        in.reset();
        count = markedCount;
    }

}
