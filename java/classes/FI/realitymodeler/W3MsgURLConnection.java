
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;
import java.util.jar.*;
import javax.servlet.*;

class MsgPipingThread extends CacheFillingThread {
    final byte LIST = 1, MOVE = 2, SEND = 3;

    EntityMemory entity = null;
    Map<String,MsgItem> msgs;
    Map<InetAddress,StringBuffer> recipients = new HashMap<InetAddress,StringBuffer>();
    HeaderList headerList, headerFields;
    OutputStream entityOut = null;
    W3Lock lock = null;
    W3Requester requester = null;
    String authorization = null, cachePath = null, categories = null, category = null, from = null, host = null, path = null, query = null, replyTo = null, sendHostEncoded = null, signEncoded = null, target = null, uniqueID = null;
    Vector <MsgItem>msgVec;
    boolean auto = false, contents = false, delete = false, forward = false, header = false, mobile = false, plain = false, remove = false, secureSend = false, servlet = false;
    int itemsInPage = 0, itemOffset = 0;
    byte action = 0;
    long time;

    class ParserRequester extends W3SmtpRequester {

        protected void toCanonical(String s) throws IOException {
            int i = s.indexOf('@');
            if (i == -1) throw new IOException("Invalid recipient " + s);
            String name = s.substring(0, i).trim(), host = s.substring(i + 1).trim();
            InetAddress address = InetAddress.getByName(host);
            StringBuffer sb = recipients.get(address);
            if (sb == null) recipients.put(address, sb = new StringBuffer());
            if (sb.length() > 0) sb.append(',');
            sb.append(name);
        }

    }

    public void to(String s) throws IOException {
        new ParserRequester().to(s);
    }

    MsgPipingThread(W3MsgURLConnection uc) {
        super(null);
        this.cachePath = uc.cachePath;
        this.host = uc.host;
        if (uc.requester != null) this.requester = (W3Requester)uc.requester.clone();
        this.query = uc.query;
    }

    public void run() {
        try {
            switch (action) {
            case LIST: {
                OutputStream htmlOut = Support.getHtmlOutputStream(out, from != null ? uc.servletPathTable : null, uc.charsetName);
                String address[] = new String[2], others[] = new String[4],
                    fromAddress = from != null ? uc.getAddress(address, from, host)[0] : null, from0 = address[0], hostEncoded = URLEncoder.encode(host, uc.charsetName),
                    fromEncoded = from != null ? URLEncoder.encode(from, uc.charsetName) : "",
                    replyToEncoded = replyTo != null ? URLEncoder.encode(replyTo, uc.charsetName) : null;
                writeBytes("<html><head>\n");
                writeBytes("<style type=\"text/css\">\n");
                writeBytes("span.listcolumn0 { width: 15em; clear: left; float: left }\n");
                writeBytes("span.listcolumn { width: 15em; float: left }\n");
                writeBytes("</style>\n");
                writeBytes("<title>" + (category != null ? category : host) + "</title>\n");
                writeBytes("<base target=\"message\">\n");
                writeBytes("</head><body>\n");
                writeBytes("<h1>" + MessageFormat.format(uc.messages.getString("messagesInHostForUser"), new Object[] {host, uc.username}) + "</h1>\n");
                if (!contents) {
                    if (servlet && !mobile) writeBytes("<a href=\"" + path.substring(0, path.lastIndexOf('/', path.length() - 2) + 1) + (query != null ? "?" + query + "&" : "?") + "new=\" target=_top>" + uc.messages.getString("main") + "</a><br>\n");
                    if (!msgVec.isEmpty())
                        if (mobile) {
                            writeBytes("<form action=\"" + path + "\" method=get>\n");
                            writeBytes("<select name=item>\n");
                        } else {
                            if (servlet) writeBytes("<a href=\"" + path + (query != null ? "?" + query + "&" : "?") + "contents=\" target=_top>" + uc.messages.getString("getAllContents") + "</a><br>\n");
                            writeBytes("<form action=\"" + path + "move\" method=post name=messages>\n");
                            writeBytes("<input name=from type=hidden value=\"" + (from0 != null ? from0 : "") + "\">\n");
                            writeBytes("<select name=category>\n");
                            writeBytes("<option value=\"destruction\">" + uc.messages.getString("destruction") + "\n");
                            if (categories != null) {
                                StringTokenizer categoryTok = new StringTokenizer(categories, ",");
                                while (categoryTok.hasMoreElements()) writeBytes("<option>" + ((String)categoryTok.nextElement()).trim() + "\n");
                            }
                            writeBytes("</select><br>\n");
                            writeBytes("<input type=submit value=\"" + uc.messages.getString("moveCheckedMessages") + "\"><p>\n");
                            writeBytes("<script language=\"Javascript\">\n");
                            writeBytes("function setMessages(checked) {\n");
                            writeBytes("for (var i = 0; i < document.messages.elements.length; i++) {\n");
                            writeBytes("document.messages.elements[i].checked = checked; } }\n");
                            writeBytes("</script>\n");
                            writeBytes("<br>\n");
                            writeBytes("<a href=\"#\" onclick=\"setMessages(true);\" target=\"_self\">" + uc.messages.getString("selectAllMessages") + "</a><br>\n");
                            writeBytes("<a href=\"#\" onclick=\"setMessages(false);\" target=\"_self\">" + uc.messages.getString("clearAllMessages") + "</a><p>\n");
                            writeBytes("<hr>\n");
                        }
                }
                boolean self = category != null && category.equalsIgnoreCase("sent mail");
                int currentTotal = 0, dummy = 0, nextItemOffset = 0, total = 0;
                boolean seekingPrevious = false;
                int msgVecSize = msgVec.size(), msgVecStart = itemOffset > 0 ? itemOffset : msgVecSize - 1;
                for (uc.index0 = msgVecStart; seekingPrevious ? uc.index0 < msgVecSize : uc.index0 >= 0; dummy += seekingPrevious ? uc.index0++ : uc.index0--) {
                    MsgItem msgItem = null;
                    HeaderList headerList = null, headerFields = new HeaderList();
                    msgItem = msgVec.elementAt(uc.index0);
                    headerList = uc.getHeader(requester, msgItem);
                    if (headerList == null || !uc.getHeader(null, headerList, headerFields, false) && !forward) continue;
                    uniqueID = msgItem.msgId;
                    if (uniqueID == null) uniqueID = "";
                    String uniqueIDEncoded = Support.encode(uniqueID, null);
                    String s;
                    Date date;
                    if ((s = headerList.getHeaderValue("date")) == null) date = null;
                    else if ((date = uc.parse(s)) != null && time != 0L && date.getTime() < time) break;
                    String to = uc.getAddress(headerList, address, host);
                    if (address[0] != null && from0 != null && address[0].equalsIgnoreCase(from0)) {
                        if (!self) continue;
                    } else if (self) continue;
                    if (category != null)
                        if ((s = headerList.getHeaderValue("x-category")) != null) {
                            if (!category.equalsIgnoreCase(s)) continue;
                        } else if (!self && !category.equals("")) continue;
                    if (!seekingPrevious) writeBytes("<!--start-->");
                    if (contents) {
                        try {
                            source = uc.getEntity(requester, msgItem);
                        } catch (IOException ex) {
                            uc.log(getClass().getName(), ex);
                            continue;
                        }
                        if (!seekingPrevious) {
                            if (header) Support.writeHeader(out, htmlOut, headerList, uc.charsetName, true);
                            Support.decodeBody(requester instanceof W3JarRequester ? source : new FeederInputStream(uc, source, requester), out, htmlOut, headerList, headerFields, uc.decoder, uc.charsetName, msgItem.index + "/" + uniqueIDEncoded + "/", requester instanceof W3JarRequester ? (EntityMemory)new EntityArchive(cachePath + uniqueIDEncoded) : (EntityMemory)new EntityCache(cachePath + uniqueIDEncoded), true, true, false, null);
                            htmlOut.write('\n');
                            htmlOut.flush();
                        }
                    } else if (!mobile && !seekingPrevious) {
                        uc.getOthers(headerList, fromAddress, address[0], host, others);
                        String href = path + msgItem.index + "/" + uniqueIDEncoded + "/?host=" + hostEncoded +
                            "&from=" + fromEncoded + (replyToEncoded != null ? "&replyTo=" + replyToEncoded : "") +
                            (to != null ? "&to=" + URLEncoder.encode(to, uc.charsetName) : "") +
                            "&subject=" + (uc.subject != null ? URLEncoder.encode(uc.subject, uc.charsetName) : "") +
                            (others[0] != null ? "&alsoto=" + URLEncoder.encode(others[0], uc.charsetName) : "") +
                            (others[1] != null ? "&alsocc=" + URLEncoder.encode(others[1], uc.charsetName) : "") +
                            (sendHostEncoded != null ? "&sendHost=" + sendHostEncoded : "") +
                            (signEncoded != null ? "&sign=" + signEncoded : "") +
                            (auto ? "&auto=" : "") + (header ? "&header=" : "") +
                            (secureSend ? "&secureSend=" : ""),
                            priority = headerList.getHeaderValue("x-priority");
                        writeBytes("<span class=\"listcolumn0\">");
                        int priorityDelta = 0;
                        if (priority != null) {
                            priorityDelta = Support.uncommentString(priority).compareTo("3");
                            if (priorityDelta < 0) writeBytes("<b>");
                            else if (priorityDelta > 0) writeBytes("<i>");
                        }
                        boolean unread = headerList.getHeaderValue("x-unread") != null;
                        if (unread) writeBytes("<u>");
                        writeBytes("<input name=\"" + Support.htmlString(uniqueID) + "\" type=checkbox value=\"\"> ");
                        uc.writeLine(out, date, (s = headerList.getHeaderValue("content-type")) != null && s.startsWith("multipart/"), (s = headerList.getHeaderValue("content-length")) != null ? s : null, href + (delete ? "&delete=" : "") + (forward ? "&forward=" : "") + (remove ? "&remove=" : ""), target, uc.subject != null ? Support.htmlString(uc.subject) : "(Untitled)", uc.charsetName, mobile);
                        writeBytes("<span class=\"listcolumn0\">" + (servlet && to != null ? "<a href=\"" + (from != null ? href + "&form=\" target=composition>" : "mailto:" + to + "\">") + Support.htmlString(address[1] != null ? address[1] : to) + "</a>" : address[1] != null ? Support.htmlString(address[1]) : "(Unknown)"));
                        if ((s = headerList.getHeaderValue("organization")) != null && !(s = s.trim()).equals(""))
                            Support.writeBytes(htmlOut, ", " + Support.decodeWords(s) + "\0", uc.charsetName);
                        writeBytes("</span>");
                        String allOthers = (others[2] != null ? others[2] : "") + (others[3] != null ? "/" + others[3] : "");
                        if (!allOthers.equals("")) writeBytes(servlet ? "<a href=\"" + (from != null ? href + "&form=&also=\" target=composition>" : "mailto:" + to + "\">") + Support.htmlString(allOthers) + "</a>" : allOthers);
                        if (unread) writeBytes("</u>");
                        if (priorityDelta < 0) writeBytes("</b>");
                        else if (priorityDelta > 0) writeBytes("</i>");
                        writeBytes("<br>\n<hr>\n");
                    } else if (!seekingPrevious) writeBytes("<option value=\"" + (uc.subject != null ? (uc.subject = Support.htmlString(uc.subject)) : "(Untitled)") + "-" + Support.htmlString(uniqueID) + "\">" + (address[1] != null ? Support.htmlString(address[1]) : address[0] != null ? Support.htmlString(address[0]) + " " : "") + (uc.subject != null ? uc.subject : ""));
                    if (!seekingPrevious) writeBytes("\n<!--end-->");
                    total++;
                    if (itemsInPage != 0 && (total >= itemsInPage || uc.index0 == 0)) {
                        if (seekingPrevious) break;
                        currentTotal = total;
                        nextItemOffset = uc.index0;
                        uc.index0 = msgVecStart;
                        total = 0;
                        seekingPrevious = true;
                    }
                }
                writeBytes("<!--footer-->");
                if (servlet && itemsInPage != 0) {
                    int previousItemOffset = uc.index0 - 1;
                    int itemOffsetIndex = query.indexOf("&itemOffset=");
                    if (itemOffsetIndex == -1) itemOffsetIndex = "itemOffset=".length();
                    else itemOffsetIndex += "&itemOffset=".length();
                    int ampersand = query.indexOf('&', itemOffsetIndex);
                    if (ampersand == -1) ampersand = query.length();
                    if (total > 0) writeBytes("<a href=\"" + path + (query != null ? "?" + query.substring(0, itemOffsetIndex) + previousItemOffset + query.substring(ampersand) : "?itemOffset=" + previousItemOffset) + "\" target=\"_self\">" + uc.messages.getString("previous") + "</a><br>\n");
                    if (nextItemOffset > 0) writeBytes("<a href=\"" + path + (query != null ? "?" + query.substring(0, itemOffsetIndex) + nextItemOffset + query.substring(ampersand) : "?itemOffset=" + nextItemOffset) + "\" target=\"_self\">" + uc.messages.getString("next") + "</a><br>\n");
                    total = currentTotal;
                }
                if (!contents) {
                    if (!msgVec.isEmpty()) {
                        if (mobile) writeBytes("</select>");
                        writeBytes("</form>");
                    }
                }
                writeBytes(MessageFormat.format(uc.messages.getString("numberMessages"), new Object[] {new Long(total)}));
                writeBytes("</body></html>");
                break;
            }
            case MOVE: {
                FI.realitymodeler.common.URLDecoder urld = new FI.realitymodeler.common.URLDecoder(true);
                if (!new String(urld.decodeStream(source)).equalsIgnoreCase("from") || urld.c != '=') throw new IOException("Invalid form contents");
                String from0 = new String(urld.decodeStream(source));
                if (urld.c != '&' || !new String(urld.decodeStream(source)).equalsIgnoreCase("category") || urld.c != '=') throw new IOException("Invalid form contents");
                category = new String(urld.decodeStream(source));
                remove = category.equalsIgnoreCase("destruction");
                Object categoryObject = null;
                try {
                    categoryObject = ((W3MsgURLConnection)uc).openCategoryObject(category, from0);
                    while (urld.c == '&') {
                        uniqueID = Support.decode(new String(urld.decodeStream(source), "UTF-8"));
                        if (urld.c != '=') break;
                        if (!msgs.containsKey(uniqueID)) {
                            urld.decodeStream(source);
                            continue;
                        }
                        if (remove) ((W3MsgURLConnection)uc).delete(this);
                        else ((W3MsgURLConnection)uc).move(this, categoryObject);
                        urld.decodeStream(source);
                    }
                } finally {
                    ((W3MsgURLConnection)uc).closeCategoryObject(categoryObject);
                }
                break;
            }
            case SEND: {
                W3File dir = new W3File(((W3MsgURLConnection)uc).getCacheRoot().replace('/', File.separatorChar));
                if (!dir.exists()) dir.mkdirs();
                W3File entityFile = ((W3MsgURLConnection)uc).makeUniqueFile(dir);
                EntityArchive entityArchive = new EntityArchive(entityFile.getPath(), headerList);
                Support.decodeBody(source, null, null, headerList, headerFields, uc.decoder, uc.charsetName, path, entityArchive, false, false, false, null);
                entityArchive.close();
                InetAddress localAddress = InetAddress.getLocalHost(),
                    loopbackAddress = InetAddress.getByName("127.0.0.1");
                String localHostAddress = localAddress.getHostAddress();
                int numOfRefs = 0;
                for (int n = 1; n <= 3; n++) {
                    Iterator iter = recipients.entrySet().iterator();
                    while (iter.hasNext()) {
                        Map.Entry entry = (Map.Entry)iter.next();
                        InetAddress address = (InetAddress)entry.getKey();
                        if (address.equals(localAddress) || address.equals(loopbackAddress)) {
                            StringTokenizer st = new StringTokenizer(((StringBuffer)entry.getValue()).toString());
                            while (st.hasMoreTokens()) {
                                W3File targetDir = new W3File(dir, st.nextToken());
                                if (!targetDir.exists()) targetDir.mkdirs();
                                W3File file = ((W3MsgURLConnection)uc).makeUniqueFile(targetDir);
                                DataOutputStream dout = null;
                                try {
                                    dout = new DataOutputStream(new FileOutputStream(file));
                                    dout.writeUTF(entityFile.getName());
                                } finally {
                                    if (dout != null) dout.close();
                                }
                                numOfRefs++;
                            }
                            iter.remove();
                        } else {
                            W3MsgURLConnection uc = new W3MsgURLConnection(new URL("msg", address.getHostAddress(), "/" + ((StringBuffer)entry.getValue()).toString()));
                            if (uc.verbose) uc.log("Notifying of " + entityFile + " to " + uc);
                            uc.setRequestProperty("X-Msg-Jar", entityFile.getName());
                            uc.setRequestProperty("X-Msg-Host", localHostAddress);
                            try {
                                uc.getInputStream();
                                if (uc.getResponseCode() == HttpURLConnection.HTTP_NO_CONTENT) iter.remove();
                            } catch (IOException ex) {}
                        }
                    }
                    if (recipients.isEmpty()) break;
                }
                if (numOfRefs > 0) {
                    W3File refFile = new W3File(entityFile.getPath() + ".ref");
                    DataOutputStream dout = null;
                    try {
                        dout = new DataOutputStream(new FileOutputStream(refFile));
                        dout.writeInt(numOfRefs);
                    } finally {
                        if (dout != null) dout.close();
                    }
                }
                break;
            }
            default:
                uc.decode(source, out, requester, headerList, headerFields, uc.decoder, cachePath + Support.encode(uniqueID, null), servlet ? path + "?" + query : null, path, header && !plain, true, from != null, !plain);
            }
            if (out != null) out.close();
            if (entity != null) entity.setReadOnly();
        } catch (Exception ex) {
            try {
                if (in != null)
                    synchronized (in) {
                        in.close();
                        in.notifyAll();
                    }
            } catch (IOException ex1) {} finally {
                if (entity != null)
                    try {
                        entityOut.close();
                        entity.delete("");
                    } catch (IOException ex1) {}
                uc.log(getClass().getName(), ex);
            }
        } finally {
            if (lock != null) lock.release();
        }
    }

}

class MsgCacheCleaning extends Thread implements FilenameFilter {
    W3File dir;
    ServletContext servletContext;
    long expirationTime;

    MsgCacheCleaning(ServletContext servletContext, W3File dir, long expirationTime) {
        this.servletContext = servletContext;
        this.dir = dir;
        this.expirationTime = expirationTime;
    }

    public boolean accept(File dir, String name) {
        return !name.endsWith(".ref");
    }

    int cleanCache(W3File dir) throws IOException {
        W3File file;
        int nFiles = 0;
        String list[] = dir.list(this);
        for (int i = 0; i < list.length; i++) {
            if ((file = new W3File(dir, list[i] + ".ref")).exists()) {
                DataInputStream din = null;
                try {
                    din = new DataInputStream(new FileInputStream(file));
                    int numOfRefs = din.readInt();
                    if (numOfRefs > 0) continue;
                } finally {
                    din.close();
                }
            }
            new W3File(dir, list[i]).delete();
            nFiles++;
        }
        return nFiles;
    }

    public void run() {
        try {
            int nFiles = cleanCache(dir);
            W3URLConnection.log(servletContext, nFiles + " files in cache " + dir.getPath() + " removed.");
        } catch (Exception ex) {
            W3URLConnection.log(servletContext, getClass().getName(), ex);
        }
    }

}

/** Implements URL connection to HTTP-servers functioning as message pools. URL must be of form:<br>
    When getting input stream of message list:<br>
    msg://host[/from]<br>
    [?auto= : quote original automatically<br>
    &amp;contents= : show in list also message contents<br>
    &amp;date=messages newer than date are listed<br>
    &amp;from=reply email address<br>
    &amp;only=only messages newer than cookie value or date are listed]<br>
    When getting input stream of specified message:<br>
    msg://host/message number/message id<br>
    [?delete= : message is only deleted<br>
    &amp;forward= : get message as raw text<br>
    &amp;header= : show message header<br>
    &amp;plain= : only plain text is returned from message<br>
    &amp;remove= : removes message after reading]<br>
    When getting output stream of unique id list of messages to be moved to some category:<br>
    msg://host<br>
    Content-Type-header must be in this case application/x-www-form-urlencoded.<br>
    When getting output stream for mailing to specific recipients:<br>
    msg:recipients/subject/from<br>
    Basic credentials must be in request property Authorization.
*/
public class W3MsgURLConnection extends W3SmtpURLConnection implements Cloneable, Comparator<MsgItem> {
    public static long cacheCleaningInterval = 24L * 60L * 60L * 1000L, cacheCleaningTime = 0L;
    public static String cacheRoot = "cache/msg/";
    public static boolean useProxy = false;
    public static String proxyHost = null;
    public static int proxyPort = 80;
    public static String msgServerHost = null;

    Map<String,MsgItem> msgs = null;
    MsgPipingThread piping = null;
    String cachePath, host;
    Vector<MsgItem> msgVec = null;
    boolean forward = false;

    public Object clone() {
        W3MsgURLConnection uc = new W3MsgURLConnection(url);
        uc.requester = requester;
        uc.msgs = msgs;
        uc.cachePath = cachePath;
        uc.host = host;
        uc.msgVec = msgVec;
        uc.loggedIn = loggedIn;
        return uc;
    }

    public int compare(MsgItem o1, MsgItem o2) {
        long delta = o1.time - o2.time;
        return delta < 0L ? -1 : delta == 0L ? 0 : 1;
    }

    public String getCacheRoot() {
        return cacheRoot;
    }

    public synchronized HeaderList getHeader(W3Requester requester, MsgItem msgItem) throws IOException, ParseException {
            return (HeaderList)msgItem.headerList.clone();
        }

    public synchronized InputStream getEntity(W3Requester requester, MsgItem msgItem) throws IOException, ParseException {
            ((W3JarRequester)requester).open(cachePath + msgItem.msgId);
            return ((W3JarRequester)requester).jarFile.getInputStream(((W3JarRequester)requester).jarFile.getEntry("1"));
        }

    void delete(MsgPipingThread piping) throws IOException {
        String name;
        DataInputStream din = null;
        try {
            din = new DataInputStream(new FileInputStream(new W3File(cachePath + username + "/" + Support.encode(piping.uniqueID, null))));
            name = din.readUTF();
        } finally {
            if (din != null) din.close();
        }
        W3File refFile;
        int numOfRefs;
        din = null;
        try {
            din = new DataInputStream(new FileInputStream(refFile = new W3File(cachePath + name + ".ref")));
            numOfRefs = din.readInt();
        } finally {
            if (din != null) din.close();
        }
        if (numOfRefs <= 1) {
            new W3File(cachePath + name).delete();
        }
        DataOutputStream dout = null;
        try {
            dout = new DataOutputStream(new FileOutputStream(refFile));
            dout.writeInt(--numOfRefs);
        } finally {
            dout.close();
        }
    }

    Object openCategoryObject(String category, String from) throws IOException {
        return null;
    }

    void move(MsgPipingThread piping, Object categoryObject) throws IOException {
    }

    void closeCategoryObject(Object categoryObject) {
    }

    W3File makeUniqueFile(W3File dir) {
        W3File file;
        String baseName = Long.toString(System.currentTimeMillis(), Character.MAX_RADIX);
        for (int n = 0;; n++)
            if (!(file = new W3File(dir, baseName + (n > 0 ? "_" + Integer.toString(n, Character.MAX_RADIX) : ""))).exists()) return file;
    }

    class SessionObject {
        Map<String,MsgItem> msgs;
        Vector<MsgItem> msgVec;
        int first, last;
    }

    public static void cleanCache(ServletContext servletContext) {
        cacheCleaningTime = System.currentTimeMillis();
        W3File dir = new W3File(cacheRoot);
        if (dir.exists()) new MsgCacheCleaning(servletContext, dir, cacheCleaningTime - cacheCleaningInterval).start();
    }

    public W3MsgURLConnection(URL url) {
        super(url);
    }

    public void checkCacheCleaning() {
        if (cacheCleaningInterval >= 0L &&
            System.currentTimeMillis() - cacheCleaningTime > cacheCleaningInterval) cleanCache(servletContext);
    }

    public void saveSession() {
        if (httpSession != null) {
            SessionObject sessionObject = (SessionObject)httpSession.getAttribute(getClass().getName() + "/sessionObject/" + host + "/" + username);
            if (sessionObject == null) sessionObject = new SessionObject();
            sessionObject.msgs = msgs;
            sessionObject.msgVec = msgVec;
            sessionObject.first = first;
            sessionObject.last = last;
            httpSession.setAttribute(getClass().getName() + "/sessionObject/" + host + "/" + username, sessionObject);
        }
    }

    public boolean list() throws IOException {
        if (!reloadNeeded() && httpSession != null) {
            SessionObject sessionObject = (SessionObject)httpSession.getAttribute(getClass().getName() + "/sessionObject/" + host + "/" + username);
            if (sessionObject != null) {
                msgs = sessionObject.msgs;
                msgVec = sessionObject.msgVec;
                first = sessionObject.first;
                last = sessionObject.last;
                return true;
            }
        }
        msgVec = new Vector<MsgItem>();
        msgs = new HashMap<String,MsgItem>();
        if (!useCaches) return false;
        File dir = new File(cachePath);
        if (!dir.exists()) return false;
        String list[] = dir.list();
        for (int i = 0; i < list.length; i++) {
            String name = list[i];
            if (!name.endsWith("_")) continue;
            File file = new File(dir, name);
            FileInputStream fin = new FileInputStream(file);
            try {
                HeaderList headerList = new HeaderList(fin);
                String id = headerList.getHeaderValue("message-id");
                if (id == null) id = headerList.getHeaderValue("-");
                MsgItem msgItem = new MsgItem(headerList, id, id, i);
                msgVec.addElement(msgItem);
                msgs.put(id, msgItem);
            } catch (ParseException ex) {
                log(getClass().getName(), ex);
            }
            fin.close();
        }
        Collections.sort(msgVec, (Comparator<MsgItem>)this);
        return false;
    }

    public void doConnect()
        throws IOException {
        if (requester != null || doOutput) return;
        String msgJar = getRequestProperty("X-Msg-Jar");
        if (msgJar != null) {
            // this server is explicitly notifed of new message, which is tried to read from origin host
            String msgHost = getRequestProperty("X-Msg-Host");
            W3File dir = new W3File(getCacheRoot().replace('/', File.separatorChar));
            if (!dir.exists()) dir.mkdirs();
            W3File entityFile = makeUniqueFile(dir);
            try {
                W3MsgURLConnection uc = new W3MsgURLConnection(new URL("msg", msgHost, "/" + msgJar));
                if (verbose) log("Getting message from " + uc);
                try {
                    uc.setRequestProperty("X-Msg-Host", url.getHost());
                    InputStream in = uc.getInputStream();
                    if (verbose)
                        synchronized (System.out) {
                            Support.sendMessage(System.out, (HeaderList)uc.getHeaderFields());
                        }
                    byte b[] = new byte[Support.bufferLength];
                    BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(entityFile));
                    try {
                        for (int n; (n = in.read(b)) > 0;) out.write(b, 0, n);
                    } finally {
                        out.close();
                    }
                } finally {
                    uc.disconnect();
                }
                if (verbose) log("Got message file " + entityFile + " of length " + entityFile.length());
                int numOfRefs = 0;
                StringTokenizer st = new StringTokenizer(path, "/");
                st = new StringTokenizer(st.nextToken(), ",");
                while (st.hasMoreTokens()) {
                    W3File targetDir = new W3File(dir, st.nextToken());
                    if (!targetDir.exists()) targetDir.mkdirs();
                    W3File file = makeUniqueFile(targetDir);
                    DataOutputStream dout = null;
                    try {
                        dout = new DataOutputStream(new FileOutputStream(file));
                        dout.writeUTF(entityFile.getName());
                    } finally {
                        if (dout != null) dout.close();
                    }
                    numOfRefs++;
                }
                if (numOfRefs > 0) {
                    W3File refFile = new W3File(entityFile.getPath() + ".ref");
                    DataOutputStream dout = null;
                    try {
                        dout = new DataOutputStream(new FileOutputStream(refFile));
                        dout.writeInt(numOfRefs);
                    } finally {
                        if (dout != null) dout.close();
                    }
                }
            } finally {
                entityFile.delete();
            }
            headerFields = new HeaderList();
            headerFields.append(new Header("", protocol_1_1 + " " + HTTP_NO_CONTENT));
            inputDone = mayKeepAlive = true;
            cached = false;
            in = null;
            return;
        }
        String msgHost = getRequestProperty("X-Msg-Host");
        if (msgHost != null) {
            // remote server is requesting message targeted to it
            File file = new File(getCacheRoot(), path);
            if (verbose) log("Returning " + file + " to " + msgHost);
            in = origin = new BufferedInputStream(new FileInputStream(file));
            headerFields = new HeaderList();
            headerFields.append(new Header("", protocol + " " + HTTP_OK + " OK"));
            headerFields.replace(new Header("Last-Modified", Support.format(new Date(lastModified = file.lastModified()))));
            headerFields.replace(new Header("Content-Type", contentType = "application/x-msg"));
            headerFields.replace(new Header("Content-Length", String.valueOf(contentLength = (int)file.length())));
            if (verbose)
                synchronized (System.out) {
                    Support.sendMessage(System.out, headerFields);
                }
            inputDone = mayKeepAlive = true;
            cached = false;
            return;
        }
        if ((host = url.getHost()).equals("") && (host = msgServerHost) == null &&
            (host = System.getProperty("msg.host")) == null) throw new IOException("No host");
        getBasicCredentials();
        if (username == null || username.equals("")) {
            in = origin = getUnauthorizedStream("Messaging authorization required");
            inputDone = true;
            return;
        }
        InetAddress addr = InetAddress.getByName(host);
        boolean local = addr.equals(InetAddress.getByName("127.0.0.1")) || addr.equals(InetAddress.getLocalHost());
        if (!local) {
            if (useProxy) proxyConnect(proxyHost, proxyPort);
            else proxyConnect(host, url.getPort() != -1 ? url.getPort() : proxyPort);
            return;
        }
        requester = new W3JarRequester();
        StringTokenizer st = new StringTokenizer(path, "/");
        if (st.countTokens() > 2 && st.nextToken().indexOf('@') == -1)
            try {
                String name;
                DataInputStream din = null;
                try {
                    din = new DataInputStream(new FileInputStream(new W3File(getCacheRoot() + username + "/" + st.nextToken())));
                    name = din.readUTF();
                } finally {
                    if (din != null) din.close();
                }
                ((W3JarRequester)requester).open(getCacheRoot() + name);
                JarEntry jarEntry = ((W3JarRequester)requester).jarFile.getJarEntry(st.nextToken());
                in = ((W3JarRequester)requester).jarFile.getInputStream(jarEntry);
                headerFields = new HeaderList();
                headerFields.append(new Header("", protocol + " " + HTTP_OK + " OK"));
                byte extra[] = jarEntry.getExtra();
                if (extra != null) HeaderList.parseHeaders(new ByteArrayInputStream(extra), headerFields, false);
                headerFields.replace(new Header("Last-Modified", Support.format(new Date(lastModified = jarEntry.getTime()))));
                headerFields.replace(new Header("Content-Length", String.valueOf(contentLength = (int)jarEntry.getSize())));
                inputDone = mayKeepAlive = true;
                cached = false;
                return;
            } catch (ParseException ex) {
                throw new IOException(Support.stackTrace(ex));
            }
        loggedIn = true;
        useCaches = false;
        msgs = new HashMap<String,MsgItem>();
        first = last = 0;
        File dir = new File(getCacheRoot() + username + "/");
        if (!dir.exists()) return;
        String list[] = dir.list();
        msgVec = new Vector<MsgItem>(list.length);
        for (int i = 0; i < list.length; i++) {
            String name = list[i], msgName;
            DataInputStream din = null;
            try {
                din = new DataInputStream(new FileInputStream(new File(dir, name)));
                msgName = din.readUTF();
            } finally {
                if (din != null) din.close();
            }
            HeaderList headerList = new HeaderList();
            ((W3JarRequester)requester).open(cachePath + name);
            Manifest manifest = ((W3JarRequester)requester).jarFile.getManifest();
            Attributes attributes = manifest.getMainAttributes();
            Iterator iter = attributes.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry entry = (Map.Entry)iter.next();
                Attributes.Name key = (Attributes.Name)entry.getKey();
                if (key.equals(Attributes.Name.MANIFEST_VERSION)) continue;
                headerList.append(new Header(key.toString(), (String)entry.getValue()));
            }
            msgVec.addElement(new MsgItem(headerList, msgName, name, i));
        }
        Collections.sort(msgVec, this);
        last = msgVec.size() - 1;
        for (int i = 0; i <= last; i++) {
            MsgItem msgItem = msgVec.elementAt(i);
            msgs.put(msgItem.msgId, msgItem);
        }
    }

    public InputStream getInputStream() throws IOException {
        if (inputDone) return in;
        if (doOutput) {
            closeStreams();
            inputDone = true;
            useCaches = false;
            if (useProxy && mayKeepAlive) {
                if (requester == null) throw new IOException("Not connected");
                in = requester.input;
                checkInput();
                return in;
            }
            if (!loggedIn) return in = getUnauthorizedStream("Authorization required");
            cached = false;
            mayKeepAlive = true;
            headerFields = new HeaderList();
            headerFields.append(new Header("", protocol_1_1 + " " + HTTP_NO_CONTENT));
            return in = null;
        }
        try {
            connect();
        } catch (LoginException ex) {}
        if (inputDone) return in;
        inputDone = true;
        setRequestMethod();
        setRequestFields();
        if (useProxy) return getInput(false);
        if (!loggedIn) return in = getUnauthorizedStream("Authorization required");
        mayKeepAlive = true;
        filling = piping = new MsgPipingThread(this);
        boolean only = false;
        String date = null, item = null;
        piping.mobile = isMobile();
        if (query != null) {
            HashMap<String,Object> queryPars = new HashMap<String,Object>();
            parseQuery(query, queryPars);
            piping.auto = Support.getParameter(queryPars, "auto") != null;
            piping.category = getRequestProperty("x-category");
            piping.categories = getRequestProperty("x-categories");
            piping.contents = Support.getParameter(queryPars, "contents") != null;
            date = Support.getParameter(queryPars, "date");
            piping.delete = Support.getParameter(queryPars, "delete") != null;
            piping.forward = forward = Support.getParameter(queryPars, "forward") != null;
            piping.from = Support.getParameter(queryPars, "from");
            piping.header = Support.getParameter(queryPars, "header") != null;
            item = Support.getParameter(queryPars, "item");
            only = Support.getParameter(queryPars, "only") != null;
            piping.plain = Support.getParameter(queryPars, "plain") != null;
            piping.remove = Support.getParameter(queryPars, "remove") != null;
            piping.replyTo = Support.getParameter(queryPars, "replyTo");
            if ((piping.signEncoded = Support.getParameter(queryPars, "sign")) != null) piping.signEncoded = URLEncoder.encode(piping.signEncoded, charsetName);
            if ((piping.sendHostEncoded = Support.getParameter(queryPars, "sendHost")) != null) piping.sendHostEncoded = URLEncoder.encode(piping.sendHostEncoded, charsetName);
            piping.secureSend = Support.getParameter(queryPars, "secureSend") != null;
            String value = Support.getParameter(queryPars, "itemOffset");
            if (value != null) {
                piping.itemOffset = Integer.parseInt(value);
                value = Support.getParameter(queryPars, "itemsInPage");
                piping.itemsInPage = value != null ? Integer.parseInt(value) : 10;
            }
        }
        piping.time = only ? getTime(date, getCookies(getRequestHeaderFields())) : 0L;
        piping.lock = null;
        StringTokenizer st = new StringTokenizer(path, "/");
        if (st.hasMoreTokens()) {
            String s = st.nextToken().trim();
            if (s.indexOf('@') == -1) {
                index = Integer.parseInt(s);
                if (st.hasMoreTokens()) piping.uniqueID = Support.decode(st.nextToken());
            } else piping.from = s;
        }
        if (piping.from != null && piping.from.equals("")) piping.from = null;
        if (piping.replyTo != null && piping.replyTo.equals("")) piping.replyTo = null;
        checkCacheCleaning();
        piping.msgVec = msgVec;
        piping.msgs = msgs;
        piping.headerFields = new HeaderList();
        piping.headerFields.append(new Header("", protocol_1_1 + " " + HTTP_OK + " OK"));
        headerFields = piping.headerFields;
        if (piping.uniqueID != null || item != null) {
            if (piping.delete) {
                piping.remove = true;
                return in = null;
            }
            if (piping.mobile) piping.plain = true;
            if (item != null) piping.uniqueID = item.substring(item.lastIndexOf('-') + 1);
            MsgItem msgItem = piping.msgs.get(piping.uniqueID);
            if (msgItem == null) throw new IOException("No message found");
            try {
                in = getEntity(requester, msgItem);
                headerList = piping.headerList = (HeaderList)msgItem.headerList.clone();
            } catch (ParseException ex) {
                throw new IOException(ex.toString());
            }
            if (forward) {
                Support.copyHeaderList(piping.headerList, piping.headerFields);
                return checkMessage(true);
            }
            try {
                getHeader(in, piping.headerList, piping.headerFields, !piping.plain);
            } catch (ParseException ex) {
                throw new IOException(ex.toString());
            }
        } else {
            piping.headerFields.append(new Header("Content-Type", Support.htmlType));
            piping.headerFields.append(new Header("Cache-Control", "no-cache"));
            piping.headerFields.append(new Header("Pragma", "no-cache"));
            if (only) setTime();
            piping.action = piping.LIST;
            Support.copyHeaderList(piping.headerFields, headerList = piping.headerList = new HeaderList());
        }
        if (params != null) piping.target = params.get("target");
        piping.servlet = requestRoot != null;
        piping.path = piping.servlet ? requestRoot : path.endsWith("/") ? "" : path.substring(path.lastIndexOf('/') + 1) + "/";
        piping.setup(this);
        in = new BufferedInputStream(piping.in = new PipedInputStream(piping.out = new PipedOutputStream()));
        piping.start();
        return checkMessage(true);
    }

    public OutputStream getOutputStream() throws IOException {
        doOutput = true;
        connect();
        if (!loggedIn) throw new LoginException("Authorization required", 
                                                url.getHost() + (getDefaultPort() != -1 ? ":" + (url.getPort() != -1 ? url.getPort() : getDefaultPort()) : ""));
        setRequestMethod();
        setDefaultRequestFields();
        if (useProxy) {
            Support.sendMessage(requester.output, requestHeaderFields);
            return requester.output;
        }
        filling = piping = new MsgPipingThread(this);
        piping.authorization = getRequestProperty("authorization");
        piping.msgVec = msgVec;
        piping.msgs = msgs;
        PipedInputStream pin = new PipedInputStream();
        sink = new PipedOutputStream(pin);
        in = pin;
        piping.setup(this);
        String s = getRequestProperty("content-type");
        if (s == null || !(s = s.trim()).equalsIgnoreCase("application/x-www-form-urlencoded")) {
            piping.action = piping.SEND;
            piping.headerList = piping.headerFields = requestHeaderFields;
            setRequestPropertiesFromQuery();
            String address[] = new String[2], to = mailRequestProperties(address, msgServerHost);
            if (to != null) piping.to(to);
        } else piping.action = piping.MOVE;
        piping.lock = new W3Lock(true);
        piping.start();
        return sink;
    }

    public void complete() throws IOException {
        if (!useProxy && piping != null && piping.action == 0 && piping.remove && msgs.containsKey(piping.uniqueID)) delete(piping);
    }

    public synchronized void closeStreams() {
            if (!useProxy) {
                if (piping != null && piping.isAlive() && piping.lock != null)
                    // Waits for output pipe stream to complete
                    try {
                        piping.lock.lock();
                        piping = null;
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                        return;
                    }
            }
            super.closeStreams();
        }

    public boolean usingProxy() {
        return useProxy;
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public int getProxyPort() {
        return proxyPort;
    }

    public boolean getForward() {
        return forward;
    }

}
