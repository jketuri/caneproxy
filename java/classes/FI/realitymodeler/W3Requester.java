
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import java.io.*;
import java.lang.reflect.*;
import java.net.*;
import java.util.zip.*;

/** Network client which supports secure and compressed connections. */
public class W3Requester implements Cloneable {
    public Socket socket = null;
    public BufferedInputStream input = null;
    public BufferedOutputStream output = null;
    public SecureSocketsLayer ssl = null;
    public String host = null;
    public int connectTimeout = 0;
    public int timeout = 0;
    public int port = -1;

    public W3Requester(String host, int port, int type, InetAddress localAddress, int localPort)
        throws IOException {
        open(host, port, type, localAddress, localPort);
    }

    public W3Requester(String host, int port, int type)
        throws IOException {
        open(host, port, type);
    }

    public W3Requester(String host, int port)
        throws IOException {
        open(host, port);
    }

    public W3Requester() {
    }

    public W3Requester copy(W3Requester requester) {
        requester.socket = socket;
        requester.input = input;
        requester.output = output;
        requester.ssl = ssl;
        return requester;
    }

    public Object clone() {
        return copy(new W3Requester());
    }

    public synchronized Socket getSocket() {
        return socket;
    }

    public synchronized boolean isConnected() {
        return socket != null && socket.isConnected();
    }

    public synchronized boolean isOpen() {
        return socket != null;
    }

    public synchronized void close()
        throws IOException {
        if (socket == null) return;
        try {
            socket.close();
        } finally {
            socket = null;
            if (ssl != null) ssl.clear();
        }
    }

    public void setStreams(int type)
        throws IOException {
        InputStream in;
        OutputStream out;
        if ((type & W3Socket.SECURE) != 0)
            try {
                if (ssl == null) ssl = new SecureSocketsLayer();
                ssl.setSocket(socket);
                ssl.connect();
                in = ssl.getInputStream();
                out = ssl.getOutputStream();
            } catch (IllegalAccessException ex) {
                throw new IOException(Support.stackTrace(ex));
            }
        else {
            in = input != null ? input : socket.getInputStream();
            out = output != null ? output : socket.getOutputStream();
        }
        if ((type & W3Socket.COMPRESSED) != 0) {
            in = new InflaterFillInputStream(in, new Inflater(true));
            out = new DeflaterFlushOutputStream(out, new Deflater(Deflater.BEST_SPEED, true));
        }
        input = new BufferedInputStream(in);
        output = new BufferedOutputStream(out);
    }

    public synchronized void open(String host, int port, int type, InetAddress localAddress, int localPort)
        throws IOException {
        if (socket != null) close();
        this.host = host;
        this.port = port;
        socket = new W3Socket(type);
        SocketAddress socketAddress = new InetSocketAddress(host, port);
        if (localAddress != null || localPort != 0) {
            SocketAddress localSocketAddress = new InetSocketAddress(localAddress, localPort);
            socket.bind(localSocketAddress);
        }
        if (timeout > 0)
            socket.setSoTimeout(timeout);
        if (connectTimeout > 0)
            socket.connect(socketAddress, connectTimeout);
        else socket.connect(socketAddress);
        //socket.setKeepAlive(true);
        //socket.setSoLinger(true, 0);
        input = new BufferedInputStream(socket.getInputStream());
        output = new BufferedOutputStream(socket.getOutputStream());
    }

    public synchronized void open(String host, int port, int type)
        throws IOException {
        open(host, port, type, null, 0);
    }

    public synchronized void open(String host, int port)
        throws IOException {
        open(host, port, W3Socket.NORMAL);
    }

    public void send(String command)
        throws IOException {
        Support.writeBytes(output, command, null);
        output.flush();
    }

    public void setConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public int getConnectTimeout() {
        return connectTimeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public int getTimeout() {
        return timeout;
    }

    public String makeRealm() {
        return host + (port != -1 ? ":" + port : "");
    }

}
