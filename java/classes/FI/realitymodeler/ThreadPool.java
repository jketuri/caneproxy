
package FI.realitymodeler;

import java.util.*;

/** Pool of waiting threads. */
public class ThreadPool {
    long timeout = 0L;
    WeakHashMap<PoolThread,Boolean> pool = new WeakHashMap<PoolThread,Boolean>();

    /** Constructs thread pool. */
    public ThreadPool() {
    }

    /** Constructs thread pool with specified timeout for idle threads. */
    public ThreadPool(long timeout) {
        this.timeout = timeout;
    }

    /** Returns number of waiting threads. */
    public int poolCount() {
        return pool.size();
    }

    /** Reserves thread from pool.
        @param thread used for cloning if no available found in the pool
        @return reserved thread */
    public PoolThread reserve(PoolThread thread) {
        if (thread.isAlive()) thread.unlock();
        else thread.start();
        try {
            synchronized (pool) {
                pool.remove(thread = pool.keySet().iterator().next());
                thread.released = false;
            }
        } catch (NoSuchElementException ex) {
            thread = (PoolThread)thread.clone();
            thread.pool = this;
        }
        return thread;
    }

    /** Releases thread to pool and starts to wait for reserving.
        @param thread thread to be released
        @return true if thread was not reserved and should be stopped. */
    public boolean release(PoolThread thread) throws InterruptedException {
        thread.released = true;
        synchronized (pool) {
            pool.put(thread, Boolean.TRUE);
        }
        for (;;)
            if (thread.lock(timeout)) return false;
            else synchronized (pool) {
                    if (thread.released) {
                        pool.remove(thread);
                        return true;
                    }
                }
    }

}
