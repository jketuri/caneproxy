
package FI.realitymodeler;

import java.io.*;
import java.util.zip.*;

public class GZIPFillInputStream extends GZIPInputStream {

    public GZIPFillInputStream(InputStream in) throws IOException {
        super(in);
    }

    public int read(byte b[], int off, int len) throws IOException {
        try {
            for (;;) {
                int n = super.read(b, off, len);
                if (n <= 0) {
                    n = inf.getRemaining();
                    inf.reset();
                    if (n > 0) inf.setInput(this.buf, this.len - n, n);
                    continue;
                }
                return n;
            }
        } catch (EOFException ex) {
            return -1;
        }
    }

}
