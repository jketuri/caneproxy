package FI.realitymodeler.cimd;

import java.io.*;
import java.net.*;
import java.sql.*;
import java.util.*;

/** This class is a super class of various operations modifying CIMD messages.
 *  One type of CIMD message can have one kind of operation.
 *  handle-method can modify CIMD message in any way it pleases,
 *  removing, changing or adding parameters, or even changing message type.
 *  Updated message is then forwarded to second party.
 *  If handle method returns true, message is not forwarded at all.
 */
public class CimdOperation {
    public static final String alive = "40",
        aliveResponse = "90",
        login = "01",
        deliveryRequest = "05",
        deliveryRequestResponse = "55",
        deliver = "20",
        deliverResponse = "70",
        deliverStatusReport = "23",
        deliverStatusReportResponse = "73",
        submit = "03",
        submitResponse = "53",
        dataCodingSchemeType = "030",
        destinationAddressType = "021",
        errorCodeType = "900",
        errorTextType = "901",
        modeType = "068",
        originatorAddressType = "023",
        passwordType = "011",
        serviceCentreTimeStampType = "060",
        tariffClassType = "064",
        userDataHeaderType = "032",
        userDataType = "033",
        userDataBinaryType = "034",
        userIdentityType = "010";
    public static final CimdParameter destinationAddressParameter = new CimdParameter(destinationAddressType, null),
        errorCodeParameter = new CimdParameter(errorCodeType, null),
        dataCodingSchemeParameter = new CimdParameter(dataCodingSchemeType, null),
        modeParameter = new CimdParameter(modeType, null),
        originatorAddressParameter = new CimdParameter(originatorAddressType, null),
        passwordParameter = new CimdParameter(passwordType, null),
        tariffClassParameter = new CimdParameter(tariffClassType, null),
        userDataParameter = new CimdParameter(userDataType, null),
        userDataBinaryParameter = new CimdParameter(userDataBinaryType, null),
        userIdentityParameter = new CimdParameter(userIdentityType, null);

    CimdEngine cimdEngine;

    public CimdOperation(CimdEngine cimdEngine) {
        this.cimdEngine = cimdEngine;
    }

    /** This method is called when request message was received.
     * @param request request message which was received
     * @return must return true if connection should be aborted
     */
    public boolean handle(CimdMessage request) throws IOException, SQLException, SocketException {
        return false;
    }

    /** This method is called when response to request message was received or
     *  if request was skipped, response must be set to relevant value.
     *  This method is also called again when response to extra message was received or
     *  if extra message was skipped, that response must be set to relevat value.
     *  If there is also extra message this method is called once for response and for message.
     * @param request request message which was received earlier.
     * @param response response message which was received.
     * @param message extra message if received.
     * @param messageResponse response to extra message if received.
     * @return must return true if extra response message is anticipated
     */
    public boolean handle(CimdMessage request, CimdMessage response, CimdMessage message, CimdMessage messageResponse) throws IOException, SQLException, SocketException {
        return false;
    }

    /** This method should return extra message if it is required to be send to server.
     * @param message request message which was received earlier.
     * @param response response message which was received.
     * @param message extra message if received.
     * @return must return extra message if it is required to be send to server or null
     */
    public CimdMessage sendExtra(CimdMessage request, CimdMessage response, CimdMessage message, CimdMessage messageResponse) throws IOException, SQLException, SocketException {
        return null;
    }

    /** This method is called when response has been sent back to client.
     *  This method can log the transaction.
     *  @param request request message which was received from the client.
     *  @param response response message which was sent to the client.
     *  @param message extra message which was sent to client or null if there was none.
     *  @param messageResponse response to extra message or null if there was no extra message.
     *  @param extraRequest extra request which was sent to server or null if there was none.
     *  @param extraResponse response message from extra request sent to server
     */
    public void log(CimdMessage request, CimdMessage response,
                    CimdMessage message, CimdMessage messageResponse,
                    CimdMessage extraRequest, CimdMessage extraResponse) throws IOException, SQLException {
    }

    /** Makes confirmation message to be sent to phone in case message's price exceeds
     *  maximum price specifed in short number profile.
     *  @param message message which was submitted or delivered and causes over priced message
     *  @param servicePrice service price which exceed the maximum price
     *  @return confirmation message
     */
    public CimdMessage makeMaxPriceCimdMessage(CimdMessage message, double servicePrice) {
        MaxPriceMessage maxPriceMessage = new MaxPriceMessage(cimdEngine);
        maxPriceMessage.price = String.valueOf(servicePrice);
        Random random = new Random(new Random().nextLong());
        long ctm;
        synchronized (cimdEngine) {
            do {
                ctm = System.currentTimeMillis();
            } while (ctm == cimdEngine.lastCtm);
            cimdEngine.lastCtm = ctm;
        }
        StringBuffer stringBuffer = new StringBuffer(Long.toString(ctm, Character.MAX_RADIX));
        while (stringBuffer.length() < 13) stringBuffer.insert(0, '0');
        String randomValue = Integer.toString(Math.abs(random.nextInt()), Character.MAX_RADIX);
        for (int length = 13 + randomValue.length(); length < 20; length++) stringBuffer.append('0');
        stringBuffer.append(randomValue);
        maxPriceMessage.randomKey = stringBuffer.toString();
        maxPriceMessage.shortNumber = message.shortNumber;
        maxPriceMessage.time = System.currentTimeMillis();
        message.randomKey = maxPriceMessage.randomKey;
        String maxPriceMessageText = encodeData(maxPriceMessage.toString(cimdEngine.maxPriceMessage));
        CimdMessage submitMessage = new CimdMessage();
        submitMessage.operationCode = submit;
        submitMessage.parameterList.add(new CimdParameter(destinationAddressType, message.msisdn));
        submitMessage.parameterList.add(new CimdParameter(originatorAddressType, message.shortNumber));
        submitMessage.parameterList.add(new CimdParameter(dataCodingSchemeType, "0"));
        submitMessage.parameterList.add(new CimdParameter(userDataType, maxPriceMessageText));
        cimdEngine.maxPriceCimdMessages.put(message.randomKey, message);
        return submitMessage;
    }

    /** Logs Submit or Deliver request to CDR.
     *  @param request request in question
     *  @param response response to request
     *  @param responseOperationCode response operation code which should match with response
     */
    public void logMessage(CimdMessage request, CimdMessage response, String responseOperationCode) throws IOException {
        if (response.operationCode.equals(responseOperationCode)
            && response.parameterList.indexOf(errorCodeParameter) == -1)
            request.successIndicator = "1";
        else request.successIndicator = "2";
        if (request.operationCode.equals(deliver)) {
            if (request.billingProfileMOStatus) {
                request.servicePrice = formatServicePrice(request.servicePriceMO);
                if (!request.billingProfileMTStatus) request.billingProfile = "10";
                else request.billingProfile = "31";
                cimdEngine.writeCdrLine(request);
            }
            return;
        }
        if (request.billingProfileMTStatus) {
            request.servicePrice = formatServicePrice(request.servicePriceMT);
            if (!request.billingProfileMOStatus) request.billingProfile = "20";
            else request.billingProfile = "32";
            cimdEngine.writeCdrLine(request);
        }
        if (request.billingProfileMEGAPUSHStatus) {
            request.servicePrice = formatServicePrice(request.servicePriceMEGAPUSH);
            request.billingProfile = "40";
            cimdEngine.writeCdrLine(request);
        }
    }

    /** Gets text sent with short message in UserData or UserDataBinary parameters.
     *  Decodes it from encoded GSM default character set if needed.
     *  @param message CIMD message whose text content must be fetched
     */
    public static String getUserText(CimdMessage message) {
        boolean isBinary = false;
        int parameterIndex = message.parameterList.indexOf(dataCodingSchemeParameter);
        if (parameterIndex != -1) {
            // As specified in GSM 03.38
            int smsDataCodingScheme = Integer.parseInt((message.parameterList.get(parameterIndex)).parameterValue);
            // Coding Group Bits is 00xx
            if ((smsDataCodingScheme & 0xc0) == 0) {
                // Bit 3 and Bit 2 are something other than 0 0
                if ((smsDataCodingScheme & 0xc) != 0) isBinary = true;
                // Coding Group Bits is 1111
            } else if ((smsDataCodingScheme & 0xf0) == 0xf0)
                // Bit 2 is other than 0
                if ((smsDataCodingScheme & 0x4) != 0) isBinary = true;
        }
        parameterIndex = message.parameterList.indexOf(userDataParameter);
        if (parameterIndex == -1) {
            parameterIndex = message.parameterList.indexOf(userDataBinaryParameter);
            if (parameterIndex == -1) return null;
            String userData = message.parameterList.get(parameterIndex).parameterValue;
            StringBuffer sb = new StringBuffer();
            int l = userData.length();
            for (int i = 0; i < l; i += 2) sb.append((char)Integer.parseInt(userData.substring(i, i + 2), 16));
            return sb.toString();
        }
        String userText = message.parameterList.get(parameterIndex).parameterValue;
        return isBinary ? userText : decodeData(userText);
    }

    /** Decodes data according to Section 6 Default user data character conversion in
     *  Nokia Artus Messaging Centers CIMD User's Guide.
     *  @param data data to be decoded
     */
    public static String decodeData(String data) {
        int l = data.length();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < l; i++) {
            char ch = data.charAt(i);
            if (ch == '_')
                if (i < l - 2) {
                    Character character = CimdEngine.codes.get(data.substring(i + 1, i + 3));
                    if (character != null) {
                        sb.append(character.charValue());
                        i += 2;
                        continue;
                    }
                }
            sb.append(ch);
        }
        return sb.toString();
    }

    /** Encodes data according to Section 6 Default user data character conversion in
     *  Nokia Artus Messaging Centers CIMD User's Guide.
     *  @param data data to be encoded
     */
    public static String encodeData(String data) {
        int l = data.length();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < l; i++) {
            char ch = data.charAt(i);
            String combi = CimdEngine.combis.get(new Character(ch));
            if (combi != null) sb.append('_').append(combi);
            else sb.append(ch);
        }
        return sb.toString();
    }

    public static String formatServicePrice(double servicePrice) {
        StringBuffer sb = new StringBuffer(String.valueOf((long)(servicePrice * 1000.0)));
        while (sb.length() < 5) sb.insert(0, '0');
        return sb.toString();
    }

    public static void main(String argv[]) throws Exception {
        System.out.println(decodeData(argv[0]));
        //        System.out.println(formatServicePrice(Double.parseDouble(argv[0])));
    }

}
