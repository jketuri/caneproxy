package FI.realitymodeler.cimd;

import FI.realitymodeler.*;
import java.net.*;
import java.io.*;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.servlet.*;

class CimdEngineShutdownHook extends Thread {
    CimdEngine cimdEngine;

    CimdEngineShutdownHook(CimdEngine cimdEngine) {
        this.cimdEngine = cimdEngine;
    }

    public void run() {
        cimdEngine.destroy();
    }

}

class CimdEngineMaxPriceThread extends Thread {
    CimdEngine cimdEngine;

    CimdEngineMaxPriceThread(CimdEngine cimdEngine) {
        this.cimdEngine = cimdEngine;
    }

    public void run() {
        while (!isInterrupted())
            try {
                Thread.sleep(cimdEngine.maxPriceDelayMs);
                long ctm = System.currentTimeMillis();
                List<String> maxPriceRandomKeyList = new ArrayList<String>();
                Iterator<Map.Entry<String, CimdMessage>> iter = cimdEngine.maxPriceCimdMessages.entrySet().iterator();
                while (iter.hasNext()) {
                    Map.Entry<String, CimdMessage> entry = iter.next();
                    CimdMessage message = entry.getValue();
                    if (message.startTransaction + cimdEngine.maxPriceDelayMs > ctm)
                        maxPriceRandomKeyList.add(entry.getKey());
                }
                Iterator<String> stringIter = maxPriceRandomKeyList.iterator();
                while (stringIter.hasNext()) {
                    String key = stringIter.next();
                    cimdEngine.maxPriceCimdMessages.remove(key);
                    if (CimdEngine.DEBUG) cimdEngine.log("Removed maxPriceCimdMessage with key " + key);
                }
            } catch (Exception ex) {
                if (ex instanceof InterruptedException) return;
                cimdEngine.handle(ex);
            }
    }

}

/** This class is the main class of Cimd Engine. It runs as a separate thread,
 *  serving CIMD-requests arriving at server socket. It can be configured as
 *  servlet in Servlet Engine using web.xml-file, or it can be run as standalone
 *  application and configurated with CimdEngine.properties-file.
 */
public class CimdEngine extends SocketServer implements Servlet, ServletConfig {
    static final boolean DEBUG = true;
    static final boolean TEST = false;
    static final int OPEN = 0, FULL = 1, TRANSFERRED = 2;
    static Map<String, Character> codes = new HashMap<String, Character>();
    static Map<Character, String> combis = new HashMap<Character, String>();
    static Map<String, CDRField> cdrFields = new HashMap<String, CDRField>();
    static DateFormat cdrTimestampFormat;
    static DateFormat serviceCentreTimestampFormat;
    static {
        try {
            // Commercial at
            codes.put("Oa", new Character('@'));
            // underscore
            codes.put("--", new Character('_'));
            // Inverted !
            codes.put("!!", new Character('\u00a1'));
            // Pounds sterling
            codes.put("L-", new Character('\u00a3'));
            // Inverted ?
            codes.put("??", new Character('\u00bf'));
            // A with dieresis
            codes.put("A\"", new Character('\u00c4'));
            // A with ring
            codes.put("A*", new Character('\u00c5'));
            // AE diphtong
            codes.put("AE", new Character('\u00c6'));
            // E with acute accent
            codes.put("E´", new Character('\u00c9'));
            // C with cedilla
            codes.put("C,", new Character('\u00c7'));
            // N with tilde
            codes.put("N~", new Character('\u00d1'));
            // O with dieresis
            codes.put("O\"", new Character('\u00d6'));
            // O with slash
            codes.put("O/", new Character('\u00d8'));
            // U with dieresis
            codes.put("U\"", new Character('\u00dc'));
            // Yen
            codes.put("Y-", new Character('\u00a5'));
            // German double-s
            codes.put("ss", new Character('\u00df'));
            // a with grave accent
            codes.put("a`", new Character('\u00e0'));
            // a with dieresis
            codes.put("a\"", new Character('\u00e4'));
            // a with ring
            codes.put("a*", new Character('\u00e5'));
            // ae diphtong
            codes.put("ae", new Character('\u00e6'));
            // e with grave accent
            codes.put("e`", new Character('\u00e8'));
            // e with acute accent
            codes.put("e´", new Character('\u00e9'));
            // Greek alphabet delta
            codes.put("gd", new Character('\u0394'));
            // Greek alphabet phi
            codes.put("gf", new Character('\u03a6'));
            // Greek alphabet gamma
            codes.put("gg", new Character('\u0393'));
            // Greek alphabet lambda
            codes.put("gl", new Character('\u039b'));
            // Greek alphabet omega
            codes.put("go", new Character('\u03a9'));
            // Greek alphabet pi
            codes.put("gp", new Character('\u03a0'));
            // Greek alphabet psi
            codes.put("gi", new Character('\u03a8'));
            // Greek alphabet sigma
            codes.put("gs", new Character('\u03a3'));
            // Greek alphabet theta
            codes.put("gt", new Character('\u0398'));
            // Greek alphabet xi
            codes.put("gx", new Character('\u039e'));
            // i with grave accent
            codes.put("i`", new Character('\u00ec'));
            // n with tilde
            codes.put("n~", new Character('\u00f1'));
            // o with grave accent
            codes.put("o`", new Character('\u00f2'));
            // o with dieresis
            codes.put("o\"", new Character('\u00f6'));
            // o with slash
            codes.put("o/", new Character('\u00f8'));
            // Currency symbol
            codes.put("ox", new Character('\u00a4'));
            // Quotation mark
            codes.put("qq", new Character('"'));
            // Section mark
            codes.put("so", new Character('\u00a7'));
            // u with grave accent
            codes.put("u`", new Character('\u00f9'));
            // u with dieresis
            codes.put("u\"", new Character('\u00fc'));

            Iterator<Map.Entry<String, Character>> iter = codes.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry<String, Character> entry = iter.next();
                combis.put(entry.getValue(), entry.getKey());
            }

            cdrFields.put("billing-profile",
                          new CDRField() {
                              public String getValue(Object context, Object message) throws IOException {
                                  CimdMessage cimdMessage = (CimdMessage)message;
                                  return cimdMessage.billingProfile;
                              }
                          });
            cdrFields.put("cimd-transaction-time",
                          new CDRField() {
                              public String getValue(Object context, Object message) throws IOException {
                                  CimdMessage cimdMessage = (CimdMessage)message;
                                  return cdrTimestampFormat.format(new Date(cimdMessage.endTransaction));
                              }
                          });
            cdrFields.put("service-price",
                          new CDRField() {
                              public String getValue(Object context, Object message) throws IOException {
                                  CimdMessage cimdMessage = (CimdMessage)message;
                                  return cimdMessage.servicePrice;
                              }
                          });
            cdrFields.put("short-number",
                          new CDRField() {
                              public String getValue(Object context, Object message) throws IOException {
                                  CimdMessage cimdMessage = (CimdMessage)message;
                                  return cimdMessage.shortNumber;
                              }
                          });
            cdrFields.put("smsgw-server-ip-address",
                          new CDRField() {
                              public String getValue(Object context, Object message) throws IOException {
                                  return ((CimdEngine)context).smsgwServerIpAddress;
                              }
                          });
            cdrFields.put("server-type",
                          new CDRField() {
                              public String getValue(Object context, Object message) throws IOException {
                                  return "S";
                              }
                          });
            cdrFields.put("running-counter",
                          new CDRField() {
                              public String getValue(Object context, Object message) throws IOException {
                                  return String.valueOf(((CimdEngine)context).runningCounter);
                              }
                          });
            cdrFields.put("service-id",
                          new CDRField() {
                              public String getValue(Object context, Object message) throws IOException {
                                  return "";
                              }
                          });
            cdrFields.put("service-type",
                          new CDRField() {
                              public String getValue(Object context, Object message) throws IOException {
                                  CimdMessage cimdMessage = (CimdMessage)message;
                                  if (cimdMessage.billingProfile.equals("40")) return "2";
                                  return "1";
                              }
                          });
            cdrFields.put("msisdn",
                          new CDRField() {
                              public String getValue(Object context, Object message) throws IOException {
                                  CimdMessage cimdMessage = (CimdMessage)message;
                                  return cimdMessage.msisdn;
                              }
                          });
            cdrFields.put("ip-address",
                          new CDRField() {
                              public String getValue(Object context, Object message) throws IOException {
                                  CimdMessage cimdMessage = (CimdMessage)message;
                                  return cimdMessage.ipAddress.getHostAddress();
                              }
                          });
            cdrFields.put("start-transaction",
                          new CDRField() {
                              public String getValue(Object context, Object message) throws IOException {
                                  CimdMessage cimdMessage = (CimdMessage)message;
                                  return cdrTimestampFormat.format(new Date(cimdMessage.startTransaction));
                              }
                          });
            cdrFields.put("end-transaction",
                          new CDRField() {
                              public String getValue(Object context, Object message) throws IOException {
                                  CimdMessage cimdMessage = (CimdMessage)message;
                                  return cdrTimestampFormat.format(new Date(cimdMessage.endTransaction));
                              }
                          });
            cdrFields.put("tariff-class",
                          new CDRField() {
                              public String getValue(Object context, Object message) throws IOException {
                                  CimdMessage cimdMessage = (CimdMessage)message;
                                  return cimdMessage.tariffClass;
                              }
                          });
            cdrFields.put("number-of-messages",
                          new CDRField() {
                              public String getValue(Object context, Object message) throws IOException {
                                  return "";
                              }
                          });
            cdrFields.put("success-indicator",
                          new CDRField() {
                              public String getValue(Object context, Object message) throws IOException {
                                  CimdMessage cimdMessage = (CimdMessage)message;
                                  return cimdMessage.successIndicator;
                              }
                          });
            cdrFields.put("service-priority",
                          new CDRField() {
                              public String getValue(Object context, Object message) throws IOException {
                                  return "";
                              }
                          });
            cdrFields.put("namp-phone-number",
                          new CDRField() {
                              public String getValue(Object context, Object message) throws IOException {
                                  CimdMessage cimdMessage = (CimdMessage)message;
                                  return cimdMessage.shortNumber;
                              }
                          });
            cdrFields.put("keyword",
                          new CDRField() {
                              public String getValue(Object context, Object message) throws IOException {
                                  CimdMessage cimdMessage = (CimdMessage)message;
                                  return cimdMessage.keyword;
                              }
                          });
            cdrFields.put("client-id",
                          new CDRField() {
                              public String getValue(Object context, Object message) throws IOException {
                                  return "";
                              }
                          });
            cdrFields.put("bearer-type",
                          new CDRField() {
                              public String getValue(Object context, Object message) throws IOException {
                                  return "";
                              }
                          });
            cdrFields.put("http-status-code",
                          new CDRField() {
                              public String getValue(Object context, Object message) throws IOException {
                                  return "";
                              }
                          });
            cdrFields.put("wap-stack-mode",
                          new CDRField() {
                              public String getValue(Object context, Object message) throws IOException {
                                  return "";
                              }
                          });
            cdrFields.put("namp-server-ip-address",
                          new CDRField() {
                              public String getValue(Object context, Object message) throws IOException {
                                  return "";
                              }
                          });
            cdrFields.put("first-event-record-number",
                          new CDRField() {
                              public String getValue(Object context, Object message) throws IOException {
                                  return String.valueOf(((CimdEngine)context).firstEventRecordNumber);
                              }
                          });
            cdrFields.put("file-number",
                          new CDRField() {
                              public String getValue(Object context, Object message) throws IOException {
                                  return String.valueOf(((CimdEngine)context).fileNumber);
                              }
                          });
            cdrFields.put("timestamp",
                          new CDRField() {
                              public String getValue(Object context, Object message) throws IOException {
                                  return cdrTimestampFormat.format(new Date(((CimdEngine)context).timestamp));
                              }
                          });
            cdrFields.put("last-event-record-number",
                          new CDRField() {
                              public String getValue(Object context, Object message) throws IOException {
                                  return String.valueOf(((CimdEngine)context).lastEventRecordNumber);
                              }
                          });
            cdrFields.put("start-timestamp-for-cdr-file-actions",
                          new CDRField() {
                              public String getValue(Object context, Object message) throws IOException {
                                  return cdrTimestampFormat.format(new Date(((CimdEngine)context).startTimestampForCdrFileActions));
                              }
                          });
            cdrFields.put("end-timestamp-for-cdr-file-actions",
                          new CDRField() {
                              public String getValue(Object context, Object message) throws IOException {
                                  return cdrTimestampFormat.format(new Date(((CimdEngine)context).endTimestampForCdrFileActions));
                              }
                          });
            cdrTimestampFormat = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss:SSS");
            serviceCentreTimestampFormat = new SimpleDateFormat("yyMMddhhmmss");
        } catch (Exception ex) {
            throw new Error(ex.toString());
        }
    }

    List<Object> cdrContent = null, cdrHeader = null, cdrTrailer = null;
    File saveFile = new File("CimdEngine.save");
    File statusFile;
    File transferFile;
    Map<String, CimdOperation> cimdOperations = new HashMap<String, CimdOperation>();
    Map<String, CimdMessage> maxPriceCimdMessages = new HashMap<String, CimdMessage>();
    InetAddress smscAddress = null;
    Properties initParameters;
    Properties testProperties = null;
    PrintWriter cdrWriter = null;
    ServletConfig servletConfig;
    ServletContext servletContext;
    SimpleDateFormat maxPriceDateFormat;
    String filePath = "cimd/cdr";
    String maxPriceMessage = null;
    String smsgwServerIpAddress;
    Thread cimdEngineMaxPriceThread = null;
    ThreadGroup group = null;
    URL billingUrl;
    boolean createTables = false;
    int backupFileNumberRead = 1;
    int backupFileNumberWrite = 1;
    int smscPort = -1;
    int cimdEnginePort = -1;
    int fileAmount = 100;
    int fileNumber;
    long cdrFileSize;
    long endTimestampForCdrFileActions;
    long fileSizeLimitBytes = 25L * 1024L * 1024L;
    long firstEventRecordNumber;
    long lastCtm = 0L;
    long lastEventRecordNumber;
    long maxPriceDelayMs = 5L * 60L * 60L * 1000L;
    long runningCounter = 1L;
    long startTimestampForCdrFileActions;
    long timestamp;
    long transferFileSize;

    /** This method parses simple pattern specifying the format of CDR-content, header and trailer lines.
     *  Literal characters are represented as themselves. Strings of format {variable-name} specify
     *  value of some variable, whose name is 'variable-name'. These patterns are replaced
     *  with actual values when generating the CDR-output. Available variable names are as follows:
     *  (these descriptions are copied from document 'Nokia Artus Messaging Platform Release 2.1
     *  Messaging: Billing Interface for NAMP Messaging, User's Guide, Optional Feature')
     *  <table>
     *  <caption>CDR fields</caption>
     *  <tr><th>server-type</th><td>Char</td><td>W/M</td><td>Specifies whether the event (transaction) is
     *  created in the WAP ("W") or messaging ("M") side.</td></tr>
     *  <tr><th>running-counter</th><td>Integer</td><td>Any valid number for the cdr-record.</td>
     *  <td>Sequence number of the CDR record. Max 8 digits.</td></tr>
     *  <tr><th>service-id</th><td>String</td><td>Not Provided</td><td>Service ID for the requested URL, retrieved
     *  from tariff file.Alphanumeric field. (Empty)</td></tr>
     *  <tr><th>service-type</th><td>Integer</td><td>1= pull
     *  2= push</td><td>Code for PULL and PUSH</td></tr>
     *  <tr><th>msisdn</th><td>String</td><td>Valid MSISDN number</td>
     *  <td>The MSISDN number of the mobile terminal. Can be empty.</td></tr>
     *  <tr><th>ip-address</th><td>String</td><td>Not Provided</td><td>The IP address of the mobile
     *  terminal.Accepted format is dotted quad e.g 195.156.32.2. (Empty)</td></tr>
     *  <tr><th>start-transaction</th><td>String</td><td>Timestamp in format yyyy:mm:dd hh:mm:ss:fff</td>
     *  <td>Timestamp for the date and time when the transaction started.</td></tr>
     *  <tr><th>end-transaction</th><td>String</td><td>Timestamp in format yyyy:mm:dd hh:mm:ss:fff</td>
     *  <td>Timestamp for the date and time when the transaction ended.</td></tr>
     *  <tr><th>tariff-class</th><td>Integer</td><td>Number</td><td>Information used for the rating of the service
     *  taken from the tariff file.</td></tr>
     *  <tr><th>number-of-messages</th><td>Integer</td><td>1 to 3</td><td>Number of mobile terminated Messages.
     *  (This field indicates volume in WAP)</td></tr>
     *  <tr><th>succcess-indicator</th><td>Integer</td><td>1=successful
     *  2=not successful</td><td>The Transaction is termed as successful, if
     *  an error free response was generated and an
     *  acknowledgement for the same is received from the user.</td></tr>
     *  <tr><th>service-priority</th><td>Integer</td><td>Priority of the
     *  requested service.</td><td>Priority of the service.</td></tr>
     *  <tr><th>namp-phone-number</th><td>String</td><td>Phone number string</td>
     *  <td>This field contains the Namp Phone number.</td></tr>
     *  <tr><th>keyword</th><td>String</td><td>Keyword string</td><td>This field containsthe requested keyword separated.</td></tr>
     *  <tr><th>client-id</th><td>String</td><td>Login id used in HTTP authentication.</td>
     *  <td>Client login id if the content-server requires HTTP basic authentication. Can be empty.</td></tr>
     *  <tr><th>bearer-type</th><td>Integer</td><td>Not provided</td><td>(Empty)</td></tr>
     *  <tr><th>http-status-code</th><td>Integer</td><td>Not exact, just an approximate indicator.</td>
     *  <td>Status code returned from the origin server.</td></tr>
     *  <tr><th>wap-stack-mode</th><td>Integer</td><td>Not Provided</td><td>(Empty)</td></tr>
     *  <tr><th>namp-server-ip-address</th><td>String</td><td>Valid IP address</td>
     *  <td>The IP address of the NAMP server (needed for example in HAP environment to make a
     *  difference which server has served the request).</td></tr>
     *  <tr><th>first-event-record-number</th><td>Integer</td><td>Number</td>
     *  <td>A unique incrementing sequence number. Has in
     *  maximum of 8 integer digits.</td></tr>
     *  <tr><th>last-event-record-number</th><td>Integer</td><td>Number</td>
     *  <td>Last event record number, which is included in this CDR file.</td></tr>
     *  <tr><th>file-number</th><td>Integer</td><td>Number</td>
     *  <td>The CDR file number, a unique incrementing sequence
     *  number with maximum of 8 integer digits.</td></tr>
     *  <tr><th>timestamp</th><td>String</td><td>Timestamp in format yyyy:mm:dd hh:mm:ss:fff</td>
     *  <td>Time when theCDRHandlerstartsto create theCDRfile.</td></tr>
     *  <tr><th>start-timestamp-for-cdr-file-actions</th><td>String</td><td>The
     *  format is yyyy:mm:dd hh:mm:ss:fff, where fff indicates milliseconds.</td>
     *  <td>Timestamp of the earliest CDR record in this file.</td></tr>
     *  <tr><th>end-timestamp-for-cdr-file-actions</th><td>String</td><td>The
     *  format is yyyy:mm:dd hh:mm:ss:fff, where fff indicates milliseconds.</td>
     *  <td>Timestamp of the latest CDR record in this file.</td></tr>
     *  <tr><th>billing-profile</th><td>Integer</td><td>10,20,31,32,40</td>
     *  <td>10=MO profile, 20=MT profile, 3X=MOMT profile, 31=MO part of MOMT profile,
     *  32=MT part of MOMT profile, 40=PUSH profile</td></tr>
     *  <tr><th>short-number</th><td>String</td><td>Short number</td>
     *  <td>This field contains the Short Number</td></tr>
     *  <tr><th>smsgw-server-ip-address</th><td>String</td><td>Valid IP-address</td>
     *  <td>The IP address of the SMSGW server.</td></tr>
     *  </table>
     */
    public List<Object> parseCdrPattern(String pattern) throws ServletException {
        List<Object> patternList = new ArrayList<Object>();
        StringTokenizer st = new StringTokenizer(pattern, ",", true);
        while (st.hasMoreElements()) {
            String token = st.nextToken();
            int i = 0, j;
            while ((j = token.indexOf(" {", i)) != -1) {
                if (j > i) patternList.add(token.substring(i, j));
                j += 2;
                int k = token.indexOf('}', j);
                if (k == -1) throw new ServletException("Invalid cdr pattern " + pattern);
                String fieldName = token.substring(j, k);
                CDRField cdrField = cdrFields.get(fieldName);
                if (cdrField == null) throw new ServletException("No cdr field " + fieldName + " found");
                patternList.add(cdrField);
                i = k + 1;
            }
            if (i < token.length()) patternList.add(token.substring(i));
        }
        return patternList;
    }

    public void printCdrPattern(PrintWriter writer, List<Object> patternList, CimdEngine cimdEngine, CimdMessage cimdMessage)
        throws IOException {
        Iterator iter = patternList.iterator();
        while (iter.hasNext()) {
            Object item = iter.next();
            if (item instanceof CDRField) writer.print(((CDRField)item).getValue(cimdEngine, cimdMessage));
            else writer.print(item);
        }
        writer.println();
    }

    byte[] makeStamp(String stamp) {
        byte[] bytes = new byte[7];
        int i = 0, j = 6;
        for (; i < 14; i += 2)
            bytes[j] = (byte)Integer.parseInt(stamp.substring(i, i + 2), 16);
        return bytes;
    }

    String readStamp(RandomAccessFile raf) throws IOException {
        StringBuffer stampBuffer = new StringBuffer(14);
        for (int i = 0; i < 7; i++) {
            String value = Integer.toString(raf.read(), 16);
            stampBuffer.insert(0, value);
            if (value.length() < 2) stampBuffer.insert(0, '0');
        }
        return stampBuffer.toString();
    }

    void writeStamp(RandomAccessFile raf, Date date) throws IOException {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        raf.write(Integer.parseInt(String.valueOf(cal.get(cal.SECOND)), 16));
        raf.write(Integer.parseInt(String.valueOf(cal.get(cal.MINUTE)), 16));
        raf.write(Integer.parseInt(String.valueOf(cal.get(cal.HOUR_OF_DAY)), 16));
        raf.write(Integer.parseInt(String.valueOf(cal.get(cal.DAY_OF_MONTH)), 16));
        raf.write(Integer.parseInt(String.valueOf(cal.get(cal.MONTH) + 1), 16));
        int year = cal.get(cal.YEAR);
        raf.write(Integer.parseInt(String.valueOf(year % 100), 16));
        raf.write(Integer.parseInt(String.valueOf(year / 100), 16));
    }

    File makeCdrFile(int fileNumber) {
        StringBuffer sb = new StringBuffer(String.valueOf(fileNumber));
        if (sb.length() < 4) sb.insert(0, '0');
        return new File(filePath, "CF" + sb.toString() + ".DAT");
    }

    File makeBackupFile(int backupFileNumber) {
        StringBuffer sb = new StringBuffer(String.valueOf(backupFileNumber));
        if (sb.length() < 6) sb.insert(0, '0');
        return new File(filePath, "ER" + sb.toString() + ".DAT");
    }

    /** This method opens a CDR file for writing.
     *  It tries to make all checks described in document
     *  'NAMP 2.1 Messaging: Billing Interface for NAMP Messaging, User's Guide'
     */
    public synchronized void openCdrFile() throws IOException {
        if (DEBUG) log("Opening CDR file");
        // This loop will wait at maximum 15 seconds
        // to transfer file to be available,
        // otherwise method supposes it to be corrupted
        for (int i = 0; i < 15; i++) {
            if (transferFile.length() < transferFileSize)
                // Transfer file seems to be in a truncated format. Wait a moment.
                try {
                    Thread.sleep(1000L);
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                    return;
                }
        }
        File cdrFile = null;
        RandomAccessFile statusRaf = new RandomAccessFile(statusFile, "rw"),
            transferRaf = new RandomAccessFile(transferFile, "rw");
        long statusFileLength = statusRaf.length(),
            transferFileLength = transferRaf.length();
        if (transferFile.length() < transferFileSize) {
            // Transfer file is supposed to be corrupted,
            // a file with open status is chosen for overwriting,
            // or if there is no file with an open status, a file
            // with status TRANSFERRED with the oldest time stamp
            // in the status file is chosen. (from [Namp_MSG_BI])
            String oldestStamp = "99991231235959";
            int oldestFileNumber = 0;
            for (fileNumber = 1; fileNumber <= fileAmount; fileNumber++) {
                long statusOffset = (long)(fileNumber - 1) * 8L;
                if (statusOffset >= statusFileLength) break;
                statusRaf.seek(statusOffset);
                int status = statusRaf.read();
                if (status == OPEN) break;
                if (status == TRANSFERRED) {
                    String statusStamp = readStamp(statusRaf);
                    if (statusStamp.compareTo(oldestStamp) < 0) {
                        oldestFileNumber = fileNumber;
                        oldestStamp = statusStamp;
                    }
                }
            }
            if (fileNumber > fileAmount) fileNumber = oldestFileNumber;
            cdrFile = makeCdrFile(fileNumber);
        } else for (fileNumber = 1; fileNumber <= fileAmount; fileNumber++) {
                long statusOffset = (long)(fileNumber - 1) * 8L;
                if (statusOffset >= statusFileLength) break;
                statusRaf.seek(statusOffset);
                int status = statusRaf.read();
                String statusStamp = readStamp(statusRaf);
                long transferOffset = (long)(fileNumber - 1) * 7L;
                transferRaf.seek(transferOffset);
                String transferStamp = readStamp(transferRaf);
                // From NAMP 2.1 Messaging: Billing Interface for NAMP Messaging, User's Guide, Optional Feature (Namp_MSG_BI):
                // If the time stamp of a transfer file for a given CDR file is later
                // than the time stamp in the status file for the same CDR file,
                // the CDR Handler interprets this file as 'fetched' by the billing
                // computer and overwrites it.
                if (transferStamp.compareTo(statusStamp) > 0) {
                    cdrFile = makeCdrFile(fileNumber);
                    File backupFile = makeBackupFile(backupFileNumberRead);
                    if (backupFile.exists()) {
                        cdrFile.delete();
                        backupFile.renameTo(cdrFile);
                        statusRaf.seek(statusOffset);
                        statusRaf.write(FULL);
                        writeStamp(statusRaf, new Date());
                        if (++backupFileNumberRead > 999999) backupFileNumberRead = 1;
                        continue;
                    }
                    statusRaf.seek(statusOffset);
                    statusRaf.write(OPEN);
                    writeStamp(statusRaf, new Date());
                    break;
                }
                // From Namp_MSG_BI:
                // If the status is TRANSFERRED and the time stamp of the transfer file is
                // earlier or equal to the one in status file, the status field is updated to
                // be FULL. (See 4.6.4 Marking a CDR file to be transferred again)
                if (status == TRANSFERRED) {
                    statusRaf.seek(statusOffset);
                    statusRaf.write(FULL);
                }
            }
        statusRaf.close();
        transferRaf.close();
        if (fileNumber > fileAmount) {
            // There is no available CDR-files, backup file must be used
            cdrFile = makeBackupFile(backupFileNumberWrite);
            if (++backupFileNumberWrite > 999999) backupFileNumberWrite = 1;
        }
        new File(cdrFile.getParentFile().getCanonicalPath()).mkdirs();
        cdrWriter = new PrintWriter(new BufferedOutputStream(new FileOutputStream(cdrFile)));
        timestamp = System.currentTimeMillis();
        firstEventRecordNumber = runningCounter;
        printCdrPattern(cdrWriter, cdrHeader, this, null);
        cdrWriter.flush();
        cdrFileSize = cdrFile.length();
        startTimestampForCdrFileActions = 0L;
    }

    public synchronized void closeCdrFile() throws IOException {
        if (DEBUG) log("Closing CDR file");
        endTimestampForCdrFileActions = System.currentTimeMillis();
        lastEventRecordNumber = runningCounter - 1L;
        printCdrPattern(cdrWriter, cdrTrailer, this, null);
        cdrWriter.close();
        RandomAccessFile statusRaf = new RandomAccessFile(statusFile, "rw");
        long statusOffset = (long)(fileNumber - 1) * 8L;
        statusRaf.seek(statusOffset);
        statusRaf.write(FULL);
        writeStamp(statusRaf, new Date());
        statusRaf.close();
    }

    public synchronized void writeCdrLine(CimdMessage cimdMessage) throws IOException {
        if (DEBUG) log("Writing CDR line");
        if (startTimestampForCdrFileActions == 0L)
            startTimestampForCdrFileActions = System.currentTimeMillis();
        StringWriter stringWriter = new StringWriter();
        printCdrPattern(new PrintWriter(stringWriter), cdrContent, this, cimdMessage);
        String content = stringWriter.toString();
        stringWriter = new StringWriter();
        printCdrPattern(new PrintWriter(stringWriter), cdrTrailer, this, null);
        String trailer = stringWriter.toString();
        int contentLength = content.length(),
            trailerLength = trailer.length();
        if (cdrFileSize + contentLength + trailerLength > fileSizeLimitBytes) {
            closeCdrFile();
            openCdrFile();
        }
        cdrWriter.print(content);
        cdrFileSize += contentLength;
        runningCounter++;
    }

    public void handle(Throwable throwable) {
        servletContext.log(throwable.toString(), throwable);
    }

    /** Initializes Cimd Engine. Following initialization parameters are available:<br>
     *  <table>
     *  <caption>Properties</caption>
     *  <tr><th>cimdEnginePort</th><td>Specifies the port where this Cimd Engine must run.</td></tr>
     *  <tr><th>connectionUrl</th><td>Specifies the JDBC connection url.</td></tr>
     *  <tr><th>cdrContent</th><td>Defines the pattern of CDR content lines, see method parseCdrPattern.</td></tr>
     *  <tr><th>cdrHeader</th><td>Defines the pattern of CDR header lines, see method parseCdrPattern.</td></tr>
     *  <tr><th>cdrTrailer</th><td>Defines the pattern of CDR trailer lines, see method parseCdrPattern.</td></tr>
     *  <tr><th>fileAmount</th><td>Specifies number of CDR files (1-9999, defaults to 100).</td></tr>
     *  <tr><th>fileSizeLimitKb</th><td>Specifies maximum size of CDR files in kilobytes (defaults to 25 MB)</td></tr>
     *  <tr><th>filePath</th><td>Specifies where CDR-files and control files are stored (defaults to
     *  web application WEBINF-directory when Cimd Engine is running as servlet,
     *  and to current directory when Cimd Engine is running as standalone application).</td></tr>
     *  <tr><th>maxPriceDelayMin</th><td>Specifies the delay of checking expired messages over maximum
     *  price, specified in minutes (defaults to 5 minute).</td></tr>
     *  <tr><th>smscAddress</th><td>Specifies the address of actual SMSC CIMD IP-address</td></tr>
     *  <tr><th>smscPort</th><td>Specifies the port of actual SMSC CIMD IP-port</td></tr>
     *  </table>
     *  @see parseCdrPattern(String)
     */
    public void init(ServletConfig config) throws ServletException {
        servletConfig = config;
        servletContext = config.getServletContext();
        try {
            log("Starting Cimd Engine");
            Enumeration initParameterNames = config.getInitParameterNames();
            while (initParameterNames.hasMoreElements()) {
                String initParameterName = (String)initParameterNames.nextElement(),
                    initParameter = config.getInitParameter(initParameterName);
                if (initParameterName.equals("cimdEnginePort")) cimdEnginePort = Integer.parseInt(initParameter);
                else if (initParameterName.equals("cdrContent")) cdrContent = parseCdrPattern(initParameter);
                else if (initParameterName.equals("cdrHeader")) cdrHeader = parseCdrPattern(initParameter);
                else if (initParameterName.equals("cdrTrailer")) cdrTrailer = parseCdrPattern(initParameter);
                else if (initParameterName.equals("fileAmount")) fileAmount = Integer.parseInt(initParameter);
                else if (initParameterName.equals("fileSizeLimitKb")) fileSizeLimitBytes = Long.parseLong(initParameter) * 1024L;
                else if (initParameterName.equals("filePath")) filePath = initParameter;
                else if (initParameterName.equals("maxPriceMessage")) maxPriceMessage = initParameter;
                else if (initParameterName.equals("maxPriceDateFormat")) maxPriceDateFormat = new SimpleDateFormat(initParameter);
                else if (initParameterName.equals("maxPriceDelayMin")) maxPriceDelayMs = Long.parseLong(initParameter) * 60L * 1000L;
                else if (initParameterName.equals("smscAddress")) smscAddress = InetAddress.getByName(initParameter);
                else if (initParameterName.equals("smscPort")) smscPort = Integer.parseInt(initParameter);
                else throw new ServletException("Unknown initialization parameter " + initParameterName);
            }
            if (TEST) {
                testProperties = new Properties();
                testProperties.load(new FileInputStream("CimdTest.properties"));
            }
            statusFile = new File(filePath, "TTSCOF00.IMG");
            new File(statusFile.getParentFile().getCanonicalPath()).mkdirs();
            byte[] statusStamp = makeStamp("19900101000000"),
                transferStamp = makeStamp("19900101000001");
            if (statusFile.createNewFile()) {
                RandomAccessFile statusRaf = new RandomAccessFile(statusFile, "rw");
                for (fileNumber = 1; fileNumber <= fileAmount; fileNumber++) {
                    statusRaf.write(TRANSFERRED);
                    statusRaf.write(statusStamp);
                }
                statusRaf.close();
            }
            transferFile = new File(filePath, "TTTCOF00.IMG");
            new File(transferFile.getParentFile().getCanonicalPath()).mkdirs();
            if (transferFile.createNewFile()) {
                RandomAccessFile transferRaf = new RandomAccessFile(transferFile, "rw");
                for (fileNumber = 1; fileNumber <= fileAmount; fileNumber++)
                    transferRaf.write(transferStamp);
                transferRaf.close();
            }
            if (saveFile.exists()) {
                DataInputStream din = new DataInputStream(new FileInputStream(saveFile));
                runningCounter = din.readLong();
                din.close();
            }
            if (smscAddress == null) smscAddress = InetAddress.getByName("servu");
            if (smscPort == -1) smscPort = 6789;
            if (cimdEnginePort == -1) cimdEnginePort = 6789;
            serverSocket = new ServerSocket(cimdEnginePort);
            group = new ThreadGroup(getClass().getName());
            request = new CimdRequestThread(group, this);
            smsgwServerIpAddress = InetAddress.getLocalHost().getHostAddress();
            timeout = 0L;
            transferFileSize = (long)fileAmount * 7L;
            cimdOperations.put(CimdOperation.login, new LoginOperation(this));
            cimdOperations.put(CimdOperation.submit, new SubmitOperation(this));
            cimdOperations.put(CimdOperation.deliveryRequest, new DeliveryRequestOperation(this));
            cimdOperations.put(CimdOperation.deliver, new DeliverOperation(this));
            openCdrFile();
            cimdEngineMaxPriceThread = new CimdEngineMaxPriceThread(this);
            cimdEngineMaxPriceThread.setDaemon(true);
            cimdEngineMaxPriceThread.start();
            start();
            log("Cimd Engine started");
        } catch (IOException ex) {
            throw new ServletException(ex);
        }
    }

    public ServletConfig getServletConfig() {
        return servletConfig;
    }

    public void service(ServletRequest request, ServletResponse response)
        throws ServletException, IOException {
    }

    public String getServletInfo() {
        return getClass().getName();
    }

    public void destroy() {
        log("Ending Cimd Engine");
        try {
            interrupt();
            try {
                if (serverSocket != null)
                    serverSocket.close();
            } catch (IOException ex) {} finally {
                if (group != null) {
                    Thread[] threads = new Thread[group.activeCount()];
                    group.enumerate(threads);
                    for (int i = 0; i < threads.length; i++)
                        if (threads[i] != null) {
                            threads[i].interrupt();
                            threads[i].join(5000L);
                        }
                }
                if (cimdEngineMaxPriceThread != null) {
                    cimdEngineMaxPriceThread.interrupt();
                    cimdEngineMaxPriceThread.join(5000L);
                }
                if (isAlive()) join();
            }
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        } finally {
            try {
                if (cdrWriter != null) {
                    closeCdrFile();
                    DataOutputStream dout = new DataOutputStream(new FileOutputStream(saveFile));
                    dout.writeLong(runningCounter);
                    dout.close();
                }
            } catch (IOException ex) {} finally {
                log("Cimd Engine ended");
            }
        }
    }

    public String getServletName() {
        return getClass().getName();
    }

    public ServletContext getServletContext() {
        return null;
    }

    public String getInitParameter(String name) {
        return initParameters.getProperty(name);
    }

    public Enumeration getInitParameterNames() {
        return initParameters.propertyNames();
    }

    public void log(String message) {
        getServletConfig().getServletContext().log(message);
    }

    public static void main(String argv[]) {
        try {
            CimdEngine cimdEngine = new CimdEngine();
            cimdEngine.initParameters = new Properties();
            cimdEngine.initParameters.load(new FileInputStream(new File(".", "CimdEngine.properties")));
            if (argv.length > 0 && argv[0].equals("create")) cimdEngine.createTables = true;
            Runtime.getRuntime().addShutdownHook(new CimdEngineShutdownHook(cimdEngine));
            cimdEngine.init(cimdEngine);
        } catch (Exception ex) {
            ex.printStackTrace();
            if (ex instanceof ServletException) ((ServletException)ex).getRootCause().printStackTrace();
        }
    }

}
