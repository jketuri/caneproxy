package FI.realitymodeler.cimd;

import java.io.*;
import java.net.*;
import java.sql.*;
import java.util.*;

public class LoginOperation extends CimdOperation {

    public LoginOperation(CimdEngine cimdEngine) {
        super(cimdEngine);
    }

    public boolean handle(CimdMessage request) throws SQLException, UnknownHostException {
        if (CimdEngine.DEBUG) cimdEngine.log(getClass().getName() + " handle(CimdMessage request)");
        String username = null, password = null;
        int parameterIndex = request.parameterList.indexOf(userIdentityParameter);
        if (parameterIndex != -1)
            username = request.parameterList.get(parameterIndex).parameterValue;
        parameterIndex = request.parameterList.indexOf(passwordParameter);
        if (parameterIndex != -1)
            password = request.parameterList.get(parameterIndex).parameterValue;
        if (CimdEngine.DEBUG) cimdEngine.log(getClass().getName() + " received " + username + " and " + password);
        if (username == null || password == null) return true;
        if (CimdEngine.TEST) {
            if (!request.ipAddress.equals(InetAddress.getByName(cimdEngine.testProperties.getProperty("cimdHost")))
                || !username.equals(cimdEngine.testProperties.getProperty("cimdUser"))
                || !password.equals(cimdEngine.testProperties.getProperty("cimdPassword"))) return true;
        } else {
            /*
              cimdEngine.connect();
              ServiceNumberProfile serviceNumberProfile = cimdEngine.dataManager.createServiceNumberProfile();
              String cimdUser = null, cimdPassword = null;
              try {
              serviceNumberProfile.loadByHost(request.ipAddress.getHostAddress());
              if (serviceNumberProfile.next()) {
              cimdUser = serviceNumberProfile.getCimdUser();
              cimdPassword = serviceNumberProfile.getCimdPassword();
              }
              } finally {
              serviceNumberProfile.disconnectCollection();
              }
              if (cimdUser == null || cimdPassword == null
              || !username.equals(cimdUser) || !password.equals(cimdPassword)) return true;
            */
        }
        if (CimdEngine.DEBUG) cimdEngine.log(getClass().getName() + " handle(CimdMessage request) succeed");
        return false;
    }

    public boolean handle(CimdMessage request, CimdMessage response, CimdMessage message, CimdMessage messageResponse) throws IOException, SQLException, SocketException {
        return false;
    }

    public void log(CimdMessage request, CimdMessage response,
                    CimdMessage message, CimdMessage messageResponse,
                    CimdMessage extraRequest, CimdMessage extraResponse) throws IOException, SQLException {
    }

}
