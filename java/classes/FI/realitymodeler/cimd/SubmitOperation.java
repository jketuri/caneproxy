package FI.realitymodeler.cimd;

import java.io.*;
import java.net.*;
import java.sql.*;
import java.util.*;

public class SubmitOperation extends CimdOperation {

    public SubmitOperation(CimdEngine cimdEngine) {
        super(cimdEngine);
    }

    public boolean handle(CimdMessage request) throws SQLException, UnknownHostException {
        int parameterIndex = request.parameterList.indexOf(tariffClassParameter);
        if (parameterIndex != -1) {
            request.tariffClass = request.parameterList.get(parameterIndex).parameterValue;
            request.parameterList.remove(parameterIndex);
        } else request.tariffClass = "";
        parameterIndex = request.parameterList.indexOf(destinationAddressParameter);
        if (parameterIndex != -1)
            request.msisdn = request.parameterList.get(parameterIndex).parameterValue;
        else request.msisdn = "";
        parameterIndex = request.parameterList.indexOf(originatorAddressParameter);
        if (parameterIndex != -1)
            request.shortNumber = request.parameterList.get(parameterIndex).parameterValue;
        else request.shortNumber = "";
        request.keyword = getUserText(request);
        double maximumPrice = -1.0;
        if (CimdEngine.TEST) {
            if (request.msisdn.indexOf(cimdEngine.testProperties.getProperty("forbiddenPhoneNumber")) != -1) {
                request.skip = true;
                return false;
            }
            maximumPrice = Double.parseDouble(cimdEngine.testProperties.getProperty("maximumPrice"));
            request.billingProfileMOStatus = Integer.parseInt(cimdEngine.testProperties.getProperty("MOStatus")) != 0;
            request.billingProfileMTStatus = Integer.parseInt(cimdEngine.testProperties.getProperty("MTStatus")) != 0;
            request.billingProfileMEGAPUSHStatus = Integer.parseInt(cimdEngine.testProperties.getProperty("MEGAPUSHStatus")) != 0;
            if (request.billingProfileMTStatus)
                if (Integer.parseInt(request.tariffClass) == Integer.parseInt(cimdEngine.testProperties.getProperty("MTTariffClass")))
                    request.servicePriceMT = Double.parseDouble(cimdEngine.testProperties.getProperty("MTPrice"));
                else request.servicePriceMT = Double.parseDouble(cimdEngine.testProperties.getProperty("MTDefaultPrice"));
            if (request.billingProfileMEGAPUSHStatus)
                if (request.msisdn.indexOf(cimdEngine.testProperties.getProperty("MEGAPUSHPhoneNumberPattern")) != -1)
                    request.servicePriceMEGAPUSH = Double.parseDouble(cimdEngine.testProperties.getProperty("MEGAPUSHPrice"));
                else {
                    request.skip = true;
                    return false;
                }
        } else {
            /*
              cimdEngine.connect();
              ForbiddenPhoneNumber forbiddenPhoneNumber = cimdEngine.dataManager.createForbiddenPhoneNumber();
              if (forbiddenPhoneNumber.checkPattern(request.shortNumber, request.msisdn)) {
              // Phone number is found in forbidden numbers.
              // Request must be skipped but error response must be generated.
              request.skip = true;
              return false;
              }
              ServiceNumberProfile serviceNumberProfile = cimdEngine.dataManager.createServiceNumberProfile();
              serviceNumberProfile.setShortNumber(request.shortNumber);
              if (serviceNumberProfile.load()) {
              maximumPrice = serviceNumberProfile.getMaximumPrice();
              request.billingProfileMOStatus = serviceNumberProfile.getBillingProfileMO();
              request.billingProfileMTStatus = serviceNumberProfile.getBillingProfileMT();
              request.billingProfileMEGAPUSHStatus = serviceNumberProfile.getBillingProfileMEGAPUSH();
              if (request.billingProfileMTStatus) {
              BillingProfileMT billingProfileMT = cimdEngine.dataManager.createBillingProfileMT();
              billingProfileMT.setShortNumber(request.shortNumber);
              billingProfileMT.setTarifClass(Integer.parseInt(request.tariffClass));
              billingProfileMT.load();
              if (billingProfileMT.load()) request.servicePriceMT = billingProfileMT.getPrice();
              else {
              billingProfileMT.setShortNumber(request.shortNumber);
              billingProfileMT.setTarifClass(0);
              if (billingProfileMT.load())
              request.servicePriceMT = billingProfileMT.getPrice();
              }
              }
              if (request.billingProfileMEGAPUSHStatus) {
              BillingProfileMEGAPUSH billingProfileMEGAPUSH = cimdEngine.dataManager.createBillingProfileMEGAPUSH();
              if (billingProfileMEGAPUSH.checkPattern(request.msisdn))
              request.servicePriceMEGAPUSH = billingProfileMEGAPUSH.getPrice();
              else {
              // Phone number has not been found in active MEGAPUSH billing profile.
              // Request must be skipped but error response must be generated.
              request.skip = true;
              return false;
              }
              }
              }
            */
        }
        double servicePrice = request.servicePriceMT + request.servicePriceMEGAPUSH;
        // If short message price is over maximum price
        if (maximumPrice >= 0.0 && servicePrice > maximumPrice) {
            if (CimdEngine.DEBUG) cimdEngine.log("Submit over maximum price");
            request.maxPriceCimdMessage = makeMaxPriceCimdMessage(request, servicePrice);
            request.alternativeCimdMessage = request.maxPriceCimdMessage;
        }
        return false;
    }

    public boolean handle(CimdMessage request, CimdMessage response, CimdMessage message, CimdMessage messageResponse) throws IOException, SQLException, SocketException {
        if (!request.skip) return false;
        // If request has been skipped due to msisdn belonging to forbidden numbers
        // or number's absence in the active MEGAPUSH billing profile
        // error response message is generated.
        response.alternativeCimdMessage = new CimdMessage();
        response.alternativeCimdMessage.operationCode = submitResponse;
        response.alternativeCimdMessage.parameterList.add(
                                                          new CimdParameter(destinationAddressType, message.msisdn));
        response.alternativeCimdMessage.parameterList.add(
                                                          new CimdParameter(errorCodeType, "300"));
        response.alternativeCimdMessage.parameterList.add(
                                                          new CimdParameter(errorTextType, "Incorrect destination address"));
        return false;
    }

    public void log(CimdMessage request, CimdMessage response,
                    CimdMessage message, CimdMessage messageResponse,
                    CimdMessage extraRequest, CimdMessage extraResponse) throws IOException, SQLException {
        if (request.skip || request.maxPriceCimdMessage != null) return;
        if (request.billingProfileMTStatus || request.billingProfileMEGAPUSHStatus)
            logMessage(request, response, submitResponse);
    }

}
