package FI.realitymodeler.cimd;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

class MaxPriceField {

    public String getValue(CimdEngine cimdEngine, MaxPriceMessage message) {
        return null;
    }

}

public class MaxPriceMessage {
    static Map<String, MaxPriceField> fields = new HashMap<String, MaxPriceField>();
    static {
        fields.put("date", new MaxPriceField() {
                public String getValue(CimdEngine cimdEngine, MaxPriceMessage message) {
                    return cimdEngine.maxPriceDateFormat.format(new Date(message.time));
                }
            });
        fields.put("price", new MaxPriceField() {
                public String getValue(CimdEngine cimdEngine, MaxPriceMessage message) {
                    return message.price;
                }
            });
        fields.put("random-key", new MaxPriceField() {
                public String getValue(CimdEngine cimdEngine, MaxPriceMessage message) {
                    return message.randomKey;
                }
            });
        fields.put("short-number", new MaxPriceField() {
                public String getValue(CimdEngine cimdEngine, MaxPriceMessage message) {
                    return message.shortNumber;
                }
            });
    }

    CimdEngine cimdEngine;
    String price;
    String randomKey;
    String shortNumber;
    long time;

    public MaxPriceMessage(CimdEngine cimdEngine) {
        this.cimdEngine = cimdEngine;
    }

    public String toString(String pattern) {
        StringBuffer buffer = new StringBuffer();
        int i = 0, j;
        while ((j = pattern.indexOf('<', i)) != -1) {
            buffer.append(pattern.substring(i, j));
            int k = pattern.indexOf('>', j + 1);
            String name = pattern.substring(j + 1, k);
            buffer.append(fields.get(name).getValue(cimdEngine, this));
            i = k + 1;
        }
        if (i < pattern.length()) buffer.append(pattern.substring(i));
        return buffer.toString();
    }

}
