package FI.realitymodeler.cimd;

import FI.realitymodeler.*;
import java.io.*;
import java.net.*;
import java.text.ParseException;
import java.sql.*;
import java.util.*;

/** This class runs as one thread, serving one connection
 *  in CIMD proxy. Possibly many CIMD messages are
 *  handled in one connection.
 */
public class CimdRequestThread extends Request {
    CimdEngine cimdEngine;
    InputStream clientIn;
    OutputStream clientOut;
    InputStream serverIn;
    OutputStream serverOut;
    int[] clientPacketNumberDelta = new int[1];
    int[] serverPacketNumberDelta = new int[1];

    public CimdRequestThread(ThreadGroup group, CimdEngine cimdEngine) {
        super(group);
        this.cimdEngine = cimdEngine;
    }

    public static void readCimdMessage(InputStream in,
                                       CimdMessage cimdMessage)
        throws IOException, ParseException {
        cimdMessage.reset();
        for (;;) {
            int c;
            // seeking next <stx>-character
            while ((c = in.read()) != -1 && c != 2);
            if (c == -1) throw new EOFException();
            StringBuffer sb = new StringBuffer();
            // reading whole message ending with <etx>-character
            while ((c = in.read()) != -1 && c != 3) sb.append((char)c);
            if (c != 3) {
                if (c == -1) throw new EOFException();
                continue;
            }
            String message = sb.toString();
            StringTokenizer st = new StringTokenizer(message, "\t", true);
            if (!st.hasMoreTokens()) throw new ParseException("Message is empty", 0);
            String headerString = st.nextToken();
            if (headerString.equals("\t")) throw new ParseException("Message header is invalid", 0);
            int colon = headerString.indexOf(':');
            if (colon == -1) throw new ParseException("Message header has no colon", 0);
            cimdMessage.operationCode = headerString.substring(0, colon);
            cimdMessage.packetNumber = Integer.parseInt(headerString.substring(colon + 1));
            if (!st.hasMoreTokens() || !st.nextToken().equals("\t"))
                throw new ParseException("Message header is not correct", 0);
            while (st.hasMoreTokens()) {
                if (!st.hasMoreTokens()) break;
                String token = st.nextToken();
                if (!st.hasMoreTokens()) {
                    // last element without <tab>-character should be checksum
                    int checkSum = 2, n = message.length() - token.length();
                    for (int i = 0; i < n; i++) {
                        checkSum += (byte)message.charAt(i);
                        checkSum &= 0xff;
                    }
                    if (checkSum != Integer.parseInt(token, 16)) throw new IOException("Check sum is not correct");
                    break;
                }
                colon = token.indexOf(':');
                if (colon == -1) throw new ParseException("Data field has no colon", 0);
                cimdMessage.parameterList.add(new CimdParameter(token.substring(0, colon), token.substring(colon + 1)));
                if (!st.nextToken().equals("\t")) throw new ParseException("Data field is not correct", 0);
            }
            return;
        }
    }

    public static void writeCimdMessage(OutputStream out,
                                        CimdMessage cimdMessage, int packetNumberDelta)
        throws IOException {
        if (cimdMessage.alternativeCimdMessage != null) {
            cimdMessage.alternativeCimdMessage.packetNumber = cimdMessage.packetNumber;
            cimdMessage = cimdMessage.alternativeCimdMessage;
        }
        StringBuffer messageBuffer = new StringBuffer();
        // appending <stx>-character
        messageBuffer.append((char)2);
        // appending header
        messageBuffer.append(cimdMessage.operationCode);
        messageBuffer.append(':');
        StringBuffer sb = new StringBuffer(String.valueOf(
                                                          updatePacketNumber(cimdMessage.packetNumber, packetNumberDelta)));
        for (int n = sb.length(); n < 3; n++) sb.insert(0, '0');
        messageBuffer.append(sb.toString());
        messageBuffer.append('\t');
        // appending possible data fields
        Iterator iter = cimdMessage.parameterList.iterator();
        while (iter.hasNext()) {
            CimdParameter parameter = (CimdParameter)iter.next();
            messageBuffer.append(parameter.parameterType);
            messageBuffer.append(':');
            messageBuffer.append(parameter.parameterValue);
            messageBuffer.append('\t');
        }
        // appending check sum
        int checkSum = 0, n = messageBuffer.length();
        for (int i = 0; i < n; i++) {
            checkSum += messageBuffer.charAt(i);
            checkSum &= 0xff;
        }
        sb = new StringBuffer(Integer.toString(checkSum, 16));
        for (n = sb.length(); n < 2; n++) sb.insert(0, '0');
        messageBuffer.append(sb.toString().toUpperCase());
        // appending <etx>-character
        messageBuffer.append((char)3);
        out.write(messageBuffer.toString().getBytes());
        out.flush();
    }

    public static final int updatePacketNumber(int packetNumber, int delta) {
        packetNumber += delta;
        if (packetNumber > 255) packetNumber %= 256;
        else if (packetNumber < 0) packetNumber = -packetNumber % 256;
        return packetNumber;
    }

    public static final int updatePacketNumberDelta(int packetNumberDelta, int delta) {
        packetNumberDelta += delta;
        if (packetNumberDelta > 255) packetNumberDelta %= 256;
        else if (packetNumberDelta < -255) packetNumberDelta %= 256;
        return packetNumberDelta;
    }

    /** This method handles request to server/client checking
     *  if there are interleaved requests from either party
     *  in the middle of operations.
     *  @param request CIMD request to be handled
     *  @param fromIn input stream where response will come
     *  @param fromOut output stream where request should be written
     *  @param toIn input stream where request came
     *  @param toOut output stream where response should be written
     *  @param fromServer indicates if request came from server side
     */
    void handleRequest(CimdMessage originalRequest, CimdMessage request,
                       InputStream fromIn, OutputStream fromOut,
                       InputStream toIn, OutputStream toOut,
                       int fromPacketNumberDelta[], int toPacketNumberDelta[],
                       boolean fromServer, int recursionLevel) throws IOException, ParseException, SQLException {
        if (++recursionLevel > 5) throw new EOFException("Too many levels of interleaved requests");
        request.startTransaction = System.currentTimeMillis();
        request.ipAddress = socket.getInetAddress();
        CimdOperation operation = cimdEngine.cimdOperations.get(request.operationCode);
        if (operation != null && operation.handle(request)) throw new EOFException("Aborted");
        CimdMessage message = new CimdMessage(), extraMessage = null, messageExtra = null, extraResponse = null;
        if (!request.skip) {
            if (CimdEngine.DEBUG) cimdEngine.log("write to "
                                               + (fromServer ? "client" : "server")
                                               + " request " + (request.alternativeCimdMessage != null
                                                                ? request.alternativeCimdMessage : request)
                                               + ",packetNumberDelta=" + fromPacketNumberDelta[0]);
            writeCimdMessage(fromOut, request, fromPacketNumberDelta[0]);
            for (;;) {
                readCimdMessage(fromIn, message);
                if (CimdEngine.DEBUG) cimdEngine.log("read from "
                                                   + (fromServer ? "client" : "server")
                                                   + " message " + message);
                // If packet number of message received from client is odd or
                // packet number of message received from server is even,
                // it indicates that it is a request.
                if (fromServer ? (message.packetNumber % 2 != 0) : (message.packetNumber % 2 == 0))
                    handleRequest(originalRequest, message, toIn, toOut, fromIn, fromOut,
                                  toPacketNumberDelta, fromPacketNumberDelta, !fromServer, recursionLevel);
                else break;
                message = new CimdMessage();
            }
            if (operation != null && operation.handle(request, message, null, null)) {
                extraMessage = new CimdMessage();
                readCimdMessage(fromIn, extraMessage);
                if (CimdEngine.DEBUG) cimdEngine.log("read from "
                                                   + (fromServer ? "client" : "server")
                                                   + " extra message " + extraMessage);
                extraMessage.startTransaction = request.startTransaction;
                extraMessage.ipAddress = request.ipAddress;
                operation = cimdEngine.cimdOperations.get(extraMessage.operationCode);
                if (operation != null) operation.handle(request, message, extraMessage, null);
            }
        } else {
            message.reset();
            message.packetNumber = request.packetNumber;
            if (operation != null && operation.handle(request, message, null, null)) {
                extraMessage = new CimdMessage();
                operation.handle(request, message, extraMessage, null);
            }
        }
        if (CimdEngine.DEBUG) cimdEngine.log("write to "
                                           + (fromServer ? "server" : "client")
                                           + " response " + (message.alternativeCimdMessage != null
                                                             ? message.alternativeCimdMessage : message)
                                           + ",packetNumberDelta=" + -fromPacketNumberDelta[0]);
        writeCimdMessage(toOut, message, -fromPacketNumberDelta[0]);
        if (request.skip) fromPacketNumberDelta[0] = updatePacketNumberDelta(fromPacketNumberDelta[0], -2);
        if (extraMessage != null) {
            messageExtra = new CimdMessage();
            if (!extraMessage.skip) {
                if (CimdEngine.DEBUG) cimdEngine.log("write to "
                                                   + (fromServer ? "server" : "client")
                                                   + " extra message " + (extraMessage.alternativeCimdMessage != null
                                                                          ? extraMessage.alternativeCimdMessage : extraMessage)
                                                   + ",packetNumberDelta=" + toPacketNumberDelta[0]);
                writeCimdMessage(toOut, extraMessage, toPacketNumberDelta[0]);
                for (;;) {
                    readCimdMessage(toIn, messageExtra);
                    if (CimdEngine.DEBUG) cimdEngine.log("read from "
                                                       + (fromServer ? "server" : "client")
                                                       + " message " + messageExtra);
                    if (!fromServer ? (messageExtra.packetNumber % 2 != 0) : (messageExtra.packetNumber % 2 == 0))
                        handleRequest(originalRequest, messageExtra, fromIn, fromOut, toIn, toOut,
                                      fromPacketNumberDelta, toPacketNumberDelta, fromServer, recursionLevel);
                    else break;
                    messageExtra = new CimdMessage();
                }
            } else {
                messageExtra.reset();
                messageExtra.packetNumber = extraMessage.packetNumber;
            }
            if (operation != null) operation.handle(request, message, extraMessage, messageExtra);
            if (CimdEngine.DEBUG) cimdEngine.log("write to "
                                               + (fromServer ? "client" : "server")
                                               + " message " + (messageExtra.alternativeCimdMessage != null
                                                                ? messageExtra.alternativeCimdMessage : messageExtra)
                                               + ",packetNumberDelta=" + -toPacketNumberDelta[0]);
            writeCimdMessage(fromOut, messageExtra, -toPacketNumberDelta[0]);
            if (extraMessage.skip) toPacketNumberDelta[0] = updatePacketNumberDelta(toPacketNumberDelta[0], -2);
        }
        CimdMessage extraRequest = operation != null ? operation.sendExtra(
                                                                           request, message, extraMessage, messageExtra) : null;
        if (extraRequest != null) {
            extraRequest.packetNumber = originalRequest.packetNumber;
            serverPacketNumberDelta[0] = updatePacketNumberDelta(serverPacketNumberDelta[0], 2);
            if (CimdEngine.DEBUG) cimdEngine.log("write to server extra request "
                                               + (extraRequest.alternativeCimdMessage != null ? extraRequest.alternativeCimdMessage : extraRequest)
                                               + ",packetNumberDelta=" + serverPacketNumberDelta[0]);
            writeCimdMessage(serverOut, extraRequest, serverPacketNumberDelta[0]);
            extraResponse = new CimdMessage();
            // This response is not sent anywhere because it comes from extra request
            for (;;) {
                readCimdMessage(serverIn, extraResponse);
                if (CimdEngine.DEBUG) cimdEngine.log("read from server message " + extraResponse);
                if (extraResponse.packetNumber % 2 == 0)
                    handleRequest(originalRequest, extraResponse, clientIn, clientOut, serverIn, serverOut,
                                  clientPacketNumberDelta, serverPacketNumberDelta, true, recursionLevel);
                else break;
                extraResponse = new CimdMessage();
            }
        }
        if (operation != null) {
            request.successIndicator = "1";
            request.endTransaction = System.currentTimeMillis();
            if (extraMessage != null) {
                extraMessage.successIndicator = "1";
                extraMessage.endTransaction = request.endTransaction;
            }
            operation.log(request, message,
                          extraMessage, messageExtra,
                          extraRequest, extraResponse);
        }
    }

    public void run() {
        Socket smscSocket = null;
        while (!isInterrupted())
            try {
                clientIn = socket.getInputStream();
                clientOut = socket.getOutputStream();
                serverPacketNumberDelta[0] = clientPacketNumberDelta[0] = 0;
                if (CimdEngine.DEBUG) cimdEngine.log("opening smscSocket");
                smscSocket = new Socket(cimdEngine.smscAddress, cimdEngine.smscPort);
                smscSocket.setSoTimeout(30000);
                serverIn = smscSocket.getInputStream();
                serverOut = smscSocket.getOutputStream();
                for (;;) {
                    CimdMessage request = new CimdMessage();
                    readCimdMessage(clientIn, request);
                    if (CimdEngine.DEBUG) cimdEngine.log("read from client request " + request);
                    handleRequest(request, request, serverIn, serverOut, clientIn, clientOut,
                                  serverPacketNumberDelta, clientPacketNumberDelta, false, 0);
                }
            } catch (Exception ex) {
                if (ex instanceof InterruptedException) return;
                cimdEngine.handle(ex);
            } finally {
                try {
                    socket.close();
                } catch (IOException ex) {
                    cimdEngine.handle(ex);
                } finally {
                    try {
                        if (smscSocket != null) smscSocket.close();
                    } catch (IOException ex) {
                        cimdEngine.handle(ex);
                    } finally {
                        smscSocket = null;
                        try {
                            if (cimdEngine.release(this)) return;
                        } catch (InterruptedException ex) {
                            return;
                        }
                    }
                }
            }
    }

    public Object clone() {
        CimdRequestThread cimdRequestThread = new CimdRequestThread(getThreadGroup(), cimdEngine);
        return cimdRequestThread;
    }

}
