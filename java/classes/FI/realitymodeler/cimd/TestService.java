package FI.realitymodeler.cimd;

import java.io.*;
import java.net.*;
import java.util.*;

/** This simple test service echoes short message back to mobile phone.
 */
public class TestService {
    static String smsgwHost = "localhost";
    static int smsgwPort = 6789;
    static String smsgwUsername = "14555smsgw2";
    static String smsgwPassword = "smsgw";

    public static void main(String argv[]) throws Throwable {
        Properties properties = new Properties();
        properties.load(new FileInputStream("TestService.properties"));
        smsgwHost = properties.getProperty("smsgwHost");
        smsgwPort = Integer.parseInt(properties.getProperty("smsgwPort"));
        smsgwUsername = properties.getProperty("smsgwUsername");
        smsgwPassword = properties.getProperty("smsgwPassword");
        CimdMessage request = new CimdMessage(), response = new CimdMessage(), message = new CimdMessage();
        Socket socket = null;
        for (;;)
            try {
                socket = new Socket(smsgwHost, smsgwPort);
                socket.setSoTimeout(30000);
                InputStream in = socket.getInputStream();
                OutputStream out = socket.getOutputStream();
                request.reset();
                request.operationCode = CimdOperation.login;
                request.parameterList.add(new CimdParameter(CimdOperation.userIdentityType, smsgwUsername));
                request.parameterList.add(new CimdParameter(CimdOperation.passwordType, smsgwPassword));
                request.packetNumber = 1;
                CimdRequestThread.writeCimdMessage(out, request, 0);
                System.out.println("wrote request " + request);
                CimdRequestThread.readCimdMessage(in, response);
                System.out.println("read response " + response);
                if (response.parameterList.isEmpty()) {
                    request.reset();
                    //                request.operationCode = CimdOperation.deliveryRequest;
                    request.operationCode = CimdOperation.alive;
                    request.packetNumber = 3;
                    CimdRequestThread.writeCimdMessage(out, request, 0);
                    System.out.println("wrote request " + request);
                    int packetNumber = 5;
                    for (;;) {
                        CimdRequestThread.readCimdMessage(in, message);
                        System.out.println("read message " + message);
                        if (message.operationCode.equals(CimdOperation.deliverStatusReport)) {
                            message.reset();
                            message.operationCode = CimdOperation.deliverStatusReportResponse;
                            CimdRequestThread.writeCimdMessage(out, message, 0);
                            System.out.println("wrote response " + message);
                            continue;
                        }
                        if (message.operationCode.equals(CimdOperation.alive)) {
                            message.reset();
                            message.operationCode = CimdOperation.aliveResponse;
                            CimdRequestThread.writeCimdMessage(out, message, 0);
                            System.out.println("wrote response " + message);
                            continue;
                        }
                        if (message.operationCode.equals(CimdOperation.deliveryRequestResponse)) {
                            CimdRequestThread.readCimdMessage(in, message);
                            message.reset();
                            message.operationCode = CimdOperation.deliverResponse;
                            CimdRequestThread.writeCimdMessage(out, message, 0);
                            System.out.println("wrote response " + message);
                            continue;
                        }
                        if (!message.operationCode.equals(CimdOperation.deliver)) continue;
                        String shortNumber = null, msisdn = null;
                        int parameterIndex = message.parameterList.indexOf(CimdOperation.destinationAddressParameter);
                        if (parameterIndex != -1)
                            shortNumber = message.parameterList.get(parameterIndex).parameterValue;
                        parameterIndex = message.parameterList.indexOf(CimdOperation.originatorAddressParameter);
                        if (parameterIndex != -1)
                            msisdn =  message.parameterList.get(parameterIndex).parameterValue;
                        String userText = CimdOperation.getUserText(message);
                        message.reset();
                        message.operationCode = CimdOperation.deliverResponse;
                        CimdRequestThread.writeCimdMessage(out, message, 0);
                        System.out.println("wrote response " + message);
                        request.reset();
                        request.operationCode = CimdOperation.submit;
                        request.parameterList.add(new CimdParameter(CimdOperation.destinationAddressType, msisdn));
                        request.parameterList.add(new CimdParameter(CimdOperation.originatorAddressType, shortNumber));
                        request.parameterList.add(new CimdParameter(CimdOperation.dataCodingSchemeType, "0"));
                        request.parameterList.add(new CimdParameter(CimdOperation.userDataType, CimdOperation.encodeData("echoed " + userText)));
                        request.parameterList.add(new CimdParameter(CimdOperation.tariffClassType, "2"));
                        request.packetNumber = packetNumber;
                        if ((packetNumber += 2) > 255) packetNumber = 1;
                        CimdRequestThread.writeCimdMessage(out, request, 0);
                        System.out.println("wrote request " + request);
                    }
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            } finally {
                if (socket != null) socket.close();
                Thread.sleep(10000L);
            }
    }

}
