package FI.realitymodeler.cimd;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/** This class serves as store of one CIMD message. It also contains distinct
 *  fields of some CIMD message parameters picked by CimdOperation-handle
 *  method. These fields can be assigned differently depending on the message type.
 */
public class CimdMessage implements Cloneable {
    List<CimdParameter> parameterList = new ArrayList<CimdParameter>();
    InetAddress ipAddress;
    CimdMessage alternativeCimdMessage;
    CimdMessage maxPriceCimdMessage;
    String billingProfile;
    String keyword;
    String mode;
    String msisdn;
    String operationCode;
    String randomKey;
    String servicePrice;
    String shortNumber;
    String successIndicator;
    String tariffClass;
    boolean skip = false;
    boolean billingProfileMOStatus;
    boolean billingProfileMTStatus;
    boolean billingProfileMEGAPUSHStatus;
    boolean foundMaxPriceMessage;
    double servicePriceMO;
    double servicePriceMT;
    double servicePriceMEGAPUSH;
    int packetNumber;
    long startTransaction;
    long endTransaction;

    public void reset() {
        parameterList.clear();
        alternativeCimdMessage = null;
        maxPriceCimdMessage = null;
        billingProfile = null;
        ipAddress = null;
        keyword = null;
        mode = null;
        msisdn = null;
        operationCode = null;
        randomKey = null;
        servicePrice = null;
        shortNumber = null;
        successIndicator = null;
        tariffClass = null;
        skip = false;
        billingProfileMOStatus = false;
        billingProfileMTStatus = false;
        billingProfileMEGAPUSHStatus = false;
        foundMaxPriceMessage = false;
        servicePriceMO = 0.0;
        servicePriceMT = 0.0;
        servicePriceMEGAPUSH = 0.0;
        startTransaction = 0L;
        endTransaction = 0L;
    }

    public Object clone() {
        CimdMessage message = new CimdMessage();
        message.parameterList = (List)((ArrayList)parameterList).clone();
        message.ipAddress = ipAddress;
        message.maxPriceCimdMessage = maxPriceCimdMessage;
        message.billingProfile = billingProfile;
        message.keyword = keyword;
        message.mode = mode;
        message.msisdn = msisdn;
        message.operationCode = operationCode;
        message.randomKey = randomKey;
        message.servicePrice = servicePrice;
        message.shortNumber = shortNumber;
        message.successIndicator = successIndicator;
        message.tariffClass = tariffClass;
        message.skip = skip;
        message.billingProfileMOStatus = billingProfileMOStatus;
        message.billingProfileMTStatus = billingProfileMTStatus;
        message.billingProfileMEGAPUSHStatus = billingProfileMEGAPUSHStatus;
        message.servicePriceMO = servicePriceMO;
        message.servicePriceMT = servicePriceMT;
        message.servicePriceMEGAPUSH = servicePriceMEGAPUSH;
        message.packetNumber = packetNumber;
        message.startTransaction = startTransaction;
        message.endTransaction = endTransaction;
        return message;
    }

    public String toString() {
        return getClass().getName()
            + "{operationCode=" + operationCode
            + ", parameterList=" + parameterList
            + ", packetNumber=" + packetNumber + "}";
    }

}
