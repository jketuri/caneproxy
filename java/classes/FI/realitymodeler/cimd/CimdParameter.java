package FI.realitymodeler.cimd;

/** This class serves as store of one CIMD parameter.
 */
public class CimdParameter implements Cloneable {
    String parameterType;
    String parameterValue;

    public CimdParameter(String parameterType, String parameterValue) {
        this.parameterType = parameterType;
        this.parameterValue = parameterValue;
    }

    public boolean equals(Object o) {
        return ((CimdParameter)o).parameterType.equals(parameterType);
    }

    public String toString() {
        return getClass().getName()
            + "{parameterType=" + parameterType
            + ", parameterValue=" + parameterValue + "}";
    }

    public Object clone() {
        return new CimdParameter(parameterType, parameterValue);
    }

}
