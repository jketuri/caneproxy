package FI.realitymodeler.cimd;

import java.io.*;
import java.net.*;
import java.sql.*;
import java.util.*;

public class DeliveryRequestOperation extends CimdOperation {

    public DeliveryRequestOperation(CimdEngine cimdEngine) {
        super(cimdEngine);
    }

    public boolean handle(CimdMessage request) {
        int parameterIndex = request.parameterList.indexOf(modeParameter);
        if (parameterIndex != -1) {
            request.mode = request.parameterList.get(parameterIndex).parameterValue;
            request.parameterList.remove(parameterIndex);
        } else request.mode = "";
        return false;
    }

    public boolean handle(CimdMessage request, CimdMessage response, CimdMessage message) throws IOException {
        // Other response message should follow if there is positive response from Delivery Request
        return response.operationCode.equals(deliveryRequestResponse) && response.parameterList.isEmpty();
    }

    public void log(CimdMessage request, CimdMessage response,
                    CimdMessage message, CimdMessage messageResponse,
                    CimdMessage extraRequest, CimdMessage extraResponse) throws IOException, SQLException {
    }

}
