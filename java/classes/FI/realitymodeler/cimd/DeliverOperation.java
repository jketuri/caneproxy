package FI.realitymodeler.cimd;

import java.io.*;
import java.net.*;
import java.sql.*;
import java.util.*;

public class DeliverOperation extends CimdOperation {

    public DeliverOperation(CimdEngine cimdEngine) {
        super(cimdEngine);
    }

    public boolean handle(CimdMessage request) throws SQLException {
        if (!request.operationCode.equals(deliver)) return false;
        // In case of receiving-SME's deliver message can be in request from server
        int parameterIndex = request.parameterList.indexOf(tariffClassParameter);
        if (parameterIndex != -1) {
            request.tariffClass = request.parameterList.get(parameterIndex).parameterValue;
            request.parameterList.remove(parameterIndex);
        } else request.tariffClass = "";
        parameterIndex = request.parameterList.indexOf(destinationAddressParameter);
        if (parameterIndex != -1)
            request.shortNumber = request.parameterList.get(parameterIndex).parameterValue;
        else request.shortNumber = "";
        parameterIndex = request.parameterList.indexOf(originatorAddressParameter);
        if (parameterIndex != -1)
            request.msisdn = request.parameterList.get(parameterIndex).parameterValue;
        else request.msisdn = "";
        request.keyword = getUserText(request);
        double maximumPrice = -1.0;
        if (CimdEngine.TEST) {
            if (request.msisdn.indexOf(cimdEngine.testProperties.getProperty("forbiddenPhoneNumber")) != -1) {
                request.skip = true;
                return false;
            }
        } else {
            /*
              cimdEngine.connect();
              ForbiddenPhoneNumber forbiddenPhoneNumber = cimdEngine.dataManager.createForbiddenPhoneNumber();
              if (forbiddenPhoneNumber.checkPattern(request.shortNumber, request.msisdn)) {
              request.skip = true;
              return false;
              }
            */
        }
        StringTokenizer st = new StringTokenizer(request.keyword);
        if (st.hasMoreTokens()) {
            String firstKeyword = st.nextToken();
            request.maxPriceCimdMessage = cimdEngine.maxPriceCimdMessages.get(firstKeyword);
            if (request.maxPriceCimdMessage != null) {
                if (request.maxPriceCimdMessage.foundMaxPriceMessage) {
                    request.maxPriceCimdMessage = null;
                    request.skip = true;
                } else {
                    request.maxPriceCimdMessage.alternativeCimdMessage = null;
                    if (request.maxPriceCimdMessage.operationCode.equals(submit)) request.skip = true;
                    else request.alternativeCimdMessage = request.maxPriceCimdMessage;
                    request.foundMaxPriceMessage = request.maxPriceCimdMessage.foundMaxPriceMessage = true;
                }
                return false;
            }
        }
        /*
          if (CimdEngine.TEST) {
          maximumPrice = Double.parseDouble(cimdEngine.testProperties.getProperty("maximumPrice"));
          request.billingProfileMOStatus = Integer.parseInt(cimdEngine.testProperties.getProperty("MOStatus")) != 0;
          request.billingProfileMTStatus = Integer.parseInt(cimdEngine.testProperties.getProperty("MTStatus")) != 0;
          request.billingProfileMEGAPUSHStatus = Integer.parseInt(cimdEngine.testProperties.getProperty("MEGAPUSHStatus")) != 0;
          if (request.billingProfileMOStatus) {
          st = new StringTokenizer(request.keyword);
          String keyword = st.nextToken().toLowerCase();
          if (keyword.equals(cimdEngine.testProperties.getProperty("MOKeyword")))
          request.servicePriceMO = Double.parseDouble(cimdEngine.testProperties.getProperty("MOPrice"));
          else request.servicePriceMO = Double.parseDouble(cimdEngine.testProperties.getProperty("MODefaultPrice"));
          }
          } else {
          ServiceNumberProfile serviceNumberProfile = cimdEngine.dataManager.createServiceNumberProfile();
          serviceNumberProfile.setShortNumber(request.shortNumber);
          if (serviceNumberProfile.load()) {
          maximumPrice = serviceNumberProfile.getMaximumPrice();
          request.billingProfileMOStatus = serviceNumberProfile.getBillingProfileMO();
          request.billingProfileMTStatus = serviceNumberProfile.getBillingProfileMT();
          request.billingProfileMEGAPUSHStatus = serviceNumberProfile.getBillingProfileMEGAPUSH();
          if (request.billingProfileMOStatus) {
          st = new StringTokenizer(request.keyword.toLowerCase());
          BillingProfileMO billingProfileMO = cimdEngine.dataManager.createBillingProfileMO();
          for (boolean isFirst = true; st.hasMoreTokens();) {
          String keyword = st.nextToken();
          billingProfileMO.setShortNumber(request.shortNumber);
          billingProfileMO.setKeyword(keyword);
          billingProfileMO.setType(isFirst);
          if (billingProfileMO.load()) request.servicePriceMO += billingProfileMO.getPrice();
          else if (isFirst) {
          billingProfileMO.setShortNumber(request.shortNumber);
          billingProfileMO.setKeyword("DEFAULT");
          billingProfileMO.setType(true);
          if (billingProfileMO.load())
          request.servicePriceMO = billingProfileMO.getPrice();
          break;
          }
          isFirst = false;
          }
          }
          }
          }
        */
        double servicePrice = request.servicePriceMO;
        // If short message price is over maximum price
        if (maximumPrice >= 0.0 && servicePrice > maximumPrice) {
            if (CimdEngine.DEBUG) cimdEngine.log("Deliver over maximum price");
            request.maxPriceCimdMessage = makeMaxPriceCimdMessage(request, servicePrice);
            request.skip = true;
        }
        return false;
    }

    public boolean handle(CimdMessage request, CimdMessage response, CimdMessage message, CimdMessage messageResponse) throws IOException, SQLException {
        if (messageResponse != null) {
            messageResponse.alternativeCimdMessage = new CimdMessage();
            messageResponse.alternativeCimdMessage.operationCode = deliverResponse;
        } else if (message != null) {
            handle(message);
            if (message.skip) {
                response.alternativeCimdMessage = new CimdMessage();
                response.alternativeCimdMessage.operationCode = deliveryRequestResponse;
                response.alternativeCimdMessage.parameterList.add(new CimdParameter(errorCodeType, "500"));
                response.alternativeCimdMessage.parameterList.add(new CimdParameter(errorTextType, "no messages available"));
            }
        } else if (response != null && request.skip) {
            response.alternativeCimdMessage = new CimdMessage();
            response.alternativeCimdMessage.operationCode = deliverResponse;
        }
        return false;
    }

    public CimdMessage sendExtra(CimdMessage request, CimdMessage response, CimdMessage message, CimdMessage messageResponse) throws IOException {
        // In case of receiving-SME's deliver message can be in request from server
        if (request.operationCode.equals(deliver)) message = request;
        if (message == null) return null;
        return message.skip ? message.maxPriceCimdMessage : null;
    }

    public void log(CimdMessage request, CimdMessage response,
                    CimdMessage message, CimdMessage messageResponse,
                    CimdMessage extraRequest, CimdMessage extraResponse) throws IOException, SQLException {
        // In case of receiving-SME's Deliver message can be in request from server
        if (request.operationCode.equals(deliver)) {
            message = request;
            messageResponse = response;
        }
        if (message.foundMaxPriceMessage) {
            if (message.skip) logMessage(message.maxPriceCimdMessage, extraResponse, submitResponse);
            else logMessage(message.maxPriceCimdMessage, messageResponse, deliverResponse);
            return;
        }
        if (message.skip) return;
        if (message.billingProfileMOStatus) logMessage(message, messageResponse, deliverResponse);
    }

}
