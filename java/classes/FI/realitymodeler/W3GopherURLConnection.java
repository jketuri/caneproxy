
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;
import javax.servlet.*;

class GopherPipingThread extends CacheFillingThread {
    String title;
    boolean mobile = false;
    RegexpPool listFilter = null;

    GopherPipingThread(W3GopherURLConnection uc) {
        super(uc);
    }

    public void run() {
        try {
            writeBytes("<html><head><title>" + title + "</title></head><body>\n");
            writeBytes("<h1>" + title + "</h1>\n");
            writeBytes("<pre>\n");
            String s;
            while ((s = Support.readLine(source)) != null) {
                StringTokenizer st = new StringTokenizer(s, "\t");
                s = st.nextToken();
                char type = s.charAt(0);
                if (type == '1') s += "/";
                s = s.substring(1);
                if (listFilter != null && listFilter.match(s) == null) continue;
                try {
                    String selector = Support.encodePath(st.nextToken()), host = st.nextToken(), port = st.nextToken();
                    writeBytes("<a href=\"gopher://" + host + ":" + port + "/" + type + selector + (type != '7' ? "?" + URLEncoder.encode(s, uc.charsetName) : "") + "\">" + Support.htmlString(s) + "</a>\n");
                } catch (NoSuchElementException ex) {
                    writeBytes(Support.htmlString(s) + "\n");
                }
            }
            writeBytes("</pre></body></html>\n");
            out.close();
        } catch (Exception ex) {
            try {
                if (in != null)
                    synchronized (in) {
                        in.close();
                        in.notifyAll();
                    }
            } catch (IOException ex1) {} finally {
                uc.log(getClass().getName(), ex);
            }
        }
    }

}

/** Implements cacheable URL connection to gopher-servers. URL must be of form: (rfc1738)<br>
    gopher://host[:port]/gopher-path
*/
public class W3GopherURLConnection extends W3URLConnection {
    public static final int GOPHER_PORT = 70;
    public static long cacheCleaningInterval = 7L * 24L * 60L * 60L * 1000L, cacheRefreshingInterval =  48L * 60L * 60L * 1000L, cacheCleaningTime = 0L;
    public static String cacheRoot = "cache/gopher/";
    public static boolean defaultUseCaches = false, useProxy = false;
    public static String proxyHost = null;
    public static int proxyPort = 80;

    static HashMap<Character,String> types = new HashMap<Character,String>();
    static {
        types.put(new Character('0'), Support.plainType);
        types.put(new Character('1'), Support.htmlType);
        types.put(new Character('2'), Support.plainType);
        types.put(new Character('3'), Support.plainType);
        types.put(new Character('4'), "application/mac-binhex40");
        types.put(new Character('5'), Support.octetStreamType);
        types.put(new Character('6'), Support.octetStreamType);
        types.put(new Character('7'), Support.htmlType);
        types.put(new Character('8'), "application/x-telnet");
        types.put(new Character('9'), Support.octetStreamType);
        types.put(new Character('+'), Support.plainType);
        types.put(new Character('T'), "application/x-tn3270");
        types.put(new Character('g'), "image/gif");
    }

    GopherPipingThread piping;
    char type;

    public static void cleanCache(ServletContext servletContext) {
        cacheCleaningTime = cleanCache(servletContext, cacheRoot, cacheCleaningInterval);
    }

    public W3GopherURLConnection(URL url) {
        super(url);
    }

    public void checkCacheCleaning() {
        if (cacheCleaningInterval >= 0L &&
            System.currentTimeMillis() - cacheCleaningTime > cacheCleaningInterval) cleanCache(servletContext);
    }

    public void open()
        throws IOException {
        if (requester != null) return;
        if (useProxy) {
            proxyConnect(proxyHost, proxyPort);
            return;
        }
        requester = new W3Requester(url.getHost(), url.getPort() != -1 ? url.getPort() : GOPHER_PORT, W3Socket.NORMAL, localAddress, localPort);
        if (timeout > 0) requester.setTimeout(timeout);
    }

    public void doConnect()
        throws IOException {
        if (connected) return;
        if (checkCacheFile(cacheRoot, GOPHER_PORT)) return;
        open();
    }

    public InputStream getInput(boolean multiple)
        throws IOException {
        if (useProxy) return super.getInput(multiple);
        if (verbose)
            requestHeaderFields.dump(logStream);
        mayKeepAlive = false;
        type = path.length() > 1 ? path.charAt(1) : '1';
        String ct = types.get(new Character(type));
        if (ct == null) ct = Support.unknownType;
        headerFields = new HeaderList();
        headerFields.append(new Header("", protocol_1_1 + " " + HTTP_OK + " OK"));
        headerFields.append(new Header("Content-Type", ct));
        requestTime = System.currentTimeMillis();
        GopherPipingThread piping = new GopherPipingThread(this);
        filling = piping;
        piping.mobile = isMobile();
        path = path.length() > 2 ? Support.decodePath(path.substring(2)) : "";
        piping.title = Support.htmlString(url.getHost());
        if (query != null) {
            HashMap<String,Object> qp = new HashMap<String,Object>();
            parseQuery(query, qp);
            String s = Support.getParameter(qp, "");
            if (s != null)
                if (type == '7') {
                    path += "\t" + s;
                    type = '1';
                } else piping.title = Support.htmlString(s);
        } else if (type == '7') {
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            Support.writeBytes(bout, "<html><head><title>" + piping.title + "</title>\n", charsetName);
            Support.writeBytes(bout, "<h1>" + piping.title + "</h1><isindex></head></html>", charsetName);
            byte b[] = bout.toByteArray();
            headerFields.append(new Header("Content-Length", String.valueOf(b.length)));
            Support.copyHeaderList(headerFields, headerList = new HeaderList());
            return in = new ByteArrayInputStream(b);
        }
        Support.copyHeaderList(headerFields, headerList = new HeaderList());
        for (int tries = 1; tries <= numberOfTries; tries++) {
            try {
                Support.writeBytes(requester.output, path + "\r\n", null);
                requester.output.flush();
                break;
            } catch (IOException ex) {
                if (tries == numberOfTries) throw ex;
            }
            log("Retrying to make request");
        }
        in = requester.input;
        boolean list = type == '1';
        if (type < '4' || type == '+') in = Support.getTextInputStream(Support.getDotInputStream(in));
        if (!list) return checkMessage(cacheHeaders);
        if (params != null) {
            String s = params.get("filter");
            if (s != null)
                try {
                    (piping.listFilter = new RegexpPool()).add(s, Boolean.TRUE);
                } catch (RegexException ex) {
                    piping.listFilter = null;
                }
        }
        piping.setup(this);
        in = new BufferedInputStream(piping.in = new PipedInputStream(piping.out = new PipedOutputStream()));
        piping.start();
        return in;
    }

    public InputStream getInputStream()
        throws IOException {
        if (inputDone) return in;
        cached = false;
        connect();
        if (inputDone) return in;
        inputDone = true;
        setRequestMethod();
        setRequestFields();
        return getInput(false);
    }

    public void setDefaultUseCaches(boolean useCaches) {
        W3GopherURLConnection.defaultUseCaches = useCaches;
    }

    public boolean getDefaultUseCaches() {
        return W3GopherURLConnection.defaultUseCaches;
    }

    public int getDefaultPort() {
        return GOPHER_PORT;
    }

    public boolean usingProxy() {
        return useProxy;
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public int getProxyPort() {
        return proxyPort;
    }

    public void setCacheCleaningInterval(long cacheCleaningInterval) {
        W3GopherURLConnection.cacheCleaningInterval = cacheCleaningInterval;
    }

    public void setCacheRefreshingInterval(long cacheRefreshingInterval) {
        W3GopherURLConnection.cacheRefreshingInterval = cacheRefreshingInterval;
    }

    public long getCacheRefreshingInterval() {
        return cacheRefreshingInterval;
    }

}
