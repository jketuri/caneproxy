
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.rmi.server.*;
import java.security.*;
import java.security.cert.*;
import java.util.*;
import javax.net.ssl.*;

/** URL stream factory for URL connection implementations and
    socket factory where MultiSocketImpl can be used when setting protocolInfo variable
    @see FI.realitymodeler.MultiSocketImpl */
public class W3Factory extends RMISocketFactory implements SocketImplFactory, URLStreamHandlerFactory, RMIFailureHandler {
    public static W3Factory instance = null, serverInstance = null;
    public static boolean socketFactoriesSet = false;

    static Map<String, W3URLStreamHandler> URLStreamHandlers = new HashMap<String, W3URLStreamHandler>();
    static {
        URLStreamHandlers.put("file", new FI.realitymodeler.protocol.file.Handler());
        URLStreamHandlers.put("ftp", new FI.realitymodeler.protocol.ftp.Handler());
        URLStreamHandlers.put("gopher", new FI.realitymodeler.protocol.gopher.Handler());
        URLStreamHandlers.put("http", new FI.realitymodeler.protocol.http.Handler());
        URLStreamHandlers.put("https", URLStreamHandlers.get("http"));
        URLStreamHandlers.put("imap4", new FI.realitymodeler.protocol.imap4.Handler());
        URLStreamHandlers.put("imap4s", URLStreamHandlers.get("imap4"));
        URLStreamHandlers.put("ldap", new FI.realitymodeler.protocol.ldap.Handler());
        URLStreamHandlers.put("smtp", new FI.realitymodeler.protocol.smtp.Handler());
        URLStreamHandlers.put("mailto", URLStreamHandlers.get("smtp"));
        URLStreamHandlers.put("smailto", URLStreamHandlers.get("smtp"));
        URLStreamHandlers.put("msg", new FI.realitymodeler.protocol.msg.Handler());
        URLStreamHandlers.put("nntp", new FI.realitymodeler.protocol.nntp.Handler());
        URLStreamHandlers.put("news", URLStreamHandlers.get("nntp"));
        URLStreamHandlers.put("pop3", new FI.realitymodeler.protocol.pop3.Handler());
        URLStreamHandlers.put("pop3s", URLStreamHandlers.get("pop3"));
        URLStreamHandlers.put("resource", new FI.realitymodeler.protocol.resource.Handler());
        URLStreamHandlers.put("sms", new FI.realitymodeler.protocol.sms.Handler());
        URLStreamHandlers.put("wais", new FI.realitymodeler.protocol.wais.Handler());
    }

    public long protocolInfo = 0;
    public int af = 0;
    public int type = MultiSocketImpl.SOCK_STREAM;
    public int protocol = MultiSocketImpl.IPPROTO_IP;
    public int group = 0;
    public int flags = MultiSocketImpl.WSA_FLAG_OVERLAPPED;

    public static synchronized void prepare()
        throws KeyManagementException, NoSuchAlgorithmException {
        if (instance != null) return;
        Support.prepare();
        SSLContext sslContext = SSLContext.getInstance("SSL");
        sslContext.init(null, new TrustManager[] {
                new X509TrustManager() {
                    public X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    public void checkClientTrusted(X509Certificate certificates[], String authType) {
                    }

                    public void checkServerTrusted(X509Certificate certificates[], String authType) {
                    }
                }
            }, new SecureRandom());
        instance = new W3Factory(false);
        serverInstance = new W3Factory(true);
        String pkgs = System.getProperty("java.protocol.handler.pkgs");
        pkgs = "FI.realitymodeler.protocol" + (pkgs != null ? "|" + pkgs : "");
        System.setProperty("java.protocol.handler.pkgs", pkgs);
        //URL.setURLStreamHandlerFactory(W3Factory.instance);
    }

    public static void getSystemProperties()
        throws ProtocolException {
        W3Factory.getSystemProperties(instance, "FI.realitymodeler.W3Factory/");
        W3Factory.getSystemProperties(serverInstance, "FI.realitymodeler.W3Factory/server-");
    }

    public static void getSystemProperties(W3Factory factory,
                                           String prefix)
        throws ProtocolException {
        String s = System.getProperty(prefix + "protocolName");
        if (s != null) factory.protocolInfo = MultiSocketImpl.getProtocolInfo(s);
        s = System.getProperty(prefix + "af");
        if (s != null) factory.af = Integer.parseInt(s);
        s = System.getProperty(prefix + "type");
        if (s != null) factory.type = Integer.parseInt(s);
        s = System.getProperty(prefix + "protocol");
        if (s != null) factory.protocol = Integer.parseInt(s);
        s = System.getProperty(prefix + "group");
        if (s != null) factory.group = Integer.parseInt(s);
        s = System.getProperty(prefix + "flags");
        if (s != null) factory.flags = Integer.parseInt(s);
    }

    public static synchronized void setSocketFactories() {
        if (socketFactoriesSet) return;
        try {
            Socket.setSocketImplFactory(instance);
        } catch (IOException ex) {}
        finally {
            try {
                ServerSocket.setSocketFactory(serverInstance);
            } catch (IOException ex) {}
        }
        socketFactoriesSet = true;
    }

    public static W3URLConnection openConnection(URL url)
        throws IOException {
        W3URLStreamHandler urlStreamHandler = URLStreamHandlers.get(url.getProtocol().toLowerCase());
        if (urlStreamHandler == null) throw new IOException("Unknown protocol " + url.getProtocol());
        return (W3URLConnection)urlStreamHandler.openConnection(url);
    }

    public W3Factory(boolean useServerDefaults) {
        af = useServerDefaults ? MultiSocketImpl.getDefaultAddressFamily() : MultiSocketImpl.AF_INET;
    }

    public Socket createSocket(String host, int port)
        throws IOException {
        return new W3Socket(host, port);
    }

    public ServerSocket createServerSocket(int port)
        throws IOException {
        return new W3ServerSocket(port);
    }

    public boolean failure(Exception ex) {
        return false;
    }

    public SocketImpl createSocketImpl() {
        return protocolInfo == -1L ? new MultiSocketImpl(-1L, 0, 0) :
            protocolInfo != 0 ? new MultiSocketImpl(protocolInfo, group, flags) :
            af != 0 ? new MultiSocketImpl(af, type, protocol, group, flags) :
            new MultiSocketImpl(group, flags);
    }

    public URLStreamHandler createURLStreamHandler(String protocol) {
        return (URLStreamHandler)URLStreamHandlers.get(protocol.toLowerCase());
    }

}
