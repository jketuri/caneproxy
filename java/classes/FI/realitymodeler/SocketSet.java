
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import java.io.*;
import java.lang.reflect.*;
import java.net.*;

public class SocketSet {
    static Field fdField, fd0Field;
    static {
        try {
            Support.loadLibrary("FI_realitymodeler");
            fdField = SocketImpl.class.getDeclaredField("fd");
            fdField.setAccessible(true);
            fd0Field = FileDescriptor.class.getDeclaredField("fd");
            fd0Field.setAccessible(true);
        } catch (Exception ex) {
            throw new Error(ex.toString());
        }
    }

    long fdSet;

    private native long construct();
    private native void destroy(long fdSet);
    private native void clr0(long fdSet, int fd0);
    private native boolean isSet0(long fdSet, int fd0);
    private native void set0(long fdSet, int fd0);
    private native void zero0(long fdSet);

    public SocketSet() {
        if ((fdSet = construct()) == 0) throw new OutOfMemoryError();
    }

    public void clr(W3Socket socket) throws IllegalAccessException {
        clr0(fdSet, fd0Field.getInt(fdField.get(socket.getSocketImpl())));
    }

    public boolean isSet(W3Socket socket) throws IllegalAccessException {
        return isSet0(fdSet, fd0Field.getInt(fdField.get(socket.getSocketImpl())));
    }

    public void set(W3Socket socket) throws IllegalAccessException {
        set0(fdSet, fd0Field.getInt(fdField.get(socket.getSocketImpl())));
    }

    public void zero() {
        zero0(fdSet);
    }

    protected void finalize() {
        if (fdSet != 0) destroy(fdSet);
    }

}
