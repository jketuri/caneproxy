
package FI.realitymodeler.sql;

import FI.realitymodeler.common.*;
import java.io.*;
import java.math.*;
import java.net.*;
import java.sql.*;
import java.util.*;

public class DatabaseResultSet implements ResultSet {
    private Map<Object, Object> context;
    private Map<Object, Object> values;
    private Vector<?> row;
    private Vector<?> selectVector;
    private Database database;

    public DatabaseResultSet(Database database, Vector<?> resultVector, Map<Object, Object> values) {
        this.database = database;
        this.values = values;
        if (resultVector == null) {
            context = null;
            row = null;
        } else if (((Integer)resultVector.firstElement()).intValue() == -1) {
            context = new HashMap<Object, Object>();
            selectVector = (Vector<?>)resultVector.elementAt(1);
        } else {
            context = null;
            row = (Vector<?>)resultVector.elementAt(1);
        }
    }

    public boolean next()
        throws SQLException {
        return relative(1);
    }

    public void close() {
    }

    public boolean wasNull() {
        return false;
    }

    public String getString(int columnIndex)
        throws SQLException {
        Object o = row.elementAt(columnIndex - 1);
        if (o instanceof String) return (String)o;
        if (o instanceof byte[]) return new String((byte[])o);
        return o.toString();
    }

    public boolean getBoolean(int columnIndex)
        throws SQLException {
        return getInt(columnIndex) != 0;
    }

    public byte getByte(int columnIndex)
        throws SQLException {
        return (byte)getInt(columnIndex);
    }

    public short getShort(int columnIndex)
        throws SQLException {
        return (short)getInt(columnIndex);
    }

    public int getInt(int columnIndex)
        throws SQLException {
        Object o = row.elementAt(columnIndex - 1);
        if (o instanceof Number) return ((Number)o).intValue();
        return new Integer((String)o).intValue();
    }

    public long getLong(int columnIndex)
        throws SQLException {
        Object o = row.elementAt(columnIndex - 1);
        if (o instanceof Number) return ((Number)o).longValue();
        return new Long((String)o).longValue();
    }

    public float getFloat(int columnIndex)
        throws SQLException {
        Object o = row.elementAt(columnIndex - 1);
        if (o instanceof Number) return ((Number)o).floatValue();
        return new Float((String)o).floatValue();
    }

    public double getDouble(int columnIndex)
        throws SQLException {
        Object o = row.elementAt(columnIndex - 1);
        if (o instanceof Number) return ((Number)o).doubleValue();
        return new Double((String)o).doubleValue();
    }

    /** @deprecated */
    @Deprecated
    public BigDecimal getBigDecimal(int columnIndex, int scale)
        throws SQLException {
        return getBigDecimal(columnIndex);
    }

    public byte[] getBytes(int columnIndex)
        throws SQLException {
        Object o = row.elementAt(columnIndex - 1);
        if (o instanceof byte[]) return (byte[])o;
        return ((String)o).getBytes();
    }

    public java.sql.Date getDate(int columnIndex)
        throws SQLException {
        Object o = row.elementAt(columnIndex - 1);
        if (o instanceof String) return java.sql.Date.valueOf((String)o);
        return new java.sql.Date(((Number)o).longValue());
    }

    public java.sql.Time getTime(int columnIndex)
        throws SQLException {
        Object o = row.elementAt(columnIndex - 1);
        if (o instanceof String) return java.sql.Time.valueOf((String)o);
        return new java.sql.Time(((Number)o).longValue());
    }

    public Timestamp getTimestamp(int columnIndex)
        throws SQLException {
        Object o = row.elementAt(columnIndex - 1);
        if (o instanceof Timestamp) return (Timestamp)o;
        if (o instanceof String) return Timestamp.valueOf((String)o);
        return new java.sql.Timestamp(((Number)o).longValue());
    }

    public URL getURL(int columnIndex)
        throws SQLException {
        Object o = row.elementAt(columnIndex - 1);
        if (o instanceof URL) return (URL)o;
        try {
            return new URL((String)o);
        } catch (MalformedURLException ex) {
            throw new SQLException(Support.stackTrace(ex));
        }
    }

    public InputStream getAsciiStream(int columnIndex)
        throws SQLException {
        Object o = row.elementAt(columnIndex - 1);
        if (!(o instanceof String[])) throw new SQLException("Column is not stream");
        try {
            return new FileInputStream(((String[])o)[0]);
        } catch (FileNotFoundException ex) {
            throw new SQLException(Support.stackTrace(ex));
        }
    }

    /** @deprecated */
    @Deprecated
    public InputStream getUnicodeStream(int columnIndex)
        throws SQLException {
        return getAsciiStream(columnIndex);
    }

    public InputStream getBinaryStream(int columnIndex)
        throws SQLException {
        return getAsciiStream(columnIndex);
    }

    public String getString(String columnName)
        throws SQLException {
        return getString(findColumn(columnName));
    }

    public boolean getBoolean(String columnName)
        throws SQLException {
        return getBoolean(findColumn(columnName));
    }

    public byte getByte(String columnName)
        throws SQLException {
        return getByte(findColumn(columnName));
    }

    public short getShort(String columnName)
        throws SQLException {
        return getShort(findColumn(columnName));
    }

    public int getInt(String columnName)
        throws SQLException {
        return getInt(findColumn(columnName));
    }

    public long getLong(String columnName)
        throws SQLException {
        return getLong(findColumn(columnName));
    }

    public float getFloat(String columnName)
        throws SQLException {
        return getFloat(findColumn(columnName));
    }

    public double getDouble(String columnName)
        throws SQLException {
        return getDouble(findColumn(columnName));
    }

    /** @deprecated */
    @Deprecated
    public BigDecimal getBigDecimal(String columnName, int scale)
        throws SQLException {
        return getBigDecimal(findColumn(columnName), scale);
    }

    public byte[] getBytes(String columnName)
        throws SQLException {
        return getBytes(findColumn(columnName));
    }

    public java.sql.Date getDate(String columnName)
        throws SQLException {
        return getDate(findColumn(columnName));
    }

    public java.sql.Time getTime(String columnName)
        throws SQLException {
        return getTime(findColumn(columnName));
    }

    public java.sql.Timestamp getTimestamp(String columnName)
        throws SQLException {
        return getTimestamp(findColumn(columnName));
    }

    public URL getURL(String columnName)
        throws SQLException {
        return getURL(findColumn(columnName));
    }

    public InputStream getAsciiStream(String columnName)
        throws SQLException {
        return getAsciiStream(findColumn(columnName));
    }

    /** @deprecated */
    @Deprecated
    public InputStream getUnicodeStream(String columnName)
        throws SQLException {
        return getUnicodeStream(findColumn(columnName));
    }

    public InputStream getBinaryStream(String columnName)
        throws SQLException {
        return getBinaryStream(findColumn(columnName));
    }

    public SQLWarning getWarnings() {
        return null;
    }

    public void clearWarnings() {
    }

    public String getCursorName() {
        return null;
    }

    public ResultSetMetaData getMetaData() {
        return null;
    }

    public Object getObject(int columnIndex)
        throws SQLException {
        return row.elementAt(columnIndex - 1);
    }

    public Object getObject(String columnName)
        throws SQLException {
        return getObject(findColumn(columnName));
    }

    public int findColumn(String columnName)
        throws SQLException {
        try {
            return database.findColumn(columnName);
        } catch (IllegalArgumentException ex) {
            throw new SQLException(Support.stackTrace(ex));
        }
    }

    public Reader getCharacterStream(int columnIndex)
        throws SQLException {
        return new InputStreamReader(getAsciiStream(columnIndex));
    }

    public Reader getCharacterStream(String columnName)
        throws SQLException {
        return new InputStreamReader(getAsciiStream(columnName));
    }

    public BigDecimal getBigDecimal(int columnIndex)
        throws SQLException {
        Object o = row.elementAt(columnIndex - 1);
        if (o instanceof BigDecimal) return (BigDecimal)o;
        if (o instanceof String) return new BigDecimal(new BigInteger((String)o));
        return new BigDecimal(((Number)o).doubleValue());
    }

    public BigDecimal getBigDecimal(String columnName)
        throws SQLException {
        return getBigDecimal(findColumn(columnName));
    }

    public boolean isBeforeFirst()
        throws SQLException {
        return false;
    }

    public boolean isAfterLast()
        throws SQLException {
        return false;
    }

    public boolean isFirst()
        throws SQLException {
        return false;
    }

    public boolean isLast()
        throws SQLException {
        return false;
    }

    public void beforeFirst()
        throws SQLException {
    }

    public void afterLast()
        throws SQLException {
    }

    public boolean first()
        throws SQLException {
        context = new HashMap<Object, Object>();
        return next();
    }

    public boolean last()
        throws SQLException {
        return false;
    }

    public int getRow()
        throws SQLException {
        return 0;
    }

    public boolean absolute(int row)
        throws SQLException {
        if (context == null) throw new SQLException();
        context = new HashMap<Object, Object>();
        return relative(row);
    }

    public boolean relative(int rows)
        throws SQLException {
        if (context == null) throw new SQLException();
        try {
            row = database.move(selectVector, context, values, rows);
        } catch (Exception ex) {
            PrintWriter pw = DriverManager.getLogWriter();
            if (pw != null) ex.printStackTrace(pw);
            throw new SQLException(Support.stackTrace(ex));
        }
        return row != null;
    }

    public boolean previous()
        throws SQLException {
        return relative(-1);
    }

    public void setFetchDirection(int direction)
        throws SQLException {
    }

    public int getFetchDirection()
        throws SQLException {
        return 0;
    }

    public void setFetchSize(int rows)
        throws SQLException {
    }

    public int getFetchSize()
        throws SQLException {
        return 0;
    }

    public int getType()
        throws SQLException {
        return 0;
    }

    public int getConcurrency()
        throws SQLException {
        return 0;
    }

    public boolean rowUpdated()
        throws SQLException {
        return false;
    }

    public boolean rowInserted()
        throws SQLException {
        return false;
    }

    public boolean rowDeleted()
        throws SQLException {
        return false;
    }

    public void updateNull(int columnIndex)
        throws SQLException {
    }

    public void updateBoolean(int columnIndex, boolean x)
        throws SQLException {
    }

    public void updateByte(int columnIndex, byte x)
        throws SQLException {
    }

    public void updateShort(int columnIndex, short x)
        throws SQLException {
    }

    public void updateInt(int columnIndex, int x)
        throws SQLException {
    }

    public void updateLong(int columnIndex, long x)
        throws SQLException {
    }

    public void updateFloat(int columnIndex, float x)
        throws SQLException {
    }

    public void updateDouble(int columnIndex, double x)
        throws SQLException {
    }

    public void updateBigDecimal(int colunmnIndex, BigDecimal x)
        throws SQLException {
    }

    public void updateString(int columnIndex, String x)
        throws SQLException {
    }

    public void updateBytes(int columnIndex, byte x[])
        throws SQLException {
    }

    public void updateDate(int columnIndex, java.sql.Date x)
        throws SQLException {
    }

    public void updateTime(int columnIndex, java.sql.Time x)
        throws SQLException {
    }

    public void updateTimestamp(int columnIndex, Timestamp x)
        throws SQLException {
    }

    public void updateAsciiStream(int columnIndex, InputStream x, int length)
        throws SQLException {
    }

    public void updateBinaryStream(int columnIndex, InputStream x, int length)
        throws SQLException {
    }

    public void updateCharacterStream(int columnIndex, Reader reader, int length)
        throws SQLException {
    }

    public void updateObject(int columnIndex, Object x, int scale)
        throws SQLException {
    }

    public void updateObject(int columnIndex, Object x)
        throws SQLException {
    }

    public void updateRef(int columnIndex, Ref x)
        throws SQLException {
    }

    public void updateBlob(int columnIndex, Blob x)
        throws SQLException {
    }

    public void updateClob(int columnIndex, Clob x)
        throws SQLException {
    }

    public void updateArray(int columnIndex, Array x)
        throws SQLException {
    }

    public void updateNull(String columnName)
        throws SQLException {
    }

    public void updateBoolean(String columnName, boolean x)
        throws SQLException {
    }

    public void updateByte(String columnName, byte x)
        throws SQLException {
    }

    public void updateShort(String columnName, short x)
        throws SQLException {
    }

    public void updateInt(String columnName, int x)
        throws SQLException {
    }

    public void updateLong(String columnName, long x)
        throws SQLException {
    }

    public void updateFloat(String columnName, float x)
        throws SQLException {
    }

    public void updateDouble(String columnName, double x)
        throws SQLException {
    }

    public void updateBigDecimal(String colunmnName, BigDecimal x)
        throws SQLException {
    }

    public void updateString(String columnName, String x)
        throws SQLException {
    }

    public void updateBytes(String columnName, byte x[])
        throws SQLException {
    }

    public void updateDate(String columnName, java.sql.Date x)
        throws SQLException {
    }

    public void updateTime(String columnName, java.sql.Time x)
        throws SQLException {
    }

    public void updateTimestamp(String columnName, Timestamp x)
        throws SQLException {
    }

    public void updateAsciiStream(String columnName, InputStream x, int length)
        throws SQLException {
    }

    public void updateBinaryStream(String columnName, InputStream x, int length)
        throws SQLException {
    }

    public void updateCharacterStream(String columnName, Reader reader, int length)
        throws SQLException {
    }

    public void updateObject(String columnName, Object x, int scale)
        throws SQLException {
    }

    public void updateObject(String columnName, Object x)
        throws SQLException {
    }

    public void updateRef(String columnName, Ref x)
        throws SQLException {
    }

    public void updateBlob(String columnName, Blob x)
        throws SQLException {
    }

    public void updateClob(String columnName, Clob x)
        throws SQLException {
    }

    public void updateArray(String columnName, Array x)
        throws SQLException {
    }

    public void insertRow()
        throws SQLException {
    }

    public void updateRow()
        throws SQLException {
    }

    public void deleteRow()
        throws SQLException {
    }

    public void refreshRow()
        throws SQLException {
    }

    public void cancelRowUpdates()
        throws SQLException {
    }

    public void moveToInsertRow()
        throws SQLException {
    }

    public void moveToCurrentRow()
        throws SQLException {
    }

    public Object getObject(int i, Map<String,Class<?>> map)
        throws SQLException {
        return null;
    }

    public Ref getRef(int i)
        throws SQLException {
        return null;
    }

    public Blob getBlob(int i)
        throws SQLException {
        return null;
    }

    public Clob getClob(int i)
        throws SQLException {
        return null;
    }

    public Array getArray(int i)
        throws SQLException {
        return null;
    }

    public java.sql.Date getDate(int columnIndex, Calendar cal)
        throws SQLException {
        return null;
    }

    public java.sql.Date getDate(String columnName, Calendar cal)
        throws SQLException {
        return null;
    }

    public java.sql.Time getTime(int columnIndex, Calendar cal)
        throws SQLException {
        return null;
    }

    public java.sql.Time getTime(String columnName, Calendar cal)
        throws SQLException {
        return null;
    }

    public Timestamp getTimestamp(int columnIndex, Calendar cal)
        throws SQLException {
        return null;
    }

    public Timestamp getTimestamp(String columnName, Calendar cal)
        throws SQLException {
        return null;
    }


    public Statement getStatement()
        throws SQLException {
        return null;
    }

    public Object getObject(String colName, Map<String,Class<?>> map)
        throws SQLException {
        return null;
    }

    public Ref getRef(String colName)
        throws SQLException {
        return null;
    }

    public Blob getBlob(String colName)
        throws SQLException {
        return null;
    }

    public Clob getClob(String colName)
        throws SQLException {
        return null;
    }

    public Array getArray(String colname)
        throws SQLException {
        return null;
    }

    public RowId getRowId(int columnIndex)
        throws SQLException {
        return null;
    }

    public RowId getRowId(String columnLabel)
        throws SQLException {
        return null;
    }

    public void updateRowId(int columnIndex, RowId rowId)
        throws SQLException {
    }

    public void updateRowId(String columnLabel, RowId rowId)
        throws SQLException {
    }

    public int getHoldability()
        throws SQLException {
        return 0;
    }

    public boolean isClosed()
        throws SQLException {
        return false;
    }

    public void updateNString(int columnIndex, String nString)
        throws SQLException {
    }

    public void updateNString(String columnLabel, String nString)
        throws SQLException {
    }

    public void updateNClob(int columnIndex, NClob nClob)
        throws SQLException {
    }

    public void updateNClob(String columnLabel, NClob nClob)
        throws SQLException {
    }

    public NClob getNClob(int columnIndex)
        throws SQLException {
        return null;
    }

    public NClob getNClob(String columnLabel)
        throws SQLException {
        return null;
    }

    public SQLXML getSQLXML(int columnIndex)
        throws SQLException {
        return null;
    }

    public SQLXML getSQLXML(String columnLabel)
        throws SQLException {
        return null;
    }

    public void updateSQLXML(int columnIndex, SQLXML sqlXml)
        throws SQLException {
    }

    public void updateSQLXML(String columnLabel, SQLXML sqlXml)
        throws SQLException {
    }

    public String getNString(int columnIndex)
        throws SQLException {
        return null;
    }

    public String getNString(String columnLabel)
        throws SQLException {
        return null;
    }

    public Reader getNCharacterStream(int columnIndex)
        throws SQLException {
        return null;
    }

    public Reader getNCharacterStream(String columnLabel)
        throws SQLException {
        return null;
    }

    public void updateNCharacterStream(int columnIndex, Reader reader, long length)
        throws SQLException {
    }

    public void updateNCharacterStream(String columnLabel, Reader reader, long length)
        throws SQLException {
    }

    public void updateAsciiStream(int columnIndex, InputStream inputStream, long length)
        throws SQLException {
    }

    public void updateBinaryStream(int columnIndex, InputStream inputStream, long length)
        throws SQLException {
    }

    public void updateCharacterStream(int columnIndex, Reader reader, long length)
        throws SQLException {
    }

    public void updateAsciiStream(String columnLabel, InputStream inputStream, long length)
        throws SQLException {
    }

    public void updateBinaryStream(String columnLabel, InputStream inputStream, long length)
        throws SQLException {
    }

    public void updateCharacterStream(String columnLabel, Reader reader, long length)
        throws SQLException {
    }

    public void updateBlob(int columnIndex, InputStream inputStream, long length)
        throws SQLException {
    }

    public void updateBlob(String columnLabel, InputStream inputStream, long length)
        throws SQLException {
    }

    public void updateClob(int columnIndex, Reader reader, long length)
        throws SQLException {
    }

    public void updateClob(String columnLabel, Reader reader, long length)
        throws SQLException {
    }

    public void updateNClob(int columnIndex, Reader reader, long length)
        throws SQLException {
    }

    public void updateNClob(String columnLabel, Reader reader, long length)
        throws SQLException {
    }

    public void updateNCharacterStream(int columnIndex, Reader reader)
        throws SQLException {
    }

    public void updateNCharacterStream(String columnLabel, Reader reader)
        throws SQLException {
    }

    public void updateAsciiStream(int columnIndex, InputStream inputStream)
        throws SQLException {
    }

    public void updateBinaryStream(int columnIndex, InputStream inputStream)
        throws SQLException {
    }

    public void updateCharacterStream(int columnIndex, Reader reader)
        throws SQLException {
    }

    public void updateAsciiStream(String columnLabel, InputStream inputStream)
        throws SQLException {
    }

    public void updateBinaryStream(String columnLabel, InputStream inputStream)
        throws SQLException {
    }

    public void updateCharacterStream(String columnLabel, Reader reader)
        throws SQLException {
    }

    public void updateBlob(int columnIndex, InputStream inputStream)
        throws SQLException {
    }

    public void updateBlob(String columnLabel, InputStream inputStream)
        throws SQLException {
    }

    public void updateClob(int columnIndex, Reader reader)
        throws SQLException {
    }

    public void updateClob(String columnLabel, Reader reader)
        throws SQLException {
    }

    public void updateNClob(int columnIndex, Reader reader)
        throws SQLException {
    }

    public void updateNClob(String columnLabel, Reader reader)
        throws SQLException {
    }

    public <T> T getObject(int columnIndex,
			   Class<T> type)
    throws SQLException {
	return null;
    }

    public <T> T getObject(String columnLabel,
			   Class<T> type)
	throws SQLException {
	return null;
    }

    public <T> T unwrap(Class<T> iface) {
        return (T)iface;
    }

    public boolean isWrapperFor(Class<?> iface) {
        return false;
    }

}
