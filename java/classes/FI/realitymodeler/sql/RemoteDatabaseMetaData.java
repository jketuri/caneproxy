
package FI.realitymodeler.sql;

import java.rmi.*;
import java.sql.*;

public interface RemoteDatabaseMetaData extends Remote {

    boolean allProceduresAreCallable()
        throws RemoteException, SQLException;

    boolean allTablesAreSelectable()
        throws RemoteException, SQLException;

    String getURL()
        throws RemoteException, SQLException;

    String getUserName()
        throws RemoteException, SQLException;

    boolean isReadOnly()
        throws RemoteException, SQLException;

    boolean nullsAreSortedHigh()
        throws RemoteException, SQLException;

    boolean nullsAreSortedLow()
        throws RemoteException, SQLException;

    boolean nullsAreSortedAtStart()
        throws RemoteException, SQLException;

    boolean nullsAreSortedAtEnd()
        throws RemoteException, SQLException;

    String getDatabaseProductName()
        throws RemoteException, SQLException;

    String getDatabaseProductVersion()
        throws RemoteException, SQLException;

    String getDriverName()
        throws RemoteException, SQLException;

    String getDriverVersion()
        throws RemoteException, SQLException;

    int getDriverMajorVersion()
        throws RemoteException;

    int getDriverMinorVersion()
        throws RemoteException;

    boolean usesLocalFiles()
        throws RemoteException, SQLException;

    boolean usesLocalFilePerTable()
        throws RemoteException, SQLException;

    boolean supportsMixedCaseIdentifiers()
        throws RemoteException, SQLException;

    boolean storesUpperCaseIdentifiers()
        throws RemoteException, SQLException;

    boolean storesLowerCaseIdentifiers()
        throws RemoteException, SQLException;

    boolean storesMixedCaseIdentifiers()
        throws RemoteException, SQLException;

    boolean supportsMixedCaseQuotedIdentifiers()
        throws RemoteException, SQLException;

    boolean storesUpperCaseQuotedIdentifiers()
        throws RemoteException, SQLException;

    boolean storesLowerCaseQuotedIdentifiers()
        throws RemoteException, SQLException;

    boolean storesMixedCaseQuotedIdentifiers()
        throws RemoteException, SQLException;

    String getIdentifierQuoteString()
        throws RemoteException, SQLException;

    String getSQLKeywords()
        throws RemoteException, SQLException;

    String getNumericFunctions()
        throws RemoteException, SQLException;

    String getStringFunctions()
        throws RemoteException, SQLException;

    String getSystemFunctions()
        throws RemoteException, SQLException;

    String getTimeDateFunctions()
        throws RemoteException, SQLException;

    String getSearchStringEscape()
        throws RemoteException, SQLException;

    String getExtraNameCharacters()
        throws RemoteException, SQLException;

    boolean supportsAlterTableWithAddColumn()
        throws RemoteException, SQLException;

    boolean supportsAlterTableWithDropColumn()
        throws RemoteException, SQLException;

    boolean supportsColumnAliasing()
        throws RemoteException, SQLException;

    boolean nullPlusNonNullIsNull()
        throws RemoteException, SQLException;

    boolean supportsConvert()
        throws RemoteException, SQLException;

    boolean supportsConvert(int fromType, int toType)
        throws RemoteException, SQLException;

    boolean supportsTableCorrelationNames()
        throws RemoteException, SQLException;

    boolean supportsDifferentTableCorrelationNames()
        throws RemoteException, SQLException;

    boolean supportsExpressionsInOrderBy()
        throws RemoteException, SQLException;

    boolean supportsOrderByUnrelated()
        throws RemoteException, SQLException;

    boolean supportsGroupBy()
        throws RemoteException, SQLException;

    boolean supportsGroupByUnrelated()
        throws RemoteException, SQLException;

    boolean supportsGroupByBeyondSelect()
        throws RemoteException, SQLException;

    boolean supportsLikeEscapeClause()
        throws RemoteException, SQLException;

    boolean supportsMultipleResultSets()
        throws RemoteException, SQLException;

    boolean supportsMultipleTransactions()
        throws RemoteException, SQLException;

    boolean supportsNonNullableColumns()
        throws RemoteException, SQLException;

    boolean supportsMinimumSQLGrammar()
        throws RemoteException, SQLException;

    boolean supportsCoreSQLGrammar()
        throws RemoteException, SQLException;

    boolean supportsExtendedSQLGrammar()
        throws RemoteException, SQLException;

    boolean supportsANSI92EntryLevelSQL()
        throws RemoteException, SQLException;

    boolean supportsANSI92IntermediateSQL()
        throws RemoteException, SQLException;

    boolean supportsANSI92FullSQL()
        throws RemoteException, SQLException;

    boolean supportsIntegrityEnhancementFacility()
        throws RemoteException, SQLException;

    boolean supportsOuterJoins()
        throws RemoteException, SQLException;

    boolean supportsFullOuterJoins()
        throws RemoteException, SQLException;

    boolean supportsLimitedOuterJoins()
        throws RemoteException, SQLException;

    String getSchemaTerm()
        throws RemoteException, SQLException;

    String getProcedureTerm()
        throws RemoteException, SQLException;

    String getCatalogTerm()
        throws RemoteException, SQLException;

    boolean isCatalogAtStart()
        throws RemoteException, SQLException;

    String getCatalogSeparator()
        throws RemoteException, SQLException;

    boolean supportsSchemasInDataManipulation()
        throws RemoteException, SQLException;

    boolean supportsSchemasInProcedureCalls()
        throws RemoteException, SQLException;

    boolean supportsSchemasInTableDefinitions()
        throws RemoteException, SQLException;

    boolean supportsSchemasInIndexDefinitions()
        throws RemoteException, SQLException;

    boolean supportsSchemasInPrivilegeDefinitions()
        throws RemoteException, SQLException;

    boolean supportsCatalogsInDataManipulation()
        throws RemoteException, SQLException;

    boolean supportsCatalogsInProcedureCalls()
        throws RemoteException, SQLException;

    boolean supportsCatalogsInTableDefinitions()
        throws RemoteException, SQLException;

    boolean supportsCatalogsInIndexDefinitions()
        throws RemoteException, SQLException;

    boolean supportsCatalogsInPrivilegeDefinitions()
        throws RemoteException, SQLException;

    boolean supportsPositionedDelete()
        throws RemoteException, SQLException;

    boolean supportsPositionedUpdate()
        throws RemoteException, SQLException;

    boolean supportsSelectForUpdate()
        throws RemoteException, SQLException;

    boolean supportsStoredProcedures()
        throws RemoteException, SQLException;

    boolean supportsSubqueriesInComparisons()
        throws RemoteException, SQLException;

    boolean supportsSubqueriesInExists()
        throws RemoteException, SQLException;

    boolean supportsSubqueriesInIns()
        throws RemoteException, SQLException;

    boolean supportsSubqueriesInQuantifieds()
        throws RemoteException, SQLException;

    boolean supportsCorrelatedSubqueries()
        throws RemoteException, SQLException;

    boolean supportsUnion()
        throws RemoteException, SQLException;

    boolean supportsUnionAll()
        throws RemoteException, SQLException;

    boolean supportsOpenCursorsAcrossCommit()
        throws RemoteException, SQLException;

    boolean supportsOpenCursorsAcrossRollback()
        throws RemoteException, SQLException;

    boolean supportsOpenStatementsAcrossCommit()
        throws RemoteException, SQLException;

    boolean supportsOpenStatementsAcrossRollback()
        throws RemoteException, SQLException;

    int getMaxBinaryLiteralLength()
        throws RemoteException, SQLException;

    int getMaxCharLiteralLength()
        throws RemoteException, SQLException;

    int getMaxColumnNameLength()
        throws RemoteException, SQLException;

    int getMaxColumnsInGroupBy()
        throws RemoteException, SQLException;

    int getMaxColumnsInIndex()
        throws RemoteException, SQLException;

    int getMaxColumnsInOrderBy()
        throws RemoteException, SQLException;

    int getMaxColumnsInSelect()
        throws RemoteException, SQLException;

    int getMaxColumnsInTable()
        throws RemoteException, SQLException;

    int getMaxConnections()
        throws RemoteException, SQLException;

    int getMaxCursorNameLength()
        throws RemoteException, SQLException;

    int getMaxIndexLength()
        throws RemoteException, SQLException;

    int getMaxSchemaNameLength()
        throws RemoteException, SQLException;

    int getMaxProcedureNameLength()
        throws RemoteException, SQLException;

    int getMaxCatalogNameLength()
        throws RemoteException, SQLException;

    int getMaxRowSize()
        throws RemoteException, SQLException;

    boolean doesMaxRowSizeIncludeBlobs()
        throws RemoteException, SQLException;

    int getMaxStatementLength()
        throws RemoteException, SQLException;

    int getMaxStatements()
        throws RemoteException, SQLException;

    int getMaxTableNameLength()
        throws RemoteException, SQLException;

    int getMaxTablesInSelect()
        throws RemoteException, SQLException;

    int getMaxUserNameLength()
        throws RemoteException, SQLException;

    int getDefaultTransactionIsolation()
        throws RemoteException, SQLException;

    boolean supportsTransactions()
        throws RemoteException, SQLException;

    boolean supportsTransactionIsolationLevel(int level)
        throws RemoteException, SQLException;

    boolean supportsDataDefinitionAndDataManipulationTransactions()
        throws RemoteException, SQLException;

    boolean supportsDataManipulationTransactionsOnly()
        throws RemoteException, SQLException;

    boolean dataDefinitionCausesTransactionCommit()
        throws RemoteException, SQLException;

    boolean dataDefinitionIgnoredInTransactions()
        throws RemoteException, SQLException;

    RemoteResultSet getProcedures(String catalog, String schemaPattern, String procedureNamePattern)
        throws RemoteException, SQLException;

    RemoteResultSet getProcedureColumns(String catalog, String schemaPattern, String procedureNamePattern, String columnNamePattern)
        throws RemoteException, SQLException;

    RemoteResultSet getTables(String catalog, String schemaPattern, String tableNamePattern, String types[])
        throws RemoteException, SQLException;

    RemoteResultSet getSchemas()
        throws RemoteException, SQLException;

    RemoteResultSet getCatalogs()
        throws RemoteException, SQLException;

    RemoteResultSet getTableTypes()
        throws RemoteException, SQLException;

    RemoteResultSet getColumns(String catalog, String schemaPattern, String tableNamePattern, String columnNamePattern)
        throws RemoteException, SQLException;

    RemoteResultSet getColumnPrivileges(String catalog, String schema, String table, String columnNamePattern)
        throws RemoteException, SQLException;

    RemoteResultSet getTablePrivileges(String catalog, String schemaPattern, String tableNamePattern)
        throws RemoteException, SQLException;

    RemoteResultSet getBestRowIdentifier(String catalog, String schema, String table, int scope, boolean nullable)
        throws RemoteException, SQLException;

    RemoteResultSet getVersionColumns(String catalog, String schema, String table)
        throws RemoteException, SQLException;

    RemoteResultSet getPrimaryKeys(String catalog, String schema, String table)
        throws RemoteException, SQLException;

    RemoteResultSet getImportedKeys(String catalog, String schema, String table)
        throws RemoteException, SQLException;

    RemoteResultSet getExportedKeys(String catalog, String schema, String table)
        throws RemoteException, SQLException;

    RemoteResultSet getCrossReference(String primaryCatalog, String primarySchema, String primaryTable, String foreignCatalog, String foreignSchema, String foreignTable)
        throws RemoteException, SQLException;

    RemoteResultSet getTypeInfo()
        throws RemoteException, SQLException;

    RemoteResultSet getIndexInfo(String catalog, String schema, String table, boolean unique, boolean approximate)
        throws RemoteException, SQLException;

    boolean supportsResultSetType(int type)
        throws RemoteException, SQLException;

    boolean supportsResultSetConcurrency(int type, int concurrency)
        throws RemoteException, SQLException;

    boolean ownUpdatesAreVisible(int type)
        throws RemoteException, SQLException;

    boolean ownDeletesAreVisible(int type)
        throws RemoteException, SQLException;

    boolean ownInsertsAreVisible(int type)
        throws RemoteException, SQLException;

    boolean othersUpdatesAreVisible(int type)
        throws RemoteException, SQLException;

    boolean othersDeletesAreVisible(int type)
        throws RemoteException, SQLException;

    boolean othersInsertsAreVisible(int type)
        throws RemoteException, SQLException;

    boolean updatesAreDetected(int type)
        throws RemoteException, SQLException;

    boolean deletesAreDetected(int type)
        throws RemoteException, SQLException;

    boolean insertsAreDetected(int type)
        throws RemoteException, SQLException;

    boolean supportsBatchUpdates()
        throws RemoteException, SQLException;

    RemoteResultSet getUDTs(String catalog, String schemaPattern, String typeNamePattern, int types[])
        throws RemoteException, SQLException;

    RemoteConnection getConnection()
        throws RemoteException, SQLException;

    boolean supportsSavepoints()
        throws RemoteException, SQLException;

    boolean supportsNamedParameters()
        throws RemoteException, SQLException;

    boolean supportsMultipleOpenResults()
        throws RemoteException, SQLException;

    boolean supportsGetGeneratedKeys()
        throws RemoteException, SQLException;

    RemoteResultSet getSuperTypes(String catalog, String schemaPattern, String typeNamePattern)
        throws RemoteException, SQLException;

    RemoteResultSet getSuperTables(String catalog, String schemaPattern, String tableNamePattern)
        throws RemoteException, SQLException;

    RemoteResultSet getAttributes(String catalog, String schemaPattern, String typeNamePattern, String attributeNamePattern)
        throws RemoteException, SQLException;

    boolean supportsResultSetHoldability(int holdability)
        throws RemoteException, SQLException;

    int getResultSetHoldability()
        throws RemoteException, SQLException;

    int getDatabaseMajorVersion()
        throws RemoteException, SQLException;

    int getDatabaseMinorVersion()
        throws RemoteException, SQLException;

    int getJDBCMajorVersion()
        throws RemoteException, SQLException;

    int getJDBCMinorVersion()
        throws RemoteException, SQLException;

    int getSQLStateType()
        throws RemoteException, SQLException;

    boolean locatorsUpdateCopy()
        throws RemoteException, SQLException;

    boolean supportsStatementPooling()
        throws RemoteException, SQLException;

    RowIdLifetime getRowIdLifetime()
        throws RemoteException, SQLException;

    ResultSet getSchemas(String catalog, String schemaPattern)
        throws RemoteException, SQLException;

    boolean supportsStoredFunctionsUsingCallSyntax()
        throws RemoteException, SQLException;

    boolean autoCommitFailureClosesAllResultSets()
        throws RemoteException, SQLException;

    ResultSet getClientInfoProperties()
        throws RemoteException, SQLException;

    ResultSet getFunctions(String catalog, String schemaPattern, String functionNamePattern)
        throws RemoteException, SQLException;

    ResultSet getFunctionColumns(String catalog, String schemaPattern, String functionNamePattern, String columnNamePatterns)
        throws RemoteException, SQLException;

}
