
package FI.realitymodeler.sql;

import java.rmi.*;
import java.rmi.server.*;
import java.sql.*;
import java.util.*;
import java.util.concurrent.*;

public class RemoteConnectionImpl extends UnicastRemoteObject implements RemoteConnection {
    private static final long serialVersionUID = 0L;

    static final String noMethod = "Method not supported";

    private Connection c;

    public RemoteConnectionImpl(Connection c)
        throws RemoteException {
        this.c = c;
    }

    public boolean created()
        throws SQLException {
        if (c instanceof DatabaseConnection) return ((DatabaseConnection)c).created();
        throw new SQLException(noMethod);
    }

    public int tableLength(String tableName)
        throws SQLException {
        if (c instanceof DatabaseConnection) return ((DatabaseConnection)c).tableLength(tableName);
        throw new SQLException(noMethod);
    }

    public RemoteStatement createStatement()
        throws RemoteException, SQLException {
        return new RemoteStatementImpl(c.createStatement());
    }

    public RemoteStatement prepareStatement(String sql)
        throws RemoteException, SQLException {
        return new RemoteStatementImpl(c.prepareStatement(sql));
    }

    public RemoteStatement prepareCall(String sql)
        throws RemoteException, SQLException {
        return new RemoteStatementImpl(c.prepareCall(sql));
    }

    public String nativeSQL(String sql)
        throws SQLException {
        return c.nativeSQL(sql);
    }

    public void setAutoCommit(boolean autoCommit)
        throws SQLException {
        c.setAutoCommit(autoCommit);
    }

    public boolean getAutoCommit()
        throws SQLException {
        return c.getAutoCommit();
    }

    public void commit()
        throws SQLException {
        c.commit();
    }

    public void rollback()
        throws SQLException {
        c.rollback();
    }

    public void close()
        throws SQLException {
        c.close();
    }

    public boolean isClosed()
        throws SQLException {
        return c.isClosed();
    }

    public RemoteDatabaseMetaData getMetaData()
        throws RemoteException, SQLException {
        return new RemoteDatabaseMetaDataImpl(c.getMetaData());
    }

    public void setReadOnly(boolean readOnly)
        throws SQLException {
        c.setReadOnly(readOnly);
    }

    public boolean isReadOnly()
        throws SQLException {
        return c.isReadOnly();
    }

    public void setCatalog(String catalog)
        throws SQLException {
        c.setCatalog(catalog);
    }

    public String getCatalog()
        throws SQLException {
        return c.getCatalog();
    }

    public void setTransactionIsolation(int level)
        throws SQLException {
        c.setTransactionIsolation(level);
    }

    public int getTransactionIsolation()
        throws SQLException {
        return c.getTransactionIsolation();
    }

    public SQLWarning getWarnings()
        throws SQLException {
        return c.getWarnings();
    }

    public void clearWarnings()
        throws SQLException {
        c.clearWarnings();
    }

    public RemoteStatement createStatement(int resultSetType, int resultSetConcurrency)
        throws RemoteException, SQLException {
        return new RemoteStatementImpl(c.createStatement(resultSetType, resultSetConcurrency));
    }

    public RemoteStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency)
        throws RemoteException, SQLException {
        return new RemoteStatementImpl(c.prepareStatement(sql, resultSetType, resultSetConcurrency));
    }

    public RemoteStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency)
        throws RemoteException, SQLException {
        return new RemoteStatementImpl(c.prepareCall(sql, resultSetType, resultSetConcurrency));
    }

    public Map<String, Class<?>> getTypeMap()
        throws SQLException {
        return c.getTypeMap();
    }

    public void setTypeMap(Map<String, Class<?>> map)
        throws SQLException {
        c.setTypeMap(map);
    }

    public void setHoldability(int holdability)
        throws SQLException {
        c.setHoldability(holdability);
    }

    public int getHoldability()
        throws SQLException {
        return c.getHoldability();
    }

    public Savepoint setSavepoint()
        throws SQLException {
        return c.setSavepoint();
    }

    public Savepoint setSavepoint(String name)
        throws SQLException {
        return c.setSavepoint(name);
    }

    public void rollback(Savepoint savepoint)
        throws SQLException {
        c.rollback(savepoint);
    }

    public void releaseSavepoint(Savepoint savepoint)
        throws SQLException {
        c.releaseSavepoint(savepoint);
    }

    public RemoteStatement createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability)
        throws RemoteException, SQLException {
        return new RemoteStatementImpl(c.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability));
    }

    public RemoteStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability)
        throws RemoteException, SQLException {
        return new RemoteStatementImpl(c.prepareStatement(sql, resultSetType, resultSetConcurrency, resultSetHoldability));
    }

    public RemoteStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability)
        throws RemoteException, SQLException {
        return new RemoteStatementImpl(c.prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability));
    }

    public RemoteStatement prepareStatement(String sql, int autoGeneratedKeys)
        throws RemoteException, SQLException {
        return new RemoteStatementImpl(c.prepareStatement(sql, autoGeneratedKeys));
    }

    public RemoteStatement prepareStatement(String sql, int columnIndexes[])
        throws RemoteException, SQLException {
        return new RemoteStatementImpl(c.prepareStatement(sql, columnIndexes));
    }

    public RemoteStatement prepareStatement(String sql, String columnNames[])
        throws RemoteException, SQLException {
        return new RemoteStatementImpl(c.prepareStatement(sql, columnNames));
    }

    public Clob createClob()
        throws SQLException {
        return c.createClob();
    }

    public Blob createBlob()
        throws SQLException {
        return c.createBlob();
    }

    public NClob createNClob()
        throws SQLException {
        return c.createNClob();
    }

    public SQLXML createSQLXML()
        throws SQLException {
        return c.createSQLXML();
    }

    public boolean isValid(int timeout)
        throws SQLException {
        return c.isValid(timeout);
    }

    public void setClientInfo(String name, String value)
        throws SQLClientInfoException {
        c.setClientInfo(name, value);
    }

    public void setClientInfo(Properties properties)
        throws SQLClientInfoException {
        c.setClientInfo(properties);
    }

    public String getClientInfo(String name)
        throws SQLException {
        return c.getClientInfo(name);
    }

    public Properties getClientInfo()
        throws SQLException {
        return c.getClientInfo();
    }

    public Array createArrayOf(String typeName, Object elements[])
        throws SQLException {
        return c.createArrayOf(typeName, elements);
    }

    public Struct createStruct(String typeName, Object elements[])
        throws SQLException {
        return c.createStruct(typeName, elements);
    }

    public void setSchema(String schema)
	throws SQLException {
	c.setSchema(schema);
    }

    public String getSchema()
	throws SQLException {
	return c.getSchema();
    }

    public void abort(Executor executor)
	throws SQLException {
	c.abort(executor);
    }

    public void setNetworkTimeout(Executor executor,
				  int milliseconds)
	throws SQLException {
	c.setNetworkTimeout(executor, milliseconds);
    }

    public int getNetworkTimeout()
	throws SQLException {
	return c.getNetworkTimeout();
    }

}
