
/* DataFile.java */

package FI.realitymodeler.sql;

import java.io.*;
import java.util.*;

/** DataFile is a general purpose random access file,
    where fixed length records can be arbitrarily added and deleted. */
public class DataFile {
    public File file;
    public int firstFree, numberFree, numberOfRecords, recordLength, recordNumber;

    /** Construct a DataFile from given file.
        For example:
        <pre>
        File f = new File("data.file");
        DataFile d = DataFile(file, 100);
        d.open();
        </pre>
        @param file Instance of File class.
        @param recordLength Record length which can be calculated by hand summing up
        lengths of all fields in record. You can get the sum also in this way:
        <pre>
        DataOutputStream o = new DataOutputStream(
        new ByteArrayOutputStream());
        o.writeInt(1);
        o.writeShort(2);
        .
        .
        .
        length = o.size();
        </pre>
        Actual record length will be 4 bytes longer, because in every record must be
        status field, which keeps linked list of deleted records.
        @see java.io.DataOutputStream
        @see java.io.ByteArrayOutputStream */
    public DataFile(File file, int recordLength) {
        this.file = file;
        this.recordLength = recordLength + 4;
    }

    public synchronized RandomAccessFile open()
        throws IOException {
        RandomAccessFile rawFile = new RandomAccessFile(file, "rw");
        try {
            rawFile.seek(0);
            if (rawFile.length() > 0L) {
                firstFree = rawFile.readInt();
                numberFree = rawFile.readInt();
                recordNumber = rawFile.readInt();
                numberOfRecords = (int)((rawFile.length() - 12) / recordLength + 1);
                return rawFile;
            }
            rawFile.writeInt(firstFree = 0);
            rawFile.writeInt(numberFree = 0);
            rawFile.writeInt(recordNumber = 0);
            rawFile.getFD().sync();
            numberOfRecords = 1;
            return rawFile;
        } catch (IOException ex) {
            rawFile.close();
            throw ex;
        }
    }

    protected void updateData(RandomAccessFile rawFile)
        throws IOException {
        rawFile.seek(0);
        rawFile.writeInt(firstFree);
        rawFile.writeInt(numberFree);
        rawFile.writeInt(recordNumber);
        rawFile.getFD().sync();
    }

    /** Seeks file to given record position.
        @param dataRef record number starting from 1. */
    public void seekFile(RandomAccessFile rawFile, int dataRef)
        throws IOException {
        rawFile.seek(12 + (dataRef - 1) * recordLength + 4);
    }

    /** Seeks file to specified offset from given record position.
        @param rawFile file
        @param dataRef record number starting from 1.
        @param offset byte offset starting from 0. */
    public void seekFile(RandomAccessFile rawFile, int dataRef, int offset)
        throws IOException {
        rawFile.seek(12 + (dataRef - 1) * recordLength + 4 + offset);
    }

    /** Seeks file to the status field of given record position.
        @param dataRef record number starting from 1. */
    public void seekStatus(RandomAccessFile rawFile, int dataRef)
        throws IOException {
        rawFile.seek(12 + (dataRef - 1) * recordLength);
    }

    /** Gets data from given record position to buffer.
        This method is useful only for simple sequential data.
        Although individual fields can be obtained with the help
        of InputStreamBuffer and DataInputStream, it is more
        straightforward to use RandomAccessFile superclasses
        methods for reading data. For example:
        <pre>
        d.seekFile(r);
        intField = d.readInt();
        shortField = d.readShort();
        .
        .
        .
        </pre>
        @see RandomAccessFile
        @param rawFile file
        @param dataRef record number starting from 1.
        @param buffer data buffer whose size must be at least record length,
        which was given to constructor. */
    public void getData(RandomAccessFile rawFile, int dataRef, byte buffer[])
        throws IOException {
        seekFile(rawFile, dataRef);
        rawFile.readFully(buffer, 0, recordLength);
    }

    /** Puts data to given record position from buffer.
        This method is useful only for simple sequential data.
        Although individual fields can be changed with the help
        of ByteArrayOutputStream and DataOutputStream, it is more
        straightforward to use RandomAccessFile superclasses
        methods for writing data. For example:
        <pre>
        d.seekFile(r);
        intField = d.writeInt();
        shortField = d.writeShort();
        .
        .
        .
        </pre>
        @see RandomAccessFile
        @param rawFile file
        @param dataRef record number starting from 1.
        @param buffer data buffer whose size must be at least record length,
        which was given to constructor. */
    public void putData(RandomAccessFile rawFile, int dataRef, byte buffer[])
        throws IOException {
        seekFile(rawFile, dataRef);
        rawFile.write(buffer, 0, recordLength);
        rawFile.getFD().sync();
    }

    /** Closes data file. This must always be called after file is not used anymore. */
    public void close(RandomAccessFile rawFile)
        throws IOException {
        rawFile.close();
    }

    /** Returns first free record number which can be used for writing.
        @return Returns new record number. */
    public synchronized int newData(RandomAccessFile rawFile)
        throws IOException {
        int dataRef;
        if (firstFree != 0) {
            dataRef = firstFree;
            seekStatus(rawFile, dataRef);
            firstFree = rawFile.readInt();
            numberFree--;
            updateData(rawFile);
        } else dataRef = numberOfRecords++;
        seekStatus(rawFile, dataRef);
        rawFile.writeInt(0);
        rawFile.getFD().sync();
        return dataRef;
    }

    /** Adds data to first free record position.
        This method is useful only for simple sequential data.
        Although individual fields can be changed with the help
        of ByteArrayOutputStream and DataOutputStream, it is more
        straightforward to use RandomAccessFile superclasses
        methods for writing data. For example:
        <pre>
        d.seekFile(d.newData());
        intField = d.writeInt();
        shortField = d.writeShort();
        .
        .
        .
        </pre>
        @see RandomAccessFile
        @param buffer data buffer whose size must be at least record length,
        which was given to constructor.
        @return Returns new record number. */
    public int addData(RandomAccessFile rawFile, byte buffer[])
        throws IOException {
        int dataRef = newData(rawFile);
        putData(rawFile, dataRef, buffer);
        return dataRef;
    }

    /** Deletes data from given record position marking the record as free.
        Warning: Do not try to delete an already deleted record.
        @param rawFile file
        @param dataRef record number which is previously received from
        addData or newData. */
    public synchronized void deleteData(RandomAccessFile rawFile, int dataRef)
        throws IOException {
        seekStatus(rawFile, dataRef);
        rawFile.writeInt(firstFree);
        firstFree = dataRef;
        numberFree++;
        updateData(rawFile);
    }

    /** Returns length of data file in number of records.
        This is not the same as number of used data records but
        reflects the physical size of the data file.
        @return Returns number of records. */
    public int fileLen() {
        return numberOfRecords;
    }

    /** Returns number of used records in data file.
        @return Returns number of used records. */
    public int usedRecs() {
        return numberOfRecords - numberFree - 1;
    }

}
