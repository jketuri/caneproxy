
package FI.realitymodeler.sql;

import FI.realitymodeler.common.*;
import java.io.*;
import java.rmi.*;

public class TransferStream implements Externalizable {
    private static final long serialVersionUID = 0L;

    public static LocalStream ls = null;
    public static W3Lock lock = new W3Lock();

    public W3Lock lock1;

    private RemoteStream rs;
    private InputStream in;
    private int l;

    public TransferStream(RemoteStream rs, InputStream in, int l) {
        this.rs = rs;
        this.in = in;
        this.l = l;
    }

    public TransferStream(RemoteStream rs, InputStream in) {
        this(rs, in, -1);
    }

    public TransferStream(InputStream in) {
        this(null, in, -1);
    }

    public TransferStream() {
        this(null, null, 0);
    }

    public void writeExternal(ObjectOutput out)
        throws IOException {
        if (rs != null) out.writeObject(rs);
        int n;
        byte b[] = new byte[Support.bufferLength];
        if (l != -1)
            while (l > 0 && (n = in.read(b,0,Math.min(b.length,l))) > 0) {
                out.writeInt(n);
                out.flush();
                out.write(b,0,n);
                out.flush();
                l -= n;
            }
        else
            while ((n = in.read(b)) > 0) {
                out.writeInt(n);
                out.flush();
                out.write(b,0,n);
                out.flush();
            }
        out.writeInt(0);
        out.flush();
        in.close();
    }

    public void readExternal(ObjectInput in)
        throws IOException {
        lock1 = new W3Lock(true);
        if (ls == null)
            try {
                ls = RemoteStreamImpl.getRemoteStream((RemoteStream)in.readObject()).getLocalStream();
            } catch (ClassNotFoundException ex) {
                throw new IOException(ex.toString());
            }
        ls.in = in;
        ls.ts = this;
        ls.lock.release();
        lock.release();
        try {
            lock1.lock();
        } catch (InterruptedException ex) {}
    }

}
