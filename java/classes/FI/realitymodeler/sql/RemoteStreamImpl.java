
package FI.realitymodeler.sql;

import java.rmi.*;
import java.rmi.server.*;

public class RemoteStreamImpl extends UnicastRemoteObject implements RemoteStream {
    private static final long serialVersionUID = 0L;
    private static RemoteStreamImpl instance;

    private LocalStream ls;

    public RemoteStreamImpl()
        throws RemoteException {
        ls = new LocalStream();
    }

    public static synchronized RemoteStreamImpl getRemoteStream(RemoteStream remoteStream)
        throws RemoteException {
        instance = null;
        remoteStream.setInstance();
        return instance;
    }

    public void setInstance() {
        instance = this;
    }

    public LocalStream getLocalStream() {
        return ls;
    }

}
