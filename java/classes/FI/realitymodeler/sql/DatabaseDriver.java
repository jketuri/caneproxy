
package FI.realitymodeler.sql;

import FI.realitymodeler.common.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.logging.*;
import javax.servlet.*;

/** Delivers local connections to internal java based database. Can be
    initialized as servlet in host machine registering it to DriverManager.
*/

public class DatabaseDriver extends GenericServlet implements Driver {
    private static final long serialVersionUID = 0L;

    static {
        try {
            DriverManager.registerDriver(new DatabaseDriver());
        } catch (Exception ex) {};
    }

    public static void prepare() {
    }

    public void service(ServletRequest req, ServletResponse res) {
    }

    public boolean check(String url, int x[]) {
        int i, j;
        if ((i = url.indexOf(':')) == -1 || i + 1 >= url.length() || (j = url.indexOf(':', i + 1)) == -1 ||
            !url.regionMatches(true, i + 1, "FI.realitymodeler", 0, j - i - 1)) return false;
        if (x != null) {
            x[0] = j + 1 < url.length() ? url.indexOf('#', j + 1) : -1;
            x[1] = j;
        }
        return true;
    }

    /** Gets connection to database with url like jdbc:FI.realitymodeler:database[#readOnly] */
    public Connection connect(String url, Properties info)
        throws SQLException {
        int x[] = new int[2];
        if (!check(url, x)) return null;
        try {
            return new DatabaseConnection(url.substring(x[1] + 1, x[0] != -1 ? x[0] : url.length()), x[0] != -1);
        } catch (Exception ex) {
            PrintWriter pw = DriverManager.getLogWriter();
            if (pw != null) ex.printStackTrace(pw);
            throw new SQLException(Support.stackTrace(ex));
        }
    }

    public boolean acceptsURL(String url) {
        return check(url, null);
    }

    public DriverPropertyInfo[] getPropertyInfo(String url, Properties info) {
        return null;
    }

    public int getMajorVersion() {
        return 1;
    }

    public int getMinorVersion() {
        return 0;
    }

    public boolean jdbcCompliant() {
        return false;
    }

    public Logger getParentLogger()
	throws SQLFeatureNotSupportedException {
	throw new SQLFeatureNotSupportedException();
    }

}
