
package FI.realitymodeler.sql;

import java.io.*;
import java.math.*;
import java.net.*;
import java.rmi.*;
import java.sql.*;
import java.util.*;

public interface RemoteStatement extends Remote {

    RemoteResultSet executeQuery(String sql)
        throws RemoteException, SQLException;

    int executeUpdate(String sql)
        throws RemoteException, SQLException;

    void close()
        throws RemoteException, SQLException;

    int getMaxFieldSize()
        throws RemoteException, SQLException;

    void setMaxFieldSize(int max)
        throws RemoteException, SQLException;

    int getMaxRows()
        throws RemoteException, SQLException;

    void setMaxRows(int max)
        throws RemoteException, SQLException;

    void setEscapeProcessing(boolean enable)
        throws RemoteException, SQLException;

    int getQueryTimeout()
        throws RemoteException, SQLException;

    void setQueryTimeout(int seconds)
        throws RemoteException, SQLException;

    void cancel()
        throws RemoteException, SQLException;

    SQLWarning getWarnings()
        throws RemoteException, SQLException;

    void clearWarnings()
        throws RemoteException, SQLException;

    void setCursorName(String name)
        throws RemoteException, SQLException;

    boolean execute(String sql)
        throws RemoteException, SQLException;

    RemoteResultSet getResultSet()
        throws RemoteException, SQLException; 

    int getUpdateCount()
        throws RemoteException, SQLException;

    boolean getMoreResults()
        throws RemoteException, SQLException;

    void setFetchDirection(int direction)
        throws RemoteException, SQLException;

    int getFetchDirection()
        throws RemoteException, SQLException;

    void setFetchSize(int rows)
        throws RemoteException, SQLException;

    int getFetchSize()
        throws RemoteException, SQLException;

    int getResultSetConcurrency()
        throws RemoteException, SQLException;

    int getResultSetType()
        throws RemoteException, SQLException;

    void addBatch(String sql)
        throws RemoteException, SQLException;

    void clearBatch()
        throws RemoteException, SQLException;

    int[] executeBatch()
        throws RemoteException, SQLException;

    RemoteConnection getConnection()
        throws RemoteException, SQLException;

    boolean getMoreResults(int current)
        throws RemoteException, SQLException;

    RemoteResultSet getGeneratedKeys()
        throws RemoteException, SQLException;

    int executeUpdate(String sql, int autoGeneratedKeys)
        throws RemoteException, SQLException;

    int executeUpdate(String sql, int columnIndexes[])
        throws RemoteException, SQLException;

    int executeUpdate(String sql, String columnNames[])
        throws RemoteException, SQLException;

    boolean execute(String sql, int autoGeneratedKeys)
        throws RemoteException, SQLException;

    boolean execute(String sql, int columnIndexes[])
        throws RemoteException, SQLException;

    boolean execute(String sql, String columnNames[])
        throws RemoteException, SQLException;

    int getResultSetHoldability()
        throws RemoteException, SQLException;

    boolean isClosed()
        throws RemoteException, SQLException;

    void setPoolable(boolean poolable)
        throws RemoteException, SQLException;

    boolean isPoolable()
        throws RemoteException, SQLException;

    void closeOnCompletion()
        throws RemoteException, SQLException;

    boolean isCloseOnCompletion()
        throws RemoteException, SQLException;

    RemoteResultSet executeQuery()
        throws RemoteException, SQLException;

    int executeUpdate()
        throws RemoteException, SQLException;

    void setNull(int parameterIndex, int sqlType)
        throws RemoteException, SQLException;

    void setBoolean(int parameterIndex, boolean x)
        throws RemoteException, SQLException;

    void setByte(int parameterIndex, byte x)
        throws RemoteException, SQLException;

    void setShort(int parameterIndex, short x)
        throws RemoteException, SQLException;

    void setInt(int parameterIndex, int x)
        throws RemoteException, SQLException;

    void setLong(int parameterIndex, long x)
        throws RemoteException, SQLException;

    void setFloat(int parameterIndex, float x)
        throws RemoteException, SQLException;

    void setDouble(int parameterIndex, double x)
        throws RemoteException, SQLException;

    void setBigDecimal(int parameterIndex, BigDecimal x)
        throws RemoteException, SQLException;

    void setString(int parameterIndex, String x)
        throws RemoteException, SQLException;

    void setBytes(int parameterIndex, byte x[])
        throws RemoteException, SQLException;

    void setDate(int parameterIndex, java.sql.Date x)
        throws RemoteException, SQLException;

    void setTime(int parameterIndex, java.sql.Time x)  throws RemoteException, SQLException;

    void setTimestamp(int parameterIndex, java.sql.Timestamp x)
        throws RemoteException, SQLException;

    RemoteStream newRemoteStream()
        throws RemoteException, InterruptedException;

    void setStream(TransferStream transferStream)
        throws RemoteException;

    void setRemoteStream(int parameterIndex, int length, boolean ascii, RemoteStream remoteStream)
        throws RemoteException, SQLException;

    void clearParameters()
        throws RemoteException, SQLException;

    void setObject(int parameterIndex, Object x, int targetSqlType, int scaleOrLength)
        throws RemoteException, SQLException;

    void setObject(int parameterIndex, Object x, int targetSqlType)
        throws RemoteException, SQLException;

    void setObject(int parameterIndex, Object x)
        throws RemoteException, SQLException;

    boolean execute()
        throws RemoteException, SQLException;

    void addBatch()
        throws RemoteException, SQLException;

    void setCharacterStream(int parameterIndex, Reader reader, int length)
        throws RemoteException, SQLException;

    void setRef(int i, Ref x)
        throws RemoteException, SQLException;

    void setBlob(int i, Blob x)
        throws RemoteException, SQLException;

    void setClob(int i, Clob x)
        throws RemoteException, SQLException;

    void setArray(int i, Array x)
        throws RemoteException, SQLException;

    RemoteResultSetMetaData getMetaData()
        throws RemoteException, SQLException;

    void setDate(int parameterIndex, java.sql.Date x, Calendar cal)
        throws RemoteException, SQLException;

    void setTime(int parameterIndex, java.sql.Time x, Calendar cal)
        throws RemoteException, SQLException;

    void setTimestamp(int parameterIndex, Timestamp x, Calendar cal)
        throws RemoteException, SQLException;

    void setNull(int parameterIndex, int sqlType, String typeName)
        throws RemoteException, SQLException;

    void setURL(int parameterIndex, URL x)
        throws RemoteException, SQLException;

    ParameterMetaData getParameterMetaData()
        throws RemoteException, SQLException;

    void setRowId(int parameterIndex, RowId rowId)
        throws RemoteException, SQLException;

    void setNString(int parameterIndex, String value)
        throws RemoteException, SQLException;

    void setNCharacterStream(int parameterIndex, Reader value, long length)
        throws RemoteException, SQLException;

    void setNClob(int parameterIndex, NClob value)
        throws RemoteException, SQLException;

    void setClob(int parameterIndex, Reader reader, long length)
        throws RemoteException, SQLException;

    void setBlob(int parameterIndex, InputStream inputStream, long length)
        throws RemoteException, SQLException;

    void setNClob(int parameterIndex, Reader reader, long length)
        throws RemoteException, SQLException;

    void setSQLXML(int parameterIndex, SQLXML sqlXml)
        throws RemoteException, SQLException;

    void setAsciiStream(int parameterIndex, InputStream inputStream, long length)
        throws RemoteException, SQLException;

    void setBinaryStream(int parameterIndex, InputStream inputStream, long length)
        throws RemoteException, SQLException;

    void setCharacterStream(int parameterIndex, Reader reader, long length)
        throws RemoteException, SQLException;

    void setAsciiStream(int parameterIndex, InputStream inputStream)
        throws RemoteException, SQLException;

    void setBinaryStream(int parameterIndex, InputStream inputStream)
        throws RemoteException, SQLException;

    void setCharacterStream(int parameterIndex, Reader reader)
        throws RemoteException, SQLException;

    void setNCharacterStream(int parameterIndex, Reader reader)
        throws RemoteException, SQLException;

    void setClob(int parameterIndex, Reader reader)
        throws RemoteException, SQLException;

    void setBlob(int parameterIndex, InputStream inputStream)
        throws RemoteException, SQLException;

    void setNClob(int parameterIndex, Reader reader)
        throws RemoteException, SQLException;

    <T> T getObject(int parameterIndex,
		    Class<T> type)
        throws RemoteException, SQLException;
    
    <T> T getObject(String parameterName,
		    Class<T> type)
        throws RemoteException, SQLException;

    void registerOutParameter(int parameterIndex, int sqlType)
        throws RemoteException, SQLException;

    void registerOutParameter(int parameterIndex, int sqlType, int scale)
        throws RemoteException, SQLException;

    boolean wasNull()
        throws RemoteException, SQLException;

    String getString(int parameterIndex)
        throws RemoteException, SQLException;

    boolean getBoolean(int parameterIndex)
        throws RemoteException, SQLException;

    byte getByte(int parameterIndex)
        throws RemoteException, SQLException;

    short getShort(int parameterIndex)
        throws RemoteException, SQLException;

    int getInt(int parameterIndex)
        throws RemoteException, SQLException;

    long getLong(int parameterIndex)
        throws RemoteException, SQLException;

    float getFloat(int parameterIndex)
        throws RemoteException, SQLException;

    double getDouble(int parameterIndex)
        throws RemoteException, SQLException;

    BigDecimal getBigDecimal(int parameterIndex, int scale)
        throws RemoteException, SQLException;

    byte[] getBytes(int parameterIndex)
        throws RemoteException, SQLException;

    java.sql.Date getDate(int parameterIndex)
        throws RemoteException, SQLException;

    java.sql.Time getTime(int parameterIndex)
        throws RemoteException, SQLException;

    java.sql.Timestamp getTimestamp(int parameterIndex)
        throws RemoteException, SQLException;

    Object getObject(int parameterIndex)
        throws RemoteException, SQLException;

    BigDecimal getBigDecimal(int parameterIndex)
        throws RemoteException, SQLException;

    Object getObject(int i, Map<String, Class<?>> map)
        throws RemoteException, SQLException;

    Ref getRef(int i)
        throws RemoteException, SQLException;

    Blob getBlob(int i)
        throws RemoteException, SQLException;

    Clob getClob(int i)
        throws RemoteException, SQLException;

    Array getArray(int i)
        throws RemoteException, SQLException;

    java.sql.Date getDate(int parameterIndex, Calendar cal)
        throws RemoteException, SQLException;

    java.sql.Time getTime(int parameterIndex, Calendar cal)
        throws RemoteException, SQLException;

    Timestamp getTimestamp(int parameterIndex, Calendar cal)
        throws RemoteException, SQLException;

    void registerOutParameter(int parameterIndex, int sqlType, String typeName)
        throws RemoteException, SQLException;

    void registerOutParameter(String parameterName, int sqlType)
        throws RemoteException, SQLException;

    void registerOutParameter(String parameterName, int sqlType, int scale)
        throws RemoteException, SQLException;

    void registerOutParameter(String parameterName, int sqlType, String typeName)
        throws RemoteException, SQLException;

    URL getURL(int parameterIndex)
        throws RemoteException, SQLException;

    void setURL(String parameterName, URL val)
        throws RemoteException, SQLException;

    void setNull(String parameterName, int sqlType)
        throws RemoteException, SQLException;

    void setBoolean(String parameterName, boolean x)
        throws RemoteException, SQLException;

    void setByte(String parameterName, byte x)
        throws RemoteException, SQLException;

    void setShort(String parameterName, short x)
        throws RemoteException, SQLException;

    void setInt(String parameterName, int x)
        throws RemoteException, SQLException;

    void setLong(String parameterName, long x)
        throws RemoteException, SQLException;

    void setFloat(String parameterName, float x)
        throws RemoteException, SQLException;

    void setDouble(String parameterName, double x)
        throws RemoteException, SQLException;

    void setBigDecimal(String parameterName, BigDecimal x)
        throws RemoteException, SQLException;

    void setString(String parameterName, String x)
        throws RemoteException, SQLException;

    void setBytes(String parameterName, byte x[])
        throws RemoteException, SQLException;

    void setDate(String parameterName, java.sql.Date x)
        throws RemoteException, SQLException;

    void setTime(String parameterName, Time x)
        throws RemoteException, SQLException;

    void setTimestamp(String parameterName, Timestamp x)
        throws RemoteException, SQLException;

    void setAsciiStream(String parameterName, InputStream x, int length)
        throws RemoteException, SQLException;

    void setBinaryStream(String parameterName, InputStream x, int length)
        throws RemoteException, SQLException;

    void setObject(String parameterName, Object x, int targetSqlType, int scale)
        throws RemoteException, SQLException;

    void setObject(String parameterName, Object x, int targetSqlType)
        throws RemoteException, SQLException;

    void setObject(String parameterName, Object x)
        throws RemoteException, SQLException;

    void setCharacterStream(String parameterName, Reader reader, int length)
        throws RemoteException, SQLException;

    void setDate(String parameterName, java.sql.Date x, Calendar cal)
        throws RemoteException, SQLException;

    void setTime(String parameterName, Time x, Calendar cal)
        throws RemoteException, SQLException;

    void setTimestamp(String parameterName, Timestamp x, Calendar cal)
        throws RemoteException, SQLException;

    void setNull(String parameterName, int sqlType, String typeName)
        throws RemoteException, SQLException;

    String getString(String parameterName)
        throws RemoteException, SQLException;

    boolean getBoolean(String parameterName)
        throws RemoteException, SQLException;

    byte getByte(String parameterName)
        throws RemoteException, SQLException;

    short getShort(String parameterName)
        throws RemoteException, SQLException;

    int getInt(String parameterName)
        throws RemoteException, SQLException;

    long getLong(String parameterName)
        throws RemoteException, SQLException;

    float getFloat(String parameterName)
        throws RemoteException, SQLException;

    double getDouble(String parameterName)
        throws RemoteException, SQLException;

    byte[] getBytes(String parameterName)
        throws RemoteException, SQLException;

    java.sql.Date getDate(String parameterName)
        throws RemoteException, SQLException;

    Time getTime(String parameterName)
        throws RemoteException, SQLException;

    Timestamp getTimestamp(String parameterName)
        throws RemoteException, SQLException;

    Object getObject(String parameterName)
        throws RemoteException, SQLException;

    BigDecimal getBigDecimal(String parameterName)
        throws RemoteException, SQLException;

    Object getObject(String parameterName, Map<String, Class<?>> map)
        throws RemoteException, SQLException;

    Ref getRef(String parameterName)
        throws RemoteException, SQLException;

    Blob getBlob(String parameterName)
        throws RemoteException, SQLException;

    Clob getClob(String parameterName)
        throws RemoteException, SQLException;

    Array getArray(String parameterName)
        throws RemoteException, SQLException;

    java.sql.Date getDate(String parameterName, Calendar cal)
        throws RemoteException, SQLException;

    Time getTime(String parameterName, Calendar cal)
        throws RemoteException, SQLException;

    Timestamp getTimestamp(String parameterName, Calendar cal)
        throws RemoteException, SQLException;

    URL getURL(String parameterName)
        throws RemoteException, SQLException;

    RowId getRowId(int parameterIndex)
        throws RemoteException, SQLException;

    RowId getRowId(String parameterName)
        throws RemoteException, SQLException;

    void setRowId(String parameterName, RowId rowId)
        throws RemoteException, SQLException;

    void setNString(String parameterName, String value)
        throws RemoteException, SQLException;

    void setNCharacterStream(String parameterName, Reader reader, long length)
        throws RemoteException, SQLException;

    void setNClob(String parameterName, NClob nclob)
        throws RemoteException, SQLException;

    void setClob(String parameterName, Reader reader, long length)
        throws RemoteException, SQLException;

    void setBlob(String parameterName, InputStream inputStream, long length)
        throws RemoteException, SQLException;

    void setNClob(String parameterName, Reader reader, long length)
        throws RemoteException, SQLException;

    NClob getNClob(int parameterIndex)
        throws RemoteException, SQLException;

    NClob getNClob(String parameterName)
        throws RemoteException, SQLException;

    void setSQLXML(String parameterName, SQLXML sqlXml)
        throws RemoteException, SQLException;

    SQLXML getSQLXML(int parameterIndex)
        throws RemoteException, SQLException;

    SQLXML getSQLXML(String parameterName)
        throws RemoteException, SQLException;

    String getNString(int parameterIndex)
        throws RemoteException, SQLException;

    String getNString(String parameterName)
        throws RemoteException, SQLException;

    Reader getNCharacterStream(int parameterIndex)
        throws RemoteException, SQLException;

    Reader getNCharacterStream(String parameterName)
        throws RemoteException, SQLException;

    Reader getCharacterStream(int parameterIndex)
        throws RemoteException, SQLException;

    Reader getCharacterStream(String parameterName)
        throws RemoteException, SQLException;

    void setBlob(String parameterName, Blob blob)
        throws RemoteException, SQLException;

    void setClob(String parameterName, Clob clob)
        throws RemoteException, SQLException;

    void setAsciiStream(String parameterName, InputStream inputStream, long length)
        throws RemoteException, SQLException;

    void setBinaryStream(String parameterName, InputStream inputStream, long length)
        throws RemoteException, SQLException;

    void setCharacterStream(String parameterName, Reader reader, long length)
        throws RemoteException, SQLException;

    void setAsciiStream(String parameterName, InputStream inputStream)
        throws RemoteException, SQLException;

    void setBinaryStream(String parameterName, InputStream inputStream)
        throws RemoteException, SQLException;

    void setCharacterStream(String parameterName, Reader reader)
        throws RemoteException, SQLException;

    void setNCharacterStream(String parameterName, Reader reader)
        throws RemoteException, SQLException;

    void setClob(String parameterName, Reader reader)
        throws RemoteException, SQLException;

    void setBlob(String parameterName, InputStream inputStream)
        throws RemoteException, SQLException;

    void setNClob(String parameterName, Reader reader)
        throws RemoteException, SQLException;

}
