
package FI.realitymodeler.sql;

import java.io.*;
import java.math.*;
import java.net.*;
import java.rmi.*;
import java.sql.*;
import java.util.*;

interface RemoteResultSet extends Remote {

    boolean next()
        throws RemoteException, SQLException;

    void close()
        throws RemoteException, SQLException;

    boolean wasNull()
        throws RemoteException, SQLException;

    String getString(int columnIndex)
        throws RemoteException, SQLException;

    boolean getBoolean(int columnIndex)
        throws RemoteException, SQLException;

    byte getByte(int columnIndex)
        throws RemoteException, SQLException;

    short getShort(int columnIndex)
        throws RemoteException, SQLException;

    int getInt(int columnIndex)
        throws RemoteException, SQLException;

    long getLong(int columnIndex)
        throws RemoteException, SQLException;

    float getFloat(int columnIndex)
        throws RemoteException, SQLException;

    double getDouble(int columnIndex)
        throws RemoteException, SQLException;

    BigDecimal getBigDecimal(int columnIndex, int scale)
        throws RemoteException, SQLException;

    byte[] getBytes(int columnIndex)
        throws RemoteException, SQLException;

    java.sql.Date getDate(int columnIndex)
        throws RemoteException, SQLException;

    java.sql.Time getTime(int columnIndex)
        throws RemoteException, SQLException;

    java.sql.Timestamp getTimestamp(int columnIndex)
        throws RemoteException, SQLException;

    URL getURL(int columnIndex)
        throws RemoteException, SQLException;

    TransferStream getStream(int columnIndex, boolean ascii)
        throws RemoteException, SQLException;

    String getString(String columnName)
        throws RemoteException, SQLException;

    boolean getBoolean(String columnName)
        throws RemoteException, SQLException;

    byte getByte(String columnName)
        throws RemoteException, SQLException;

    short getShort(String columnName)
        throws RemoteException, SQLException;

    int getInt(String columnName)
        throws RemoteException, SQLException;

    long getLong(String columnName)
        throws RemoteException, SQLException;

    float getFloat(String columnName)
        throws RemoteException, SQLException;

    double getDouble(String columnName)
        throws RemoteException, SQLException;

    BigDecimal getBigDecimal(String columnName, int scale)
        throws RemoteException, SQLException;

    byte[] getBytes(String columnName)
        throws RemoteException, SQLException;

    java.sql.Date getDate(String columnName)
        throws RemoteException, SQLException;

    java.sql.Time getTime(String columnName)
        throws RemoteException, SQLException;

    java.sql.Timestamp getTimestamp(String columnName)
        throws RemoteException, SQLException;

    URL getURL(String columnName)
        throws RemoteException, SQLException;

    SQLWarning getWarnings()
        throws RemoteException, SQLException;

    void clearWarnings()
        throws RemoteException, SQLException;

    String getCursorName()
        throws RemoteException, SQLException;

    RemoteResultSetMetaData getMetaData()
        throws RemoteException, SQLException;

    Object getObject(int columnIndex)
        throws RemoteException, SQLException;

    Object getObject(String columnName)
        throws RemoteException, SQLException;

    int findColumn(String columnName)
        throws RemoteException, SQLException;

    Reader getCharacterStream(int columnIndex)
        throws RemoteException, SQLException;

    Reader getCharacterStream(String columnName)
        throws RemoteException, SQLException;

    BigDecimal getBigDecimal(int columnIndex)
        throws RemoteException, SQLException;

    BigDecimal getBigDecimal(String columnName)
        throws RemoteException, SQLException;

    boolean isBeforeFirst()
        throws RemoteException, SQLException;

    boolean isAfterLast()
        throws RemoteException, SQLException;

    boolean isFirst()
        throws RemoteException, SQLException;

    boolean isLast()
        throws RemoteException, SQLException;

    void beforeFirst()
        throws RemoteException, SQLException;

    void afterLast()
        throws RemoteException, SQLException;

    boolean first()
        throws RemoteException, SQLException;

    boolean last()
        throws RemoteException, SQLException;

    int getRow()
        throws RemoteException, SQLException;

    boolean absolute(int row)
        throws RemoteException, SQLException;

    boolean relative(int rows)
        throws RemoteException, SQLException;

    boolean previous()
        throws RemoteException, SQLException;

    void setFetchDirection(int direction)
        throws RemoteException, SQLException;

    int getFetchDirection()
        throws RemoteException, SQLException;

    void setFetchSize(int rows)
        throws RemoteException, SQLException;

    int getFetchSize()
        throws RemoteException, SQLException;

    int getType()
        throws RemoteException, SQLException;

    int getConcurrency()
        throws RemoteException, SQLException;

    boolean rowUpdated()
        throws RemoteException, SQLException;

    boolean rowInserted()
        throws RemoteException, SQLException;

    boolean rowDeleted()
        throws RemoteException, SQLException;

    void updateNull(int columnIndex)
        throws RemoteException, SQLException;

    void updateBoolean(int columnIndex, boolean x)
        throws RemoteException, SQLException;

    void updateByte(int columnIndex, byte x)
        throws RemoteException, SQLException;

    void updateShort(int columnIndex, short x)
        throws RemoteException, SQLException;

    void updateInt(int columnIndex, int x)
        throws RemoteException, SQLException;

    void updateLong(int columnIndex, long x)
        throws RemoteException, SQLException;

    void updateFloat(int columnIndex, float x)
        throws RemoteException, SQLException;

    void updateDouble(int columnIndex, double x)
        throws RemoteException, SQLException;

    void updateBigDecimal(int colunmnIndex, BigDecimal x)
        throws RemoteException, SQLException;

    void updateString(int columnIndex, String x)
        throws RemoteException, SQLException;

    void updateBytes(int columnIndex, byte x[])
        throws RemoteException, SQLException;

    void updateDate(int columnIndex, java.sql.Date x)
        throws RemoteException, SQLException;

    void updateTime(int columnIndex, java.sql.Time x)
        throws RemoteException, SQLException;

    void updateTimestamp(int columnIndex, Timestamp x)
        throws RemoteException, SQLException;

    void updateAsciiStream(int columnIndex, java.io.InputStream x, int length)
        throws RemoteException, SQLException;

    void updateBinaryStream(int columnIndex, java.io.InputStream x, int length)
        throws RemoteException, SQLException;

    void updateCharacterStream(int columnIndex, Reader reader, int length)
        throws RemoteException, SQLException;

    void updateObject(int columnIndex, Object x, int scale)
        throws RemoteException, SQLException;

    void updateObject(int columnIndex, Object x)
        throws RemoteException, SQLException;

    void updateRef(int columnIndex, Ref x)
        throws RemoteException, SQLException;

    void updateBlob(int columnIndex, Blob x)
        throws RemoteException, SQLException;

    void updateClob(int columnIndex, Clob x)
        throws RemoteException, SQLException;

    void updateArray(int columnIndex, Array x)
        throws RemoteException, SQLException;

    void updateNull(String columnName)
        throws RemoteException, SQLException;

    void updateBoolean(String columnName, boolean x)
        throws RemoteException, SQLException;

    void updateByte(String columnName, byte x)
        throws RemoteException, SQLException;

    void updateShort(String columnName, short x)
        throws RemoteException, SQLException;

    void updateInt(String columnName, int x)
        throws RemoteException, SQLException;

    void updateLong(String columnName, long x)
        throws RemoteException, SQLException;

    void updateFloat(String columnName, float x)
        throws RemoteException, SQLException;

    void updateDouble(String columnName, double x)
        throws RemoteException, SQLException;

    void updateBigDecimal(String colunmnName, BigDecimal x)
        throws RemoteException, SQLException;

    void updateString(String columnName, String x)
        throws RemoteException, SQLException;

    void updateBytes(String columnName, byte x[])
        throws RemoteException, SQLException;

    void updateDate(String columnName, java.sql.Date x)
        throws RemoteException, SQLException;

    void updateTime(String columnName, java.sql.Time x)
        throws RemoteException, SQLException;

    void updateTimestamp(String columnName, Timestamp x)
        throws RemoteException, SQLException;

    void updateAsciiStream(String columnName, java.io.InputStream x, int length)
        throws RemoteException, SQLException;

    void updateBinaryStream(String columnName, java.io.InputStream x, int length)
        throws RemoteException, SQLException;

    void updateCharacterStream(String columnName, Reader reader, int length)
        throws RemoteException, SQLException;

    void updateObject(String columnName, Object x, int scale)
        throws RemoteException, SQLException;

    void updateObject(String columnName, Object x)
        throws RemoteException, SQLException;

    void updateRef(String columnName, Ref x)
        throws RemoteException, SQLException;

    void updateBlob(String columnName, Blob x)
        throws RemoteException, SQLException;

    void updateClob(String columnName, Clob x)
        throws RemoteException, SQLException;

    void updateArray(String columnName, Array x)
        throws RemoteException, SQLException;

    void insertRow()
        throws RemoteException, SQLException;

    void updateRow()
        throws RemoteException, SQLException;

    void deleteRow()
        throws RemoteException, SQLException;

    void refreshRow()
        throws RemoteException, SQLException;

    void cancelRowUpdates()
        throws RemoteException, SQLException;

    void moveToInsertRow()
        throws RemoteException, SQLException;

    void moveToCurrentRow()
        throws RemoteException, SQLException;

    Object getObject(int i, Map<String, Class<?>> map)
        throws RemoteException, SQLException;

    Ref getRef(int i)
        throws RemoteException, SQLException;

    Blob getBlob(int i)
        throws RemoteException, SQLException;

    Clob getClob(int i)
        throws RemoteException, SQLException;

    Array getArray(int i)
        throws RemoteException, SQLException;

    java.sql.Date getDate(int i, Calendar cal)
        throws RemoteException, SQLException;

    java.sql.Date getDate(String columnName, Calendar cal)
        throws RemoteException, SQLException;

    java.sql.Time getTime(int i, Calendar cal)
        throws RemoteException, SQLException;

    java.sql.Time getTime(String columnName, Calendar cal)
        throws RemoteException, SQLException;

    Timestamp getTimestamp(int i, Calendar cal)
        throws RemoteException, SQLException;

    Timestamp getTimestamp(String columnName, Calendar cal)
        throws RemoteException, SQLException;

    Statement getStatement()
        throws RemoteException, SQLException;

    Object getObject(String columnName, Map<String, Class<?>> map)
        throws RemoteException, SQLException;

    Ref getRef(String columnName)
        throws RemoteException, SQLException;

    Blob getBlob(String columnName)
        throws RemoteException, SQLException;

    Clob getClob(String columnName)
        throws RemoteException, SQLException;

    Array getArray(String columnName)
        throws RemoteException, SQLException;

    RowId getRowId(int columnIndex)
        throws RemoteException, SQLException;

    RowId getRowId(String columnLabel)
        throws RemoteException, SQLException;

    void updateRowId(int columnIndex, RowId rowId)
        throws RemoteException, SQLException;

    void updateRowId(String columnLabel, RowId rowId)
        throws RemoteException, SQLException;

    int getHoldability()
        throws RemoteException, SQLException;

    boolean isClosed()
        throws RemoteException, SQLException;

    void updateNString(int columnIndex, String nString)
        throws RemoteException, SQLException;

    void updateNString(String columnLabel, String nString)
        throws RemoteException, SQLException;

    void updateNClob(int columnIndex, NClob nClob)
        throws RemoteException, SQLException;

    void updateNClob(String columnLabel, NClob nClob)
        throws RemoteException, SQLException;

    NClob getNClob(int columnIndex)
        throws RemoteException, SQLException;

    NClob getNClob(String columnLabel)
        throws RemoteException, SQLException;

    SQLXML getSQLXML(int columnIndex)
        throws RemoteException, SQLException;

    SQLXML getSQLXML(String columnLabel)
        throws RemoteException, SQLException;

    void updateSQLXML(int columnIndex, SQLXML sqlXml)
        throws RemoteException, SQLException;

    void updateSQLXML(String columnLabel, SQLXML sqlXml)
        throws RemoteException, SQLException;

    String getNString(int columnIndex)
        throws RemoteException, SQLException;

    String getNString(String columnLabel)
        throws RemoteException, SQLException;

    Reader getNCharacterStream(int columnIndex)
        throws RemoteException, SQLException;

    Reader getNCharacterStream(String columnLabel)
        throws RemoteException, SQLException;

    void updateNCharacterStream(int columnIndex, Reader reader, long length)
        throws RemoteException, SQLException;

    void updateNCharacterStream(String columnLabel, Reader reader, long length)
        throws RemoteException, SQLException;

    void updateAsciiStream(int columnIndex, InputStream inputStream, long length)
        throws RemoteException, SQLException;

    void updateBinaryStream(int columnIndex, InputStream inputStream, long length)
        throws RemoteException, SQLException;

    void updateCharacterStream(int columnIndex, Reader reader, long length)
        throws RemoteException, SQLException;

    void updateAsciiStream(String columnLabel, InputStream inputStream, long length)
        throws RemoteException, SQLException;

    void updateBinaryStream(String columnLabel, InputStream inputStream, long length)
        throws RemoteException, SQLException;

    void updateCharacterStream(String columnLabel, Reader reader, long length)
        throws RemoteException, SQLException;

    void updateBlob(int columnIndex, InputStream inputStream, long length)
        throws RemoteException, SQLException;

    void updateBlob(String columnLabel, InputStream inputStream, long length)
        throws RemoteException, SQLException;

    void updateClob(int columnIndex, Reader reader, long length)
        throws RemoteException, SQLException;

    void updateClob(String columnLabel, Reader reader, long length)
        throws RemoteException, SQLException;

    void updateNClob(int columnIndex, Reader reader, long length)
        throws RemoteException, SQLException;

    void updateNClob(String columnLabel, Reader reader, long length)
        throws RemoteException, SQLException;

    void updateNCharacterStream(int columnIndex, Reader reader)
        throws RemoteException, SQLException;

    void updateNCharacterStream(String columnLabel, Reader reader)
        throws RemoteException, SQLException;

    void updateAsciiStream(int columnIndex, InputStream inputStream)
        throws RemoteException, SQLException;

    void updateBinaryStream(int columnIndex, InputStream inputStream)
        throws RemoteException, SQLException;

    void updateCharacterStream(int columnIndex, Reader reader)
        throws RemoteException, SQLException;

    void updateAsciiStream(String columnLabel, InputStream inputStream)
        throws RemoteException, SQLException;

    void updateBinaryStream(String columnLabel, InputStream inputStream)
        throws RemoteException, SQLException;

    void updateCharacterStream(String columnLabel, Reader reader)
        throws RemoteException, SQLException;

    void updateBlob(int columnIndex, InputStream inputStream)
        throws RemoteException, SQLException;

    void updateBlob(String columnLabel, InputStream inputStream)
        throws RemoteException, SQLException;

    void updateClob(int columnIndex, Reader reader)
        throws RemoteException, SQLException;

    void updateClob(String columnLabel, Reader reader)
        throws RemoteException, SQLException;

    void updateNClob(int columnIndex, Reader reader)
        throws RemoteException, SQLException;

    void updateNClob(String columnLabel, Reader reader)
        throws RemoteException, SQLException;

    <T> T getObject(int columnIndex,
		    Class<T> type)
	throws RemoteException, SQLException;

    <T> T getObject(String columnLabel,
			   Class<T> type)
	throws RemoteException, SQLException;
}
