
package FI.realitymodeler.sql;

import java.rmi.*;
import java.sql.*;
import java.util.*;
import java.util.logging.*;

public interface RemoteDriver extends Remote {

    RemoteConnection getConnection(String url)
        throws RemoteException, SQLException;

    Connection connect(String url, Properties info)
        throws RemoteException, SQLException;

    boolean acceptsURL(String url)
        throws RemoteException, SQLException;

    DriverPropertyInfo[] getPropertyInfo(String url, Properties info)
        throws RemoteException, SQLException;

    int getMajorVersion()
        throws RemoteException, SQLException;

    int getMinorVersion()
        throws RemoteException, SQLException;

    boolean jdbcCompliant()
        throws RemoteException, SQLException;

    Logger getParentLogger()
        throws RemoteException, SQLException;

}
