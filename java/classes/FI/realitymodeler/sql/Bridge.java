
package FI.realitymodeler.sql;

import java.rmi.*;
import java.sql.*;

public interface Bridge extends Remote {

    RemoteConnection getConnection()
        throws RemoteException, SQLException;

}
