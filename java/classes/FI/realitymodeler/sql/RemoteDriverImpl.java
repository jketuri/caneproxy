
package FI.realitymodeler.sql;

import java.io.*;
import java.rmi.*;
import java.rmi.registry.*;
import java.rmi.server.*;
import java.sql.*;
import java.util.*;
import java.util.logging.*;
import javax.servlet.*;

/** Delivers unrestricted connections to remote data bases through
    bridged connection which uses actual connection obtained from remote
    Driver. Can be initialized as servlet in host machine. */

public class RemoteDriverImpl extends UnicastRemoteObject implements RemoteDriver, Driver, Servlet {
    private static final long serialVersionUID = 0L;

    static {
        try {
            DriverManager.registerDriver(new RemoteDriverImpl());
        } catch (Exception ex) {}
    }

    private String name;
    private ServletConfig config;

    public RemoteDriverImpl()
        throws RemoteException {
    }

    public static void prepare() {
    }

    /** Initializes this driver as servlet in host machine binding it to
        registry to be used by clients by connect-method.  Initialization
        parameter 'bindName' can give alternative bind name which defaults to
        this classes name. 

        @see connect */

    public void init(ServletConfig config)
        throws ServletException {
        if ((name = config.getInitParameter("bindName")) == null)
            name = "rmi://localhost:" + Registry.REGISTRY_PORT + "/" + getClass().getName();
        try {
            Class.forName("jdbc.odbc.JdbcOdbcDriver");
            Naming.rebind(name, this);
        } catch (Exception ex) {
            throw new ServletException(ex.toString());
        }
    }

    public ServletConfig getServletConfig() {
        return config;
    }

    public void service(ServletRequest req, ServletResponse res) {
    }

    public String getServletInfo() {
        return getClass().getName();
    }

    public void destroy() {
        try {
            Naming.unbind(name);
        } catch (Exception ex) {}
    }

    public RemoteConnection getConnection(String url)
        throws RemoteException, SQLException {
        return new RemoteConnectionImpl(DriverManager.getConnection(url));
    }

    public boolean check(String url, int x[]) {
        int i, j;
        if ((i = url.indexOf(':')) == -1 || i + 1 >= url.length() || (j = url.indexOf(':', i + 1)) == -1 ||
            !url.regionMatches(true, i + 1, "FI.realitymodeler.Remote", 0, j - i - 1) || j + 1 >= url.length() ||
            (i = url.indexOf('/', j + 1)) == -1) return false;
        if (x != null) {
            x[0] = i;
            x[1] = j;
        }
        return true;
    }

    /** Gets connection to database with url like jdbc:FI.realitymodeler.Remote:host/url */
    public Connection connect(String url, Properties info)
        throws SQLException {
        int x[] = new int[2];
        if (!check(url, x)) return null;
        try {
            return new BridgeConnection(((RemoteDriver)Naming.lookup("rmi://" + url.substring(x[1] + 1,
                                                                                              x[0]) + "/" + getClass().getName())).getConnection(url.substring(x[0] + 1)));
        } catch (Exception ex) {
            PrintWriter pw = DriverManager.getLogWriter();
            if (pw != null) ex.printStackTrace(pw);
            if (ex instanceof SQLException) throw (SQLException)ex;
            throw new SQLException(ex.toString());
        }
    }

    public boolean acceptsURL(String url) {
        return check(url, null);
    }

    public DriverPropertyInfo[] getPropertyInfo(String url, Properties info) {
        return null;
    }

    public int getMajorVersion() {
        return 1;
    }

    public int getMinorVersion() {
        return 0;
    }

    public boolean jdbcCompliant() {
        return false;
    }

    public Logger getParentLogger()
	throws SQLFeatureNotSupportedException {
	throw new SQLFeatureNotSupportedException();
    }

}
