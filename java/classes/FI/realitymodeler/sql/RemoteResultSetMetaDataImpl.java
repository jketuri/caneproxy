
package FI.realitymodeler.sql;

import java.rmi.*;
import java.rmi.server.*;
import java.sql.*;

public class RemoteResultSetMetaDataImpl extends UnicastRemoteObject implements RemoteResultSetMetaData {
    private static final long serialVersionUID = 0L;

    private ResultSetMetaData rsmd;

    public RemoteResultSetMetaDataImpl(ResultSetMetaData rsmd)
        throws RemoteException {
        this.rsmd = rsmd;
    }

    public int getColumnCount()
        throws SQLException {
        return rsmd.getColumnCount();
    }

    public boolean isAutoIncrement(int columnIndex)
        throws SQLException {
        return rsmd.isAutoIncrement(columnIndex);
    }

    public boolean isCaseSensitive(int columnIndex)
        throws SQLException {
        return rsmd.isCaseSensitive(columnIndex);
    }

    public boolean isSearchable(int columnIndex)
        throws SQLException {
        return rsmd.isSearchable(columnIndex);
    }

    public boolean isCurrency(int columnIndex)
        throws SQLException {
        return rsmd.isCurrency(columnIndex);
    }

    public int isNullable(int columnIndex)
        throws SQLException {
        return rsmd.isNullable(columnIndex);
    }

    public boolean isSigned(int columnIndex)
        throws SQLException {
        return rsmd.isSigned(columnIndex);
    }

    public int getColumnDisplaySize(int columnIndex)
        throws SQLException {
        return rsmd.getColumnDisplaySize(columnIndex);
    }

    public String getColumnLabel(int columnIndex)
        throws SQLException {
        return rsmd.getColumnLabel(columnIndex);
    }

    public String getColumnName(int columnIndex)
        throws SQLException {
        return rsmd.getColumnName(columnIndex);
    }

    public String getSchemaName(int columnIndex)
        throws SQLException {
        return rsmd.getSchemaName(columnIndex);
    }

    public int getPrecision(int columnIndex)
        throws SQLException {
        return rsmd.getPrecision(columnIndex);
    }

    public int getScale(int columnIndex)
        throws SQLException {
        return rsmd.getScale(columnIndex);
    }

    public String getTableName(int columnIndex)
        throws SQLException {
        return rsmd.getTableName(columnIndex);
    }

    public String getCatalogName(int columnIndex)
        throws SQLException {
        return rsmd.getCatalogName(columnIndex);
    }

    public int getColumnType(int columnIndex)
        throws SQLException {
        return rsmd.getColumnType(columnIndex);
    }

    public String getColumnTypeName(int columnIndex)
        throws SQLException {
        return rsmd.getColumnTypeName(columnIndex);
    }

    public boolean isReadOnly(int columnIndex)
        throws SQLException {
        return rsmd.isReadOnly(columnIndex);
    }

    public boolean isWritable(int columnIndex)
        throws SQLException {
        return rsmd.isWritable(columnIndex);
    }

    public boolean isDefinitelyWritable(int columnIndex)
        throws SQLException {
        return rsmd.isDefinitelyWritable(columnIndex);
    }

    public String getColumnClassName(int columnIndex)
        throws SQLException {
        return rsmd.getColumnClassName(columnIndex);
    }

}
