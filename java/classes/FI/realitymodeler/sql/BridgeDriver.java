
package FI.realitymodeler.sql;

import java.io.*;
import java.net.*;
import java.rmi.*;
import java.sql.*;
import java.util.*;
import java.util.logging.*;

/** Delivers controlled connections to remote data sources configured in
    remote registry as BridgeImpl-classes. 

    @see FI.realitymodeler.sql.BridgeImpl */

public class BridgeDriver implements Driver {
    static {
        try {
            DriverManager.registerDriver(new BridgeDriver());
        } catch (Exception ex) {};
    }

    public static void prepare() {
    }

    public boolean check(String url, int x[]) {
        int i, j;
        if ((i = url.indexOf(':')) == -1 || i + 1 >= url.length() || (j = url.indexOf(':', i + 1)) == -1 ||
            !url.regionMatches(true, i + 1, "FI.realitymodeler.Bridge", 0, j - i - 1) || j + 1 >= url.length() ||
            (i = url.indexOf('/', j + 1)) == -1) return false;
        if (x != null) {
            x[0] = i;
            x[1] = j;
        }
        return true;
    }

    /** Gets connection to database with url like
        jdbc:FI.realitymodeler.Bridge:host/data source.  If host is local,
        assumes that connection to internal java-based database is required. */

    public Connection connect(String url, Properties info)
        throws SQLException {
        int x[] = new int[2];
        if (!check(url, x)) return null;
        String host = url.substring(x[1] + 1, x[0]).trim(), name = url.substring(x[0]).trim();
        Connection connection = null;
        Exception ex = null;
        try {
            connection = host.equalsIgnoreCase("localhost") ?
                (Connection)new DatabaseConnection(name.substring(1)) :
                (Connection)new BridgeConnection(((Bridge)Naming.lookup("rmi://" + host +
                                                                        "/FI.realitymodeler.BridgeImpl" + name)).getConnection());
        } catch (IOException ex1) {
            ex = ex1;
        } catch (NotBoundException ex1) {
            ex = ex1;
        } finally {
            if (ex != null) {
                PrintWriter pw = DriverManager.getLogWriter();
                if (pw != null) ex.printStackTrace(pw);
                throw new SQLException(ex.toString());
            }
        }
        return connection;
    }

    public boolean acceptsURL(String url) {
        return check(url, null);
    }

    public DriverPropertyInfo[] getPropertyInfo(String url, Properties info) {
        return null;
    }

    public int getMajorVersion() {
        return 1;
    }

    public int getMinorVersion() {
        return 0;
    }

    public boolean jdbcCompliant() {
        return false;
    }

    public Logger getParentLogger()
	throws SQLFeatureNotSupportedException {
	throw new SQLFeatureNotSupportedException();
    }

}
