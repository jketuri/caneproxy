
package FI.realitymodeler.sql;

import FI.realitymodeler.common.*;
import java.io.*;
import java.util.*;

public class IndexComparator implements Comparator<IndexItem>, Serializable {
    private static final long serialVersionUID = 0L;

    transient DataItem dataItems[] = null;
    transient IndexContext xcontext = null;

    public int compare(IndexItem indexItem1, IndexItem indexItem2) {
        try {
            if (indexItem1.key == null) indexItem1.key = xcontext.indexFile.buildKey(xcontext, dataItems, indexItem1.dataRef);
            if (indexItem2.key == null) indexItem2.key = xcontext.indexFile.buildKey(xcontext, dataItems, indexItem2.dataRef);
            int d = Support.compare(indexItem1.key, indexItem2.key);
            return d < 0 ? -1 : d == 0 ? 0 : 1;
        } catch (IOException ex) {
            return -1;
        }
    }

}
