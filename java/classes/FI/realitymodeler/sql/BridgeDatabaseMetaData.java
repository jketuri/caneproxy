
package FI.realitymodeler.sql;

import java.rmi.*;
import java.sql.*;

public class BridgeDatabaseMetaData implements DatabaseMetaData {
    private RemoteDatabaseMetaData rdmd;

    public BridgeDatabaseMetaData(RemoteDatabaseMetaData rdmd) {
        this.rdmd = rdmd;
    }

    public boolean allProceduresAreCallable()
        throws SQLException {
        try { return rdmd.allProceduresAreCallable(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean allTablesAreSelectable()
        throws SQLException {
        try { return rdmd.allTablesAreSelectable(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String getURL()
        throws SQLException {
        try { return rdmd.getURL(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String getUserName()
        throws SQLException {
        try { return rdmd.getUserName(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean isReadOnly()
        throws SQLException {
        try { return rdmd.isReadOnly(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean nullsAreSortedHigh()
        throws SQLException {
        try { return rdmd.nullsAreSortedHigh(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean nullsAreSortedLow()
        throws SQLException {
        try { return rdmd.nullsAreSortedLow(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean nullsAreSortedAtStart()
        throws SQLException {
        try { return rdmd.nullsAreSortedAtStart(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean nullsAreSortedAtEnd()
        throws SQLException {
        try { return rdmd.nullsAreSortedAtEnd(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String getDatabaseProductName()
        throws SQLException {
        try { return rdmd.getDatabaseProductName(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String getDatabaseProductVersion()
        throws SQLException {
        try { return rdmd.getDatabaseProductVersion(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String getDriverName()
        throws SQLException {
        try { return rdmd.getDriverName(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String getDriverVersion()
        throws SQLException {
        try { return rdmd.getDriverVersion(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getDriverMajorVersion() {
        try { return rdmd.getDriverMajorVersion(); } catch (RemoteException ex) { return -1; }
    }

    public int getDriverMinorVersion() {
        try { return rdmd.getDriverMinorVersion(); } catch (RemoteException ex) { return -1; }
    }

    public boolean usesLocalFiles()
        throws SQLException {
        try { return rdmd.usesLocalFiles(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean usesLocalFilePerTable()
        throws SQLException {
        try { return rdmd.usesLocalFilePerTable(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsMixedCaseIdentifiers()
        throws SQLException {
        try { return rdmd.supportsMixedCaseIdentifiers(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean storesUpperCaseIdentifiers()
        throws SQLException {
        try { return rdmd.storesUpperCaseIdentifiers(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean storesLowerCaseIdentifiers()
        throws SQLException {
        try { return rdmd.storesLowerCaseIdentifiers(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean storesMixedCaseIdentifiers()
        throws SQLException {
        try { return rdmd.storesMixedCaseIdentifiers(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsMixedCaseQuotedIdentifiers()
        throws SQLException {
        try { return rdmd.supportsMixedCaseQuotedIdentifiers(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean storesUpperCaseQuotedIdentifiers()
        throws SQLException {
        try { return rdmd.storesUpperCaseQuotedIdentifiers(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean storesLowerCaseQuotedIdentifiers()
        throws SQLException {
        try { return rdmd.storesLowerCaseQuotedIdentifiers(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean storesMixedCaseQuotedIdentifiers()
        throws SQLException {
        try { return rdmd.storesMixedCaseQuotedIdentifiers(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String getIdentifierQuoteString()
        throws SQLException {
        try { return rdmd.getIdentifierQuoteString(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String getSQLKeywords()
        throws SQLException {
        try { return rdmd.getSQLKeywords(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String getNumericFunctions()
        throws SQLException {
        try { return rdmd.getNumericFunctions(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String getStringFunctions()
        throws SQLException {
        try { return rdmd.getStringFunctions(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String getSystemFunctions()
        throws SQLException {
        try { return rdmd.getSystemFunctions(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String getTimeDateFunctions()
        throws SQLException {
        try { return rdmd.getTimeDateFunctions(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String getSearchStringEscape()
        throws SQLException {
        try { return rdmd.getSearchStringEscape(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String getExtraNameCharacters()
        throws SQLException {
        try { return rdmd.getExtraNameCharacters(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsAlterTableWithAddColumn()
        throws SQLException {
        try { return rdmd.supportsAlterTableWithAddColumn(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsAlterTableWithDropColumn()
        throws SQLException {
        try { return rdmd.supportsAlterTableWithDropColumn(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsColumnAliasing()
        throws SQLException {
        try { return rdmd.supportsColumnAliasing(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean nullPlusNonNullIsNull()
        throws SQLException {
        try { return rdmd.nullPlusNonNullIsNull(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsConvert()
        throws SQLException {
        try { return rdmd.supportsConvert(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsConvert(int fromType, int toType)
        throws SQLException {
        try { return rdmd.supportsConvert(fromType, toType); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsTableCorrelationNames()
        throws SQLException {
        try { return rdmd.supportsTableCorrelationNames(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsDifferentTableCorrelationNames()
        throws SQLException {
        try { return rdmd.supportsDifferentTableCorrelationNames(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsExpressionsInOrderBy()
        throws SQLException {
        try { return rdmd.supportsExpressionsInOrderBy(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsOrderByUnrelated()
        throws SQLException {
        try { return rdmd.supportsOrderByUnrelated(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsGroupBy()
        throws SQLException {
        try { return rdmd.supportsGroupBy(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsGroupByUnrelated()
        throws SQLException {
        try { return rdmd.supportsGroupByUnrelated(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsGroupByBeyondSelect()
        throws SQLException {
        try { return rdmd.supportsGroupByBeyondSelect(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsLikeEscapeClause()
        throws SQLException {
        try { return rdmd.supportsLikeEscapeClause(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsMultipleResultSets()
        throws SQLException {
        try { return rdmd.supportsMultipleResultSets(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsMultipleTransactions()
        throws SQLException {
        try { return rdmd.supportsMultipleTransactions(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsNonNullableColumns()
        throws SQLException {
        try { return rdmd.supportsNonNullableColumns(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsMinimumSQLGrammar()
        throws SQLException {
        try { return rdmd.supportsMinimumSQLGrammar(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsCoreSQLGrammar()
        throws SQLException {
        try { return rdmd.supportsCoreSQLGrammar(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsExtendedSQLGrammar()
        throws SQLException {
        try { return rdmd.supportsExtendedSQLGrammar(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsANSI92EntryLevelSQL()
        throws SQLException {
        try { return rdmd.supportsANSI92EntryLevelSQL(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsANSI92IntermediateSQL()
        throws SQLException {
        try { return rdmd.supportsANSI92IntermediateSQL(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsANSI92FullSQL()
        throws SQLException {
        try { return rdmd.supportsANSI92FullSQL(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsIntegrityEnhancementFacility()
        throws SQLException {
        try { return rdmd.supportsIntegrityEnhancementFacility(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsOuterJoins()
        throws SQLException {
        try { return rdmd.supportsOuterJoins(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsFullOuterJoins()
        throws SQLException {
        try { return rdmd.supportsFullOuterJoins(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsLimitedOuterJoins()
        throws SQLException {
        try { return rdmd.supportsLimitedOuterJoins(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String getSchemaTerm()
        throws SQLException {
        try { return rdmd.getSchemaTerm(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String getProcedureTerm()
        throws SQLException {
        try { return rdmd.getProcedureTerm(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String getCatalogTerm()
        throws SQLException {
        try { return rdmd.getCatalogTerm(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean isCatalogAtStart()
        throws SQLException {
        try { return rdmd.isCatalogAtStart(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String getCatalogSeparator()
        throws SQLException {
        try { return rdmd.getCatalogSeparator(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsSchemasInDataManipulation()
        throws SQLException {
        try { return rdmd.supportsSchemasInDataManipulation(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsSchemasInProcedureCalls()
        throws SQLException {
        try { return rdmd.supportsSchemasInProcedureCalls(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsSchemasInTableDefinitions()
        throws SQLException {
        try { return rdmd.supportsSchemasInTableDefinitions(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsSchemasInIndexDefinitions()
        throws SQLException {
        try { return rdmd.supportsSchemasInIndexDefinitions(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsSchemasInPrivilegeDefinitions()
        throws SQLException {
        try { return rdmd.supportsSchemasInPrivilegeDefinitions(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsCatalogsInDataManipulation()
        throws SQLException {
        try { return rdmd.supportsCatalogsInDataManipulation(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsCatalogsInProcedureCalls()
        throws SQLException {
        try { return rdmd.supportsCatalogsInProcedureCalls(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsCatalogsInTableDefinitions()
        throws SQLException {
        try { return rdmd.supportsCatalogsInTableDefinitions(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsCatalogsInIndexDefinitions()
        throws SQLException {
        try { return rdmd.supportsCatalogsInIndexDefinitions(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsCatalogsInPrivilegeDefinitions()
        throws SQLException {
        try { return rdmd.supportsCatalogsInPrivilegeDefinitions(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsPositionedDelete()
        throws SQLException {
        try { return rdmd.supportsPositionedDelete(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsPositionedUpdate()
        throws SQLException {
        try { return rdmd.supportsPositionedUpdate(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsSelectForUpdate()
        throws SQLException {
        try { return rdmd.supportsSelectForUpdate(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsStoredProcedures()
        throws SQLException {
        try { return rdmd.supportsStoredProcedures(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsSubqueriesInComparisons()
        throws SQLException {
        try { return rdmd.supportsSubqueriesInComparisons(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsSubqueriesInExists()
        throws SQLException {
        try { return rdmd.supportsSubqueriesInExists(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsSubqueriesInIns()
        throws SQLException {
        try { return rdmd.supportsSubqueriesInIns(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsSubqueriesInQuantifieds()
        throws SQLException {
        try { return rdmd.supportsSubqueriesInQuantifieds(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsCorrelatedSubqueries()
        throws SQLException {
        try { return rdmd.supportsCorrelatedSubqueries(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsUnion()
        throws SQLException {
        try { return rdmd.supportsUnion(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsUnionAll()
        throws SQLException {
        try { return rdmd.supportsUnionAll(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsOpenCursorsAcrossCommit()
        throws SQLException {
        try { return rdmd.supportsOpenCursorsAcrossCommit(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsOpenCursorsAcrossRollback()
        throws SQLException {
        try { return rdmd.supportsOpenCursorsAcrossRollback(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsOpenStatementsAcrossCommit()
        throws SQLException {
        try { return rdmd.supportsOpenStatementsAcrossCommit(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsOpenStatementsAcrossRollback()
        throws SQLException {
        try { return rdmd.supportsOpenStatementsAcrossRollback(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getMaxBinaryLiteralLength()
        throws SQLException {
        try { return rdmd.getMaxBinaryLiteralLength(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getMaxCharLiteralLength()
        throws SQLException {
        try { return rdmd.getMaxCharLiteralLength(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getMaxColumnNameLength()
        throws SQLException {
        try { return rdmd.getMaxColumnNameLength(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getMaxColumnsInGroupBy()
        throws SQLException {
        try { return rdmd.getMaxColumnsInGroupBy(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getMaxColumnsInIndex()
        throws SQLException {
        try { return rdmd.getMaxColumnsInIndex(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getMaxColumnsInOrderBy()
        throws SQLException {
        try { return rdmd.getMaxColumnsInOrderBy(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getMaxColumnsInSelect()
        throws SQLException {
        try { return rdmd.getMaxColumnsInSelect(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getMaxColumnsInTable()
        throws SQLException {
        try { return rdmd.getMaxColumnsInTable(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getMaxConnections()
        throws SQLException {
        try { return rdmd.getMaxConnections(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getMaxCursorNameLength()
        throws SQLException {
        try { return rdmd.getMaxCursorNameLength(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getMaxIndexLength()
        throws SQLException {
        try { return rdmd.getMaxIndexLength(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getMaxSchemaNameLength()
        throws SQLException {
        try { return rdmd.getMaxSchemaNameLength(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getMaxProcedureNameLength()
        throws SQLException {
        try { return rdmd.getMaxProcedureNameLength(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getMaxCatalogNameLength()
        throws SQLException {
        try { return rdmd.getMaxCatalogNameLength(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getMaxRowSize()
        throws SQLException {
        try { return rdmd.getMaxRowSize(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean doesMaxRowSizeIncludeBlobs()
        throws SQLException {
        try { return rdmd.doesMaxRowSizeIncludeBlobs(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getMaxStatementLength()
        throws SQLException {
        try { return rdmd.getMaxStatementLength(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getMaxStatements()
        throws SQLException {
        try { return rdmd.getMaxStatements(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getMaxTableNameLength()
        throws SQLException {
        try { return rdmd.getMaxTableNameLength(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getMaxTablesInSelect()
        throws SQLException {
        try { return rdmd.getMaxTablesInSelect(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getMaxUserNameLength()
        throws SQLException {
        try { return rdmd.getMaxUserNameLength(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getDefaultTransactionIsolation()
        throws SQLException {
        try { return rdmd.getDefaultTransactionIsolation(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsTransactions()
        throws SQLException {
        try { return rdmd.supportsTransactions(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsTransactionIsolationLevel(int level)
        throws SQLException {
        try { return rdmd.supportsTransactionIsolationLevel(level); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsDataDefinitionAndDataManipulationTransactions()
        throws SQLException {
        try { return rdmd.supportsDataDefinitionAndDataManipulationTransactions(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsDataManipulationTransactionsOnly()
        throws SQLException {
        try { return rdmd.supportsDataManipulationTransactionsOnly(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean dataDefinitionCausesTransactionCommit()
        throws SQLException {
        try { return rdmd.dataDefinitionCausesTransactionCommit(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean dataDefinitionIgnoredInTransactions()
        throws SQLException {
        try { return rdmd.dataDefinitionIgnoredInTransactions(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public ResultSet getProcedures(String catalog, String schemaPattern, String procedureNamePattern)
        throws SQLException {
        try { return new BridgeResultSet(rdmd.getProcedures(catalog, schemaPattern, procedureNamePattern)); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public ResultSet getProcedureColumns(String catalog, String schemaPattern, String procedureNamePattern, String columnNamePattern)
        throws SQLException {
        try { return new BridgeResultSet(rdmd.getProcedureColumns(catalog, schemaPattern, procedureNamePattern,
                                                                  columnNamePattern)); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public ResultSet getTables(String catalog,String schemaPattern,String tableNamePattern,String types[])
        throws SQLException {
        try { return new BridgeResultSet(rdmd.getTables(catalog, schemaPattern, tableNamePattern, types)); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public ResultSet getSchemas()
        throws SQLException {
        try { return new BridgeResultSet(rdmd.getSchemas()); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public ResultSet getCatalogs()
        throws SQLException {
        try { return new BridgeResultSet(rdmd.getCatalogs()); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public ResultSet getTableTypes()
        throws SQLException {
        try { return new BridgeResultSet(rdmd.getTableTypes()); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public ResultSet getColumns(String catalog, String schemaPattern, String tableNamePattern, String columnNamePattern)
        throws SQLException {
        try { return new BridgeResultSet(rdmd.getColumns(catalog, schemaPattern, tableNamePattern, columnNamePattern)); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public ResultSet getColumnPrivileges(String catalog, String schema, String table, String columnNamePattern)
        throws SQLException {
        try { return new BridgeResultSet(rdmd.getColumnPrivileges(catalog, schema, table, columnNamePattern)); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public ResultSet getTablePrivileges(String catalog, String schemaPattern, String tableNamePattern)
        throws SQLException {
        try { return new BridgeResultSet(rdmd.getTablePrivileges(catalog, schemaPattern, tableNamePattern)); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public ResultSet getBestRowIdentifier(String catalog, String schema, String table, int scope, boolean nullable)
        throws SQLException {
        try { return new BridgeResultSet(rdmd.getBestRowIdentifier(catalog, schema, table, scope, nullable)); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public ResultSet getVersionColumns(String catalog, String schema, String table)
        throws SQLException {
        try { return new BridgeResultSet(rdmd.getVersionColumns(catalog, schema, table)); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public ResultSet getPrimaryKeys(String catalog, String schema, String table)
        throws SQLException {
        try { return new BridgeResultSet(rdmd.getPrimaryKeys(catalog, schema, table)); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public ResultSet getImportedKeys(String catalog, String schema, String table)
        throws SQLException {
        try { return new BridgeResultSet(rdmd.getImportedKeys(catalog, schema, table)); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public ResultSet getExportedKeys(String catalog, String schema, String table)
        throws SQLException {
        try { return new BridgeResultSet(rdmd.getExportedKeys(catalog, schema, table)); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public ResultSet getCrossReference(String primaryCatalog, String primarySchema, String primaryTable, String foreignCatalog,String foreignSchema,String foreignTable)
        throws SQLException {
        try { return new BridgeResultSet(rdmd.getCrossReference(primaryCatalog, primarySchema, primaryTable, foreignCatalog, foreignSchema, foreignTable)); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public ResultSet getTypeInfo()
        throws SQLException {
        try { return new BridgeResultSet(rdmd.getTypeInfo()); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public ResultSet getIndexInfo(String catalog, String schema, String table, boolean unique, boolean approximate)
        throws SQLException {
        try { return new BridgeResultSet(rdmd.getIndexInfo(catalog, schema, table, unique, approximate)); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsResultSetType(int type)
        throws SQLException {
        try { return rdmd.supportsResultSetType(type); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsResultSetConcurrency(int type, int concurrency)
        throws SQLException {
        try { return rdmd.supportsResultSetConcurrency(type, concurrency); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean ownUpdatesAreVisible(int type)
        throws SQLException {
        try { return rdmd.ownUpdatesAreVisible(type); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean ownDeletesAreVisible(int type)
        throws SQLException {
        try { return rdmd.ownDeletesAreVisible(type); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean ownInsertsAreVisible(int type)
        throws SQLException {
        try { return rdmd.ownInsertsAreVisible(type); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean othersUpdatesAreVisible(int type)
        throws SQLException {
        try { return rdmd.othersUpdatesAreVisible(type); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean othersDeletesAreVisible(int type)
        throws SQLException {
        try { return rdmd.othersDeletesAreVisible(type); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean othersInsertsAreVisible(int type)
        throws SQLException {
        try { return rdmd.othersInsertsAreVisible(type); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean updatesAreDetected(int type)
        throws SQLException {
        try { return rdmd.updatesAreDetected(type); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean deletesAreDetected(int type)
        throws SQLException {
        try { return rdmd.deletesAreDetected(type); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean insertsAreDetected(int type)
        throws SQLException {
        try { return rdmd.insertsAreDetected(type); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsBatchUpdates()
        throws SQLException {
        try { return rdmd.supportsBatchUpdates(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public ResultSet getUDTs(String catalog, String schemaPattern, String typeNamePattern, int types[])
        throws SQLException {
        try { return new BridgeResultSet(rdmd.getUDTs(catalog, schemaPattern, typeNamePattern, types)); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Connection getConnection()
        throws SQLException {
        try { return new BridgeConnection(rdmd.getConnection()); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsSavepoints()
        throws SQLException {
        try { return rdmd.supportsSavepoints(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsNamedParameters()
        throws SQLException {
        try { return rdmd.supportsNamedParameters(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsMultipleOpenResults()
        throws SQLException {
        try { return rdmd.supportsMultipleOpenResults(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsGetGeneratedKeys()
        throws SQLException {
        try { return rdmd.supportsGetGeneratedKeys(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public ResultSet getSuperTypes(String catalog, String schemaPattern, String typeNamePattern)
        throws SQLException {
        try { return new BridgeResultSet(rdmd.getSuperTypes(catalog, schemaPattern, typeNamePattern)); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public ResultSet getSuperTables(String catalog, String schemaPattern, String tableNamePattern)
        throws SQLException {
        try { return new BridgeResultSet(rdmd.getSuperTables(catalog, schemaPattern, tableNamePattern)); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public ResultSet getAttributes(String catalog, String schemaPattern, String typeNamePattern, String attributeNamePattern)
        throws SQLException {
        try { return new BridgeResultSet(rdmd.getAttributes(catalog, schemaPattern, typeNamePattern, attributeNamePattern)); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsResultSetHoldability(int holdability)
        throws SQLException {
        try { return rdmd.supportsResultSetHoldability(holdability); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getResultSetHoldability()
        throws SQLException {
        try { return rdmd.getResultSetHoldability(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getDatabaseMajorVersion()
        throws SQLException {
        try { return rdmd.getDatabaseMajorVersion(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getDatabaseMinorVersion()
        throws SQLException {
        try { return rdmd.getDatabaseMinorVersion(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getJDBCMajorVersion()
        throws SQLException {
        try { return rdmd.getJDBCMajorVersion(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getJDBCMinorVersion()
        throws SQLException {
        try { return rdmd.getJDBCMinorVersion(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getSQLStateType()
        throws SQLException {
        try { return rdmd.getSQLStateType(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean locatorsUpdateCopy()
        throws SQLException {
        try { return rdmd.locatorsUpdateCopy(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsStatementPooling()
        throws SQLException {
        try { return rdmd.supportsStatementPooling(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public RowIdLifetime getRowIdLifetime()
        throws SQLException {
        try { return rdmd.getRowIdLifetime(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public ResultSet getSchemas(String catalog, String schemaPattern)
        throws SQLException {
        try { return rdmd.getSchemas(catalog, schemaPattern); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean supportsStoredFunctionsUsingCallSyntax()
        throws SQLException {
        try { return rdmd.supportsStoredFunctionsUsingCallSyntax(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean autoCommitFailureClosesAllResultSets()
        throws SQLException {
        try { return rdmd.autoCommitFailureClosesAllResultSets(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public ResultSet getClientInfoProperties()
        throws SQLException {
        try { return rdmd.getClientInfoProperties(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public ResultSet getFunctions(String catalog, String schemaPattern, String functionNamePattern)
        throws SQLException {
        try { return rdmd.getFunctions(catalog, schemaPattern, functionNamePattern); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public ResultSet getFunctionColumns(String catalog, String schemaPattern, String functionNamePattern, String columnNamePattern)
        throws SQLException {
        try { return rdmd.getFunctionColumns(catalog, schemaPattern, functionNamePattern, columnNamePattern); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public ResultSet getPseudoColumns(String catalog,
				      String schemaPattern,
				      String tableNamePattern,
				      String columnNamePattern)
    {
	return null;
    }

    public boolean generatedKeyAlwaysReturned()
    {
	return false;
    }

    public <T> T unwrap(Class<T> iface) {
        return (T)iface;
    }

    public boolean isWrapperFor(Class<?> iface) {
        return false;
    }

}
