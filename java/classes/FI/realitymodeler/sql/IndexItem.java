
/* IndexItem.java */

package FI.realitymodeler.sql;

import java.io.*;

/** IndexItem is a general class, which holds index item's data reference and key string.
    This is used for internal storage, parameter passing and for returning results. */
public class IndexItem implements Serializable {
    private static final long serialVersionUID = 0L;

    /** dataRef is a general data reference, which can point for example to the data file record,
        which has specified contents of key field. Or it can point to arbitrary position in a text file,
        where context corresponds to specified key. Generally it must be possible to reconstruct key from
        referenced locations, in case the index file must be recovered. */
    public int dataRef;
    public int dataRefs[] = null;
    transient public byte key[];

    public void copy(IndexContext xcontext, IndexItem indexItem)
        throws IOException {
        dataRef = indexItem.dataRef;
        dataRefs = indexItem.dataRefs;
        key = null;
        xcontext.dataRef = dataRef;
        if (xcontext.dataRefIndex >= 0 && dataRefs != null) xcontext.dataRef = dataRefs[xcontext.dataRefIndex];
        if (xcontext.dataRef != 0) key = xcontext.indexFile.buildKey(xcontext, xcontext.indexFile.dataItems, xcontext.dataRef);
    }

    public String toString() {
        StringBuffer buffer = null;
        if (dataRefs != null) {
            buffer = new StringBuffer();
            for (int i = 0; i < dataRefs.length; i++) buffer.append(',').append(dataRefs[i]);
        }
        return getClass().getName() + "{dataRef=" + dataRef +
            (buffer != null ? ",dataRefs=" + buffer.toString() : "") +
            (key != null ? ",key=" + new String(key) + "}" : "}");
    }

}

