
package FI.realitymodeler.sql;

import java.io.*;
import java.math.*;
import java.net.*;
import java.rmi.*;
import java.rmi.server.*;
import java.sql.*;
import java.util.*;

public class RemoteResultSetImpl extends UnicastRemoteObject implements RemoteResultSet {
    private static final long serialVersionUID = 0L;

    private ResultSet rs;

    public RemoteResultSetImpl(ResultSet rs)
        throws RemoteException {
        this.rs = rs;
    }

    public boolean next()
        throws SQLException {
        return rs.next();
    }

    public void close()
        throws SQLException {
        rs.close();
    }

    public boolean wasNull()
        throws SQLException {
        return rs.wasNull();
    }

    public String getString(int columnIndex)
        throws SQLException {
        return rs.getString(columnIndex);
    }

    public boolean getBoolean(int columnIndex)
        throws SQLException {
        return rs.getBoolean(columnIndex);
    }

    public byte getByte(int columnIndex)
        throws SQLException {
        return rs.getByte(columnIndex);
    }

    public short getShort(int columnIndex)
        throws SQLException {
        return rs.getShort(columnIndex);
    }

    public int getInt(int columnIndex)
        throws SQLException {
        return rs.getInt(columnIndex);
    }

    public long getLong(int columnIndex)
        throws SQLException {
        return rs.getLong(columnIndex);
    }

    public float getFloat(int columnIndex)
        throws SQLException {
        return rs.getFloat(columnIndex);
    }

    public double getDouble(int columnIndex)
        throws SQLException {
        return rs.getDouble(columnIndex);
    }

    public BigDecimal getBigDecimal(int columnIndex, int scale)
        throws SQLException {
        return rs.getBigDecimal(columnIndex);
    }

    public byte[] getBytes(int columnIndex)
        throws SQLException {
        return rs.getBytes(columnIndex);
    }

    public java.sql.Date getDate(int columnIndex)
        throws SQLException {
        return rs.getDate(columnIndex);
    }

    public java.sql.Time getTime(int columnIndex)
        throws SQLException {
        return rs.getTime(columnIndex);
    }

    public java.sql.Timestamp getTimestamp(int columnIndex)
        throws SQLException {
        return rs.getTimestamp(columnIndex);
    }

    public URL getURL(int columnIndex)
        throws SQLException {
        return rs.getURL(columnIndex);
    }

    public TransferStream getStream(int columnIndex, boolean ascii)
        throws SQLException {
        return new TransferStream(ascii ? rs.getAsciiStream(columnIndex) : rs.getBinaryStream(columnIndex));
    }

    public String getString(String columnName)
        throws SQLException {
        return rs.getString(columnName);
    }

    public boolean getBoolean(String columnName)
        throws SQLException {
        return rs.getBoolean(columnName);
    }

    public byte getByte(String columnName)
        throws SQLException {
        return rs.getByte(columnName);
    }

    public short getShort(String columnName)
        throws SQLException {
        return rs.getShort(columnName);
    }

    public int getInt(String columnName)
        throws SQLException {
        return rs.getInt(columnName);
    }

    public long getLong(String columnName)
        throws SQLException {
        return rs.getLong(columnName);
    }

    public float getFloat(String columnName)
        throws SQLException {
        return rs.getFloat(columnName);
    }

    public double getDouble(String columnName)
        throws SQLException {
        return rs.getDouble(columnName);
    }

    public BigDecimal getBigDecimal(String columnName, int scale)
        throws SQLException {
        return rs.getBigDecimal(columnName);
    }

    public byte[] getBytes(String columnName)
        throws SQLException {
        return rs.getBytes(columnName);
    }

    public java.sql.Date getDate(String columnName)
        throws SQLException {
        return rs.getDate(columnName);
    }

    public java.sql.Time getTime(String columnName)
        throws SQLException {
        return rs.getTime(columnName);
    }

    public java.sql.Timestamp getTimestamp(String columnName)
        throws SQLException {
        return rs.getTimestamp(columnName);
    }

    public URL getURL(String columnName)
        throws SQLException {
        return rs.getURL(columnName);
    }

    public SQLWarning getWarnings()
        throws SQLException {
        return rs.getWarnings();
    }

    public void clearWarnings()
        throws SQLException {
        rs.clearWarnings();
    }

    public String getCursorName()
        throws SQLException {
        return rs.getCursorName();
    }

    public RemoteResultSetMetaData getMetaData()
        throws RemoteException, SQLException {
        return new RemoteResultSetMetaDataImpl(rs.getMetaData());
    }

    public Object getObject(int columnIndex)
        throws SQLException {
        return rs.getObject(columnIndex);
    }

    public Object getObject(String columnName)
        throws SQLException {
        return rs.getObject(columnName);
    }

    public int findColumn(String columnName)
        throws SQLException {
        return rs.findColumn(columnName);
    }

    public Reader getCharacterStream(int columnIndex)
        throws SQLException {
        return rs.getCharacterStream(columnIndex);
    }

    public Reader getCharacterStream(String columnName)
        throws SQLException {
        return rs.getCharacterStream(columnName);
    }

    public BigDecimal getBigDecimal(int columnIndex)
        throws SQLException {
        return rs.getBigDecimal(columnIndex);
    }

    public BigDecimal getBigDecimal(String columnName)
        throws SQLException {
        return rs.getBigDecimal(columnName);
    }

    public boolean isBeforeFirst()
        throws SQLException {
        return rs.isBeforeFirst();
    }

    public boolean isAfterLast()
        throws SQLException {
        return rs.isAfterLast();
    }

    public boolean isFirst()
        throws SQLException {
        return rs.isFirst();
    }

    public boolean isLast()
        throws SQLException {
        return rs.isLast();
    }

    public void beforeFirst()
        throws SQLException {
        rs.beforeFirst();
    }

    public void afterLast()
        throws SQLException {
        rs.afterLast();
    }

    public boolean first()
        throws SQLException {
        return rs.first();
    }

    public boolean last()
        throws SQLException {
        return rs.last();
    }

    public int getRow()
        throws SQLException {
        return rs.getRow();
    }

    public boolean absolute(int row)
        throws SQLException {
        return rs.absolute(row);
    }

    public boolean relative(int rows)
        throws SQLException {
        return rs.relative(rows);
    }

    public boolean previous()
        throws SQLException {
        return rs.previous();
    }

    public void setFetchDirection(int direction)
        throws SQLException {
        rs.setFetchDirection(direction);
    }

    public int getFetchDirection()
        throws SQLException {
        return rs.getFetchDirection();
    }

    public void setFetchSize(int rows)
        throws SQLException {
        rs.setFetchSize(rows);
    }

    public int getFetchSize()
        throws SQLException {
        return rs.getFetchSize();
    }

    public int getType()
        throws SQLException {
        return rs.getType();
    }

    public int getConcurrency()
        throws SQLException {
        return rs.getConcurrency();
    }

    public boolean rowUpdated()
        throws SQLException {
        return rs.rowUpdated();
    }

    public boolean rowInserted()
        throws SQLException {
        return rs.rowInserted();
    }

    public boolean rowDeleted()
        throws SQLException {
        return rs.rowDeleted();
    }

    public void updateNull(int columnIndex)
        throws SQLException {
        rs.updateNull(columnIndex);
    }

    public void updateBoolean(int columnIndex, boolean x)
        throws SQLException {
        rs.updateBoolean(columnIndex, x);
    }

    public void updateByte(int columnIndex, byte x)
        throws SQLException {
        rs.updateByte(columnIndex, x);
    }

    public void updateShort(int columnIndex, short x)
        throws SQLException {
        rs.updateShort(columnIndex, x);
    }

    public void updateInt(int columnIndex, int x)
        throws SQLException {
        rs.updateInt(columnIndex, x);
    }

    public void updateLong(int columnIndex, long x)
        throws SQLException {
        rs.updateLong(columnIndex, x);
    }

    public void updateFloat(int columnIndex, float x)
        throws SQLException {
        rs.updateFloat(columnIndex, x);
    }

    public void updateDouble(int columnIndex, double x)
        throws SQLException {
        rs.updateDouble(columnIndex, x);
    }

    public void updateBigDecimal(int columnIndex, BigDecimal x)
        throws SQLException {
        rs.updateBigDecimal(columnIndex, x);
    }

    public void updateString(int columnIndex, String x)
        throws SQLException {
        rs.updateString(columnIndex, x);
    }

    public void updateBytes(int columnIndex, byte x[])
        throws SQLException {
        rs.updateBytes(columnIndex, x);
    }

    public void updateDate(int columnIndex, java.sql.Date x)
        throws SQLException {
        rs.updateDate(columnIndex, x);
    }

    public void updateTime(int columnIndex, java.sql.Time x)
        throws SQLException {
        rs.updateTime(columnIndex, x);
    }

    public void updateTimestamp(int columnIndex, Timestamp x)
        throws SQLException {
        rs.updateTimestamp(columnIndex, x);
    }

    public void updateAsciiStream(int columnIndex, java.io.InputStream x, int length)
        throws SQLException {
        rs.updateAsciiStream(columnIndex, x, length);
    }

    public void updateBinaryStream(int columnIndex, java.io.InputStream x, int length)
        throws SQLException {
        rs.updateBinaryStream(columnIndex, x, length);
    }

    public void updateCharacterStream(int columnIndex, Reader reader, int length)
        throws SQLException {
        rs.updateCharacterStream(columnIndex, reader, length);
    }

    public void updateObject(int columnIndex, Object x, int scale)
        throws SQLException {
        rs.updateObject(columnIndex, x, scale);
    }

    public void updateObject(int columnIndex, Object x)
        throws SQLException {
        rs.updateObject(columnIndex, x);
    }

    public void updateRef(int columnIndex, Ref x)
        throws SQLException {
        rs.updateRef(columnIndex, x);
    }

    public void updateBlob(int columnIndex, Blob x)
        throws SQLException {
        rs.updateBlob(columnIndex, x);
    }

    public void updateClob(int columnIndex, Clob x)
        throws SQLException {
        rs.updateClob(columnIndex, x);
    }

    public void updateArray(int columnIndex, Array x)
        throws SQLException {
        rs.updateArray(columnIndex, x);
    }

    public void updateNull(String columnName)
        throws SQLException {
        rs.updateNull(columnName);
    }

    public void updateBoolean(String columnName, boolean x)
        throws SQLException {
        rs.updateBoolean(columnName, x);
    }

    public void updateByte(String columnName, byte x)
        throws SQLException {
        rs.updateByte(columnName, x);
    }

    public void updateShort(String columnName, short x)
        throws SQLException {
        rs.updateShort(columnName, x);
    }

    public void updateInt(String columnName, int x)
        throws SQLException {
        rs.updateInt(columnName, x);
    }

    public void updateLong(String columnName, long x)
        throws SQLException {
        rs.updateLong(columnName, x);
    }

    public void updateFloat(String columnName, float x)
        throws SQLException {
        rs.updateFloat(columnName, x);
    }

    public void updateDouble(String columnName, double x)
        throws SQLException {
        rs.updateDouble(columnName, x);
    }

    public void updateBigDecimal(String columnName, BigDecimal x)
        throws SQLException {
        rs.updateBigDecimal(columnName, x);
    }

    public void updateString(String columnName, String x)
        throws SQLException {
        rs.updateString(columnName, x);
    }

    public void updateBytes(String columnName, byte x[])
        throws SQLException {
        rs.updateBytes(columnName, x);
    }

    public void updateDate(String columnName, java.sql.Date x)
        throws SQLException {
        rs.updateDate(columnName, x);
    }

    public void updateTime(String columnName, java.sql.Time x)
        throws SQLException {
        rs.updateTime(columnName, x);
    }

    public void updateTimestamp(String columnName, Timestamp x)
        throws SQLException {
        rs.updateTimestamp(columnName, x);
    }

    public void updateAsciiStream(String columnName, java.io.InputStream x, int length)
        throws SQLException {
        rs.updateAsciiStream(columnName, x, length);
    }

    public void updateBinaryStream(String columnName, java.io.InputStream x, int length)
        throws SQLException {
        rs.updateBinaryStream(columnName, x, length);
    }

    public void updateCharacterStream(String columnName, Reader reader, int length)
        throws SQLException {
        rs.updateCharacterStream(columnName, reader, length);
    }

    public void updateObject(String columnName, Object x, int scale)
        throws SQLException {
        rs.updateObject(columnName, x, scale);
    }

    public void updateObject(String columnName, Object x)
        throws SQLException {
        rs.updateObject(columnName, x);
    }

    public void updateRef(String columnName, Ref x)
        throws SQLException {
        rs.updateRef(columnName, x);
    }

    public void updateBlob(String columnName, Blob x)
        throws SQLException {
        rs.updateBlob(columnName, x);
    }

    public void updateClob(String columnName, Clob x)
        throws SQLException {
        rs.updateClob(columnName, x);
    }

    public void updateArray(String columnName, Array x)
        throws SQLException {
        rs.updateArray(columnName, x);
    }

    public void insertRow()
        throws SQLException {
        rs.insertRow();
    }

    public void updateRow()
        throws SQLException {
        rs.updateRow();
    }

    public void deleteRow()
        throws SQLException {
        rs.deleteRow();
    }

    public void refreshRow()
        throws SQLException {
        rs.refreshRow();
    }

    public void cancelRowUpdates()
        throws SQLException {
        rs.cancelRowUpdates();
    }

    public void moveToInsertRow()
        throws SQLException {
        rs.moveToInsertRow();
    }

    public void moveToCurrentRow()
        throws SQLException {
        rs.insertRow();
    }

    public Object getObject(int i, Map<String, Class<?>> map)
        throws SQLException {
        return rs.getObject(i, map);
    }

    public Ref getRef(int i)
        throws SQLException {
        return rs.getRef(i);
    }

    public Blob getBlob(int i)
        throws SQLException {
        return rs.getBlob(i);
    }

    public Clob getClob(int i)
        throws SQLException {
        return rs.getClob(i);
    }

    public Array getArray(int i)
        throws SQLException {
        return rs.getArray(i);
    }

    public java.sql.Date getDate(int columnIndex, Calendar cal)
        throws SQLException {
        return rs.getDate(columnIndex, cal);
    }

    public java.sql.Date getDate(String columnName, Calendar cal)
        throws SQLException {
        return rs.getDate(columnName);
    }

    public java.sql.Time getTime(int columnIndex, Calendar cal)
        throws SQLException {
        return rs.getTime(columnIndex, cal);
    }

    public java.sql.Time getTime(String columnName, Calendar cal)
        throws SQLException {
        return rs.getTime(columnName);
    }

    public java.sql.Timestamp getTimestamp(int columnIndex, Calendar cal)
        throws SQLException {
        return rs.getTimestamp(columnIndex, cal);
    }

    public java.sql.Timestamp getTimestamp(String columnName, Calendar cal)
        throws SQLException {
        return rs.getTimestamp(columnName, cal);
    }

    public Statement getStatement()
        throws SQLException {
        return rs.getStatement();
    }

    public Object getObject(String columnName, Map<String, Class<?>> map)
        throws SQLException {
        return rs.getObject(columnName, map);
    }

    public Ref getRef(String columnName)
        throws SQLException {
        return rs.getRef(columnName);
    }

    public Blob getBlob(String columnName)
        throws SQLException {
        return rs.getBlob(columnName);
    }

    public Clob getClob(String columnName)
        throws SQLException {
        return rs.getClob(columnName);
    }

    public Array getArray(String columnName)
        throws SQLException {
        return rs.getArray(columnName);
    }

    public RowId getRowId(int columnIndex)
        throws SQLException {
        return rs.getRowId(columnIndex);
    }

    public RowId getRowId(String columnLabel)
        throws SQLException {
        return rs.getRowId(columnLabel);
    }

    public void updateRowId(int columnIndex, RowId rowId)
        throws SQLException {
        rs.updateRowId(columnIndex, rowId);
    }

    public void updateRowId(String columnLabel, RowId rowId)
        throws SQLException {
        rs.updateRowId(columnLabel, rowId);
    }

    public int getHoldability()
        throws SQLException {
        return rs.getHoldability();
    }

    public boolean isClosed()
        throws SQLException {
        return rs.isClosed();
    }

    public void updateNString(int columnIndex, String nString)
        throws SQLException {
        rs.updateNString(columnIndex, nString);
    }

    public void updateNString(String columnLabel, String nString)
        throws SQLException {
        rs.updateNString(columnLabel, nString);
    }

    public void updateNClob(int columnIndex, NClob nClob)
        throws SQLException {
        rs.updateNClob(columnIndex, nClob);
    }

    public void updateNClob(String columnLabel, NClob nClob)
        throws SQLException {
        rs.updateNClob(columnLabel, nClob);
    }

    public NClob getNClob(int columnIndex)
        throws SQLException {
        return rs.getNClob(columnIndex);
    }

    public NClob getNClob(String columnLabel)
        throws SQLException {
        return rs.getNClob(columnLabel);
    }

    public SQLXML getSQLXML(int columnIndex)
        throws SQLException {
        return rs.getSQLXML(columnIndex);
    }

    public SQLXML getSQLXML(String columnLabel)
        throws SQLException {
        return rs.getSQLXML(columnLabel);
    }

    public void updateSQLXML(int columnIndex, SQLXML sqlXml)
        throws SQLException {
        rs.updateSQLXML(columnIndex, sqlXml);
    }

    public void updateSQLXML(String columnLabel, SQLXML sqlXml)
        throws SQLException {
        rs.updateSQLXML(columnLabel, sqlXml);
    }

    public String getNString(int columnIndex)
        throws SQLException {
        return rs.getNString(columnIndex);
    }

    public String getNString(String columnLabel)
        throws SQLException {
        return rs.getNString(columnLabel);
    }

    public Reader getNCharacterStream(int columnIndex)
        throws SQLException {
        return rs.getNCharacterStream(columnIndex);
    }

    public Reader getNCharacterStream(String columnLabel)
        throws SQLException {
        return rs.getNCharacterStream(columnLabel);
    }

    public void updateNCharacterStream(int columnIndex, Reader reader, long length)
        throws SQLException {
        rs.updateNCharacterStream(columnIndex, reader, length);
    }

    public void updateNCharacterStream(String columnLabel, Reader reader, long length)
        throws SQLException {
        rs.updateNCharacterStream(columnLabel, reader, length);
    }

    public void updateAsciiStream(int columnIndex, InputStream inputStream, long length)
        throws SQLException {
        rs.updateAsciiStream(columnIndex, inputStream, length);
    }

    public void updateBinaryStream(int columnIndex, InputStream inputStream, long length)
        throws SQLException {
        rs.updateBinaryStream(columnIndex, inputStream, length);
    }

    public void updateCharacterStream(int columnIndex, Reader reader, long length)
        throws SQLException {
        rs.updateCharacterStream(columnIndex, reader, length);
    }

    public void updateAsciiStream(String columnLabel, InputStream inputStream, long length)
        throws SQLException {
        rs.updateAsciiStream(columnLabel, inputStream, length);
    }

    public void updateBinaryStream(String columnLabel, InputStream inputStream, long length)
        throws SQLException {
        rs.updateBinaryStream(columnLabel, inputStream, length);
    }

    public void updateCharacterStream(String columnLabel, Reader reader, long length)
        throws SQLException {
        rs.updateCharacterStream(columnLabel, reader, length);
    }

    public void updateBlob(int columnIndex, InputStream inputStream, long length)
        throws SQLException {
        rs.updateBlob(columnIndex, inputStream, length);
    }

    public void updateBlob(String columnLabel, InputStream inputStream, long length)
        throws SQLException {
        rs.updateBlob(columnLabel, inputStream, length);
    }

    public void updateClob(int columnIndex, Reader reader, long length)
        throws SQLException {
        rs.updateClob(columnIndex, reader, length);
    }

    public void updateClob(String columnLabel, Reader reader, long length)
        throws SQLException {
        rs.updateClob(columnLabel, reader, length);
    }

    public void updateNClob(int columnIndex, Reader reader, long length)
        throws SQLException {
        rs.updateNClob(columnIndex, reader, length);
    }

    public void updateNClob(String columnLabel, Reader reader, long length)
        throws SQLException {
        rs.updateNClob(columnLabel, reader, length);
    }

    public void updateNCharacterStream(int columnIndex, Reader reader)
        throws SQLException {
        rs.updateNCharacterStream(columnIndex, reader);
    }

    public void updateNCharacterStream(String columnLabel, Reader reader)
        throws SQLException {
        rs.updateNCharacterStream(columnLabel, reader);
    }

    public void updateAsciiStream(int columnIndex, InputStream inputStream)
        throws SQLException {
        rs.updateAsciiStream(columnIndex, inputStream);
    }

    public void updateBinaryStream(int columnIndex, InputStream inputStream)
        throws SQLException {
        rs.updateBinaryStream(columnIndex, inputStream);
    }

    public void updateCharacterStream(int columnIndex, Reader reader)
        throws SQLException {
        rs.updateCharacterStream(columnIndex, reader);
    }

    public void updateAsciiStream(String columnLabel, InputStream inputStream)
        throws SQLException {
        rs.updateAsciiStream(columnLabel, inputStream);
    }

    public void updateBinaryStream(String columnLabel, InputStream inputStream)
        throws SQLException {
        rs.updateBinaryStream(columnLabel, inputStream);
    }

    public void updateCharacterStream(String columnLabel, Reader reader)
        throws SQLException {
        rs.updateCharacterStream(columnLabel, reader);
    }

    public void updateBlob(int columnIndex, InputStream inputStream)
        throws SQLException {
        rs.updateBlob(columnIndex, inputStream);
    }

    public void updateBlob(String columnLabel, InputStream inputStream)
        throws SQLException {
        rs.updateBlob(columnLabel, inputStream);
    }

    public void updateClob(int columnIndex, Reader reader)
        throws SQLException {
        rs.updateClob(columnIndex, reader);
    }

    public void updateClob(String columnLabel, Reader reader)
        throws SQLException {
        rs.updateClob(columnLabel, reader);
    }

    public void updateNClob(int columnIndex, Reader reader)
        throws SQLException {
        rs.updateNClob(columnIndex, reader);
    }

    public void updateNClob(String columnLabel, Reader reader)
        throws SQLException {
        rs.updateNClob(columnLabel, reader);
    }

    public <T> T getObject(int columnIndex,
			   Class<T> type)
    throws SQLException {
        return rs.getObject(columnIndex, type);
    }

    public <T> T getObject(String columnLabel,
			   Class<T> type)
	throws SQLException {
        return rs.getObject(columnLabel, type);
    }

    public <T> T unwrap(Class<T> iface) {
        return (T)iface;
    }

    public boolean isWrapperFor(Class<?> iface) {
        return false;
    }

}
