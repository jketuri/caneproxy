
/* IndexFile.java */

package FI.realitymodeler.sql;

import FI.realitymodeler.common.*;
import java.io.*;
import java.lang.reflect.*;
import java.util.*;
import jdbm.*;
import jdbm.btree.*;
import jdbm.helper.*;
import jdbm.recman.*;

/** IndexFile is a special data file, which holds index keys for searching records
    from ordinary data files. */
public class IndexFile {
    static Field comparatorField;
    static {
        try {
            comparatorField = BTree.class.getDeclaredField("_comparator");
            comparatorField.setAccessible(true);
        } catch (NoSuchFieldException ex) {
            throw new Error(ex.toString());
        }
    }
    public boolean duplicateKeys;
    public boolean hasLength;
    public int keyLength;

    BTree indexTree = null;
    DataFile dataFile = null, blockFile = null;
    DataItem dataItems[] = null;
    IndexComparator indexComparator = null;
    RecordManager recordManager = null;

    public static int keyLength(DataItem dataItems[], boolean hasLength, DataFile blockFile) {
        int keyLength = 0, l = blockFile != null || hasLength ? dataItems.length - 1 : dataItems.length;
        for (int i = 0; i < l; i++) keyLength += dataItems[i].length;
        return keyLength;
    }

    /** Construct a IndexFile from given file.
        For example:
        <pre>
        File f = new File("index.file");
        IndexFile i = IndexFile(f, 4, false);
        </pre>
        These parameters must be given, because constructor must call it's
        super constructor first, and after that can't know if file existed or not
        except when it has been notified of this beforehand.
        Key length can be calculated by hand summing up length
        of all fields in key. You can get the sum also in this way:
        <pre>
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        DataOutputStream dout = new DataOutputStream(bout);
        dout.writeInt(1);
        dout.writeShort(2);
        .
        .
        .
        keyLength = bout.size();
        </pre>
        @param name name.
        @param recordManager record manager.
        @param dataItems data items.
        @param duplicateKeys Flag which indicates if duplicate keys are allowed.
        True means, that they are allowed.
        @param hasLength specifies if last key field has length.
        @param dataFile specifies associated data file.
        @param blockFile specifies associated block file if last key field
        is variable length. */
    public IndexFile(String name, RecordManager recordManager, DataItem dataItems[], boolean duplicateKeys, boolean hasLength, DataFile dataFile, DataFile blockFile)
        throws IOException {
        long recid = recordManager.getNamedObject(name);
        if (recid == 0L) {
            indexComparator = new IndexComparator();
            indexTree = BTree.createInstance(recordManager, indexComparator);
            indexComparator.dataItems = dataItems;
            indexComparator.xcontext = null;
            recordManager.setNamedObject(name, indexTree.getRecid());
            recordManager.commit();
        }
        else
            try {
                indexTree = BTree.load(recordManager, recid);
                indexComparator = (IndexComparator)comparatorField.get(indexTree);
                indexComparator.dataItems = dataItems;
            } catch (IllegalAccessException ex) {
                throw new IOException(Support.stackTrace(ex));
            }
        this.recordManager = recordManager;
        this.dataItems = dataItems;
        this.duplicateKeys = duplicateKeys;
        this.hasLength = hasLength;
        this.dataFile = dataFile;
        this.blockFile = blockFile;
    }

    public IndexFile(String name, RecordManager recordManager, int keyLength, boolean duplicateKeys)
        throws IOException {
        this(name, recordManager, new DataItem[] {new DataItem(0, keyLength)}, duplicateKeys, false, null, null);
    }

    public IndexContext openIndex(boolean writeOnly, DataFile blockFile)
        throws IOException {
        if (this.blockFile != null) blockFile = this.blockFile;
        IndexContext xcontext = new IndexContext();
        indexComparator.xcontext = xcontext;
        xcontext.indexFile = this;
        try {
            if (dataFile != null) xcontext.rawDataFile = dataFile.open();
            if (blockFile != null) xcontext.rawBlockFile = blockFile.open();
            return xcontext;
        } catch (IOException ex) {
            if (xcontext.rawDataFile != null) blockFile.close(xcontext.rawDataFile);
            if (xcontext.rawBlockFile != null) blockFile.close(xcontext.rawBlockFile);
            throw ex;
        }
    }

    public IndexContext openIndex()
        throws IOException {
        return openIndex(false, null);
    }

    public void recover(IndexContext xcontext)
        throws IOException, IllegalArgumentException {
        if (indexTree.size() > 0) return;
        IndexItem xitem = new IndexItem();
        int dataRef, fileLen = dataFile.fileLen();
        for (dataRef = 1; dataRef < fileLen; dataRef++) {
            dataFile.seekStatus(xcontext.rawDataFile, dataRef);
            if (xcontext.rawDataFile.readInt() != 0) continue;
            xitem.dataRef = dataRef;
            if (!addItem(xcontext, xitem)) throw new IllegalArgumentException("Duplicate keys");
        }
        update(xcontext);
    }

    public byte[] buildKey(IndexContext xcontext, DataItem dataItems[], int dataRef)
        throws IOException {
        int blockRef = 0, length, number;
        if (blockFile != null || hasLength) {
            number = dataItems.length - 1;
            dataFile.seekFile(xcontext.rawDataFile, dataRef, dataItems[number].offset);
            blockRef = blockFile != null ? xcontext.rawDataFile.readInt() : 0;
            length = keyLength + xcontext.rawDataFile.readInt();
        } else {
            number = dataItems.length;
            length = keyLength;
        }
        byte key[] = new byte[length];
        length = 0;
        for (int i = 0; i < number; i++) {
            dataFile.seekFile(xcontext.rawDataFile, dataRef, dataItems[i].offset);
            switch (dataItems[i].caseType) {
            case DataItem.OTHER:
                xcontext.rawDataFile.readFully(key, length, dataItems[i].length);
                break;
            case DataItem.CHAR:
                for (int j = 0; j < dataItems[i].length;) {
                    char c = Character.toUpperCase(xcontext.rawDataFile.readChar());
                    key[j++] = (byte)(c >> 8 & 0xff);
                    key[j++] = (byte)(c & 0xff);
                }
                break;
            case DataItem.BINARY:
                for (int j = 0; j < dataItems[i].length;)
                    key[j++] = (byte)Character.toUpperCase((char)xcontext.rawDataFile.readByte());
                break;
            }
            length += dataItems[i].length;
        }
        if (blockFile == null && !hasLength) return key;
        dataFile.seekFile(xcontext.rawDataFile, dataRef, dataItems[dataItems.length - 1].offset + (blockFile != null ? 8 : 4));
        xcontext.rawDataFile.readFully(key, length, Math.min(key.length - length, dataItems[dataItems.length - 1].length));
        length += dataItems[dataItems.length - 1].length;
        while (blockRef != 0) {
            blockFile.seekFile(xcontext.rawBlockFile, blockRef);
            blockRef = xcontext.rawBlockFile.readInt();
            xcontext.rawBlockFile.readFully(key, length, Math.min(key.length - length, blockFile.recordLength - 4));
            length += blockFile.recordLength - 4;
        }
        return key;
    }

    /** Updates index file to reflect changes made by adding and deleting
        keys. This must be called after keys have been added or deleted to
        ensure that changes are visible also after index file has not been
        closed properly. */
    public synchronized void update(IndexContext xcontext)
        throws IOException {
        recordManager.commit();
    }

    /** Closes index file. This must always be called after file is not used anymore. */
    public void close(IndexContext xcontext)
        throws IOException {
        update(xcontext);
        xcontext.close();
    }

    /** Adds an index item to index file.
        @param item An index item to be added. Key and data reference must be set.
        Data reference can be any useful integer, for example data file's record number
        or file position in a text file, when static text query system is in question.
        Key can be build in the following way:
        <pre>
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream o = new DataOutputStream(b);
        o.writeInt(1);
        o.writeShort(2);
        .
        .
        .
        key = b.toByteArray();
        </pre>
        @return Returns true if index item was successfully added. Returns false if
        you tried to add a duplicate key, when such are not allowed.
        @see FI.realitymodeler.sql.IndexItem */
    public synchronized boolean addItem(IndexContext xcontext, IndexItem item)
        throws IOException {
        int dataRef = item.dataRef;
        if (findItem(xcontext, item)) {
            if (item.dataRefs != null) {
                int dataRefs[] = new int[item.dataRefs.length + 1];
                dataRefs[dataRefs.length - 1] = dataRef;
                item.dataRefs = dataRefs;
            } else item.dataRefs = new int[] {dataRef};
        }
        indexTree.insert(item, item, true);
        return true;
    }

    /** Gets next index item from the ascending sequence of index items.
        Before the first call of this method, one of the search methods must
        be called to establish the internal pointer.
        @param item An index item where result will be returned.
        @return Returns true if next item exists. Returns false if there is no
        next item, and if this method is called again, returns the first index item.
        The dataRef is also set to 0, if this method returns false. */
    public boolean nextItem(IndexContext xcontext, IndexItem item)
        throws IOException {
        if (xcontext.tupleBrowser == null) xcontext.tupleBrowser = indexTree.browse();
        else if (item.dataRefs != null)
            if (xcontext.dataRefIndex >= 0) {
                if (xcontext.dataRefIndex < item.dataRefs.length) {
                    item.dataRef = item.dataRefs[xcontext.dataRefIndex++];
                    return true;
                }
            } else xcontext.dataRefIndex = 0;
        if (xcontext.tupleBrowser.getNext(xcontext.tuple)) {
            item.copy(xcontext, (IndexItem)xcontext.tuple.getKey());
            return true;
        }
        return false;
    }

    /** Gets previous index item from the ascending sequence of index items.
        Before the first call of this method, one of the search methods must
        be called to establish the internal pointer.
        @param item An index item where result will be returned.
        @return Returns true if next item exists. Returns false if there is no
        previous item, and if this method is called again, returns the last index item.
        The dataRef is also set to 0, if this method returns false. */
    public boolean prevItem(IndexContext xcontext, IndexItem item)
        throws IOException {
        if (xcontext.tupleBrowser == null) xcontext.tupleBrowser = indexTree.browse(null);
        else if (item.dataRefs != null)
            if (xcontext.dataRefIndex >= 0) {
                item.dataRef = item.dataRefs[xcontext.dataRefIndex--];
                return true;
            } else {
                item.dataRef = xcontext.dataRef;
                xcontext.dataRefIndex = -1;
                return true;
            }
        if (xcontext.tupleBrowser.getPrevious(xcontext.tuple)) {
            item.copy(xcontext, (IndexItem)xcontext.tuple.getKey());
            if (item.dataRefs != null) {
                xcontext.dataRef = item.dataRef;
                xcontext.dataRefIndex = item.dataRefs.length - 2;
                item.dataRef = item.dataRefs[item.dataRefs.length - 1];
            }
            return true;
        }
        return false;
    }

    /** Locates the index item, whose key exactly matches the given key and data reference if this
        differs from zero. If data reference is not specified, result is undetermined. This method
        is useful when setting index pointer before calling nextItem or prevItem -methods.
        @param item An index item, whose key and data reference must be set.
        @return Returns true if an index item was located, otherwise false.
        @see FI.realitymodeler.sql.IndexFile#nextItem
        @see FI.realitymodeler.sql.IndexFile#prevItem */
    public boolean locateItem(IndexContext xcontext, IndexItem item)
        throws IOException {
        xcontext.tupleBrowser = indexTree.browse(item);
        if (xcontext.tupleBrowser == null) return false;
        int dataRef = item.dataRef;
        if (!nextItem(xcontext, item)) return false;
        if (item.dataRef == dataRef) {
            xcontext.dataRefIndex = -1;
            return true;
        }
        if (item.dataRefs == null) return false;
        for (int dataRefIndex = 0; dataRefIndex < item.dataRefs.length; dataRefIndex++)
            if (item.dataRefs[dataRefIndex] == dataRef) {
                xcontext.dataRefIndex = dataRefIndex;
                return true;
            }
        return false;
    }

    /** Locates the index item, whose key exactly matches the given key. If the index file
        contains duplicate keys, this method always locates the first matching index item.
        Following index items can be retrieved using nextItem method.
        @param item An index item, whose key must be set, and where result will be returned.
        Key must be exactly same as the searched one. It's length must be equal, and all it's
        characters, including zeros.
        @return Returns true if an index item was found, otherwise false.
        The dataRef is also set to 0, if this method returns false.
        @see FI.realitymodeler.sql.IndexFile#nextItem
        @see FI.realitymodeler.sql.IndexFile#searchItem
        @see FI.realitymodeler.sql.IndexFile#searchAItem
        @see FI.realitymodeler.sql.IndexFile#searchTheItem */
    public boolean findItem(IndexContext xcontext, IndexItem item)
        throws IOException {
        xcontext.tupleBrowser = indexTree.browse(item);
        if (xcontext.tupleBrowser == null) return false;
        return nextItem(xcontext, item);
    }

    /** Locates the first index item, whose key is equal to or greater than the given key.
        This method can be used when only the first part of the key is known. If the index file
        contains duplicate keys, this method always locates the first matching index item.
        @param item An index item, whose key must be set, and where result will be returned.
        @return Returns true if an index item was found, otherwise false.
        The dataRef is also set to 0, if this method returns false. */
    public boolean searchItem(IndexContext xcontext, IndexItem item)
        throws IOException {
        xcontext.tuple = indexTree.findGreaterOrEqual(item);
        if (xcontext.tuple == null) return false;
        item.copy(xcontext, (IndexItem)xcontext.tuple.getKey());
        xcontext.tupleBrowser = indexTree.browse(item);
        if (xcontext.tupleBrowser == null) return false;
        return nextItem(xcontext, item);
    }

    /** Locates the first index item, whose first part of the key matches the given key.
        This method can be used when only the first part of the key is known. If the index file
        contains duplicate keys, this method always locates the first matching index item.
        Given key must match exactly the first part of the searched one.
        @param item An index item, whose key must be set, and where result will be returned.
        @return Returns true if an index item was found, otherwise false.
        The dataRef is also set to 0, if this method returns false. */
    public boolean searchAItem(IndexContext xcontext, IndexItem item)
        throws IOException {
        IndexItem item1 = new IndexItem();
        item1.copy(xcontext, item);
        return searchItem(xcontext, item) && Support.compare(item.key, item1.key, 0, 0, item1.key.length) == 0;
    }

    /** Locates the first index item, whose non-null part of the key matches the given key.
        If the index file contains duplicate keys, this method always locates the first matching index
        item. Given key must not start with whitespace.
        @param item An index item, whose key must be set, and where result will be returned.
        @return Returns true if an index item was found, otherwise false.
        The dataRef is also set to 0, if this method returns false. */
    public boolean searchTheItem(IndexContext xcontext, IndexItem item)
        throws IOException {
        IndexItem item1 = new IndexItem();
        item1.copy(xcontext, item);
        if (searchItem(xcontext, item)) {
            int n = item.key.length;
            while (n > 0 && item.key[n - 1] == 0) n--;
            return Support.compare(item.key, item1.key, 0, 0, n) == 0;
        }
        return false;
    }

    /** Gets first index item of ascending sequence of index items.
        @param item An index item, where result will be returned.
        @return Returns true if an index item was found, otherwise false.
        The dataRef is also set to 0, if this method returns false. */
    public boolean firstItem(IndexContext xcontext, IndexItem item, IndexContext context)
        throws IOException {
        context.clear();
        return nextItem(xcontext, item);
    }

    /** Gets last index item of ascending sequence of index items.
        @param item An index item, where result will be returned.
        @return Returns true if an index item was found, otherwise false.
        The dataRef is also set to 0, if this method returns false. */
    public boolean lastItem(IndexContext xcontext, IndexItem item, IndexContext context)
        throws IOException {
        context.clear();
        return prevItem(xcontext, item);
    }

    /** Deletes an index item from the index file. Key and data reference must be set.
        @param item An index item, where key and data reference must be set.
        @return Returns true if an index item was found, otherwise false. */
    public synchronized boolean deleteItem(IndexContext xcontext, IndexItem item)
        throws IOException {
        if (!locateItem(xcontext, item)) return false;
        int dataRefIndex1;
        if (xcontext.dataRefIndex == -1) {
            if (item.dataRefs == null) return indexTree.remove(item) != null;
            item.dataRef = item.dataRefs[0];
            dataRefIndex1 = 0;
        } else dataRefIndex1 = xcontext.dataRefIndex;
        int dataRefs[] = new int[item.dataRefs.length - 1];
        for (int dataRefIndex = 0; dataRefIndex < dataRefIndex1; dataRefIndex++)
            dataRefs[dataRefIndex] = item.dataRefs[dataRefIndex];
        for (int dataRefIndex = dataRefIndex1; dataRefIndex < item.dataRefs.length - 1; dataRefIndex++)
            dataRefs[dataRefIndex] = item.dataRefs[dataRefIndex + 1];
        item.dataRefs = dataRefs;
        indexTree.insert(item, item, true);
        return true;
    }

}
