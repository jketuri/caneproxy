
package FI.realitymodeler.sql;

import java.rmi.*;
import java.sql.*;

public class BridgeResultSetMetaData implements ResultSetMetaData {
    RemoteResultSetMetaData rrsmd;

    public BridgeResultSetMetaData(RemoteResultSetMetaData rrsmd) {
        this.rrsmd = rrsmd;
    }

    public int getColumnCount()
        throws SQLException {
        try { return rrsmd.getColumnCount(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean isAutoIncrement(int columnIndex)
        throws SQLException {
        try { return rrsmd.isAutoIncrement(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean isCaseSensitive(int columnIndex)
        throws SQLException {
        try { return rrsmd.isCaseSensitive(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean isSearchable(int columnIndex)
        throws SQLException {
        try { return rrsmd.isSearchable(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean isCurrency(int columnIndex)
        throws SQLException {
        try { return rrsmd.isCurrency(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int isNullable(int columnIndex)
        throws SQLException {
        try { return rrsmd.isNullable(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean isSigned(int columnIndex)
        throws SQLException {
        try { return rrsmd.isSigned(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getColumnDisplaySize(int columnIndex)
        throws SQLException {
        try { return rrsmd.getColumnDisplaySize(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String getColumnLabel(int columnIndex)
        throws SQLException {
        try { return rrsmd.getColumnLabel(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String getColumnName(int columnIndex)
        throws SQLException {
        try { return rrsmd.getColumnName(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String getSchemaName(int columnIndex)
        throws SQLException {
        try { return rrsmd.getSchemaName(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getPrecision(int columnIndex)
        throws SQLException {
        try { return rrsmd.getPrecision(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getScale(int columnIndex)
        throws SQLException {
        try { return rrsmd.getScale(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String getTableName(int columnIndex)
        throws SQLException {
        try { return rrsmd.getTableName(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String getCatalogName(int columnIndex)
        throws SQLException {
        try { return rrsmd.getCatalogName(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getColumnType(int columnIndex)
        throws SQLException {
        try { return rrsmd.getColumnType(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String getColumnTypeName(int columnIndex)
        throws SQLException {
        try { return rrsmd.getColumnTypeName(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean isReadOnly(int columnIndex)
        throws SQLException {
        try { return rrsmd.isReadOnly(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean isWritable(int columnIndex)
        throws SQLException {
        try { return rrsmd.isWritable(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean isDefinitelyWritable(int columnIndex)
        throws SQLException {
        try { return rrsmd.isDefinitelyWritable(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String getColumnClassName(int columnIndex)
        throws SQLException {
        try { return rrsmd.getColumnClassName(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public <T> T unwrap(Class<T> iface) {
        return (T)iface;
    }

    public boolean isWrapperFor(Class<?> iface) {
        return false;
    }

}
