
package FI.realitymodeler.sql;

import java.io.*;
import java.math.*;
import java.net.*;
import java.rmi.*;
import java.sql.*;
import java.util.*;

public class BridgeStatement implements Statement, PreparedStatement, CallableStatement, Runnable {
    private RemoteStatement rs;
    private int parameterIndex, type;
    private TransferStream transferStream;

    public BridgeStatement(RemoteStatement rs) {
        this.rs = rs;
    }

    public ResultSet executeQuery(String sql)
        throws SQLException {
        try { return new BridgeResultSet(rs.executeQuery(sql)); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int executeUpdate(String sql)
        throws SQLException {
        try { return rs.executeUpdate(sql); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void close()
        throws SQLException {
        try { rs.close(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getMaxFieldSize()
        throws SQLException {
        try { return rs.getMaxFieldSize(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setMaxFieldSize(int max)
        throws SQLException {
        try { rs.setMaxFieldSize(max); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getMaxRows()
        throws SQLException {
        try { return rs.getMaxRows(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setMaxRows(int max)
        throws SQLException {
        try { rs.setMaxRows(max); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setEscapeProcessing(boolean enable)
        throws SQLException {
        try { rs.setEscapeProcessing(enable); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getQueryTimeout()
        throws SQLException {
        try { return rs.getQueryTimeout(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setQueryTimeout(int seconds)
        throws SQLException {
        try { rs.setQueryTimeout(seconds); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void cancel()
        throws SQLException {
        try { rs.cancel(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public SQLWarning getWarnings()
        throws SQLException {
        try { return rs.getWarnings(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void clearWarnings()
        throws SQLException {
        try { rs.clearWarnings(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setCursorName(String name)
        throws SQLException {
        try { rs.setCursorName(name); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean execute(String sql)
        throws SQLException {
        try { return rs.execute(sql); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public ResultSet getResultSet()
        throws SQLException {
        try { return new BridgeResultSet(rs.getResultSet()); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getUpdateCount()
        throws SQLException {
        try { return rs.getUpdateCount(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean getMoreResults()
        throws SQLException {
        try { return rs.getMoreResults(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setFetchDirection(int direction)
        throws SQLException {
        try { rs.setFetchDirection(direction); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getFetchDirection()
        throws SQLException {
        try { return rs.getFetchDirection(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setFetchSize(int rows)
        throws SQLException {
        try { rs.setFetchSize(rows); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getFetchSize()
        throws SQLException {
        try { return rs.getFetchSize(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getResultSetConcurrency()
        throws SQLException {
        try { return rs.getResultSetConcurrency(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getResultSetType()
        throws SQLException {
        try { return rs.getResultSetType(); } catch (RemoteException ex) { return 0; }
    }

    public void addBatch(String sql)
        throws SQLException {
        try { rs.addBatch(sql); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void clearBatch()
        throws SQLException {
        try { rs.clearBatch(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int[] executeBatch()
        throws SQLException {
        try { return rs.executeBatch(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Connection getConnection()
        throws SQLException {
        try { return new BridgeConnection(rs.getConnection()); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean getMoreResults(int current)
        throws SQLException {
        try { return rs.getMoreResults(current); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public ResultSet getGeneratedKeys()
        throws SQLException {
        try { return new BridgeResultSet(rs.getGeneratedKeys()); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int executeUpdate(String sql, int autoGeneratedKeys)
        throws SQLException {
        try { return rs.executeUpdate(sql, autoGeneratedKeys); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int executeUpdate(String sql, int columnIndexes[])
        throws SQLException {
        try { return rs.executeUpdate(sql, columnIndexes); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int executeUpdate(String sql, String columnNames[])
        throws SQLException {
        try { return rs.executeUpdate(sql, columnNames); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean execute(String sql, int autoGeneratedKeys)
        throws SQLException {
        try { return rs.execute(sql, autoGeneratedKeys); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean execute(String sql, int columnIndexes[])
        throws SQLException {
        try { return rs.execute(sql, columnIndexes); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean execute(String sql, String columnNames[])
        throws SQLException {
        try { return rs.execute(sql, columnNames); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getResultSetHoldability()
        throws SQLException {
        try { return rs.getResultSetHoldability(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean isClosed()
        throws SQLException {
        try { return rs.isClosed(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setPoolable(boolean poolable)
        throws SQLException {
        try { rs.setPoolable(poolable); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean isPoolable()
        throws SQLException {
        try { return rs.isPoolable(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void closeOnCompletion()
	throws SQLException {
        try { rs.closeOnCompletion(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean isCloseOnCompletion()
	throws SQLException {
	try { return rs.isCloseOnCompletion(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public ResultSet executeQuery()
        throws SQLException {
        try { return new BridgeResultSet(rs.executeQuery()); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int executeUpdate()
        throws SQLException {
        try { return rs.executeUpdate(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setNull(int parameterIndex, int sqlType)
        throws SQLException {
        try { rs.setNull(parameterIndex, sqlType); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setBoolean(int parameterIndex, boolean x)
        throws SQLException {
        try { rs.setBoolean(parameterIndex, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setByte(int parameterIndex, byte x)
        throws SQLException {
        try { rs.setByte(parameterIndex, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setShort(int parameterIndex, short x)
        throws SQLException {
        try { rs.setShort(parameterIndex, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setInt(int parameterIndex, int x)
        throws SQLException {
        try { rs.setInt(parameterIndex, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setLong(int parameterIndex, long x)
        throws SQLException {
        try { rs.setLong(parameterIndex, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setFloat(int parameterIndex, float x)
        throws SQLException {
        try { rs.setFloat(parameterIndex, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setDouble(int parameterIndex, double x)
        throws SQLException {
        try { rs.setDouble(parameterIndex, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setBigDecimal(int parameterIndex, BigDecimal x)
        throws SQLException {
        try { rs.setBigDecimal(parameterIndex, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setString(int parameterIndex, String x)
        throws SQLException {
        try { rs.setString(parameterIndex, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setBytes(int parameterIndex, byte x[])
        throws SQLException {
        try { rs.setBytes(parameterIndex, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setDate(int parameterIndex, java.sql.Date x)
        throws SQLException {
        try { rs.setDate(parameterIndex, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setTime(int parameterIndex, java.sql.Time x)  throws SQLException {
        try { rs.setTime(parameterIndex, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setTimestamp(int parameterIndex, Timestamp x)
        throws SQLException {
        try { rs.setTimestamp(parameterIndex, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void run() {
        try { rs.setStream(transferStream); } catch (RemoteException ex) {};
    }

    private void setStream(int parameterIndex, java.io.InputStream x, int length, boolean ascii)
        throws SQLException {
        try {
            RemoteStream remoteStream = rs.newRemoteStream();
            this.parameterIndex = parameterIndex;
            this.type = type;
            this.transferStream = new TransferStream(remoteStream, x, length);
            rs.setRemoteStream(parameterIndex, length, ascii, remoteStream);
            new Thread(this).start();
        } catch (Exception ex) { throw new SQLException(ex.toString()); }
    }

    public void setAsciiStream(int parameterIndex, java.io.InputStream x, int length)
        throws SQLException {
        setStream(parameterIndex, x, length, true);
    }

    @Deprecated
    public void setUnicodeStream(int parameterIndex, java.io.InputStream x, int length)
        throws SQLException {
        setStream(parameterIndex, x, length, false);
    }

    public void setBinaryStream(int parameterIndex, java.io.InputStream x, int length)  throws SQLException {
        setStream(parameterIndex, x, length, false);
    }

    public void clearParameters()
        throws SQLException {
        try { rs.clearParameters(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setObject(int parameterIndex, Object x, int targetSqlType, int scaleOrLength)
        throws SQLException {
        try { rs.setObject(parameterIndex, x, targetSqlType, scaleOrLength); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setObject(int parameterIndex, Object x, int targetSqlType)
        throws SQLException {
        try { rs.setObject(parameterIndex, x, targetSqlType); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setObject(int parameterIndex, Object x)
        throws SQLException {
        try { rs.setObject(parameterIndex, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean execute()
        throws SQLException {
        try { return rs.execute(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void addBatch()
        throws SQLException {
        try { rs.addBatch(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setCharacterStream(int parameterIndex, Reader reader, int length)
        throws SQLException {
        try { rs.setCharacterStream(parameterIndex, reader, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setRef(int i, Ref x)
        throws SQLException {
        try { rs.setRef(i, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setBlob(int i, Blob x)
        throws SQLException {
        try { rs.setBlob(i, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setClob(int i, Clob x)
        throws SQLException {
        try { rs.setClob(i, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setArray(int i, Array x)
        throws SQLException {
        try { rs.setArray(i, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public ResultSetMetaData getMetaData()
        throws SQLException {
        try { return new BridgeResultSetMetaData(rs.getMetaData()); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setDate(int parameterIndex, java.sql.Date x, Calendar cal)
        throws SQLException {
        try { rs.setDate(parameterIndex, x, cal); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setTime(int parameterIndex, java.sql.Time x, Calendar cal)
        throws SQLException {
        try { rs.setTime(parameterIndex, x, cal); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setTimestamp(int parameterIndex, Timestamp x, Calendar cal)
        throws SQLException {
        try { rs.setTimestamp(parameterIndex, x, cal); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setNull(int parameterIndex, int sqlType, String typeName)
        throws SQLException {
        try { rs.setNull(parameterIndex, sqlType, typeName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setURL(int parameterIndex, URL x)
        throws SQLException {
        try { rs.setURL(parameterIndex, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public ParameterMetaData getParameterMetaData()
        throws SQLException {
        try { return rs.getParameterMetaData(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setRowId(int parameterIndex, RowId rowId)
        throws SQLException {
        try { rs.setRowId(parameterIndex, rowId); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setNString(int parameterIndex, String value)
        throws SQLException {
        try { rs.setNString(parameterIndex, value); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setNCharacterStream(int parameterIndex, Reader value, long length)
        throws SQLException {
        try { rs.setNCharacterStream(parameterIndex, value, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setNClob(int parameterIndex, NClob value)
        throws SQLException {
        try { rs.setNClob(parameterIndex, value); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setClob(int parameterIndex, Reader reader, long length)
        throws SQLException {
        try { rs.setClob(parameterIndex, reader, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setBlob(int parameterIndex, InputStream inputStream, long length)
        throws SQLException {
        try { rs.setBlob(parameterIndex, inputStream, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setNClob(int parameterIndex, Reader reader, long length)
        throws SQLException {
        try { rs.setNClob(parameterIndex, reader, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setSQLXML(int parameterIndex, SQLXML sqlXml)
        throws SQLException {
        try { rs.setSQLXML(parameterIndex, sqlXml); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setAsciiStream(int parameterIndex, InputStream inputStream, long length)
        throws SQLException {
        try { rs.setAsciiStream(parameterIndex, inputStream, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setBinaryStream(int parameterIndex, InputStream inputStream, long length)
        throws SQLException {
        try { rs.setBinaryStream(parameterIndex, inputStream, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setCharacterStream(int parameterIndex, Reader reader, long length)
        throws SQLException {
        try { rs.setCharacterStream(parameterIndex, reader, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setAsciiStream(int parameterIndex, InputStream inputStream)
        throws SQLException {
        try { rs.setAsciiStream(parameterIndex, inputStream); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setBinaryStream(int parameterIndex, InputStream inputStream)
        throws SQLException {
        try { rs.setBinaryStream(parameterIndex, inputStream); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setCharacterStream(int parameterIndex, Reader reader)
        throws SQLException {
        try { rs.setCharacterStream(parameterIndex, reader); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setNCharacterStream(int parameterIndex, Reader reader)
        throws SQLException {
        try { rs.setNCharacterStream(parameterIndex, reader); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setClob(int parameterIndex, Reader reader)
        throws SQLException {
        try { rs.setClob(parameterIndex, reader); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setBlob(int parameterIndex, InputStream inputStream)
        throws SQLException {
        try { rs.setBlob(parameterIndex, inputStream); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setNClob(int parameterIndex, Reader reader)
        throws SQLException {
        try { rs.setNClob(parameterIndex, reader); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public <T> T getObject(int parameterIndex,
			   Class<T> type)
	throws SQLException {
        try { return rs.getObject(parameterIndex, type); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public <T> T getObject(String parameterName,
			   Class<T> type)
	throws SQLException {
        try { return rs.getObject(parameterName, type); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void registerOutParameter(int parameterIndex, int sqlType)
        throws SQLException {
        try { rs.registerOutParameter(parameterIndex, sqlType); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void registerOutParameter(int parameterIndex, int sqlType, int scale)
        throws SQLException {
        try { rs.registerOutParameter(parameterIndex, sqlType, scale); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean wasNull()
        throws SQLException {
        try { return rs.wasNull(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String getString(int parameterIndex)
        throws SQLException {
        try { return rs.getString(parameterIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean getBoolean(int parameterIndex)
        throws SQLException {
        try { return rs.getBoolean(parameterIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public byte getByte(int parameterIndex)
        throws SQLException {
        try { return rs.getByte(parameterIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public short getShort(int parameterIndex)
        throws SQLException {
        try { return rs.getShort(parameterIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getInt(int parameterIndex)
        throws SQLException {
        try { return rs.getInt(parameterIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public long getLong(int parameterIndex)
        throws SQLException {
        try { return rs.getLong(parameterIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public float getFloat(int parameterIndex)
        throws SQLException {
        try { return rs.getFloat(parameterIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public double getDouble(int parameterIndex)
        throws SQLException {
        try { return rs.getDouble(parameterIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    @Deprecated
    public BigDecimal getBigDecimal(int parameterIndex, int scale)
        throws SQLException {
        try { return rs.getBigDecimal(parameterIndex, scale); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public byte[] getBytes(int parameterIndex)
        throws SQLException {
        try { return rs.getBytes(parameterIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public java.sql.Date getDate(int parameterIndex)
        throws SQLException {
        try { return rs.getDate(parameterIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public java.sql.Time getTime(int parameterIndex)
        throws SQLException {
        try { return rs.getTime(parameterIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Timestamp getTimestamp(int parameterIndex)
        throws SQLException {
        try { return rs.getTimestamp(parameterIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Object getObject(int parameterIndex)
        throws SQLException {
        try { return rs.getObject(parameterIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public BigDecimal getBigDecimal(int parameterIndex)
        throws SQLException {
        try { return rs.getBigDecimal(parameterIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Object getObject(int i, Map<String,Class<?>> map)
        throws SQLException {
        try { return rs.getObject(i, map); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Ref getRef(int i)
        throws SQLException {
        try { return rs.getRef(i); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Blob getBlob(int i)
        throws SQLException {
        try { return rs.getBlob(i); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Clob getClob(int i)
        throws SQLException {
        try { return rs.getClob(i); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Array getArray(int i)
        throws SQLException {
        try { return rs.getArray(i); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public java.sql.Date getDate(int parameterIndex, Calendar cal)
        throws SQLException {
        try { return rs.getDate(parameterIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public java.sql.Time getTime(int parameterIndex, Calendar cal)
        throws SQLException {
        try { return rs.getTime(parameterIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Timestamp getTimestamp(int parameterIndex, Calendar cal)
        throws SQLException {
        try { return rs.getTimestamp(parameterIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void registerOutParameter(int parameterIndex, int sqlType, String typeName)
        throws SQLException {
        try { rs.registerOutParameter(parameterIndex, sqlType, typeName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void registerOutParameter(String parameterName, int sqlType)
        throws SQLException {
        try { rs.registerOutParameter(parameterName, sqlType); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void registerOutParameter(String parameterName, int sqlType, int scale)
        throws SQLException {
        try { rs.registerOutParameter(parameterName, sqlType, scale); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void registerOutParameter(String parameterName, int sqlType, String typeName)
        throws SQLException {
        try { rs.registerOutParameter(parameterName, sqlType, typeName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public URL getURL(int parameterIndex)
        throws SQLException {
        try { return rs.getURL(parameterIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setURL(String parameterName, URL val)
        throws SQLException {
        try { rs.setURL(parameterName, val); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setNull(String parameterName, int sqlType)
        throws SQLException {
        try { rs.setNull(parameterName, sqlType); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setBoolean(String parameterName, boolean x)
        throws SQLException {
        try { rs.setBoolean(parameterName, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setByte(String parameterName, byte x)
        throws SQLException {
        try { rs.setByte(parameterName, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setShort(String parameterName, short x)
        throws SQLException {
        try { rs.setShort(parameterName, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setInt(String parameterName, int x)
        throws SQLException {
        try { rs.setInt(parameterName, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setLong(String parameterName, long x)
        throws SQLException {
        try { rs.setLong(parameterName, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setFloat(String parameterName, float x)
        throws SQLException {
        try { rs.setFloat(parameterName, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setDouble(String parameterName, double x)
        throws SQLException {
        try { rs.setDouble(parameterName, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setBigDecimal(String parameterName, BigDecimal x)
        throws SQLException {
        try { rs.setBigDecimal(parameterName, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setString(String parameterName, String x)
        throws SQLException {
        try { rs.setString(parameterName, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setBytes(String parameterName, byte x[])
        throws SQLException {
        try { rs.setBytes(parameterName, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setDate(String parameterName, java.sql.Date x)
        throws SQLException {
        try { rs.setDate(parameterName, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setTime(String parameterName, Time x)
        throws SQLException {
        try { rs.setTime(parameterName, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setTimestamp(String parameterName, Timestamp x)
        throws SQLException {
        try { rs.setTimestamp(parameterName, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setAsciiStream(String parameterName, InputStream x, int length)
        throws SQLException {
        try { rs.setAsciiStream(parameterName, x, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setBinaryStream(String parameterName, InputStream x, int length)
        throws SQLException {
        try { rs.setBinaryStream(parameterName, x, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setObject(String parameterName, Object x, int targetSqlType, int scale)
        throws SQLException {
        try { rs.setObject(parameterName, x, targetSqlType, scale); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setObject(String parameterName, Object x, int targetSqlType)
        throws SQLException {
        try { rs.setObject(parameterName, x, targetSqlType); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setObject(String parameterName, Object x)
        throws SQLException {
        try { rs.setObject(parameterName, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setCharacterStream(String parameterName, Reader reader, int length)
        throws SQLException {
        try { rs.setCharacterStream(parameterName, reader, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setDate(String parameterName, java.sql.Date x, Calendar cal)
        throws SQLException {
        try { rs.setDate(parameterName, x, cal); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setTime(String parameterName, Time x, Calendar cal)
        throws SQLException {
        try { rs.setTime(parameterName, x, cal); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setTimestamp(String parameterName, Timestamp x, Calendar cal)
        throws SQLException {
        try { rs.setTimestamp(parameterName, x, cal); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setNull(String parameterName, int sqlType, String typeName)
        throws SQLException {
        try { rs.setNull(parameterName, sqlType, typeName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String getString(String parameterName)
        throws SQLException {
        try { return rs.getString(parameterName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean getBoolean(String parameterName)
        throws SQLException {
        try { return rs.getBoolean(parameterName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public byte getByte(String parameterName)
        throws SQLException {
        try { return rs.getByte(parameterName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public short getShort(String parameterName)
        throws SQLException {
        try { return rs.getShort(parameterName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getInt(String parameterName)
        throws SQLException {
        try { return rs.getInt(parameterName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public long getLong(String parameterName)
        throws SQLException {
        try { return rs.getLong(parameterName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public float getFloat(String parameterName)
        throws SQLException {
        try { return rs.getFloat(parameterName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public double getDouble(String parameterName)
        throws SQLException {
        try { return rs.getDouble(parameterName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public byte[] getBytes(String parameterName)
        throws SQLException {
        try { return rs.getBytes(parameterName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public java.sql.Date getDate(String parameterName)
        throws SQLException {
        try { return rs.getDate(parameterName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Time getTime(String parameterName)
        throws SQLException {
        try { return rs.getTime(parameterName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Timestamp getTimestamp(String parameterName)
        throws SQLException {
        try { return rs.getTimestamp(parameterName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Object getObject(String parameterName)
        throws SQLException {
        try { return rs.getObject(parameterName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public BigDecimal getBigDecimal(String parameterName)
        throws SQLException {
        try { return rs.getBigDecimal(parameterName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Object getObject(String parameterName, Map<String,Class<?>> map)
        throws SQLException {
        try { return rs.getObject(parameterName, map); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Ref getRef(String parameterName)
        throws SQLException {
        try { return rs.getRef(parameterName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Blob getBlob(String parameterName)
        throws SQLException {
        try { return rs.getBlob(parameterName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Clob getClob(String parameterName)
        throws SQLException {
        try { return rs.getClob(parameterName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Array getArray(String parameterName)
        throws SQLException {
        try { return rs.getArray(parameterName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public java.sql.Date getDate(String parameterName, Calendar cal)
        throws SQLException {
        try { return rs.getDate(parameterName, cal); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Time getTime(String parameterName, Calendar cal)
        throws SQLException {
        try { return rs.getTime(parameterName, cal); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Timestamp getTimestamp(String parameterName, Calendar cal)
        throws SQLException {
        try { return rs.getTimestamp(parameterName, cal); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public URL getURL(String parameterName)
        throws SQLException {
        try { return rs.getURL(parameterName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public RowId getRowId(int parameterIndex)
        throws SQLException {
        try { return rs.getRowId(parameterIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public RowId getRowId(String parameterName)
        throws SQLException {
        try { return rs.getRowId(parameterName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setRowId(String parameterName, RowId rowId)
        throws SQLException {
        try { rs.setRowId(parameterName, rowId); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setNString(String parameterName, String value)
        throws SQLException {
        try { rs.setNString(parameterName, value); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setNCharacterStream(String parameterName, Reader reader, long length)
        throws SQLException {
        try { rs.setNCharacterStream(parameterName, reader, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setNClob(String parameterName, NClob nclob)
        throws SQLException {
        try { rs.setNClob(parameterName, nclob); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setClob(String parameterName, Reader reader, long length)
        throws SQLException {
        try { rs.setClob(parameterName, reader, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setBlob(String parameterName, InputStream inputStream, long length)
        throws SQLException {
        try { rs.setBlob(parameterName, inputStream, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setNClob(String parameterName, Reader reader, long length)
        throws SQLException {
        try { rs.setNClob(parameterName, reader, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public NClob getNClob(int parameterIndex)
        throws SQLException {
        try { return rs.getNClob(parameterIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public NClob getNClob(String parameterName)
        throws SQLException {
        try { return rs.getNClob(parameterName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setSQLXML(String parameterName, SQLXML sqlXml)
        throws SQLException {
        try { rs.setSQLXML(parameterName, sqlXml); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public SQLXML getSQLXML(int parameterIndex)
        throws SQLException {
        try { return rs.getSQLXML(parameterIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public SQLXML getSQLXML(String parameterName)
        throws SQLException {
        try { return rs.getSQLXML(parameterName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String getNString(int parameterIndex)
        throws SQLException {
        try { return rs.getNString(parameterIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String getNString(String parameterName)
        throws SQLException {
        try { return rs.getNString(parameterName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Reader getNCharacterStream(int parameterIndex)
        throws SQLException {
        try { return rs.getNCharacterStream(parameterIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Reader getNCharacterStream(String parameterName)
        throws SQLException {
        try { return rs.getNCharacterStream(parameterName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Reader getCharacterStream(int parameterIndex)
        throws SQLException {
        try { return rs.getCharacterStream(parameterIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Reader getCharacterStream(String parameterName)
        throws SQLException {
        try { return rs.getCharacterStream(parameterName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setBlob(String parameterName, Blob blob)
        throws SQLException {
        try { rs.setBlob(parameterName, blob); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setClob(String parameterName, Clob clob)
        throws SQLException {
        try { rs.setClob(parameterName, clob); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setAsciiStream(String parameterName, InputStream inputStream, long length)
        throws SQLException {
        try { rs.setAsciiStream(parameterName, inputStream, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setBinaryStream(String parameterName, InputStream inputStream, long length)
        throws SQLException {
        try { rs.setBinaryStream(parameterName, inputStream, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setCharacterStream(String parameterName, Reader reader, long length)
        throws SQLException {
        try { rs.setCharacterStream(parameterName, reader, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setAsciiStream(String parameterName, InputStream inputStream)
        throws SQLException {
        try { rs.setAsciiStream(parameterName, inputStream); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setBinaryStream(String parameterName, InputStream inputStream)
        throws SQLException {
        try { rs.setBinaryStream(parameterName, inputStream); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setCharacterStream(String parameterName, Reader reader)
        throws SQLException {
        try { rs.setCharacterStream(parameterName, reader); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setNCharacterStream(String parameterName, Reader reader)
        throws SQLException {
        try { rs.setNCharacterStream(parameterName, reader); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setClob(String parameterName, Reader reader)
        throws SQLException {
        try { rs.setClob(parameterName, reader); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setBlob(String parameterName, InputStream inputStream)
        throws SQLException {
        try { rs.setBlob(parameterName, inputStream); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setNClob(String parameterName, Reader reader)
        throws SQLException {
        try { rs.setNClob(parameterName, reader); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public <T> T unwrap(Class<T> iface) {
        return (T)iface;
    }

    public boolean isWrapperFor(Class<?> iface) {
        return false;
    }

}
