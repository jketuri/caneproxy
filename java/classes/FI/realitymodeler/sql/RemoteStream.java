
package FI.realitymodeler.sql;

import java.rmi.*;

public interface RemoteStream extends Remote {

    void setInstance()
        throws RemoteException;

}
