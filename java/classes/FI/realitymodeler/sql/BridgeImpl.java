
package FI.realitymodeler.sql;

import java.rmi.*;
import java.rmi.registry.*;
import java.rmi.server.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;

/** Configures data sources for public use through registry. Data
    sources can be configured as servlets. */

public class BridgeImpl extends UnicastRemoteObject implements Bridge, Servlet {
    static final long serialVersionUID = 0L;
    static {
        try {
            Class.forName("jdbc.odbc.JdbcOdbcDriver");
        } catch (ClassNotFoundException ex) {};
    }

    private String name = null, url = null;
    private ServletConfig config;

    public BridgeImpl(String host, String name, String url)
        throws Exception {
        this.name = name;
        this.url = url;
        begin(host);
    }

    private void begin(String host)
        throws Exception {
        Naming.rebind(name = "rmi://" + (host != null ? host : "localhost:" + Registry.REGISTRY_PORT) + "/" + getClass().getName() + "/" + name, this);
    }

    /** Initializes data source as servlet to be used through BridgeDriver. 

        Initialization parameters are:<p>

        name=name to be used as data source name in BridgeDriver's
        connect-method.<p>

        url=url which is used to actually connect to specified data source in
        host machine. */

    public void init(ServletConfig config)
        throws ServletException {
        this.config = config;
        String host = config.getInitParameter("host");
        name = config.getInitParameter("name");
        url = config.getInitParameter("url");
        if (name == null || url == null) throw new ServletException("Parameters missing");
        try {
            begin(host);
        } catch (Exception ex) {
            throw new ServletException(ex.toString());
        }
    }

    public ServletConfig getServletConfig() {
        return config;
    }

    public void service(ServletRequest req, ServletResponse res) {
    }

    public String getServletInfo() {
        return getClass().getName();
    }

    public void destroy() {
        try {
            Naming.unbind(name);
        } catch (Exception ex) {}
    }

    public RemoteConnection getConnection()
        throws RemoteException, SQLException {
        return new RemoteConnectionImpl(DriverManager.getConnection(url));
    }

}
