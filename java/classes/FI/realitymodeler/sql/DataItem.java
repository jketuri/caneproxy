
package FI.realitymodeler.sql;

public class DataItem {
    static final int OTHER = 0, CHAR = 1, BINARY = 2;

    int offset;
    int length;
    byte caseType;

    public DataItem(int offset, int length, int caseType) {
        this.offset = offset;
        this.length = length;
        this.caseType = (byte)caseType;
    }

    public DataItem(int offset, int length) {
        this(offset, length, OTHER);
    }

    public String toString() {
        return getClass().getName() + "{offset=" + offset + ",length=" + length + ",caseType=" + caseType + "}";
    }

}