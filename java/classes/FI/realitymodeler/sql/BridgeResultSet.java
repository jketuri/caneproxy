
package FI.realitymodeler.sql;

import java.io.*;
import java.math.*;
import java.net.*;
import java.rmi.*;
import java.sql.*;
import java.util.*;

public class BridgeResultSet implements ResultSet, Runnable {
    private RemoteResultSet rrs;
    private int columnIndex;
    private boolean ascii;

    public BridgeResultSet(RemoteResultSet rrs) {
        this.rrs = rrs;
    }

    public boolean next()
        throws SQLException {
        try { return rrs.next(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void close()
        throws SQLException {
        try { rrs.close(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean wasNull()
        throws SQLException {
        try { return rrs.wasNull(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String getString(int columnIndex)
        throws SQLException {
        try { return rrs.getString(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean getBoolean(int columnIndex)
        throws SQLException {
        try { return rrs.getBoolean(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public byte getByte(int columnIndex)
        throws SQLException {
        try { return rrs.getByte(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public short getShort(int columnIndex)
        throws SQLException {
        try { return rrs.getShort(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getInt(int columnIndex)
        throws SQLException {
        try { return rrs.getInt(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public long getLong(int columnIndex)
        throws SQLException {
        try { return rrs.getLong(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public float getFloat(int columnIndex)
        throws SQLException {
        try { return rrs.getFloat(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public double getDouble(int columnIndex)
        throws SQLException {
        try { return rrs.getDouble(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    @Deprecated
    public BigDecimal getBigDecimal(int columnIndex, int scale)
        throws SQLException {
        try { return rrs.getBigDecimal(columnIndex, scale); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public byte[] getBytes(int columnIndex)
        throws SQLException {
        try { return rrs.getBytes(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public java.sql.Date getDate(int columnIndex)
        throws SQLException {
        try { return rrs.getDate(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public java.sql.Time getTime(int columnIndex)
        throws SQLException {
        try { return rrs.getTime(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public java.sql.Timestamp getTimestamp(int columnIndex)
        throws SQLException {
        try { return rrs.getTimestamp(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public URL getURL(int columnIndex)
        throws SQLException {
        try { return rrs.getURL(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void run() {
        try { rrs.getStream(columnIndex, ascii); } catch (Exception ex) {};
    }

    private java.io.InputStream getStream(int columnIndex, boolean ascii)
        throws SQLException {
        LocalStream ls;
        try {
            TransferStream.lock.lock();
            TransferStream.ls = ls = new LocalStream();
            this.columnIndex = columnIndex;
            this.ascii = ascii;
            new Thread(this).start();
            return ls;
        } catch (Exception ex) { throw new SQLException(ex.toString()); }
    }

    public java.io.InputStream getAsciiStream(int columnIndex)
        throws SQLException {
        return getStream(columnIndex, true);
    }

    @Deprecated
    public java.io.InputStream getUnicodeStream(int columnIndex)
        throws SQLException {
        return getStream(columnIndex, false);
    }

    public java.io.InputStream getBinaryStream(int columnIndex)
        throws SQLException {
        return getStream(columnIndex, false);
    }

    public String getString(String columnName)
        throws SQLException {
        try { return rrs.getString(columnName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean getBoolean(String columnName)
        throws SQLException {
        try { return rrs.getBoolean(columnName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public byte getByte(String columnName)
        throws SQLException {
        try { return rrs.getByte(columnName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public short getShort(String columnName)
        throws SQLException {
        try { return rrs.getShort(columnName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getInt(String columnName)
        throws SQLException {
        try { return rrs.getInt(columnName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public long getLong(String columnName)
        throws SQLException {
        try { return rrs.getLong(columnName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public float getFloat(String columnName)
        throws SQLException {
        try { return rrs.getFloat(columnName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public double getDouble(String columnName)
        throws SQLException {
        try { return rrs.getDouble(columnName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    @Deprecated
    public BigDecimal getBigDecimal(String columnName, int scale)
        throws SQLException {
        try { return rrs.getBigDecimal(columnName, scale); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public byte[] getBytes(String columnName)
        throws SQLException {
        try { return rrs.getBytes(columnName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public java.sql.Date getDate(String columnName)
        throws SQLException {
        try { return rrs.getDate(columnName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public java.sql.Time getTime(String columnName)
        throws SQLException {
        try { return rrs.getTime(columnName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public java.sql.Timestamp getTimestamp(String columnName)
        throws SQLException {
        try { return rrs.getTimestamp(columnName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public URL getURL(String columnName)
        throws SQLException {
        try { return rrs.getURL(columnName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public java.io.InputStream getAsciiStream(String columnName)
        throws SQLException {
        try { return getStream(rrs.findColumn(columnName), true); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    @Deprecated
    public java.io.InputStream getUnicodeStream(String columnName)
        throws SQLException {
        try { return getStream(rrs.findColumn(columnName), false); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public java.io.InputStream getBinaryStream(String columnName)
        throws SQLException {
        try { return getStream(rrs.findColumn(columnName), false); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public SQLWarning getWarnings()
        throws SQLException {
        try { return rrs.getWarnings(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void clearWarnings()
        throws SQLException {
        try { rrs.clearWarnings(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String getCursorName()
        throws SQLException {
        try { return rrs.getCursorName(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public ResultSetMetaData getMetaData()
        throws SQLException {
        try { return new BridgeResultSetMetaData(rrs.getMetaData()); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Object getObject(int columnIndex)
        throws SQLException {
        try { return rrs.getObject(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Object getObject(String columnName)
        throws SQLException {
        try { return rrs.getObject(columnName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int findColumn(String columnName)
        throws SQLException {
        try { return rrs.findColumn(columnName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public java.io.Reader getCharacterStream(int columnIndex)
        throws SQLException {
        try { return rrs.getCharacterStream(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public java.io.Reader getCharacterStream(String columnName)
        throws SQLException {
        try { return rrs.getCharacterStream(columnName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public BigDecimal getBigDecimal(int columnIndex)
        throws SQLException {
        try { return rrs.getBigDecimal(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public BigDecimal getBigDecimal(String columnName)
        throws SQLException {
        try { return rrs.getBigDecimal(columnName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean isBeforeFirst()
        throws SQLException {
        try { return rrs.isBeforeFirst(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean isAfterLast()
        throws SQLException {
        try { return rrs.isAfterLast(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean isFirst()
        throws SQLException {
        try { return rrs.isFirst(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean isLast()
        throws SQLException {
        try { return rrs.isLast(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void beforeFirst()
        throws SQLException {
        try { rrs.beforeFirst(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void afterLast()
        throws SQLException {
        try { rrs.afterLast(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean first()
        throws SQLException {
        try { return rrs.first(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean last()
        throws SQLException {
        try { return rrs.last(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getRow()
        throws SQLException {
        try { return rrs.getRow(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean absolute(int row)
        throws SQLException {
        try { return rrs.absolute(row); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean relative(int rows)
        throws SQLException {
        try { return rrs.relative(rows); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean previous()
        throws SQLException {
        try { return rrs.previous(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setFetchDirection(int direction)
        throws SQLException {
        try { rrs.setFetchDirection(direction); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getFetchDirection()
        throws SQLException {
        try { return rrs.getFetchDirection(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setFetchSize(int rows)
        throws SQLException {
        try { rrs.setFetchSize(rows); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getFetchSize()
        throws SQLException {
        try { return rrs.getFetchSize(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getType()
        throws SQLException {
        try { return rrs.getType(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getConcurrency()
        throws SQLException {
        try { return rrs.getConcurrency(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean rowUpdated()
        throws SQLException {
        try { return rrs.rowUpdated(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean rowInserted()
        throws SQLException {
        try { return rrs.rowInserted(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean rowDeleted()
        throws SQLException {
        try { return rrs.rowDeleted(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateNull(int columnIndex)
        throws SQLException {
        try { rrs.updateNull(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateBoolean(int columnIndex, boolean x)
        throws SQLException {
        try { rrs.updateBoolean(columnIndex, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateByte(int columnIndex, byte x)
        throws SQLException {
        try { rrs.updateByte(columnIndex, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateShort(int columnIndex, short x)
        throws SQLException {
        try { rrs.updateShort(columnIndex, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateInt(int columnIndex, int x)
        throws SQLException {
        try { rrs.updateInt(columnIndex, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateLong(int columnIndex, long x)
        throws SQLException {
        try { rrs.updateLong(columnIndex, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateFloat(int columnIndex, float x)
        throws SQLException {
        try { rrs.updateFloat(columnIndex, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateDouble(int columnIndex, double x)
        throws SQLException {
        try { rrs.updateDouble(columnIndex, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    @Deprecated
    public void updateBigDecimal(int colunmnIndex, BigDecimal x)
        throws SQLException {
        try { rrs.updateBigDecimal(columnIndex, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateString(int columnIndex, String x)
        throws SQLException {
        try { rrs.updateString(columnIndex, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateBytes(int columnIndex, byte x[])
        throws SQLException {
        try { rrs.updateBytes(columnIndex, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateDate(int columnIndex, java.sql.Date x)
        throws SQLException {
        try { rrs.updateDate(columnIndex, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateTime(int columnIndex, java.sql.Time x)
        throws SQLException {
        try { rrs.updateTime(columnIndex, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateTimestamp(int columnIndex, Timestamp x)
        throws SQLException {
        try { rrs.updateTimestamp(columnIndex, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateAsciiStream(int columnIndex, java.io.InputStream x, int length)
        throws SQLException {
        try { rrs.updateAsciiStream(columnIndex, x, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateBinaryStream(int columnIndex, java.io.InputStream x, int length)
        throws SQLException {
        try { rrs.updateBinaryStream(columnIndex, x, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateCharacterStream(int columnIndex, java.io.Reader reader, int length)
        throws SQLException {
        try { rrs.updateCharacterStream(columnIndex, reader, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateObject(int columnIndex, Object x, int scale)
        throws SQLException {
        try { rrs.updateObject(columnIndex, x, scale); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateObject(int columnIndex, Object x)
        throws SQLException {
        try { rrs.updateObject(columnIndex, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateRef(int columnIndex, Ref x)
        throws SQLException {
        try { rrs.updateRef(columnIndex, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateBlob(int columnIndex, Blob x)
        throws SQLException {
        try { rrs.updateBlob(columnIndex, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateClob(int columnIndex, Clob x)
        throws SQLException {
        try { rrs.updateClob(columnIndex, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateArray(int columnIndex, Array x)
        throws SQLException {
        try { rrs.updateArray(columnIndex, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateNull(String columnName)
        throws SQLException {
        try { rrs.updateNull(columnName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateBoolean(String columnName, boolean x)
        throws SQLException {
        try { rrs.updateBoolean(columnName, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateByte(String columnName, byte x)
        throws SQLException {
        try { rrs.updateByte(columnName, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateShort(String columnName, short x)
        throws SQLException {
        try { rrs.updateShort(columnName, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateInt(String columnName, int x)
        throws SQLException {
        try { rrs.updateInt(columnName, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateLong(String columnName, long x)
        throws SQLException {
        try { rrs.updateLong(columnName, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateFloat(String columnName, float x)
        throws SQLException {
        try { rrs.updateFloat(columnName, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateDouble(String columnName, double x)
        throws SQLException {
        try { rrs.updateDouble(columnName, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateBigDecimal(String columnName, BigDecimal x)
        throws SQLException {
        try { rrs.updateBigDecimal(columnName, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateString(String columnName, String x)
        throws SQLException {
        try { rrs.updateString(columnName, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateBytes(String columnName, byte x[])
        throws SQLException {
        try { rrs.updateBytes(columnName, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateDate(String columnName, java.sql.Date x)
        throws SQLException {
        try { rrs.updateDate(columnName, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateTime(String columnName, java.sql.Time x)
        throws SQLException {
        try { rrs.updateTime(columnName, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateTimestamp(String columnName, Timestamp x)
        throws SQLException {
        try { rrs.updateTimestamp(columnName, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateAsciiStream(String columnName, java.io.InputStream x, int length)
        throws SQLException {
        try { rrs.updateAsciiStream(columnName, x, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateBinaryStream(String columnName, java.io.InputStream x, int length)
        throws SQLException {
        try { rrs.updateBinaryStream(columnName, x, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateCharacterStream(String columnName, java.io.Reader reader, int length)
        throws SQLException {
        try { rrs.updateCharacterStream(columnName, reader, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateObject(String columnName, Object x, int scale)
        throws SQLException {
        try { rrs.updateObject(columnName, x, scale); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateObject(String columnName, Object x)
        throws SQLException {
        try { rrs.updateObject(columnName, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateRef(String columnName, Ref x)
        throws SQLException {
        try { rrs.updateRef(columnName, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateBlob(String columnName, Blob x)
        throws SQLException {
        try { rrs.updateBlob(columnName, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateClob(String columnName, Clob x)
        throws SQLException {
        try { rrs.updateClob(columnName, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateArray(String columnName, Array x)
        throws SQLException {
        try { rrs.updateArray(columnName, x); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void insertRow()
        throws SQLException {
        try { rrs.insertRow(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateRow()
        throws SQLException {
        try { rrs.updateRow(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void deleteRow()
        throws SQLException {
        try { rrs.deleteRow(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void refreshRow()
        throws SQLException {
        try { rrs.refreshRow(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void cancelRowUpdates()
        throws SQLException {
        try { rrs.cancelRowUpdates(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void moveToInsertRow()
        throws SQLException {
        try { rrs.moveToInsertRow(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void moveToCurrentRow()
        throws SQLException {
        try { rrs.insertRow(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Object getObject(int i, java.util.Map<String,Class<?>> map)
        throws SQLException {
        try { return rrs.getObject(i, map); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Ref getRef(int i)
        throws SQLException {
        try { return rrs.getRef(i); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Blob getBlob(int i)
        throws SQLException {
        try { return rrs.getBlob(i); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Clob getClob(int i)
        throws SQLException {
        try { return rrs.getClob(i); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Array getArray(int i)
        throws SQLException {
        try { return rrs.getArray(i); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public java.sql.Date getDate(int columnIndex, Calendar cal)
        throws SQLException {
        try { return rrs.getDate(columnIndex, cal); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public java.sql.Date getDate(String columnName, Calendar cal)
        throws SQLException {
        try { return rrs.getDate(columnName, cal); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public java.sql.Time getTime(int columnIndex, Calendar cal)
        throws SQLException {
        try { return rrs.getTime(columnIndex, cal); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public java.sql.Time getTime(String columnName, Calendar cal)
        throws SQLException {
        try { return rrs.getTime(columnName, cal); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Timestamp getTimestamp(int columnIndex, Calendar cal)
        throws SQLException {
        try { return rrs.getTimestamp(columnIndex, cal); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Timestamp getTimestamp(String columnName, Calendar cal)
        throws SQLException {
        try { return rrs.getTimestamp(columnName, cal); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Statement getStatement()
        throws SQLException {
        try { return rrs.getStatement(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Object getObject(String columnName, java.util.Map<String,Class<?>> map)
        throws SQLException {
        try { return rrs.getObject(columnName, map); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Ref getRef(String columnName)
        throws SQLException {
        try { return rrs.getRef(columnName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Blob getBlob(String columnName)
        throws SQLException {
        try { return rrs.getBlob(columnName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Clob getClob(String columnName)
        throws SQLException {
        try { return rrs.getClob(columnName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Array getArray(String columnName)
        throws SQLException {
        try { return rrs.getArray(columnName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public RowId getRowId(int columnIndex)
        throws SQLException {
        try { return rrs.getRowId(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public RowId getRowId(String columnLabel)
        throws SQLException {
        try { return rrs.getRowId(columnLabel); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateRowId(int columnIndex, RowId rowId)
        throws SQLException {
        try { rrs.updateRowId(columnIndex, rowId); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateRowId(String columnLabel, RowId rowId)
        throws SQLException {
        try { rrs.updateRowId(columnLabel, rowId); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getHoldability()
        throws SQLException {
        try { return rrs.getHoldability(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean isClosed()
        throws SQLException {
        try { return rrs.isClosed(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateNString(int columnIndex, String nString)
        throws SQLException {
        try { rrs.updateNString(columnIndex, nString); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateNString(String columnLabel, String nString)
        throws SQLException {
        try { rrs.updateNString(columnLabel, nString); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateNClob(int columnIndex, NClob nClob)
        throws SQLException {
        try { rrs.updateNClob(columnIndex, nClob); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateNClob(String columnLabel, NClob nClob)
        throws SQLException {
        try { rrs.updateNClob(columnLabel, nClob); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public NClob getNClob(int columnIndex)
        throws SQLException {
        try { return rrs.getNClob(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public NClob getNClob(String columnLabel)
        throws SQLException {
        try { return rrs.getNClob(columnLabel); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public SQLXML getSQLXML(int columnIndex)
        throws SQLException {
        try { return rrs.getSQLXML(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public SQLXML getSQLXML(String columnLabel)
        throws SQLException {
        try { return rrs.getSQLXML(columnLabel); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateSQLXML(int columnIndex, SQLXML sqlXml)
        throws SQLException {
        try { rrs.updateSQLXML(columnIndex, sqlXml); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateSQLXML(String columnLabel, SQLXML sqlXml)
        throws SQLException {
        try { rrs.updateSQLXML(columnLabel, sqlXml); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String getNString(int columnIndex)
        throws SQLException {
        try { return rrs.getNString(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String getNString(String columnLabel)
        throws SQLException {
        try { return rrs.getNString(columnLabel); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Reader getNCharacterStream(int columnIndex)
        throws SQLException {
        try { return rrs.getNCharacterStream(columnIndex); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Reader getNCharacterStream(String columnLabel)
        throws SQLException {
        try { return rrs.getNCharacterStream(columnLabel); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateNCharacterStream(int columnIndex, Reader reader, long length)
        throws SQLException {
        try { rrs.updateNCharacterStream(columnIndex, reader, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateNCharacterStream(String columnLabel, Reader reader, long length)
        throws SQLException {
        try { rrs.updateNCharacterStream(columnLabel, reader, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateAsciiStream(int columnIndex, InputStream inputStream, long length)
        throws SQLException {
        try { rrs.updateAsciiStream(columnIndex, inputStream, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateBinaryStream(int columnIndex, InputStream inputStream, long length)
        throws SQLException {
        try { rrs.updateBinaryStream(columnIndex, inputStream, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateCharacterStream(int columnIndex, Reader reader, long length)
        throws SQLException {
        try { rrs.updateCharacterStream(columnIndex, reader, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateAsciiStream(String columnLabel, InputStream inputStream, long length)
        throws SQLException {
        try { rrs.updateAsciiStream(columnLabel, inputStream, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateBinaryStream(String columnLabel, InputStream inputStream, long length)
        throws SQLException {
        try { rrs.updateBinaryStream(columnLabel, inputStream, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateCharacterStream(String columnLabel, Reader reader, long length)
        throws SQLException {
        try { rrs.updateCharacterStream(columnLabel, reader, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateBlob(int columnIndex, InputStream inputStream, long length)
        throws SQLException {
        try { rrs.updateBlob(columnIndex, inputStream, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateBlob(String columnLabel, InputStream inputStream, long length)
        throws SQLException {
        try { rrs.updateBlob(columnLabel, inputStream, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateClob(int columnIndex, Reader reader, long length)
        throws SQLException {
        try { rrs.updateClob(columnIndex, reader, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateClob(String columnLabel, Reader reader, long length)
        throws SQLException {
        try { rrs.updateClob(columnLabel, reader, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateNClob(int columnIndex, Reader reader, long length)
        throws SQLException {
        try { rrs.updateNClob(columnIndex, reader, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateNClob(String columnLabel, Reader reader, long length)
        throws SQLException {
        try { rrs.updateNClob(columnLabel, reader, length); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateNCharacterStream(int columnIndex, Reader reader)
        throws SQLException {
        try { rrs.updateNCharacterStream(columnIndex, reader); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateNCharacterStream(String columnLabel, Reader reader)
        throws SQLException {
        try { rrs.updateNCharacterStream(columnLabel, reader); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateAsciiStream(int columnIndex, InputStream inputStream)
        throws SQLException {
        try { rrs.updateAsciiStream(columnIndex, inputStream); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateBinaryStream(int columnIndex, InputStream inputStream)
        throws SQLException {
        try { rrs.updateBinaryStream(columnIndex, inputStream); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateCharacterStream(int columnIndex, Reader reader)
        throws SQLException {
        try { rrs.updateCharacterStream(columnIndex, reader); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateAsciiStream(String columnLabel, InputStream inputStream)
        throws SQLException {
        try { rrs.updateAsciiStream(columnLabel, inputStream); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateBinaryStream(String columnLabel, InputStream inputStream)
        throws SQLException {
        try { rrs.updateBinaryStream(columnLabel, inputStream); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateCharacterStream(String columnLabel, Reader reader)
        throws SQLException {
        try { rrs.updateCharacterStream(columnLabel, reader); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateBlob(int columnIndex, InputStream inputStream)
        throws SQLException {
        try { rrs.updateBlob(columnIndex, inputStream); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateBlob(String columnLabel, InputStream inputStream)
        throws SQLException {
        try { rrs.updateBlob(columnLabel, inputStream); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateClob(int columnIndex, Reader reader)
        throws SQLException {
        try { rrs.updateClob(columnIndex, reader); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateClob(String columnLabel, Reader reader)
        throws SQLException {
        try { rrs.updateClob(columnLabel, reader); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateNClob(int columnIndex, Reader reader)
        throws SQLException {
        try { rrs.updateNClob(columnIndex, reader); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void updateNClob(String columnLabel, Reader reader)
        throws SQLException {
        try { rrs.updateNClob(columnLabel, reader); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public <T> T getObject(int columnIndex,
			   Class<T> type)
    throws SQLException {
        try { return rrs.getObject(columnIndex, type); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public <T> T getObject(String columnLabel,
			   Class<T> type)
	throws SQLException {
        try { return rrs.getObject(columnLabel, type); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public <T> T unwrap(Class<T> iface) {
        return (T)iface;
    }

    public boolean isWrapperFor(Class<?> iface) {
        return false;
    }

}

