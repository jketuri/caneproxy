
package FI.realitymodeler.sql;

import java.io.*;
import jdbm.helper.*;

public class IndexContext {
    transient IndexFile indexFile = null;
    transient RandomAccessFile rawDataFile = null, rawBlockFile = null;
    transient TupleBrowser tupleBrowser = null;
    transient Tuple tuple = new Tuple();
    transient int dataRefIndex = -1;
    transient int dataRef = 0;

    public void clear()
        throws IOException {
        tupleBrowser = null;
    }

    void closeData()
        throws IOException {
        if (rawDataFile != null) {
            rawDataFile.close();
            rawDataFile = null;
        }
    }

    public void close()
        throws IOException {
        closeData();
        if (rawBlockFile != null) {
            rawBlockFile.close();
            rawBlockFile = null;
        }
    }

    public String toString() {
        return getClass().getName() + "{indexFile=" + indexFile +
            ",rawDataFile=" + rawDataFile + ",rawBlockFile=" + rawBlockFile +
            ",tupleBrowser=" + tupleBrowser + ",tuple=" + tuple +
            ",dataRefIndex=" + dataRefIndex + ",dataRef=" + dataRef + "}";
    }

    protected void finalize()
        throws IOException {
        close();
    }

}
