
package FI.realitymodeler.sql;

import FI.realitymodeler.common.*;
import java.io.*;

public class LocalStream extends InputStream {
    public ObjectInput in = null;
    public TransferStream ts = null;
    public W3Lock lock = new W3Lock(true);

    private int m = 0;
    private boolean started = false;

    private final boolean check()
        throws IOException {
        if (!started) {
            try {
                lock.lock();
            } catch (InterruptedException ex) {
                throw new IOException();
            }
            started = true;
        }
        if (m == -1 || m == 0 && (m = in.readInt()) == 0) {
            m = -1;
            ts.lock1.release();
            return true;
        }
        return false;
    }

    public int read()
        throws IOException {
        if (check()) return -1;
        int c = in.read();
        if (c == -1) return m = -1;
        m--;
        check();
        return c;
    }

    public int read(byte b[])
        throws IOException {
        if (check()) return -1;
        if (b.length == 0) return 0;
        int n = in.read(b,0,Math.min(b.length,m));
        if (n <= 0) m = -1;
        else m -= n;
        check();
        return n;
    }

    public int read(byte b[],int off,int len)
        throws IOException {
        if (check()) return -1;
        if (b.length - off <= 0) return 0;
        int n = in.read(b,off,Math.min(Math.min(b.length - off,len),m));
        if (n <= 0) m = -1;
        else m -= n;
        check();
        return n;
    }

    public long skip(long n)
        throws IOException {
        if (check()) return 0;
        n = in.skip(Math.min(n,m));
        m -= n;
        check();
        return n;
    }

    public int available()
        throws IOException {
        if (check()) return 0;
        return m;
    }

    protected void finalize() {
        if (m > 0 && in != null)
            try {
                in.close();
            } catch (IOException ex) {};
        if (ts != null) ts.lock1.release();
    }

}
