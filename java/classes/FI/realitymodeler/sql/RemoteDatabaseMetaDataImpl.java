
package FI.realitymodeler.sql;

import java.rmi.*;
import java.rmi.server.*;
import java.sql.*;

public class RemoteDatabaseMetaDataImpl extends UnicastRemoteObject implements RemoteDatabaseMetaData {
    private static final long serialVersionUID = 0L;

    private DatabaseMetaData dmd;

    public RemoteDatabaseMetaDataImpl(DatabaseMetaData dmd)
        throws RemoteException {
        this.dmd = dmd;
    }

    public boolean allProceduresAreCallable()
        throws SQLException {
        return dmd.allProceduresAreCallable();
    }

    public boolean allTablesAreSelectable()
        throws SQLException {
        return dmd.allTablesAreSelectable();
    }

    public String getURL()
        throws SQLException {
        return dmd.getURL();
    }

    public String getUserName()
        throws SQLException {
        return dmd.getUserName();
    }

    public boolean isReadOnly()
        throws SQLException {
        return dmd.isReadOnly();
    }

    public boolean nullsAreSortedHigh()
        throws SQLException {
        return dmd.nullsAreSortedHigh();
    }

    public boolean nullsAreSortedLow()
        throws SQLException {
        return dmd.nullsAreSortedLow();
    }

    public boolean nullsAreSortedAtStart()
        throws SQLException {
        return dmd.nullsAreSortedAtStart();
    }

    public boolean nullsAreSortedAtEnd()
        throws SQLException {
        return dmd.nullsAreSortedAtEnd();
    }

    public String getDatabaseProductName()
        throws SQLException {
        return dmd.getDatabaseProductName();
    }

    public String getDatabaseProductVersion()
        throws SQLException {
        return dmd.getDatabaseProductVersion();
    }

    public String getDriverName()
        throws SQLException {
        return dmd.getDriverName();
    }

    public String getDriverVersion()
        throws SQLException {
        return dmd.getDriverVersion();
    }

    public int getDriverMajorVersion() {
        return dmd.getDriverMajorVersion();
    }

    public int getDriverMinorVersion() {
        return dmd.getDriverMinorVersion();
    }

    public boolean usesLocalFiles()
        throws SQLException {
        return dmd.usesLocalFiles();
    }

    public boolean usesLocalFilePerTable()
        throws SQLException {
        return dmd.usesLocalFilePerTable();
    }

    public boolean supportsMixedCaseIdentifiers()
        throws SQLException {
        return dmd.supportsMixedCaseIdentifiers();
    }

    public boolean storesUpperCaseIdentifiers()
        throws SQLException {
        return dmd.storesUpperCaseIdentifiers();
    }

    public boolean storesLowerCaseIdentifiers()
        throws SQLException {
        return dmd.storesLowerCaseIdentifiers();
    }

    public boolean storesMixedCaseIdentifiers()
        throws SQLException {
        return dmd.storesMixedCaseIdentifiers();
    }

    public boolean supportsMixedCaseQuotedIdentifiers()
        throws SQLException {
        return dmd.supportsMixedCaseQuotedIdentifiers();
    }

    public boolean storesUpperCaseQuotedIdentifiers()
        throws SQLException {
        return dmd.storesUpperCaseQuotedIdentifiers();
    }

    public boolean storesLowerCaseQuotedIdentifiers()
        throws SQLException {
        return dmd.storesLowerCaseQuotedIdentifiers();
    }

    public boolean storesMixedCaseQuotedIdentifiers()
        throws SQLException {
        return dmd.storesMixedCaseQuotedIdentifiers();
    }

    public String getIdentifierQuoteString()
        throws SQLException {
        return dmd.getIdentifierQuoteString();
    }

    public String getSQLKeywords()
        throws SQLException {
        return dmd.getSQLKeywords();
    }

    public String getNumericFunctions()
        throws SQLException {
        return dmd.getNumericFunctions();
    }

    public String getStringFunctions()
        throws SQLException {
        return dmd.getStringFunctions();
    }

    public String getSystemFunctions()
        throws SQLException {
        return dmd.getSystemFunctions();
    }

    public String getTimeDateFunctions()
        throws SQLException {
        return dmd.getTimeDateFunctions();
    }

    public String getSearchStringEscape()
        throws SQLException {
        return dmd.getSearchStringEscape();
    }

    public String getExtraNameCharacters()
        throws SQLException {
        return dmd.getExtraNameCharacters();
    }

    public boolean supportsAlterTableWithAddColumn()
        throws SQLException {
        return dmd.supportsAlterTableWithAddColumn();
    }

    public boolean supportsAlterTableWithDropColumn()
        throws SQLException {
        return dmd.supportsAlterTableWithDropColumn();
    }

    public boolean supportsColumnAliasing()
        throws SQLException {
        return dmd.supportsColumnAliasing();
    }

    public boolean nullPlusNonNullIsNull()
        throws SQLException {
        return dmd.nullPlusNonNullIsNull();
    }

    public boolean supportsConvert()
        throws SQLException {
        return dmd.supportsConvert();
    }

    public boolean supportsConvert(int fromType, int toType)
        throws SQLException {
        return dmd.supportsConvert(fromType, toType);
    }

    public boolean supportsTableCorrelationNames()
        throws SQLException {
        return dmd.supportsTableCorrelationNames();
    }

    public boolean supportsDifferentTableCorrelationNames()
        throws SQLException {
        return dmd.supportsDifferentTableCorrelationNames();
    }

    public boolean supportsExpressionsInOrderBy()
        throws SQLException {
        return dmd.supportsExpressionsInOrderBy();
    }

    public boolean supportsOrderByUnrelated()
        throws SQLException {
        return dmd.supportsOrderByUnrelated();
    }

    public boolean supportsGroupBy()
        throws SQLException {
        return dmd.supportsGroupBy();
    }

    public boolean supportsGroupByUnrelated()
        throws SQLException {
        return dmd.supportsGroupByUnrelated();
    }

    public boolean supportsGroupByBeyondSelect()
        throws SQLException {
        return dmd.supportsGroupByBeyondSelect();
    }

    public boolean supportsLikeEscapeClause()
        throws SQLException {
        return dmd.supportsLikeEscapeClause();
    }

    public boolean supportsMultipleResultSets()
        throws SQLException {
        return dmd.supportsMultipleResultSets();
    }

    public boolean supportsMultipleTransactions()
        throws SQLException {
        return dmd.supportsMultipleTransactions();
    }

    public boolean supportsNonNullableColumns()
        throws SQLException {
        return dmd.supportsNonNullableColumns();
    }

    public boolean supportsMinimumSQLGrammar()
        throws SQLException {
        return dmd.supportsMinimumSQLGrammar();
    }

    public boolean supportsCoreSQLGrammar()
        throws SQLException {
        return dmd.supportsCoreSQLGrammar();
    }

    public boolean supportsExtendedSQLGrammar()
        throws SQLException {
        return dmd.supportsExtendedSQLGrammar();
    }

    public boolean supportsANSI92EntryLevelSQL()
        throws SQLException {
        return dmd.supportsANSI92EntryLevelSQL();
    }

    public boolean supportsANSI92IntermediateSQL()
        throws SQLException {
        return dmd.supportsANSI92IntermediateSQL();
    }

    public boolean supportsANSI92FullSQL()
        throws SQLException {
        return dmd.supportsANSI92FullSQL();
    }

    public boolean supportsIntegrityEnhancementFacility()
        throws SQLException {
        return dmd.supportsIntegrityEnhancementFacility();
    }

    public boolean supportsOuterJoins()
        throws SQLException {
        return dmd.supportsOuterJoins();
    }

    public boolean supportsFullOuterJoins()
        throws SQLException {
        return dmd.supportsFullOuterJoins();
    }

    public boolean supportsLimitedOuterJoins()
        throws SQLException {
        return dmd.supportsLimitedOuterJoins();
    }

    public String getSchemaTerm()
        throws SQLException {
        return dmd.getSchemaTerm();
    }

    public String getProcedureTerm()
        throws SQLException {
        return dmd.getProcedureTerm();
    }

    public String getCatalogTerm()
        throws SQLException {
        return dmd.getCatalogTerm();
    }

    public boolean isCatalogAtStart()
        throws SQLException {
        return dmd.isCatalogAtStart();
    }

    public String getCatalogSeparator()
        throws SQLException {
        return dmd.getCatalogSeparator();
    }

    public boolean supportsSchemasInDataManipulation()
        throws SQLException {
        return dmd.supportsSchemasInDataManipulation();
    }

    public boolean supportsSchemasInProcedureCalls()
        throws SQLException {
        return dmd.supportsSchemasInProcedureCalls();
    }

    public boolean supportsSchemasInTableDefinitions()
        throws SQLException {
        return dmd.supportsSchemasInTableDefinitions();
    }

    public boolean supportsSchemasInIndexDefinitions()
        throws SQLException {
        return dmd.supportsSchemasInIndexDefinitions();
    }

    public boolean supportsSchemasInPrivilegeDefinitions()
        throws SQLException {
        return dmd.supportsSchemasInPrivilegeDefinitions();
    }

    public boolean supportsCatalogsInDataManipulation()
        throws SQLException {
        return dmd.supportsCatalogsInDataManipulation();
    }

    public boolean supportsCatalogsInProcedureCalls()
        throws SQLException {
        return dmd.supportsCatalogsInProcedureCalls();
    }

    public boolean supportsCatalogsInTableDefinitions()
        throws SQLException {
        return dmd.supportsCatalogsInTableDefinitions();
    }

    public boolean supportsCatalogsInIndexDefinitions()
        throws SQLException {
        return dmd.supportsCatalogsInIndexDefinitions();
    }

    public boolean supportsCatalogsInPrivilegeDefinitions()
        throws SQLException {
        return dmd.supportsCatalogsInPrivilegeDefinitions();
    }

    public boolean supportsPositionedDelete()
        throws SQLException {
        return dmd.supportsPositionedDelete();
    }

    public boolean supportsPositionedUpdate()
        throws SQLException {
        return dmd.supportsPositionedUpdate();
    }

    public boolean supportsSelectForUpdate()
        throws SQLException {
        return dmd.supportsSelectForUpdate();
    }

    public boolean supportsStoredProcedures()
        throws SQLException {
        return dmd.supportsStoredProcedures();
    }

    public boolean supportsSubqueriesInComparisons()
        throws SQLException {
        return dmd.supportsSubqueriesInComparisons();
    }

    public boolean supportsSubqueriesInExists()
        throws SQLException {
        return dmd.supportsSubqueriesInExists();
    }

    public boolean supportsSubqueriesInIns()
        throws SQLException {
        return dmd.supportsSubqueriesInIns();
    }

    public boolean supportsSubqueriesInQuantifieds()
        throws SQLException {
        return dmd.supportsSubqueriesInQuantifieds();
    }

    public boolean supportsCorrelatedSubqueries()
        throws SQLException {
        return dmd.supportsCorrelatedSubqueries();
    }

    public boolean supportsUnion()
        throws SQLException {
        return dmd.supportsUnion();
    }

    public boolean supportsUnionAll()
        throws SQLException {
        return dmd.supportsUnionAll();
    }

    public boolean supportsOpenCursorsAcrossCommit()
        throws SQLException {
        return dmd.supportsOpenCursorsAcrossCommit();
    }

    public boolean supportsOpenCursorsAcrossRollback()
        throws SQLException {
        return dmd.supportsOpenCursorsAcrossRollback();
    }

    public boolean supportsOpenStatementsAcrossCommit()
        throws SQLException {
        return dmd.supportsOpenStatementsAcrossCommit();
    }

    public boolean supportsOpenStatementsAcrossRollback()
        throws SQLException {
        return dmd.supportsOpenStatementsAcrossRollback();
    }

    public int getMaxBinaryLiteralLength()
        throws SQLException {
        return dmd.getMaxBinaryLiteralLength();
    }

    public int getMaxCharLiteralLength()
        throws SQLException {
        return dmd.getMaxCharLiteralLength();
    }

    public int getMaxColumnNameLength()
        throws SQLException {
        return dmd.getMaxColumnNameLength();
    }

    public int getMaxColumnsInGroupBy()
        throws SQLException {
        return dmd.getMaxColumnsInGroupBy();
    }

    public int getMaxColumnsInIndex()
        throws SQLException {
        return dmd.getMaxColumnsInIndex();
    }

    public int getMaxColumnsInOrderBy()
        throws SQLException {
        return dmd.getMaxColumnsInOrderBy();
    }

    public int getMaxColumnsInSelect()
        throws SQLException {
        return dmd.getMaxColumnsInSelect();
    }

    public int getMaxColumnsInTable()
        throws SQLException {
        return dmd.getMaxColumnsInTable();
    }

    public int getMaxConnections()
        throws SQLException {
        return dmd.getMaxConnections();
    }

    public int getMaxCursorNameLength()
        throws SQLException {
        return dmd.getMaxCursorNameLength();
    }

    public int getMaxIndexLength()
        throws SQLException {
        return dmd.getMaxIndexLength();
    }

    public int getMaxSchemaNameLength()
        throws SQLException {
        return dmd.getMaxSchemaNameLength();
    }

    public int getMaxProcedureNameLength()
        throws SQLException {
        return dmd.getMaxProcedureNameLength();
    }

    public int getMaxCatalogNameLength()
        throws SQLException {
        return dmd.getMaxCatalogNameLength();
    }

    public int getMaxRowSize()
        throws SQLException {
        return dmd.getMaxRowSize();
    }

    public boolean doesMaxRowSizeIncludeBlobs()
        throws SQLException {
        return dmd.doesMaxRowSizeIncludeBlobs();
    }

    public int getMaxStatementLength()
        throws SQLException {
        return dmd.getMaxStatementLength();
    }

    public int getMaxStatements()
        throws SQLException {
        return dmd.getMaxStatements();
    }

    public int getMaxTableNameLength()
        throws SQLException {
        return dmd.getMaxTableNameLength();
    }

    public int getMaxTablesInSelect()
        throws SQLException {
        return dmd.getMaxTablesInSelect();
    }

    public int getMaxUserNameLength()
        throws SQLException {
        return dmd.getMaxUserNameLength();
    }

    public int getDefaultTransactionIsolation()
        throws SQLException {
        return dmd.getDefaultTransactionIsolation();
    }

    public boolean supportsTransactions()
        throws SQLException {
        return dmd.supportsTransactions();
    }

    public boolean supportsTransactionIsolationLevel(int level)
        throws SQLException {
        return dmd.supportsTransactionIsolationLevel(level);
    }

    public boolean supportsDataDefinitionAndDataManipulationTransactions()
        throws SQLException {
        return dmd.supportsDataDefinitionAndDataManipulationTransactions();
    }

    public boolean supportsDataManipulationTransactionsOnly()
        throws SQLException {
        return dmd.supportsDataManipulationTransactionsOnly();
    }

    public boolean dataDefinitionCausesTransactionCommit()
        throws SQLException {
        return dmd.dataDefinitionCausesTransactionCommit();
    }

    public boolean dataDefinitionIgnoredInTransactions()
        throws SQLException {
        return dmd.dataDefinitionIgnoredInTransactions();
    }

    public RemoteResultSet getProcedures(String catalog, String schemaPattern, String procedureNamePattern)
        throws RemoteException, SQLException {
        return new RemoteResultSetImpl(dmd.getProcedures(catalog,schemaPattern,procedureNamePattern));
    }

    public RemoteResultSet getProcedureColumns(String catalog, String schemaPattern, String procedureNamePattern, String columnNamePattern)
        throws RemoteException, SQLException {
        return new RemoteResultSetImpl(dmd.getProcedureColumns(catalog, schemaPattern, procedureNamePattern, columnNamePattern));
    }

    public RemoteResultSet getTables(String catalog, String schemaPattern, String tableNamePattern, String types[])
        throws RemoteException, SQLException {
        return new RemoteResultSetImpl(dmd.getTables(catalog, schemaPattern, tableNamePattern, types));
    }

    public RemoteResultSet getSchemas()
        throws RemoteException, SQLException {
        return new RemoteResultSetImpl(dmd.getSchemas());
    }

    public RemoteResultSet getCatalogs()
        throws RemoteException, SQLException {
        return new RemoteResultSetImpl(dmd.getCatalogs());
    }

    public RemoteResultSet getTableTypes()
        throws RemoteException, SQLException {
        return new RemoteResultSetImpl(dmd.getTableTypes());
    }

    public RemoteResultSet getColumns(String catalog, String schemaPattern, String tableNamePattern, String columnNamePattern)
        throws RemoteException, SQLException {
        return new RemoteResultSetImpl(dmd.getColumns(catalog, schemaPattern, tableNamePattern, columnNamePattern));
    }

    public RemoteResultSet getColumnPrivileges(String catalog, String schema, String table, String columnNamePattern)
        throws RemoteException, SQLException {
        return new RemoteResultSetImpl(dmd.getColumnPrivileges(catalog, schema, table, columnNamePattern));
    }

    public RemoteResultSet getTablePrivileges(String catalog, String schemaPattern, String tableNamePattern)
        throws RemoteException, SQLException {
        return new RemoteResultSetImpl(dmd.getTablePrivileges(catalog, schemaPattern, tableNamePattern));
    }

    public RemoteResultSet getBestRowIdentifier(String catalog, String schema, String table, int scope, boolean nullable)
        throws RemoteException, SQLException {
        return new RemoteResultSetImpl(dmd.getBestRowIdentifier(catalog, schema, table, scope, nullable));
    }

    public RemoteResultSet getVersionColumns(String catalog, String schema, String table)
        throws RemoteException, SQLException {
        return new RemoteResultSetImpl(dmd.getVersionColumns(catalog, schema, table));
    }

    public RemoteResultSet getPrimaryKeys(String catalog, String schema, String table)
        throws RemoteException, SQLException {
        return new RemoteResultSetImpl(dmd.getPrimaryKeys(catalog, schema, table));
    }

    public RemoteResultSet getImportedKeys(String catalog, String schema, String table)
        throws RemoteException, SQLException {
        return new RemoteResultSetImpl(dmd.getImportedKeys(catalog, schema, table));
    }

    public RemoteResultSet getExportedKeys(String catalog, String schema, String table)
        throws RemoteException, SQLException {
        return new RemoteResultSetImpl(dmd.getExportedKeys(catalog, schema, table));
    }

    public RemoteResultSet getCrossReference(String primaryCatalog, String primarySchema, String primaryTable, String foreignCatalog, String foreignSchema, String foreignTable)
        throws RemoteException, SQLException {
        return new RemoteResultSetImpl(dmd.getCrossReference(primaryCatalog, primarySchema, primaryTable, foreignCatalog, foreignSchema, foreignTable));
    }

    public RemoteResultSet getTypeInfo()
        throws RemoteException, SQLException {
        return new RemoteResultSetImpl(dmd.getTypeInfo());
    }

    public RemoteResultSet getIndexInfo(String catalog, String schema, String table, boolean unique, boolean approximate)
        throws RemoteException, SQLException {
        return new RemoteResultSetImpl(dmd.getIndexInfo(catalog, schema, table, unique, approximate));
    }

    public boolean supportsResultSetType(int type)
        throws SQLException {
        return dmd.supportsResultSetType(type);
    }

    public boolean supportsResultSetConcurrency(int type, int concurrency)
        throws SQLException {
        return dmd.supportsResultSetConcurrency(type, concurrency);
    }

    public boolean ownUpdatesAreVisible(int type)
        throws SQLException {
        return dmd.ownUpdatesAreVisible(type);
    }

    public boolean ownDeletesAreVisible(int type)
        throws SQLException {
        return dmd.ownDeletesAreVisible(type);
    }

    public boolean ownInsertsAreVisible(int type)
        throws SQLException {
        return dmd.ownInsertsAreVisible(type);
    }

    public boolean othersUpdatesAreVisible(int type)
        throws SQLException {
        return dmd.othersUpdatesAreVisible(type);
    }

    public boolean othersDeletesAreVisible(int type)
        throws SQLException {
        return dmd.othersDeletesAreVisible(type);
    }

    public boolean othersInsertsAreVisible(int type)
        throws SQLException {
        return dmd.othersInsertsAreVisible(type);
    }

    public boolean updatesAreDetected(int type)
        throws SQLException {
        return dmd.updatesAreDetected(type);
    }

    public boolean deletesAreDetected(int type)
        throws SQLException {
        return dmd.deletesAreDetected(type);
    }

    public boolean insertsAreDetected(int type)
        throws SQLException {
        return dmd.insertsAreDetected(type);
    }

    public boolean supportsBatchUpdates()
        throws SQLException {
        return dmd.supportsBatchUpdates();
    }

    public RemoteResultSet getUDTs(String catalog, String schemaPattern, String typeNamePattern,  int types[])
        throws RemoteException, SQLException {
        return new RemoteResultSetImpl(dmd.getUDTs(catalog,  schemaPattern,  typeNamePattern,  types));
    }

    public RemoteConnection getConnection()
        throws RemoteException, SQLException {
        return new RemoteConnectionImpl(dmd.getConnection());
    }

    public boolean supportsSavepoints()
        throws SQLException {
        return dmd.supportsSavepoints();
    }

    public boolean supportsNamedParameters()
        throws SQLException {
        return dmd.supportsNamedParameters();
    }

    public boolean supportsMultipleOpenResults()
        throws SQLException {
        return dmd.supportsMultipleOpenResults();
    }

    public boolean supportsGetGeneratedKeys()
        throws SQLException {
        return dmd.supportsGetGeneratedKeys();
    }

    public RemoteResultSet getSuperTypes(String catalog, String schemaPattern, String typeNamePattern)
        throws RemoteException, SQLException {
        return new RemoteResultSetImpl(dmd.getSuperTypes(catalog, schemaPattern, typeNamePattern));
    }

    public RemoteResultSet getSuperTables(String catalog, String schemaPattern, String tableNamePattern)
        throws RemoteException, SQLException {
        return new RemoteResultSetImpl(dmd.getSuperTables(catalog, schemaPattern, tableNamePattern));
    }

    public RemoteResultSet getAttributes(String catalog, String schemaPattern, String typeNamePattern, String attributeNamePattern)
        throws RemoteException, SQLException {
        return new RemoteResultSetImpl(dmd.getAttributes(catalog, schemaPattern, typeNamePattern, attributeNamePattern));
    }

    public boolean supportsResultSetHoldability(int holdability)
        throws SQLException {
        return dmd.supportsResultSetHoldability(holdability);
    }

    public int getResultSetHoldability()
        throws SQLException {
        return dmd.getResultSetHoldability();
    }

    public int getDatabaseMajorVersion()
        throws SQLException {
        return dmd.getDatabaseMajorVersion();
    }

    public int getDatabaseMinorVersion()
        throws SQLException {
        return dmd.getDatabaseMinorVersion();
    }

    public int getJDBCMajorVersion()
        throws SQLException {
        return dmd.getJDBCMajorVersion();
    }

    public int getJDBCMinorVersion()
        throws SQLException {
        return dmd.getJDBCMinorVersion();
    }

    public int getSQLStateType()
        throws SQLException {
        return dmd.getSQLStateType();
    }

    public boolean locatorsUpdateCopy()
        throws SQLException {
        return dmd.locatorsUpdateCopy();
    }

    public boolean supportsStatementPooling()
        throws SQLException {
        return dmd.supportsStatementPooling();
    }

    public RowIdLifetime getRowIdLifetime()
        throws SQLException {
        return dmd.getRowIdLifetime();
    }

    public ResultSet getSchemas(String catalog, String schemaPattern)
        throws SQLException {
        return dmd.getSchemas(catalog, schemaPattern);
    }

    public boolean supportsStoredFunctionsUsingCallSyntax()
        throws SQLException {
        return dmd.supportsStoredFunctionsUsingCallSyntax();
    }

    public boolean autoCommitFailureClosesAllResultSets()
        throws SQLException {
        return dmd.autoCommitFailureClosesAllResultSets();
    }

    public ResultSet getClientInfoProperties()
        throws SQLException {
        return dmd.getClientInfoProperties();
    }

    public ResultSet getFunctions(String catalog, String schemaPattern, String functionNamePattern)
        throws SQLException {
        return dmd.getFunctions(catalog, schemaPattern, functionNamePattern);
    }

    public ResultSet getFunctionColumns(String catalog, String schemaPattern, String functionNamePattern, String columnNamePattern)
        throws SQLException {
        return dmd.getFunctionColumns(catalog, schemaPattern, functionNamePattern, columnNamePattern);
    }

}
