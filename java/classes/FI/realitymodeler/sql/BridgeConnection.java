
package FI.realitymodeler.sql;

import java.rmi.*;
import java.sql.*;
import java.util.*;
import java.util.concurrent.*;

public class BridgeConnection implements Connection {
    private RemoteConnection rc;

    public BridgeConnection(RemoteConnection rc) {
        this.rc = rc;
    }

    public boolean created()
        throws SQLException {
        try { return rc.created(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int tableLength(String tableName)
        throws SQLException {
        try { return rc.tableLength(tableName); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Statement createStatement()
        throws SQLException {
        try { return new BridgeStatement(rc.createStatement()); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public PreparedStatement prepareStatement(String sql)
        throws SQLException {
        try { return new BridgeStatement(rc.prepareStatement(sql)); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public CallableStatement prepareCall(String sql)
        throws SQLException {
        try { return new BridgeStatement(rc.prepareCall(sql)); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String nativeSQL(String sql)
        throws SQLException {
        try { return rc.nativeSQL(sql); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setAutoCommit(boolean autoCommit)
        throws SQLException {
        try { rc.setAutoCommit(autoCommit); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean getAutoCommit()
        throws SQLException {
        try { return rc.getAutoCommit(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void commit()
        throws SQLException {
        try { rc.commit(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void rollback()
        throws SQLException {
        try { rc.rollback(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void close()
        throws SQLException {
        try { rc.close(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean isClosed()
        throws SQLException {
        try { return rc.isClosed(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public DatabaseMetaData getMetaData()
        throws SQLException {
        try { return new BridgeDatabaseMetaData(rc.getMetaData()); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setReadOnly(boolean readOnly)
        throws SQLException {
        try { rc.setReadOnly(readOnly); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean isReadOnly()
        throws SQLException {
        try { return rc.isReadOnly(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setCatalog(String catalog)
        throws SQLException {
        try { rc.setCatalog(catalog); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String getCatalog()
        throws SQLException {
        try { return rc.getCatalog(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setTransactionIsolation(int level)
        throws SQLException {
        try { rc.setTransactionIsolation(level); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getTransactionIsolation()
        throws SQLException {
        try { return rc.getTransactionIsolation(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public SQLWarning getWarnings()
        throws SQLException {
        try { return rc.getWarnings(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void clearWarnings()
        throws SQLException {
        try { rc.clearWarnings(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Statement createStatement(int resultSetType, int resultSetConcurrency)
        throws SQLException {
        try { return new BridgeStatement(rc.createStatement(resultSetType, resultSetConcurrency)); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency)
        throws SQLException {
        try { return new BridgeStatement(rc.prepareStatement(sql, resultSetType, resultSetConcurrency)); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency)
        throws SQLException {
        try { return new BridgeStatement(rc.prepareCall(sql, resultSetType, resultSetConcurrency)); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Map<String, Class<?>> getTypeMap()
        throws SQLException {
        try { return rc.getTypeMap(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setTypeMap(Map<String, Class<?>> map)
        throws SQLException {
        try { rc.setTypeMap(map); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setHoldability(int holdability)
        throws SQLException {
        try { rc.setHoldability(holdability); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getHoldability()
        throws SQLException {
        try { return rc.getHoldability(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Savepoint setSavepoint()
        throws SQLException {
        try { return rc.setSavepoint(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Savepoint setSavepoint(String name)
        throws SQLException {
        try { return rc.setSavepoint(name); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void rollback(Savepoint savepoint)
        throws SQLException {
        try { rc.rollback(savepoint); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void releaseSavepoint(Savepoint savepoint)
        throws SQLException {
        try { rc.releaseSavepoint(savepoint); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Statement createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability)
        throws SQLException {
        try { return new BridgeStatement(rc.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability)); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability)
        throws SQLException {
        try { return new BridgeStatement(rc.prepareStatement(sql, resultSetType, resultSetConcurrency, resultSetHoldability)); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability)
        throws SQLException {
        try { return new BridgeStatement(rc.prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability)); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys)
        throws SQLException {
        try { return new BridgeStatement(rc.prepareStatement(sql, autoGeneratedKeys)); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public PreparedStatement prepareStatement(String sql, int columnIndexes[])
        throws SQLException {
        try { return new BridgeStatement(rc.prepareStatement(sql, columnIndexes)); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public PreparedStatement prepareStatement(String sql, String columnNames[])
        throws SQLException {
        try { return new BridgeStatement(rc.prepareStatement(sql, columnNames)); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Clob createClob()
        throws SQLException {
        try { return rc.createClob(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Blob createBlob()
        throws SQLException {
        try { return rc.createBlob(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public NClob createNClob()
        throws SQLException {
        try { return rc.createNClob(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public SQLXML createSQLXML()
        throws SQLException {
        try { return rc.createSQLXML(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public boolean isValid(int timeout)
        throws SQLException {
        try { return rc.isValid(timeout); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setClientInfo(String name, String value)
        throws SQLClientInfoException {
        try { rc.setClientInfo(name, value); } catch (RemoteException ex) { throw new SQLClientInfoException(ex.toString(), null); }
    }

    public void setClientInfo(Properties properties)
        throws SQLClientInfoException {
        try { rc.setClientInfo(properties); } catch (RemoteException ex) { throw new SQLClientInfoException(ex.toString(), null); }
    }

    public String getClientInfo(String name)
        throws SQLException {
        try { return rc.getClientInfo(name); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Properties getClientInfo()
        throws SQLException {
        try { return rc.getClientInfo(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Array createArrayOf(String typeName, Object elements[])
        throws SQLException {
        try { return rc.createArrayOf(typeName, elements); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public Struct createStruct(String typeName, Object elements[])
        throws SQLException {
        try { return rc.createStruct(typeName, elements); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setSchema(String schema)
	throws SQLException {
        try { rc.setSchema(schema); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public String getSchema()
	throws SQLException {
        try { return rc.getSchema(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void abort(Executor executor)
	throws SQLException {
        try { rc.abort(executor); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public void setNetworkTimeout(Executor executor,
				  int milliseconds)
	throws SQLException {
        try { rc.setNetworkTimeout(executor, milliseconds); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public int getNetworkTimeout()
	throws SQLException {
        try { return rc.getNetworkTimeout(); } catch (RemoteException ex) { throw new SQLException(ex.toString()); }
    }

    public <T> T unwrap(Class<T> iface) {
        return (T)iface;
    }

    public boolean isWrapperFor(Class<?> iface) {
        return false;
    }

}
