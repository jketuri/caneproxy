
package FI.realitymodeler.sql;

import java.rmi.*;
import java.sql.*;

public interface RemoteResultSetMetaData extends Remote {

    int getColumnCount()
        throws RemoteException, SQLException;

    boolean isAutoIncrement(int columnIndex)
        throws RemoteException, SQLException;

    boolean isCaseSensitive(int columnIndex)
        throws RemoteException, SQLException;

    boolean isSearchable(int columnIndex)
        throws RemoteException, SQLException;

    boolean isCurrency(int columnIndex)
        throws RemoteException, SQLException;

    int isNullable(int columnIndex)
        throws RemoteException, SQLException;

    boolean isSigned(int columnIndex)
        throws RemoteException, SQLException;

    int getColumnDisplaySize(int columnIndex)
        throws RemoteException, SQLException;

    String getColumnLabel(int columnIndex)
        throws RemoteException, SQLException;

    String getColumnName(int columnIndex)
        throws RemoteException, SQLException;

    String getSchemaName(int columnIndex)
        throws RemoteException, SQLException;

    int getPrecision(int columnIndex)
        throws RemoteException, SQLException;

    int getScale(int columnIndex)
        throws RemoteException, SQLException;

    String getTableName(int columnIndex)
        throws RemoteException, SQLException;

    String getCatalogName(int columnIndex)
        throws RemoteException, SQLException;

    int getColumnType(int columnIndex)
        throws RemoteException, SQLException;

    String getColumnTypeName(int columnIndex)
        throws RemoteException, SQLException;

    boolean isReadOnly(int columnIndex)
        throws RemoteException, SQLException;

    boolean isWritable(int columnIndex)
        throws RemoteException, SQLException;

    boolean isDefinitelyWritable(int columnIndex)
        throws RemoteException, SQLException;

    String getColumnClassName(int columnIndex)
        throws RemoteException, SQLException;

}
