
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Implements URL connection to mail.com-email provider sites. URL must be of form:<br>
    When getting input stream of message list:<br>
    mailcom://host[/from]<br>
    [?auto= : quote original automatically<br>
    &amp;contents= : show in list also message contents<br>
    &amp;date=messages newer than date are listed<br>
    &amp;from=reply email address<br>
    &amp;only=only messages newer than cookie value or date are listed]<br>
    When getting input stream of specified message:<br>
    mailcom://host[/message ID]<br>
    [?ID=id of article to be retrieved<br>
    &amp;delete= : message is only deleted<br>
    &amp;forward= : get message as raw text<br>
    &amp;header= : show message header<br>
    &amp;plain= : only plain text is returned from message<br>
    &amp;remove= : removes message after reading]<br>
    When getting output stream of unique id list of messages to be moved to some category:<br>
    pop3://host<br>
    Basic credentials must be in request property Authorization.<br>
*/
public class W3MailComURLConnection extends W3MsgURLConnection implements Cloneable {
    public static long cacheCleaningInterval = 24L * 60L * 60L * 1000L, cacheCleaningTime = 0L;
    public static String cacheRoot = "cache/pop3/";
    public static String mailComServerHost = null;
    public static String proxyHost = null;
    public static boolean defaultUseCaches = false, useProxy = false;
    public static int proxyPort = 80;

    Cookie cookies[];
    W3HttpURLConnection mailUc;
    MsgPipingThread piping;
    RegexpPool nameParts[];
    String cachePath, host;
    URL mailUrl;
    Vector<URL> urls;
    boolean forward = false;

    public Object clone() {
        W3MailComURLConnection uc = new W3MailComURLConnection(url);
        uc.requester = requester;
        uc.cachePath = cachePath;
        uc.host = host;
        uc.loggedIn = loggedIn;
        uc.urls = urls;
        return uc;
    }

    public synchronized HeaderList getHeader(W3Requester requester, MsgItem msgItem)
        throws IOException, ParseException {
        return null;
    }

    public synchronized InputStream getEntity(W3Requester requester, MsgItem msgItem)
        throws IOException, ParseException {
        mailUc.set(new URL("http", host, url.getPort(), "/mail/readmail.page?MsgId=" + index + "&sort=SORTBYDATE"));
        return null;
    }

    void delete(MsgPipingThread piping)
        throws IOException {
        /*
          piping.requester.send("DELE " + piping.msgs.get(piping.uniqueID) + "\r\n");
          try {
          checkOK(piping.requester);
          } catch (IOException ex) {
          if (ex instanceof SocketException) throw ex;
          }
        */
        new EntityCache(piping.cachePath + Support.encodePath(piping.uniqueID)).reset();
    }

    public static void cleanCache(ServletContext servletContext) {
        cacheCleaningTime = cleanCache(servletContext, cacheRoot, cacheCleaningInterval);
    }

    public W3MailComURLConnection(URL url) {
        super(url);
    }

    public void checkCacheCleaning() {
        if (cacheCleaningInterval >= 0L &&
            System.currentTimeMillis() - cacheCleaningTime > cacheCleaningInterval) cleanCache(servletContext);
    }

    public void doConnect()
        throws IOException {
        if ((host = url.getHost()).equals("") && (host = mailComServerHost) == null &&
            (host = System.getProperty("mailCom.host")) == null) throw new IOException("No host");
        getBasicCredentials();
        cachePath = cacheRoot + host.toLowerCase() + "/" + username + "/";
        StringTokenizer st = new StringTokenizer(path, "/");
        int tc = st.countTokens();
        if ((tc > 1 && useCaches || tc > 2) && st.nextToken().indexOf('@') == -1 &&
            checkCacheFile(cachePath + st.nextToken(), tc > 2 ? st.nextToken() : "") || requester != null) return;
        nameParts = new RegexpPool[] {new RegexpPool(), null};
        try {
            nameParts[0].add(new URL("http", host, url.getPort(), "/mail/readmail.page").toString(), Boolean.TRUE);
        } catch (RegexException ex) {
            throw new IOException(ex.toString());
        }
        mailUc = new W3HttpURLConnection(mailUrl = new URL("http", host, url.getPort(), "/member/login.page"));
        mailUc.connect();
    }

    public void login()
        throws IOException {
        if (loggedIn) return;
        if (username.equals("") || password.equals("")) throw new LoginException("No username or password", mailUc.requester.makeRealm());
        Vector<HtmlForm> forms = new Vector<HtmlForm>();
        InputStream mailIn = mailUc.getInputStream();
        try {
            parse(requester.input, null, null, forms, mailUrl, null, new HashMap<String,Object>(),
                  null, null, null, null, null, null, true, false);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
            return;
        }
        if (forms.isEmpty()) throw new IOException("Login form not found");
        HtmlForm form = forms.firstElement();
        Enumeration names = form.getNames().elements();
        StringBuffer sb = new StringBuffer();
        while (names.hasMoreElements()) {
            if (sb.length() > 0) sb.append('&');
            String name = (String)names.nextElement();
            if (name.equalsIgnoreCase("login_id")) sb.append(URLEncoder.encode(name, charsetName)).append('=').append(URLEncoder.encode(username, charsetName));
            else if (name.equalsIgnoreCase("password")) sb.append(URLEncoder.encode(name, charsetName)).append('=').append(URLEncoder.encode(password, charsetName));
            else {
                Object field = form.getField(name);
                String value;
                if (field instanceof Vector) {
                    value = (String)((Vector)field).firstElement();
                    ((Vector)field).removeElementAt(0);
                } else value = (String)field;
                sb.append(URLEncoder.encode(name, charsetName)).append('=').append(URLEncoder.encode(value, charsetName));
            }
        }
        mailUc.set(mailUc.getURL());
        mailUc.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        mailUc.setRequestProperty("Content-Length", String.valueOf(sb.length()));
        OutputStream mailOut = mailUc.getOutputStream();
        Support.writeBytes(mailOut, sb.toString(), null);
        mailOut.flush();
        mailIn = mailUc.getInputStream();
        cookies = getCookies((HeaderList)mailUc.getHeaderFields());
        byte b[] = new byte[Support.bufferLength];
        for (int n; (n = mailIn.read(b)) > 0;);
        loggedIn = true;
        useCaches = false;
    }

    public void close() {
        if (mailUc != null) {
            mailUc.disconnect();
            mailUc = null;
        }
        super.close();
    }

    public InputStream getInputStream()
        throws IOException {
        if (inputDone) return in;
        if (doOutput) return super.getInputStream();
        mailUc.set(new URL("http", host, url.getPort(), "/mail/mailbox.page?folder=Inbox"));
        InputStream mailIn = mailUc.getInputStream();
        try {
            parse(mailIn, urls, null, null, mailUrl, nameParts, new HashMap<String,Object>(), null, null, null, null,
                  null, null, true, false);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
            return null;
        }
        return super.getInputStream();
    }

    public void setDefaultUseCaches(boolean useCaches) {
        W3MailComURLConnection.defaultUseCaches = useCaches;
    }

    public boolean getDefaultUseCaches() {
        return W3MailComURLConnection.defaultUseCaches;
    }

    public int getDefaultPort() {
        return 80;
    }

    public boolean usingProxy() {
        return useProxy;
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public int getProxyPort() {
        return proxyPort;
    }

    public void setCacheCleaningInterval(long cacheCleaningInterval) {
        W3MailComURLConnection.cacheCleaningInterval = cacheCleaningInterval;
    }

}
