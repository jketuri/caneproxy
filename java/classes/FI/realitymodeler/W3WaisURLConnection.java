
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;
import javax.servlet.*;

class WaisPipingThread extends CacheFillingThread {
    DataInputStream din;
    EntityMemory entity = null;
    OutputStream entityOut = null;
    String base = null, databaseNames = null, host = null, path = null, query = null, seedWords = null;
    boolean list = false, mobile = false, servlet = false;
    long maxDocs, size;

    WaisPipingThread(W3WaisURLConnection uc) {
        super(uc);
        this.host = uc.host;
        this.query = uc.query;
    }

    long readCompressedLong() throws IOException {
        byte b;
        long l = 0;
        do {
            b = din.readByte();
            size--;
            l <<= 7;
            l |= b & 0x7f;
        } while ((b & 0x80) != 0);
        return l;
    }

    long readNumber() throws IOException {
        long l = 0, len = readCompressedLong();
        size -= len;
        while (len > 0) {
            l <<= 8;
            l |= din.readByte() & 0xff;
            len--;
        }
        return l;
    }

    String readString() throws IOException {
        long len = readCompressedLong();
        size -= len;
        byte b[] = new byte[(int)len];
        din.readFully(b);
        return new String(b);
    }

    int readWaisHeader() throws IOException {
        byte b[] = new byte[10];
        din.readFully(b);
        int length = Integer.parseInt(new String(b));
        byte type = din.readByte(), version = din.readByte();
        din.readFully(b);
        String server = new String(b);
        byte compression = din.readByte(), encoding = din.readByte(), checkSum = din.readByte();
        if (uc.verbose) uc.log("WaisHeader: server=" + server + ", compression=" + compression + ", encoding=" + encoding + ", checkSum=" + checkSum + ", length=" + length);
        return length;
    }

    public void run() {
        try {
            if (list) {
                writeBytes("<html><head><title>" + host + "</title>");
                writeBytes("<base target=\"message\">\n");
                writeBytes("</head><body>\n");
                writeBytes("<h2>" + MessageFormat.format(uc.messages.getString("resultsFromHost"), new Object[] {host}));
                if (!mobile) {
                    if (!databaseNames.equals("")) writeBytes("<br>\n" +
                                                              MessageFormat.format(uc.messages.getString("ofDatabases"), new Object[] {databaseNames}));
                    if (!seedWords.equals("")) writeBytes("<br>\n" +
                                                          MessageFormat.format(uc.messages.getString("withSeedWords"), new Object[] {seedWords}));
                }
                writeBytes("</h2>\n");
                if (servlet) writeBytes("<a href=\"" + path.substring(0, path.lastIndexOf('/', path.length() - 2) + 1) +
                                        (query != null ? query + "&" : "?") + "new=\" target=_top>" + uc.messages.getString("main") + "</a><br>\n");
            }
            if (readWaisHeader() == 0) throw new IOException("Empty response");
            size = din.readShort();
            byte pduType = din.readByte();
            size--;
            byte status = din.readByte();
            size--;
            if (list) writeBytes("<table>\n<tr><th>" + uc.messages.getString("searchStatus") + "</th><td>" + status +  "</td></tr>\n");
            if (uc.verbose) uc.log("Search status: " + status);
            int resultCount = din.readByte() << 16;
            size--;
            resultCount += din.readShort();
            size -= 2;
            if (list) writeBytes("<tr><th>" + uc.messages.getString("resultCount") + "</th><td>" + resultCount + "</td></tr>\n");
            if (uc.verbose) uc.log("Result count: " + resultCount);
            int recordsReturned = din.readByte() << 16;
            size--;
            recordsReturned += din.readShort();
            size -= 2;
            if (list) writeBytes("<tr><th>" + uc.messages.getString("numberOfRecordsReturned") + "</th><td>" + recordsReturned + "</td></tr>\n");
            if (uc.verbose) uc.log("Number of records returned: " + recordsReturned);
            int nextPos = din.readByte() << 16;
            size--;
            nextPos += din.readShort();
            size -= 2;
            String s;
            long l;
            while (size > 0)
                switch ((int)readCompressedLong()) {
                case 26:
                    // result-set-status
                    l = readNumber();
                    if (list) writeBytes("<tr><th>" + uc.messages.getString("resultSetStatus") + "</th><td>" + l + "</td></tr>\n");
                    if (uc.verbose) uc.log("Result set status: " + l);
                    break;
                case 27:
                    // present-status
                    l = readNumber();
                    if (list) writeBytes("<tr><th>" + uc.messages.getString("presentStatus") + "</th><td>" + l + "</td></tr>\n");
                    if (uc.verbose) uc.log("Present status: " + l);
                    break;
                case 2:	
                    // reference-id
                    s = readString();
                    if (list) writeBytes("<tr><th>" + uc.messages.getString("referenceId") + "</th><td>" + s + "</td></tr>\n");
                    if (uc.verbose) uc.log("Reference id: " + s);
                    break;
                default:
                    throw new IOException("Unknown code");
                }
            readCompressedLong();
            size = readCompressedLong();
            String fields[] = new String[((W3WaisURLConnection)uc).fieldNames.length];
            boolean gotDate = false;
            int nth = 1;
            while (size > 0) {
                switch ((int)readCompressedLong()) {
                case 28: {
                    // database-diagnostics-records
                    short len = din.readShort();
                    size -= 2 + len;
                    byte b1 = din.readByte(), b2 = din.readByte();
                    if (list) writeBytes("<tr><th>" + uc.messages.getString("diagnostics") + "</th><td>" + b1 + " " + b2 + "</td></tr>\n");
                    if (uc.verbose) uc.log("Diagnostics: " + b1 + " " + b2);
                    if  (len > 3) {
                        byte b[] = new byte[len - 3];
                        din.readFully(b);
                        if (list) writeBytes("<tr><th>" + uc.messages.getString("additionalInfo") + "</th><td>" + new String(b) + "</td></tr>\n");
                        if (uc.verbose) uc.log("Additional info: " + new String(b));
                    }
                    b1 = din.readByte();
                    if (list) writeBytes("<tr><th>" + uc.messages.getString("surrogate") + "</th><td>" + b1 + "</td></tr>\n");
                    if (uc.verbose) uc.log("Surrogate: " + b1);
                    break;
                }
                case 29:
                    break;
                case 115:
                    // seed-words-used
                    s = readString();
                    if (list) writeBytes("<tr><th>" + uc.messages.getString("seedWordsUsed") + "</th><td>" + s + "</td></tr>\n");
                    if (uc.verbose) uc.log("Seed words used: " + s);
                    break;
                case 150:
                    // document-header-group
                case 151:
                    // document-short-header-group
                case 152:
                    // document-long-header-group
                case 153:
                    // document-text-group
                case 154:
                    // document-headline-group
                case 155: {
                    // document-code-group
                    long size0 = readCompressedLong();
                    size0 = size - size0;
                    String documentID = null, type = "";
                    for (int i = 0; i < fields.length; i++) fields[i] = null;
                    int tag;
                    while (size > size0)
                        switch (tag = (int)readCompressedLong()) {
                        case 116:
                            // document-id
                            documentID = readString();
                            break;
                        case 117:
                            // version-number
                        case 118:
                            // score
                        case 119:
                            // best-match
                        case 120:
                            // document-length
                        case 131:
                            // lines
                            fields[tag >= 131 ? tag - 120 : tag - 117] = String.valueOf(readNumber());
                            break;
                        case 121:
                            // source
                        case 122:
                            // date
                        case 123:
                            // head-line
                        case 124:
                            // origin-city
                        case 128:
                            // stock-codes
                        case 129:
                            // company-codes
                        case 130:
                            // industry-codes
                            fields[tag >= 128 ? tag - 120 : tag - 117] = readString();
                            if (tag == 122) gotDate = true;
                            break;
                        case 132: {
                            // type-block
                            long size1 = readCompressedLong();
                            size1 = size - size1;
                            s = null;
                            while (size > size1) {
                                if (s == null) s = "";
                                else s += " ";
                                readCompressedLong();
                                s += type = readString();
                            }
                            fields[tag - 120] = s;
                            break;
                        }
                        case 127: {
                            // document-text
                            long size1 = readCompressedLong();
                            size -= size1;
                            byte b[] = new byte[Support.bufferLength];
                            for (int n; size1 > 0 && (n = source.read(b, 0, (int)Math.min(b.length, size1))) > 0;) {
                                out.write(b, 0, n);
                                size1 -= n;
                            }
                            out.close();
                            return;
                        }
                        default:
                            throw new IOException("Unknown code");
                        }
                    if (list) {
                        writeBytes("</table>\n<hr>\n");
                        String nthDocument = MessageFormat.format(uc.messages.getString("nthDocument"), new Object[] {new Long(nth)});
                        if (base != null && gotDate) {
                            if ((s = fields[6]) != null) {
                                if (File.separatorChar == '\\') s = s.replace('\\', '/');
                                writeBytes("<a href=\"" + base + s + "\">" + nthDocument + "</a><br>\n");
                            }
                        } else if (documentID != null)
                            writeBytes("<a href=\"" + path + type + "/" + Support.encodePath(documentID) + "\">" + nthDocument + "</a><br>\n");
                        writeBytes("<table>\n");
                        for (int i = 0; i < fields.length; i++)
                            if (fields[i] != null) writeBytes("<tr><th>" + ((W3WaisURLConnection)uc).fieldNames[i] + ":</th><td>" + Support.htmlString(fields[i]) + "</td></tr>\n");
                    }
                    nth++;
                }
                    break;
                default:
                    throw new IOException("Unknown code");
                }
            }
            if (list) writeBytes("</table></body></html>\n");
            out.close();
            if (entity != null) entity.setReadOnly();
        } catch (Exception ex) {
            try {
                if (in != null)
                    synchronized (in) {
                        in.close();
                        in.notifyAll();
                    }
            } catch (IOException ex1) {} finally {
                if (entity != null)
                    try {
                        entityOut.close();
                        entity.delete("");
                    } catch (IOException ex1) {}
                uc.log(getClass().getName(), ex);
            }
        }
    }

}

/** Implements URL connection to Wide Area Information Servers. URLs must be of form (RFC 1739):<br>
    Get input stream of result list:<br>
    wais://host[:port]/database names[/max. number of documents]?seed words<br>
    [&amp;base=base url if head line of records contains file path]<br>
    Get input stream of specific document:<br>
    wais://host[:port]/database name/wais type/wais path<br>
    [;type=h(tml, default) or p(lain)]<br>
    [?start=chunk start position<br>
    &amp;end=chunk end position<br>
    &amp;type=chunk type which is wb(yte), wl(ine, default) or wp(aragraph)]<br>
*/
public class W3WaisURLConnection extends W3URLConnection {
    public static final int WAIS_PORT = 210;
    public static long cacheCleaningInterval = 24L * 60L * 60L * 1000L, cacheCleaningTime = 0L;
    public static String cacheRoot = "cache/wais/";
    public static boolean defaultUseCaches = false, useProxy = false;
    public static String proxyHost = null;
    public static int proxyPort = 80;
    public static String fieldNames[] = {
        messages.getString("versionNumber"),
        messages.getString("score"),
        messages.getString("bestMatch"),
        messages.getString("documentLength"),
        messages.getString("source"),
        messages.getString("date"),
        messages.getString("headLine"),
        messages.getString("originCity"),
        messages.getString("stockCodes"),
        messages.getString("companyCodes"),
        messages.getString("industryCodes"),
        messages.getString("lines"),
        messages.getString("type")
    };

    DataOutputStream dout;
    String host;

    public String getCacheRoot() {
        return cacheRoot;
    }

    public static void cleanCache(ServletContext servletContext) {
        cacheCleaningTime = cleanCache(servletContext, cacheRoot, cacheCleaningInterval);
    }

    public W3WaisURLConnection(URL url) {
        super(url);
    }

    public void checkCacheCleaning() {
        if (cacheCleaningInterval >= 0L &&
            System.currentTimeMillis() - cacheCleaningTime > cacheCleaningInterval) cleanCache(servletContext);
    }

    public void open() throws IOException {
        if (useProxy) {
            proxyConnect(proxyHost, proxyPort);
            return;
        }
        requester = new W3Requester(host, url.getPort() != -1 ? url.getPort() : WAIS_PORT);
        if (timeout > 0) requester.setTimeout(timeout);
    }

    public void doConnect()
        throws IOException {
        if (requester != null) return;
        if ((host = url.getHost()).equals("-")) host = InetAddress.getLocalHost().getHostAddress();
        String cachePath = cacheRoot + host.toLowerCase() + (url.getPort() != -1 &&
                                                             url.getPort() != WAIS_PORT ? host + "#" + url.getPort() : host).toLowerCase() + "/";
        StringTokenizer st = new StringTokenizer(path, "/");
        int tc = st.countTokens();
        if (tc > 1 && checkCacheFile(cachePath, st.nextToken("").substring(1))) return;
        open();
    }

    void writeCompressedLong(long l) throws IOException {
        long m = 0;
        while (l != 0) {
            m <<= 7;
            m |= l & 0x7f;
            l >>= 7;
        }
        byte b;
        for (;;) {
            b = (byte)(m & 0x7f);
            if ((m >>= 7) == 0) {
                dout.writeByte(b);
                break;
            }
            dout.writeByte(b | 0x80);
        }
    }

    void writeNumber(long l) throws IOException {
        long len = 0, m = 0;
        do {
            m <<= 8;
            m |= l & 0xff;
            l >>= 8;
            len++;
        } while (l != 0);
        writeCompressedLong(len);
        do dout.write((int)(m & 0xff));
        while ((m >>= 8) != 0);
    }

    void writeString(String s) throws IOException {
        writeCompressedLong(s.length());
        dout.writeBytes(s);
    }

    void writeWaisHeader(int len) throws IOException {
        String s = String.valueOf(len);
        if (s.length() < 10) dout.writeBytes("0000000000".substring(s.length()));
        // header-length
        dout.writeBytes(s);
        // type
        dout.writeByte('z');
        // version
        dout.writeByte('2');
        // server
        dout.writeBytes("wais      ");
        // compression
        dout.writeByte(' ');
        // encoding
        dout.writeByte(' ');
        dout.writeByte('0');
        dout.flush();
    }

    public InputStream getInputStream() throws IOException {
        if (inputDone) return in;
        connect();
        inputDone = true;
        setRequestMethod();
        setRequestFields();
        if (useProxy) return getInput(false);
        mayKeepAlive = true;
        String chunkType = "wl", chunkStart = "0", chunkEnd = String.valueOf(Integer.MAX_VALUE >> 8),
            documentID = null, type = null, s;
        WaisPipingThread piping = new WaisPipingThread(this);
        filling = piping;
        piping.mobile = isMobile();
        piping.maxDocs = 200;
        piping.base = null;
        piping.list = true;
        piping.seedWords = "";
        if (query != null) {
            HashMap<String,Object> qp = new HashMap<String,Object>();
            parseQuery(query.substring(1), qp);
            if ((s = Support.getParameter(qp, "")) != null) piping.seedWords = Support.unquoteString(s);
            if ((piping.base = Support.getParameter(qp, "base")) != null &&
                (piping.base = piping.base.trim()).equals("")) piping.base = "http://" + host + "/";
            if ((s = Support.getParameter(qp, "start")) != null && !(s = s.trim()).equals("")) chunkStart = s;
            if ((s = Support.getParameter(qp, "end")) != null && !(s = s.trim()).equals("")) chunkEnd = s;
            if ((s = Support.getParameter(qp, "type")) != null && !(s = s.trim()).equals("")) chunkType = s;
        }
        path = Support.getParameters(path, null);
        piping.databaseNames = "";
        StringTokenizer st = new StringTokenizer(path, "/");
        if (st.hasMoreTokens()) {
            piping.databaseNames = st.nextToken().trim();
            if (piping.databaseNames.equals("-")) piping.databaseNames = "";
            if (st.hasMoreTokens())
                if ((type = st.nextToken().trim()).length() == 0 || !Character.isDigit(type.charAt(0))) {
                    piping.list = false;
                    if (st.hasMoreTokens()) {
                        documentID = Support.decodePath(st.nextToken("").substring(1));
                        piping.maxDocs = 1;
                    }
                } else piping.maxDocs = Long.parseLong(type);
        }
        checkCacheCleaning();
        ByteArrayOutputStream bout = new ByteArrayOutputStream(), bout1 = new ByteArrayOutputStream();
        dout = new DataOutputStream(bout);
        DataOutputStream dout1 = new DataOutputStream(bout1);
        in = requester.input;
        piping.din = new DataInputStream(in);
        // pdu-type: searchAPDU
        dout.writeByte(22);
        // small-set-upper-bound
        dout.writeByte(0x00);
        dout.writeShort(0x0400);
        // large-set-lower-bound
        dout.writeByte(0x00);
        dout.writeShort(0x0800);
        // medium-set-present-number
        dout.writeByte(0x00);
        dout.writeShort(0x0800);
        // replace-indicator
        dout.writeByte(0x01);
        st = new StringTokenizer(piping.databaseNames);
        while (st.hasMoreTokens()) {
            // database-names
            dout.writeByte(18);
            writeString(st.nextToken());
        }
        // query-type
        dout.writeByte(20);
        dout.writeByte(0x01);
        // relevance-feedback-query or text-retrieval-query
        dout.writeByte(piping.list ? '3' : '1');
        dout1.writeShort(bout.size());
        bout.writeTo(dout1);
        bout.reset();
        if (piping.list) {
            // seed-words
            dout.writeByte(106);
            writeString(piping.seedWords);
            // max-documents-retrieved
            dout.writeByte(114);
            writeNumber(piping.maxDocs);
            dout = dout1;
            // user-information-length
            writeCompressedLong(99);
        } else {
            // attribute-list
            dout.writeByte(44);
            writeString("un re ig ig ig ig");
            // system-control-number equal
            // term
            writeCompressedLong(45);
            writeString(documentID);
            // attribute-list
            dout.writeByte(44);
            writeString("wt re ig ig ig ig");
            // data-type equal
            // system-control-number equal
            // term
            writeCompressedLong(45);
            dout.writeByte(0x00);
            // operator
            writeCompressedLong(46);
            writeString("a");
            // attribute-list
            dout.writeByte(44);
            writeString(chunkType + " ro ig ig ig ig");
            // byte greater-than-or-equal
            // term
            writeCompressedLong(45);
            writeString(chunkStart);
            writeCompressedLong(46);
            // operator
            writeString("a");
            // attribute-list
            dout.writeByte(44);
            writeString(chunkType + " rp ig ig ig ig");
            // byte less-than-or-equal
            // term
            writeCompressedLong(45);
            writeString(chunkEnd);
            writeCompressedLong(46);
            // operator
            writeString("a");
            dout = dout1;
            // query
            writeCompressedLong(21);
        }
        writeCompressedLong(bout.size());
        bout.writeTo(dout);
        dout = new DataOutputStream(requester.output);
        writeWaisHeader(bout1.size());
        bout1.writeTo(dout);
        dout.flush();
        String ct;
        if (piping.list) ct = Support.htmlType;
        else if ((ct = guessContentType("." + type)) == null) ct = Support.unknownType;
        headerFields = new HeaderList();
        headerFields.append(new Header("", protocol_1_1 + " " + HTTP_OK + " OK"));
        headerFields.append(new Header("Content-Type", ct));
        if (piping.list) {
            headerFields.append(new Header("Cache-Control", "no-cache"));
            headerFields.append(new Header("Pragma", "no-cache"));
        }
        Support.copyHeaderList(headerFields, headerList = new HeaderList());
        piping.servlet = requestRoot != null;
        piping.path = piping.servlet ? requestRoot : path.endsWith("/") ? "" : path.substring(path.lastIndexOf('/') + 1) + "/";
        piping.setup(this);
        in = new BufferedInputStream(piping.in = new PipedInputStream(piping.out = new PipedOutputStream()));
        piping.start();
        return checkMessage(true);
    }

    public void setDefaultUseCaches(boolean useCaches) {
        W3WaisURLConnection.defaultUseCaches = useCaches;
    }

    public boolean getDefaultUseCaches() {
        return W3WaisURLConnection.defaultUseCaches;
    }

    public int getDefaultPort() {
        return WAIS_PORT;
    }

    public boolean usingProxy() {
        return useProxy;
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public int getProxyPort() {
        return proxyPort;
    }

}
