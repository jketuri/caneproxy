
/* Functions handling socket sets. */

#ifdef _WIN32
#ifdef __GNUC__
#define __USE_W32_SOCKETS
#include <windows.h>
#define FD_CAST
#else
#include <winsock2.h>
#define FD_CAST (unsigned long)
#endif
#else
#include <sys/socket.h>
#include <sys/time.h>
#define FD_CAST
#endif
#include "FI_realitymodeler_SocketSet.h"

extern "C" JNIEXPORT jlong JNICALL Java_FI_realitymodeler_SocketSet_construct(JNIEnv *env, jobject obj)
{
    return (jlong)new char[sizeof(fd_set)];
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_SocketSet_destroy(JNIEnv *env, jobject obj, jlong fdSet)
{
    delete (char *)fdSet;
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_SocketSet_clr0(JNIEnv *env, jobject obj, jlong fdSet, jint fd)
{
    FD_CLR(FD_CAST fd, (fd_set *)fdSet);
}

extern "C" JNIEXPORT jboolean JNICALL Java_FI_realitymodeler_SocketSet_isSet0(JNIEnv *env, jobject obj, jlong fdSet, jint fd)
{
    return FD_ISSET(fd, (fd_set *)fdSet);
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_SocketSet_set0(JNIEnv *env, jobject obj, jlong fdSet, jint fd)
{
    FD_SET(FD_CAST fd, (fd_set *)fdSet);
}

extern "C" JNIEXPORT void JNICALL Java_FI_realitymodeler_SocketSet_zero0(JNIEnv *env, jobject obj, jlong fdSet)
{
    FD_ZERO((fd_set *)fdSet);
}
