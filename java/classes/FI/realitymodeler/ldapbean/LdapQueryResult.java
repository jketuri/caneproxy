
package FI.realitymodeler.ldapbean;

import java.util.*;
import javax.naming.*;
import javax.naming.directory.*;

public class LdapQueryResult {
    private BasicAttributes basicAttributes;
    private String objectString = null;

    LdapQueryResult(SearchResult searchResult, LdapBean ldapBean) throws NamingException {
        basicAttributes = new BasicAttributes(true);
        Attributes attributes = searchResult.getAttributes();
        Iterator iter = (ldapBean.attributeAliases.isEmpty() && ldapBean.ldapBean != null ? ldapBean.ldapBean.attributeAliases : ldapBean.attributeAliases).entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry entry = (Map.Entry)iter.next();
            Attribute attribute = attributes.get((String)entry.getKey());
            if (attribute == null) continue;
            BasicAttribute basicAttribute = new BasicAttribute((String)entry.getValue());
            for (NamingEnumeration valueEnum = attribute.getAll(); valueEnum.hasMore();) basicAttribute.add(valueEnum.next());
            basicAttributes.put(basicAttribute);
        }
        if (ldapBean.getObjectParameter() == null) return;
        Object object = searchResult.getObject();
        if (object == null) return;
        objectString = object instanceof String ? (String)object : object.toString();
        BasicAttribute basicAttribute = new BasicAttribute(ldapBean.getObjectParameter(), objectString);
        basicAttributes.put(basicAttribute);
    }

    public String getAttribute(String name) throws NamingException {
        return LdapBean.getAttributeValue(basicAttributes, name, 0);
    }

    public String getIndexedAttribute(String name, int index) throws NamingException {
        return LdapBean.getAttributeValue(basicAttributes, name, index);
    }

    public String getObject() {
        return objectString;
    }

}
