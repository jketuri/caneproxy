
package FI.realitymodeler.ldapbean;

import FI.realitymodeler.common.*;
import java.io.*;
import java.text.*;
import java.util.*;
import javax.naming.*;
import javax.naming.directory.*;
import javax.naming.ldap.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** General Ldap Bean, which can query and modify directory. */
public class LdapBean {
    static HashMap<ServletConfig,LdapBean> ldapBeans = new HashMap<ServletConfig,LdapBean>();

    BasicAttributes userAttributes = null;
    Boolean activeDirectory = null;
    Boolean byteArrayValues = null;
    Boolean loginAlways = null;
    Boolean modifyOnly = null;
    Boolean serverLogin = null;
    Boolean shortPrincipalFormat = null;
    Boolean snFromCn = null;
    HashMap<String,String> userParameters = null;
    HashMap<String,String> customValues = new HashMap<String,String>();
    HashMap<String,String> attributeAliases = new HashMap<String,String>();
    HashMap<String,String> parameterAliases = new HashMap<String,String>();
    HashMap<String,String> searchFilterPatterns = new HashMap<String,String>();
    Hashtable<String,String> environment = new Hashtable<String,String>();
    InitialLdapContext ldapContext = null;
    Integer timeLimit = null;
    LdapBean ldapBean = null;
    LdapQueryResult queryResult = null;
    Long countLimit = null;
    NamingEnumeration searchResultEnum = null;
    String action = null;
    String attributes = null;
    String dn = null;
    String dnPattern = null;
    String filter = null;
    String filterPattern = null;
    String freeSearchFilterPattern = null;
    String host = null;
    String objectClass = null;
    String objectParameter = null;
    String result = "failed";
    String state = "started";
    String scope = null;
    String searchFilterPattern = null;
    String sortAttributes = null;
    String passwordAttribute = null;
    String usernameAttribute = null;
    SearchResult firstSearchResult = null;
    SearchResult secondSearchResult = null;
    ServletConfig servletConfig = null;
    ServletContext servletContext = null;
    boolean loopEnded = false;
    boolean verbose = false;
    int currentIndex = 1;
    int loopCount = 0;
    int skipNumber = 0;

    /** Gets indexed attribute value.
        @param attribute attribute whose value is queried
        @param index index number of value which is queried
        @return attribute value as String or null if not found. */
    public static final String getAttributeValue(Attribute attribute, int index)
        throws NamingException {
        try {
            Object value = attribute != null && attribute.size() > index ? attribute.get(index) : null;
            if (value instanceof String) return (String)value;
            if (value instanceof byte[]) return new String((byte[])value);
        } catch (NoSuchElementException ex) {}
        return null;
    }

    /** Gets indexed value of attribute with specified name.
        @param attributes attributes whose attribute is queried
        @param name name of attribute which is queried
        @param index index number of value which is queried
        @return attribute value as String or null if not found. */
    public static final String getAttributeValue(Attributes attributes, String name, int index)
        throws NamingException {
        Attribute attribute = attributes.get(name.toLowerCase());
        if (attribute == null) return null;
        return getAttributeValue(attribute, index);
    }

    /** Sets action to be done when accessing LDAP server. Action can be one
        of the following:<br>

        save Tries to rebind object with associated attributes to the
        directory.<br>

        change Tries to rebind object to the directory modifying 
        associated attributes.<br>

        delete Tries to unbind object from the directory.<br>

        login Forces login to directory. Credentials supplied
        in environment properties are not used in this case.<br>

        Distinguished name is constructed in all cases using dnPattern-property
        if available, otherwise parameter dn is used. 

        @param action Action to be done. */
    public void setAction(String action) {
        this.action = action;
    }

    public String getAction() {
        return action != null ? action : ldapBean != null ? ldapBean.getAction() : null;
    }

    /** Indicates that MS Active Directory is used as LDAP-server. */
    public void setActiveDirectory(boolean activeDirectory) {
        this.activeDirectory = new Boolean(activeDirectory);
    }

    public boolean getActiveDirectory() {
        return activeDirectory != null ? activeDirectory.booleanValue() : ldapBean != null ? ldapBean.getActiveDirectory() : false;
    }

    /** Gets named attribute value in current query result.

        @param attributeName name of the attribute

        @return attribute value or null if not found. */

    public String getAttribute(String attributeName)
        throws NamingException {
        if (queryResult == null) queryResult = getQueryResult(0);
        return queryResult != null ? queryResult.getAttribute(attributeName) : null;
    }

    /** Sets alias for attribute name. Only one alias can be given to
        specific attribute name. 

        @param attributeAlias alias for attribute name

        @param attributeName attribute name */

    public void setAttributeAlias(String attributeAlias, String attributeName) {
        attributeAliases.put(attributeName, attributeAlias);
    }

    public String getAttributeAlias(String attributeName) {
        String alias = attributeAliases.get(attributeName = attributeName.toLowerCase());
        return alias != null ? alias : ldapBean != null ? ldapBean.getAttributeAlias(attributeName) : attributeName;
    }

    /** Sets list of attribute names whose values are to be returned in
        queries. If this is not specified, all attributes are returned. 

        @param attributes list of attribute names separated with comma (,) */

    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    public String getAttributes() {
        return attributes != null ? attributes : ldapBean != null ? ldapBean.getAttributes() : null;
    }

    public void setByteArrayValues(boolean byteArrayValues) {
        this.byteArrayValues = new Boolean(byteArrayValues);
    }

    public boolean getByteArrayValues() {
        return byteArrayValues != null ? byteArrayValues.booleanValue() : ldapBean != null ? ldapBean.getByteArrayValues() : false;
    }

    /** Sets count limit of results to be returned in queries.

        @param countLimit count limit of results */
    public void setCountLimit(long countLimit) {
        this.countLimit = new Long(countLimit);
    }

    public long getCountLimit() {
        return countLimit != null ? countLimit.longValue() : ldapBean != null ? ldapBean.getCountLimit() : 0L;
    }

    public void setShortPrincipalFormat(boolean shortPrincipalFormat) {
        this.shortPrincipalFormat = new Boolean(shortPrincipalFormat);
    }

    public boolean getShortPrincipalFormat() {
        return shortPrincipalFormat != null ? shortPrincipalFormat.booleanValue() : ldapBean != null ? ldapBean.getShortPrincipalFormat() : false;
    }

    /** Sets named custom parameter which can be accessed from template
        pages. Ldap bean self doesn't use these values in any way. 

        @param parameter parameter name to be set

        @param value of the parameter to be set */

    public void setCustom(String parameter, String value) {
        customValues.put(parameter, value);
    }

    public String getCustom(String parameter) {
        String custom = customValues.get(parameter);
        return custom != null ? custom : ldapBean != null ? ldapBean.getCustom(parameter) : null;
    }

    /** Sets distinguished name to be used as context when searching from directory.

        @param dn distinguished name */

    public void setDn(String dn) {
        this.dn = dn;
    }

    public String getDn() {
        return dn != null ? dn : ldapBean != null ? ldapBean.getDn() : "";
    }

    /** Sets distinguished name pattern which is used when modifying the
        directory. Attribute names enclosed in braces {} can be used inside
        pattern, where they are replaced with values from request. 

        @param dnPattern distinguished name pattern

        @see setAction */

    public void setDnPattern(String dnPattern) {
        this.dnPattern = dnPattern;
    }

    public String getDnPattern() {
        return dnPattern != null ? dnPattern : ldapBean != null ? ldapBean.getDnPattern() : null;
    }

    /** Sets environment property which is used when obtaining the initial
        context to directory. 

        @param propertyName name of the property

        @param value value of the property. */

    public void setEnvironment(String propertyName, String value) {
        environment.put(propertyName, value);
    }

    public String getEnvironment(String propertyName) {
        String property = environment.get(propertyName);
        return property != null ? property : ldapBean != null ? ldapBean.getEnvironment(propertyName) : null;
    }

    /** Sets filter which is used when querying directory and no property searchString is specified.

        @param filter LDAP filter */

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public String getFilter() {
        return filter != null ? filter : ldapBean != null ? ldapBean.getFilter() : null;
    }

    /** Sets filter pattern which is used when querying directory and no
        property searchString is specified. Attributes names enclosed in braces {} can be used inside pattern, where they are replaced with values from
        request. 

        @param filterPattern filter pattern */
    public void setFilterPattern(String filterPattern) {
        this.filterPattern = filterPattern;
    }

    public String getFilterPattern() {
        return filterPattern != null ? filterPattern : ldapBean != null ? ldapBean.getFilterPattern() : null;
    }

    /** Sets free search filter pattern which is used when property
        searchString is specified but property searchAttribute is not.
        Attribute names enclosed in braces {} can be used inside pattern, where
        they are replaced with values from request.  Empty braces {} indicate
        place where value of property searchString is embedded. 

        @param freeSearchFilterPattern free search filter pattern.

        @see setSearchFilterPattern */
    public void setFreeSearchFilterPattern(String freeSearchFilterPattern) {
        this.freeSearchFilterPattern = freeSearchFilterPattern;
    }

    public String getFreeSearchFilterPattern() {
        return freeSearchFilterPattern != null ? freeSearchFilterPattern : ldapBean != null ? ldapBean.getFreeSearchFilterPattern() : null;
    }

    /** Sets host name where LDAP server is residing. If this is not specified,
        localhost is assumed.

        @param host host name */

    public void setHost(String host) {
        this.host = host;
    }

    public String getHost() {
        return host != null ? host : ldapBean != null ? ldapBean.getHost() : null;
    }

    /** Gets attribute value of current result from specified index.

        @param attributeName name of the attribute

        @param index index of value */

    public String getIndexedAttribute(String attributeName, int index)
        throws NamingException {
        if (queryResult == null) queryResult = getQueryResult(0);
        return queryResult != null ? queryResult.getIndexedAttribute(attributeName, index) : null;
    }

    public void setLoginAlways(boolean loginAlways) {
        this.loginAlways = new Boolean(loginAlways);
    }

    public boolean getLoginAlways() {
        return loginAlways != null ? loginAlways.booleanValue() : ldapBean != null ? ldapBean.getLoginAlways() : false;
    }

    public void setLoopCount(int loopCount) {
        this.loopCount = loopCount;
    }

    public int getLoopCount() {
        return loopCount;
    }

    public void setLoopEnded(boolean loopEnded) {
        this.loopEnded = loopEnded;
    }

    public boolean getLoopEnded() {
        return loopEnded;
    }

    public boolean getLoopStarted() {
        return skipNumber == 0;
    }

    public void setModifyOnly(boolean modifyOnly) {
        this.modifyOnly = new Boolean(modifyOnly);
    }

    public boolean getModifyOnly() {
        return modifyOnly != null ? modifyOnly.booleanValue() : ldapBean != null ? ldapBean.getModifyOnly() : false;
    }

    public int getNewSkipNumber() {
        return skipNumber + loopCount;
    }

    /** Gets object value of current query result.

        @return object value */

    public Object getObject()
        throws NamingException {
        if (queryResult == null) queryResult = getQueryResult(0);
        return queryResult != null ? queryResult.getObject() : null;
    }

    /** Sets object class where objects are stored in the directory.

        @param objectClass name of the LDAP object class. */

    public void setObjectClass(String objectClass) {
        this.objectClass = objectClass;
    }

    public String getObjectClass() {
        return objectClass != null ? objectClass : ldapBean != null ? ldapBean.getObjectClass() : null;
    }

    /** Sets name of the parameter where object is placed in the request. 

        @param objectParameter name of the object parameter. */
    public void setObjectParameter(String objectParameter) {
        this.objectParameter = objectParameter;
    }

    public String getObjectParameter() {
        return objectParameter != null ? objectParameter : ldapBean != null ? ldapBean.getObjectParameter() : null;
    }

    /** Sets alias name for parameter names. When making request to Ldap
        Bean, this alias name can be used in place of parameter name. For
        example 'o' can be set to be alias for 'objectClass'. This method can be
        also used to set value alias for searchAttribute and objectClass. If
        value of these parameters is found as parameterAlias it is replaced with
        parameterName. */

    public void setParameterAlias(String parameterAlias, String parameterName) {
        parameterAliases.put(parameterName, parameterAlias);
    }

    public String getParameterAlias(String parameterName) {
        String alias = parameterAliases.get(parameterName);
        return alias != null ? alias : ldapBean != null ? ldapBean.getParameterAlias(parameterName) : parameterName;
    }

    public void setPasswordAttribute(String passwordAttribute) {
        this.passwordAttribute = passwordAttribute;
    }

    public String getPasswordAttribute() {
        return passwordAttribute != null ? passwordAttribute : ldapBean != null ? ldapBean.getPasswordAttribute() : null;
    }

    public void setUsernameAttribute(String usernameAttribute) {
        this.usernameAttribute = usernameAttribute;
    }

    public String getUsernameAttribute() {
        return usernameAttribute != null ? usernameAttribute : ldapBean != null ? ldapBean.getUsernameAttribute() : null;
    }

    /** Gets next query result.

        @param index index of the result. This index can actually only go up. 

        @return query result */

    public LdapQueryResult getQueryResult(int index)
        throws NamingException {
        SearchResult searchResult = null;
        if (firstSearchResult != null) {
            searchResult = firstSearchResult;
            firstSearchResult = null;
        } else if (secondSearchResult != null) {
            searchResult = secondSearchResult;
            secondSearchResult = null;
        } else if (searchResultEnum == null) return null;
        else try {
                for (;currentIndex < index; currentIndex++) searchResult = (SearchResult)searchResultEnum.next();
            } catch (NoSuchElementException ex) {
                searchResultEnum.close();
                searchResultEnum = null;
                return null;
            }
        return queryResult = new LdapQueryResult(searchResult, this);
    }

    /** Gets result of processing request. Result can be one of the following:

        failed Failed to process the request.<br>

        foundOne Found one result in query.<br>

        foundMany Found many result in query.<br>

        deleted Deleted object from directory.<br>

        saved Saved object with associated attributes to the directory.<br>

        changed Changed object in the directory without modifying associated attributes.

        @return result value. */

    public String getResult() {
        return result;
    }

    /** Sets scope of the query. This can be one of the following:<br>

        base Object scope query.<br>

        sub Subtree scope query.<br>

        one One level scope query. This is the default value.

        @param scope Scope value. */

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getScope() {
        return scope != null ? scope : ldapBean != null ? ldapBean.getScope() : null;
    }

    /** Sets search filter pattern which is used when properties
        searchString and searchAttribute are specified.  Attribute names
        enclosed in braces {} can be used inside pattern, where they are
        replaced with values from request. Empty braces {} indicate place where
        value of property searchString is embedded.

        @param searchFilterPattern search filter pattern.

        @see setFreeSearchFilterPattern */
    public void setSearchFilterPattern(String searchFilterPattern) {
        this.searchFilterPattern = searchFilterPattern;
    }

    public String getSearchFilterPattern() {
        return searchFilterPattern != null ? searchFilterPattern : ldapBean != null ? ldapBean.getSearchFilterPattern() : null;
    }

    /** Sets attribute specified search filter pattern which is used when
        property searchString is specified and property searchAttribute is set
        to specified attribute. Attribute names enclosed in braces {} can be
        used insided pattern, where they are replaced with values from request.
        Empty braces {} indicate place where value of property searchString is
        embedded. 

        @param attributeName name of the attribute

        @param searchFilterPattern search filter pattern */

    public void setSearchFilterPattern(String attributeName, String searchFilterPattern) {
        searchFilterPatterns.put(attributeName, searchFilterPattern);
    }

    public String getSearchFilterPattern(String attributeName) {
        String searchFilterPattern = searchFilterPatterns.get(attributeName);
        return searchFilterPattern != null ? searchFilterPattern : ldapBean != null ? ldapBean.getSearchFilterPattern(attributeName) : null;
    }

    /** Sets flag, which indicates that login is done always by the server
        and is not required to be done by the user. */

    public void setServerLogin(boolean serverLogin) {
        this.serverLogin = new Boolean(serverLogin);
    }

    public boolean getServerLogin() {
        return serverLogin != null ? serverLogin.booleanValue() : ldapBean != null ? ldapBean.getServerLogin() : false;
    }

    /** Sets flag which indicates if sn-attribute is obtained from
        cn-attribute by taking it's first word.

        @param snFromCn flag indicating if sn-attribute is taken from cn-attribute */

    public void setSnFromCn(boolean snFromCn) {
        this.snFromCn = new Boolean(snFromCn);
    }

    public boolean getSnFromCn() {
        return snFromCn != null ? snFromCn.booleanValue() : ldapBean != null ? ldapBean.getSnFromCn() : false;
    }

    public void setSortAttributes(String sortAttributes) {
        this.sortAttributes = sortAttributes;
    }

    public String getSortAttributes() {
        return sortAttributes != null ? sortAttributes : ldapBean != null ? ldapBean.getSortAttributes() : null;
    }

    /** Gets state after processing request. State can be one of the
        following:<br>

        started No attributes were specified and property searchString was
        null<br>

        confirm Properties operation and confirmOperation were identical which
        means that specifed operation should be confirmed.<br>

        login Login to the directory failed and relogin is required.<br>

        updated Directory was updated with save- or change-action.<br>

        searched Directory was queried.<br>

        @return state value. */

    public String getState() {
        return state;
    }

    /** Sets time limit in milliseconds used when querying the directory.

        @param timeLimit time limit in milliseconds. */

    public void setTimeLimit(int timeLimit) {
        this.timeLimit = new Integer(timeLimit);
    }

    public int getTimeLimit() {
        return timeLimit != null ? timeLimit.intValue() : ldapBean != null ? ldapBean.getTimeLimit() : 0;
    }

    /** Returns attribute value with name which is mapped to actual
        attribute name. 

        @param name name which is mapped to attribute name.

        @return attribute value or null if not found. */

    public String getUserAttribute(String name)
        throws NamingException {
        return getAttributeValue(userAttributes, name, 0);
    }

    /** Returns parameter value which was given in request with name
        which is mapped to actual parameter name.

        @param name name which is mapped to parameter name.

        @return parameter value or null if not found. */

    public String getUserParameter(String name) {
        return userParameters.get(name);
    }

    public boolean getVerbose() {
        return verbose;
    }

    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    public void setServlet(String property, String value)
        throws ServletException, ParseException {
        StringTokenizer st = new StringTokenizer(property, ":");
        if (!st.hasMoreTokens() || !st.nextToken().equalsIgnoreCase("ldap")) return;
        value = Support.decode(value);
        if (verbose) servletContext.log("setServlet " + property + " to " + value);
        String name = st.nextToken().trim();
        if (name.equals("action")) setAction(value);
        else if (name.equals("activeDirectory")) setActiveDirectory(Support.isTrue(value, name));
        else if (name.equals("attributeAlias")) setAttributeAlias(st.nextToken(), value);
        else if (name.equals("attributes")) setAttributes(value);
        else if (name.equals("byteArrayValues")) setByteArrayValues(Support.isTrue(value, name));
        else if (name.equals("countLimit")) setCountLimit(Long.parseLong(value));
        else if (name.equals("custom")) setCustom(st.nextToken(), value);
        else if (name.equals("dn")) setDn(value);
        else if (name.equals("dnPattern")) setDnPattern(value);
        else if (name.equals("environment")) setEnvironment(st.nextToken(), value);
        else if (name.equals("filter")) setFilter(value);
        else if (name.equals("filterPattern")) setFilterPattern(value);
        else if (name.equals("freeSearchFilterPattern")) setFreeSearchFilterPattern(value);
        else if (name.equals("host")) setHost(value);
        else if (name.equals("loginAlways")) setLoginAlways(Support.isTrue(value, name));
        else if (name.equals("modifyOnly")) setModifyOnly(Support.isTrue(value, name));
        else if (name.equals("objectClass")) setObjectClass(value);
        else if (name.equals("objectParameter")) setObjectParameter(value);
        else if (name.equals("parameterAlias")) setParameterAlias(st.nextToken(), value);
        else if (name.equals("passwordAttribute")) setPasswordAttribute(value);
        else if (name.equals("scope")) setScope(value);
        else if (name.equals("searchFilterPattern")) {
            if (st.hasMoreTokens()) setSearchFilterPattern(st.nextToken(), value);
            else setSearchFilterPattern(value);
        } else if (name.equals("serverLogin")) setServerLogin(Support.isTrue(value, name));
        else if (name.equals("shortPrincipalFormat")) setShortPrincipalFormat(Support.isTrue(value, name));
        else if (name.equals("snFromCn")) setSnFromCn(Support.isTrue(value, name));
        else if (name.equals("sortAttributes")) setSortAttributes(value);
        else if (name.equals("timeLimit")) setTimeLimit(Integer.parseInt(value));
        else if (name.equals("usernameAttribute")) setUsernameAttribute(value);
        else throw new ServletException("Unknown ldap bean servlet parameter " + name);
    }

    public void setServletConfig(ServletConfig servletConfig)
        throws ServletException, ParseException {
        if (servletConfig == null) return;
        this.servletConfig = servletConfig;
        servletContext = servletConfig.getServletContext();
        if (Support.isTrue(servletConfig.getInitParameter("verbose"), "verbose") ||
            servletContext.getAttribute("FI.realitymodeler.server.W3Server/verbose") == Boolean.TRUE) setVerbose(true);
        if ((ldapBean = ldapBeans.get(servletConfig)) != null) return;
        ldapBean = new LdapBean();
        ldapBean.servletContext = servletContext;
        if (getVerbose()) ldapBean.setVerbose(true);
        Enumeration initParameterNames = servletConfig.getInitParameterNames();
        while (initParameterNames.hasMoreElements()) {
            String name = (String)initParameterNames.nextElement();
            ldapBean.setServlet(name, servletConfig.getInitParameter(name));
        }
        ldapBeans.put(servletConfig, ldapBean);
    }

    String fillPattern(String pattern, Attributes attributes, String searchAttribute, String searchString, boolean filter, boolean change)
        throws NamingException, ServletException {
        StringBuffer patternBuf = new StringBuffer();
        int i = 0, j;
        while ((j = pattern.indexOf('{', i)) != -1) {
            patternBuf.append(pattern.substring(i, j));
            if ((i = pattern.indexOf('}', j + 1)) == -1) throw new ServletException("Invalid pattern " + pattern);
            String name = pattern.substring(j + 1, i++).trim();
            Attribute attribute = attributes.get(name);
            if (change) attributes.remove(name);
            if (attribute != null && attribute.size() > 0)
                try {
                    String value = (String)attribute.get(attribute.size() - 1);
                    if (value != null) {
                        patternBuf.append(filter ? Support.escapeFilter(value) : Support.escapeDn(value));
                        continue;
                    }
                } catch (NoSuchElementException ex) {}
            if (searchString != null) {
                patternBuf.append(filter ? Support.escapeFilter(searchString) : Support.escapeDn(searchString));
                continue;
            }
            if (!filter) return null;
            patternBuf.append('*');
        }
        if (i < pattern.length()) patternBuf.append(pattern.substring(i));
        return searchAttribute != null ? Support.replace(patternBuf.toString(), "[]", searchAttribute) : patternBuf.toString();
    }

    private String getParameter(HttpServletRequest req, String name) {
        String parameter = req.getParameter(name = getParameterAlias(name));
        if (parameter != null && (parameter = parameter.trim()).equals("")) parameter = null;
        if (parameter != null) userParameters.put(name, parameter);
        return parameter;
    }

    public void processRequest(HttpServletRequest req)
        throws IOException, NamingException, ServletException {
        userParameters = new HashMap<String,String>();
        String action = getAction(), attributes = getParameter(req, "attributes");
        if (action == null && (action = getParameter(req, "a")) != null) action = action.toLowerCase();
        if (attributes == null) attributes = getAttributes();
        String attributeArray[] = null;
        if (attributes != null) {
            StringTokenizer st = new StringTokenizer(attributes, ",");
            attributeArray = new String[st.countTokens()];
            for (int i = 0; i < attributeArray.length; i++) attributeArray[i] = st.nextToken().trim();
        }
        String dn = getParameter(req, "dn");
        if (dn == null) dn = getDn();
        String filter = getParameter(req, "filter");
        if (filter == null) filter = getFilter();
        filter = filter != null ? filter.trim() : "";
        String host = getParameter(req, "host");
        if (host == null) host = getHost();
        String objectClass = getParameter(req, "objectClass");
        if (objectClass != null) objectClass = getParameterAlias(objectClass);
        else objectClass = getObjectClass();
        Attribute objectClassAttribute = null;
        if (objectClass != null) {
            StringTokenizer st = new StringTokenizer(objectClass, ",");
            objectClassAttribute = new BasicAttribute("objectClass");
            while (st.hasMoreTokens()) objectClassAttribute.add(st.nextToken().trim());
        }
        String objectParameter = getObjectParameter();
        String scope = getParameter(req, "scope");
        if (scope == null) scope = getScope();
        scope = scope != null ? scope.trim().toLowerCase() : "";
        String searchAttribute = getParameter(req, "searchAttribute");
        if (searchAttribute != null) searchAttribute = getParameterAlias(searchAttribute);
        String searchString = getParameter(req, "searchString");
        String username = getParameter(req, "username"), password = getParameter(req, "password");
        if (username == null) username = "";
        if (password == null) password = "";
        String s = getParameter(req, "countLimit");
        long countLimit = s == null ? getCountLimit() : Long.parseLong(s);
        s = getParameter(req, "timeLimit");
        int timeLimit = s == null ? getTimeLimit() : Integer.parseInt(s);
        s = getParameter(req, "skipNumber");
        skipNumber = s == null ? 0 : Integer.parseInt(s);
        BasicAttributes basicAttributes = new BasicAttributes(true), byteAttributes = getByteArrayValues() ? new BasicAttributes(true) : null;
        userAttributes = new BasicAttributes(true);
        Iterator iter = (attributeAliases.isEmpty() && ldapBean != null ? ldapBean.attributeAliases : attributeAliases).entrySet().iterator();
        while (iter.hasNext()) {
            Map.Entry entry = (Map.Entry)iter.next();
            String parameterName = (String)entry.getValue();
            String values[] = req.getParameterValues(parameterName);
            if ((values == null || values.length == 0) &&
                ((parameterName = parameterAliases.get(parameterName)) == null ||
                 (values = req.getParameterValues(parameterName)) == null || values.length == 0)) continue;
            Attribute basicAttribute = new BasicAttribute((String)entry.getKey()),
                byteAttribute = getByteArrayValues() ? new BasicAttribute((String)entry.getKey()) : null,
                userAttribute = new BasicAttribute(parameterName);
            for (int i = 0; i < values.length; i++) {
                String value = values[i].trim();
                if (!value.equals("")) {
                    basicAttribute.add(value);
                    if (getByteArrayValues()) byteAttribute.add(value.getBytes());
                    userAttribute.add(value);
                }
            }
            if (basicAttribute.size() > 0) {
                basicAttributes.put(basicAttribute);
                userAttributes.put(userAttribute);
            }
        }
        String operation = getParameter(req, "operation");
        if (operation != null) {
            String confirmOperation = getParameter(req, "confirmOperation");
            if (confirmOperation != null && operation.equals(confirmOperation)) {
                result = "failed";
                state = "confirm";
                return;
            }
        }
        if (basicAttributes.size() == 0 && searchString == null) {
            result = "failed";
            state = "started";
            return;
        }
        if (objectClassAttribute != null) basicAttributes.put(objectClassAttribute);
        else objectClassAttribute = basicAttributes.get("objectclass");
        String objectClassName = objectClassAttribute != null ? getAttributeValue(objectClassAttribute, objectClassAttribute.size() - 1) : null;
        boolean save = false, change = false, delete = false, login = false;
        if (action != null)
            if (action.equals("save")) save = true;
            else if (action.equals("change")) change = true;
            else if (action.equals("delete")) delete = true;
            else if (action.equals("login")) login = true;
        Hashtable<String,String> dirEnv = new Hashtable<String,String>();
        if (ldapBean != null) dirEnv.putAll(ldapBean.environment);
        dirEnv.putAll(environment);
        if (!username.equals("") && !getActiveDirectory()) {
            String providerUrl = dirEnv.get(Context.PROVIDER_URL);
            if (providerUrl != null) username = "cn=" + Support.escapeDn(username);
            if (!getShortPrincipalFormat()) username += "," + providerUrl.substring(providerUrl.lastIndexOf('/') + 1);
        }
        if (!dirEnv.containsKey(Context.INITIAL_CONTEXT_FACTORY) && System.getProperty(Context.INITIAL_CONTEXT_FACTORY) == null)
            dirEnv.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        if (host != null) dirEnv.put(Context.PROVIDER_URL, "ldap://" + host);
        if (!dirEnv.containsKey(Context.REFERRAL)) dirEnv.put(Context.REFERRAL, "follow");
        if (!getServerLogin() && (save || delete || login || !username.equals(""))) {
            if (!username.equals("")) dirEnv.put(Context.SECURITY_PRINCIPAL, username);
            dirEnv.put(Context.SECURITY_CREDENTIALS, password);
        }
        if (action == null && !getLoginAlways() ||
            !dirEnv.containsKey(Context.SECURITY_PRINCIPAL) && System.getProperty(Context.SECURITY_PRINCIPAL) == null) {
            dirEnv.remove(Context.SECURITY_PRINCIPAL);
            dirEnv.remove(Context.SECURITY_CREDENTIALS);
            dirEnv.put(Context.SECURITY_AUTHENTICATION, "none");
        } else if (!dirEnv.containsKey(Context.SECURITY_AUTHENTICATION) &&
                   System.getProperty(Context.SECURITY_AUTHENTICATION) == null) dirEnv.put(Context.SECURITY_AUTHENTICATION, "simple");
        if (verbose) servletContext.log("Trying to open initial dir context with parameters " + dirEnv);
        try {
            ldapContext = new InitialLdapContext(dirEnv, new Control[0]);
        } catch (NamingSecurityException ex) {
            if (action == null) throw ex;
            if (verbose) servletContext.log(ex.toString(), ex);
            result = "failed";
            state = "login";
            return;
        }
        if (action != null && !login) {
            String passwordAttribute = getPasswordAttribute(), usernameAttribute = getUsernameAttribute();
            if (usernameAttribute == null || (username = getAttributeValue(basicAttributes, usernameAttribute, 0)) == null) username = "";
            String madeDn = getDnPattern() != null ? fillPattern(getDnPattern(), basicAttributes, null, null, false, change || getModifyOnly()) : dn;
            if (madeDn == null) {
                result = "invalid";
                state = "check";
                return;
            }
            if (getSnFromCn()) {
                String cn = getAttributeValue(basicAttributes, "cn", 0);
                if (cn != null) {
                    StringTokenizer st = new StringTokenizer(cn);
                    if (st.hasMoreTokens()) basicAttributes.put(new BasicAttribute("sn", st.nextToken()));
                }
            }
            if (save || change) {
                if (change)
                    if (getActiveDirectory()) {
                        Hashtable<String,String> dirEnv1 = new Hashtable<String,String>(dirEnv);
                        dirEnv.put(Context.SECURITY_PRINCIPAL, username);
                        dirEnv.put(Context.SECURITY_CREDENTIALS, password);
                        InitialLdapContext ldapContext1 = null;
                        if (verbose) servletContext.log("Trying to open initial dir context for credentials check with parameters " + dirEnv);
                        try {
                            ldapContext1 = new InitialLdapContext(dirEnv, new Control[0]);
                        } catch (NamingSecurityException ex) {
                            result = "failed";
                            state = "login";
                            return;
                        } finally {
                            if (ldapContext1 != null) ldapContext1.close();
                        }
                    } else if (passwordAttribute != null) {
                        NamingEnumeration namingElements = null;
                        try {
                            boolean passed = false;
                            if (verbose) servletContext.log("Search " + madeDn + " for password");
                            try {
                                namingElements = ldapContext.search(madeDn, "(objectClass=" + objectClassName + ")", null,
                                                                    new SearchControls(SearchControls.OBJECT_SCOPE, 1L, timeLimit > 0 ? timeLimit : 0, new String[] {passwordAttribute}, false, true));
                            } catch (NameNotFoundException ex) {}
                            if (namingElements != null)
                                try {
                                    SearchResult result = (SearchResult)namingElements.next();
                                    Object value = "";
                                    Attribute attribute = result.getAttributes().get(passwordAttribute);
                                    if (attribute != null)
                                        try {
                                            value = attribute.get();
                                        } catch (NoSuchElementException ex) {}
                                    if (Support.compare(value, password) == 0) passed = true;
                                } catch (NoSuchElementException ex) {}
                            if (!passed) {
                                result = "failed";
                                state = "login";
                                return;
                            }
                        } finally {
                            if (namingElements != null) namingElements.close();
                        }
                        String newPassword = getAttributeValue(basicAttributes, passwordAttribute, 0);
                        if (newPassword != null)
                            if (!(newPassword = newPassword.trim()).equals("")) {
                                s = getParameter(req, "retype");
                                if (s == null || !newPassword.equals(s)) {
                                    result = "retype";
                                    state = "login";
                                    return;
                                }
                            } else basicAttributes.remove(passwordAttribute);
                    }
                Object object = null;
                if (objectParameter != null) {
                    String values[] = req.getParameterValues(objectParameter);
                    if (values != null && values.length > 0) object = values[0];
                }
                Exception ex = null;
                try {
                    if (save && !getModifyOnly() || object != null) {
                        if (verbose) servletContext.log("Rebind " + madeDn + " to " + object + " with attributes " + basicAttributes);
                        ldapContext.rebind(madeDn, object, save ? getByteArrayValues() ? byteAttributes : basicAttributes : null);
                    }
                    if ((change || getModifyOnly()) && basicAttributes.size() > 0) {
                        if (getActiveDirectory()) basicAttributes.remove("objectClass");
                        if (verbose) servletContext.log("modifyAttributes of " + madeDn + " with " + basicAttributes);
                        ldapContext.modifyAttributes(madeDn, DirContext.REPLACE_ATTRIBUTE, basicAttributes);
                    }
                } catch (NamingSecurityException ex1) {
                    result = "failed";
                    state = "login";
                    return;
                } catch (NamingException ex1) {
                    ex = ex1;
                }
                if (ex != null) {
                    if (servletContext != null) servletContext.log(ex.toString(), ex);
                    result = "failed";
                } else result = save ? "saved" : "changed";
                state = "updated";
                return;
            } else if (delete) {
                Exception ex = null;
                try {
                    if (verbose) servletContext.log("unbind " + madeDn);
                    ldapContext.unbind(madeDn);
                } catch (NamingSecurityException ex1) {
                    result = "failed";
                    state = "login";
                    return;
                } catch (NamingException ex1) {
                    ex = ex1;
                }
                if (ex != null) {
                    if (servletContext != null) servletContext.log(Support.stackTrace(ex));
                    result = "failed";
                } else result = "deleted";
                state = "updated";
                return;
            }
        }
        if (searchString != null)
            if (searchAttribute != null) {
                String searchFilterPattern = getSearchFilterPattern(searchAttribute);
                if (searchFilterPattern == null) searchFilterPattern = getSearchFilterPattern();
                if (searchFilterPattern != null) filter = fillPattern(searchFilterPattern, basicAttributes, searchAttribute, searchString, true, false);
                else if (basicAttributes.get(searchAttribute) == null) basicAttributes.put(searchAttribute, searchString);
            } else filter = fillPattern(getFreeSearchFilterPattern(), basicAttributes, searchAttribute, searchString, true, false);
        else if (getFilterPattern() != null) filter = fillPattern(getFilterPattern(), basicAttributes, null, null, true, false);
        if (filter.equals("")) {
            NamingEnumeration attributeEnum = basicAttributes.getAll();
            try {
                StringBuffer filterBuf = new StringBuffer();
                while (attributeEnum.hasMore())
                    try {
                        Attribute attribute = (Attribute)attributeEnum.next();
                        if (searchAttribute != null && !searchAttribute.equalsIgnoreCase(attribute.getID()) && !attribute.getID().equalsIgnoreCase("objectclass")) continue;
                        if (attribute.size() > 0)
                            try {
                                String value = (String)attribute.get(attribute.size() - 1);
                                if (value == null) continue;
                                if (filterBuf.length() == 0) filterBuf.append("(&");
                                filterBuf.append('(').append(attribute.getID()).append('=').append(Support.escapeFilter(value)).append(')');
                            } catch (NoSuchElementException ex) {}
                    } catch (NoSuchElementException ex) {}
                if (filterBuf.length() > 0) filter = filterBuf.append(')').toString();
            } finally {
                attributeEnum.close();
            }
            if (filter.equals("")) filter = "(objectClass=" + objectClassName + ")";
        }
        String sortAttributes = getParameter(req, "sortAttributes");
        if (sortAttributes == null) sortAttributes = getSortAttributes();
        if (sortAttributes != null) {
            BasicControl requestControls[] = new BasicControl[1];
            StringTokenizer st = new StringTokenizer(sortAttributes, ",");
            SortKey sortKeyArray[] = new SortKey[st.countTokens()];
            for (int i = 0; i < sortKeyArray.length; i++) sortKeyArray[i] = new SortKey(st.nextToken());
            requestControls[0] = new SortControl(sortKeyArray, SortControl.CRITICAL);
            if (verbose) servletContext.log("Setting search control with attributes " + sortAttributes);
            ldapContext.setRequestControls(requestControls);
        }
        try {
            if (verbose) servletContext.log("search " + dn + " with filter " + filter);
            searchResultEnum = ldapContext.search(dn, filter, null,
                                                  new SearchControls(scope.equals("base") ? SearchControls.OBJECT_SCOPE :
                                                                     scope.equals("sub") ? SearchControls.SUBTREE_SCOPE : SearchControls.ONELEVEL_SCOPE,
                                                                     countLimit > 0L ? countLimit : 0L, timeLimit > 0 ? timeLimit : 0, attributeArray, objectParameter != null, true));
        } catch (NameNotFoundException ex) {}
        if (searchResultEnum != null)
            try {
                for (int n = skipNumber; n > 0; n--) searchResultEnum.next();
                firstSearchResult = (SearchResult)searchResultEnum.next();
            } catch (NoSuchElementException ex) {
                searchResultEnum.close();
                searchResultEnum = null;
            }
        if (searchResultEnum != null)
            try {
                secondSearchResult = (SearchResult)searchResultEnum.next();
                result = "foundMany";
            } catch (NoSuchElementException ex) {
                searchResultEnum.close();
                searchResultEnum = null;
                result = "foundOne";
            } else result = "failed";
        state = "searched";
        if (verbose) servletContext.log("Result: " + result + ",state: " + state);
    }

    public void close()
        throws NamingException {
        if (verbose) servletContext.log("Closing Ldap Bean");
        try {
            if (searchResultEnum != null) {
                searchResultEnum.close();
                searchResultEnum = null;
            }
        } finally {
            if (ldapContext != null) {
                ldapContext.close();
                ldapContext = null;
            }
        }
    }

}
