
package FI.realitymodeler.protocol.pop3;

import FI.realitymodeler.*;
import java.net.*;

public class Handler extends FI.realitymodeler.protocol.ftp.Handler
{

public URLConnection openConnection(URL url)
{
	return new W3Pop3URLConnection(url);
}

}
