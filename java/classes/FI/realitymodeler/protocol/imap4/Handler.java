
package FI.realitymodeler.protocol.imap4;

import FI.realitymodeler.*;
import java.net.*;

public class Handler extends FI.realitymodeler.protocol.ftp.Handler
{

public URLConnection openConnection(URL url)
{
	return new W3Imap4URLConnection(url);
}

}
