
package FI.realitymodeler.protocol.ftp;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.net.*;

public class Handler extends W3URLStreamHandler
{

public URLConnection openConnection(URL u)
{
	return new W3FtpURLConnection(u);
}

protected void parseURL(URL u, String spec, int start, int limit)
{
	super.parseURL(u, spec, start, limit);
	String protocol = u.getProtocol(), host = u.getHost(), path = u.getPath();
// Lower cases protocol and host name and canonizes path for comparision purposes
	if (protocol != null) protocol = protocol.toLowerCase();
	if (host != null) host = host.toLowerCase();
	if (path != null) path = Support.canonizePath(path);
	super.setURL(u, protocol, host, u.getPort(), u.getAuthority(), u.getUserInfo(), path, u.getQuery(), u.getRef());
}

}
