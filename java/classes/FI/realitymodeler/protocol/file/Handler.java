
package FI.realitymodeler.protocol.file;

import FI.realitymodeler.*;
import java.io.*;
import java.net.*;

public class Handler extends W3URLStreamHandler
{

public URLConnection openConnection(URL url)
{
	return new W3FileURLConnection(url);
}

public URLConnection openConnection(URL url, Proxy proxy)
{
	return new W3FileURLConnection(url);
}

protected void parseURL(URL url, String spec, int start, int limit)
{
	int limit1 = limit;
	while (limit1 > 0 && (spec.charAt(limit1 - 1) == File.separatorChar || spec.charAt(limit1 - 1) == '/')) limit1--;
	if (limit1 < limit) limit = limit1 + 1;
	super.parseURL(url, spec, start, limit);
	String protocol = url.getProtocol(), host = url.getHost(), path = url.getPath();
// Lower cases protocol and host name and normalizes path for comparision purposes
	if (protocol != null) protocol = protocol.toLowerCase();
	if (host != null) host = host.toLowerCase();
	if (path != null) path = W3URLConnection.normalize(path);
	super.setURL(url, protocol, host, url.getPort(), url.getAuthority(), url.getUserInfo(), path, url.getQuery(), url.getRef());
}

}

