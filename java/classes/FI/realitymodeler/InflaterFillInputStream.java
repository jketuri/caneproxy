
package FI.realitymodeler;

import java.io.*;
import java.util.zip.*;

public class InflaterFillInputStream extends InflaterInputStream {

    public InflaterFillInputStream(InputStream in, Inflater inf) {
	super(in, inf);
    }

    public int read(byte b[], int off, int len) throws IOException {
	try {
            for (;;) {
                int n = super.read(b, off, len);
                if (n <= 0) {
                    n = inf.getRemaining();
                    inf.reset();
                    if (n > 0) inf.setInput(this.buf, this.len - n, n);
                    continue;
                }
                return n;
            }
	} catch (EOFException ex) {
            return -1;
	}
    }

}
