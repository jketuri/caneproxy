
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import java.io.*;
import java.text.*;
import java.util.*;

/** Converts HTTP 1.1 chunked stream to normal input stream. */
public class ChunkedInputStream extends FilterInputStream {
    public HeaderList footer = null;

    boolean started = false;
    int count = 0, markedCount = 0;

    public ChunkedInputStream(InputStream in) {
        super(in);
    }

    boolean check()
        throws IOException {
        if (count > 0) return false;
        if (count == -1) return true;
        if (started && Support.readLine(in) == null) {
            count = -1;
            return true;
        }
        String line = Support.readLine(in);
        if (line == null) {
            count = -1;
            return true;
        }
        String chunkSize = Support.getParameters(line, null).trim();
        count = chunkSize.equals("") ? 0 : Integer.parseInt(chunkSize, 16);
        if (count <= 0) {
            try {
                footer = new HeaderList(in);
            } catch (ParseException ex) {
                throw new IOException(ex.toString());
            }
            count = -1;
            return true;
        }
        started = true;
        return false;
    }

    public int read()
        throws IOException {
        if (check()) return -1;
        count--;
        return in.read();
    }

    public int read(byte b[], int off, int len)
        throws IOException {
        if (check()) return -1;
        int n = in.read(b, off, Math.min(count, len));
        count -= n;
        return n;
    }

    public void close() {
    }

    public void mark(int readLimit) {
        in.mark(readLimit);
        markedCount = count;
    }

    public void reset()
        throws IOException {
        in.reset();
        count = markedCount;
    }

    public boolean hasLeft() {
        return footer == null;
    }

}
