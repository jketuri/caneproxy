
package FI.realitymodeler;

import FI.realitymodeler.common.*;
import java.io.*;
import java.lang.reflect.*;

public class W3File extends File {
    static final long serialVersionUID = 0L;

    static Field fd0Field;
    static {
        try {
            Support.loadLibrary("FI_realitymodeler");
            initialize();
            fd0Field = FileDescriptor.class.getDeclaredField("fd");
            fd0Field.setAccessible(true);
        } catch (Exception ex) {
            throw new Error(ex.toString());
        }
    }

    public static final int ST_DEV = 0, ST_INO = 1, ST_MODE = 2, ST_NLINK = 3, ST_UID = 4, ST_GID = 5, ST_RDEV = 6, ST_SIZE = 7, ST_ATIME = 8, ST_MTIME = 9, ST_CTIME = 10;
    public static final int S_IFMT = 0x0170000, S_IFDIR = 0x0040000, S_IFCHR = 0x0020000, S_IFIFO = 0x0010000, S_IFREG = 0x0100000, S_IREAD = 0x0000400, S_IWRITE = 0x0000200, S_IEXEC = 0x0000100;

    public static native int getTimeZone();
    public static native int getMaxFilenameLength();

    private static native void initialize();
    private static native void lockBytes0(int fd0, int nbytes);
    private static native void unlockBytes0(int fd0, int nbytes);

    private native void info0(String path, long longValues[]);
    private native int lastAccess0(String path);
    private native int timeCreated0(String path);
    private native int timeModified0(String path);
    private native void setLastTimes0(String path, int atime, int mtime);
    private native void setReadAndWrite0(String path);
    private native void setOffline0(String path);
    private native void setOnline0(String path);
    private native boolean isOffline0(String path);
    private native boolean isWriteable0(String path);

    public W3File(String path) {
        super(path);
    }

    public W3File(String path, String name) {
        super(path, name);
    }

    public W3File(File dir, String name) {
        super(dir, name);
    }

    public void info(short shortValues[], long longValues[]) throws IOException {
        SecurityManager security = System.getSecurityManager();
        if (security != null) security.checkRead(getPath());
        info0(getCanonicalPath(), longValues);
    }

    public long lastAccess() throws IOException {
        SecurityManager security = System.getSecurityManager();
        if (security != null) security.checkRead(getPath());
        return ((long)lastAccess0(getCanonicalPath()) & 0xFFFFFFFFL) * 1000L;
    }

    public long timeCreated() throws IOException {
        SecurityManager security = System.getSecurityManager();
        if (security != null) security.checkRead(getPath());
        return ((long)timeCreated0(getCanonicalPath()) & 0xFFFFFFFFL) * 1000L;
    }

    public long timeModified() throws IOException {
        SecurityManager security = System.getSecurityManager();
        if (security != null) security.checkRead(getPath());
        return ((long)timeModified0(getCanonicalPath()) & 0xFFFFFFFFL) * 1000L;
    }

    public void setLastTimes(long atime, long mtime) throws IOException {
        SecurityManager security = System.getSecurityManager();
        if (security != null) security.checkWrite(getPath());
        setLastTimes0(getCanonicalPath(), atime >= 0L ? (int)(atime / 1000L) : 0, mtime >= 0L ? (int)(mtime / 1000L) : 0);
    }

    public boolean setReadAndWrite() {
        SecurityManager security = System.getSecurityManager();
        if (security != null) security.checkWrite(getPath());
        try {
            setReadAndWrite0(getCanonicalPath());
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

    public boolean setOffline() {
        SecurityManager security = System.getSecurityManager();
        if (security != null) security.checkWrite(getPath());
        try {
            setOffline0(getCanonicalPath());
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

    public boolean setOnline() {
        SecurityManager security = System.getSecurityManager();
        if (security != null) security.checkWrite(getPath());
        try {
            setOnline0(getCanonicalPath());
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

    public boolean isOffline() throws IOException {
        SecurityManager security = System.getSecurityManager();
        if (security != null) security.checkRead(getPath());
        return isOffline0(getCanonicalPath());
    }

    public boolean isWriteable() throws IOException {
        SecurityManager security = System.getSecurityManager();
        if (security != null) security.checkRead(getPath());
        return isWriteable0(getCanonicalPath());
    }

    public void copy(File file) throws IOException {
        byte b[] = new byte[Support.bufferLength];
        BufferedInputStream in = new BufferedInputStream(new FileInputStream(file));
        try {
            File dirs = new W3File(getParent().replace('/', File.separatorChar));
            if (!dirs.exists()) dirs.mkdirs();
            BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(this));
            try {
                for (int n; (n = in.read(b)) > 0;) out.write(b, 0, n);
            } finally {
                out.close();
            }
        } finally {
            in.close();
        }
    }

    public static void lockBytes(FileDescriptor fd, int nbytes) throws IOException, IllegalAccessException {
        lockBytes0(fd0Field.getInt(fd), nbytes);
    }

    public static void unlockBytes(FileDescriptor fd, int nbytes) throws IOException, IllegalAccessException {
        unlockBytes0(fd0Field.getInt(fd), nbytes);
    }

    public static void main(String argv[]) throws IOException {
        W3File f = new W3File(argv[0]);
        System.out.println("canonicalPath="+f.getCanonicalPath());
        System.out.println("lastAccess="+f.lastAccess());
        System.out.println("timeCreated="+f.timeCreated());
    }

}
