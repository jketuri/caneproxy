
import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;

public class Test {

    public static void main(String argv[])
        throws Exception {
        System.out.println(System.getProperties());
        W3Factory.prepare();
        InputStream in = new URL("file:/usr/local/server").openConnection().getInputStream();
        byte b[] = new byte[Support.bufferLength];
        for (int n; (n = in.read(b)) > 0;)
            System.out.write(b, 0, n);
    }

}
