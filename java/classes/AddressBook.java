
import FI.realitymodeler.sql.*;
import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.sql.*;
import java.util.*;

public class AddressBook extends Applet implements ActionListener, AdjustmentListener
{
	static String names[] =
	{
		"Internet address",
		"Contact information"
	};
	int mh;
	Panel panel;
	ResultSet rs;
	Connection dbc;
	int itemn = 0, nitems = -1;
	Label labels[] = new Label[2];
	PreparedStatement dbs1, dbs2, dbi, dbd;
	TextField textfields[] = new TextField[2];
	Button addButton, deleteButton, searchButton, mailButton, homePageButton, ftpButton;
	Scrollbar scrollbar;
	FontMetrics fm;

public void init()
{
	String name = getParameter("NAME");
	if (name == null) name = getClass().getName();
	setBackground(Color.white);
	setForeground(Color.black);
	GridBagLayout gbl = new GridBagLayout();
	GridBagConstraints gbc = new GridBagConstraints();
	setLayout(gbl);
	gbc.weightx = 1;
	add(addButton = new Button("Add"));
	gbl.setConstraints(addButton, gbc);
	addButton.addActionListener(this);
	add(deleteButton = new Button("Delete"));
	gbl.setConstraints(deleteButton, gbc);
	deleteButton.addActionListener(this);
	add(searchButton = new Button("Search"));
	gbl.setConstraints(searchButton, gbc);
	searchButton.addActionListener(this);
	add(mailButton = new Button("Mail"));
	gbl.setConstraints(mailButton, gbc);
	mailButton.addActionListener(this);
	add(homePageButton = new Button("Home page"));
	gbl.setConstraints(homePageButton, gbc);
	homePageButton.addActionListener(this);
	add(ftpButton = new Button("Ftp"));
	gbl.setConstraints(ftpButton, gbc);
	ftpButton.addActionListener(this);
	gbc.gridwidth = 3;
	gbc.gridx = 0;
	gbc.gridy = 1;
	gbc.anchor = gbc.WEST;
	for (int i = 0; i < 2; i++)
	{
		add(textfields[i] = new TextField(50));
		gbl.setConstraints(textfields[i], gbc);
		gbc.gridx = gbc.RELATIVE;
	}
	gbc.gridx = 0;
	gbc.gridy = 2;
	for (int i = 0; i < 2; i++)
	{
		add(labels[i] = new Label(names[i]));
		gbl.setConstraints(labels[i], gbc);
		gbc.gridx = gbc.RELATIVE;
	}
	add(panel = new Panel());
	gbc.fill = gbc.BOTH;
	gbc.gridx = 0;
	gbc.gridy = 3;
	gbc.gridwidth = gbc.gridheight = gbc.REMAINDER;
	gbc.weighty = 1;
	gbl.setConstraints(panel, gbc);
	add(scrollbar = new Scrollbar(Scrollbar.VERTICAL));
	scrollbar.addAdjustmentListener(this);
	gbc.anchor = gbc.EAST;
	gbc.fill = gbc.VERTICAL;
	gbc.gridx = 8;
	gbc.gridy = 0;
	gbc.gridwidth = 1;
	gbc.gridheight = gbc.REMAINDER;
	gbl.setConstraints(scrollbar, gbc);
	addMouseListener(new MouseAdapter() {
	public void mouseClicked(MouseEvent e)
	{
		int n = (e.getY() - panel.getLocation().y) / mh;
		try {
		if (n < 0 || n >= nitems) return;
		if (n == itemn || rs.relative(n - itemn))
		{
			itemn = n;
			for (n = 0; n < 2; n++) textfields[n].setText(rs.getString(n + 1));
		}
		} catch (SQLException ex) { ex.printStackTrace(); }
	}
	});
	try {
	RemoteDriverImpl.prepare();
	dbc = DriverManager.getConnection("jdbc:FI.realitymodeler.Remote:" + getDocumentBase().getHost() +
	"/jdbc:FI.realitymodeler/" + name);
	dbs1 = dbc.prepareStatement("SELECT * FROM AB");
	dbs2 = dbc.prepareStatement("SELECT * FROM AB\nWHERE ADDR=?");
	dbd = dbc.prepareStatement("DELETE FROM AB\nWHERE ADDR=?");
	dbi = dbc.prepareStatement("INSERT INTO AB VALUES (?, ?)");
	try
	{
		dbs1.executeQuery();
	}
	catch (SQLException ex)
	{
		Statement s = dbc.createStatement();
		s.execute("CREATE TABLE AB (ADDR BINARY(50), INFO BINARY(50))");
		s.execute("CREATE UNIQUE INDEX AB ON AB (ADDR)");
	}
	rs = dbs1.executeQuery();
	} catch (Exception ex)
	{
		showStatus(ex.getMessage());
		ex.printStackTrace();
	}
}

public void destroy()
{
	try { dbc.close(); }
	catch (SQLException ex) { ex.printStackTrace(); }
}

void drawItem(Graphics g) throws SQLException
{
	int y = (itemn + 1) * mh;
	g.drawString(rs.getString(1), 10, y);
	g.drawString(rs.getString(2), labels[1].getLocation().x, y);
}

public void paint(Graphics g)
{
	Graphics gp = panel.getGraphics();
	int i;
	if (nitems == -1)
	{
		gp.setFont(new Font("Helvetica", Font.PLAIN, 12));
		fm = gp.getFontMetrics();
		mh = fm.getHeight();
	}
	nitems = panel.getSize().height / mh;
	int h = mh * 2, n;
	try {
	scrollbar.setValues(0, nitems, 0, ((BridgeConnection)dbc).tableLength("AB"));
	gp.clearRect(0, 0, panel.getSize().width, panel.getSize().height);
	if (itemn > 0) rs.relative(-itemn);
	for (itemn = 0; itemn < nitems && rs.next(); itemn++) drawItem(gp);
	} catch (Exception ex) { ex.printStackTrace(); }
	nitems = itemn;
}

public void scroll(int n)
{
	Graphics g = panel.getGraphics();
	if (n > 0)
	{
		g.copyArea(0, n * mh, panel.getSize().width, (nitems - n) * mh, 0, n * mh);
		g.clearRect(0, 0, panel.getSize().width, n * mh);
	}
	else
	{
		g.copyArea(0, 0, panel.getSize().width, (nitems + n) * mh, 0, n * mh);
		g.clearRect(0, (nitems + n) * mh, panel.getSize().width, -n * mh);
	}
}

public void actionPerformed(ActionEvent e)
{
	Object source = e.getSource();
	try {
	if (source == addButton)
	{
		for (int n = 0; n < 2; n++) dbi.setString(n + 1, textfields[n].getText().trim());
		dbi.execute();
	}
	else if (source == deleteButton)
	{
		dbd.setString(1, textfields[0].getText().trim());
		dbd.execute();
	}
	else if (source == searchButton)
	{
		dbs2.setString(1, textfields[0].getText().trim());
		ResultSet rs2 = dbs2.executeQuery();
		for (int n = 0; n < 2; n++) textfields[n].setText(rs2.getString(n + 1));
	}
	else if (source == mailButton)
	{
		String s = "mailto:", a = textfields[0].getText();
		if (a.indexOf('@') != -1) s += a;
		else s += "postmaster@" + a;
		getAppletContext().showDocument(new URL(s));
	}
	else if (source == homePageButton)
	{
		String s = "http://", a = textfields[0].getText();
		int i = a.indexOf('@'), n = 0;
		if (i != -1) a = a.substring(i + 1) + "/" + "~" + a.substring(0, i);
		while (n < 2 && (i = a.indexOf('.', i + 1)) != -1) n++;
		if (n < 2) s += "www.";
		s += a;
		getAppletContext().showDocument(new URL(s));
	}
	else if (source == ftpButton)
	{
		String s = "ftp://", a = textfields[0].getText();
		int i = a.indexOf('@'), n = 0;
		if (i != -1) a = a.substring(i + 1);
		while (n < 2 && (i = a.indexOf('.', i + 1)) != -1) n++;
		if (n < 2) s += "ftp.";
		s += a;
		getAppletContext().showDocument(new URL(s));
	}
	} catch (Exception ex) { ex.printStackTrace(); }
}

public void adjustmentValueChanged(AdjustmentEvent e)
{
	try {
	switch (e.getAdjustmentType()) {
	case AdjustmentEvent.UNIT_INCREMENT:
		if (!rs.relative(-1 - itemn)) return;
		itemn = 0;
		scroll(1);
		drawItem(panel.getGraphics());
		break;
	case AdjustmentEvent.UNIT_DECREMENT:
		if (!rs.relative(nitems - itemn)) return;
		itemn = nitems - 1;
		scroll(-1);
		drawItem(panel.getGraphics());
		break;
	case AdjustmentEvent.BLOCK_INCREMENT:
		if (!rs.relative(nitems - itemn)) return;
		itemn = 0;
		break;
	case AdjustmentEvent.BLOCK_DECREMENT:
		if (!rs.relative(-nitems - itemn)) return;
		itemn = 0;
		break;
	case AdjustmentEvent.TRACK:
	{
		itemn = 0;
		int value = e.getValue();
		rs = dbs1.executeQuery();
		if (value > 0) rs.relative(value);
		break;
	}
	}
	repaint();
	} catch (SQLException ex) { ex.printStackTrace(); }
}

}
