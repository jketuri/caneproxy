
import java.io.*;
import java.util.*;

public class Ascii
{

public static void main(String argv[]) throws Exception
{
	System.out.println("<pre>");
	for (int i = 32; i < 255; i++) System.out.println(i + "=" + (char)i + ",0x" + Integer.toString(i, 16));
}

}