Java Enterprise Edition (JEE)-compatible web application server and
HTTP/1.1 web server and caching forward and reverse proxy server.
Contains also an automatic HTTP client, which currently handles only
HTML. So if your links to other pages are coded with JavaScript, this
client cannot follow your links.  Contains also among others servlets,
which can read Mail from POP- and IMAP-servers, and a servlet which
can read News from NNTP-server. Also a Wais-servlet, Ldap-servlet, and
CGI-servlets.  Special feature in Mail, and News-servlets is an option
to read all content from one source once to one HTML-page. So you can
read your Inbox contents to one HTML-page. Also inline attachments are
shown correctly. Proxy supports followining HTTP- enveloped protocols
in additition to HTTP: Ftp, Gopher, File, Imap, Ldap, Nntp, Pop3, Smtp
and Wais. Seven last protocols can be used only with chained
CaneProxy-servers, as they are not standard Netscape-protocols.
