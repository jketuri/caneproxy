
import FI.realitymodeler.common.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.text.*;
import java.util.*;
import org.apache.crimson.parser.*;
import org.apache.crimson.tree.*;
import org.xml.sax.helpers.*;
import org.w3c.dom.*;
import org.xml.sax.*;

class StringComparator implements Comparator
{

public int compare(Object o1, Object o2)
{
	return ((String)o1).compareTo((String)o2);
}

}

class ButtonListener implements ActionListener
{
	boolean pressed = false;

public void actionPerformed(ActionEvent e)
{
	pressed = true;
	((Window)((Button)e.getSource()).getParent()).setVisible(false);
}

}

class EmptyEntityResolver implements EntityResolver
{

public InputSource resolveEntity(String publicId, String systemId)
{
	return new InputSource(new ByteArrayInputStream(new byte[0]));
}

}

public class ttf_install
{

static void copyFile(File fromFile, File toFile) throws IOException
{
	FileInputStream fin = null;
	FileOutputStream fout = null;
	try
	{
		fin = new FileInputStream(fromFile);
		fout = new FileOutputStream(toFile);
		byte b[] = new byte[4096];
		for (int n; (n = fin.read(b)) > 0;) fout.write(b, 0, n);
	}
	finally
	{
		try
		{
			if (fin != null) fin.close();
		}
		finally
		{
			if (fout != null) fout.close();
		}
	}
}

static void copyDir(File fromDir, File toDir, boolean recurse) throws IOException
{
	toDir.mkdir();
	String list[] = fromDir.list();
	for (int i = 0; i < list.length; i++)
	{
		File file = new File(fromDir, list[i]);
		if (file.isFile() && !file.isDirectory()) copyFile(new File(fromDir, list[i]), new File(toDir, list[i]));
		else if (file.isDirectory() && recurse)
		{
			File dir = new File(toDir, list[i]);
			dir.mkdir();
			copyDir(file, dir, true);
		}
	}
}

static boolean dialog(String title, String message, TextField field, boolean question)
{
	Dialog dialog = new Dialog(new Frame(), title);
	dialog.addWindowListener(new WindowAdapter()
	{
		public void windowClosing(WindowEvent e)
		{
			e.getWindow().setVisible(false);
		}
	});
	dialog.setLayout(new GridLayout(3 + (field != null ? 1 : 0), 1));
	TextArea textArea = new TextArea(message, 4, 80, TextArea.SCROLLBARS_NONE);
	textArea.setEditable(false);
	dialog.add(textArea);
	if (field != null) dialog.add(field);
	Button proceed = new Button(question ? "Yes" : "Proceed");
	proceed.addActionListener(new ButtonListener());
	dialog.add(proceed);
	Button cancel = new Button(question ? "No" : "Cancel");
	ButtonListener cancelListener = new ButtonListener();
	cancel.addActionListener(cancelListener);
	dialog.add(cancel);
	dialog.setSize(640, 200);
	dialog.setModal(true);
	dialog.setVisible(true);
	return cancelListener.pressed;
}

static boolean dialog(String title)
{
	return dialog(title, title, null, false);
}

static void exec(String cmd, boolean check) throws InterruptedException, IOException
{
	Process process = Runtime.getRuntime().exec(cmd);
	process.waitFor();
	if (!check || process.exitValue() == 0) return;
	dialog("Error " + process.exitValue());
	System.out.println("Command " + cmd + " returned error value " + process.exitValue());
	System.exit(process.exitValue());
}

static HashMap readServlets(File file) throws IOException, ParseException
{
	StreamTokenizer sTok = null;
	HashMap servlets = new HashMap();
	BufferedReader bf = new BufferedReader(new FileReader(file));
	try
	{
		sTok = new StreamTokenizer(bf);
		sTok.resetSyntax();
		sTok.whitespaceChars(0, 32);
		sTok.wordChars(33, 255);
		sTok.ordinaryChar('=');
		sTok.commentChar('#');
		sTok.quoteChar('"');
		Vector options = null;
		HashMap initParameters = null;
		String name = null, locator = null;
		while (sTok.nextToken() != sTok.TT_EOF)
		{
			if (sTok.ttype != sTok.TT_WORD && sTok.ttype != '"') throw new ParseException("Unexpected token", 0);
			String s = sTok.sval;
			if (initParameters != null && initParameters.size() == 2 && s.startsWith(":"))
			{
				options.addElement(s);
				continue;
			}
			if (sTok.nextToken() == '=')
			{
				sTok.nextToken();
				if (sTok.ttype != sTok.TT_WORD && sTok.ttype != '"' || initParameters == null) throw new ParseException("Unexpected token", 0);
				initParameters.put(s, sTok.sval);
				continue;
			}
			if (sTok.ttype == sTok.TT_EOF) break;
			if (name != null) servlets.put(name, initParameters);
			name = s;
			locator = sTok.sval;
			options = new Vector();
			initParameters = new HashMap();
			initParameters.put(Boolean.TRUE, locator);
			initParameters.put(Boolean.FALSE, options);
		}
		if (name != null) servlets.put(name, initParameters);
	}
	catch (Exception ex)
	{
		if (file != null && sTok != null) throw new ParseException("Error in file " + file.getPath() + " in line " + sTok.lineno(), sTok.lineno());
		throw new ParseException("Error reading parameter files", 0);
	}
	finally
	{
		bf.close();
	}
	return servlets;
}

static String encode(String value)
{
	StringBuffer valueBuf = new StringBuffer();
	int l = value.length();
	for (int i = 0; i < l; i++)
	{
		if ("\\\"".indexOf(value.charAt(i)) != -1) valueBuf.append('\\');
		valueBuf.append(value.charAt(i));
	}
	return valueBuf.toString();
}

static void saveServlets(File file, HashMap servlets) throws IOException
{
	TreeMap servletMap = new TreeMap(new StringComparator());
	Iterator servletIter = servlets.entrySet().iterator();
	while (servletIter.hasNext())
	{
		Map.Entry entry = (Map.Entry)servletIter.next();
		servletMap.put(entry.getKey(), entry.getValue());
	}
	PrintWriter pw = new PrintWriter(new FileOutputStream(file));
	try
	{
		pw.println("\n# Servlets saved by bc_install at " + new Date() + "\n");
		servletIter = servletMap.entrySet().iterator();
		while (servletIter.hasNext())
		{
			Map.Entry entry = (Map.Entry)servletIter.next();
			HashMap initParameters = (HashMap)entry.getValue();
			pw.print(entry.getKey() + " " + initParameters.get(Boolean.TRUE));
			Vector options = (Vector)initParameters.get(Boolean.FALSE);
			if (options != null)
			{
				Iterator iter = options.iterator();
				while (iter.hasNext()) pw.print(" " + iter.next());
			}
			pw.println();
			TreeMap initParameterMap = new TreeMap(new StringComparator());
			Iterator initParameterIter = initParameters.entrySet().iterator();
			while (initParameterIter.hasNext())
			{
				Map.Entry initEntry = (Map.Entry)initParameterIter.next();
				if (!(initEntry.getKey() instanceof String)) continue;
				initParameterMap.put(initEntry.getKey(), initEntry.getValue());
			}
			initParameterIter = initParameterMap.entrySet().iterator();
			while (initParameterIter.hasNext())
			{
				Map.Entry initEntry = (Map.Entry)initParameterIter.next();
				pw.println("\t" + initEntry.getKey() + "=\"" + encode((String)initEntry.getValue()) + "\"");
			}
			pw.println();
		}
	}
	finally
	{
		pw.close();
	}
}

public static String encodedArgs(HashMap initParameters)
{
	StringBuffer initArgs = new StringBuffer();
	Iterator iter = initParameters.entrySet().iterator();
	for (boolean started = false; iter.hasNext();)
	{
		Map.Entry initEntry = (Map.Entry)iter.next();
		if (!(initEntry.getKey() instanceof String)) continue;
		if (started) initArgs.append(',');
		initArgs.append(initEntry.getKey()).append('=').append(Support.encode((String)initEntry.getValue(), ",=\\#"));
		started = true;
	}
	return initArgs.toString();
}

public static void main(String argv[])
{
	try {
	Properties regValues = new Properties();
	if (File.separatorChar == '\\')
	{
		exec("bin\\ttf_install.exe", true);
		FileInputStream fin = new FileInputStream("reg_values.properties");
		try
		{
			regValues.load(fin);
		}
		finally
		{
			fin.close();
		}
	}
	File file = new File("ttf_servlet_init");
	HashMap ttfServlet = readServlets(file);
	Iterator iter = ttfServlet.entrySet().iterator();
	if (!iter.hasNext()) throw new ParseException("Parameter file " + file.getPath() + " is bad", 0);
	Map.Entry entry = (Map.Entry)iter.next();
	String name = (String)entry.getKey();
	HashMap initParameters = (HashMap)entry.getValue();
	String locator = (String)initParameters.get(Boolean.TRUE);
	Dialog dialog = new Dialog(new Frame(), "TTF Servlet Settings");
	dialog.setModal(true);
	dialog.setLayout(new GridLayout(7, 2));
	dialog.add(new Label("Platform"));
	Choice platform = new Choice();
	platform.add("Tomcat web server 3.0");
	platform.add("Java Servlet Development Kit 2.0");
	platform.add("JRun 2.3");
	platform.add("Keppi Server");
	dialog.add(platform);
	dialog.add(new Label("Sql driver classes"));
	TextField driverClasses = new TextField((String)initParameters.get("sql:driverClasses"));
	dialog.add(driverClasses);
	dialog.add(new Label("Sql user name"));
	TextField user = new TextField((String)initParameters.get("sql:info:user"));
	dialog.add(user);
	dialog.add(new Label("Sql user password"));
	TextField password = new TextField((String)initParameters.get("sql:info:password"));
	dialog.add(password);
	dialog.add(new Label("Sql URL"));
	TextField url = new TextField((String)initParameters.get("sql:url"));
	dialog.add(url);
	dialog.add(new Label("Verbose program operation"));
	Checkbox verbose = new Checkbox("verbose", false);
	dialog.add(verbose);
	Button install = new Button("Install");
	dialog.add(install);
	Button cancel = new Button("Cancel");
	dialog.add(cancel);
	ButtonListener installListener = new ButtonListener();
	install.addActionListener(installListener);
	ButtonListener cancelListener = new ButtonListener();
	cancel.addActionListener(cancelListener);
	dialog.addWindowListener(new WindowAdapter()
	{
		public void windowClosing(WindowEvent e)
		{
			e.getWindow().setVisible(false);
		}
	});
	dialog.setSize(640, 240);
	do
	{
		dialog.setVisible(true);
		if (cancelListener.pressed) break;
		if (!installListener.pressed) break;
		initParameters.put("sql:driverClasses", driverClasses.getText());
		initParameters.put("sql:info:user", user.getText());
		initParameters.put("sql:info:password", password.getText());
		initParameters.put("sql:url", url.getText());
		if (verbose.getState()) initParameters.put("verbose", "true");
		String JavaHome;
		if (File.separatorChar == '/') {
		    TextField textField = new TextField("/usr/java/jre1.5.0");
		    if (dialog("JDK directory", "Please give the Java Runtime Environment 1.5 directory", textField, false)) break;
		    JavaHome = textField.getText();
		} else {
		    JavaHome = regValues.getProperty("Software\\JavaSoft\\Java Runtime Environment\\1.5_JavaHome");
		    if (JavaHome == null) {
			if (dialog("Java Runtime Environment 1.5 is not installed or you have no administrator-privileges")) break;
			continue;
		    }
		}
		switch (platform.getSelectedIndex()) {
		case 0:
		{
			TextField destDirField = new TextField(File.separatorChar == '/' ? "/usr/local/ttf" : "C:\\Program Files\\ttf");
			if (dialog("Destination directory", "Please give the preferrable destination directory for service files", destDirField, false)) break;
			File destDir = new File(destDirField.getText());
			copyDir(new File("xml"), destDir, false);
			File webpages = new File(destDir, "webpages");
			copyDir(new File("webpages"), webpages, true);
			initParameters.put("directory", destDir.getCanonicalPath());
			initParameters.put("sql:dbInitFilename", new File(destDir, "ttf_db_init").getCanonicalPath());
			File webInfDir = new File(webpages, "WEB-INF"),
			webXml = new File(webInfDir, "web.xml");
			InputSource inputSource = Resolver.createInputSource(webXml);
			XMLReader xmlReader = XMLReaderFactory.createXMLReader();
			XmlDocumentBuilder xmlDocumentBuilder = new XmlDocumentBuilder();
			xmlReader.setContentHandler(xmlDocumentBuilder);
			xmlReader.setEntityResolver(new EmptyEntityResolver());
			xmlDocumentBuilder.setIgnoringLexicalInfo(false);
			xmlDocumentBuilder.setDisableNamespaces(false);
			xmlReader.setContentHandler(xmlDocumentBuilder);
			xmlReader.parse(inputSource);
			XmlDocument doc = xmlDocumentBuilder.getDocument();
			Element root = doc.getDocumentElement(),
			servlet = doc.createElement("servlet"),
			servletName = doc.createElement("servlet-name"),
			servletClass = doc.createElement("servlet-class"),
			oldServlet = null;
			servletName.appendChild(doc.createTextNode("ttf"));
			servlet.appendChild(servletName);
			servletClass.appendChild(doc.createTextNode(locator));
			servlet.appendChild(servletClass);
			NodeList list = root.getChildNodes();
			int l = list.getLength();
			for (int i = 0; i < l; i++)
			{
				Node node = list.item(i);
				if (node.getNodeName() != null &&
				node.getNodeName().equals("servlet"))
				{
					NodeList list1 = node.getChildNodes();
					int l1 = list1.getLength();
					for (int i1 = 0; i1 < l1; i1++)
					{
						Node node1 = list1.item(i1);
						if (node1.getNodeName() != null &&
						node1.getNodeName().equals("servlet-name"))
						{
							Node node2 = node1.getFirstChild();
							if (node2.getNodeValue() != null &&
							node2.getNodeValue().equals("ttf")) oldServlet = (Element)node;
						}
					}
				}
			}
			iter = initParameters.entrySet().iterator();
			while (iter.hasNext())
			{
				entry = (Map.Entry)iter.next();
				if (!(entry.getKey() instanceof String)) continue;
				Element initParam = doc.createElement("init-param"),
				paramName = doc.createElement("param-name"),
				paramValue = doc.createElement("param-value");
				paramName.appendChild(doc.createTextNode("\n" + entry.getKey() + "\n"));
				paramValue.appendChild(doc.createTextNode("\n" + entry.getValue() + "\n"));
				initParam.appendChild(paramName);
				initParam.appendChild(paramValue);
				servlet.appendChild(initParam);
			}
			if (oldServlet != null) root.replaceChild(servlet, oldServlet);
			else root.appendChild(servlet);
			FileWriter fwriter = new FileWriter(webXml);
			try
			{
				doc.write(fwriter);
			}
			finally
			{
				fwriter.close();
			}
			if (File.separatorChar == '/')
			    copyDir(new File("lib/ext"), new File(JavaHome, "jre/lib/ext"), false);
			else copyDir(new File("lib/ext"), new File(JavaHome, "lib/ext"), false);
			dialog("Run servlet web server", "You can go to directory " + destDir.getCanonicalPath() + " and invoke there script sws.bat to run servlet web server. " +
			"It uses parameter file " + webXml.getCanonicalPath() + " modified by this program.", null, false);
			break;
		}
		case 1:
		{
			TextField destDirField = new TextField(File.separatorChar == '/' ? "/usr/local/ttf" : "C:\\Program Files\\ttf");
			if (dialog("Destination directory", "Please give the preferrable destination directory for service files", destDirField, false)) break;
			File destDir = new File(destDirField.getText());
			copyDir(new File("xml"), destDir, false);
			initParameters.put("directory", destDir.getCanonicalPath());
			initParameters.put("sql:dbInitFilename", new File(destDir, "ttf_db_init").getCanonicalPath());
			Properties servletProperties = new Properties();
			File servletPropertiesFile = new File(destDir, "servlet.properties");
			if (servletPropertiesFile.exists())
			{
				copyFile(servletPropertiesFile, new File(destDir, "servlet.properties.back"));
				FileInputStream fin = new FileInputStream(servletPropertiesFile);
				try
				{
					servletProperties.load(fin);
				}
				finally
				{
					fin.close();
				}
			}
			name = name.substring(name.lastIndexOf('/') + 1);
			servletProperties.put("servlet." + name + ".code", locator);
			servletProperties.put("servlet." + name + ".initArgs", encodedArgs(initParameters));
			FileOutputStream fout = new FileOutputStream(servletPropertiesFile);
			try
			{
				servletProperties.store(fout, "servlet.properties stored by bc_install");
			}
			finally
			{
				fout.close();
			}
			copyDir(new File("lib/ext"), new File(JavaHome, "jre/lib/ext"), false);
			dialog("Run servletrunner", "You can go to directory " + destDir.getCanonicalPath() + " and invoke there script sr.bat to run servletrunner. " +
			"It uses parameter file " + servletPropertiesFile.getCanonicalPath() + " modified by this program.", null, false);
			break;
		}
		case 2:
		{
			TextField JRunDir = new TextField(File.separatorChar == '/' ? "/usr/local/JRun" : "C:\\JRun");
			if (dialog("JRun root directory", "Please give the JRun 2.3 root directory containing the jsm-default directory", JRunDir, false)) break;
			if (!new File(JRunDir.getText()).exists())
			{
				if (dialog("Directory " + JRunDir.getText() + " does not exist")) break;
				continue;
			}
			Properties jsmProperties = new Properties();
			File jsmPropertiesFile = new File(JRunDir.getText(), "jsm-default/properties/jsm.properties");
			FileInputStream fin = new FileInputStream(jsmPropertiesFile);
			try
			{
				jsmProperties.load(fin);
			}
			finally
			{
				fin.close();
			}
			File jsm_lib = new File(JRunDir.getText(), "jsm-default/lib");
			copyDir(new File("lib/ext"), jsm_lib, false);
			String javaClasspath = jsmProperties.getProperty("java.classpath");
			if (javaClasspath == null) javaClasspath = "";
			StringTokenizer st = new StringTokenizer(javaClasspath, ";");
			ArrayList paths = new ArrayList();
			while (st.hasMoreTokens()) paths.add(st.nextToken().trim());
			String jarNames[] = new File("lib/ext").list();
			for (int i = 0; i < jarNames.length; i++) paths.add(new File(jsm_lib, jarNames[i]).getCanonicalPath());
			for (int i = paths.size() - 1; i >= 0; i--)
			{
				int x = paths.indexOf(paths.get(i));
				if (x != i) paths.remove(i);
			}
			StringBuffer javaClasspathBuf = new StringBuffer();
			for (int i = 0; i < paths.size(); i++) javaClasspathBuf.append(paths.get(i)).append(';');
			jsmProperties.put("java.classpath", javaClasspathBuf.toString());
			copyFile(jsmPropertiesFile, new File(jsmPropertiesFile.getCanonicalPath() + ".back"));
			FileOutputStream fout = new FileOutputStream(jsmPropertiesFile);
			try
			{
				jsmProperties.store(fout, "jsm.properties stored by bc_install");
			}
			finally
			{
				fout.close();
			}
			File jsm_xml = new File(JRunDir.getText(), "jsm-default/xml");
			copyDir(new File("xml"), jsm_xml, false);
			initParameters.put("directory", jsm_xml.getCanonicalPath());
			initParameters.put("sql:dbInitFilename", new File(jsm_xml, "ttf_db_init").getCanonicalPath());
			String dirNames[] = {"jse", "jseweb"};
			for (int i = 0; i < dirNames.length; i++)
			{
				Properties servletsProperties = new Properties();
				File servletsPropertiesFile = new File(JRunDir.getText(), "jsm-default/services/" + dirNames[i] + "/properties/servlets.properties");
				copyFile(servletsPropertiesFile, new File(servletsPropertiesFile.getCanonicalPath() + ".back"));
				fin = new FileInputStream(servletsPropertiesFile);
				try
				{
					servletsProperties.load(fin);
				}
				finally
				{
					fin.close();
				}
				name = name.substring(name.lastIndexOf('/') + 1);
				servletsProperties.put("servlet." + name + ".code", locator);
				servletsProperties.put("servlet." + name + ".args", encodedArgs(initParameters));
				servletsProperties.put("servlet." + name + ".preload", "true");
				fout = new FileOutputStream(servletsPropertiesFile);
				try
				{
					servletsProperties.store(fout, "servlets.properties stored by bc_install");
				}
				finally
				{
					fout.close();
				}
			}
			break;
		}
		case 3:
		{
			File serverOptionsFile = new File(File.separatorChar == '/' ? "/server_options" : "C:\\server_options");
			if (!serverOptionsFile.exists())
			{
				if (dialog("Keppi-server is not installed (server_options-file is missing)")) break;
				continue;
			}
			ByteArrayOutputStream bout = new ByteArrayOutputStream();
			byte b[] = new byte[Support.bufferLength];
			FileInputStream fin = new FileInputStream(serverOptionsFile);
			try
			{
				for (int n; (n = fin.read(b)) > 0;) bout.write(b, 0, n);
			}
			finally
			{
				fin.close();
			}
			StringTokenizer st = new StringTokenizer(new String(bout.toByteArray()));
			String directory = null;
			while (st.hasMoreTokens())
			{
				String arg = st.nextToken();
				if (arg.charAt(0) == '-')
				for (int i = 1; i < arg.length(); i++)
				switch (Character.toLowerCase(arg.charAt(i))) {
				case 'd':
					directory = st.nextToken();
					continue;
				case 'b':
				case 'c':
				case 'l':
				case 'p':
					st.nextToken();
					continue;
				default:
					continue;
				}
			}
			if (directory == null) throw new ParseException("Parameter file " + serverOptionsFile.getPath() + " is bad", 0);
			copyDir(new File("lib/ext"), new File(JavaHome, "lib/ext"), false);
			File server_xml = new File(directory, "xml");
			copyDir(new File("xml"), server_xml, false);
			initParameters.put("directory", server_xml.getCanonicalPath());
			initParameters.put("sql:dbInitFilename", new File(server_xml, "ttf_db_init").getCanonicalPath());
			File servletsFile = new File(directory, "servlets");
			HashMap servlets = servletsFile.exists() ? readServlets(servletsFile) : new HashMap();
			servlets.put(name, initParameters);
			copyFile(servletsFile, new File(directory, "servlets.back"));
			saveServlets(servletsFile, servlets);
			break;
		}
		}
		break;
	}
	while (!dialog("Retry installation?"));
	System.exit(0);
	} catch (Throwable th)
	{
		th.printStackTrace();
		dialog("Error " + th.toString());
		System.exit(1);
	}
}

}
