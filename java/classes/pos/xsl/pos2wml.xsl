<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="xml" omit-xml-declaration="no"/>
<xsl:output doctype-public="-//WAPFORUM//DTD WML 1.1//EN" doctype-system="http://www.wapforum.org/DTD/wml_1.1.xml"/>

<xsl:template match="xml:useBean"/>

<xsl:template match="page">
<wml>
	<template>
		<do type="prev">
			<prev/>
		</do>
	</template>
	<xsl:apply-templates/>
</wml>
</xsl:template>

<xsl:template match="content">
	<card id="{@name}" title="{headline}">
		<p>
			<xsl:apply-templates/>
		</p>
	</card>
</xsl:template>

<xsl:template match="headline"/>

<xsl:template match="procedure">
	<xsl:apply-templates/>
	<do type="accept" label="{@label}">
		<go method="get" href="{@name}.wml">
			<xsl:apply-templates mode="procedure"/>
		</go>
	</do>
</xsl:template>

<xsl:template match="record">
	<xsl:apply-templates/>
</xsl:template>

<xsl:template match="action | alarm | classification | priority | recurrence | status | time_transparency | fullname-list">
	<xsl:value-of select="label"/>
	<select name="{@name}" value="{value}">
		<xsl:apply-templates select="choice"/>
	</select>
</xsl:template>

<xsl:template match="choice">
	<option value="{.}"><xsl:value-of select="."/></option>
</xsl:template>

<xsl:template match="alternative">
	<a href="{name}.wml{href}"><xsl:value-of select="label"/></a><br/>
</xsl:template>

<xsl:template match="link">
	<a href="{href}"><xsl:value-of select="label"/></a>
</xsl:template>

<xsl:template match="msgitem">
	<xsl:apply-templates mode="msgitem"/>
</xsl:template>

<xsl:template match="headers">
	<b>
		<xsl:apply-templates mode="headers"/>
	</b>
</xsl:template>

<xsl:template match="text">
	<xsl:value-of select="."/>
</xsl:template>

<xsl:template match="date" mode="msgitem">
	<xsl:value-of select="."/><xsl:text> </xsl:text>
</xsl:template>

<xsl:template match="subject | from" mode="msgitem">
	<a href="msg.wml{link}"><xsl:value-of select="value"/></a><xsl:text> </xsl:text>
</xsl:template>

<xsl:template match="date | subject" mode="headers">
	<xsl:value-of select="."/><br/>
</xsl:template>

<xsl:template match="from" mode="headers">
	<a href="msg.wml{link}"><xsl:value-of select="value"/></a><br/>
</xsl:template>

<xsl:template match="password">
	<xsl:value-of select="label"/><input name="{@name}" type="password"/><br/>
</xsl:template>

<xsl:template match="* | subject">
	<xsl:value-of select="label"/><input name="{@name}" value="{value}"/><br/>
</xsl:template>

<xsl:template match="sessionid"/>

<xsl:template match="sessionid" mode="procedure">
	<postfield name="{@name}" value="{.}"/>
</xsl:template>

<xsl:template match="* | subject | password" mode="procedure">
	<postfield name="{@name}" value="$({@name})"/>
</xsl:template>

<xsl:template match="action | alarm | classification | priority | recurrence | status | time_transparency | fullname-list" mode="procedure">
	<postfield name="{@name}" value="$({@name})"/>
</xsl:template>

</xsl:stylesheet>