<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:output method="html"/>

<xsl:template match="xml:useBean"/>

<xsl:template match="page">
<html>
	<xsl:apply-templates/>
</html>
</xsl:template>

<xsl:template match="content">
	<head>
		<title><xsl:value-of select="headline"/></title>
	</head>
	<body>
		<xsl:apply-templates/>
	</body>
</xsl:template>

<xsl:template match="headline">
	<h1><xsl:value-of select="."/></h1>
</xsl:template>

<xsl:template match="procedure">
	<form action="{@name}.html" method="get">
		<xsl:apply-templates/>
		<input type="submit" value="{@label}"> </input>
	</form>
</xsl:template>

<xsl:template match="record">
	<table>
		<xsl:apply-templates/>
	</table>
</xsl:template>

<xsl:template match="action | alarm | classification | priority | recurrence | status | time_transparency | fullname-list">
	<tr>
		<th>
			<xsl:value-of select="label"/>
		</th>
		<td>
			<select name="{@name}" value="{value}">
				<xsl:apply-templates select="choice"/>
			</select>
		</td>
	</tr>
</xsl:template>

<xsl:template match="choice">
	<option>
		<xsl:value-of select="."/>
	</option>
</xsl:template>

<xsl:template match="alternative">
	<a href="{name}.html{href}"><xsl:value-of select="label"/></a><br></br>
</xsl:template>

<xsl:template match="link">
	<a href="{href}"><xsl:value-of select="label"/></a><br/>
</xsl:template>

<xsl:template match="msgitem">
	<xsl:apply-templates mode="msgitem"/>
	<hr/>
</xsl:template>

<xsl:template match="headers">
	<table>
		<xsl:apply-templates mode="headers"/>
	</table>
</xsl:template>

<xsl:template match="text">
	<xsl:value-of select="."/>
</xsl:template>

<xsl:template match="date" mode="msgitem">
	<xsl:value-of select="."/><xsl:text> </xsl:text>
</xsl:template>

<xsl:template match="subject | from" mode="msgitem">
	<a href="msg.html{link}"><xsl:value-of select="value"/></a><xsl:text> </xsl:text>
</xsl:template>

<xsl:template match="date | subject" mode="headers">
	<tr><th><xsl:value-of select="."/></th></tr>
</xsl:template>

<xsl:template match="from" mode="headers">
	<tr><th><a href="msg.html{link}"><xsl:value-of select="value"/></a></th></tr>
</xsl:template>

<xsl:template match="textarea">
	<textarea name="{@name}" rows="20" cols="76" wrap="hard">
	</textarea>
	<br/>
</xsl:template>

<xsl:template match="sessionid">
	<input name="{@name}" type="hidden" value="{.}"> </input>
</xsl:template>

<xsl:template match="password">
	<tr>
		<th>
			<xsl:value-of select="label"/>
		</th>
		<td>
			<input name="{@name}" size="50" type="password"> </input>
		</td>
	</tr>
</xsl:template>

<xsl:template match="* | subject">
	<tr>
		<th>
			<xsl:value-of select="label"/>
		</th>
		<td>
			<input name="{@name}" value="{value}" size="50"> </input>
		</td>
	</tr>
</xsl:template>

</xsl:stylesheet>