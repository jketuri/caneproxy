
import FI.realitymodeler.common.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.text.*;
import java.util.*;

class StringComparator implements Comparator
{

public int compare(Object o1, Object o2)
{
	return ((String)o1).compareTo((String)o2);
}

}

class ButtonListener implements ActionListener
{
	boolean pressed = false;

public void actionPerformed(ActionEvent e)
{
	pressed = true;
	((Window)((Button)e.getSource()).getParent()).setVisible(false);
}

}

public class pos_install
{

static void copyFile(File fromFile, File toFile) throws IOException
{
	FileInputStream fin = null;
	FileOutputStream fout = null;
	try
	{
		fin = new FileInputStream(fromFile);
		fout = new FileOutputStream(toFile);
		byte b[] = new byte[4096];
		for (int n; (n = fin.read(b)) > 0;) fout.write(b, 0, n);
	}
	finally
	{
		try
		{
			if (fin != null) fin.close();
		}
		finally
		{
			if (fout != null) fout.close();
		}
	}
}

static void copyDir(File fromDir, File toDir, boolean recurse) throws IOException
{
	toDir.mkdir();
	String list[] = fromDir.list();
	for (int i = 0; i < list.length; i++)
	{
		File file = new File(fromDir, list[i]);
		if (file.isFile() && !file.isDirectory()) copyFile(new File(fromDir, list[i]), new File(toDir, list[i]));
		else if (file.isDirectory() && recurse)
		{
			File dir = new File(toDir, list[i]);
			dir.mkdir();
			copyDir(file, dir, true);
		}
	}
}

static boolean dialog(String title, String message, TextField field, boolean question)
{
	Dialog dialog = new Dialog(new Frame(), title);
	dialog.addWindowListener(new WindowAdapter()
	{
		public void windowClosing(WindowEvent e)
		{
			e.getWindow().setVisible(false);
		}
	});
	dialog.setLayout(new GridLayout(3 + (field != null ? 1 : 0), 1));
	TextArea textArea = new TextArea(message, 4, 80, TextArea.SCROLLBARS_NONE);
	textArea.setEditable(false);
	dialog.add(textArea);
	if (field != null) dialog.add(field);
	Button proceed = new Button(question ? "Yes" : "Proceed");
	proceed.addActionListener(new ButtonListener());
	dialog.add(proceed);
	Button cancel = new Button(question ? "No" : "Cancel");
	ButtonListener cancelListener = new ButtonListener();
	cancel.addActionListener(cancelListener);
	dialog.add(cancel);
	dialog.setSize(640, 200);
	dialog.setModal(true);
	dialog.setVisible(true);
	return cancelListener.pressed;
}

static boolean dialog(String title)
{
	return dialog(title, title, null, false);
}

static void exec(String cmd, boolean check) throws InterruptedException, IOException
{
	Process process = Runtime.getRuntime().exec(cmd);
	process.waitFor();
	if (!check || process.exitValue() == 0) return;
	dialog("Error " + process.exitValue());
	System.out.println("Command " + cmd + " returned error value " + process.exitValue());
	System.exit(process.exitValue());
}

static HashMap readServlets(File file) throws IOException, ParseException
{
	StreamTokenizer sTok = null;
	HashMap servlets = new HashMap();
	BufferedReader bf = new BufferedReader(new FileReader(file));
	try
	{
		sTok = new StreamTokenizer(bf);
		sTok.resetSyntax();
		sTok.whitespaceChars(0, 32);
		sTok.wordChars(33, 255);
		sTok.ordinaryChar('=');
		sTok.commentChar('#');
		sTok.quoteChar('"');
		Vector options = null;
		HashMap initParameters = null;
		String name = null, locator = null;
		while (sTok.nextToken() != sTok.TT_EOF)
		{
			if (sTok.ttype != sTok.TT_WORD && sTok.ttype != '"') throw new ParseException("Unexpected token", 0);
			String s = sTok.sval;
			if (initParameters != null && initParameters.size() == 2 && s.startsWith(":"))
			{
				options.addElement(s);
				continue;
			}
			if (sTok.nextToken() == '=')
			{
				sTok.nextToken();
				if (sTok.ttype != sTok.TT_WORD && sTok.ttype != '"' || initParameters == null) throw new ParseException("Unexpected token", 0);
				initParameters.put(s, sTok.sval);
				continue;
			}
			if (sTok.ttype == sTok.TT_EOF) break;
			if (name != null) servlets.put(name, initParameters);
			name = s;
			locator = sTok.sval;
			options = new Vector();
			initParameters = new HashMap();
			initParameters.put(Boolean.TRUE, locator);
			initParameters.put(Boolean.FALSE, options);
		}
		if (name != null) servlets.put(name, initParameters);
	}
	catch (Exception ex)
	{
		if (file != null && sTok != null) throw new ParseException("Error in file " + file.getPath() + " in line " + sTok.lineno(), sTok.lineno());
		throw new ParseException("Error reading parameter files", 0);
	}
	finally
	{
		bf.close();
	}
	return servlets;
}

static String encode(String value)
{
	StringBuffer valueBuf = new StringBuffer();
	int l = value.length();
	for (int i = 0; i < l; i++)
	{
		if ("\\\"".indexOf(value.charAt(i)) != -1) valueBuf.append('\\');
		valueBuf.append(value.charAt(i));
	}
	return valueBuf.toString();
}

static void saveServlets(File file, HashMap servlets) throws IOException
{
	TreeMap servletMap = new TreeMap(new StringComparator());
	Iterator servletIter = servlets.entrySet().iterator();
	while (servletIter.hasNext())
	{
		Map.Entry entry = (Map.Entry)servletIter.next();
		servletMap.put(entry.getKey(), entry.getValue());
	}
	PrintWriter pw = new PrintWriter(new FileOutputStream(file));
	try
	{
		pw.println("\n# Servlets saved by pos_install at " + new Date() + "\n");
		servletIter = servletMap.entrySet().iterator();
		while (servletIter.hasNext())
		{
			Map.Entry entry = (Map.Entry)servletIter.next();
			HashMap initParameters = (HashMap)entry.getValue();
			pw.print(entry.getKey() + " " + initParameters.get(Boolean.TRUE));
			Vector options = (Vector)initParameters.get(Boolean.FALSE);
			if (options != null)
			{
				Iterator iter = options.iterator();
				while (iter.hasNext()) pw.print(" " + iter.next());
			}
			pw.println();
			TreeMap initParameterMap = new TreeMap(new StringComparator());
			Iterator initParameterIter = initParameters.entrySet().iterator();
			while (initParameterIter.hasNext())
			{
				Map.Entry initEntry = (Map.Entry)initParameterIter.next();
				if (!(initEntry.getKey() instanceof String)) continue;
				initParameterMap.put(initEntry.getKey(), initEntry.getValue());
			}
			initParameterIter = initParameterMap.entrySet().iterator();
			while (initParameterIter.hasNext())
			{
				Map.Entry initEntry = (Map.Entry)initParameterIter.next();
				pw.println("\t" + initEntry.getKey() + "=\"" + encode((String)initEntry.getValue()) + "\"");
			}
			pw.println();
		}
	}
	finally
	{
		pw.close();
	}
}

public static String encodedArgs(HashMap initParameters)
{
	StringBuffer initArgs = new StringBuffer();
	Iterator iter = initParameters.entrySet().iterator();
	for (boolean started = false; iter.hasNext();)
	{
		Map.Entry initEntry = (Map.Entry)iter.next();
		if (!(initEntry.getKey() instanceof String)) continue;
		if (started) initArgs.append(',');
		initArgs.append(initEntry.getKey()).append('=').append(Support.encode((String)initEntry.getValue(), ",=\\#"));
		started = true;
	}
	return initArgs.toString();
}

public static void main(String argv[])
{
	try {
	exec("bin\\pos_install.exe", true);
	Properties regValues = new Properties();
	FileInputStream fin = new FileInputStream("reg_values.properties");
	try
	{
		regValues.load(fin);
	}
	finally
	{
		fin.close();
	}
	boolean activeDirectory = !dialog("Active Directory", "Are you using Microsoft Active Directory as LDAP Server?", null, true);
	File file = new File((activeDirectory ? "ads_" : "") + "pos_servlet_init");
	HashMap posServlet = readServlets(file);
	Iterator iter = posServlet.entrySet().iterator();
	if (!iter.hasNext()) throw new ParseException("Parameter file " + file.getPath() + " is bad", 0);
	Map.Entry entry = (Map.Entry)iter.next();
	String name = (String)entry.getKey();
	HashMap initParameters = (HashMap)entry.getValue();
	String locator = (String)initParameters.get(Boolean.TRUE);
	Dialog dialog = new Dialog(new Frame(), "Personal Office Services settings");
	dialog.setModal(true);
	dialog.setLayout(new GridLayout(9, 2));
	dialog.add(new Label("Platform"));
	Choice platform = new Choice();
	platform.add("Nokia WAP Server");
	platform.add("JRun 2.3");
	platform.add("JavaServer Web Development Kit 1.0");
	platform.add("Java Servlet Development Kit 2.0");
	platform.add("Keppi Server");
	platform.select(4);
	dialog.add(platform);
	dialog.add(new Label("BaseURL"));
	TextField baseUrl = new TextField((String)initParameters.get("cal:baseUrl"));
	dialog.add(baseUrl);
	dialog.add(new Label("IMAP host"));
	TextField host = new TextField((String)initParameters.get("msg:host"));
	dialog.add(host);
	dialog.add(new Label("SMTP host"));
	TextField smtpHost = new TextField((String)initParameters.get("msg:smtpHost"));
	dialog.add(smtpHost);
	dialog.add(new Label("Provider URL"));
	TextField providerUrl = new TextField((String)initParameters.get("ldap:environment:java.naming.provider.url"));
	dialog.add(providerUrl);
	dialog.add(new Label("Security principal"));
	TextField securityPrincipal = new TextField((String)initParameters.get("ldap:environment:java.naming.security.principal"));
	dialog.add(securityPrincipal);
	dialog.add(new Label("Security credentials"));
	TextField securityCredentials = new TextField((String)initParameters.get("ldap:environment:java.naming.security.credentials"));
	dialog.add(securityCredentials);
	dialog.add(new Label("Security authentication"));
	Choice securityAuthentication = new Choice();
	securityAuthentication.add("simple");
	securityAuthentication.add("CRAM-MD5");
	String authentication = (String)initParameters.get("ldap:environment:java.naming.security.authentication");
	for (int i = 0; i < 2; i++)
	if (securityAuthentication.getItem(i).equalsIgnoreCase(authentication))
	{
		securityAuthentication.select(i);
		break;
	}
	dialog.add(securityAuthentication);
	Button install = new Button("Install");
	dialog.add(install);
	Button cancel = new Button("Cancel");
	dialog.add(cancel);
	ButtonListener installListener = new ButtonListener();
	install.addActionListener(installListener);
	ButtonListener cancelListener = new ButtonListener();
	cancel.addActionListener(cancelListener);
	dialog.addWindowListener(new WindowAdapter()
	{
		public void windowClosing(WindowEvent e)
		{
			e.getWindow().setVisible(false);
		}
	});
	dialog.setSize(640, 300);
	do
	{
		dialog.setVisible(true);
		if (cancelListener.pressed) break;
		if (!installListener.pressed) break;
		initParameters.put("cal:baseUrl", baseUrl.getText());
		initParameters.put("msg:host", host.getText());
		initParameters.put("msg:smtpHost", smtpHost.getText());
		initParameters.put("ldap:environment:java.naming.provider.url", providerUrl.getText());
		initParameters.put("ldap:environment:java.naming.security.principal", securityPrincipal.getText());
		initParameters.put("ldap:environment:java.naming.security.credentials", securityCredentials.getText());
		initParameters.put("ldap:environment:java.naming.security.credentials", securityCredentials.getText());
		initParameters.put("ldap:environment:java.naming.security.authentication", securityAuthentication.getSelectedItem());
		String s;
		switch (platform.getSelectedIndex()) {
		case 0:
		{
			String JavaHome = regValues.getProperty("Software\\JavaSoft\\Java Runtime Environment\\1.3_JavaHome");
			if (JavaHome == null)
			{
				if (dialog("Java Runtime Environment 1.3 is not installed or you have no administrator-privileges")) break;
				continue;
			}
			String NokiaWAPServer = regValues.getProperty("Software\\Nokia\\Nokia WAP Server_Path");
			if (NokiaWAPServer == null)
			{
				if (dialog("Nokia WAP Server is not installed or you have no administrator-privileges")) break;
				continue;
			}
			Properties ServletsProperties = new Properties(), UrlMappingsProperties = new Properties();
			File ServletsPropertiesFile = new File(NokiaWAPServer, "config\\Servlets.config"),
			UrlMappingsPropertiesFile = new File(NokiaWAPServer, "config\\UrlMappings.config");
			fin = new FileInputStream(ServletsPropertiesFile);
			try
			{
				ServletsProperties.load(fin);
			}
			finally
			{
				fin.close();
			}
			fin = new FileInputStream(UrlMappingsPropertiesFile);
			try
			{
				UrlMappingsProperties.load(fin);
			}
			finally
			{
				fin.close();
			}
			new File(JavaHome, "lib\\ext\\xml.jar").delete();
			copyDir(new File("lib\\ext"), new File(JavaHome, "lib\\ext"), false);
			copyDir(new File("nokia"), new File(JavaHome, "lib\\ext"), false);
			File nws_xml = new File(NokiaWAPServer, "xml");
			copyDir(new File("xml"), nws_xml, false);
			String servletNames = ServletsProperties.getProperty("servlet.names");
			if (servletNames == null) throw new ParseException("File " + ServletsPropertiesFile + " is bad, property servlet.names is missing", 0);
			StringTokenizer st = new StringTokenizer(servletNames, ",");
			StringBuffer sb = new StringBuffer();
			while (st.hasMoreTokens())
			{
				s = st.nextToken();
				if (s.trim().equalsIgnoreCase("pos")) continue;
				if (sb.length() > 0) sb.append(',');
				sb.append(s);
			}
			if (sb.length() > 0) sb.append(',');
			sb.append("Pos");
			initParameters.put("directory", nws_xml.getCanonicalPath());
			ServletsProperties.put("servlet.names", sb.toString());
			ServletsProperties.put("servlet.Pos.code", locator);
			ServletsProperties.put("servlet.Pos.initArgs", encodedArgs(initParameters));
			Vector urlmapNames = new Vector(), urlmapUrls = new Vector();
			for (int n = 1; (s = UrlMappingsProperties.getProperty("urlmap." + n + ".name")) != null; n++)
			{
				if (s.trim().equalsIgnoreCase("pos")) continue;
				urlmapNames.addElement(s);
				if ((s = UrlMappingsProperties.getProperty("urlmap." + n + ".url")) == null)
				throw new ParseException("File " + UrlMappingsPropertiesFile + " is bad, property urlmap." + n + ".url is missing", 0);
				urlmapUrls.addElement(s);
			}
			urlmapNames.insertElementAt("Pos", 0);
			urlmapUrls.insertElementAt("http://pos/", 0);
			for (int i = 0; i < urlmapNames.size(); i++)
			{
				UrlMappingsProperties.put("urlmap." + (i + 1) + ".name", urlmapNames.elementAt(i));
				UrlMappingsProperties.put("urlmap." + (i + 1) + ".url", urlmapUrls.elementAt(i));
			}
			UrlMappingsProperties.put("urlmap.total", String.valueOf(urlmapNames.size()));
			FileOutputStream fout = new FileOutputStream(ServletsPropertiesFile);
			try
			{
				ServletsProperties.store(fout, "Configuration of Servlets stored by pos_install");
			}
			finally
			{
				fout.close();
			}
			fout = new FileOutputStream(UrlMappingsPropertiesFile);
			try
			{
				UrlMappingsProperties.store(fout, "Configuration of UrlMappings stored by pos_install");
			}
			finally
			{
				fout.close();
			}
			break;
		}
		case 1:
		{
			TextField JRunDir = new TextField("C:\\JRun");
			if (dialog("JRun root directory", "Please give the JRun 2.3 root directory containing the jsm-default directory", JRunDir, false)) break;
			if (!new File(JRunDir.getText()).exists())
			{
				if (dialog("Directory " + JRunDir.getText() + " does not exist")) break;
				continue;
			}
			Properties jsmProperties = new Properties();
			File jsmPropertiesFile = new File(JRunDir.getText(), "jsm-default\\properties\\jsm.properties");
			fin = new FileInputStream(jsmPropertiesFile);
			try
			{
				jsmProperties.load(fin);
			}
			finally
			{
				fin.close();
			}
			File jsm_lib = new File(JRunDir.getText(), "jsm-default\\lib");
			new File(jsm_lib, "xml.jar").delete();
			copyDir(new File("lib\\ext"), jsm_lib, false);
			String javaClasspath = jsmProperties.getProperty("java.classpath");
			if (javaClasspath == null) javaClasspath = "";
			StringTokenizer st = new StringTokenizer(javaClasspath, ";");
			ArrayList paths = new ArrayList();
			while (st.hasMoreTokens()) paths.add(st.nextToken().trim());
			String jarNames[] = new File("lib\\ext").list();
			for (int i = 0; i < jarNames.length; i++) paths.add(new File(jsm_lib, jarNames[i]).getCanonicalPath());
			for (int i = paths.size() - 1; i >= 0; i--)
			{
				int x = paths.indexOf(paths.get(i));
				if (x != i) paths.remove(i);
			}
			StringBuffer javaClasspathBuf = new StringBuffer();
			for (int i = 0; i < paths.size(); i++) javaClasspathBuf.append(paths.get(i)).append(';');
			jsmProperties.put("java.classpath", javaClasspathBuf.toString());
			copyFile(jsmPropertiesFile, new File(jsmPropertiesFile.getCanonicalPath() + ".back"));
			FileOutputStream fout = new FileOutputStream(jsmPropertiesFile);
			try
			{
				jsmProperties.store(fout, "jsm.properties stored by pos_install");
			}
			finally
			{
				fout.close();
			}
			File jsm_xml = new File(JRunDir.getText(), "jsm-default\\xml");
			copyDir(new File("xml"), jsm_xml, false);
			initParameters.put("directory", jsm_xml.getCanonicalPath());
			String dirNames[] = {"jse", "jseweb"};
			for (int i = 0; i < dirNames.length; i++)
			{
				Properties servletsProperties = new Properties();
				File servletsPropertiesFile = new File(JRunDir.getText(), "jsm-default\\services\\" + dirNames[i] + "\\properties\\servlets.properties");
				copyFile(servletsPropertiesFile, new File(servletsPropertiesFile.getCanonicalPath() + ".back"));
				fin = new FileInputStream(servletsPropertiesFile);
				try
				{
					servletsProperties.load(fin);
				}
				finally
				{
					fin.close();
				}
				name = name.substring(name.lastIndexOf('/') + 1);
				servletsProperties.put("servlet." + name + ".code", locator);
				servletsProperties.put("servlet." + name + ".args", encodedArgs(initParameters));
				servletsProperties.put("servlet." + name + ".preload", "true");
				fout = new FileOutputStream(servletsPropertiesFile);
				try
				{
					servletsProperties.store(fout, "servlets.properties stored by pos_install");
				}
				finally
				{
					fout.close();
				}
			}
			break;
		}
		case 2:
		{
			String JavaHome = regValues.getProperty("Software\\JavaSoft\\Java Development Kit\\1.3_JavaHome");
			if (JavaHome == null)
			{
				if (dialog("Java Development Kit 1.3 is not installed or you have no administrator-privileges")) break;
				continue;
			}
			TextField jswdkRootDir = new TextField("C:\\jswdk-1.0.1\\webpages");
			if (dialog("JSWDK root directory", "Please give the JSWDK root directory containing the WEB-INF directory. Ensure that files there are write enabled.", jswdkRootDir, false)) break;
			if (!new File(jswdkRootDir.getText()).exists())
			{
				if (dialog("Directory " + jswdkRootDir.getText() + " does not exist")) break;
				continue;
			}
			new File(JavaHome, "jre\\lib\\ext\\xml.jar").delete();
			copyDir(new File("lib\\ext"), new File(JavaHome, "jre\\lib\\ext"), false);
			File jswdk_xml = new File(jswdkRootDir.getText(), "xml");
			copyDir(new File("xml"), jswdk_xml, false);
			initParameters.put("directory", jswdk_xml.getCanonicalPath());
			Properties servletsProperties = new Properties();
			File servletsPropertiesFile = new File(new File(jswdkRootDir.getText(), "WEB-INF"), "servlets.properties");
			if (servletsPropertiesFile.exists())
			{
				copyFile(servletsPropertiesFile, new File(new File(jswdkRootDir.getText(), "WEB-INF"), "servlets.properties.back"));
				fin = new FileInputStream(servletsPropertiesFile);
				try
				{
					servletsProperties.load(fin);
				}
				finally
				{
					fin.close();
				}
			}
			else new File(servletsPropertiesFile.getParent()).mkdirs();
			name = name.substring(name.lastIndexOf('/') + 1);
			servletsProperties.put(name + ".code", locator);
			servletsProperties.put(name + ".initparams", encodedArgs(initParameters));
			FileOutputStream fout = new FileOutputStream(servletsPropertiesFile);
			try
			{
				servletsProperties.store(fout, "servlet.properties stored by pos_install");
			}
			finally
			{
				fout.close();
			}
			break;
		}
		case 3:
		{
			String JavaHome = regValues.getProperty("Software\\JavaSoft\\Java Development Kit\\1.3_JavaHome");
			if (JavaHome == null)
			{
				if (dialog("Java Development Kit 1.3 is not installed or you have no administrator-privileges")) break;
				continue;
			}
			String JSDKPath = regValues.getProperty("Software\\JavaSoft\\Java Servlet Development Kit 2.0\\2.0_Path");
			if (JSDKPath == null)
			{
				if (dialog("Java Servlet Development Kit 2.0 is not installed or you have no administrator-privileges")) break;
				continue;
			}
			new File(JavaHome, "jre\\lib\\ext\\xml.jar").delete();
			copyDir(new File("lib\\ext"), new File(JavaHome, "jre\\lib\\ext"), false);
			File jsdk_xml = new File(JSDKPath, "xml");
			copyDir(new File("xml"), jsdk_xml, false);
			initParameters.put("directory", jsdk_xml.getCanonicalPath());
			Properties servletProperties = new Properties();
			File servletPropertiesFile = new File(JSDKPath, "servlet.properties");
			if (servletPropertiesFile.exists())
			{
				copyFile(servletPropertiesFile, new File(JSDKPath, "servlet.properties.back"));
				fin = new FileInputStream(servletPropertiesFile);
				try
				{
					servletProperties.load(fin);
				}
				finally
				{
					fin.close();
				}
			}
			name = name.substring(name.lastIndexOf('/') + 1);
			servletProperties.put("servlet." + name + ".code", locator);
			servletProperties.put("servlet." + name + ".initArgs", encodedArgs(initParameters));
			FileOutputStream fout = new FileOutputStream(servletPropertiesFile);
			try
			{
				servletProperties.store(fout, "servlet.properties stored by pos_install");
			}
			finally
			{
				fout.close();
			}
			fout = new FileOutputStream(new File(JSDKPath, "sr.bat"));
			try
			{
				fout.write("bin\\servletrunner -s servlet.properties %1 %2 %3 %4 %5".getBytes());
			}
			finally
			{
				fout.close();
			}
			dialog("Run servletrunner", "You can go to directory " + JSDKPath + " and invoke batch file sr.bat there to run servletrunner. " +
			"It uses parameter file " + servletPropertiesFile.getCanonicalPath() + " modified by this program.", null, false);
			break;
		}
		case 4:
		{
			String JavaHome = regValues.getProperty("Software\\JavaSoft\\Java Runtime Environment\\1.3_JavaHome");
			if (JavaHome == null)
			{
				if (dialog("Java Runtime Environment 1.3 is not installed or you have no administrator-privileges")) break;
				continue;
			}
			File serverOptionsFile = new File("C:\\server_options");
			if (!serverOptionsFile.exists())
			{
				if (dialog("Keppi-server is not installed (C:\\server_options-file is missing)")) break;
				continue;
			}
			ByteArrayOutputStream bout = new ByteArrayOutputStream();
			byte b[] = new byte[Support.bufferLength];
			fin = new FileInputStream(serverOptionsFile);
			try
			{
				for (int n; (n = fin.read(b)) > 0;) bout.write(b, 0, n);
			}
			finally
			{
				fin.close();
			}
			StringTokenizer st = new StringTokenizer(new String(bout.toByteArray()));
			String directory = null;
			while (st.hasMoreTokens())
			{
				String arg = st.nextToken();
				if (arg.charAt(0) == '-')
				for (int i = 1; i < arg.length(); i++)
				switch (Character.toLowerCase(arg.charAt(i))) {
				case 'd':
					directory = st.nextToken();
					continue;
				case 'b':
				case 'c':
				case 'l':
				case 'p':
					st.nextToken();
					continue;
				default:
					continue;
				}
			}
			if (directory == null) throw new ParseException("Parameter file " + serverOptionsFile.getPath() + " is bad", 0);
			new File(JavaHome, "lib\\ext\\xml.jar").delete();
			copyDir(new File("lib\\ext"), new File(JavaHome, "lib\\ext"), false);
			File server_xml = new File(directory, "xml");
			copyDir(new File("xml"), server_xml, false);
			initParameters.put("directory", server_xml.getCanonicalPath());
			initParameters.put("getContentTypeFromServletContext", "true");
			File servletsFile = new File(directory, "servlets");
			HashMap servlets = servletsFile.exists() ? readServlets(servletsFile) : new HashMap();
			servlets.put(name, initParameters);
			copyFile(servletsFile, new File(directory, "servlets.back"));
			saveServlets(servletsFile, servlets);
			break;
		}
		}
		break;
	}
	while (!dialog("Retry installation?"));
	System.exit(0);
	} catch (Throwable th)
	{
		th.printStackTrace();
		dialog("Error " + th.toString());
		System.exit(1);
	}
}

}
