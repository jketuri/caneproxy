/*************************************************************************
** CONFIDENTIAL AND PROPRIETARY SOURCE CODE OF GENIENT LIMITED.
**
** Copyright (c) 2000 Genient Limited.
** All Rights Reserved.
**
** Use of this Source Code is subject to the terms of the applicable
** license agreement from Genient Limited.  The copyright
** notice(s) in this Source Code does not indicate actual or intended
** publication of this Source Code.
**
** @(#)LdapServlet.java					1.0					2000/07/27
**
**************************************************************************/
package com.genient.webmail;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import java.net.*;
import HTTPClient.*;

/**
 * Servlet for testing Ldap.
 */
public class LdapServlet extends HttpServlet {

	private String m_ldapsu;
	private String m_ldappw;
	private String m_ldapsrv;
	private String m_ldapport;
	private String m_wmsu;
	private String m_wmpw;
	private String m_wmsrv;
	private String m_wmport;
	private String m_wmlang;

/**
 * Handles HTTP GET request.
 * @param request
 * @param response
 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
		m_ldapsu = request.getParameter("ldapsu");
		m_ldappw = request.getParameter("ldappw");
		m_ldapsrv = request.getParameter("ldapsrv");
		m_ldapport = request.getParameter("ldapport");
		m_wmsu = request.getParameter("wmsu");
		m_wmpw = request.getParameter("wmpw");
		m_wmsrv = request.getParameter("wmsrv");
		m_wmport = request.getParameter("wmport");
		m_wmlang = request.getParameter("wmlang");
		Ldap l = new Ldap(m_ldapsu, m_ldappw, m_ldapsrv, m_ldapport);
		boolean created = false;
		try {
			created = l.createWmuser(request.getParameter("uid"),
				request.getParameter("ou"),
				request.getParameter("o"),
				request.getParameter("cn"),
				request.getParameter("givenname"),
				request.getParameter("sn"),
				request.getParameter("mailhost"),
				request.getParameter("maildeliveryoption"),
				request.getParameter("mail"),
				request.getParameter("upw"));
		} catch(WebmailException e) {
			//
		}
		String data = "";
		boolean touchlogin = (request.getParameter("touchlogin") != null);
		if (touchlogin) {
			Webmail wm = new Webmail(m_wmsu, m_wmpw, m_wmsrv, m_wmport, m_wmlang);
			try {
				data = wm.touchLogin(request.getParameter("uid"), request.getParameter("upw"));
			} catch(WebmailException e) {
				//
			}
		}
		String title = this.getServletConfig().getServletName();
		createOutput(data, title, created, request, response);
	}

/**
 * Handles HTTP POST request (calls doGet).
 * @param request
 * @param response
 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
		doGet(request, response);
	}

/**
 * Creates HTML output.
 * @param title page title.
 * @param created user succesfully created.
 * @param request
 * @param response
 */
	private void createOutput(String data, String title, boolean created, HttpServletRequest request, HttpServletResponse response)
		throws IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println(ServletUtilities.headWithTitle(title) + "<body>\n" + "<h1>" + title + "</h1>");
		out.println("Creation of user " + (created ? "succeeded" : "failed") +  ".<br>");
		boolean touchlogin = (request.getParameter("touchlogin") != null);
		if (touchlogin) {
			out.println("<hr>View source to see response from \"touch-login\"<br>\n<!--" + data +  "--><br>");
		}
		out.println("</body>");
		out.println("</html>");
	}

}