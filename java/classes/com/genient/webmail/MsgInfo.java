/*************************************************************************
** CONFIDENTIAL AND PROPRIETARY SOURCE CODE OF GENIENT LIMITED.
**
** Copyright (c) 2000 Genient Limited.
** All Rights Reserved.
**
** Use of this Source Code is subject to the terms of the applicable
** license agreement from Genient Limited.  The copyright
** notice(s) in this Source Code does not indicate actual or intended
** publication of this Source Code.
**
** @(#)MsgInfo.java						1.0					2000/07/27
**
**************************************************************************/
package com.genient.webmail;

import java.util.*;

/**
 * Auxiliary class for passing elementary message info.
 */
public class MsgInfo {

	private final static int READ = 0x0001;
	private final static int ATTACH = 0x0020;
	private final static int HIGH = 0x0040;

	private int m_msgid;
	private String m_subject;
	private String m_sender;
	private long m_date;
	private int m_flags;

/**
 * Constructor.
 * @param msgid Message id (relative to mbox).
 * @param subject Message subject.
 * @param sender Message sender.
 * @param flags Message flags.
 */
	public MsgInfo(int msgid, String subject, String sender, long date, int flags) {

		m_msgid = msgid;
		m_subject = subject;
		m_sender = sender;
		m_date = date;
		m_flags = flags;

	}

	public int getMsgId() {

		return m_msgid;

	}

	public String getSubject() {

		if (m_subject.equals("")) {
			return "[ No Subject ]";
		} else {
			return m_subject;
		}

	}

	public String getSender() {

		return m_sender;

	}

	public String getDate() {

		return new Date(m_date * 1000).toString();

	}

	public boolean isRead() {

		return ((m_flags & READ) == READ);

	}

	public boolean hasAttachment() {

		return ((m_flags & ATTACH) == ATTACH);

	}

	public boolean hasHighPriority() {

		return ((m_flags & HIGH) == HIGH);

	}

}