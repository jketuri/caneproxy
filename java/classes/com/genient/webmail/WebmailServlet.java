/*************************************************************************
** CONFIDENTIAL AND PROPRIETARY SOURCE CODE OF GENIENT LIMITED.
**
** Copyright (c) 2000 Genient Limited.
** All Rights Reserved.
**
** Use of this Source Code is subject to the terms of the applicable
** license agreement from Genient Limited.  The copyright
** notice(s) in this Source Code does not indicate actual or intended
** publication of this Source Code.
**
** @(#)WebmailServlet.java				1.0					2000/07/27
**
**************************************************************************/
package com.genient.webmail;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import java.net.*;
import HTTPClient.*;

/**
 * Servlet for testing Webmail.
 */
public class WebmailServlet extends HttpServlet {

	private String m_su;
	private String m_pw;
	private String m_srv;
	private String m_port;
	private String m_lang;

/**
 * Handles HTTP GET request.
 * @param request
 * @param response
 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
		m_su = request.getParameter("su");
		m_pw = request.getParameter("pw");
		m_srv = request.getParameter("srv");
		m_port = request.getParameter("port");
		m_lang = request.getParameter("lang");
		HttpSession session = request.getSession(true);
		if (!session.isNew()) {
			// invalidateRequested => invalidate
			boolean invalidateRequested = (request.getParameter("invprev") != null);
			String oldUser = (String)session.getAttribute("user");
			String newUser = (String)request.getParameter("user");
			// otherUser => invalidate
			boolean otherUser = !oldUser.equals(newUser);
			if (invalidateRequested || otherUser) {
				session.invalidate();
				session = request.getSession(true);
				storeState(session, request);
			}
		} else {
			storeState(session, request);
		}
		Webmail wm = new Webmail(m_su, m_pw, m_srv, m_port, m_lang);
		String link = "";
		Hashtable links = new Hashtable();
		try {
			link = wm.getLink((String)session.getAttribute("user"), request.getRemoteAddr(), session, "inbox");
			links.put("inboxLink", link);
			link = wm.getLink((String)session.getAttribute("user"), request.getRemoteAddr(), session, "collectext");
			links.put("collectextLink", link);
			link = wm.getLink((String)session.getAttribute("user"), request.getRemoteAddr(), session, "searchfor");
			links.put("searchforLink", link);
			link = wm.getLink((String)session.getAttribute("user"), request.getRemoteAddr(), session, "folders");
			links.put("foldersLink", link);
			link = wm.getLink((String)session.getAttribute("user"), request.getRemoteAddr(), session, "mailto", request.getParameter("mailto"));
			links.put("mailtoLink", link);
			link = wm.getLink((String)session.getAttribute("user"), request.getRemoteAddr(), session, "readmsg");
			links.put("readmsgLink", link);
			link = wm.getLink((String)session.getAttribute("user"), request.getRemoteAddr(), session, "config");
			links.put("configLink", link);
		} catch(WebmailException e) {
			//
		}
		int unread = 0;
		int count = Integer.valueOf(request.getParameter("count")).intValue();
		int start = Integer.valueOf(request.getParameter("start")).intValue();
		MsgInfo[] msginfo = new MsgInfo[count];
		boolean mboxinfo = (request.getParameter("mboxinfo") != null);
		if (mboxinfo) {
			try {
				unread = wm.getUnread((String)session.getAttribute("user"), request.getParameter("mbox"));
				msginfo = wm.getMsgs((String)session.getAttribute("user"), request.getParameter("mbox"), start, count);
			} catch(WebmailException e) {
				//
			}
		}
		// Create the output.
		String title = this.getServletConfig().getServletName();
		createOutput(title, links, unread, msginfo, request, response);
	}

/**
 * Handles HTTP POST request (calls doGet).
 * @param request
 * @param response
 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException {
		doGet(request, response);
	}

/**
 * Stores server-Webmail session data in client-server session object.
 * @param session
 * @param request
 */
	private void storeState(HttpSession session, HttpServletRequest request) {
		session.setAttribute("user", request.getParameter("user"));
	}

/**
 * Creates HTML output.
 * @param title page title.
 * @param links links to various Webmail functions.
 * @param unread
 * @param msginfo
 * @param request
 * @param response
 */
	private void createOutput(String title, Hashtable links, int unread, MsgInfo[] msginfo, HttpServletRequest request, HttpServletResponse response)
		throws IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println(ServletUtilities.headWithTitle(title) + "<body>\n" + "<h1>" + title + "</h1>");
		out.println("<a href=\"" + (String)links.get("inboxLink") + "\">inbox</a><br>");
		out.println("<a href=\"" + (String)links.get("collectextLink") + "\">collect external</a><br>");
		out.println("<a href=\"" + (String)links.get("searchforLink") + "\">search</a><br>");
		out.println("<a href=\"" + (String)links.get("foldersLink") + "\">folders</a><br>");
		out.println("<a href=\"" + (String)links.get("mailtoLink") + "\">mail to:" +
			request.getParameter("mailto") + "</a><br>");
		out.println("<a href=\"" + (String)links.get("configLink") + "\">config</a><br>");
		boolean mboxinfo = (request.getParameter("mboxinfo") != null);
		if (mboxinfo) {
			out.println("<br>");
			out.println("Inbox contains " + unread + " unread message" + (unread==1 ? "" : "s") + "<br>");
			out.println("<br>");
			out.println("<table border=\"1\">");
			out.println("<tr><td>Message Id</td><td>Subject</td><td>Sender</td><td>Date</td><td>Unread</td><td>Urgent</td><td>Attachment</td></tr>");
			for (int i = 0; i < msginfo.length; i++) {
				if (msginfo[i].getMsgId() == -1) {
					break;
				}
				out.print("<tr>");
				out.print("<td>");
				out.print(msginfo[i].getMsgId());
				out.print("</td>");
				out.print("<td>");
				out.print("<a href=\"" + (String)links.get("readmsgLink") + i + "\">" + msginfo[i].getSubject() + "</a>");
				out.print("</td>");
				out.print("<td>");
				out.print(msginfo[i].getSender());
				out.print("</td>");
				out.print("<td>");
				out.print(msginfo[i].getDate());
				out.print("</td>");
				out.print("<td>");
				out.print((msginfo[i].isRead() ? "_" : "X"));
				out.print("</td>");
				out.print("<td>");
				out.print((msginfo[i].hasHighPriority() ? "X" : "_"));
				out.print("</td>");
				out.print("<td>");
				out.print((msginfo[i].hasAttachment() ? "X" : "_"));
				out.print("</td>");
				out.println("</tr>");
			}
			out.println("</table>");
		}
		out.println("</body>");
		out.println("</html>");
	}

}