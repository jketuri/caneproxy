/*************************************************************************
** CONFIDENTIAL AND PROPRIETARY SOURCE CODE OF GENIENT LIMITED.
**
** Copyright (c) 2000 Genient Limited.
** All Rights Reserved.
**
** Use of this Source Code is subject to the terms of the applicable
** license agreement from Genient Limited.  The copyright
** notice(s) in this Source Code does not indicate actual or intended
** publication of this Source Code.
**
** @(#)Webmail.java						1.0					2000/07/26
**
**************************************************************************/
package com.genient.webmail;

import java.io.*;
import java.net.*;
import java.util.*;
import javax.mail.*;

/**
 * Class for generating URLs to Webmail functions.
 */
public class Webmail {

	private String m_su;
	private String m_pw;
	private String m_srv;
	private String m_port;
	private String m_lang;

/**
 * Constructor.
 * @param su Msg server super user.
 * @param pw Msg server super user's password.
 * @param srv Msg server fully qualified domain name.
 * @param port Msg server's Webmail port.
 * @param lang Webmail language.
 */
	public Webmail(String su, String pw, String srv, String port, String lang) {

		m_su = su;
		m_pw = pw;
		m_srv = srv;
		m_port = port;
		m_lang= lang;

	}

/**
 * Returns URL to call specified Webmail function for user from their client's IP number.
 * Logs on if no Webmail session Id is found in session.
 * This version for Webmail functions without parameters.
 * @param uid user id.
 * @param clientip client's IP number.
 * @param session App server session.
 * @param func Webmail function = [inbox|collectext|folders|config|contacts|searchfor|readmsg].
 * @return the URL.
 */
	public String getLink(String uid, String clientip, HttpSession session, String func)
		throws WebmailException {

		String wmsid = getStoredWmsid(uid, clientip, session);
		String baseLink = "http://" +
			m_srv + ":" + m_port + "/" +
			m_lang + "/" +
			"mail.html" + "?" +
			"sid=" + wmsid + "&" +
			"lang=" + m_lang + "&" +
			"host=http://" + m_srv + ":" + m_port + "/&" +
			"cert=false";
		if (func.equals("inbox")) {
			return baseLink;
		} else if (func.equals("collectext")) {
			return baseLink + "&collectext=true";
		} else if (func.equals("folders")) {
			return baseLink + "&folders=true";
		} else if (func.equals("config")) {
			return baseLink + "&config=true";
		} else if (func.equals("contacts")) {
			return baseLink + "&contacts=true";
		} else if (func.equals("searchfor")) {
			return baseLink + "&searchfor=true";
		} else if (func.equals("readmsg")) {
			return baseLink + "&readmsg=";
		} else {
			throw new WebmailException("Unknown param :" + func);
		}

	}

/**
 * Returns URL to call specified Webmail function for user from their client's IP number.
 * Logs on if no Webmail session Id is found in session.
 * This version for function with one parameter (String).
 * @param uid user id.
 * @param clientip client's IP number.
 * @param session App server session.
 * @param func Webmail function = [mailto].
 * @param param Webmail function parameter.
 * @return the URL.
 */
	public String getLink(String uid, String clientip, HttpSession session, String func,
		String param) throws WebmailException {

		String wmsid = getStoredWmsid(uid, clientip, session);
		String baseLink = "http://" +
			m_srv + ":" + m_port + "/" +
			m_lang + "/" +
			"mail.html" + "?" +
			"sid=" + wmsid + "&" +
			"lang=" + m_lang + "&" +
			"host=http://" + m_srv + ":" + m_port + "/&" +
			"cert=false";
		if (func.equals("mailto")) {
			return baseLink + "&mailto=" + param;
		} else {
			throw new WebmailException("Unknown parameter: " + func);
		}

	}

/**
 */
	public String touchLogin(String uid, String pw) throws WebmailException {

		String wmsid = "";
/*
		try {
			wmsid = getWmsidByProxy(uid, InetAddress.getLocalHost().toString());
		} catch(UnknownHostException e) {
			throw new WebmailException(e);
		}

		String logonLink="http://" +
			m_srv + ":" +
			m_port + "/login.msc?" +
			"user=" + uid + "&" +
			"password=" + pw;
		try {
			URL logonUrl = new URL(logonLink);
			HTTPConnection conn = new HTTPClient.HTTPConnection(logonUrl);
			HTTPResponse resp = conn.Get(logonUrl.getFile());
			URI uri = resp.getEffectiveURI();
			String URIstr = uri.toString();
			int pos = URIstr.indexOf("sid=") ;
			if (pos<0) {
				throw new Exception("No substring \"sid=\" found in response URI.");
			}
			String wmsidEq = URIstr.substring(pos, URIstr.length());
			// Get rid of the first 4 chars ("sid=").
			wmsid = wmsidEq.substring(4, wmsidEq.indexOf("&"));
			if (wmsid.equals("")) {
				throw new Exception("No value for substring \"sid=\" found in response URI.");
			}
		} catch(IOException e) {
			throw new WebmailException(e);
		} catch(ModuleException e) {
			throw new WebmailException(e);
		} catch(Exception e) {
			throw new WebmailException(e);
		}
		String inboxLink = "http://" +
			m_srv + ":" + m_port + "/" +
			m_lang + "/" +
			"mail.html" + "?" +
			"sid=" + wmsid + "&" +
			"lang=" + m_lang + "&" +
			"host=http://" + m_srv + ":" + m_port + "/&" +
			"cert=false";
		String data = "";
		try {
			URL inboxUrl = new URL(inboxLink);
			HTTPConnection conn = new HTTPClient.HTTPConnection(inboxUrl);
			HTTPResponse resp = conn.Get(inboxUrl.getFile());
			data = new String(resp.getData());
		} catch(IOException e) {
			throw new WebmailException(e);
		} catch(ModuleException e) {
			throw new WebmailException(e);
		}
		String logoutLink="http://" +
			m_srv + ":" +
			m_port + "/cmd.msc?" +
			"cmd=logout" + "&" +
			"sid=" + wmsid;
		String data2 = "";
		try {
			URL logoutUrl = new URL(logoutLink);
			HTTPConnection conn = new HTTPClient.HTTPConnection(logoutUrl);
			HTTPResponse resp = conn.Get(logoutUrl.getFile());
			data2 = new String(resp.getData());
		} catch(IOException e) {
			throw new WebmailException(e);
		} catch(ModuleException e) {
			throw new WebmailException(e);
		}
		return data;
*/

	}

/**
 * Returns URL to log out user from Webmail.
 * Webmail session Id must exist in session (user must be logged on).
 * @param session App server session.
 * @return the URL.
 */
	public String getLogoutLink(HttpSession session) throws WebmailException {

		String wmsid = "";
		if (session.getAttribute("wmsid") != null) {
			wmsid = (String)session.getAttribute("wmsid");
		} else {
			throw new WebmailException("No wmsid found in session.");
		}
		String logoutLink="http://" +
			m_srv + ":" +
			m_port + "/cmd.msc?" +
			"cmd=logout" + "&" +
			"sid=" + wmsid;
		return logoutLink;

	}

/**
 * Returns number of unread messages in user's mbox.
 * @param uid user id.
 * @param mbox mailbox to check.
 * @return the number.
 */
	public int getUnread(String uid, String mbox) throws WebmailException {

/*
		String wmsid = "";
		try {
			wmsid = getWmsidByProxy(uid, InetAddress.getLocalHost().toString());
		} catch(UnknownHostException e) {
			throw new WebmailException(e);
		}
		String mboxLink="http://" +
			m_srv + ":" +
			m_port + "/mbox.msc?" +
			"sid=" + wmsid + "&" +
			"mbox=" + mbox;
		String data = "";
		try {
			URL mboxUrl = new URL(mboxLink);
			HTTPConnection conn = new HTTPClient.HTTPConnection(mboxUrl);
			HTTPResponse resp = conn.Get(mboxUrl.getFile());
			data = new String(resp.getData());
		} catch(IOException e) {
			throw new WebmailException(e);
		} catch(ModuleException e) {
			throw new WebmailException(e);
		}
		return extractUnread(data);
*/
	Folder folder = store.getFolder(mbox);
	folder.open(Folder.READ_ONLY);
	try
	{
		return folder.getUnreadMessageCount();
	}
	finally
	{
		folder.close();
	}
}

/**
 * Extracts number of unread messages from data.
 * @param data response data from request to mbox.msc Webmail component.
 * @return the number.
 */
/*
	private int extractUnread(String data) throws WebmailException {

		// Find first occurrence of "var size=".
		int pos = data.indexOf("var size=");
		if (pos < 0) {
			throw new WebmailException("No \"var size=\" found in response data.");
		}
		int pos2 = data.indexOf("\n", pos);
		String str = data.substring(pos, pos2);
		pos = str.indexOf("=");
		int size = Integer.valueOf(str.substring(pos + 1, str.length())).intValue();
		// Find first occurrence of "var seen=".
		pos = data.indexOf("var seen=");
		if (pos < 0) {
			throw new WebmailException("No \"var seen=\" found in response data.");
		}
		pos2 = data.indexOf("\n", pos);
		str = data.substring(pos, pos2);
		pos = str.indexOf("=");
		int seen = Integer.valueOf(str.substring(pos + 1, str.length())).intValue();
		return (size - seen);

	}
 */

/**
 * Returns array with basic message info for messages in user's mailbox.
 * @param uid user id.
 * @param mbox mailbox to check.
 * @param start first message in mailbox for which to return info.
 * @param count number of messages in mailbox for which to return info.
 * @return the message info array.
 */
	public MsgInfo[] getMsgs(String uid, String mbox, int start, int count)
		throws WebmailException {
/*
		String wmsid = "";
		try {
			wmsid = getWmsidByProxy(uid, InetAddress.getLocalHost().toString());
		} catch(UnknownHostException e) {
			throw new WebmailException(e);
		}
		String mboxLink="http://" +
			m_srv + ":" +
			m_port + "/mbox.msc?" +
			"sid=" + wmsid + "&" +
			"mbox=" + mbox + "&" +
			"start=" + start + "&" +
			"count=" + count;
		String data = "";
		MsgInfo[] msginfo = new MsgInfo[count];
		try {
			URL mboxUrl = new URL(mboxLink);
			HTTPConnection conn = new HTTPClient.HTTPConnection(mboxUrl);
			HTTPResponse resp = conn.Get(mboxUrl.getFile());
			data = new String(resp.getData());
		} catch(IOException e) {
			throw new WebmailException(e);
		} catch(ModuleException e) {
			throw new WebmailException(e);
		}
		fillArray(data, msginfo);
		return msginfo;
*/
	Folder folder = store.getFolder(mbox);
	folder.open(Folder.READ_ONLY);
	try
	{
		Message messages[] = folder.getMessages(start, start + count);
		for (int i = 0; i < messages.length; i++)
		{
			msgInfo[i] = new MsgInfo("", messages[i].getSubject(),
			messages[i].getFrom(), messages[i].getReceivedDate().getTime(), 0);
		}
	}
	finally
	{
		folder.close(false);
	}

	}

/**
 */
/*
	private void fillArray(String data, MsgInfo[] msginfo) throws WebmailException {

		// Find first occurrence of "msg[".
		int pos = data.indexOf("msg[");
		if (pos < 0) {
			throw new WebmailException("No \"msg[\" found in response data.");
		}
		// Strip leading lines.
		data = data.substring(pos, data.length());
		// Find first occurrence of "var sort=".
		pos = data.indexOf("var sort=");
		if (pos < 0) {
			throw new WebmailException("No \"var sort=\" found in response data.");
		}
		// Strip trailing lines.
		data = data.substring(0, pos);
		// Parse msg lines.
		StringTokenizer t = new StringTokenizer(data, "\n");
		for (int i = 0; i < msginfo.length; i++) {
			if (t.hasMoreTokens()) {
				String msgline = t.nextToken();
				int pos2 = msgline.indexOf("(");
				// Strip leading chars.
				msgline = msgline.substring(pos2 + 1, msgline.length());
				StringTokenizer t2 = new StringTokenizer(msgline, ",");
				String msgid = t2.nextToken();
				String dummy = t2.nextToken();
				String date = t2.nextToken();
				String flags = t2.nextToken();
				String sender = t2.nextToken();
				// Unquote.
				sender = sender.substring(1, sender.length() - 1);
				String subject = t2.nextToken();
				// Unquote and lose trailing parenthesis.
				subject = subject.substring(1, subject.length() - 2);
				msginfo[i] = new MsgInfo(Integer.valueOf(msgid).intValue(), subject, sender, Long.valueOf(date).longValue(), Integer.valueOf(flags).intValue());
			} else {
				msginfo[i] = new MsgInfo(-1, "", "", 0, 0);
			}
		}

	}
 */

/**
 * Gets previously stored Webmail session id for user from their client's IP number, or gets a
 * fresh Webmail session id if none is found in session.
 * @param uid user id.
 * @param clientip client's IP number.
 * @param session App server session.
 * @return the Webmail session id.
 */
/*
	public String getStoredWmsid(String uid, String clientip, HttpSession session)
		throws WebmailException {

		String wmsid;
		if (session.getAttribute("wmsid") == null) {
			// No wmsid found in session; log on.
			wmsid = getWmsidByProxy(uid, clientip);
			// Store wmsid in session for future use.
			session.setAttribute("wmsid", wmsid);
		} else {
			// Wmsid found in session; no need to log on.
			// TODO: What happens if wmsid has timed-out?
			wmsid = (String)session.getAttribute("wmsid");
		}
		return wmsid;

	}
*/

/**
 * Gets a Webmail session id for user from given IP number.
 * @param uid user id.
 * @param ip IP number.
 * @return the Webmail session id.
 */
/*
	private String getWmsidByProxy(String uid, String ip)
		throws WebmailException {

		String wmsid = "";
		String logonLink="http://" +
			m_srv + ":" +
			m_port + "/login.msc?" +
			"user=" + m_su + "&" +
			"password=" + m_pw + "&" +
			"proxyauth=" + uid + "&" +
			"proxyaddress=" + ip;
		try {
			URL logonUrl = new URL(logonLink);
			HTTPConnection conn = new HTTPClient.HTTPConnection(logonUrl);
			HTTPResponse resp = conn.Get(logonUrl.getFile());
			// Example URI: http://adp.netscape.com/en/mail.html?sid=e5qv98v5t8xk08te&lang=en&host=http://adp.netscape.com/&cert=false
			URI uri = resp.getEffectiveURI();
			String URIstr = uri.toString();
			int pos = URIstr.indexOf("sid=") ;
			if (pos<0) {
				throw new Exception("No substring \"sid=\" found in response URI.");
			}
			String wmsidEq = URIstr.substring(pos, URIstr.length());
			// Get rid of the first 4 chars ("sid=").
			wmsid = wmsidEq.substring(4, wmsidEq.indexOf("&"));
			if (wmsid.equals("")) {
				throw new Exception("No value for substring \"sid=\" found in response URI.");
			}
		} catch(IOException e) {
			throw new WebmailException(e);
		} catch(ModuleException e) {
			throw new WebmailException(e);
		} catch(Exception e) {
			throw new WebmailException(e);
		}
		return wmsid;

	}
*/

}