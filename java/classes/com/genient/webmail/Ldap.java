/*************************************************************************
** CONFIDENTIAL AND PROPRIETARY SOURCE CODE OF GENIENT LIMITED.
**
** Copyright (c) 2000 Genient Limited.
** All Rights Reserved.
**
** Use of this Source Code is subject to the terms of the applicable
** license agreement from Genient Limited.  The copyright
** notice(s) in this Source Code does not indicate actual or intended
** publication of this Source Code.
**
** @(#)Ldap.java						1.0					2000/07/27
**
**************************************************************************/
package com.genient.webmail;

import java.util.*;
import javax.naming.*;
import javax.naming.directory.*;
import javax.naming.ldap.*;

/**
 * Class for manipulating LDAP server for Webmail/Contacts and Calendar.
 */
public class Ldap {

	private String m_su;
	private String m_pw;
	private String m_srv;
	private String m_port;

/**
 * Constructor.
 * @param su Dir server super user.
 * @param pw Dir server super user's password.
 * @param srv Dir server fully qualified domain name.
 * @param port Dir server's LDAP port.
 */
public Ldap(String su, String pw, String srv, String port)
{
	m_su = su;
	m_pw = pw;
	m_srv = srv;
	m_port = port;
}

void addAttribute(BasicAttributes attrs, String attrName, String attrValue)
{
	BasicAttribute attr = new BasicAttribute(attrName);
	StringTokenizer st = new StringTokenizer(attrValue, ",");
	while (st.hasMoreTokens()) attr.add(st.nextToken().trim());
	attrs.put(attr);
}

/**
 */
public boolean createWmuser(String uid, String ou, String o, String cn, String givenname,
String sn, String mailhost, String maildeliveryoption, String mail, String upw) throws WebmailException
{

/* Specify the DN of the new entry. */
		String dn = "uid=" + uid + ",ou=" + ou + ", o=" + o; // Example: "uid=wbjensen, ou=People, o=Airius.com";

/* Create a new attribute set for the entry. */
		BasicAttributes attrs = new BasicAttributes();

/* Create and add attributes to the attribute set. */
// Add object class attribute
		BasicAttribute attr = new BasicAttribute("objectclass", true);
		String objectclass_values[] = {"top", "person", "organizationalPerson", "inetOrgPerson",
			"mailrecipient", "nsmessagingserveruser"};
		for (int i = 0; i < objectclass_values.length; i++) attr.add(objectclass_values[i]);
		attrs.put(attr);
		addAttribute(attrs, "cn", cn);
		addAttribute(attrs, "givenname", givenname);
		addAttribute(attrs, "sn", sn);
		addAttribute(attrs, "mailhost", mailhost);
		addAttribute(attrs, "maildeliveryoption", maildeliveryoption);
		addAttribute(attrs, "mail", mail);
		addAttribute(attrs, "uid", uid);
// TODO: This seems to be encryptedese for "password"; can we pass plain text also?
//user edwin on ldap server vizier.genient.com: {SHA}W6ph5Mm5Pz8GgiULbPgzG37mj9g=
//user fabio on ldap server bsunny.genient.com: {SHA}W6ph5Mm5Pz8GgiULbPgzG37mj9g=
		upw="{SHA}W6ph5Mm5Pz8GgiULbPgzG37mj9g=";
		addAttribute(attrs, "userpassword", upw);
// TODO: Not sure if these are needed at creation time.
		attr = new BasicAttribute("nswmextendeduserprefs");
		String nswmextendeduserprefs_values[] = {"meDraftFolder=Drafts",
		"meSentFolder=Sent", "meTrashFolder=Trash", "meInitialized=true"};
		for (int i = 0; i < nswmextendeduserprefs_values.length; i++) attr.add(nswmextendeduserprefs_values);
		attrs.put(attr);
/* Create an entry with this DN and these attributes . */
		Hashtable dirEnv = new Hashtable();
		dirEnv.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		dirEnv.put(Context.PROVIDER_URL, "ldap://" + m_srv + ":" + m_port);
		dirEnv.put(Context.SECURITY_PRINCIPAL, m_su);
		dirEnv.put(Context.SECURITY_CREDENTIALS, m_pw);
		dirEnv.put(Context.SECURITY_AUTHENTICATION, "simple");
		InitialLdapContext ldapContext = null;
		try
		{
			ldapContext = new InitialLdapContext(dirEnv, new Control[0]);
			ldapContext.rebind(dn, null, attrs);
		}
		catch (NamingException ex)
		{
			throw new WebmailException(ex);
		}
		finally
		{
			if (ldapContext != null)
			try
			{
				ldapContext.close();
			}
			catch (NamingException ex) {}
		}
		return true;
	}

}