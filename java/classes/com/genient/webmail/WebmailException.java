/*************************************************************************
** CONFIDENTIAL AND PROPRIETARY SOURCE CODE OF GENIENT LIMITED.
**
** Copyright (c) 2000 Genient Limited.
** All Rights Reserved.
**
** Use of this Source Code is subject to the terms of the applicable
** license agreement from Genient Limited.  The copyright
** notice(s) in this Source Code does not indicate actual or intended
** publication of this Source Code.
**
** @(#)WebmailException.java			1.0					2000/07/25
**
**************************************************************************/
package com.genient.webmail;

public class WebmailException extends Exception {

	private Exception innerException;

	public WebmailException(String s) {

		super(s);

	}

	public WebmailException(Exception e) {

		fillInStackTrace();
		innerException = e;

	}

	public Exception getInnerException() {

		return innerException;

	}

}