
package com.wapit;

import java.awt.*;
import java.io.*;
import java.net.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Wapit Java Exercise 1 solved following instructions in file
wapit_java_excercise1.doc (refers to exorcism?)
@author Joonas Keturi */
public class Exercise1 extends HttpServlet
{
	byte bitmap[];

/** Writes long integer in multi byte integer format
as specified in WAP Wireless Application Environment Specicification Version 1.1
6.3.1 Multi-byte Integer format */
void writeMultiByte(OutputStream out, long value) throws IOException
{
	if (value == 0L)
	{
		out.write(0);
		return;
	}
	byte bytes[] = new byte[5];
	int i = 0;
	do bytes[i++] = (byte)(value & 0x7f);
	while ((value >>= 7) != 0L);
	while (i > 1) out.write(bytes[--i] | 0x80);
	out.write(bytes[0]);
}

public void init(ServletConfig config) throws ServletException
{
	super.init(config);
	InputStream in = ClassLoader.getSystemResourceAsStream("com/wapit/resources/data.txt");
	StreamTokenizer sTok = new StreamTokenizer(new BufferedReader(new InputStreamReader(in)));
	sTok.resetSyntax();
	sTok.whitespaceChars(0, 32);
	sTok.wordChars(33, 255);
	sTok.ordinaryChar(',');
	sTok.ordinaryChar(';');
	sTok.parseNumbers();
	int maxX = Integer.MIN_VALUE, maxY = Integer.MIN_VALUE;
	Vector points = new Vector();
	try
	{
		while (sTok.nextToken() != sTok.TT_EOF)
		{
			if (sTok.ttype == sTok.TT_NUMBER)
			{
				int x = (int)sTok.nval;
				if (sTok.nextToken() == ',')
				{
					if (sTok.nextToken() == sTok.TT_NUMBER)
					{
						int y = (int)sTok.nval;
						maxX = Math.max(maxX, x);
						maxY = Math.max(maxY, y);
						points.addElement(new Point(x, y));
						if (sTok.nextToken() == ';') continue;
						if (sTok.ttype == sTok.TT_EOF) break;
					}
				}
			}
			throw new ServletException("Unexpected token in line " + sTok.lineno() + " in file data.txt");
		}
		int width = maxX + 1, height = maxY + 1;
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
// WBMP header as specified in WAP Wireless Application Environment Specification Version 1.1 Appendix A
// TypeField
		bout.write(0x00);
// FixHeaderField
		bout.write(0x00);
// Width
		writeMultiByte(bout, width);
// Height
		writeMultiByte(bout, height);
		int headerSize = bout.size(), length = (width + 7) / 8;
		byte bytes[] = bout.toByteArray();
		bitmap = new byte[headerSize + height * length];
		System.arraycopy(bytes, 0, bitmap, 0, headerSize);
		Enumeration enum = points.elements();
		while (enum.hasMoreElements())
		{
			Point point = (Point)enum.nextElement();
			bitmap[headerSize + point.y * length + point.x / 8] |= 1 << 7 - point.x % 8;
		}
	}
	catch (IOException ex)
	{
		throw new ServletException(ex.toString());
	}
}

public void destroy()
{
	super.destroy();
}

public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
{
	res.setStatus(HttpURLConnection.HTTP_OK);
	res.setContentType("image/vnd.wap.wbmp; level=0");
	res.setContentLength(bitmap.length);
	OutputStream out = res.getOutputStream();
	out.write(bitmap);
	out.close();
}

}