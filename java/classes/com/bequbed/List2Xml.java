
package com.bequbed;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class List2Xml extends HttpServlet
{
	String targetDirectory;
	Vector tagNames = new Vector();

public void init(ServletConfig config) throws ServletException
{
	super.init(config);
	targetDirectory = config.getInitParameter("targetDirectory");
	if (targetDirectory == null) throw new ServletException("'targetDirectory' is not specified");
	String s = config.getInitParameter("tagNames");
	if (s == null) throw new ServletException("'tagNames' is not specified");
	StringTokenizer st = new StringTokenizer(s, ",");
	while (st.hasMoreTokens()) tagNames.addElement(st.nextToken().trim());
}

public void destroy()
{
	super.destroy();
}

public void writeStartTag(BufferedWriter writer, String tagName) throws IOException
{
	writer.write("<" + tagName + ">");
	writer.newLine();
}

public void writeEndTag(BufferedWriter writer, String tagName) throws IOException
{
	writer.write("</" + tagName + ">");
	writer.newLine();
}

public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
{
	URL sourceUrl = new URL(req.getHeader("X-SourceURL"));
	String fileName = sourceUrl.getFile();
	fileName = fileName.substring(fileName.lastIndexOf('/') + 1);
	int i = fileName.lastIndexOf('.');
	if (i != -1) fileName = fileName.substring(0, i);
	BufferedReader reader = req.getReader();
	BufferedWriter writer = new BufferedWriter(new FileWriter(new File(targetDirectory, fileName + ".xml")));
	try
	{
		writeStartTag(writer, (String)tagNames.firstElement());
		String line;
		while ((line = reader.readLine()) != null)
		{
			writeStartTag(writer, (String)tagNames.elementAt(1));
			StringTokenizer st = new StringTokenizer(line, ",");
			for (i = 2; st.hasMoreTokens(); i++)
			{
				writeStartTag(writer, (String)tagNames.elementAt(i));
				Support.writeXmlString(writer, st.nextToken().trim());
				writeEndTag(writer, (String)tagNames.elementAt(i));
			}
			writeEndTag(writer, (String)tagNames.elementAt(1));
		}
		writeEndTag(writer, (String)tagNames.firstElement());
	}
	finally
	{
		writer.close();
	}
	res.setStatus(HttpURLConnection.HTTP_NO_CONTENT);
}

}