
import java.util.*;
import javax.ejb.*;
import java.sql.*;
import javax.naming.*;

public class ILanguaEJB implements SessionBean
{
	private SessionContext context;

public void setSessionContext(SessionContext context)
{
	this.context = context;
}

public void ejbRemove()
{
}

public void ejbActivate()
{
}

public void ejbPassivate()
{
}

public void ejbCreate(String username, String password)
{
}

public Enumeration getToDoList()
{
}

public Enumeration getJobList()
{
}

public void makeJob(Job job)
{
}

public Enumeration getReferralList()
{
}

public void makeReferral(Referral referral)
{
}

public void getDetails(Details details)
{
}

public void saveDetails(Details details)
{
}

public Enumeration getContactList()
{
}

}