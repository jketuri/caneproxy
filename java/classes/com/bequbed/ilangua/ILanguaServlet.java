
package com.bequbed.ilangua;

import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;
import javax.naming.*;
import javax.rmi.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ILanguaServlet extends HttpServlet
{

class ILanguaSession implements HttpSessionBindingListener
{
	ILangua iLangua;

public void valueBound(HttpSessionBindingEvent event)
{
}

public void valueUnbound(HttpSessionBindingEvent event)
{
}

}

public void init(ServletConfig config) throws ServletException
{
	super.init(config);
}

public void destroy()
{
	super.destroy();
}

public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
{
	String tab = req.getParameter("tab");
	if (tab == null) tab = "";
	tab = tab.trim().toLowerCase();
	String credentials = req.getHeader("authorization");
	if (credentials == null)
	{
		res.setStatus(res.SC_UNAUTHORIZED);
		return;
	}
	StringTokenizer st = new StringTokenizer(credentials);
	if (!st.hasMoreTokens() || !st.nextToken().equalsIgnoreCase("basic") || !st.hasMoreTokens())
	{
		res.setStatus(res.SC_UNAUTHORIZED);
		return;
	}
	st = new StringTokenizer(new String(new BASE64Decoder().decodeBuffer(st.nextToken())), ":");
	if (!st.hasMoreTokens())
	{
		res.setStatus(res.SC_UNAUTHORIZED);
		return;
	}
	String username = st.nextToken();
	HttpSession httpSession = req.getSession(false);
	if (httpSession == null)
	{
		httpSession = req.getSession(true);
		password = st.hasMoreTokens() ? st.nextToken() : "";
		Context initial = new InitialContext();
		Object objref = initial.lookup("iLangua");
		ILanguaHome home = (JobHome)PortableRemoteObject.narrow(objref, ILanguaHome.class);
		ILangua iLangua = home.create(username, password);
		ILanguaSession iLanguaSession = new ILanguaSession();
		iLanguaSession.iLangua = iLangua;
		httpSession.setAttribute(getClass().getName(), iLanguaSession);
	}
	PrintWriter writer = res.getOutputStream();
	writer.println("<?xml version=\"1.0\"?>");
	if (tab.equals("jobq"))
	{
	}
}

}
