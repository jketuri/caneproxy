
import FI.realitymodeler.common.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.text.*;
import java.util.*;

class StringComparator implements Comparator
{

public int compare(Object o1, Object o2)
{
	return ((String)o1).compareTo((String)o2);
}

}

class ButtonListener implements ActionListener
{
	boolean pressed = false;

public void actionPerformed(ActionEvent e)
{
	pressed = true;
	((Window)((Button)e.getSource()).getParent()).setVisible(false);
}

}

public class lm_install
{

static void copyFile(File fromFile, File toFile) throws IOException
{
	FileInputStream fin = null;
	FileOutputStream fout = null;
	try
	{
		fin = new FileInputStream(fromFile);
		fout = new FileOutputStream(toFile);
		byte b[] = new byte[4096];
		for (int n; (n = fin.read(b)) > 0;) fout.write(b, 0, n);
	}
	finally
	{
		try
		{
			if (fin != null) fin.close();
		}
		finally
		{
			if (fout != null) fout.close();
		}
	}
}

static void copyDir(File fromDir, File toDir, boolean recurse) throws IOException
{
	toDir.mkdir();
	String list[] = fromDir.list();
	for (int i = 0; i < list.length; i++)
	{
		File file = new File(fromDir, list[i]);
		if (file.isFile() && !file.isDirectory()) copyFile(new File(fromDir, list[i]), new File(toDir, list[i]));
		else if (file.isDirectory() && recurse)
		{
			File dir = new File(toDir, list[i]);
			dir.mkdir();
			copyDir(file, dir, true);
		}
	}
}

static boolean dialog(String title, String message, TextField field, boolean question)
{
	Dialog dialog = new Dialog(new Frame(), title);
	dialog.addWindowListener(new WindowAdapter()
	{
		public void windowClosing(WindowEvent e)
		{
			e.getWindow().setVisible(false);
		}
	});
	dialog.setLayout(new GridLayout(3 + (field != null ? 1 : 0), 1));
	TextArea textArea = new TextArea(message, 4, 80, TextArea.SCROLLBARS_NONE);
	textArea.setEditable(false);
	dialog.add(textArea);
	if (field != null) dialog.add(field);
	Button proceed = new Button(question ? "Yes" : "Proceed");
	proceed.addActionListener(new ButtonListener());
	dialog.add(proceed);
	Button cancel = new Button(question ? "No" : "Cancel");
	ButtonListener cancelListener = new ButtonListener();
	cancel.addActionListener(cancelListener);
	dialog.add(cancel);
	dialog.setSize(640, 200);
	dialog.setModal(true);
	dialog.setVisible(true);
	return cancelListener.pressed;
}

static boolean dialog(String title)
{
	return dialog(title, title, null, false);
}

static void exec(String cmd, boolean check) throws InterruptedException, IOException
{
	Process process = Runtime.getRuntime().exec(cmd);
	process.waitFor();
	if (!check || process.exitValue() == 0) return;
	dialog("Error " + process.exitValue());
	System.out.println("Command " + cmd + " returned error value " + process.exitValue());
	System.exit(process.exitValue());
}

public static void main(String argv[])
{
	try {
	Properties regValues = new Properties();
	if (File.separatorChar == '\\')
	{
		exec("lm_install.exe", true);
		FileInputStream fin = new FileInputStream("reg_values.properties");
		try
		{
			regValues.load(fin);
		}
		finally
		{
			fin.close();
		}
	}
	Properties properties = new Properties();
	FileInputStream fin = new FileInputStream("FI_realitymodeler_gps_LiveMap.properties");
	try
	{
		properties.load(fin);
	}
	finally
	{
		fin.close();
	}
	Dialog dialog = new Dialog(new Frame(), "LiveMap Settings");
	dialog.setModal(true);
	dialog.setLayout(new GridLayout(4, 2));
	dialog.add(new Label("Data directory"));
	String s = properties.getProperty("FI.realitymodeler.gps.LiveMap/dataDirectory");
	TextField dataDirectory = new TextField(s != null ? s : "");
	dialog.add(dataDirectory);
	dialog.add(new Label("Map file directory"));
	s = properties.getProperty("FI.realitymodeler.gps.LiveMap/mapFileDirectory");
	TextField mapFileDirectory = new TextField(s != null ? s : "");
	dialog.add(mapFileDirectory);
	dialog.add(new Label("Serial port"));
	Choice portName = new Choice();
	portName.setSize(portName.getPreferredSize());
	portName.add("COM1");
	portName.add("COM2");
	portName.add("COM3");
	portName.add("COM4");
	s = properties.getProperty("FI.realitymodeler.gps.LiveMap/portName");
	if (s != null) portName.select(s);
	dialog.add(portName);
	Button install = new Button("Install");
	install.setSize(install.getPreferredSize());
	dialog.add(install);
	Button cancel = new Button("Cancel");
	cancel.setSize(cancel.getPreferredSize());
	dialog.add(cancel);
	ButtonListener installListener = new ButtonListener();
	install.addActionListener(installListener);
	ButtonListener cancelListener = new ButtonListener();
	cancel.addActionListener(cancelListener);
	dialog.addWindowListener(new WindowAdapter()
	{
		public void windowClosing(WindowEvent e)
		{
			e.getWindow().setVisible(false);
		}
	});
	dialog.setSize(640, 240);
	do
	{
		String JavaHome = regValues.getProperty("Software\\JavaSoft\\Java Runtime Environment\\1.4_JavaHome");
		if (JavaHome == null)
		{
			if (dialog("Java Runtime Environment 1.4 is not installed or you have no administrator privileges")) break;
			continue;
		}
		dialog.setVisible(true);
		if (cancelListener.pressed) break;
		if (!installListener.pressed) break;
		properties.put("FI.realitymodeler.gps.LiveMap/dataDirectory", dataDirectory.getText());
		properties.put("FI.realitymodeler.gps.LiveMap/mapFileDirectory", mapFileDirectory.getText());
		properties.put("FI.realitymodeler.gps.LiveMap/portName", portName.getSelectedItem());
		new File(new File(dataDirectory.getText()).getCanonicalPath()).mkdirs();
		copyDir(new File("bin"), new File(JavaHome, "bin"), true);
		copyDir(new File("lib"), new File(JavaHome, "lib"), true);
		copyDir(new File("files"), new File(dataDirectory.getText()), false);
		FileOutputStream out = new FileOutputStream(new File(JavaHome, "lib/FI_realitymodeler_gps_LiveMap.properties"));
		try
		{
			properties.store(out, "LiveMap properties stored by lm_install");
		}
		finally
		{
			out.close();
		}
		break;
	}
	while (!dialog("Retry installation?"));
	System.exit(0);
	} catch (Throwable th)
	{
		th.printStackTrace();
		dialog("Error " + th.toString());
		System.exit(1);
	}
}

}
