
import FI.realitymodeler.p2p.*;
import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.rmi.*;
import java.util.*;
import javax.jms.*;

public class Conversation extends Applet implements ActionListener, ItemListener, Runnable
{
	Peer2Peer p2p = null;
	TopicConnection connection = null;
	TopicSession session = null;
	TopicPublisher publisher = null;
	TopicSubscriber subscriber = null;
	Panel buttons = new Panel();
	Button call = new Button("call"), channels = new Button("channels");
	TextField field = new TextField(100), title = new TextField(100);
	java.awt.List list = new java.awt.List(40, false);
	boolean listChans = false;
	Thread thread = null;

public void openConnection() throws JMSException
{
	connection = p2p.createTopicConnection();
	session = connection.createTopicSession(false, TopicSession.AUTO_ACKNOWLEDGE);
}

public void openChannel(String topicName) throws JMSException
{
	Topic topic = p2p.getTopic(topicName);
	if (topic == null) topic = session.createTopic(topicName);
	publisher = session.createPublisher(topic);
	subscriber = session.createSubscriber(topic);
}

public void init()
{
	try
	{
		p2p = Peer2Peer.getPeer2Peer(getDocumentBase().getHost());
	}
	catch (Exception ex)
	{
		ex.printStackTrace();
		getAppletContext().showStatus(ex.toString());
		destroy();
		return;
	}
	GridBagLayout gbl = new GridBagLayout();
	GridBagConstraints gbc = new GridBagConstraints();
	setLayout(gbl);
	gbl.setConstraints(field, gbc);
	add(field);
	field.addActionListener(this);
	Label l = new Label("message/peer(s)");
	gbc.gridy = 1;
	gbl.setConstraints(l, gbc);
	add(l);
	gbc.gridy = 2;
	gbl.setConstraints(title, gbc);
	add(title);
	title.addActionListener(this);
	l = new Label("title");
	gbc.gridy = 3;
	gbl.setConstraints(l, gbc);
	add(l);
	buttons.add(call);
	call.addActionListener(this);
	buttons.add(channels);
	channels.addActionListener(this);
	gbc.gridy = 4;
	gbl.setConstraints(buttons, gbc);
	add(buttons);
	gbc.gridy = 5;
	gbc.weightx = gbc.weighty = 1;
	gbc.fill = gbc.BOTH;
	gbl.setConstraints(list, gbc);
	add(list);
	list.addItemListener(this);
	try
	{
		openConnection();
		String ref = getDocumentBase().getRef();
		if (ref != null && !ref.equals(""))
		{
			openChannel(ref);
			title.setText(ref);
			(thread = new Thread(this)).start();
		}
		else dispatchEvent(new ActionEvent(channels, 0, channels.getLabel()));
	}
	catch (Exception ex)
	{
		ex.printStackTrace();
		destroy();
	}
}

public void closeChannel()
{
	try
	{
		try
		{
			if (publisher != null)
			{
				publisher.close();
				publisher = null;
			}
		}
		finally
		{
			if (subscriber != null)
			{
				subscriber.close();
				subscriber = null;
			}
		}
	}
	catch (JMSException ex) {}
}

public void closeConnection()
{
	try
	{
		try
		{
			if (session != null)
			{
				session.close();
				session = null;
			}
		}
		finally
		{
			if (connection != null)
			{
				connection.close();
				connection = null;
			}
		}
	}
	catch (JMSException ex) {}
}

public void destroy()
{
	try
	{
		closeChannel();
	}
	finally
	{
		closeConnection();
	}
}

public void run()
{
	if (listChans)
	{
		try
		{
			Topic topic = p2p.iterateTopics();
			for (;;) list.add((topic = p2p.iterateTopic(topic)).getTopicName());
		}
		catch (NoSuchElementException ex) {}
		catch (Exception ex)
		{
			ex.printStackTrace();
			getAppletContext().showStatus(ex.toString());
		}
		return;
	}
	for (;;)
	try
	{
		ObjectMessage message = (ObjectMessage)subscriber.receive();
		list.add(message.getJMSReplyTo() + ": " + message.getObject());
		if (list.getItemCount() > 1000) list.remove(0);
	}
	catch (Exception ex)
	{
		if (ex instanceof InterruptedException) break;
		ex.printStackTrace();
		getAppletContext().showStatus(ex.toString());
	}
}

public void actionPerformed(ActionEvent e)
{
	Object source = e.getSource();
	try
	{
		if (source == field)
		{
			if (publisher == null) return;
			char chs[] = new char[field.getText().length()];
			field.getText().getChars(0, field.getText().length(), chs, 0);
			ObjectMessage message = session.createObjectMessage(field.getText());
			publisher.publish(message);
			field.setText("");
		}
		else if (source == call)
		{
			Topic topic = p2p.getTopic(title.getText());
			if (topic == null) topic = session.createTopic(title.getText());
			QueueConnection queueConnection = p2p.createQueueConnection();
			try
			{
				QueueSession queueSession = queueConnection.createQueueSession(false, QueueSession.AUTO_ACKNOWLEDGE);
				try
				{
					StringTokenizer st = new StringTokenizer(field.getText());
					while (st.hasMoreTokens())
					{
						javax.jms.Queue queue = p2p.getQueue("Peer2PeerApplet/" + InetAddress.getByName(st.nextToken().trim()).getHostAddress());
						if (queue == null) continue;
						QueueSender sender = queueSession.createSender(queue);
						try
						{
							sender.send(session.createObjectMessage("/Conversation.html#" + topic.getTopicName()));
						}
						finally
						{
							sender.close();
						}
					}
				}
				finally
				{
					queueSession.close();
				}
			}
			finally
			{
				queueConnection.close();
			}
			field.setText("");
		}
		else if (source == channels)
		{
			list.removeAll();
			if (thread != null && thread.isAlive()) thread.interrupt();
			listChans = true;
			new Thread(this).start();
		}
		else if (source == title)
		{
			if (thread != null && thread.isAlive()) thread.interrupt();
			closeChannel();
			openChannel(title.getText());
			title.setText(title.getText());
			listChans = false;
			(thread = new Thread(this)).start();
		}
	}
	catch (Exception ex)
	{
		ex.printStackTrace();
		getAppletContext().showStatus(ex.toString());
	}
}

public void itemStateChanged(ItemEvent e)
{
	Object source = e.getSource();
	try
	{
		if (source == list)
		{
			closeChannel();
			String topicName = list.getItem(((Integer)e.getItem()).intValue());
			openChannel(topicName);
			title.setText(topicName);
		}
	}
	catch (Exception ex)
	{
		ex.printStackTrace();
		getAppletContext().showStatus(ex.toString());
	}
}

}
