/*
 * COPYRIGHT Nokia Telecommunications OY, 1998, All rights reserved.
 */

package namp2.remote;

import java.io.*;
import java.net.*;

import java.util.Hashtable;
import java.util.Date;

/**
 * COPYRIGHT Nokia Telecommunications OY, 1998, All rights reserved.
 * <p>
 * Namp2Connection is a connection between NAMP2 and external application.
 * Instances of this class can be used to 
 * <ol>
 *     <li>Convert Namp2Request to Namp2Response</li>
 *     <li>Push Namp2Request to NAMP2 is it would come from specified MSISDN client</li>
 * </ol>
 * <p>
 * In general, using connection is a four step
 * process:
 * <ol>
 *     <li>Create connection object -&gt; Namp2Connection n2c = new
 *         Namp2Connection(&quot;localhost&quot;, 5678);</li>
 *     <li>Start listening for Namp2Responses -&gt; n2c.listen();</li>
 *     <li>Use connection -&gt; Namp2Response res = n2c.send(namp2Request, 60000); or long id = n2c.submit(namp2Request);
 *     <li>close connection -&gt; n2c.close();
 * </ol>
 * If remote client is pure PUSH client and it don't need to receive responses
 * then connecting or closing is not needed:
 * <ol>
 *     <li>Create connection object -&gt; Namp2Connection n2c = new
 *         Namp2Connection(&quot;localhost&quot;, 5678);</li>
 *     <li>Use connection -&gt; long id = n2c.submit(namp2Request);
 * </ol>
 *
 * @author Teppo Koskinen
 * @version 0.5, 01/05/98
 */
public final class Namp2Connection implements Runnable {
    
    // Configuration parameters
    private int soTimeout = 10000; // 10 seconds

    // utilities
    private ServerSocket serverSocket;
    private Thread thread;
    private Hashtable storage;
    
    // NAMP2 Address
    private String namp2Host;
    private int namp2Port;

    // State information
    private boolean running;
    
    /**
     * Creates a socket connection between this client and 
     * specified NAMP2 INET subsystem.
     * 
     * @param   host    IP address or domain name for NAMP2 server
     * @param   port    NAMP2 server port number
     */
    public Namp2Connection(String host, int port) {
        namp2Host = host;
        namp2Port = port;
        
        storage = new Hashtable();
    }

    /**
     * Connect method establishes the communication channel and
     * starts listening responses.
     * 
     * @exception  IOException if an I/O error occurs when opening the socket.
     */
    public void listen() throws IOException {
        if (!running) {
            try {
                serverSocket = new ServerSocket(0);
            }
            catch(IOException ex) {
                throw ex;
            }

            running = true;
            thread = new Thread(this);
            thread.setDaemon(true);
            thread.start();
        }
    }

    /**
     * Close the connection
     * 
     * @exception  IOException if an I/O error occurs when closing the socket
     */
    public void close() throws IOException {
        if(running) {
            thread.stop();
            serverSocket.close();
        }
    }        

    /**
     * Main loop for receiving responses. Used only internally!
     */
    public void run() {

        Socket socket = null;

        while(running) {
            try {
                socket = serverSocket.accept();

                processConnection(socket);
                socket.close();
            }
            catch(IOException e) {
                socket = null;
            }
        }
        running = false;
    }

    /**
     * Process one response.
     * 
     * @param   socket  communication channel to NAMP2
     * @exception  IOException if socket cannot be read or written
     */
    private void processConnection(Socket socket) throws IOException {
        String status = "201 Created";

        socket.setSoTimeout(soTimeout);
        
        InputStream is = socket.getInputStream();
        OutputStream os = socket.getOutputStream();

        Namp2Response res = null;

        try {
            res = new Namp2Response(is);
        }
        catch(Namp2ConnectionException ex) {
            status = "400 Bad Request";
        }

        os.write(("HTTP/1.0 " + status + "\r\n\r\n").getBytes());

        if(res != null) {
            Semaphore sem;
            Long id = new Long(res.getId());
            
            Object o = storage.get(id);
            
            if(o != null && o instanceof Semaphore) {
                sem = (Semaphore) o;

                storage.put(id, res);
                sem.V();
            }
        }
    }
    
    /**
     * Process Namp2Request and return Namp2Response received from NAMP2. 
     * <p>
     * Request is sent to NAMP2 INET subsystem that will
     * process the request and send the resulting response back.
     *
     * @param   req     Filled in NAMP2 request
     * @param   rrTimeout how many milliseconds are waited for response
     * @return  Namp2Response Namp2Response created by NAMP2 as a result of 
     * processing the given request. Null if response was not received.
     * 
     * @exception  IOException if IO error occurs while communicating with NAMP2
     * @exception  Namp2ConnectionException if protocol used by NAMP2 is not 
     * supported by this component.
     */
    public Namp2Response send(Namp2Request req, int rrTimeout) throws IOException, Namp2ConnectionException {
        req.setAddr("http://" + InetAddress.getLocalHost().getHostAddress() + ":" + serverSocket.getLocalPort() + '/');
        Semaphore sem = new Semaphore(0, rrTimeout);

        Long id = new Long(submit(req));
        storage.put(id, sem);
        
        sem.P();

        Object res = storage.remove(id);

        if(res instanceof Semaphore) {
            return null;
        }

        return (Namp2Response) res;
    }
    
    /**
     * Sends single message to phone client. Doesn't wait for response.
     * 
     * @param   req     Filled in NAMP2 request
     * @return  long    Timestamp for first NAMP2 action log entry
     * @exception  IOException if cannot communicate with NAMP2
     * @exception  Namp2ConnectionException if connection is not connected or
     * NAMP2 doesn't support same protocol as this client.
     */
    public long submit(Namp2Request req) throws IOException, Namp2ConnectionException {
        Socket socket = new Socket(namp2Host, namp2Port);
        socket.setSoTimeout(soTimeout);
        
        InputStream is = socket.getInputStream();
        OutputStream os = socket.getOutputStream();

        req.send(os);
        os.flush();

        Namp2RequestAck ack = new Namp2RequestAck(is);
        
        socket.close();
        
        return ack.id;
    }
    
    /**
     * For debug purposes. Tests different modes of usage.
     * 
     * @param   args    Command line parameters
     */
    public static void main(String[] args) throws NumberFormatException, IOException, Namp2ConnectionException {
        
        String host = (args.length > 0) ? args[0]: "localhost";
        int port = (args.length > 1) ? Integer.parseInt(args[1]): 5678;
        int test = (args.length > 2) ? Integer.parseInt(args[2]): 1;
        String msisdn = (args.length > 3) ? args[3]: "44997034005";

        System.out.println("http://" + host + ":" + port + "/");

        Namp2Connection conn = new Namp2Connection(host, port);
        Namp2Request req = new Namp2Request();
            
        if(test == 1) {
            req.setKeyword("TEXTPUSH");
            req.setProtocol(Namp2Request.PROTOCOL_NBS);
            req.setContent("New mail".getBytes(), false);
            req.setAddr("msisdn://" + msisdn + "/");
        
            conn.submit(req);
        }
        else if(test == 2) {
            req.setKeyword("TEXTPUSH");
            req.setContent("New mail".getBytes(), false);
            req.setAddr("msisdn://" + msisdn + "/");
        
            conn.submit(req);
        }
        else if(test == 3) {
            req.setKeyword("TEXTPUSH");
            req.setProtocol(Namp2Request.PROTOCOL_NBS);
            req.setContent(
                ("Nokia Artus Messaging Platform 2 (NAMP2) " +
                "enables utilisation of existing internet " +
                "services in thin client environment. " +
                "NAMP2 provides means to filter internet " +
                "data to a form, which can be effectively " +
                "browsed by a mobile phone. " +
                "Means to perform content push to mobile clients. " +
                "Means to provide personalised services.").getBytes(), false);
            req.setAddr("msisdn://" + msisdn + "/");
        
            conn.submit(req);
        }
        else if(test == 4) {
            req.setKeyword("TEXTPUSH");
            req.setContent(
                ("Nokia Artus Messaging Platform 2 (NAMP2) " +
                "enables utilisation of existing internet " +
                "services in thin client environment. " +
                "NAMP2 provides means to filter internet " +
                "data to a form, which can be effectively " +
                "browsed by a mobile phone. " +
                "Means to perform content push to mobile clients. " +
                "Means to provide personalised services.").getBytes(), false);
            req.setAddr("msisdn://" + msisdn + "/");
        
            conn.submit(req);
        }
        else if(test == 5) {
            req.setKeyword("BINPUSH");
            FileInputStream fis = new FileInputStream(args[4]);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            for(int b = fis.read(); b != -1; b = fis.read()) {
                baos.write(b);
            }
            fis.close();
            req.setContent(baos.toByteArray(), true);
            req.setProtocol(Namp2Request.PROTOCOL_NBS);
            req.setAddr("msisdn://" + msisdn + "/");
        
            conn.submit(req);
        }
        else if(test == 6) {
            req.setKeyword("BINPUSH");
            FileInputStream fis = new FileInputStream(args[4]);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            for(int b = fis.read(); b != -1; b = fis.read()) {
                baos.write(b);
            }
            fis.close();
            req.setContent(baos.toByteArray(), true);
            req.setProtocol(Namp2Request.PROTOCOL_NBS);
            req.setAddr("msisdn://" + msisdn + "/");
        
            conn.submit(req);
        }
        else if(test == 7) {
            conn.listen();
            req.setKeyword("PHONES");
            req.setProtocol(Namp2Request.PROTOCOL_NBS);
        
            Namp2Response res = conn.send(req, 20000);
            if(res == null) {
                System.out.println("NO response");
            }
            else {
                System.out.println("Response");
                System.out.println("ID: " + res.getId());
                System.out.println("Expiration time: " + new java.util.Date(res.getValidityPeriod()).toString());
                System.out.println("NBS port: " + res.getNbsPort());

                Namp2Message[] msgs = res.getMessages();

                System.out.println("Number of fragments: " + msgs.length);

                System.out.println("Content type: TEXT");

                for(int i = 0; i < msgs.length; i++) {
                    System.out.print("Header: ");
                    System.out.write(msgs[i].getHeader());
                    System.out.println();

                    System.out.print("Content: ");
                    System.out.write(msgs[i].getData());
                    System.out.println();
                }
            }
        }
        else if(test == 8) {
            conn.listen();
            req.setKeyword("PHONES");
        
            Namp2Response res = conn.send(req, 20000);
            if(res == null) {
                System.out.println("NO response");
            }
            else {
                System.out.println("Response");
                System.out.println("ID: " + res.getId());
                System.out.println("Expiration time: " + new java.util.Date(res.getValidityPeriod()).toString());
                System.out.println("NBS port: " + res.getNbsPort());

                Namp2Message[] msgs = res.getMessages();

                System.out.println("Number of fragments: " + msgs.length);

                System.out.println("Content type: TEXT");

                for(int i = 0; i < msgs.length; i++) {
                    System.out.print("Header: ");
                    System.out.write(msgs[i].getHeader());
                    System.out.println();

                    System.out.print("Content: ");
                    System.out.write(msgs[i].getData());
                    System.out.println();
                }
            }
        }
        else if(test == 9) {
            conn.listen();
            req.setKeyword("PHONE");
            req.addParameter("2110", "");
            req.setProtocol(Namp2Request.PROTOCOL_NBS);
        
            Namp2Response res = conn.send(req, 20000);
            if(res == null) {
                System.out.println("NO response");
            }
            else {
                System.out.println("Response");
                System.out.println("ID: " + res.getId());
                System.out.println("Expiration time: " + new java.util.Date(res.getValidityPeriod()).toString());
                System.out.println("NBS port: " + res.getNbsPort());

                Namp2Message[] msgs = res.getMessages();

                System.out.println("Number of fragments: " + msgs.length);

                System.out.println("Content type: TEXT");

                for(int i = 0; i < msgs.length; i++) {
                    System.out.print("Header: ");
                    System.out.write(msgs[i].getHeader());
                    System.out.println();

                    System.out.print("Content: ");
                    System.out.write(msgs[i].getData());
                    System.out.println();
                }
            }
        }
        else if(test == 10) {
            conn.listen();
            req.setKeyword("PHONE");
            req.addParameter("2110", "");
        
            Namp2Response res = conn.send(req, 20000);
            if(res == null) {
                System.out.println("NO response");
            }
            else {
                System.out.println("Response");
                System.out.println("ID: " + res.getId());
                System.out.println("Expiration time: " + new java.util.Date(res.getValidityPeriod()).toString());
                System.out.println("NBS port: " + res.getNbsPort());

                Namp2Message[] msgs = res.getMessages();

                System.out.println("Number of fragments: " + msgs.length);

                System.out.println("Content type: TEXT");

                for(int i = 0; i < msgs.length; i++) {
                    System.out.print("Header: ");
                    System.out.write(msgs[i].getHeader());
                    System.out.println();

                    System.out.print("Content: ");
                    System.out.write(msgs[i].getData());
                    System.out.println();
                }
            }
        }
        else if(test == 11) {
            conn.listen();
            req.setKeyword("TONE");
            req.addParameter("axelf", "");
            req.setProtocol(Namp2Request.PROTOCOL_NBS);
        
            Namp2Response res = conn.send(req, 20000);
            if(res == null) {
                System.out.println("NO response");
            }
            else {
                System.out.println("Response");
                System.out.println("ID: " + res.getId());
                System.out.println("Expiration time: " + new java.util.Date(res.getValidityPeriod()).toString());
                System.out.println("NBS port: " + res.getNbsPort());

                Namp2Message[] msgs = res.getMessages();

                System.out.println("Number of fragments: " + msgs.length);

                System.out.println("Content type: TEXT");
                
                System.out.println("Data saved as axelf.ota");

                FileOutputStream fos = new FileOutputStream("axelf.ota");
                for(int i = 0; i < msgs.length; i++) {
                    fos.write(msgs[i].getData());
                }
                fos.close();
            }
        }
        else if(test == 12) {
            conn.listen();
            req.setKeyword("TONE");
            req.addParameter("imperial", "");
            req.setProtocol(Namp2Request.PROTOCOL_NBS);
        
            Namp2Response res = conn.send(req, 20000);
            if(res == null) {
                System.out.println("NO response");
            }
            else {
                System.out.println("Response");
                System.out.println("ID: " + res.getId());
                System.out.println("Expiration time: " + new java.util.Date(res.getValidityPeriod()).toString());
                System.out.println("NBS port: " + res.getNbsPort());

                Namp2Message[] msgs = res.getMessages();

                System.out.println("Number of fragments: " + msgs.length);

                System.out.println("Content type: TEXT");
                
                System.out.println("Data saved as axelf.ota");

                FileOutputStream fos = new FileOutputStream("axelf.ota");
                for(int i = 0; i < msgs.length; i++) {
                    fos.write(msgs[i].getData());
                }
                fos.close();
            }
        }
        else if(test == 13) {
            conn.listen();
            req.setContent(("//SCKL15CC " + 
                            "..menu\n" + 
                            ".<Select:\n" + 
                            "<>PHONES\n" + 
                            "<>*Something\n").getBytes(), false);

            Namp2Response res = conn.send(req, 20000);
            if(res == null) {
                System.out.println("NO response");
            }
            else {
                System.out.println("Response");
                System.out.println("ID: " + res.getId());
                System.out.println("Expiration time: " + new java.util.Date(res.getValidityPeriod()).toString());
                System.out.println("NBS port: " + res.getNbsPort());

                Namp2Message[] msgs = res.getMessages();

                System.out.println("Number of fragments: " + msgs.length);

                System.out.println("Content type: TEXT");

                for(int i = 0; i < msgs.length; i++) {
                    System.out.print("Header: ");
                    System.out.write(msgs[i].getHeader());
                    System.out.println();

                    System.out.print("Content: ");
                    System.out.write(msgs[i].getData());
                    System.out.println();
                }
                System.out.println();
            }
        }
        else if(test == 14) {
            req.setKeyword("TEXTPUSH");
            req.setProtocol(Namp2Request.PROTOCOL_NBS);
            req.setContent("New mail".getBytes(), false);
            req.setAddr("msisdn://" + msisdn + "");
        
            conn.submit(req);
        }
        else if(test == 15) {
            conn.listen();
            for(int i = 0; i < 3; i++) {
                new Tester(conn).start();
            }
        }
    }
}    

class Tester extends Thread {
    Namp2Connection conn;
    
    Tester(Namp2Connection c) {
        conn = c;
    }
    
    static int testerN = 0;
    private static synchronized int getN() {
        return testerN++;
    }
        
    public void run() {
        long startTime = new Date().getTime();
        Namp2Request req = new Namp2Request();
        req.setKeyword("LOOPBACK");
        req.setProtocol(Namp2Request.PROTOCOL_NBS);

        try {
            while(new Date().getTime() < startTime + 36000000) {
                String in;
                req.setContent((in = String.valueOf(getN())).getBytes(), false);
                Namp2Response res = conn.send(req, 20000);
                
                if(res == null) {
                    System.out.println("timeout: ");
                }
                else {
                    String out = new String(res.getMessages()[0].getData());
                    System.out.println(in + "=" + out);
                }
            }
        }
        catch(IOException ex) {
            System.out.println(ex.toString());
        }
        catch(Namp2ConnectionException ex) {
            System.out.println(ex.toString());
        }
    }
}

