/*
 * COPYRIGHT Nokia Telecommunications OY, 1998, All rights reserved.
 */

package namp2.remote;

import java.io.*;
import java.util.*;
import java.text.*;

/**
 * COPYRIGHT Nokia Telecommunications OY, 1998, All rights reserved.
 * <p>
 * Data carrier object created by reading Request ACK information 
 * from InputStream.
 *
 * @author Teppo Koskinen
 * @version 1.0, 27/07/98
 */
public class Namp2RequestAck {
    // Request ID (Timestamp of the first action log entry
    long id;

    /**
     * Reads the Request acknowledgement from given InputStream.
     * 
     * @param   is      InputStream for reading the acknowledgement
     * @exception  IOException if IO error occurs while communicating with NAMP2
     * @exception  Namp2ConnectionException if protocol used by NAMP2 is not 
     * supported by this component.
     */
    public Namp2RequestAck(InputStream is) throws IOException, Namp2ConnectionException {
        HttpInputStream http = new HttpInputStream(is);
        
        byte[] buf = new byte[256];

        int len = http.readLine(buf);
        String line = new String(buf, 0, len);
        
//        System.out.println("Status-Line: " + line);
        
        StringTokenizer st = new StringTokenizer(line, " ");
        if(!st.nextToken().equals("HTTP/1.0")) {
            throw new Namp2ConnectionException("HTTP version not supported");
        }
        
        if(!st.nextToken().equals("201")) {
            throw new Namp2ConnectionException("NAMP2 was unable to process request");
        }
        
        while((len = http.readLine(buf)) > 0) { // skip all other headers
//            System.out.println(new String(buf,0,len));
        }
        
        len = http.readLine(buf);
//        System.out.println("Identifier: " + new String(buf, 0, len));

        try {
            id = Long.parseLong(new String(buf, 0, len));
        }
        catch(NumberFormatException ex) {
            throw new Namp2ConnectionException("Unsupported identifier format");
        }
    }
}