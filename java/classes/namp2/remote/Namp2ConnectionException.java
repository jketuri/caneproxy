/*
 * COPYRIGHT Nokia Telecommunications OY, 1998, All rights reserved.
 */

package namp2.remote;

/**
 * COPYRIGHT Nokia Telecommunications OY, 1998, All rights reserved.
 * <p>
 * Signals that the protocol between Namp2Connection and 
 * NAMP2 INET is failed.
 *
 * @author Teppo Koskinen
 * @version 1.0, 27/07/98
 */
public class Namp2ConnectionException extends Exception {
    /**
     * Constructs an Namp2ConnectionException with no specified detail message.
     */
    public Namp2ConnectionException() {
        super();
    }

    /**
     * Constructs an Namp2ConnectionException with specified detail message.
     */
    public Namp2ConnectionException(String s) {
        super(s);
    }
}
