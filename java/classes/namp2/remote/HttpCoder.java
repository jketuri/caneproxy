/*
 * COPYRIGHT Nokia Telecommunications OY, 1998, All rights reserved.
 */

package namp2.remote;

/**
 * COPYRIGHT Nokia Telecommunications OY, 1998, All rights reserved.
 * <p>
 * Does simple URL encoding and decoding.
 *
 * @author Teppo Koskinen
 * @version 1.0, 27/07/98
 */
public class HttpCoder {

    private static final char[] hex = {
        '0', '1', '2', '3', 
        '4', '5', '6', '7',
        '8', '9', 'A', 'B',
        'C', 'D', 'E', 'F'};
    
    /**
     * Converts byte array to String. SP is converted to '+' -sign 
     * and all non alphanumeric characters are converted into a %HH 
     * sequence, where H stands for hex number.
     * 
     * @param   b       source byte sequence
     * @return  String Resulting character string
     */
    public static String encode(byte[] b) {
        if(b == null) {
            return null;
        }
        if(b.length == 0) {
            return new String();
        }
        
        StringBuffer sb = new StringBuffer(3 * b.length);
        for(int i = 0; i < b.length; i++) {
            byte c = b[i];
            
            if((c > 47 && c < 58) || (c > 64 && c < 91) || (c > 96 && c < 123)) {
                sb.append((char) c);
            }
            else if(c == 32) {
                sb.append('+');
            }
            else {
                sb.append('%');
                sb.append(hex[c / 16]);
                sb.append(hex[c % 16]);
            }
        }
        return sb.toString();
    }
    
    /**
     * Converts String to a byte array. '+' -sign is converted to SP and
     * all %HH sequences are replaced with a binary octet.
     * 
     * @param   src    Source content
     * @return  byte[] Resulting byte array
     */
    public static byte[] decode(String src) {
        if(src == null) {
            return null;
        }
        if(src.length() == 0) {
            return new byte[0];
        }
        
        byte[] b = new byte[src.length()];
        int dst = 0;

        for(int i=0; i < src.length();) {
            char c = src.charAt(i++);
            
            if(c == '+') {
                b[dst++] = 32;
            }
            else if (c == '%') {
                b[dst++] = (byte) ((16 * unhex((byte) src.charAt(i++))) + unhex((byte) src.charAt(i++)));
            }
            else {
                b[dst++] = (byte) c;
            }
        }
        
        byte[] ret = new byte[dst];
        System.arraycopy(b, 0, ret, 0, dst);
        return ret;
    }
    
    /**
     * Internal tool for converting single hex digit into a decimal number.
     * 
     * @param   b       ascii value of hex number
     * @return  int value of hex number (0-15)
     */
    private static int unhex(byte b) {
        if (b >= 48 && b <= 57) {
            return b-48;
        }
        if(b >= 65 && b <= 70) {
            return b-65;
        }
        if(b >= 97 && b <= 102) {
            return b-97;
        }
        
        return 0;
    }
}

