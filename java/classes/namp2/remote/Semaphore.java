/*
 * COPYRIGHT Nokia Telecommunications OY, 1998, All rights reserved.
 */

package namp2.remote;

import java.util.Date;

/**
 *
 * COPYRIGHT Nokia Telecommunications OY, 1998, All rights reserved.
 * <p>
 * Standard semaphore implementation. Semaphore is a synchronization
 * component that is useful for solving consumer / producer
 * problems.
 * <p>
 * Semaphore has a intial value given in constructor. After that threads
 * may call semaphores V() or P() methods. 
 * <p>
 * When P is called then value 
 * is checked. If value is zero then thread is put to sleep. If value
 * is more than zero then value is decremented by one and thread may
 * continue execution.
 * <p>
 * When V is called then number of threads put to sleep is checked.
 * If number is zero then value is incremented. If number is more than
 * zero then one thread is awaken and it is allowed to continue execution.
 * 
 * @author Teppo Koskinen
 * @version 0.5, 01/01/98
 */

public class Semaphore {
    /**
     * Semaphore value. How many P operations are allowed if no
     * V operations are called.
     */
    private int value;
    
    /**
     * Debugging purposes
     */
    private int timeout;
    
    /**
     * Creates new Semaphore with initial value 0
     */
    public Semaphore() {
        this.value = 0;
        this.timeout = 0;
    }
    
    /**
     * Creates new Semaphore with specified initial value.
     * 
     * @param   initialValue Initial value
     */
    public Semaphore(int initialValue) {
        this.value = initialValue;
        this.timeout = 0;
    }
    
    /**
     * Only for debugging purposes
     * 
     * @param   initialValue Initial value
     * @param   timeout Number of milliseconds threads may be blocked in P()
     */
    public Semaphore(int initialValue, int timeout) {
        this.value = initialValue;
        this.timeout = timeout;
    }
    
    /**
     * Awakens one sleeping thread or increments value.
     */
    public synchronized void V() {
        value++;
        notify();
    }
    
    /**
     * Decrements value or puts thread to sleep.
     * 
     */
    public synchronized void P() {
        while(value == 0) {
            try { 
                if(timeout == 0) {
                    wait();
                }
                else {
                    wait(timeout);
                    return;
                }
            }
            catch(InterruptedException ex) {
            }
        }
        value--;
    }
}
