package namp2.remote;

import java.io.*;
import java.net.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

public class TestServlet extends HttpServlet {

    Namp2Connection conn;

    public void init(ServletConfig config) throws ServletException {
        try {
            conn = new Namp2Connection("localhost", 5678);
            conn.listen();
        }
        catch(IOException ex) {
        }
    }

    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

        res.setContentType("text/html");

        ServletOutputStream out = res.getOutputStream();

        out.println("<html>\n");
        out.println("  <body>\n");
        out.println("    <form method=post>\n");
        out.println("      <p><textarea name=content rows=4 cols=53></textarea></p>\n");
        out.println("      <p>MSISDN: <input type=text size=30 name=msisdn></p>\n");
        out.println("      <p><input type=submit></p>\n");
        out.println("    </form>\n");
        out.println("  </body>\n");
        out.println("</html>\n");

        out.close();
    }

    public void doPost(HttpServletRequest httpreq, HttpServletResponse httpres) throws ServletException, IOException {
        try {
            Enumeration e = httpreq.getParameterNames();
            while(e.hasMoreElements()) {
                String name = (String) e.nextElement();
                String value = httpreq.getParameter(name);
                System.out.println(name + "=" + value);
            }

            httpres.setContentType("text/html");
            ServletOutputStream out = httpres.getOutputStream();

            String content = httpreq.getParameter("content");
            String msisdn = httpreq.getParameter("msisdn");
            if(msisdn == null) {
                msisdn = "";
            }

            if(content == null) {
                out.println("<html>\n");
                out.println("  <body>\n");
                out.println("    <form method=post>\n");
                out.println("      <p><textarea name=content rows=4 cols=53></textarea></p>\n");
                out.println("      <p>MSISDN: <input type=text size=30 name=msisdn value="+msisdn+"></p>\n");
                out.println("      <p><input type=submit></p>\n");
                out.println("    </form>\n");
                out.println("  </body>\n");
                out.println("</html>\n");
            }
            else {
                Namp2Request req = new Namp2Request();
                req.setContent(content.getBytes(), false);

                if(msisdn.length() > 0) {
                    if(msisdn.charAt(0) == '+') {
                        msisdn = msisdn.substring(1);
                    }
                    req.setAddr("msisdn://" + msisdn + "/");
                    long id = conn.submit(req);

                    out.println("<html>\n");
                    out.println("  <body>\n");
                    out.println("    <form method=post>\n");
                    out.println("      <p><textarea name=content rows=4 cols=53>" + content + "</textarea></p>\n");
                    out.println("      <p>MSISDN: <input type=text size=30 name=msisdn value="+msisdn+"></p>\n");
                    out.println("      <p><input type=submit></p>\n");
                    out.println("    </form>\n");

                    out.println("<h2>Response</h2>");
                    out.println("<table border=3>");
                    out.println("<tr><td>Identifier</td><td>" + id + "</td></tr>");
                    out.println("</table>");
                }
                else {
                    Namp2Response res = conn.send(req, 20000);

                    Namp2Message[] msgs = res.getMessages();

                    StringBuffer tmp = new StringBuffer();
                    for(int i = 0; i < msgs.length; i++) {
                        tmp.append(new String(msgs[i].getData()));
                    }

                    out.println("<html>\n");
                    out.println("  <body>\n");
                    out.println("    <form method=post>\n");
                    out.println("      <p><textarea name=content rows=4 cols=53>" + tmp.toString() + "</textarea></p>\n");
                    out.println("      <p>MSISDN: <input type=text size=30 name=msisdn value="+msisdn+"></p>\n");
                    out.println("      <p><input type=submit></p>\n");
                    out.println("    </form>\n");

                    if(res == null) {
                        out.println("<h2>NO RESPONSE</h2>");
                    }
                    else {
                        out.println("<h2>Response</h2>");
                        out.println("<table border=3>");
                        out.println("<tr><td>Identifier</td><td>" + res.getId() + "</td></tr>");
                        out.println("<tr><td>Expiration time</td><td>" + new java.util.Date(res.getValidityPeriod()).toString() + "</td></tr>");
                        out.println("<tr><td>NBS port</td><td>" + res.getNbsPort() + "</td></tr>");
                        out.println("<tr><td>Number of fragments</td><td>" + msgs.length + "</td></tr>");
                        out.println("<tr><td>Content type</td><td>TEXT</td></tr>");
                        for(int i = 0; i < msgs.length; i++) {
                            out.println("<tr><td>Header[" + i + "]</td><td>");
                            out.println(new String(msgs[i].getHeader()) + "</td></tr>");

                            StringBuffer body = new StringBuffer();
                            byte[] src = msgs[i].getData();
                            for(int j = 0; j < src.length; j++) {
                                body.append("&#");
                                body.append((int) src[j]);
                                body.append(';');
                            }

                            out.println("<tr><td>Content[" + i + "]</td><td>");
                            out.println(body.toString() + "</td></tr>");
                        }
                        out.println("</table>");
                    }
                    out.println("  </body>");
                    out.println("</html>");
                }
            }
            out.close();
        }
        catch(Namp2ConnectionException ex) {
        }
    }
}
