/*
 * COPYRIGHT Nokia Telecommunications OY, 1998, All rights reserved.
 */

package namp2.remote;

/**
 * COPYRIGHT Nokia Telecommunications OY, 1998, All rights reserved.
 * <p>
 * Data carrier object for short message user-data and user-data-header.
 *
 * @author Teppo Koskinen
 * @version 1.0, 27/07/98
 */
public class Namp2Message {

    //----------------------------------------------------
    // Message data
    //----------------------------------------------------

    /**
     * short message user-data-header
     */
    private byte[] header;
    
    /**
     * short message user-data
     */
    private byte[] data;
    
    /**
     * Constructs new Namp2Message with specified User Data Header and 
     * User Data.
     * 
     * @param   header  NBS header (UDH)
     * @param   data    Short message body (UD)
     */
    Namp2Message(byte[] header, byte[] data) {
        this.header = header;
        this.data = data;
    }
    
    /**
     * Returns the byte array that is the user-data-header of short message.
     * 
     * @return  byte[] User data header
     */
    public byte[] getHeader() {
        return header;
    }

    /**
     * Returns the byte array that is the user-data of short message.
     * 
     * @return  byte[] User data 
     */
    public byte[] getData() {
        return data;
    }
}
