/*
 * COPYRIGHT Nokia Telecommunications OY, 1998, All rights reserved.
 */

package namp2.remote;

import java.io.*;

/**
 * COPYRIGHT Nokia Telecommunications OY, 1998, All rights reserved.
 * <p>
 * HttpInputStream allows to read a specified number of bytes from
 * the underlying InputStream or a sequence of bytes until CR, LF or CR+LF
 * is read.
 *
 * @author Teppo Koskinen
 * @version 1.0, 27/07/98
 */
public class HttpInputStream extends FilterInputStream {

    /**
     * Creates a new HttpInputStream to read data from the 
     * underlying InputStream.
     *
     * @param in the underlying input stream
     */
    public HttpInputStream(InputStream in) {
        super(in);
    }

    /**
     * Internal buffer to hold 1 byte pushback
     */
    private int buffer = -1;

    /**
     * Reads from this http input stream until all the bytes are read. 
     * The first buf.length bytes are stored to buf array.
     * Method blocks until the end of the 
     * stream is detected, or an exception is thrown.
     * @param buf the buffer into which the data is read. 
     * @return the number of bytes stored in buf
     * @throws EOFException if this input stream reaches the 
     * end before reading all the bytes. 
     * @throws IOException if an I/O error occurs.
     */
    public int readAll(byte[] buf) throws IOException {
        int len = 0;
        int b;

        b = (buffer == -1) ? in.read(): buffer;
        while(b != -1) {
            if(len < buf.length) {
                buf[len++] = (byte) b;
            }
            b = in.read();
        }
        return len;
    }
    
    /**
     * Reads specified number of bytes from the underlying InputStream 
     * into an array of bytes. Method blocks until input is available
     * or end of stream is reached.
     *
     * @param buf the buffer into which the data is read. 
     * @param maxLen number of bytes to read.
     * @return the number of bytes stored in buf
     * @throws IOException if an I/O error occurs.
     */
    public int readBytes(byte[] buf, int maxLen) throws IOException {
        int len = 0;
        int b;
        
        if(maxLen == 0) {
            return 0;
        }
        
        b = (buffer == -1) ? in.read(): buffer;
        while(b != -1 && len < maxLen) {
            buf[len++] = (byte) b;
            if(len == maxLen) break;
            b = in.read();
        }

        buffer = -1;
        return len;
    }

    /**
     * Reads next line from this HttpInputStream. 
     * This method successively reads bytes from the underlying 
     * input stream until it reaches the end of line or
     * end of input stream.
     * <p> 
     * A line of text is terminated by a carriage return 
     * character ('\r'), a newline character ('\n'), a carriage 
     * return character immediately followed by a newline 
     * character, or the end of the input stream. The 
     * line-terminating character(s), if any, are not returned 
     * as part of the string that is returned. 
     * <p>
     * This method blocks until a newline character is read, 
     * a carriage return and the byte following it are read 
     * (to see if it is a newline), the end of the stream is 
     * detected, or an exception is thrown. 
     *
     * @param buf the buffer into which the first buf.length bytes 
     * of data is stored. 
     * @return the number of bytes stored in buf
     * @throws IOException if an I/O error occurs.
     */
    public int readLine(byte[] buf) throws IOException {
        int len = 0;
        int b;

        b = (buffer == -1) ? in.read(): buffer;
        while(b != -1) {
            if(b == 13) {
                b = in.read();
                if(b == 10) {
                    buffer = -1;
                }
                else {
                    buffer = b;
                }
                return len;
            }
            if(b == 10) {
                buffer = -1;
                return len;
            }
            if(len < buf.length) {
                buf[len++] = (byte) b;
            }
            b = in.read();
        }

        return len;
    }
}    