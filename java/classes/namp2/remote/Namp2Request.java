/*
 * COPYRIGHT Nokia Telecommunications OY, 1998, All rights reserved.
 */

package namp2.remote;

import java.io.*;
import java.util.Vector;

/**
 * COPYRIGHT Nokia Telecommunications OY, 1998, All rights reserved.
 * <p>
 * Data carrier object used to convert request information
 * to http based protocol.
 *
 * @author Teppo Koskinen
 * @version 1.0, 27/07/98
 */
public class Namp2Request {
    
    //----------------------------------------------------
    // Constants
    //----------------------------------------------------

    /**
     * Specifies that the phone client is capable of processing NBS -headers
     */
    public static final int PROTOCOL_NBS       = 1;

    /**
     * Specifies that the phone client is not capable of processing NBS -headers
     */
    public static final int PROTOCOL_PLAIN_SMS = 2;

    //----------------------------------------------------
    // Request data
    //----------------------------------------------------

    /**
     * Destination address can be MSISDN number in international format 
     * (without '+' -sign) or IP host as domain -name or IP address.
     * format is http://%d.%d.%d.%d:port/ or msisdn://cccoosssssss/
     * Trailing slash is required.
     */
    private String addr;

    /**
     * NAMP2 service keyword.
     * If keyword is not defined then service content is parsed by NAMP2 as it
     * were TTML uplink page.
     */
    private String keyword;

    /**
     * NAMP2 service parameter names.
     * If keyword is not defined then service content is parsed by NAMP2 as it
     * were TTML uplink page.
     */
    private Vector keys;

    /**
     * NAMP2 service parameter values.
     * If keyword is not defined then service content is parsed by NAMP2 as it
     * were TTML uplink page.
     */
    private Vector values;

    /**
     * Specifies whether the destination client supports NBS headers or not.
     * Can have values:
     * <pre><ul>
     * <li>namp2.remote.Namp2Request.PROTOCOL_NBS</li>
     * <li>namp2.remote.Namp2Request.PROTOCOL_PLAIN_SMS</li>
     * </ul></pre>
     * This field specifies if NAMP2 will add NBS headers to 
     * outgoing response.
     */
    private int protocol;

    /**
     * Request user-data. If keyword is not specified then NAMP2 will interpret content 
     * as uplink-ttml-page. If keyword is specified then NAMP2 will interpret content
     * as prefetched content.
     */
    private byte[] content;
    
    /**
     * Specifies if the content is binary octet stream or plain text.
     */
    private boolean binary;

    /**
     * Default constructor
     */
    public Namp2Request() {
        keys = new Vector();
        values = new Vector();
    }
    
    /**
     * Set request keyword.
     * 
     * @param   keyword NAMP2 service keyword
     */
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
    
    /**
     * Add new parameter (name/value -pair) to request. Client can use if
     * content is already parsed.
     * 
     * @param   key     parameter name may not be null
     * @param   value   parameter value may be null
     */
    public void addParameter(String key, String value) {
        keys.addElement(key);
        values.addElement(value);
    }
    
    /**
     * Set the client protocol. Possible values are
     * <ul>
     * <li>namp2.remote.Namp2Request.NBS</li>
     * <li>namp2.remote.Namp2Request.PLAIN_SMS (default)</li>
     * </ul>
     *
     * @param   protocol Specifies the protocol that client can use (NBS or PLAIN_SMS)
     * @return  false if protocol is not recognized 
     */
    public boolean setProtocol(int protocol) {
        switch (protocol) {
            case PROTOCOL_NBS:
            case PROTOCOL_PLAIN_SMS: {
                this.protocol = protocol;
                return true;
            }
        }
        return false;
    }
    
    /**
     * Set content. Used for two purposes:
     * <ul>
     * <li>External push systems can send predefined content.</li>
     * <li>Phone simulator can send unparsed content that INET uses to
     * extract keyword, parameters, protocol and language.</li>
     * </ul>
     * @param   content Predefined or unparsed content
     * @param   isBinary Specifies if content is eight bit octet stream or 7 bit plain text
     */
    public void setContent(byte[] content, boolean isBinary) {
        this.content = content;
        binary = isBinary;
    }

    /**
     * Set MSISDN. MSISDN specifies the client that receives the response 
     * produced by NAMP2. If MSISDN is not set then NAMP2 will send the 
     * response back to the IP address that sent the request.
     * 
     * @param   addr    Destination MSISDN in the form msisdn://358401234567/
     */
    public void setAddr(String addr) {
        this.addr = addr;
    }
    
    /**
     * Converts request to http based protocol.
     * 
     * @param   dos     Stream where request is written
     * @exception  IOException if IO error occurs while communicating with NAMP2
     * @exception  Namp2ConnectionException if protocol used by NAMP2 is not 
     * supported by this component.
     */
    void send(OutputStream os) throws IOException, Namp2ConnectionException {
        
        StringBuffer sb = new StringBuffer();
        
        sb.append("POST ");
        sb.append(addr);

        if(protocol == PROTOCOL_NBS) {
            sb.append("NBS/");
        }
        else {
            sb.append("PLAIN_SMS/");
        }
        
        if(keyword != null && keyword.length() > 0) {
            sb.append(keyword);
            
            if(keys != null && keys.size() > 0) {
                for(int i=0; i < keys.size(); i++) {
                    sb.append((i == 0) ? "?": "&");
                    String key = (String) keys.elementAt(i);

                    sb.append(HttpCoder.encode(key.getBytes()));
                    
                    String value = (String) values.elementAt(i);
                    if(value != null && value.length() > 0) {
                        sb.append("=");
                        sb.append(HttpCoder.encode(value.getBytes()));
                    }
                }
            }
        }
        
        sb.append(" HTTP/1.0\r\n");

        if(content != null && content.length > 0) {
            sb.append("Content-Type: ");
            if(binary) {
                sb.append("application/octet-stream\r\n");
            }
            else {
                sb.append("text/plain\r\n");
            }
            sb.append("Content-Length: ");
            sb.append(content.length);
            sb.append("\r\n");
        }
        
        sb.append("\r\n");
        
        os.write(sb.toString().getBytes());
        if(content != null && content.length > 0) {
            os.write(content);
        }
    }

    /**
     * For debug purposes
     */
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("Namp2Request: ");
        sb.append("addr: ");
        sb.append(addr);
        sb.append(" ; ");
        sb.append("keyword: ");
        sb.append(keyword);
        sb.append(" ; ");

        if(keys.size() > 0) {
            sb.append("parameters: ");

            for(int i = 0; i < keys.size(); i++) {
                if(i > 0) {
                    sb.append(", ");
                }                
                String key = (String) keys.elementAt(i);
                String value = (String) values.elementAt(i);
                sb.append(key);
                sb.append('=');
                sb.append(value);
            }
            sb.append(" ; ");
        }        
        
        sb.append("protocol: ");
        if(protocol == PROTOCOL_NBS) {
            sb.append("NBS");
        }
        else {
            sb.append("PLAIN_SMS");
        }
        sb.append(" ; ");
        
        sb.append("content type: ");
        if(binary) {
            sb.append("binary");
        }
        else {
            sb.append("text");
        }
        sb.append(" ; ");
        
        if(!binary) {
            sb.append("content: ");
            sb.append(content);
        }
        
        return sb.toString();
    }
}
