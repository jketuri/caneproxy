/*
        Copyright (C) 2002-2003  Luis Parravicini <luis@ktulu.com.ar>

        This program is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation; either version 2 of the License, or
        (at Your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program; if not, write to the Free Software
        Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/


package ar.com.ktulu.yenc;

/**
 * Thrown to indicate that a header had a missing parameter.
 *
 * @author Luis Parravicini <luis@ktulu.com.ar>
 */
public class MissingParameterException extends YEncException {
    static final long serialVersionUID = 0L;

    private String parameter;
    private String header;

    /**
     * Constructs a <code>MissingParameterException</code> with the
     * specified detail message. The others arguments are the name of
     * the missing parameter and the name of the header to which the
     * parameter belongs to.
     */
    public MissingParameterException(String parameter, String header,
                                     String s) {
        super(s);
        this.parameter = parameter;
        this.header = header;
    }

    /**
     * Constructs a <code>MissingParameterException</code> with a default
     * message. The exception message is constructed using the parameter
     * and header's name.
     *
     * @see #MissingParameterException(String, String, String)
     */
    public MissingParameterException(String parameter, String header) {
        this(parameter, header, parameter+" not specified in "+header);
    }

    /** Returns the missing parameter name. */
    public String getParameterName() {
        return parameter;
    }

    /** Returns the header to which the parameter belongs to. */
    public String getHeaderName() {
        return header;
    }

}
