
import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ServiceServlet extends HttpServlet
{

    public static boolean login(HttpServletRequest req, HttpServletResponse res) throws IOException
    {
        String login = null, s;
        if ((s = req.getHeader("cookie")) != null) {
            StringTokenizer st = new StringTokenizer(s, ";");
            while (st.hasMoreTokens()) {
                if ((s = st.nextToken().trim()).equals("")) continue;
                StringTokenizer st1 = new StringTokenizer(s, "=");
                if (st1.hasMoreTokens() &&
                    st1.nextToken().trim().equalsIgnoreCase("login") &&
                    st1.hasMoreTokens()) {
                    login = st1.nextToken().trim();
                    break;
                }
            }
        }
        else if ((login = req.getQueryString()) != null)
            res.setHeader("Set-Cookie", "login=" + login + "; path=/servlet/public/service");
        if (login != null) {
            s = new String(new BASE64Decoder().decodeStream(new String(new URLDecoder().decodeStream(login))));
            StringTokenizer st = new StringTokenizer(s, ":");
            String username, password;
            if (st.hasMoreTokens())
                {
                    username = st.nextToken();
                    password = st.hasMoreTokens() ? st.nextToken() : "";
                }
            else username = password = "";
            return true;
        }
        res.setContentType("text/html");
        res.setHeader("Pragma", "no-cache");
        ServletOutputStream out = res.getOutputStream();
        out.print("<html><head><title>Session Login</title></head><body>\n");
        out.print("<h1>Session Login</h1>\n");
        out.print("<applet codebase=\"/\" code=\"LoginApplet\" width=300 height=100>\n");
        out.print("<param name=\"location\" value=\"" + req.getRequestURI() + "\">\n");
        out.print("</applet></body></html>");
        return false;
    }

    public void service(HttpServletRequest req, HttpServletResponse res) throws IOException
    {
        if (!login(req, res)) return;
        ServletOutputStream out = res.getOutputStream();
        out.print("<html><head><title>Services</title></head><body>\n");
        File f = new File("services.txt");
        if (f.exists()) {
            String path = req.getServletPath();
            if (!path.endsWith("/")) path += "/";
            out.print("<h3>Available services</h3>\n");
            FileInputStream fin = new FileInputStream(f);
            String s;
            while ((s = Support.readLine(fin)) != null)
                if (!s.equals("")) out.print("<a href=\"" + path + s + "\">" + s + "</a><br>\n");
        }
        else out.print("<h3>No services available</h3>\n");
        out.print("</body></html>");
    }

}
