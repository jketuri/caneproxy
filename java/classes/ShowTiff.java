
import FI.realitymodeler.common.*;
import FI.realitymodeler.image.*;
import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.io.*;
import java.net.*;

class ShowTiffFrame extends Frame implements ActionListener
{
	BufferedImage image;
	TextField tf;

public ShowTiffFrame()
{
	setSize(512, 512);
	setTitle(getClass().getName());
	add("North", tf = new TextField(80));
	tf.addActionListener(this);
	addWindowListener(new WindowAdapter()
	{
		public void windowClosing(WindowEvent event)
		{
			System.exit(0);
		}
	}
	);
}

public void paint(Graphics g)
{
	if (image != null) ((Graphics2D)g).drawImage(image, null, 0, tf.getHeight());
}

public void actionPerformed(ActionEvent event)
{
	if (event.getSource() == tf)
	{
		image = ShowTiff.getImage(tf.getText(), this);
		repaint();
	}
}

}

public class ShowTiff extends Applet implements ActionListener
{
	BufferedImage image;
	TextField tf;

public static BufferedImage getImage(String name, Component comp) throws ImagingOpException
{
	try
	{
		TiffImage image = new TiffImage(name, comp.getWidth(), comp.getHeight());
		System.out.println(image.header.properties);
		return image;
	}
	catch (IOException ex)
	{
		throw new ImagingOpException(Support.stackTrace(ex));
	}
}

public void init()
{
	add("North", tf = new TextField(80));
	tf.addActionListener(this);
	String name = getParameter("NAME");
	if (name != null)
	try
	{
		String s = new URL(getDocumentBase(), name).toString();
		tf.setText(s);
		image = getImage(s, this);
		repaint();
	}
	catch (MalformedURLException ex)
	{
		ex.printStackTrace();
	}
}

public void paint(Graphics g)
{
	if (image != null) ((Graphics2D)g).drawImage(image, null, 0, tf.getHeight());
}

public void actionPerformed(ActionEvent event)
{
	if (event.getSource() == tf)
	{
		image = getImage(tf.getText(), this);
		repaint();
	}
}

public static void main(String argv[])
{
	ShowTiffFrame stf = new ShowTiffFrame();
	stf.show();
	if (argv.length > 0)
	{
		stf.tf.setText(argv[0]);
		stf.image = getImage(argv[0], stf);
		stf.repaint();
	}
}

}
