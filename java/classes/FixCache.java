
import FI.realitymodeler.*;
import FI.realitymodeler.common.*;
import java.io.*;
import java.util.*;

public class FixCache
{

    public static void main(String argv[])
        throws Exception
    {
        W3File file0 = new W3File(argv[0]);
        if (argv.length > 1) {
            file0.setOffline();
            file0.setReadOnly();
            System.exit(0);
        }
        BufferedInputStream in = new BufferedInputStream(new FileInputStream(file0));
        HeaderList headerList = new HeaderList(in);
        String contentLengthStr = headerList.getHeaderValue("content-length"),
            lastModifiedStr = headerList.getHeaderValue("last-modified");
        if (lastModifiedStr == null) throw new Exception("Cannot be fixed");
        Date lastModifiedDate = Support.parse(lastModifiedStr);
        if (lastModifiedDate == null) throw new Exception("Unknown date format " + lastModifiedStr);
        W3File file = new W3File(argv[0] + ".fixed");
        BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(file));
        Support.sendHeaderList(out, headerList);
        byte b[] = new byte[Support.bufferLength];
        for (int n; (n = in.read(b)) > 0;) out.write(b, 0, n);
        out.close();
        in.close();
        long lastModified = lastModifiedDate.getTime();
        file.setLastTimes(lastModified, lastModified);
        file.setOffline();
        file0.delete();
        file.renameTo(file0);
        file0.setReadOnly();
    }

}
