
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUF_LEN 4096
#define MIN(a,b) ((a) < (b) ? (a) : (b))

int main(int argn, char *argv[])
{
	char b[BUF_LEN];
	if (argn == 2)
	{
		FILE *t = fopen(argv[1], "wb");
		int argv1len = strlen(argv[1]);
		for (int fn =1;; fn++)
		{
			itoa(fn, strcpy(b, argv[1]) + argv1len, 10);
			FILE *f = fopen(b, "rb");
			if (!f) break;
			int n;
			while ((n = fread(b, 1, BUF_LEN, f)) > 0)
			if (fwrite(b, 1, n, t) != n)
			{
				puts("Error writing file");
				fclose(f);
				fclose(t);
				return 1;
			}
		}
		fclose(t);
		return 0;
	}
	if (argn < 3)
	{
		puts("Give filename, start and end position");
		return 1;
	}
	FILE *f = fopen(argv[1], "rb");
	if (!f)
	{
		puts("File not found");
		return 1;
	}
	long fl, o = atol(argv[2]), l, sl;
	fseek(f, 0, SEEK_END);
	fl = ftell(f);
	if (argn < 4) l = fl;
	else l = atol(argv[3]);
	sl = l;
	l -= o;
	fl -= o;
	int argv1len = strlen(argv[1]);
	fseek(f, o, SEEK_SET);
	for (int fn = 1;; fn++)
	{
		itoa(fn, strcpy(b, argv[1]) + argv1len, 10);
		FILE *t = fopen(b, "wb");
		if (!t)
		{
			fclose(f);
			puts("Couldn't open output file");
			return 1;
		}
		int n;
		while (l > 0 && (n = fread(b, 1, MIN(BUF_LEN, l), f)) > 0)
		if (n < 0 || fwrite(b, 1, n, t) != n)
		{
			puts("Error reading or writing file");
			fclose(t);
			fclose(f);
			return 1;
		}
		else l -= n;
		fclose(t);
		if (argn < 5) break;
		fl -= sl;
		if (fl <= 0) break;
		l = sl;
	}
	fclose(f);
	return 0;
}
