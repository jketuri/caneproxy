/**
 * This file was automatically generated.  Do not manually modify this file.
 *
 * Runtime vendor: SunSoft, Inc.
 * Runtime version: 1.0
 *
 * Visual vendor: SunSoft, Inc.
 * Visual version: 1.0
 */


import sunsoft.jws.visual.rt.awt.GBConstraints;
import sunsoft.jws.visual.rt.base.*;
import sunsoft.jws.visual.rt.shadow.*;
import sunsoft.jws.visual.rt.shadow.java.awt.*;
import sunsoft.jws.visual.rt.type.*;

import sunsoft.jws.visual.rt.base.Root;
import sunsoft.jws.visual.rt.shadow.java.awt.FrameShadow;
import sunsoft.jws.visual.rt.shadow.GBPanelShadow;
import sunsoft.jws.visual.rt.shadow.java.awt.ButtonShadow;
import sunsoft.jws.visual.rt.shadow.java.awt.LabelShadow;
import sunsoft.jws.visual.rt.shadow.TabbedFolderShadow;
import FI.ncscst.jws.OwnRadioButtonPanelShadow;
import sunsoft.jws.visual.rt.shadow.LabelBarShadow;
import sunsoft.jws.visual.rt.shadow.java.awt.TextFieldShadow;
import sunsoft.jws.visual.rt.shadow.java.awt.CheckboxShadow;
import sunsoft.jws.visual.rt.shadow.java.awt.ChoiceShadow;
import sunsoft.jws.visual.rt.shadow.java.awt.TextAreaShadow;
import sunsoft.jws.visual.rt.shadow.ColumnListShadow;
import sunsoft.jws.visual.rt.shadow.java.awt.MenuBarShadow;
import sunsoft.jws.visual.rt.shadow.java.awt.MenuShadow;
import sunsoft.jws.visual.rt.shadow.java.awt.MenuItemShadow;
import sunsoft.jws.visual.rt.shadow.java.awt.FileDialogShadow;

public class MetadataRoot extends Root {
  public CheckboxShadow Advanced;
  public ButtonShadow ApplyButton;
  public LabelBarShadow AuthorLabel;
  public CheckboxShadow Automatic;
  public LabelBarShadow BaseLabel;
  public ChoiceShadow BonusClass;
  public LabelShadow BonusClassLabel;
  public CheckboxShadow Both;
  public ButtonShadow BrowseButton;
  public GBPanelShadow ButtonPanel;
  public LabelShadow CDistributionDateLabel;
  public CheckboxShadow CPAlone;
  public CheckboxShadow CPandSP;
  public OwnRadioButtonPanelShadow CampaignDates;
  public GBPanelShadow CampaignDatesCard;
  public TextFieldShadow CampaignDistributionDate;
  public TextFieldShadow CampaignDistributionTime;
  public LabelShadow CampaignDistributionTimeLabel;
  public TextFieldShadow CampaignExpireDate;
  public LabelShadow CampaignExpireDateLabel;
  public TextFieldShadow CampaignExpireTime;
  public LabelShadow CampaignExpireTimeLabel;
  public TextFieldShadow CampaignPreviewDate;
  public LabelShadow CampaignPreviewDateLabel;
  public TextFieldShadow CampaignPreviewTime;
  public LabelShadow CampaignPreviewTimeLabel;
  public ChoiceShadow Category;
  public LabelShadow CategoryLabel;
  public MenuItemShadow ClearItem;
  public ColumnListShadow ContentFiles;
  public GBPanelShadow ContentLanguage;
  public LabelShadow ContentLanguageLabel;
  public ChoiceShadow ContentProvider;
  public LabelShadow ContentProviderLabel;
  public LabelShadow ContentUsage;
  public TextFieldShadow Copyright;
  public LabelShadow CopyrightLabel;
  public TextFieldShadow CopyrightsOwner;
  public LabelShadow CopyrightsOwnerLabel;
  public MenuItemShadow CreatePackageItem;
  public TextFieldShadow CreationDate;
  public LabelShadow CreationDateLabel;
  public LabelShadow Customisation;
  public MenuItemShadow DeliverPackageItem;
  public TextAreaShadow Description;
  public LabelShadow DescriptionLabel;
  public TextFieldShadow DistributionDate;
  public LabelShadow DistributionDateLabel;
  public TextFieldShadow DistributionTime;
  public LabelShadow DistributionTimeLabel;
  public TextFieldShadow DocumentBase;
  public LabelShadow DocumentBaseLabel;
  public TextFieldShadow EmailAddress;
  public LabelShadow EmailAddressLabel;
  public CheckboxShadow English;
  public MenuItemShadow ExitItem;
  public TextFieldShadow ExpireDate;
  public LabelShadow ExpireDateLabel;
  public TextFieldShadow ExpireTime;
  public LabelShadow ExpireTimeLabel;
  public MenuItemShadow ExtractFilesItem;
  public TextFieldShadow FileDescription;
  public LabelShadow FileDescriptionLabel;
  public MenuShadow FileMenu;
  public CheckboxShadow Free;
  public CheckboxShadow Germany;
  public LabelBarShadow HTTPLabel;
  public TextFieldShadow HTTPProxy;
  public LabelShadow HTTPProxyLabel;
  public TextFieldShadow HTTPProxyPort;
  public LabelShadow HTTPProxyPortLabel;
  public CheckboxShadow Italian;
  public CheckboxShadow ItemByItem;
  public OwnRadioButtonPanelShadow ItemDescription;
  public GBPanelShadow ItemDescriptionCard;
  public LabelBarShadow ItemDescriptionLabel;
  public OwnRadioButtonPanelShadow ItemFields;
  public TextFieldShadow ItemFilename;
  public LabelShadow ItemFilenameLabel;
  public ChoiceShadow ItemUsage;
  public LabelShadow ItemUsageLabel;
  public TextFieldShadow Keywords;
  public LabelShadow KeywordsLabel;
  public TextFieldShadow Layout1;
  public LabelShadow Layout1Label;
  public TextFieldShadow Layout2;
  public LabelShadow Layout2Label;
  public TextFieldShadow Layout3;
  public LabelShadow Layout3Label;
  public FileDialogShadow LoadFileDialog;
  public MenuItemShadow LoadItem;
  public LabelShadow LocationInfo;
  public LabelBarShadow MailLabel;
  public CheckboxShadow Manual;
  public ChoiceShadow MediaType;
  public LabelShadow MediaTypeLabel;
  public TextAreaShadow Message;
  public ButtonShadow MetadataButton;
  public FrameShadow MetadataFrame;
  public MenuBarShadow MetadataMenuBar;
  public GBPanelShadow MetadataPanel;
  public TabbedFolderShadow MetadataTabbedFolder;
  public CheckboxShadow NoNotification;
  public CheckboxShadow NoStatistics;
  public CheckboxShadow NoTemplate;
  public CheckboxShadow Normal;
  public TextAreaShadow Note;
  public LabelShadow NoteLabel;
  public LabelShadow Notification;
  public CheckboxShadow NotificationNeeded;
  public LabelBarShadow NumberOfPagesLabel;
  public TextFieldShadow PageID;
  public LabelShadow PageIDLabel;
  public ChoiceShadow PageLayout;
  public LabelShadow PageLayoutLabel;
  public TextFieldShadow Password;
  public LabelShadow PasswordLabel;
  public OwnRadioButtonPanelShadow Preferences;
  public GBPanelShadow PreferencesCard;
  public TextFieldShadow PreviewDate;
  public LabelShadow PreviewDateLabel;
  public TextFieldShadow PreviewTime;
  public LabelShadow PreviewTimeLabel;
  public ChoiceShadow PriceClass;
  public LabelShadow PriceClassLabel;
  public LabelShadow PriceFixer;
  public OwnRadioButtonPanelShadow Prices;
  public GBPanelShadow PricesCard;
  public LabelBarShadow PricesLabel;
  public LabelShadow Pricing;
  public ChoiceShadow Priority;
  public LabelShadow PriorityLabel;
  public TextFieldShadow PurchasePrice;
  public LabelShadow PurchasePriceLabel;
  public OwnRadioButtonPanelShadow ReleaseDates;
  public LabelBarShadow ReleaseDatesLabel;
  public TextFieldShadow ReleaseDistributionDate;
  public LabelShadow ReleaseDistributionDateLabel;
  public TextFieldShadow ReleaseDistributionTime;
  public LabelShadow ReleaseDistributionTimeLabel;
  public TextFieldShadow ReleaseExpireDate;
  public LabelShadow ReleaseExpireDateLabel;
  public TextFieldShadow ReleaseExpireTime;
  public LabelShadow ReleaseExpireTimeLabel;
  public TextFieldShadow ReleasePreviewDate;
  public LabelShadow ReleasePreviewDateLabel;
  public TextFieldShadow ReleasePreviewTime;
  public LabelShadow ReleasePreviewTimeLabel;
  public ButtonShadow RemoveButton;
  public CheckboxShadow Restricted;
  public TextFieldShadow SMTPServer;
  public LabelShadow SMTPServerLabel;
  public CheckboxShadow SP;
  public CheckboxShadow SPAlone;
  public MenuItemShadow SaveAndSendItem;
  public FileDialogShadow SaveFileDialog;
  public MenuItemShadow SaveItem;
  public ButtonShadow SavePreferencesButton;
  public TextFieldShadow SellingPrice;
  public LabelShadow SellingPriceLabel;
  public OwnRadioButtonPanelShadow ServiceDescription;
  public GBPanelShadow ServiceDescriptionCard;
  public LabelBarShadow ServiceDescriptionLabel;
  public TextFieldShadow ServiceName;
  public LabelShadow ServiceNameLabel;
  public LabelShadow ServicePackageDescriptionLabel;
  public OwnRadioButtonPanelShadow Special;
  public GBPanelShadow SpecialCard;
  public LabelShadow Statistics;
  public CheckboxShadow StatisticsNeeded;
  public CheckboxShadow SubscriptionBased;
  public TextFieldShadow TemplateFile;
  public LabelShadow TemplateFileLabel;
  public LabelShadow TemplateType;
  public MenuShadow ToolsMenu;
  public LabelBarShadow UsageLabel;
  public CheckboxShadow User;
  public TextFieldShadow Username;
  public LabelShadow UsernameLabel;
  public TextFieldShadow VATAmount;
  public LabelShadow VATAmountLabel;
  public TextFieldShadow VATClass;
  public LabelShadow VATClassLabel;
  public CheckboxShadow WhatsNew;
  public GBPanelShadow gbpanel2;
  public LabelBarShadow labelbar7;
  public Root root1;

  public MetadataRoot(Group group) {
    setGroup(group);

    MetadataPanel = new GBPanelShadow();
    MetadataPanel.set("name", "MetadataPanel");
    add(MetadataPanel);
    {
      int _tmp[] = {14};
      MetadataPanel.set("rowHeights", _tmp);
    }
    MetadataPanel.set("GBConstraints", new GBConstraints("x=0;y=0;fill=both"));
    {
      int _tmp[] = {14};
      MetadataPanel.set("columnWidths", _tmp);
    }
    {
      double _tmp[] = {0.0};
      MetadataPanel.set("rowWeights", _tmp);
    }
    MetadataPanel.set("layoutSize", new java.awt.Dimension(415, 365));
    {
      double _tmp[] = {0.0};
      MetadataPanel.set("columnWeights", _tmp);
    }
    MetadataPanel.set("layoutLocation", new java.awt.Point(600, 0));

    MetadataButton = new ButtonShadow();
    MetadataButton.set("name", "MetadataButton");
    MetadataPanel.add(MetadataButton);
    MetadataButton.set("GBConstraints", new GBConstraints("x=0;y=0"));
    MetadataButton.set("text", "Metadata");

    MetadataFrame = new FrameShadow();
    MetadataFrame.set("name", "MetadataFrame");
    add(MetadataFrame);
    MetadataFrame.set("title", "Service Definition");
    MetadataFrame.set("layoutSize", new java.awt.Dimension(952, 825));
    MetadataFrame.set("layoutLocation", new java.awt.Point(-32000, -32000));

    gbpanel2 = new GBPanelShadow();
    gbpanel2.set("name", "gbpanel2");
    MetadataFrame.add(gbpanel2);
    {
      int _tmp[] = {14,14,14};
      gbpanel2.set("rowHeights", _tmp);
    }
    gbpanel2.set("GBConstraints", new GBConstraints("x=0;y=0;fill=both"));
    {
      int _tmp[] = {14};
      gbpanel2.set("columnWidths", _tmp);
    }
    {
      double _tmp[] = {0.0,1.0,0.0};
      gbpanel2.set("rowWeights", _tmp);
    }
    {
      double _tmp[] = {1.0};
      gbpanel2.set("columnWeights", _tmp);
    }

    ServicePackageDescriptionLabel = new LabelShadow();
    ServicePackageDescriptionLabel.set("name", "ServicePackageDescriptionLabel");
    gbpanel2.add(ServicePackageDescriptionLabel);
    ServicePackageDescriptionLabel.set("GBConstraints", new GBConstraints("x=0;y=0"));
    ServicePackageDescriptionLabel.set("text", "Service Package Description");
    ServicePackageDescriptionLabel.set("font", convert("java.awt.Font", "name=Dialog;style=bold;size=16"));

    MetadataTabbedFolder = new TabbedFolderShadow();
    MetadataTabbedFolder.set("name", "MetadataTabbedFolder");
    gbpanel2.add(MetadataTabbedFolder);
    MetadataTabbedFolder.set("GBConstraints", new GBConstraints("x=0;y=1;fill=both"));

    ServiceDescriptionCard = new GBPanelShadow();
    ServiceDescriptionCard.set("name", "ServiceDescriptionCard");
    MetadataTabbedFolder.add(ServiceDescriptionCard);
    {
      int _tmp[] = {14,14,14};
      ServiceDescriptionCard.set("rowHeights", _tmp);
    }
    {
      int _tmp[] = {14};
      ServiceDescriptionCard.set("columnWidths", _tmp);
    }
    ServiceDescriptionCard.set("layoutName", "Service Description");
    {
      double _tmp[] = {0.0,0.0,1.0};
      ServiceDescriptionCard.set("rowWeights", _tmp);
    }
    {
      double _tmp[] = {1.0};
      ServiceDescriptionCard.set("columnWeights", _tmp);
    }

    ServiceDescription = new OwnRadioButtonPanelShadow();
    ServiceDescription.set("name", "ServiceDescription");
    ServiceDescriptionCard.add(ServiceDescription);
    {
      int _tmp[] = {14,14,14,14,14,14,14,14,14,14};
      ServiceDescription.set("rowHeights", _tmp);
    }
    ServiceDescription.set("GBConstraints", new GBConstraints("x=0;y=0;fill=both"));
    {
      int _tmp[] = {14,14,14,14,14};
      ServiceDescription.set("columnWidths", _tmp);
    }
    ServiceDescription.set("anchor", new sunsoft.jws.visual.rt.type.AnchorEnum("north"));
    {
      double _tmp[] = {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
      ServiceDescription.set("rowWeights", _tmp);
    }
    {
      double _tmp[] = {0.0,1.0,1.0,0.0,1.0};
      ServiceDescription.set("columnWeights", _tmp);
    }

    AuthorLabel = new LabelBarShadow();
    AuthorLabel.set("name", "AuthorLabel");
    ServiceDescription.add(AuthorLabel);
    AuthorLabel.set("GBConstraints", new GBConstraints("x=0;y=0;width=5;fill=horizontal"));
    AuthorLabel.set("text", "Author");

    ContentProviderLabel = new LabelShadow();
    ContentProviderLabel.set("name", "ContentProviderLabel");
    ServiceDescription.add(ContentProviderLabel);
    ContentProviderLabel.set("GBConstraints", new GBConstraints("x=0;y=1"));
    ContentProviderLabel.set("text", "Content Provider:");

    CreationDateLabel = new LabelShadow();
    CreationDateLabel.set("name", "CreationDateLabel");
    ServiceDescription.add(CreationDateLabel);
    CreationDateLabel.set("GBConstraints", new GBConstraints("x=2;y=1"));
    CreationDateLabel.set("text", "Creation date:");

    CreationDate = new TextFieldShadow();
    CreationDate.set("name", "CreationDate");
    ServiceDescription.add(CreationDate);
    CreationDate.set("GBConstraints", new GBConstraints("x=3;y=1;fill=horizontal"));

    ServiceDescriptionLabel = new LabelBarShadow();
    ServiceDescriptionLabel.set("name", "ServiceDescriptionLabel");
    ServiceDescription.add(ServiceDescriptionLabel);
    ServiceDescriptionLabel.set("GBConstraints", new GBConstraints("x=0;y=2;width=5;fill=horizontal"));
    ServiceDescriptionLabel.set("text", "Service description");

    ServiceNameLabel = new LabelShadow();
    ServiceNameLabel.set("name", "ServiceNameLabel");
    ServiceDescription.add(ServiceNameLabel);
    ServiceNameLabel.set("GBConstraints", new GBConstraints("x=2;y=3"));
    ServiceNameLabel.set("text", "Service name:");

    ServiceName = new TextFieldShadow();
    ServiceName.set("name", "ServiceName");
    ServiceDescription.add(ServiceName);
    ServiceName.set("GBConstraints", new GBConstraints("x=3;y=3;width=2;fill=horizontal"));

    CategoryLabel = new LabelShadow();
    CategoryLabel.set("name", "CategoryLabel");
    ServiceDescription.add(CategoryLabel);
    CategoryLabel.set("GBConstraints", new GBConstraints("x=0;y=3"));
    CategoryLabel.set("text", "Category:");

    DescriptionLabel = new LabelShadow();
    DescriptionLabel.set("name", "DescriptionLabel");
    ServiceDescription.add(DescriptionLabel);
    DescriptionLabel.set("GBConstraints", new GBConstraints("x=0;y=4"));
    DescriptionLabel.set("text", "Description:");

    KeywordsLabel = new LabelShadow();
    KeywordsLabel.set("name", "KeywordsLabel");
    ServiceDescription.add(KeywordsLabel);
    KeywordsLabel.set("GBConstraints", new GBConstraints("x=0;y=6"));
    KeywordsLabel.set("text", "Keywords:");

    Keywords = new TextFieldShadow();
    Keywords.set("name", "Keywords");
    ServiceDescription.add(Keywords);
    Keywords.set("GBConstraints", new GBConstraints("x=1;y=6;width=4;fill=horizontal"));

    ContentLanguageLabel = new LabelShadow();
    ContentLanguageLabel.set("name", "ContentLanguageLabel");
    ServiceDescription.add(ContentLanguageLabel);
    ContentLanguageLabel.set("GBConstraints", new GBConstraints("x=0;y=7"));
    ContentLanguageLabel.set("text", "Content language:");

    ContentLanguage = new GBPanelShadow();
    ContentLanguage.set("name", "ContentLanguage");
    ServiceDescription.add(ContentLanguage);
    {
      int _tmp[] = {14};
      ContentLanguage.set("rowHeights", _tmp);
    }
    ContentLanguage.set("GBConstraints", new GBConstraints("x=1;y=7;width=3;fill=both"));
    {
      int _tmp[] = {14,14,14};
      ContentLanguage.set("columnWidths", _tmp);
    }
    {
      double _tmp[] = {0.0};
      ContentLanguage.set("rowWeights", _tmp);
    }
    {
      double _tmp[] = {0.0,0.0,0.0};
      ContentLanguage.set("columnWeights", _tmp);
    }

    English = new CheckboxShadow();
    English.set("name", "English");
    ContentLanguage.add(English);
    English.set("GBConstraints", new GBConstraints("x=0;y=0"));
    English.set("text", "English");
    English.set("state", Boolean.TRUE);

    Germany = new CheckboxShadow();
    Germany.set("name", "Germany");
    ContentLanguage.add(Germany);
    Germany.set("GBConstraints", new GBConstraints("x=1;y=0"));
    Germany.set("text", "Germany");

    Italian = new CheckboxShadow();
    Italian.set("name", "Italian");
    ContentLanguage.add(Italian);
    Italian.set("GBConstraints", new GBConstraints("x=2;y=0"));
    Italian.set("text", "Italian");

    UsageLabel = new LabelBarShadow();
    UsageLabel.set("name", "UsageLabel");
    ServiceDescription.add(UsageLabel);
    UsageLabel.set("GBConstraints", new GBConstraints("x=0;y=8;width=5;fill=horizontal"));
    UsageLabel.set("text", "Usage");

    ContentUsage = new LabelShadow();
    ContentUsage.set("name", "ContentUsage");
    ServiceDescription.add(ContentUsage);
    ContentUsage.set("GBConstraints", new GBConstraints("x=0;y=9"));
    ContentUsage.set("text", "Content usage:");

    Free = new CheckboxShadow();
    Free.set("name", "Free");
    ServiceDescription.add(Free);
    Free.set("GBConstraints", new GBConstraints("x=1;y=9"));
    Free.set("text", "Free");
    Free.set("state", Boolean.TRUE);

    Restricted = new CheckboxShadow();
    Restricted.set("name", "Restricted");
    ServiceDescription.add(Restricted);
    Restricted.set("GBConstraints", new GBConstraints("x=2;y=9"));
    Restricted.set("text", "Restricted");

    CopyrightsOwnerLabel = new LabelShadow();
    CopyrightsOwnerLabel.set("name", "CopyrightsOwnerLabel");
    ServiceDescription.add(CopyrightsOwnerLabel);
    CopyrightsOwnerLabel.set("GBConstraints", new GBConstraints("x=3;y=9"));
    CopyrightsOwnerLabel.set("text", "Copyrights owner:");

    CopyrightsOwner = new TextFieldShadow();
    CopyrightsOwner.set("name", "CopyrightsOwner");
    ServiceDescription.add(CopyrightsOwner);
    CopyrightsOwner.set("GBConstraints", new GBConstraints("x=4;y=9;fill=horizontal"));

    ContentProvider = new ChoiceShadow();
    ContentProvider.set("name", "ContentProvider");
    ServiceDescription.add(ContentProvider);
    ContentProvider.set("items", convert("[Ljava.lang.String;", "Bertelsmann,Datanord,Reuters"));
    ContentProvider.set("GBConstraints", new GBConstraints("x=1;y=1"));
    ContentProvider.set("anchor", new sunsoft.jws.visual.rt.type.AnchorEnum("west"));
    ContentProvider.set("selectedItem", "Bertelsmann");

    Category = new ChoiceShadow();
    Category.set("name", "Category");
    ServiceDescription.add(Category);
    Category.set("items", convert("[Ljava.lang.String;", "news,city info,travel,finance,knowledge,entertainment,communication"));
    Category.set("GBConstraints", new GBConstraints("x=1;y=3"));
    Category.set("anchor", new sunsoft.jws.visual.rt.type.AnchorEnum("west"));
    Category.set("selectedItem", "news");

    Description = new TextAreaShadow();
    Description.set("name", "Description");
    ServiceDescription.add(Description);
    Description.set("GBConstraints", new GBConstraints("x=0;y=5;width=5;fill=both"));
    Description.set("numRows", new Integer(3));

    ReleaseDates = new OwnRadioButtonPanelShadow();
    ReleaseDates.set("name", "ReleaseDates");
    ServiceDescriptionCard.add(ReleaseDates);
    {
      int _tmp[] = {14,14,14};
      ReleaseDates.set("rowHeights", _tmp);
    }
    ReleaseDates.set("GBConstraints", new GBConstraints("x=0;y=1;fill=both"));
    {
      int _tmp[] = {14,14,14,14,14,14};
      ReleaseDates.set("columnWidths", _tmp);
    }
    {
      double _tmp[] = {0.0,0.0,0.0};
      ReleaseDates.set("rowWeights", _tmp);
    }
    {
      double _tmp[] = {0.0,1.0,0.0,1.0,0.0,1.0};
      ReleaseDates.set("columnWeights", _tmp);
    }

    ReleaseDatesLabel = new LabelBarShadow();
    ReleaseDatesLabel.set("name", "ReleaseDatesLabel");
    ReleaseDates.add(ReleaseDatesLabel);
    ReleaseDatesLabel.set("GBConstraints", new GBConstraints("x=0;y=0;width=6;fill=horizontal"));
    ReleaseDatesLabel.set("text", "Release dates");

    ReleasePreviewDateLabel = new LabelShadow();
    ReleasePreviewDateLabel.set("name", "ReleasePreviewDateLabel");
    ReleaseDates.add(ReleasePreviewDateLabel);
    ReleasePreviewDateLabel.set("GBConstraints", new GBConstraints("x=0;y=1"));
    ReleasePreviewDateLabel.set("text", "Preview date:");

    ReleasePreviewDate = new TextFieldShadow();
    ReleasePreviewDate.set("name", "ReleasePreviewDate");
    ReleaseDates.add(ReleasePreviewDate);
    ReleasePreviewDate.set("GBConstraints", new GBConstraints("x=1;y=1;fill=horizontal"));

    ReleaseDistributionDate = new TextFieldShadow();
    ReleaseDistributionDate.set("name", "ReleaseDistributionDate");
    ReleaseDates.add(ReleaseDistributionDate);
    ReleaseDistributionDate.set("GBConstraints", new GBConstraints("x=3;y=1;fill=horizontal"));

    ReleaseExpireDate = new TextFieldShadow();
    ReleaseExpireDate.set("name", "ReleaseExpireDate");
    ReleaseDates.add(ReleaseExpireDate);
    ReleaseExpireDate.set("GBConstraints", new GBConstraints("x=5;y=1;fill=horizontal"));

    ReleasePreviewTime = new TextFieldShadow();
    ReleasePreviewTime.set("name", "ReleasePreviewTime");
    ReleaseDates.add(ReleasePreviewTime);
    ReleasePreviewTime.set("GBConstraints", new GBConstraints("x=1;y=2;fill=horizontal"));

    ReleaseDistributionTime = new TextFieldShadow();
    ReleaseDistributionTime.set("name", "ReleaseDistributionTime");
    ReleaseDates.add(ReleaseDistributionTime);
    ReleaseDistributionTime.set("GBConstraints", new GBConstraints("x=3;y=2;fill=horizontal"));

    ReleaseExpireTime = new TextFieldShadow();
    ReleaseExpireTime.set("name", "ReleaseExpireTime");
    ReleaseDates.add(ReleaseExpireTime);
    ReleaseExpireTime.set("GBConstraints", new GBConstraints("x=5;y=2;fill=horizontal"));

    ReleasePreviewTimeLabel = new LabelShadow();
    ReleasePreviewTimeLabel.set("name", "ReleasePreviewTimeLabel");
    ReleaseDates.add(ReleasePreviewTimeLabel);
    ReleasePreviewTimeLabel.set("GBConstraints", new GBConstraints("x=0;y=2"));
    ReleasePreviewTimeLabel.set("text", "Preview time:");

    ReleaseDistributionDateLabel = new LabelShadow();
    ReleaseDistributionDateLabel.set("name", "ReleaseDistributionDateLabel");
    ReleaseDates.add(ReleaseDistributionDateLabel);
    ReleaseDistributionDateLabel.set("GBConstraints", new GBConstraints("x=2;y=1"));
    ReleaseDistributionDateLabel.set("text", "Distribution date:");

    ReleaseDistributionTimeLabel = new LabelShadow();
    ReleaseDistributionTimeLabel.set("name", "ReleaseDistributionTimeLabel");
    ReleaseDates.add(ReleaseDistributionTimeLabel);
    ReleaseDistributionTimeLabel.set("GBConstraints", new GBConstraints("x=2;y=2"));
    ReleaseDistributionTimeLabel.set("text", "Distribution time:");

    ReleaseExpireDateLabel = new LabelShadow();
    ReleaseExpireDateLabel.set("name", "ReleaseExpireDateLabel");
    ReleaseDates.add(ReleaseExpireDateLabel);
    ReleaseExpireDateLabel.set("GBConstraints", new GBConstraints("x=4;y=1"));
    ReleaseExpireDateLabel.set("text", "Expire date:");

    ReleaseExpireTimeLabel = new LabelShadow();
    ReleaseExpireTimeLabel.set("name", "ReleaseExpireTimeLabel");
    ReleaseDates.add(ReleaseExpireTimeLabel);
    ReleaseExpireTimeLabel.set("GBConstraints", new GBConstraints("x=4;y=2"));
    ReleaseExpireTimeLabel.set("text", "Expire time:");

    ItemDescriptionCard = new GBPanelShadow();
    ItemDescriptionCard.set("name", "ItemDescriptionCard");
    MetadataTabbedFolder.add(ItemDescriptionCard);
    {
      int _tmp[] = {14,14};
      ItemDescriptionCard.set("rowHeights", _tmp);
    }
    {
      int _tmp[] = {14};
      ItemDescriptionCard.set("columnWidths", _tmp);
    }
    ItemDescriptionCard.set("layoutName", "Item Description");
    ItemDescriptionCard.set("visible", Boolean.FALSE);
    {
      double _tmp[] = {0.0,1.0};
      ItemDescriptionCard.set("rowWeights", _tmp);
    }
    {
      double _tmp[] = {1.0};
      ItemDescriptionCard.set("columnWeights", _tmp);
    }

    ItemDescription = new OwnRadioButtonPanelShadow();
    ItemDescription.set("name", "ItemDescription");
    ItemDescriptionCard.add(ItemDescription);
    {
      int _tmp[] = {14,14};
      ItemDescription.set("rowHeights", _tmp);
    }
    ItemDescription.set("GBConstraints", new GBConstraints("x=0;y=0;fill=both"));
    {
      int _tmp[] = {14,14};
      ItemDescription.set("columnWidths", _tmp);
    }
    {
      double _tmp[] = {0.0,0.0};
      ItemDescription.set("rowWeights", _tmp);
    }
    {
      double _tmp[] = {1.0,1.0};
      ItemDescription.set("columnWeights", _tmp);
    }

    ItemFields = new OwnRadioButtonPanelShadow();
    ItemFields.set("name", "ItemFields");
    ItemDescription.add(ItemFields);
    {
      int _tmp[] = {14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14};
      ItemFields.set("rowHeights", _tmp);
    }
    ItemFields.set("GBConstraints", new GBConstraints("x=0;y=1;fill=both"));
    {
      int _tmp[] = {14,14};
      ItemFields.set("columnWidths", _tmp);
    }
    {
      double _tmp[] = {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
      ItemFields.set("rowWeights", _tmp);
    }
    {
      double _tmp[] = {0.0,1.0};
      ItemFields.set("columnWeights", _tmp);
    }

    ItemFilenameLabel = new LabelShadow();
    ItemFilenameLabel.set("name", "ItemFilenameLabel");
    ItemFields.add(ItemFilenameLabel);
    ItemFilenameLabel.set("GBConstraints", new GBConstraints("x=0;y=1"));
    ItemFilenameLabel.set("text", "Item filename:");

    MediaTypeLabel = new LabelShadow();
    MediaTypeLabel.set("name", "MediaTypeLabel");
    ItemFields.add(MediaTypeLabel);
    MediaTypeLabel.set("GBConstraints", new GBConstraints("x=0;y=2"));
    MediaTypeLabel.set("text", "Media type:");

    SellingPriceLabel = new LabelShadow();
    SellingPriceLabel.set("name", "SellingPriceLabel");
    ItemFields.add(SellingPriceLabel);
    SellingPriceLabel.set("GBConstraints", new GBConstraints("x=0;y=12"));
    SellingPriceLabel.set("text", "Selling price:");

    PurchasePriceLabel = new LabelShadow();
    PurchasePriceLabel.set("name", "PurchasePriceLabel");
    ItemFields.add(PurchasePriceLabel);
    PurchasePriceLabel.set("GBConstraints", new GBConstraints("x=0;y=13"));
    PurchasePriceLabel.set("text", "Purchase price:");

    VATClassLabel = new LabelShadow();
    VATClassLabel.set("name", "VATClassLabel");
    ItemFields.add(VATClassLabel);
    VATClassLabel.set("GBConstraints", new GBConstraints("x=0;y=14"));
    VATClassLabel.set("text", "VAT class:");

    VATAmountLabel = new LabelShadow();
    VATAmountLabel.set("name", "VATAmountLabel");
    ItemFields.add(VATAmountLabel);
    VATAmountLabel.set("GBConstraints", new GBConstraints("x=0;y=15"));
    VATAmountLabel.set("text", "VAT amount:");

    PreviewDateLabel = new LabelShadow();
    PreviewDateLabel.set("name", "PreviewDateLabel");
    ItemFields.add(PreviewDateLabel);
    PreviewDateLabel.set("GBConstraints", new GBConstraints("x=0;y=4"));
    PreviewDateLabel.set("text", "Preview date:");

    ExpireDateLabel = new LabelShadow();
    ExpireDateLabel.set("name", "ExpireDateLabel");
    ItemFields.add(ExpireDateLabel);
    ExpireDateLabel.set("GBConstraints", new GBConstraints("x=0;y=8"));
    ExpireDateLabel.set("text", "Expire date:");

    ItemUsageLabel = new LabelShadow();
    ItemUsageLabel.set("name", "ItemUsageLabel");
    ItemFields.add(ItemUsageLabel);
    ItemUsageLabel.set("GBConstraints", new GBConstraints("x=0;y=10"));
    ItemUsageLabel.set("text", "Item usage:");

    PageLayoutLabel = new LabelShadow();
    PageLayoutLabel.set("name", "PageLayoutLabel");
    ItemFields.add(PageLayoutLabel);
    PageLayoutLabel.set("GBConstraints", new GBConstraints("x=0;y=16"));
    PageLayoutLabel.set("text", "Page layout #:");

    DistributionDateLabel = new LabelShadow();
    DistributionDateLabel.set("name", "DistributionDateLabel");
    ItemFields.add(DistributionDateLabel);
    DistributionDateLabel.set("GBConstraints", new GBConstraints("x=0;y=6"));
    DistributionDateLabel.set("text", "Distribution date:");

    CopyrightLabel = new LabelShadow();
    CopyrightLabel.set("name", "CopyrightLabel");
    ItemFields.add(CopyrightLabel);
    CopyrightLabel.set("GBConstraints", new GBConstraints("x=0;y=11"));
    CopyrightLabel.set("text", "Copyright:");

    PageIDLabel = new LabelShadow();
    PageIDLabel.set("name", "PageIDLabel");
    ItemFields.add(PageIDLabel);
    PageIDLabel.set("GBConstraints", new GBConstraints("x=0;y=17"));
    PageIDLabel.set("text", "Page ID #:");

    ItemFilename = new TextFieldShadow();
    ItemFilename.set("name", "ItemFilename");
    ItemFields.add(ItemFilename);
    ItemFilename.set("GBConstraints", new GBConstraints("x=1;y=1;fill=horizontal"));

    SellingPrice = new TextFieldShadow();
    SellingPrice.set("name", "SellingPrice");
    ItemFields.add(SellingPrice);
    SellingPrice.set("GBConstraints", new GBConstraints("x=1;y=12;fill=horizontal"));

    PurchasePrice = new TextFieldShadow();
    PurchasePrice.set("name", "PurchasePrice");
    ItemFields.add(PurchasePrice);
    PurchasePrice.set("GBConstraints", new GBConstraints("x=1;y=13;fill=horizontal"));

    VATClass = new TextFieldShadow();
    VATClass.set("name", "VATClass");
    ItemFields.add(VATClass);
    VATClass.set("GBConstraints", new GBConstraints("x=1;y=14;fill=horizontal"));

    VATAmount = new TextFieldShadow();
    VATAmount.set("name", "VATAmount");
    ItemFields.add(VATAmount);
    VATAmount.set("GBConstraints", new GBConstraints("x=1;y=15;fill=horizontal"));

    PreviewDate = new TextFieldShadow();
    PreviewDate.set("name", "PreviewDate");
    ItemFields.add(PreviewDate);
    PreviewDate.set("GBConstraints", new GBConstraints("x=1;y=4;fill=horizontal"));

    DistributionDate = new TextFieldShadow();
    DistributionDate.set("name", "DistributionDate");
    ItemFields.add(DistributionDate);
    DistributionDate.set("GBConstraints", new GBConstraints("x=1;y=6;fill=horizontal"));

    ExpireDate = new TextFieldShadow();
    ExpireDate.set("name", "ExpireDate");
    ItemFields.add(ExpireDate);
    ExpireDate.set("GBConstraints", new GBConstraints("x=1;y=8;fill=horizontal"));

    Copyright = new TextFieldShadow();
    Copyright.set("name", "Copyright");
    ItemFields.add(Copyright);
    Copyright.set("GBConstraints", new GBConstraints("x=1;y=11;fill=horizontal"));

    PageID = new TextFieldShadow();
    PageID.set("name", "PageID");
    ItemFields.add(PageID);
    PageID.set("GBConstraints", new GBConstraints("x=1;y=17;fill=horizontal"));

    ItemDescriptionLabel = new LabelBarShadow();
    ItemDescriptionLabel.set("name", "ItemDescriptionLabel");
    ItemFields.add(ItemDescriptionLabel);
    ItemDescriptionLabel.set("GBConstraints", new GBConstraints("x=0;y=0;width=2;fill=horizontal"));
    ItemDescriptionLabel.set("text", "Item description");

    PageLayout = new ChoiceShadow();
    PageLayout.set("name", "PageLayout");
    ItemFields.add(PageLayout);
    PageLayout.set("items", convert("[Ljava.lang.String;", "1,2,3"));
    PageLayout.set("GBConstraints", new GBConstraints("x=1;y=16"));
    PageLayout.set("anchor", new sunsoft.jws.visual.rt.type.AnchorEnum("west"));
    PageLayout.set("selectedItem", "1");

    MediaType = new ChoiceShadow();
    MediaType.set("name", "MediaType");
    ItemFields.add(MediaType);
    MediaType.set("items", convert("[Ljava.lang.String;", "text,graphics,image,audio,video,animation"));
    MediaType.set("GBConstraints", new GBConstraints("x=1;y=2"));
    MediaType.set("anchor", new sunsoft.jws.visual.rt.type.AnchorEnum("west"));
    MediaType.set("selectedItem", "text");

    PreviewTimeLabel = new LabelShadow();
    PreviewTimeLabel.set("name", "PreviewTimeLabel");
    ItemFields.add(PreviewTimeLabel);
    PreviewTimeLabel.set("GBConstraints", new GBConstraints("x=0;y=5"));
    PreviewTimeLabel.set("text", "Preview time:");

    DistributionTimeLabel = new LabelShadow();
    DistributionTimeLabel.set("name", "DistributionTimeLabel");
    ItemFields.add(DistributionTimeLabel);
    DistributionTimeLabel.set("GBConstraints", new GBConstraints("x=0;y=7"));
    DistributionTimeLabel.set("text", "Distribution time:");

    ExpireTimeLabel = new LabelShadow();
    ExpireTimeLabel.set("name", "ExpireTimeLabel");
    ItemFields.add(ExpireTimeLabel);
    ExpireTimeLabel.set("GBConstraints", new GBConstraints("x=0;y=9"));
    ExpireTimeLabel.set("text", "Expire time:");

    PreviewTime = new TextFieldShadow();
    PreviewTime.set("name", "PreviewTime");
    ItemFields.add(PreviewTime);
    PreviewTime.set("GBConstraints", new GBConstraints("x=1;y=5;fill=horizontal"));

    DistributionTime = new TextFieldShadow();
    DistributionTime.set("name", "DistributionTime");
    ItemFields.add(DistributionTime);
    DistributionTime.set("GBConstraints", new GBConstraints("x=1;y=7;fill=horizontal"));

    ExpireTime = new TextFieldShadow();
    ExpireTime.set("name", "ExpireTime");
    ItemFields.add(ExpireTime);
    ExpireTime.set("GBConstraints", new GBConstraints("x=1;y=9;fill=horizontal"));

    ItemUsage = new ChoiceShadow();
    ItemUsage.set("name", "ItemUsage");
    ItemFields.add(ItemUsage);
    ItemUsage.set("items", convert("[Ljava.lang.String;", "category frame,logo frame,content frame,service list frame,service tools frame"));
    ItemUsage.set("GBConstraints", new GBConstraints("x=1;y=10"));
    ItemUsage.set("anchor", new sunsoft.jws.visual.rt.type.AnchorEnum("west"));
    ItemUsage.set("selectedItem", "category frame");

    FileDescriptionLabel = new LabelShadow();
    FileDescriptionLabel.set("name", "FileDescriptionLabel");
    ItemFields.add(FileDescriptionLabel);
    FileDescriptionLabel.set("GBConstraints", new GBConstraints("x=0;y=3"));
    FileDescriptionLabel.set("text", "Description:");

    FileDescription = new TextFieldShadow();
    FileDescription.set("name", "FileDescription");
    ItemFields.add(FileDescription);
    FileDescription.set("GBConstraints", new GBConstraints("x=1;y=3;fill=horizontal"));

    ButtonPanel = new GBPanelShadow();
    ButtonPanel.set("name", "ButtonPanel");
    ItemDescription.add(ButtonPanel);
    {
      int _tmp[] = {14};
      ButtonPanel.set("rowHeights", _tmp);
    }
    ButtonPanel.set("GBConstraints", new GBConstraints("x=0;y=0;width=2;fill=both"));
    {
      int _tmp[] = {14,14,14};
      ButtonPanel.set("columnWidths", _tmp);
    }
    {
      double _tmp[] = {0.0};
      ButtonPanel.set("rowWeights", _tmp);
    }
    {
      double _tmp[] = {0.0,0.0,1.0};
      ButtonPanel.set("columnWeights", _tmp);
    }

    ApplyButton = new ButtonShadow();
    ApplyButton.set("name", "ApplyButton");
    ButtonPanel.add(ApplyButton);
    ApplyButton.set("GBConstraints", new GBConstraints("x=1;y=0"));
    ApplyButton.set("text", "Apply");

    RemoveButton = new ButtonShadow();
    RemoveButton.set("name", "RemoveButton");
    ButtonPanel.add(RemoveButton);
    RemoveButton.set("GBConstraints", new GBConstraints("x=2;y=0"));
    RemoveButton.set("text", "Remove");

    BrowseButton = new ButtonShadow();
    BrowseButton.set("name", "BrowseButton");
    ButtonPanel.add(BrowseButton);
    BrowseButton.set("GBConstraints", new GBConstraints("x=0;y=0"));
    BrowseButton.set("text", "Browse...");

    ContentFiles = new ColumnListShadow();
    ContentFiles.set("name", "ContentFiles");
    ItemDescription.add(ContentFiles);
    ContentFiles.set("GBConstraints", new GBConstraints("x=1;y=1;fill=both"));
    ContentFiles.set("highlightItems", Boolean.TRUE);
    ContentFiles.set("headers", convert("[Ljava.lang.String;", "key=List of attached content files:"));

    PreferencesCard = new GBPanelShadow();
    PreferencesCard.set("name", "PreferencesCard");
    MetadataTabbedFolder.add(PreferencesCard);
    {
      int _tmp[] = {14,14};
      PreferencesCard.set("rowHeights", _tmp);
    }
    {
      int _tmp[] = {14};
      PreferencesCard.set("columnWidths", _tmp);
    }
    PreferencesCard.set("layoutName", "Preferences");
    PreferencesCard.set("visible", Boolean.FALSE);
    {
      double _tmp[] = {0.0,1.0};
      PreferencesCard.set("rowWeights", _tmp);
    }
    {
      double _tmp[] = {1.0};
      PreferencesCard.set("columnWeights", _tmp);
    }

    Preferences = new OwnRadioButtonPanelShadow();
    Preferences.set("name", "Preferences");
    PreferencesCard.add(Preferences);
    {
      int _tmp[] = {14,14,14,14,14,14,14,14,14,14,14};
      Preferences.set("rowHeights", _tmp);
    }
    Preferences.set("GBConstraints", new GBConstraints("x=0;y=0;fill=both"));
    {
      int _tmp[] = {14,14,14,14};
      Preferences.set("columnWidths", _tmp);
    }
    {
      double _tmp[] = {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
      Preferences.set("rowWeights", _tmp);
    }
    {
      double _tmp[] = {0.0,1.0,0.0,1.0};
      Preferences.set("columnWeights", _tmp);
    }

    BaseLabel = new LabelBarShadow();
    BaseLabel.set("name", "BaseLabel");
    Preferences.add(BaseLabel);
    BaseLabel.set("GBConstraints", new GBConstraints("x=0;y=0;width=4;fill=horizontal"));
    BaseLabel.set("text", "Base");

    HTTPLabel = new LabelBarShadow();
    HTTPLabel.set("name", "HTTPLabel");
    Preferences.add(HTTPLabel);
    HTTPLabel.set("GBConstraints", new GBConstraints("x=0;y=2;width=4;fill=horizontal"));
    HTTPLabel.set("text", "HTTP");

    MailLabel = new LabelBarShadow();
    MailLabel.set("name", "MailLabel");
    Preferences.add(MailLabel);
    MailLabel.set("GBConstraints", new GBConstraints("x=0;y=5;width=4;fill=horizontal"));
    MailLabel.set("text", "Mail");

    DocumentBaseLabel = new LabelShadow();
    DocumentBaseLabel.set("name", "DocumentBaseLabel");
    Preferences.add(DocumentBaseLabel);
    DocumentBaseLabel.set("GBConstraints", new GBConstraints("x=0;y=1"));
    DocumentBaseLabel.set("text", "Document base:");

    HTTPProxyLabel = new LabelShadow();
    HTTPProxyLabel.set("name", "HTTPProxyLabel");
    Preferences.add(HTTPProxyLabel);
    HTTPProxyLabel.set("GBConstraints", new GBConstraints("x=0;y=3"));
    HTTPProxyLabel.set("text", "HTTP Proxy:");

    UsernameLabel = new LabelShadow();
    UsernameLabel.set("name", "UsernameLabel");
    Preferences.add(UsernameLabel);
    UsernameLabel.set("GBConstraints", new GBConstraints("x=0;y=4"));
    UsernameLabel.set("text", "Username:");

    SMTPServerLabel = new LabelShadow();
    SMTPServerLabel.set("name", "SMTPServerLabel");
    Preferences.add(SMTPServerLabel);
    SMTPServerLabel.set("GBConstraints", new GBConstraints("x=0;y=6"));
    SMTPServerLabel.set("text", "SMTP Server:");

    EmailAddressLabel = new LabelShadow();
    EmailAddressLabel.set("name", "EmailAddressLabel");
    Preferences.add(EmailAddressLabel);
    EmailAddressLabel.set("GBConstraints", new GBConstraints("x=0;y=7"));
    EmailAddressLabel.set("text", "Email address:");

    NoteLabel = new LabelShadow();
    NoteLabel.set("name", "NoteLabel");
    Preferences.add(NoteLabel);
    NoteLabel.set("GBConstraints", new GBConstraints("x=0;y=8"));
    NoteLabel.set("text", "Note:");

    DocumentBase = new TextFieldShadow();
    DocumentBase.set("name", "DocumentBase");
    Preferences.add(DocumentBase);
    DocumentBase.set("GBConstraints", new GBConstraints("x=1;y=1;width=3;fill=horizontal"));

    HTTPProxy = new TextFieldShadow();
    HTTPProxy.set("name", "HTTPProxy");
    Preferences.add(HTTPProxy);
    HTTPProxy.set("GBConstraints", new GBConstraints("x=1;y=3;fill=horizontal"));

    Username = new TextFieldShadow();
    Username.set("name", "Username");
    Preferences.add(Username);
    Username.set("GBConstraints", new GBConstraints("x=1;y=4;fill=horizontal"));

    SMTPServer = new TextFieldShadow();
    SMTPServer.set("name", "SMTPServer");
    Preferences.add(SMTPServer);
    SMTPServer.set("GBConstraints", new GBConstraints("x=1;y=6;fill=horizontal"));

    EmailAddress = new TextFieldShadow();
    EmailAddress.set("name", "EmailAddress");
    Preferences.add(EmailAddress);
    EmailAddress.set("GBConstraints", new GBConstraints("x=1;y=7;width=2;fill=horizontal"));

    HTTPProxyPortLabel = new LabelShadow();
    HTTPProxyPortLabel.set("name", "HTTPProxyPortLabel");
    Preferences.add(HTTPProxyPortLabel);
    HTTPProxyPortLabel.set("GBConstraints", new GBConstraints("x=2;y=3"));
    HTTPProxyPortLabel.set("text", "Port:");

    PasswordLabel = new LabelShadow();
    PasswordLabel.set("name", "PasswordLabel");
    Preferences.add(PasswordLabel);
    PasswordLabel.set("GBConstraints", new GBConstraints("x=2;y=4"));
    PasswordLabel.set("text", "Password:");

    HTTPProxyPort = new TextFieldShadow();
    HTTPProxyPort.set("name", "HTTPProxyPort");
    Preferences.add(HTTPProxyPort);
    HTTPProxyPort.set("GBConstraints", new GBConstraints("x=3;y=3;fill=horizontal"));

    Password = new TextFieldShadow();
    Password.set("name", "Password");
    Preferences.add(Password);
    Password.set("GBConstraints", new GBConstraints("x=3;y=4;fill=horizontal"));
    Password.set("echoCharacter", new Character('#'));

    Note = new TextAreaShadow();
    Note.set("name", "Note");
    Preferences.add(Note);
    Note.set("GBConstraints", new GBConstraints("x=0;y=9;width=4;fill=both"));
    Note.set("numRows", new Integer(5));

    SavePreferencesButton = new ButtonShadow();
    SavePreferencesButton.set("name", "SavePreferencesButton");
    Preferences.add(SavePreferencesButton);
    SavePreferencesButton.set("GBConstraints", new GBConstraints("x=0;y=10"));
    SavePreferencesButton.set("text", "Save preferences");

    PricesCard = new GBPanelShadow();
    PricesCard.set("name", "PricesCard");
    MetadataTabbedFolder.add(PricesCard);
    {
      int _tmp[] = {14,14};
      PricesCard.set("rowHeights", _tmp);
    }
    {
      int _tmp[] = {14};
      PricesCard.set("columnWidths", _tmp);
    }
    PricesCard.set("layoutName", "Prices");
    PricesCard.set("visible", Boolean.FALSE);
    {
      double _tmp[] = {0.0,1.0};
      PricesCard.set("rowWeights", _tmp);
    }
    {
      double _tmp[] = {1.0};
      PricesCard.set("columnWeights", _tmp);
    }

    Prices = new OwnRadioButtonPanelShadow();
    Prices.set("name", "Prices");
    PricesCard.add(Prices);
    {
      int _tmp[] = {14,14,14,14,14,14,14};
      Prices.set("rowHeights", _tmp);
    }
    Prices.set("GBConstraints", new GBConstraints("x=0;y=0;fill=both"));
    {
      int _tmp[] = {14,14,14,14};
      Prices.set("columnWidths", _tmp);
    }
    {
      double _tmp[] = {0.0,0.0,0.0,0.0,0.0,0.0,0.0};
      Prices.set("rowWeights", _tmp);
    }
    {
      double _tmp[] = {0.0,1.0,1.0,1.0};
      Prices.set("columnWeights", _tmp);
    }

    PricesLabel = new LabelBarShadow();
    PricesLabel.set("name", "PricesLabel");
    Prices.add(PricesLabel);
    PricesLabel.set("GBConstraints", new GBConstraints("x=0;y=0;width=4;fill=horizontal"));
    PricesLabel.set("text", "Prices");

    Statistics = new LabelShadow();
    Statistics.set("name", "Statistics");
    Prices.add(Statistics);
    Statistics.set("GBConstraints", new GBConstraints("x=0;y=1"));
    Statistics.set("text", "Statistics:");

    Pricing = new LabelShadow();
    Pricing.set("name", "Pricing");
    Prices.add(Pricing);
    Pricing.set("GBConstraints", new GBConstraints("x=0;y=2"));
    Pricing.set("text", "Pricing:");

    PriceClassLabel = new LabelShadow();
    PriceClassLabel.set("name", "PriceClassLabel");
    Prices.add(PriceClassLabel);
    PriceClassLabel.set("GBConstraints", new GBConstraints("x=0;y=4"));
    PriceClassLabel.set("text", "Price class:");

    BonusClassLabel = new LabelShadow();
    BonusClassLabel.set("name", "BonusClassLabel");
    Prices.add(BonusClassLabel);
    BonusClassLabel.set("GBConstraints", new GBConstraints("x=0;y=5"));
    BonusClassLabel.set("text", "Bonus class:");

    PriorityLabel = new LabelShadow();
    PriorityLabel.set("name", "PriorityLabel");
    Prices.add(PriorityLabel);
    PriorityLabel.set("GBConstraints", new GBConstraints("x=0;y=6"));
    PriorityLabel.set("text", "Priority:");

    PriceFixer = new LabelShadow();
    PriceFixer.set("name", "PriceFixer");
    Prices.add(PriceFixer);
    PriceFixer.set("GBConstraints", new GBConstraints("x=0;y=3"));
    PriceFixer.set("text", "Price fixer:");

    StatisticsNeeded = new CheckboxShadow();
    StatisticsNeeded.set("name", "StatisticsNeeded");
    Prices.add(StatisticsNeeded);
    StatisticsNeeded.set("GBConstraints", new GBConstraints("x=1;y=1"));
    StatisticsNeeded.set("text", "Needed");
    StatisticsNeeded.set("state", Boolean.TRUE);
    StatisticsNeeded.set("anchor", new sunsoft.jws.visual.rt.type.AnchorEnum("west"));

    NoStatistics = new CheckboxShadow();
    NoStatistics.set("name", "NoStatistics");
    Prices.add(NoStatistics);
    NoStatistics.set("GBConstraints", new GBConstraints("x=2;y=1"));
    NoStatistics.set("text", "No statistics");
    NoStatistics.set("anchor", new sunsoft.jws.visual.rt.type.AnchorEnum("west"));

    SubscriptionBased = new CheckboxShadow();
    SubscriptionBased.set("name", "SubscriptionBased");
    Prices.add(SubscriptionBased);
    SubscriptionBased.set("GBConstraints", new GBConstraints("x=1;y=2"));
    SubscriptionBased.set("text", "Subscription based");
    SubscriptionBased.set("state", Boolean.TRUE);
    SubscriptionBased.set("anchor", new sunsoft.jws.visual.rt.type.AnchorEnum("west"));

    ItemByItem = new CheckboxShadow();
    ItemByItem.set("name", "ItemByItem");
    Prices.add(ItemByItem);
    ItemByItem.set("GBConstraints", new GBConstraints("x=2;y=2"));
    ItemByItem.set("text", "Item by item");
    ItemByItem.set("anchor", new sunsoft.jws.visual.rt.type.AnchorEnum("west"));

    Both = new CheckboxShadow();
    Both.set("name", "Both");
    Prices.add(Both);
    Both.set("GBConstraints", new GBConstraints("x=3;y=2"));
    Both.set("text", "Both");
    Both.set("anchor", new sunsoft.jws.visual.rt.type.AnchorEnum("west"));

    CPandSP = new CheckboxShadow();
    CPandSP.set("name", "CPandSP");
    Prices.add(CPandSP);
    CPandSP.set("GBConstraints", new GBConstraints("x=1;y=3"));
    CPandSP.set("text", "CP and SP");
    CPandSP.set("state", Boolean.TRUE);
    CPandSP.set("anchor", new sunsoft.jws.visual.rt.type.AnchorEnum("west"));

    SPAlone = new CheckboxShadow();
    SPAlone.set("name", "SPAlone");
    Prices.add(SPAlone);
    SPAlone.set("GBConstraints", new GBConstraints("x=2;y=3"));
    SPAlone.set("text", "SP alone");
    SPAlone.set("anchor", new sunsoft.jws.visual.rt.type.AnchorEnum("west"));

    CPAlone = new CheckboxShadow();
    CPAlone.set("name", "CPAlone");
    Prices.add(CPAlone);
    CPAlone.set("GBConstraints", new GBConstraints("x=3;y=3"));
    CPAlone.set("text", "CP alone");
    CPAlone.set("anchor", new sunsoft.jws.visual.rt.type.AnchorEnum("west"));

    PriceClass = new ChoiceShadow();
    PriceClass.set("name", "PriceClass");
    Prices.add(PriceClass);
    PriceClass.set("items", convert("[Ljava.lang.String;", "1,2"));
    PriceClass.set("GBConstraints", new GBConstraints("x=1;y=4"));
    PriceClass.set("anchor", new sunsoft.jws.visual.rt.type.AnchorEnum("west"));
    PriceClass.set("selectedItem", "1");

    BonusClass = new ChoiceShadow();
    BonusClass.set("name", "BonusClass");
    Prices.add(BonusClass);
    BonusClass.set("items", convert("[Ljava.lang.String;", "1,2"));
    BonusClass.set("GBConstraints", new GBConstraints("x=1;y=5"));
    BonusClass.set("anchor", new sunsoft.jws.visual.rt.type.AnchorEnum("west"));
    BonusClass.set("selectedItem", "1");

    Priority = new ChoiceShadow();
    Priority.set("name", "Priority");
    Prices.add(Priority);
    Priority.set("items", convert("[Ljava.lang.String;", "1,2,3,4,5,6,7,8,9,10"));
    Priority.set("GBConstraints", new GBConstraints("x=1;y=6"));
    Priority.set("anchor", new sunsoft.jws.visual.rt.type.AnchorEnum("west"));
    Priority.set("selectedItem", "1");

    CampaignDatesCard = new GBPanelShadow();
    CampaignDatesCard.set("name", "CampaignDatesCard");
    MetadataTabbedFolder.add(CampaignDatesCard);
    {
      int _tmp[] = {14,14};
      CampaignDatesCard.set("rowHeights", _tmp);
    }
    {
      int _tmp[] = {14};
      CampaignDatesCard.set("columnWidths", _tmp);
    }
    CampaignDatesCard.set("layoutName", "Campaign Dates");
    CampaignDatesCard.set("visible", Boolean.FALSE);
    {
      double _tmp[] = {0.0,1.0};
      CampaignDatesCard.set("rowWeights", _tmp);
    }
    {
      double _tmp[] = {1.0};
      CampaignDatesCard.set("columnWeights", _tmp);
    }

    CampaignDates = new OwnRadioButtonPanelShadow();
    CampaignDates.set("name", "CampaignDates");
    CampaignDatesCard.add(CampaignDates);
    {
      int _tmp[] = {14,14,14};
      CampaignDates.set("rowHeights", _tmp);
    }
    CampaignDates.set("GBConstraints", new GBConstraints("x=0;y=0;fill=both"));
    {
      int _tmp[] = {14,14,14,14,14,14};
      CampaignDates.set("columnWidths", _tmp);
    }
    {
      double _tmp[] = {0.0,0.0,0.0};
      CampaignDates.set("rowWeights", _tmp);
    }
    {
      double _tmp[] = {0.0,1.0,0.0,1.0,0.0,1.0};
      CampaignDates.set("columnWeights", _tmp);
    }

    labelbar7 = new LabelBarShadow();
    labelbar7.set("name", "labelbar7");
    CampaignDates.add(labelbar7);
    labelbar7.set("GBConstraints", new GBConstraints("x=0;y=0;width=6;fill=horizontal"));
    labelbar7.set("text", "CampaignDates");

    CampaignPreviewDateLabel = new LabelShadow();
    CampaignPreviewDateLabel.set("name", "CampaignPreviewDateLabel");
    CampaignDates.add(CampaignPreviewDateLabel);
    CampaignPreviewDateLabel.set("GBConstraints", new GBConstraints("x=0;y=1"));
    CampaignPreviewDateLabel.set("text", "Preview date:");

    CampaignPreviewDate = new TextFieldShadow();
    CampaignPreviewDate.set("name", "CampaignPreviewDate");
    CampaignDates.add(CampaignPreviewDate);
    CampaignPreviewDate.set("GBConstraints", new GBConstraints("x=1;y=1;fill=horizontal"));

    CampaignDistributionDate = new TextFieldShadow();
    CampaignDistributionDate.set("name", "CampaignDistributionDate");
    CampaignDates.add(CampaignDistributionDate);
    CampaignDistributionDate.set("GBConstraints", new GBConstraints("x=3;y=1;fill=horizontal"));

    CampaignExpireDate = new TextFieldShadow();
    CampaignExpireDate.set("name", "CampaignExpireDate");
    CampaignDates.add(CampaignExpireDate);
    CampaignExpireDate.set("GBConstraints", new GBConstraints("x=5;y=1;fill=horizontal"));

    CampaignPreviewTime = new TextFieldShadow();
    CampaignPreviewTime.set("name", "CampaignPreviewTime");
    CampaignDates.add(CampaignPreviewTime);
    CampaignPreviewTime.set("GBConstraints", new GBConstraints("x=1;y=2;fill=horizontal"));

    CampaignDistributionTime = new TextFieldShadow();
    CampaignDistributionTime.set("name", "CampaignDistributionTime");
    CampaignDates.add(CampaignDistributionTime);
    CampaignDistributionTime.set("GBConstraints", new GBConstraints("x=3;y=2;fill=horizontal"));

    CampaignExpireTime = new TextFieldShadow();
    CampaignExpireTime.set("name", "CampaignExpireTime");
    CampaignDates.add(CampaignExpireTime);
    CampaignExpireTime.set("GBConstraints", new GBConstraints("x=5;y=2;fill=horizontal"));

    CampaignPreviewTimeLabel = new LabelShadow();
    CampaignPreviewTimeLabel.set("name", "CampaignPreviewTimeLabel");
    CampaignDates.add(CampaignPreviewTimeLabel);
    CampaignPreviewTimeLabel.set("GBConstraints", new GBConstraints("x=0;y=2"));
    CampaignPreviewTimeLabel.set("text", "Preview time:");

    CDistributionDateLabel = new LabelShadow();
    CDistributionDateLabel.set("name", "CDistributionDateLabel");
    CampaignDates.add(CDistributionDateLabel);
    CDistributionDateLabel.set("GBConstraints", new GBConstraints("x=2;y=1"));
    CDistributionDateLabel.set("text", "Distribution date:");

    CampaignDistributionTimeLabel = new LabelShadow();
    CampaignDistributionTimeLabel.set("name", "CampaignDistributionTimeLabel");
    CampaignDates.add(CampaignDistributionTimeLabel);
    CampaignDistributionTimeLabel.set("GBConstraints", new GBConstraints("x=2;y=2"));
    CampaignDistributionTimeLabel.set("text", "Distribution time:");

    CampaignExpireDateLabel = new LabelShadow();
    CampaignExpireDateLabel.set("name", "CampaignExpireDateLabel");
    CampaignDates.add(CampaignExpireDateLabel);
    CampaignExpireDateLabel.set("GBConstraints", new GBConstraints("x=4;y=1"));
    CampaignExpireDateLabel.set("text", "Expire date:");

    CampaignExpireTimeLabel = new LabelShadow();
    CampaignExpireTimeLabel.set("name", "CampaignExpireTimeLabel");
    CampaignDates.add(CampaignExpireTimeLabel);
    CampaignExpireTimeLabel.set("GBConstraints", new GBConstraints("x=4;y=2"));
    CampaignExpireTimeLabel.set("text", "Expire time:");

    SpecialCard = new GBPanelShadow();
    SpecialCard.set("name", "SpecialCard");
    MetadataTabbedFolder.add(SpecialCard);
    {
      int _tmp[] = {14,14};
      SpecialCard.set("rowHeights", _tmp);
    }
    {
      int _tmp[] = {14};
      SpecialCard.set("columnWidths", _tmp);
    }
    SpecialCard.set("layoutName", "Special");
    SpecialCard.set("visible", Boolean.FALSE);
    {
      double _tmp[] = {0.0,1.0};
      SpecialCard.set("rowWeights", _tmp);
    }
    {
      double _tmp[] = {1.0};
      SpecialCard.set("columnWeights", _tmp);
    }

    Special = new OwnRadioButtonPanelShadow();
    Special.set("name", "Special");
    SpecialCard.add(Special);
    {
      int _tmp[] = {14,14,14,14,14,14,14};
      Special.set("rowHeights", _tmp);
    }
    Special.set("GBConstraints", new GBConstraints("x=0;y=0;fill=both"));
    {
      int _tmp[] = {14,14,14,14,14,14};
      Special.set("columnWidths", _tmp);
    }
    {
      double _tmp[] = {0.0,0.0,0.0,0.0,0.0,0.0,0.0};
      Special.set("rowWeights", _tmp);
    }
    {
      double _tmp[] = {0.0,1.0,1.0,1.0,1.0,1.0};
      Special.set("columnWeights", _tmp);
    }

    Notification = new LabelShadow();
    Notification.set("name", "Notification");
    Special.add(Notification);
    Notification.set("GBConstraints", new GBConstraints("x=0;y=0"));
    Notification.set("text", "Notification:");

    TemplateType = new LabelShadow();
    TemplateType.set("name", "TemplateType");
    Special.add(TemplateType);
    TemplateType.set("GBConstraints", new GBConstraints("x=0;y=2"));
    TemplateType.set("text", "Template type:");

    LocationInfo = new LabelShadow();
    LocationInfo.set("name", "LocationInfo");
    Special.add(LocationInfo);
    LocationInfo.set("GBConstraints", new GBConstraints("x=0;y=1"));
    LocationInfo.set("text", "Location info:");

    TemplateFileLabel = new LabelShadow();
    TemplateFileLabel.set("name", "TemplateFileLabel");
    Special.add(TemplateFileLabel);
    TemplateFileLabel.set("GBConstraints", new GBConstraints("x=0;y=3"));
    TemplateFileLabel.set("text", "Template file:");

    Customisation = new LabelShadow();
    Customisation.set("name", "Customisation");
    Special.add(Customisation);
    Customisation.set("GBConstraints", new GBConstraints("x=0;y=4"));
    Customisation.set("text", "Customisation:");

    Layout1Label = new LabelShadow();
    Layout1Label.set("name", "Layout1Label");
    Special.add(Layout1Label);
    Layout1Label.set("GBConstraints", new GBConstraints("x=0;y=6"));
    Layout1Label.set("text", "Layout #1:");

    NotificationNeeded = new CheckboxShadow();
    NotificationNeeded.set("name", "NotificationNeeded");
    Special.add(NotificationNeeded);
    NotificationNeeded.set("GBConstraints", new GBConstraints("x=1;y=0"));
    NotificationNeeded.set("text", "Needed");
    NotificationNeeded.set("state", Boolean.TRUE);
    NotificationNeeded.set("anchor", new sunsoft.jws.visual.rt.type.AnchorEnum("west"));

    WhatsNew = new CheckboxShadow();
    WhatsNew.set("name", "WhatsNew");
    Special.add(WhatsNew);
    WhatsNew.set("GBConstraints", new GBConstraints("x=2;y=0"));
    WhatsNew.set("text", "What's new");
    WhatsNew.set("anchor", new sunsoft.jws.visual.rt.type.AnchorEnum("west"));

    NoNotification = new CheckboxShadow();
    NoNotification.set("name", "NoNotification");
    Special.add(NoNotification);
    NoNotification.set("GBConstraints", new GBConstraints("x=3;y=0"));
    NoNotification.set("text", "No notification");
    NoNotification.set("anchor", new sunsoft.jws.visual.rt.type.AnchorEnum("west"));

    Manual = new CheckboxShadow();
    Manual.set("name", "Manual");
    Special.add(Manual);
    Manual.set("GBConstraints", new GBConstraints("x=1;y=1"));
    Manual.set("text", "Manual");
    Manual.set("state", Boolean.TRUE);
    Manual.set("anchor", new sunsoft.jws.visual.rt.type.AnchorEnum("west"));

    Automatic = new CheckboxShadow();
    Automatic.set("name", "Automatic");
    Special.add(Automatic);
    Automatic.set("GBConstraints", new GBConstraints("x=2;y=1"));
    Automatic.set("text", "Automatic");
    Automatic.set("anchor", new sunsoft.jws.visual.rt.type.AnchorEnum("west"));

    Normal = new CheckboxShadow();
    Normal.set("name", "Normal");
    Special.add(Normal);
    Normal.set("GBConstraints", new GBConstraints("x=1;y=2"));
    Normal.set("text", "Normal");
    Normal.set("state", Boolean.TRUE);
    Normal.set("anchor", new sunsoft.jws.visual.rt.type.AnchorEnum("west"));

    Advanced = new CheckboxShadow();
    Advanced.set("name", "Advanced");
    Special.add(Advanced);
    Advanced.set("GBConstraints", new GBConstraints("x=2;y=2"));
    Advanced.set("text", "Advanced");
    Advanced.set("anchor", new sunsoft.jws.visual.rt.type.AnchorEnum("west"));

    NoTemplate = new CheckboxShadow();
    NoTemplate.set("name", "NoTemplate");
    Special.add(NoTemplate);
    NoTemplate.set("GBConstraints", new GBConstraints("x=3;y=2"));
    NoTemplate.set("text", "No template");
    NoTemplate.set("anchor", new sunsoft.jws.visual.rt.type.AnchorEnum("west"));

    User = new CheckboxShadow();
    User.set("name", "User");
    Special.add(User);
    User.set("GBConstraints", new GBConstraints("x=1;y=4"));
    User.set("text", "User");
    User.set("state", Boolean.TRUE);
    User.set("anchor", new sunsoft.jws.visual.rt.type.AnchorEnum("west"));

    SP = new CheckboxShadow();
    SP.set("name", "SP");
    Special.add(SP);
    SP.set("GBConstraints", new GBConstraints("x=2;y=4"));
    SP.set("text", "SP");
    SP.set("anchor", new sunsoft.jws.visual.rt.type.AnchorEnum("west"));

    NumberOfPagesLabel = new LabelBarShadow();
    NumberOfPagesLabel.set("name", "NumberOfPagesLabel");
    Special.add(NumberOfPagesLabel);
    NumberOfPagesLabel.set("GBConstraints", new GBConstraints("x=0;y=5;width=6;fill=horizontal"));
    NumberOfPagesLabel.set("text", "Number of pages");

    Layout2Label = new LabelShadow();
    Layout2Label.set("name", "Layout2Label");
    Special.add(Layout2Label);
    Layout2Label.set("GBConstraints", new GBConstraints("x=2;y=6"));
    Layout2Label.set("text", "Layout #2:");

    Layout3Label = new LabelShadow();
    Layout3Label.set("name", "Layout3Label");
    Special.add(Layout3Label);
    Layout3Label.set("GBConstraints", new GBConstraints("x=4;y=6"));
    Layout3Label.set("text", "Layout #3:");

    Layout1 = new TextFieldShadow();
    Layout1.set("name", "Layout1");
    Special.add(Layout1);
    Layout1.set("GBConstraints", new GBConstraints("x=1;y=6;fill=horizontal"));

    Layout2 = new TextFieldShadow();
    Layout2.set("name", "Layout2");
    Special.add(Layout2);
    Layout2.set("GBConstraints", new GBConstraints("x=3;y=6;fill=horizontal"));

    Layout3 = new TextFieldShadow();
    Layout3.set("name", "Layout3");
    Special.add(Layout3);
    Layout3.set("GBConstraints", new GBConstraints("x=5;y=6;fill=horizontal"));

    TemplateFile = new TextFieldShadow();
    TemplateFile.set("name", "TemplateFile");
    Special.add(TemplateFile);
    TemplateFile.set("GBConstraints", new GBConstraints("x=1;y=3;width=5;fill=horizontal"));

    Message = new TextAreaShadow();
    Message.set("name", "Message");
    gbpanel2.add(Message);
    Message.set("GBConstraints", new GBConstraints("x=0;y=2;fill=both"));
    Message.set("numRows", new Integer(3));
    Message.set("editable", Boolean.FALSE);

    MetadataMenuBar = new MenuBarShadow();
    MetadataMenuBar.set("name", "MetadataMenuBar");
    MetadataFrame.add(MetadataMenuBar);
    MetadataMenuBar.set("font", convert("java.awt.Font", "name=Dialog;style=bold;size=12"));

    FileMenu = new MenuShadow();
    FileMenu.set("name", "FileMenu");
    MetadataMenuBar.add(FileMenu);
    FileMenu.set("text", "File");
    FileMenu.set("font", convert("java.awt.Font", "name=Dialog;style=bold;size=12"));
    FileMenu.set("canTearOff", Boolean.FALSE);

    LoadItem = new MenuItemShadow();
    LoadItem.set("name", "LoadItem");
    FileMenu.add(LoadItem);
    LoadItem.set("text", "Load...");
    LoadItem.set("font", convert("java.awt.Font", "name=Dialog;style=bold;size=12"));

    SaveItem = new MenuItemShadow();
    SaveItem.set("name", "SaveItem");
    FileMenu.add(SaveItem);
    SaveItem.set("text", "Save...");
    SaveItem.set("font", convert("java.awt.Font", "name=Dialog;style=bold;size=12"));

    ClearItem = new MenuItemShadow();
    ClearItem.set("name", "ClearItem");
    FileMenu.add(ClearItem);
    ClearItem.set("text", "Clear");
    ClearItem.set("font", convert("java.awt.Font", "name=Dialog;style=bold;size=12"));

    ExitItem = new MenuItemShadow();
    ExitItem.set("name", "ExitItem");
    FileMenu.add(ExitItem);
    ExitItem.set("text", "Exit");
    ExitItem.set("font", convert("java.awt.Font", "name=Dialog;style=bold;size=12"));

    ToolsMenu = new MenuShadow();
    ToolsMenu.set("name", "ToolsMenu");
    MetadataMenuBar.add(ToolsMenu);
    ToolsMenu.set("text", "Tools");
    ToolsMenu.set("font", convert("java.awt.Font", "name=Dialog;style=bold;size=12"));
    ToolsMenu.set("canTearOff", Boolean.FALSE);

    ExtractFilesItem = new MenuItemShadow();
    ExtractFilesItem.set("name", "ExtractFilesItem");
    ToolsMenu.add(ExtractFilesItem);
    ExtractFilesItem.set("text", "Extract files...");
    ExtractFilesItem.set("font", convert("java.awt.Font", "name=Dialog;style=bold;size=12"));

    SaveAndSendItem = new MenuItemShadow();
    SaveAndSendItem.set("name", "SaveAndSendItem");
    ToolsMenu.add(SaveAndSendItem);
    SaveAndSendItem.set("text", "Save and Send...");
    SaveAndSendItem.set("font", convert("java.awt.Font", "name=Dialog;style=bold;size=12"));

    CreatePackageItem = new MenuItemShadow();
    CreatePackageItem.set("name", "CreatePackageItem");
    ToolsMenu.add(CreatePackageItem);
    CreatePackageItem.set("text", "Create package...");
    CreatePackageItem.set("font", convert("java.awt.Font", "name=Dialog;style=bold;size=12"));

    DeliverPackageItem = new MenuItemShadow();
    DeliverPackageItem.set("name", "DeliverPackageItem");
    ToolsMenu.add(DeliverPackageItem);
    DeliverPackageItem.set("text", "Deliver package...");
    DeliverPackageItem.set("font", convert("java.awt.Font", "name=Dialog;style=bold;size=12"));

    LoadFileDialog = new FileDialogShadow();
    LoadFileDialog.set("name", "LoadFileDialog");
    add(LoadFileDialog);
    LoadFileDialog.set("title", "Load");
    LoadFileDialog.set("layoutSize", new java.awt.Dimension(0, 0));
    LoadFileDialog.set("layoutLocation", new java.awt.Point(0, 0));

    SaveFileDialog = new FileDialogShadow();
    SaveFileDialog.set("name", "SaveFileDialog");
    add(SaveFileDialog);
    SaveFileDialog.set("title", "Save");
    SaveFileDialog.set("layoutSize", new java.awt.Dimension(0, 0));
    SaveFileDialog.set("mode", new sunsoft.jws.visual.rt.type.ModeEnum("save"));
    SaveFileDialog.set("layoutLocation", new java.awt.Point(0, 0));
  }

  // methods from lib/visual/gen/methods.java

  /**
   * Converts a string to the specified type.
   */
  private Object convert(String type, String value) {
    return(Converter.getConverter(type).convertFromString(value));
  }
}
