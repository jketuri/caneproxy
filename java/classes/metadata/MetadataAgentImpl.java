
import FI.ncscst.*;
import FI.ncscst.common.*;
import java.io.*;
import java.rmi.*;
import java.rmi.server.*;
import java.util.*;
import javax.servlet.*;

public class MetadataAgentImpl extends UnicastRemoteObject implements MetadataAgent, Servlet
{
	String documentBase;
	OutputStream logStream;

	private String name;
	private ServletConfig config;

public MetadataAgentImpl() throws RemoteException
{
}

public void init(ServletConfig config) throws ServletException
{
	this.config = config;
	documentBase = config.getInitParameter("documentBase");
	if (documentBase == null) documentBase = "";
	if (!documentBase.equals("") && !documentBase.endsWith("/")) documentBase += "/";
	try
	{
		logStream = new BufferedOutputStream(new FileOutputStream("/metadata_agent.log", true));
		if ((name = config.getInitParameter("bindName")) == null) name = "rmi:" + getClass().getName();
		Naming.rebind(name, this);
	}
	catch (Exception ex)
	{
		throw new ServletException(ex.toString());
	}
}

public ServletConfig getServletConfig()
{
	return config;
}


public void service(ServletRequest req, ServletResponse res)
{
}

public String getServletInfo()
{
	return getClass().getName();
}

public void destroy()
{
	try
	{
		Naming.unbind(name);
	}
	catch (Exception ex) {}
}

/** Extract named service package.
@param name Name of service package.
@param absPath Indicates that name is absolute path.
@return Returns true if extracted successfully. */
public boolean extract(String name, boolean absPath) throws RemoteException
{
	if (!absPath) name = documentBase + name;
	File f = new File(name);
	Properties r;
	try
	{
		BufferedInputStream in = new BufferedInputStream(new FileInputStream(name));
		try
		{
			r = CustomMetadata.load(in, true, documentBase + "services" + "/");
		}
		finally
		{
			in.close();
		}
		DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(name)));
		try
		{
			CustomMetadata.save(out, false, r);
		}
		finally
		{
			out.close();
		}
		if (logStream != null)
		synchronized (logStream)
		{
			try
			{
				Support.writeBytes(logStream, "Service package " + name + " extracted at " + new Date() + "\n");
			}
			catch (IOException ex)
			{
				logStream = null;
			}
		}
		return true;
	}
	catch (Exception ex)
	{
		f.delete();
		ex.printStackTrace();
		if (logStream != null)
		synchronized (logStream)
		{
			try
			{
				Support.writeBytes(logStream, "Exception occured when extracting service package " + name + " at " + new Date() + "\n" +
				Support.stackTrace(ex));
			}
			catch (IOException ex1)
			{
				logStream = null;
			}
		}
		throw new RemoteException(ex.toString());
	}
}

public static void main(String args[]) throws Exception
{
	if (args.length == 0)
	{
		System.out.println("Give package name and optionally document base directory (defaults to /home)");
		return;
	}
	MetadataAgentImpl c = new MetadataAgentImpl();
	c.documentBase = args.length > 1 ? args[1] : "/home";
	c.extract(args[0], true);
}

}
