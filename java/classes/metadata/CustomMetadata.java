
import FI.ncscst.*;
import FI.ncscst.common.*;
import java.awt.*;
import java.io.*;
import java.net.*;
import java.rmi.*;
import java.util.*;
import sunsoft.jws.visual.rt.awt.*;
import sunsoft.jws.visual.rt.base.*;
import sunsoft.jws.visual.rt.type.*;
import sunsoft.jws.visual.rt.shadow.*;
import sunsoft.jws.visual.rt.shadow.java.awt.*;

class CustomMetadataException extends Exception
{

public CustomMetadataException(String s)
{
	super(s);
}

}

public class CustomMetadata
{
	static final String propertiesName = "/metadata.properties";

	static int mediaType, description, previewDate, previewTime, distributionDate, distributionTime,
	expireDate, expireTime, itemUsage, copyright, sellingPrice, purchasePrice, VATClass, VATAmount,
	pageID, pageLayout, path, number;

	static
	{
		int i = 0;
		mediaType = i++;
		description = i++;
		sellingPrice = i++;
		purchasePrice = i++;
		VATClass = i++;
		VATAmount = i++;
		previewDate = i++;
		previewTime = i++;
		distributionDate = i++;
		distributionTime = i++;
		expireDate = i++;
		expireTime = i++;
		itemUsage = i++;
		copyright = i++;
		pageLayout = i++;
		pageID = i++;
		path = i++;
		number = i++;
	}

	String host;
	Metadata md;
	MetadataRoot gui;
	OutputStream logStream;
	Hashtable descriptions = new Hashtable();
	Vector ads = new Vector();
	boolean fileSys = false, touched = false;
	int port, row = -1;

public static final String getTitle(LabelShadow ls)
{
	String t = (String)ls.get("text");
	return t.substring(0, t.length() - 1);
}

public static String takeDate(String v, String title) throws CustomMetadataException
{
	try
	{
		return v.trim().equals("") ? "" : java.sql.Date.valueOf(v).toString();
	}
	catch (Exception ex)
	{
		throw new CustomMetadataException(title + " must be in yyyy-mm-dd format");
	}
}

public static String takeTime(String v, String title) throws CustomMetadataException
{
	try
	{
		return v.trim().equals("") ? "" : java.sql.Time.valueOf(v).toString();
	}
	catch (Exception ex)
	{
		throw new CustomMetadataException(title + " must be in hh:mm format");
	}
}

public static String getDate(TextFieldShadow tfs, LabelShadow ls) throws CustomMetadataException
{
	String s = (String)tfs.get("text");
	if (s.trim().equals("")) return "";
	try
	{
		return java.sql.Date.valueOf(s).toString();
	}
	catch (Exception ex)
	{
		throw new CustomMetadataException(getTitle(ls) + " must be in yyyy-mm-dd format");
	}
}

public static String getTime(TextFieldShadow tfs, LabelShadow ls) throws CustomMetadataException
{
	String s = (String)tfs.get("text");
	if (s.trim().equals("")) return "";
	try
	{
		return java.sql.Time.valueOf(s).toString();
	}
	catch (Exception ex)
	{
		throw new CustomMetadataException(getTitle(ls) + " must be in hh:mm format");
	}
}

public static final String getValue(Properties r, String s)
{
	return (s = (String)r.get(s)) != null ? s : "";
}

public static long save(DataOutputStream dout, boolean a, Properties r) throws Exception
{
	dout.writeBytes("ContentProvider=" + getValue(r, "ContentProvider") +
	"\nCategory=" + getValue(r, "Category") +
	"\nServiceName=" + getValue(r, "ServiceName") +
	"\n\nDescription=\n");
	StringTokenizer st = new StringTokenizer(getValue(r, "Description"), "\n");
	while (st.hasMoreTokens()) dout.writeBytes(st.nextToken() + "\n");
	dout.writeBytes("\nContentFilenames=\n");
	Vector m = (Vector)r.get("ContentFiles");
	if (m != null)
	{
		Enumeration e = m.elements();
		while (e.hasMoreElements()) dout.writeBytes(((Object[])e.nextElement())[0] + "\n");
	}
	dout.writeBytes(
/*
	"\nKeywords=" + getValue(r, "Keywords") +
*/
	"\nContentLanguage=" + getValue(r, "ContentLanguage") +
/*
	"\nContentUsage=" + getValue(r, "ContentUsage") +
	"\nCopyrightsOwner=" + getValue(r, "CopyrightsOwner") +
*/
	"\nCreationDate=" + getValue(r, "CreationDate") +
	"\nReleasePreviewDate=" + getValue(r, "ReleasePreviewDate") +
	"\nReleasePreviewTime=" + getValue(r, "ReleasePreviewTime") +
	"\nReleaseDistributionDate=" + getValue(r, "ReleaseDistributionDate") +
	"\nReleaseDistributionTime=" + getValue(r, "ReleaseDistributionTime") +
	"\nReleaseExpireDate=" + getValue(r, "ReleaseExpireDate") +
	"\nReleaseExpireTime=" + getValue(r, "ReleaseExpireTime") +
/*
	"\nCampaignPreviewDate=" + getValue(r, "CampaignPreviewDate") +
	"\nCampaignPreviewTime=" + getValue(r, "CampaignPreviewTime") +
	"\nCampaignDistributionDate=" + getValue(r, "CampaignDistributionDate") +
	"\nCampaignDistributionTime=" + getValue(r, "CampaignDistributionTime") +
	"\nCampaignExpireDate=" + getValue(r, "CampaignExpireDate") +
	"\nCampaignExpireTime=" + getValue(r, "CampaignExpireTime") +
	"\nStatistics=" + getValue(r, "Statistics") +
	"\nPricing=" + getValue(r, "Pricing") +
	"\nPriceFixer=" + getValue(r, "PriceFixer") +
	"\nPriceClass=" + getValue(r, "PriceClass") +
	"\nBonusClass=" + getValue(r, "BonusClass") +
	"\nPriority=" + getValue(r, "Priority") +
	"\nNotification=" + getValue(r, "Notification") +
	"\nLocation=" + getValue(r, "Location") +
	"\nTemplateType=" + getValue(r, "TemplateType") +
	"\nCustomisation=" + getValue(r, "Customisation") +
	"\nLayout1=" + getValue(r, "Layout1") +
	"\nLayout2=" + getValue(r, "Layout2") +
	"\nLayout3=" + getValue(r, "Layout3") +
*/
	"\n\nContentFiles=\n");
	long l = 0;
	if (m != null)
	{
		Enumeration e = m.elements();
		while (e.hasMoreElements())
		{
			String sa[] = (String[])((Object[])e.nextElement())[1];
			dout.writeBytes("MediaType=" + sa[mediaType] +
			"\nDescription=" + sa[description] + "\n");
			File f = new File(sa[path]);
			if (!f.exists()) throw new CustomMetadataException("File " + f.getPath() + " doesn't exist");
			String s = "Object=\n" + f.length() + "\n";
			l += s.length() + f.length();
			if (a)
			{
				dout.writeBytes(s);
				BufferedInputStream in = new BufferedInputStream(new FileInputStream(f));
				try
				{
					int n;
					byte b[] = new byte[Support.bufferLength];
					while ((n = in.read(b)) > 0) dout.write(b, 0, n);
				}
				finally
				{
					in.close();
				}
			}
			dout.writeBytes("PreviewDate=" + sa[previewDate] +
			"\nPreviewTime=" + sa[previewTime] +
			"\nDistributionDate=" + sa[distributionDate] +
			"\nDistributionTime=" + sa[distributionTime] +
			"\nExpireDate=" + sa[expireDate] +
			"\nExpireTime=" + sa[expireTime] +
			"\nItemUsage=" + sa[itemUsage] +
			"\nCopyright=" + sa[copyright] +
/*
			"\nSellingPrice=" + sa[sellingPrice] +
			"\nPurchasePrice=" + sa[purchasePrice] +
			"\nVATClass=" + sa[VATClass] +
			"\nVATAmount=" + sa[VATAmount] +
			"\nPageID=" + sa[pageID] +
			"\nLayoutNumber=" + sa[pageLayout] +
*/
			"\nPath=" + sa[path] +
			"\n\n");
		}
	}
	return l;
}

public static Properties load(InputStream in, boolean a, String db) throws Exception
{
	int x;
	Vector m = new Vector();
	Properties r = new Properties();
	String s, v, cID = null, sID = null;
	while ((s = Support.readLine(in)) != null)
	{
		if ((x = s.indexOf('=')) == -1) continue;
		v = x + 1 < s.length() ? s.substring(x + 1) : "";
		s = s.substring(0, x);
		if (s.equals("ContentProvider")) r.put(s, cID = v);
		else if (s.equals("ServiceName")) r.put(s, sID = v);
		else if (s.equals("Description"))
		{
			String t;
			StringBuffer sb = new StringBuffer();
			while ((t = Support.readLine(in)) != null && !t.equals("")) sb.append(t + "\n");
			r.put(s, sb.toString());
		}
		else if (s.equals("ContentFilenames"))
		while ((s = Support.readLine(in)) != null && !s.equals(""))
		{
			Object oa[] = new Object[2];
			oa[0] = s;
			oa[1] = new String[number];
			m.addElement(oa);
		}
		else if (s.equals("ContentFiles"))
		{
			if (m.size() > 0) r.put(s, m);
			int i = 0;
			while ((s = Support.readLine(in)) != null)
			{
				if ((x = s.indexOf('=')) == -1)
				{
					i++;
					continue;
				}
				v = x + 1 < s.length() ? s.substring(x + 1) : "";
				s = s.substring(0, x);
				Object oa[] = (Object[])m.elementAt(i);
				String sa[] = (String[])oa[1];
				if (s.equals("LayoutNumber")) sa[pageLayout] = v;
				else if (s.equals("MediaType")) sa[mediaType] = v;
				else if (s.equals("Description")) sa[description] = v;
				else if (s.equals("Object"))
				{
					long l = Long.parseLong(Support.readLine(in));
					if (a)
					{
						File f = new File(db + cID + File.separator + sID +
						File.separator + (String)oa[0]);
						File h = new File(f.getParent());
						if (!h.exists()) h.mkdirs();
						FileOutputStream fout = new FileOutputStream(f);
						int n;
						byte b[] = new byte[Support.bufferLength];
						while (l > 0 &&
						(n = in.read(b, 0, (int)Math.min(b.length, l))) > 0)
						{
							fout.write(b, 0, n);
							l -= n;
						}
						fout.close();
					}
					else in.skip(l);
				}
				else if (s.equals("SellingPrice")) sa[sellingPrice] = v;
				else if (s.equals("PurchasePrice")) sa[purchasePrice] = v;
				else if (s.equals("VATClass")) sa[VATClass] = v;
				else if (s.equals("VATAmount")) sa[VATAmount] = v;
				else if (s.equals("PreviewDate"))
				sa[previewDate] = takeDate(v, "Preview date");
				else if (s.equals("PreviewTime"))
				sa[previewTime] = takeTime(v, "Preview time");
				else if (s.equals("DistributionDate"))
				sa[distributionDate] = takeDate(v, "Distribution date");
				else if (s.equals("DistributionTime"))
				sa[distributionTime] = takeTime(v, "Distribution time");
				else if (s.equals("ExpireDate"))
				sa[expireDate] = takeDate(v, "Expire date");
				else if (s.equals("ExpireTime"))
				sa[expireTime] = takeTime(v, "Expire time");
				else if (s.equals("ItemUsage")) sa[itemUsage] = v;
				else if (s.equals("Copyright")) sa[copyright] = v;
				else if (s.equals("Path")) sa[path] = v;
			}
		}
		else r.put(s, v);
	}
	return r;
}

InputStream takeInput(String name) throws IOException
{
	if (fileSys)
	{
		File f = new File(name);
		if (f.exists()) return new FileInputStream(f);
	}
	else
	{
		OwnHttpURLConnection uc = new OwnHttpURLConnection(new URL("http", host, port, name));
		setConnection(uc);
		InputStream in = uc.getInputStream();
		if (uc.getStatus() == 200) return in;
	}
	return null;
}

public CustomMetadata(Metadata md, MetadataRoot gui)
{
	try
	{
		this.md = md;
		this.gui = gui;
		gui.ContentFiles.createBody();
		gui.LoadFileDialog.createBody();
		gui.SaveFileDialog.createBody();
		gui.ToolsMenu.remove(gui.SaveAndSendItem);
		gui.MetadataTabbedFolder.remove(gui.CampaignDatesCard);
		gui.MetadataTabbedFolder.remove(gui.PricesCard);
		gui.MetadataTabbedFolder.remove(gui.SpecialCard);
		gui.MetadataTabbedFolder.remove(gui.PreferencesCard);
		gui.ServiceDescription.remove(gui.KeywordsLabel);
		gui.ServiceDescription.remove(gui.Keywords);
		gui.ServiceDescription.remove(gui.UsageLabel);
		gui.ServiceDescription.remove(gui.ContentUsage);
		gui.ServiceDescription.remove(gui.Free);
		gui.ServiceDescription.remove(gui.Restricted);
		gui.ServiceDescription.remove(gui.CopyrightsOwnerLabel);
		gui.ServiceDescription.remove(gui.CopyrightsOwner);
		gui.ItemFields.remove(gui.SellingPriceLabel);
		gui.ItemFields.remove(gui.SellingPrice);
		gui.ItemFields.remove(gui.PurchasePriceLabel);
		gui.ItemFields.remove(gui.PurchasePrice);
		gui.ItemFields.remove(gui.VATClassLabel);
		gui.ItemFields.remove(gui.VATClass);
		gui.ItemFields.remove(gui.VATAmountLabel);
		gui.ItemFields.remove(gui.VATAmount);
		gui.ItemFields.remove(gui.PageLayoutLabel);
		gui.ItemFields.remove(gui.PageLayout);
		gui.ItemFields.remove(gui.PageIDLabel);
		gui.ItemFields.remove(gui.PageID);
		if (System.getProperty("home") == null) logStream = System.out;
		else logStream = new BufferedOutputStream(new FileOutputStream(
		System.getProperty("home") + "/metadata.log", true));
		if (md.getApplet() != null)
		{
			URL u = md.getApplet().getCodeBase();
			host = u.getHost();
			port = u.getPort();
			InetAddress ia = InetAddress.getByName(host);
			if (ia.equals(InetAddress.getLocalHost()) || ia.getHostAddress().equals("127.0.0.1"))
			fileSys = true;
		}
		else
		{
			gui.remove(gui.MetadataPanel);
			String a[] = md.getCmdLineArgs();
			if (a.length > 0)
			{
				host = a[0];
				port = a.length > 1 ? Integer.parseInt(a[1]) : -1;
			}
			else fileSys = true;
		}
		File f = new File(propertiesName);
		if (f.exists())
		{
			Properties r = new Properties();
			r.load(new FileInputStream(f));
			Enumeration e = r.elements(), k = r.keys();
			while (k.hasMoreElements())
			{
				String s = (String)k.nextElement(), v = (String)e.nextElement();
				TextFieldShadow tfs = (TextFieldShadow)gui.resolve(s);
				if (tfs != null) tfs.set("text", v);
			}
		}
		f = new File("Metadata.mtd");
		if (f.exists())
		{
			BufferedInputStream in = new BufferedInputStream(new FileInputStream(f));
			try
			{
				loadContent(in, false);
			}
			finally
			{
				in.close();
			}
			gui.PreviewDate.set("text", gui.ReleasePreviewDate.get("text"));
			gui.PreviewTime.set("text", gui.ReleasePreviewTime.get("text"));
			gui.DistributionDate.set("text", gui.ReleaseDistributionDate.get("text"));
			gui.DistributionTime.set("text", gui.ReleaseDistributionTime.get("text"));
			gui.ExpireDate.set("text", gui.ReleaseExpireDate.get("text"));
			gui.ExpireTime.set("text", gui.ReleaseExpireTime.get("text"));
		}
		else
		{
			Date t = new Date();
			String dt = new java.sql.Date(t.getTime()).toString(),
			tm = new java.sql.Time(t.getTime()).toString();
			gui.CreationDate.set("text", dt);
			gui.ReleasePreviewDate.set("text", dt);
			gui.ReleasePreviewTime.set("text", tm);
			gui.CampaignPreviewDate.set("text", dt);
			gui.CampaignPreviewTime.set("text", tm);
			gui.PreviewDate.set("text", dt);
			gui.PreviewTime.set("text", tm);
			gui.Layout1.set("text", "1");
			gui.Layout2.set("text", "1");
			gui.Layout3.set("text", "1");
		}
		InputStream in;
		if ((in = takeInput("Metadata.var")) !=  null)
		try
		{
			Properties r = new Properties();
			r.load(in);
			Enumeration e = r.elements(), k = r.keys();
			while (k.hasMoreElements())
			{
				String s = (String)k.nextElement(), v = (String)e.nextElement();
				if (s.equals("EmailAddresses"))
				{
					StringTokenizer st = new StringTokenizer(v, ",");
					while (st.hasMoreElements()) ads.addElement(st.nextToken());
				}
				else
				{
					ChoiceShadow cs = (ChoiceShadow)gui.resolve(s);
					if (cs != null)
					{
						cs.set("items", new StringArrayConverter().convertFromString(v));
						cs.set("selectedItem", new StringTokenizer(v, ",").nextToken());
					}
				}
			}
		}
		finally
		{
			in.close();
		}
		else System.out.println("Metadata.var not found");
		if ((in = takeInput("Metadata.txt")) !=  null)
		try
		{
			Properties r = new Properties();
			r.load(in);
			Enumeration e = r.elements(), k = r.keys();
			while (k.hasMoreElements())
			{
				Shadow s = (Shadow)gui.resolve((String)k.nextElement());
				String v = (String)e.nextElement();
				s.createBody();
				descriptions.put(s.getBody(), v);
			}
		}
		finally
		{
			in.close();
		}
		else System.out.println("Metadata.txt not found");
	}
	catch (Exception ex)
	{
		ex.printStackTrace();
		System.exit(1);
	}
}

public void log(String msg)
{
	if (logStream != null)
	synchronized (logStream)
	{
		try
		{
			Support.writeBytes(logStream, msg + "\n");
		}
		catch (IOException ex)
		{
			logStream = null;
		}
	}
}

public void setConnection(OwnHttpURLConnection uc)
{
	String s;
	uc.setUseCaches(false);
	s = ((String)gui.Username.get("text")).trim();
	if (!s.equals(""))
	{
		s = new BASE64Encoder().encodeBuffer(s.getBytes());
		uc.setRequestProperty("Authorization", "Basic: " + s.substring(0, s.length() - 1));
	}
	s = ((String)gui.HTTPProxy.get("text")).trim();
	if (!s.equals(""))
	{
		OwnHttpURLConnection.useProxy = true;
		OwnHttpURLConnection.proxyPort = Integer.parseInt((String)gui.HTTPProxyPort.get("text"));
		OwnHttpURLConnection.proxyHost = s;
	}
	else OwnHttpURLConnection.useProxy = false;
}

public String getCheckboxes(ContainerShadow cs)
{
	boolean g = false;
	StringBuffer sb = new StringBuffer();
	Enumeration e = cs.getChildList();
	while (e.hasMoreElements())
	{
		AttributeManager am = (AttributeManager)e.nextElement();
		if (((Boolean)am.get("state")).booleanValue())
		{
			if (g) sb.append(',');
			sb.append(am.get("name"));
			g = true;
		}
	}
	return sb.toString();
}

public void resetCheckboxes(ContainerShadow cs)
{
	boolean g = true;
	Enumeration e = cs.getChildList();
	while (e.hasMoreElements())
	{
		AttributeManager am = (AttributeManager)e.nextElement();
		am.set("state", g ? Boolean.TRUE : Boolean.FALSE);
		g = false;
	}
}

public long saveContent(DataOutputStream dout, boolean a) throws Exception
{
	Properties r = new Properties();
	r.put("ContentProvider", gui.ContentProvider.get("selectedItem"));
	r.put("Category", gui.Category.get("selectedItem"));
	r.put("ServiceName", gui.ServiceName.get("text"));
	r.put("Description", gui.Description.get("text"));
	ColumnList cl = (ColumnList)gui.ContentFiles.getBody();
	Vector m = new Vector(cl.entries());
	for (int i = 0; i < cl.entries(); i++)
	{
		Object oa[] = new Object[2];
		oa[0] = cl.getItem(i, 0);
		oa[1] = cl.getItem(i, 1);
		m.addElement(oa);
	}
	r.put("ContentFiles", m);
/*
	r.put("Keywords", gui.Keywords.get("text"));
*/
	r.put("ContentLanguage", getCheckboxes(gui.ContentLanguage));
/*
	r.put("ContentUsage", (((Boolean)gui.Free.get("state")).booleanValue() ? "Free" : "Restricted"));
	r.put("CopyrightsOwner", gui.CopyrightsOwner.get("text"));
*/
	r.put("CreationDate", getDate(gui.CreationDate, gui.CreationDateLabel));
	r.put("ReleasePreviewDate", getDate(gui.ReleasePreviewDate, gui.ReleasePreviewDateLabel));
	r.put("ReleasePreviewTime", getTime(gui.ReleasePreviewTime, gui.ReleasePreviewTimeLabel));
	r.put("ReleaseDistributionDate", getDate(gui.ReleaseDistributionDate, gui.ReleaseDistributionDateLabel));
	r.put("ReleaseDistributionTime", getTime(gui.ReleaseDistributionTime, gui.ReleaseDistributionTimeLabel));
	r.put("ReleaseExpireDate", getDate(gui.ReleaseExpireDate, gui.ReleaseExpireDateLabel));
	r.put("ReleaseExpireTime", getTime(gui.ReleaseExpireTime, gui.ReleaseExpireTimeLabel));
/*
	r.put("CampaignPreviewDate", getDate(gui.CampaignPreviewDate, gui.CampaignPreviewDateLabel));
	r.put("CampaignPreviewTime", getTime(gui.CampaignPreviewTime, gui.CampaignPreviewTimeLabel));
	r.put("CampaignDistributionDate", getDate(gui.CampaignDistributionDate, gui.CampaignDistributionDateLabel));
	r.put("CampaignDistributionTime", getTime(gui.CampaignDistributionTime, gui.CampaignDistributionTimeLabel));
	r.put("CampaignExpireDate", getDate(gui.CampaignExpireDate, gui.CampaignExpireDateLabel));
	r.put("CampaignExpireTime", getTime(gui.CampaignExpireTime, gui.CampaignExpireTimeLabel));
	r.put("Statistics", (((Boolean)gui.StatisticsNeeded.get("state")).booleanValue() ?
	"Needed" : "No statistics"));
	r.put("Pricing", (((Boolean)gui.SubscriptionBased.get("state")).booleanValue() ?
	"Subscription based" : ((Boolean)gui.ItemByItem.get("state")).booleanValue() ?
	"Item by item" : "Both"));
	r.put("PriceFixer", (((Boolean)gui.CPandSP.get("state")).booleanValue() ?
	"CPandSP" : ((Boolean)gui.CPAlone.get("state")).booleanValue() ? "CP" : "SP"));
	r.put("PriceClass", gui.PriceClass.get("selectedItem"));
	r.put("BonusClass", gui.BonusClass.get("selectedItem"));
	r.put("Priority", gui.Priority.get("selectedItem"));
	r.put("Notification",
	(((Boolean)gui.NotificationNeeded.get("state")).booleanValue() ?
	"Needed" : ((Boolean)gui.WhatsNew.get("state")).booleanValue() ?
	"What's new" : "No notification"));
	r.put("Location",
	(((Boolean)gui.Manual.get("state")).booleanValue() ? "Manual" : "Automatic"));
	r.put("TemplateType",
	(((Boolean)gui.Normal.get("state")).booleanValue() ?
	"Normal" : ((Boolean)gui.Advanced.get("state")).booleanValue() ?
	"Advanced" : "No template"));
	r.put("Customisation",
	(((Boolean)gui.User.get("state")).booleanValue() ? "User" : "SP"));
	r.put("Layout1", String.valueOf(Integer.parseInt((String)gui.Layout1.get("text")));
	r.put("Layout2", String.valueOf(Integer.parseInt((String)gui.Layout2.get("text")));
	r.put("Layout3", String.valueOf(Integer.parseInt((String)gui.Layout3.get("text")));
*/
	return save(dout, a, r);
}

public void loadContent(InputStream in, boolean a) throws Exception
{
	String db = ((String)gui.DocumentBase.get("text")).trim();
	if (!db.equals("") && !db.endsWith("/")) db += "/";
	Properties p = load(in, a, db);
	Enumeration e = p.elements(), k = p.keys();
	while (k.hasMoreElements())
	{
		Shadow s = (Shadow)gui.resolve((String)k.nextElement());
		Object v = e.nextElement();
		if (s != null)
		if (s instanceof TextFieldShadow || s instanceof TextAreaShadow) s.set("text", (String)v);
		else if (s instanceof ChoiceShadow) s.set("selectedItem", (String)v);
		else if (s instanceof LabelShadow)
		{
			Shadow s1 = (Shadow)gui.resolve((String)v);
			if (s1 != null) s1.set("state", Boolean.TRUE);
		}
		else if (s instanceof PanelShadow)
		{
			Enumeration e1 = ((PanelShadow)s).getChildList();
			while (e1.hasMoreElements())
			((AttributeManager)e1.nextElement()).set("state", Boolean.FALSE);
			StringTokenizer st = new StringTokenizer((String)v, ",");
			while (st.hasMoreTokens())
			{
				Shadow s1 = (Shadow)gui.resolve(st.nextToken());
				if (s1 != null) s1.set("state", Boolean.TRUE);
			}
		}
		else if (s instanceof ColumnListShadow)
		{
			ColumnList cl = (ColumnList)s.getBody();
			if (cl == null) continue;
			cl.delItems();
			Enumeration e1 = ((Vector)v).elements();
			while (e1.hasMoreElements()) cl.addItem((Object[])e1.nextElement());
		}
	}
}

public boolean handleEvent(Message msg, Event evt)
{
	try
	{
		String currentCard = ((TabbedFolder)gui.MetadataTabbedFolder.getBody()).getCurrentCard();
		switch (evt.id) {
		case Event.MOUSE_MOVE:
			if (currentCard.equals("Service Description"))
			{
				getDate(gui.CreationDate, gui.CreationDateLabel);
				getDate(gui.ReleasePreviewDate, gui.ReleasePreviewDateLabel);
				getTime(gui.ReleasePreviewTime, gui.ReleasePreviewTimeLabel);
				getDate(gui.ReleaseDistributionDate, gui.ReleaseDistributionDateLabel);
				getTime(gui.ReleaseDistributionTime, gui.ReleaseDistributionTimeLabel);
				getDate(gui.ReleaseExpireDate, gui.ReleaseExpireDateLabel);
				getTime(gui.ReleaseExpireTime, gui.ReleaseExpireTimeLabel);
			}
			else if (currentCard.equals("Item Description"))
			{
				getDate(gui.PreviewDate, gui.PreviewDateLabel);
				getTime(gui.PreviewTime, gui.PreviewTimeLabel);
				getDate(gui.DistributionDate, gui.DistributionDateLabel);
				getTime(gui.DistributionTime, gui.DistributionTimeLabel);
				getDate(gui.ExpireDate, gui.ExpireDateLabel);
				getTime(gui.ExpireTime, gui.ExpireTimeLabel);
			}
/*
			else if (currentCard.equals("Campaign Dates"))
			{
				getDate(gui.CampaignPreviewDate, gui.CampaignPreviewDateLabel);
				getTime(gui.CampaignPreviewTime, gui.CampaignPreviewTimeLabel);
				getDate(gui.CampaignDistributionDate, gui.CampaignDistributionDateLabel);
				getTime(gui.CampaignDistributionTime, gui.CampaignDistributionTimeLabel);
				getDate(gui.CampaignExpireDate, gui.CampaignExpireDateLabel);
				getTime(gui.CampaignExpireTime, gui.CampaignExpireTimeLabel);
			}
*/
			if (evt.target instanceof Component)
			{
				Component c = (Component)evt.target;
				c = c.getComponentAt(evt.x, evt.y);
				if (c != null)
				{
					String s = (String)descriptions.get(c);
					if (s != null) gui.Message.set("text", s);
				}
			}
			return false;
		case Event.KEY_PRESS:
			touched = true;
			return false;
		case Event.ACTION_EVENT:
			gui.Message.set("text", "");
			if (((String)gui.ItemFilename.get("text")).trim().equals(""))
			{
				gui.PreviewDate.set("text", gui.ReleasePreviewDate.get("text"));
				gui.PreviewTime.set("text", gui.ReleasePreviewTime.get("text"));
				gui.DistributionDate.set("text", gui.ReleaseDistributionDate.get("text"));
				gui.DistributionTime.set("text", gui.ReleaseDistributionTime.get("text"));
				gui.ExpireDate.set("text", gui.ReleaseExpireDate.get("text"));
				gui.ExpireTime.set("text", gui.ReleaseExpireTime.get("text"));
			}
			if (msg.target == gui.MetadataButton)
			{
				gui.MetadataFrame.show();
				return true;
			}
			if (msg.target == gui.ApplyButton)
			{
				String name = ((String)gui.ItemFilename.get("text")).trim();
				if (name.equals("")) throw new CustomMetadataException("There's no filename");
				if (!new File(name).exists())
				throw new CustomMetadataException("File " + name + " doesn't exist");
				name = name.substring(Math.max(name.lastIndexOf('/'),
				name.lastIndexOf(File.separatorChar)) + 1);
				ColumnList cl = (ColumnList)gui.ContentFiles.getBody();
				int n = cl.entries();
				for (row = 0; row < n; row++)
				if (cl.getItem(row, 0).equals(name)) break;
				String sa[] = row == n ? new String[number] : (String[])cl.getItem(row, 1);
				sa[mediaType] = (String)gui.MediaType.get("selectedItem");
				sa[description] = (String)gui.FileDescription.get("text");
				sa[previewDate] = getDate(gui.PreviewDate, gui.PreviewDateLabel);
				sa[previewTime] = getTime(gui.PreviewTime, gui.PreviewTimeLabel);
				sa[distributionDate] = getDate(gui.DistributionDate, gui.DistributionDateLabel);
				sa[distributionTime] = getTime(gui.DistributionTime, gui.DistributionTimeLabel);
				sa[expireDate] = getDate(gui.ExpireDate, gui.ExpireDateLabel);
				sa[expireTime] = getTime(gui.ExpireTime, gui.ExpireTimeLabel);
				sa[itemUsage] = (String)gui.ItemUsage.get("selectedItem");
				sa[copyright] =  (String)gui.Copyright.get("text");
				sa[sellingPrice] = (String)gui.SellingPrice.get("text");
				sa[purchasePrice] = (String)gui.PurchasePrice.get("text");
				sa[VATClass] = (String)gui.VATClass.get("text");
				sa[VATAmount] = (String)gui.VATAmount.get("text");
				sa[pageID] = (String)gui.PageID.get("text");
				sa[pageLayout] = (String)gui.PageLayout.get("selectedItem");
				sa[path] = (String)gui.ItemFilename.get("text");
				if (row == n)
				{
					Object oa[] = new Object[2];
					oa[0] = name;
					oa[1] = sa;
					cl.addItem(oa);
					row = cl.entries() - 1;
				}
				cl.highlight(row);
				return true;
			}
			if (msg.target == gui.BrowseButton)
			{
				FileDialog fd = (FileDialog)gui.LoadFileDialog.getBody();
				fd.setFile("*.*");
				fd.show();
				if (fd.getFile() == null) return true;
				gui.ItemFilename.set("text", fd.getDirectory() + fd.getFile());
				msg.target = gui.ItemFilename;
			}
			if (msg.target == gui.RemoveButton)
			{
				if (row != -1)
				{
					ColumnList cl = (ColumnList)gui.ContentFiles.getBody();
					cl.delItems(row, row);
					if (row >= cl.entries()) row = -1;
				}
				return true;
			}
			if (msg.target == gui.LoadItem || msg.target == gui.ExtractFilesItem)
			{
				FileDialog fd = (FileDialog)gui.LoadFileDialog.getBody();
				fd.setFile("*.*");
				fd.show();
				if (fd.getFile() != null)
				{
					String fn = fd.getDirectory() + fd.getFile();
					BufferedInputStream in = new BufferedInputStream(new FileInputStream(fn));
					try
					{
						loadContent(in, msg.target == gui.ExtractFilesItem);
					}
					finally
					{
						in.close();
					}
					gui.Message.set("text", "Metadata loaded" +
					(msg.target == gui.ExtractFilesItem ? " and extracted" : ""));
					if (msg.target == gui.ExtractFilesItem) log("Service package " + fn + " extracted at " + new Date());
				}
				return true;
			}
			if (msg.target == gui.SaveItem || msg.target == gui.SaveAndSendItem ||
			msg.target == gui.CreatePackageItem || msg.target == gui.DeliverPackageItem)
			{
				String cID = (String)gui.ContentProvider.get("selectedItem"),
				sID = (String)gui.ServiceName.get("text");
				if (msg.target == gui.SaveAndSendItem || msg.target == gui.CreatePackageItem ||
				msg.target == gui.DeliverPackageItem)
				{
					if (((ColumnList)gui.ContentFiles.getBody()).entries() == 0)
					throw new CustomMetadataException("There aren't any content files");
					if (sID.equals("")) throw new CustomMetadataException("Give service name");
					if (((String)gui.ReleaseDistributionDate.get("text")).trim().equals(""))
					throw new CustomMetadataException("Give release distribution date");
				}
				FileDialog fd = (FileDialog)gui.SaveFileDialog.getBody();
				fd.setFile(sID.equals("") ? "Metadata.mtd" : msg.target == gui.CreatePackageItem ||
				msg.target == gui.DeliverPackageItem ? cID + "_" + sID + ".spg" : sID + ".mtd");
				fd.show();
				if (fd.getFile() != null)
				{
					long l;
					String fn;
					File f = new File(fn = fd.getDirectory() + fd.getFile());
					DataOutputStream dout = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(f)));
					try
					{
						l = saveContent(dout, msg.target == gui.CreatePackageItem || msg.target == gui.DeliverPackageItem);
					}
					finally
					{
						dout.close();
					}
					if (msg.target == gui.SaveItem || msg.target == gui.SaveAndSendItem)
					{
						l += f.length();
						touched = false;
					}
					else if (msg.target == gui.CreatePackageItem || msg.target == gui.DeliverPackageItem)
					log("Service package " + fn + " created at " + new Date());
					String name;
					if (msg.target == gui.SaveAndSendItem)
					{
						name = "incoming/" + cID + "_" + sID + ".spg";
						OwnHttpURLConnection uc = new OwnHttpURLConnection(new URL("http", host, port, name));
						try
						{
							setConnection(uc);
							uc.setRequestProperty("Content-Length", String.valueOf(l));
							dout = new DataOutputStream(new BufferedOutputStream(uc.getOutputStream()));
							saveContent(dout, true);
							dout.flush();
							uc.getInputStream();
							int status;
							if ((status = uc.getStatus()) >= 300)
							throw new CustomMetadataException("Failed to send (status = " + status + ")");
							log("Service package " + name + " of metadata " + fn + " sent at " + new Date());
						}
						finally
						{
							uc.close();
						}
					}
					else name = fn;
					if (msg.target == gui.SaveAndSendItem || msg.target == gui.DeliverPackageItem)
					{
						((MetadataAgent)Naming.lookup("rmi://" + host + "/MetadataAgentImpl")).extract(name,
						msg.target == gui.DeliverPackageItem);
						log("Notified service provider agent of package " + name + " at " + new Date());
/*
						String s = ((String)gui.EmailAddress.get("text")).trim();
						if (!s.equals("") && ads.size() > 0)
						{
							OwnSmtpClient sc = new OwnSmtpClient();
							try
							{
								sc.from("<" + s + ">");
								Enumeration e = ads.elements();
								while (e.hasMoreElements()) sc.to("<" + e.nextElement() + ">");
								p = sc.startMessage();
								p.print("Subject: Service package " + name + "\r\n\r\n" +
								InetAddress.getLocalHost().getHostAddress() + " tried to send service package " + name + "\r\n" +
								Support.replace((String)gui.Note.get("text"), "\n", "\r\n") + "\r\n");
								p.close();
								synchronized (logStream)
								{
									Support.writeBytes(logStream, "Mailed to service provider staff of package " + name + " at " + new Date());
								}
							}
							finally
							{
								sc.closeServer();
							}
						}
*/
						gui.Message.set("text", msg.target == gui.SaveItem || msg.target == gui.SaveAndSendItem ?
						"Metadata saved" + (msg.target == gui.SaveAndSendItem ? " and sent" : "") :
						"Metadata created" + (msg.target == gui.DeliverPackageItem ? "and notified" : ""));
					}
				}
				return true;
			}
			if (msg.target == gui.SavePreferencesButton)
			{
				Properties r = new Properties();
				Enumeration e = gui.Preferences.getChildList();
				while (e.hasMoreElements())
				{
					AttributeManager am = (AttributeManager)e.nextElement();
					if (am instanceof TextFieldShadow) r.put(am.get("name"), am.get("text"));
				}
				BufferedOutputStream fout = new BufferedOutputStream(new FileOutputStream(new File(propertiesName)));
				r.save(fout, "# Metadata properties");
				fout.close();
				gui.Message.set("text", "Preferences saved");
				return true;
			}
			if (msg.target == gui.ClearItem)
			{
				if (touched)
				{
					evt.id = Event.ACTION_EVENT;
					msg.target = gui.SaveItem;
					handleEvent(msg, evt);
				}
				Date t = new Date();
				String dt = new java.sql.Date(t.getTime()).toString(),
				tm = new java.sql.Time(t.getTime()).toString();
				gui.CreationDate.set("text", dt);
				gui.ServiceName.set("text", "");
				gui.Description.set("text", "");
				gui.Keywords.set("text", "");
				resetCheckboxes(gui.ContentLanguage);
				gui.Free.set("state", Boolean.TRUE);
				gui.CopyrightsOwner.set("text", "");
				gui.ReleasePreviewDate.set("text", dt);
				gui.ReleaseDistributionDate.set("text", "");
				gui.ReleaseExpireDate.set("text", "");
				gui.ReleasePreviewTime.set("text", tm);
				gui.ReleaseDistributionTime.set("text", "");
				gui.ReleaseExpireTime.set("text", "");
				gui.StatisticsNeeded.set("state", Boolean.TRUE);
				gui.SubscriptionBased.set("state", Boolean.TRUE);
				gui.CPandSP.set("state", Boolean.TRUE);
				gui.PriceClass.set("selectedItem", ((String[])gui.PriceClass.get("items"))[0]);
				gui.BonusClass.set("selectedItem", ((String[])gui.BonusClass.get("items"))[0]);
				gui.Priority.set("selectedItem", ((String[])gui.Priority.get("items"))[0]);
				gui.CampaignPreviewDate.set("text", dt);
				gui.CampaignDistributionDate.set("text", "");
				gui.CampaignExpireDate.set("text", "");
				gui.CampaignPreviewTime.set("text", tm);
				gui.CampaignDistributionTime.set("text", "");
				gui.CampaignExpireTime.set("text", "");
				gui.NotificationNeeded.set("state", Boolean.TRUE);
				gui.Manual.set("state", Boolean.TRUE);
				gui.Normal.set("state", Boolean.TRUE);
				gui.TemplateFile.set("text", "");
				gui.User.set("state", Boolean.TRUE);
				gui.Layout1.set("text", "1");
				gui.Layout2.set("text", "1");
				gui.Layout3.set("text", "1");
				((ColumnList)gui.ContentFiles.getBody()).delItems();
				gui.ItemFilename.set("text","");
				msg.target = gui.ItemFilename;
				return true;
			}
			if (msg.target == gui.ExitItem)
			{
				if (touched)
				{
					evt.id = Event.ACTION_EVENT;
					msg.target = gui.SaveItem;
					handleEvent(msg, evt);
				}
				System.exit(0);
			}
			if (msg.target == gui.ItemFilename)
			{
				String ct = OwnURLConnection.guessContentType((String)gui.ItemFilename.get("text"));
				gui.MediaType.set("selectedItem", ct != null ? ct.substring(0, ct.indexOf('/')) :
				((String[])gui.MediaType.get("items"))[0]);
				gui.FileDescription.set("text", "");
				gui.PreviewDate.set("text", gui.ReleasePreviewDate.get("text"));
				gui.PreviewTime.set("text", gui.ReleasePreviewTime.get("text"));
				gui.DistributionDate.set("text", gui.ReleaseDistributionDate.get("text"));
				gui.DistributionTime.set("text", gui.ReleaseDistributionTime.get("text"));
				gui.ExpireDate.set("text", gui.ReleaseExpireDate.get("text"));
				gui.ExpireTime.set("text", gui.ReleaseExpireTime.get("text"));
				gui.ItemUsage.set("selectedItem", ((String[])gui.ItemUsage.get("items"))[0]);
				gui.Copyright.set("text", "");
				gui.SellingPrice.set("text", "");
				gui.PurchasePrice.set("text", "");
				gui.VATClass.set("text", "");
				gui.VATAmount.set("text", "");
				gui.PageLayout.set("selectedItem", ((String[])gui.PageLayout.get("items"))[0]);
				gui.PageID.set("text", "");
				return true;
			}
			break;
		case Event.LIST_SELECT:
			if (msg.target == gui.ContentFiles)
			{
				row = ((Number)evt.arg).intValue();
				ColumnList cl = (ColumnList)gui.ContentFiles.getBody();
				String sa[] = (String[])cl.getItem(row, 1);
				gui.ItemFilename.set("text", sa[path]);
				gui.MediaType.set("selectedItem", sa[mediaType]);
				gui.FileDescription.set("text", sa[description]);
				gui.PreviewDate.set("text", sa[previewDate]);
				gui.PreviewTime.set("text", sa[previewTime]);
				gui.DistributionDate.set("text", sa[distributionDate]);
				gui.DistributionTime.set("text", sa[distributionTime]);
				gui.ExpireDate.set("text", sa[expireDate]);
				gui.ExpireTime.set("text", sa[expireTime]);
				gui.ItemUsage.set("selectedItem", sa[itemUsage]);
				gui.Copyright.set("text", sa[copyright]);
				gui.SellingPrice.set("text", sa[sellingPrice]);
				gui.PurchasePrice.set("text", sa[purchasePrice]);
				gui.VATClass.set("text", sa[VATClass]);
				gui.VATAmount.set("text", sa[VATAmount]);
				gui.PageID.set("text", sa[pageID]);
				gui.PageLayout.set("selectedItem", sa[pageLayout]);
				return true;
			}
			break;
		case Event.WINDOW_DESTROY:
			if (touched)
			{
				evt.id = Event.ACTION_EVENT;
				msg.target = gui.SaveItem;
				handleEvent(msg, evt);
			}
			System.exit(0);
		}
	}
	catch (Exception ex)
	{
		if (ex instanceof CustomMetadataException)
		{
			gui.Message.set("text", ex.getMessage());
			return false;
		}
		gui.Message.set("text", Support.stackTrace(ex));
		ex.printStackTrace();
		if (logStream != null)
		synchronized (logStream)
		{
			try
			{
				Support.writeBytes(logStream, "Exception occured at " + new Date() + "\n" +
				Support.stackTrace(ex));
			}
			catch (IOException ex1)
			{
				logStream = null;
			}
		}
	}
	return false;
}

}
