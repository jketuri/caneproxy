
import java.rmi.*;

public interface MetadataAgent extends Remote
{

boolean extract(String name,boolean absPath) throws RemoteException;

}
