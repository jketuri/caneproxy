/**
 * This file was automatically generated.  Do not manually modify this file.
 *
 * Runtime vendor: SunSoft, Inc.
 * Runtime version: 1.0
 *
 * Visual vendor: SunSoft, Inc.
 * Visual version: 1.0
 */



import sunsoft.jws.visual.rt.base.Group;
import sunsoft.jws.visual.rt.base.MainHelper;
import java.applet.Applet;

/**
 * Generated Main class
 *
 * @version 1.20, 05/21/96
 */
public class MetadataMain extends Applet {
  /**
   * Helper class for the generated main class.  This variable is only
   * used when we are running as an applet.
   */
  private MainHelper helper;

  /**
   * Called when application is run from the command line.
   */
  public static void main(String args[]) {
    MainHelper helper = new MainHelper();
    helper.checkVersion(1.0);

    Group group = new Metadata();
    helper.main(group, args);
  }

  /**
   * Called when the applet is loaded.
   */
  public void init() {
    helper = new MainHelper();
    helper.checkVersion(1.0);

    Group group = new Metadata();
    helper.init(this, group);
  }

  /**
   * Called whenever the applet's page is visited.
   */
  public void start(){
    helper.start();
  }

  /**
   * Called by the browser when the user leaves the page.
   */
  public void stop() {
    helper.stop();
  }

  /**
   * Called by the browser when the applet should be destroyed.
   */
  public void destroy() {
    helper.destroy();
  }
}
