
import java.io.*;
import java.util.*;

public class Morse
{

static Hashtable ht = new Hashtable();
static {
ht.put(".-","A");
ht.put("-...","B");
ht.put("-.-.","C");
ht.put("-..","D");
ht.put(".","E");
ht.put("..-.","F");
ht.put("--.","G");
ht.put("....","H");
ht.put("..","I");
ht.put(".---","J");
ht.put("-.-","K");
ht.put(".-..","L");
ht.put("--","M");
ht.put("-.","N");
ht.put("---","O");
ht.put(".--.","P");
ht.put("--.-","Q");
ht.put(".-.","R");
ht.put("...","S");
ht.put("-","T");
ht.put("..-","U");
ht.put("...-","V");
ht.put(".--","W");
ht.put("-..-","X");
ht.put("-.--","Y");
ht.put("--..","Z");
ht.put("........","e");
ht.put(".-...","w");
ht.put(".-.-.","m");
ht.put("...-.-","w");
ht.put("-.-","x");
ht.put("-..-.","/");
ht.put(".----","1");
ht.put("..---","2");
ht.put("...--","3");
ht.put("....-","4");
ht.put(".....","5");
ht.put("-....","6");
ht.put("--...","7");
ht.put("---..","8");
ht.put("----.","9");
ht.put("-----","0");
ht.put(".-.-.-",".");
ht.put("--..--",",");
ht.put("---...",":");
ht.put("-...-","-");
ht.put("-.--.-","(");
ht.put("..--..","?");
}
public static void main(String argv[]) throws Exception
{
StreamTokenizer sTok = new StreamTokenizer(new InputStreamReader(System.in));
sTok.resetSyntax();
sTok.wordChars('-', '.');
sTok.whitespaceChars(0, '-' - 1);
sTok.whitespaceChars('.' + 1, 255);
sTok.ordinaryChar('/');
boolean delim = false;
while (sTok.nextToken() != sTok.TT_EOF)
{
if (sTok.ttype == '/') {
if (delim) System.out.print(' ');
delim = true;
continue;
}
else delim = false;
String sign = (String)ht.get(sTok.sval);
if (sign != null) System.out.print(sign);
}
}

}