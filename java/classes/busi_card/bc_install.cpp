
#include <process.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <windows.h>

#define LENGTH(array) (sizeof(array) / sizeof(*array))

typedef struct
{
	HKEY key;
	LPSTR name;
	LPBYTE data;
	DWORD type;
	DWORD size;
} REGVALUE;

LPSTR message(DWORD n)
{
	LPSTR msg = NULL;
	FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, NULL, n,
	MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&msg, 0, NULL);
	return msg;
}

void error(LPSTR msg)
{
	MessageBox(NULL, msg, "Install Error", MB_OK | MB_APPLMODAL);
}

/* Makes a new string concatenating all strings given as parameters
(list must end with NULL). */
LPSTR makeString(LPSTR str, ...)
{
	va_list list;
	va_start(list, str);
	LPSTR s = str;
	int l = 0;
	do l += strlen(s);
	while (s = va_arg(list, LPSTR));
	va_end(list);
	va_start(list, str);
	s = str;
	str = new char[l + 1];
	str[0] = '\0';
	do strcat(str, s);
	while (s = va_arg(list, LPSTR));
	va_end(list);
	return str;
}

/* Gets value from registry under key and with name of value given in structure parameter.
Checks type if it's non-zero. */
void getRegValue(REGVALUE &value, DWORD type, BOOL check)
{
	if (RegQueryValueEx(value.key, value.name, NULL, &value.type, NULL, &value.size) != ERROR_SUCCESS)
	{
		value.data = NULL;
		if (check) throw makeString("Failed to query name ", value.name, ". ", message(GetLastError()), NULL);
		return;
	}
	if (type && value.type != type) throw makeString("Type of value ", value.name, " is wrong", NULL);
	value.data = new unsigned char[(int)value.size];
	if (RegQueryValueEx(value.key, value.name, NULL, NULL, value.data, &value.size) != ERROR_SUCCESS)
	throw makeString("Failed to query value ", value.name, ". ", message(GetLastError()), NULL);
}

/* Sets registry value. */
void setRegValue(HKEY key, LPCTSTR name, DWORD type, CONST BYTE *data, DWORD size)
{
	if (RegSetValueEx(key, name, 0, type, data, size) != ERROR_SUCCESS)
	throw makeString("Failed to set value ", name, ". ", message(GetLastError()), NULL);
}

void setRegValue(REGVALUE &value)
{
	if (value.data) setRegValue(value.key, value.name, value.type, value.data, value.size);
}

void copyValues(HKEY fromKey, HKEY toKey)
{
	DWORD valuesCount, maxValueNameLen, maxValueLen;
	if (RegQueryInfoKey(fromKey, NULL, NULL, NULL, NULL, NULL, NULL,
	&valuesCount, &maxValueNameLen, &maxValueLen, NULL, NULL) != ERROR_SUCCESS)
	throw makeString("Failed to query key info. ", message(GetLastError()), NULL);
	REGVALUE *regValues = new REGVALUE[valuesCount];
	DWORD valueIndex;
	for (valueIndex = 0; valueIndex < valuesCount; valueIndex++)
	{
		regValues[valueIndex].key = toKey;
		DWORD valueNameLen = maxValueNameLen + 1;
		regValues[valueIndex].name = new CHAR[valueNameLen];
		regValues[valueIndex].data = new UCHAR[maxValueLen];
		regValues[valueIndex].size = maxValueLen;
		if (RegEnumValue(fromKey, valueIndex, regValues[valueIndex].name,
		&valueNameLen, NULL, &regValues[valueIndex].type,
		regValues[valueIndex].data, &regValues[valueIndex].size) != ERROR_SUCCESS)
		throw makeString("Failed to enumerate value. ", message(GetLastError()), NULL);
	}
	for (valueIndex = 0; valueIndex < valuesCount; valueIndex++) setRegValue(regValues[valueIndex]);
}

void encode(FILE *regValuesFile, LPSTR value)
{
	int l = strlen(value);
	for (int i = 0; i < l; i++)
	{
		if (strchr(" \\", ((LPSTR)value)[i])) fputc('\\', regValuesFile);
		fputc(((LPSTR)value)[i], regValuesFile);
	}
}

void saveValues(FILE *regValuesFile, LPTSTR keyName, LPTSTR valueName, ...)
{
	HKEY key;
	if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, keyName, 0, KEY_READ, &key) != ERROR_SUCCESS) return;
	va_list list;
	va_start(list, valueName);
	do
	{
		REGVALUE value = {key, valueName};
		getRegValue(value, REG_SZ, TRUE);
		encode(regValuesFile, (LPSTR)keyName);
		fputc('_', regValuesFile);
		encode(regValuesFile, (LPSTR)valueName);
		fputc('=', regValuesFile);
		encode(regValuesFile, (LPSTR)value.data);
		fputc('\n', regValuesFile);
	}
	while (valueName = va_arg(list, LPTSTR));
	va_end(list);
}

int main()
{
	try
	{
		FILE *regValuesFile = fopen("reg_values.properties", "wt");
		saveValues(regValuesFile, "Software\\JavaSoft\\Java Development Kit\\1.3", "JavaHome", NULL);
		saveValues(regValuesFile, "Software\\JavaSoft\\Java Runtime Environment\\1.3", "JavaHome", NULL);
		saveValues(regValuesFile, "Software\\JavaSoft\\Java Servlet Development Kit 2.0\\2.0", "Path", NULL);
		saveValues(regValuesFile, "Software\\Nokia\\Nokia WAP Server", "Path", NULL);
		fclose(regValuesFile);
	}
	catch (LPSTR msg)
	{
		error(msg);
		return 1;
	}
	return 0;
}
