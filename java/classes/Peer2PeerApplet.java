
import FI.realitymodeler.p2p.*;
import java.applet.*;
import java.net.*;
import java.rmi.*;
import javax.jms.*;

public class Peer2PeerApplet extends Applet implements Runnable
{
	Peer2Peer p2p = null;
	QueueConnection connection = null;
	QueueSession session = null;
	QueueReceiver receiver = null;

public void run()
{
	for (;;)
	try
	{
		ObjectMessage message = (ObjectMessage)receiver.receive();
		getAppletContext().showDocument(new URL(getDocumentBase(), (String)message.getObject()));
	}
	catch (Exception ex)
	{
		ex.printStackTrace();
		getAppletContext().showStatus(ex.toString());
	}
}

public void init()
{
	try
	{
		p2p = Peer2Peer.getPeer2Peer(getDocumentBase().getHost());
		connection = p2p.createQueueConnection();
		session = connection.createQueueSession(false, QueueSession.AUTO_ACKNOWLEDGE);
		receiver = session.createReceiver(session.createQueue(getClass().getName() + "/" + InetAddress.getLocalHost().getHostAddress()));
	}
	catch (Exception ex)
	{
		ex.printStackTrace();
		getAppletContext().showStatus(ex.toString());
		destroy();
		return;
	}
	new Thread(this).start();
}

public void destroy()
{
	try
	{
		try
		{
			if (receiver != null) receiver.close();
		}
		finally
		{
			try
			{
				if (session != null) session.close();
			}
			finally
			{
				if (connection != null) connection.close();
			}
		}
	}
	catch (JMSException ex) {}
}

}
