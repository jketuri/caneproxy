
import FI.realitymodeler.common.*;
import java.io.*;

public class Print
{

public static void main(String argv[]) throws IOException
{
	ConvertOutputStream out = new ConvertOutputStream(System.out, "\n", "\r\n");
	int n;
	byte b[] = new byte[1024];
	while ((n = System.in.read(b)) > 0) out.write(b, 0, n);
	out.write('\f');
}

}
