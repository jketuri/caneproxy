verify on
if a%OS%==aWindows_NT goto skip1
keyb su
doskey
:skip1
set DRIVE=D:
@rem set HOME=%USERPROFILE%
set HOME=%~dp0..\..\..
set J2EE_HOME=%DRIVE%\j2sdkee
set JAVAHOME=%HOME%\java
set JAVA_HOME=%DRIVE%\Program Files\Java\jdk1.8.0_66
set JDKHOME=%JAVA_HOME%
set JREHOME=%DRIVE%\Program Files\Java\jre6
set ANT_HOME=%JAVAHOME%\apache-ant-1.7.1
set OPENSSL=%JAVAHOME%\openssl-1.0.2l
set OPENSSL64=%JAVAHOME%\openssl64\openssl-1.0.2l
set TOMCAT_HOME=%JAVAHOME%\apache-tomcat-8.5.15
set JAVA_OPTS=-Djava.compiler=NONE -Dfile.encoding=UTF8 -Duser.timezone=GMT -Djava.security.auth.login.config=./conf/jaas.config -Dorg.apache.catalina.loader.WebappClassLoader.ENABLE_CLEAR_REFERENCES=false -Dsun.lang.ClassLoader.allowArraySyntax=true -server -Xms512m -Xmx1024m -XX:MaxPermSize=256m -XX:MaxNewSize=256m
set PATH=%DRIVE%\Program Files\NASM;%JAVA_HOME%\bin;%DRIVE%\bin;%DRIVE%\MinGW\bin;%DRIVE%\UnxUtils\usr\local\wbin;%DRIVE%\emacs-23.1\bin;%ANT_HOME%\bin;%JAVAHOME%\bin;%JAVAHOME%\jikes-1.21\src;%JAVAHOME%\libiconv\bin;%OPENSSL%\out32dll;%JAVAHOME%\classes\keppi;%path%
set wk=-e0 -o-1 -d%DRIVE%/wake init.wk
set JAVAC_OPTIONS=-Xlint -deprecation -encoding UTF-8
if not "%VS100COMNTOOLS%" == "" call "%VS100COMNTOOLS%\vsvars32.bat"
if not "%VS100COMNTOOLS%" == "" call "C:\Program Files\Microsoft SDKs\Windows\v7.1\Bin\SetEnv.cmd"
call "%JAVAHOME%\classes\keppi\clpath.bat" %1
if not exist "%HOME%\.emacs" (
   copy dot_emacs "%HOME%\.emacs"
)
