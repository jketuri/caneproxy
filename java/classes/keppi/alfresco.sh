if test "$1" != ""
then
PROGRAM_DIR=$1
else
PROGRAM_DIR=$HOME/KeppiProxy
fi
if test "$2" != ""
then
ALFRESCO_DIR=$2
else
ALFRESCO_DIR=$HOME/apps/alfresco/tomcat
fi
mkdir -p $PROGRAM_DIR/shared/classes
cp -rv $ALFRESCO_DIR/shared/classes/* $PROGRAM_DIR/shared/classes/
mkdir -p $PROGRAM_DIR/webapps
cp -rv $ALFRESCO_DIR/webapps/*.war $PROGRAM_DIR/webapps/
