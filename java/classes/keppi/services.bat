@echo off
if "%1"=="" echo Give drive and directory where java system resides as a first parameter
if "%2"=="" echo Give drive and directory where home page files are as a second parameter
if "%1"=="" goto end
if "%2"=="" goto end
md %1\server
md %1\server\incoming
xcopy %1\classes\metadata\*.html %2
xcopy %1\classes\metadata\*.class %2
xcopy %1\classes\metadata\*.txt %2
xcopy %1\classes\metadata\*.var %2
xcopy %1\classes\FI\tenet\*.class %2\FI\tenet\
xcopy %1\classes\FI\tenet\server\mime_types %1\server\
xcopy %1\classes\FI\tenet\server\virtual_paths %1\server\
echo>%1\server\remote_servers MetadataAgentImpl %2
echo>%1\server\server.properties createRegistry=true
echo>>%1\server\server.properties baseDirectory=%2
echo>>%1\server\server.properties dataDirectory=%1\server
echo>>%1\server\server.properties logging=true
%1\bin\W3Service -install
:end
