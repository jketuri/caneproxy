@if "%1" == "" goto help
xcopy bin\*.* %1\bin\
xcopy /s lib\*.* %1\lib\
@echo If files were copied, you can use now testmob.bat
@goto end
:help
@echo Give Java Runtime Directory as parameter. Don't use trailing slash.
@echo If you use JDK, e.g. C:\jdk\jre
@echo If you use only JRE, e.g. "C:\Program Files\JavaSoft\JRE\1.2"
:end