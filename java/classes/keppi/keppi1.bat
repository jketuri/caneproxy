setlocal
if "%JAVA_OPTS%" == "" set JAVA_OPTS=-Dfile.encoding=UTF8 -Duser.timezone=GMT -Djava.security.auth.login.config=./conf/jaas.config -Dorg.apache.catalina.loader.WebappClassLoader.ENABLE_CLEAR_REFERENCES=false -Dsun.lang.ClassLoader.allowArraySyntax=true -Xms512m -Xmx1024m -XX:MaxPermSize=256m -XX:MaxNewSize=256m
set PATH=.\bin;%PATH%
java %JAVA_OPTS% -jar .\bin\FI_realitymodeler_server.jar %1 %2 %3 %4 %5
endlocal
