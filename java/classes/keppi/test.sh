#!/bin/sh

config=$1
if test -z "$config"
then
	config='test.pg';
else
	shift
fi

port=$1
if test -z "$port"
then
	port=8000
elif test "$port" = "s"
then
	port=3128
	shift
else
	shift
fi

set -x
rm -v /tmp/ts.log /tmp/ts.con /tmp/tc.log /tmp/tc.con
./src/server/polysrv \
	--config $config \
	--cfg_dirs workloads/include \
	--verb_lvl 10 \
	--log /tmp/ts.log \
	--console /tmp/ts.con $* &
sleep 3

./src/client/polyclt \
        --proxy 10.9.34.54:$port \
	--config $config \
	--cfg_dirs workloads/include \
	--verb_lvl 10 \
	--log /tmp/tc.log \
	--console /tmp/tc.con $*

sleep 10;
killall -INT polysrv

tail /tmp/t[sc].con

./src/logextractors/lx /tmp/ts.log | fgrep rate
./src/logextractors/lx /tmp/tc.log | fgrep rate

cd ./log
cp /tmp/tc.log /tmp/ts.log .
rm -rfv /tmp/polyrep
.././src/loganalyzers/reporter --label "Test" *.log
cp -rv /tmp/polyrep .

set +x
