#!/bin/sh

if test "$1" != ""
then
rm -f $HOME/.ssh/id_dsa $HOME/.ssh/id_rsa $HOME/.ssh/identity
ssh-keygen -P "" -f $HOME/.ssh/id_dsa -t dsa
#ssh-keygen -P "" -f $HOME/.ssh/id_rsa -t rsa
#ssh-keygen -P "" -f $HOME/.ssh/identity -t rsa1
chmod 0644 ~/.ssh/id_dsa.pub
#chmod 0644 ~/.ssh/id_rsa.pub
#chmod 0644 ~/.ssh/identity.pub
fi
scp -p ~/.ssh/id_dsa.pub $CVSUSER@$CVSHOST:~/.ssh/authorized_keys2
#scp -p ~/.ssh/identity.pub $CVSUSER@$CVSHOST:~/.ssh/authorized_keys
#scp -p ~/.ssh/id_rsa.pub $CVSUSER@$CVSHOST:~/.ssh/authorized_keys
ssh $CVSUSER@$CVSHOST chmod go-w /home/$CVSUSER/.ssh
ssh $CVSUSER@$CVSHOST chmod 600 /home/$CVSUSER/.ssh/authorized_keys2
