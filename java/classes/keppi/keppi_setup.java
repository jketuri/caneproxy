
import FI.realitymodeler.common.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import java.text.*;
import java.util.*;

public class keppi_setup
    implements ActionListener, Comparator<String> {
    BufferedReader reader = null;
    TextField managerUsername = null;
    TextField managerPassword = null;
    TextField managerPasswordConfirmation = null;
    TextField port = null;
    TextField sslPort = null;
    TextField serverID = null;
    TextField sslServerID = null;
    TextField serverName = null;
    TextField sslServerName = null;
    TextField privateKeyPassword = null;
    TextField smtpServerHost = null;
    Checkbox ftpProxyCache = null;
    Checkbox gopherProxyCache = null;
    Checkbox httpProxyCache = null;
    Checkbox ftpProxyDisabled = null;
    Checkbox gopherProxyDisabled = null;
    Checkbox httpProxyDisabled = null;
    Checkbox imap4ProxyDisabled = null;
    Checkbox msgProxyDisabled = null;
    Checkbox nntpProxyDisabled = null;
    Checkbox pop3ProxyDisabled = null;
    Checkbox smtpProxyDisabled = null;
    Checkbox waisProxyDisabled = null;
    Checkbox sslProxyDisabled = null;
    Choice serverType = null;
    TextField ftpProxyHost = null;
    TextField ftpProxyPort = null;
    TextField gopherProxyHost = null;
    TextField gopherProxyPort = null;
    TextField httpProxyHost = null;
    TextField httpProxyPort = null;
    TextField imap4ProxyHost = null;
    TextField imap4ProxyPort = null;
    TextField msgProxyHost = null;
    TextField msgProxyPort = null;
    TextField nntpProxyHost = null;
    TextField nntpProxyPort = null;
    TextField pop3ProxyHost = null;
    TextField pop3ProxyPort = null;
    TextField smtpProxyHost = null;
    TextField smtpProxyPort = null;
    TextField waisProxyHost = null;
    TextField waisProxyPort = null;
    TextField sslProxyHost = null;
    TextField sslProxyPort = null;
    Window window = null;
    boolean pressed = false;

    public keppi_setup() {
    }

    public keppi_setup(Window window) {
        this.window = window;
    }

    public int compare(String o1, String o2) {
        return o1.compareTo(o2);
    }

    public void actionPerformed(ActionEvent e) {
        pressed = true;
        window.setVisible(false);
    }

    void copyFile(File fromFile, File toFile)
        throws IOException {
        FileInputStream fin = null;
        FileOutputStream fout = null;
        try {
            fin = new FileInputStream(fromFile);
            fout = new FileOutputStream(toFile);
            byte b[] = new byte[4096];
            for (int n; (n = fin.read(b)) > 0;) fout.write(b, 0, n);
        } finally {
            try {
                if (fin != null) fin.close();
            } finally {
                if (fout != null) fout.close();
            }
        }
    }

    void copyDir(File fromDir, File toDir, boolean recurse)
        throws IOException {
        toDir.mkdirs();
        String list[] = fromDir.list();
        for (int i = 0; i < list.length; i++) {
            File file = new File(fromDir, list[i]);
            if (file.isFile() && !file.isDirectory()) {
                File toFile = new File(toDir, list[i]);
                if (!toFile.exists() || list[i].indexOf('.') != -1 && !list[i].endsWith(".properties")) {
                    copyFile(new File(fromDir, list[i]), toFile);
                }
            }
            else if (file.isDirectory() && recurse) {
                File dir = new File(toDir, list[i]);
                dir.mkdir();
                copyDir(file, dir, true);
            }
        }
    }

    boolean dialog(String title, String message, TextField field, String fieldName,
                   String choiceName, String[] choiceNames, String flagName,
                   boolean question, boolean textOnly, Properties serverProperties)
        throws IOException {
        if (textOnly) {
            System.out.println(message);
            if (choiceName != null) {
                int count = choiceNames.length;
                System.out.println("Choice? Type 0-" + (count - 1) + " and press Enter");
                System.out.println("Default: " + serverProperties.getProperty(choiceName));
                for (int index = 0; index < count; index++) {
                    System.out.println(index + " " + choiceNames[index]);
                }
                String line = reader.readLine();
                if (line == null) return true;
                line = line.trim();
                if (!"".equals(line)) {
                    int index = Integer.valueOf(line);
                    serverProperties.setProperty(choiceName, choiceNames[index]);
                }
                return false;
            }
            if (flagName != null) {
                System.out.println("Selection? Type Y or N and press Enter");
                System.out.println("Default: " + (isTrue(serverProperties.getProperty(flagName)) ? "Y" : "N"));
                String line = reader.readLine();
                if (line == null) return true;
                line = line.trim();
                if (!"".equals(line)) {
                    serverProperties.setProperty(flagName, flag(Character.toUpperCase(line.charAt(0)) == 'Y'));
                }
                return false;
            }
            if (fieldName != null) {
                System.out.println("Default: " + serverProperties.getProperty(fieldName));
                String line = reader.readLine();
                if (line == null) return true;
                line = line.trim();
                if (!"".equals(line)) {
                    serverProperties.setProperty(fieldName, line);
                }
            }
            if (!question) {
                System.out.println("Continue? Type Y (default) or N and press Enter");
                String line = reader.readLine();
                if (line == null) return true;
                line = line.trim();
                if (!"".equals(line)) {
                    return Character.toUpperCase(line.charAt(0)) == 'N';
                }
            }
            return false;
        }
        Dialog dialog = new Dialog(new Frame(), title);
        dialog.addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    e.getWindow().setVisible(false);
                }
            });
        dialog.setLayout(new GridLayout(3 + (field != null ? 1 : 0), 1));
        TextArea textArea = new TextArea(message, 4, 80, TextArea.SCROLLBARS_NONE);
        textArea.setEditable(false);
        dialog.add(textArea);
        if (field != null) dialog.add(field);
        Button proceed = new Button(question ? "Yes" : "Proceed");
        proceed.addActionListener(new keppi_setup(dialog));
        dialog.add(proceed);
        Button cancel = new Button(question ? "No" : "Cancel");
        keppi_setup cancelListener = new keppi_setup(dialog);
        cancel.addActionListener(cancelListener);
        dialog.add(cancel);
        dialog.setSize(300, 200);
        dialog.setModal(true);
        dialog.setVisible(true);
        return cancelListener.pressed;
    }

    boolean dialog(String title, boolean textOnly)
        throws IOException {
        return dialog(title, title, null, null, null, null, null, false, textOnly, null);
    }

    void exec(String cmd,
                     boolean check,
                     boolean textOnly)
        throws InterruptedException, IOException {
        Process process = Runtime.getRuntime().exec(cmd);
        process.waitFor();
        if (!check || process.exitValue() == 0) return;
        dialog("Error " + process.exitValue(), textOnly);
        System.out.println("Command " + cmd + " returned error value " + process.exitValue());
        System.exit(process.exitValue());
    }

    Map<String, Map<Object, Object>> readServlets(File file)
        throws IOException, ParseException {
        StreamTokenizer sTok = null;
        Map<String, Map<Object, Object>> servlets = new HashMap<String, Map<Object, Object>>();
        BufferedReader bf = new BufferedReader(new FileReader(file));
        try {
            sTok = new StreamTokenizer(bf);
            sTok.resetSyntax();
            sTok.whitespaceChars(0, 32);
            sTok.wordChars(33, 255);
            sTok.ordinaryChar('=');
            sTok.commentChar('#');
            sTok.quoteChar('"');
            Vector<String> options = null;
            Map<Object, Object> initParameters = null;
            String name = null, locator = null;
            while (sTok.nextToken() != sTok.TT_EOF) {
                if (sTok.ttype != sTok.TT_WORD && sTok.ttype != '"') throw new ParseException("Unexpected token", 0);
                String s = sTok.sval;
                if (initParameters != null && initParameters.size() == 2 && s.startsWith(":")) {
                    options.addElement(s);
                    continue;
                }
                if (sTok.nextToken() == '=') {
                    sTok.nextToken();
                    if (sTok.ttype != sTok.TT_WORD && sTok.ttype != '"' || initParameters == null) throw new ParseException("Unexpected token", 0);
                    initParameters.put(s, sTok.sval);
                    continue;
                }
                if (sTok.ttype == sTok.TT_EOF) break;
                if (name != null) servlets.put(name, initParameters);
                name = s;
                locator = sTok.sval;
                options = new Vector<String>();
                initParameters = new HashMap<Object, Object>();
                initParameters.put(Boolean.TRUE, locator);
                initParameters.put(Boolean.FALSE, options);
            }
            if (name != null) servlets.put(name, initParameters);
        } catch (Exception ex) {
            if (file != null && sTok != null) throw new ParseException("Error in file " + file.getPath() + " in line " + sTok.lineno(), sTok.lineno());
            throw new ParseException("Error reading parameter files", 0);
        } finally {
            bf.close();
        }
        return servlets;
    }

    String encode(String value) {
        StringBuffer valueBuf = new StringBuffer();
        int l = value.length();
        for (int i = 0; i < l; i++) {
            if ("\\\"".indexOf(value.charAt(i)) != -1) valueBuf.append('\\');
            valueBuf.append(value.charAt(i));
        }
        return valueBuf.toString();
    }

    void saveServlets(File file,
                      Map<String, Map<Object, Object>> servlets)
        throws IOException {
        Map<String, Map<Object, Object>> servletMap = new TreeMap<String, Map<Object, Object>>(new keppi_setup());
        Iterator<Map.Entry<String, Map<Object, Object>>> servletIter = servlets.entrySet().iterator();
        while (servletIter.hasNext()) {
            Map.Entry<String, Map<Object, Object>> entry = servletIter.next();
            servletMap.put(entry.getKey(), entry.getValue());
        }
        PrintWriter pw = new PrintWriter(new FileOutputStream(file));
        try {
            pw.println("\n# Servlets saved by bc_install at " + new Date() + "\n");
            servletIter = servletMap.entrySet().iterator();
            while (servletIter.hasNext()) {
                Map.Entry<String, Map<Object, Object>> entry = servletIter.next();
                Map<Object, Object> initParameters = entry.getValue();
                pw.print(entry.getKey() + " " + initParameters.get(Boolean.TRUE));
                Vector<String> options = (Vector<String>)initParameters.get(Boolean.FALSE);
                if (options != null) {
                    Iterator<String> iter = options.iterator();
                    while (iter.hasNext()) pw.print(" " + iter.next());
                }
                pw.println();
                Map<String, String> initParameterMap = new TreeMap<String, String>(new keppi_setup());
                Iterator<Map.Entry<Object, Object>> initParameterIter = initParameters.entrySet().iterator();
                while (initParameterIter.hasNext()) {
                    Map.Entry<Object, Object> initEntry = initParameterIter.next();
                    if (!(initEntry.getKey() instanceof String)) continue;
                    initParameterMap.put((String)initEntry.getKey(), (String)initEntry.getValue());
                }
                Iterator<Map.Entry<String, String>> initParameterMapIter = initParameterMap.entrySet().iterator();
                while (initParameterMapIter.hasNext()) {
                    Map.Entry<String, String> initEntry = initParameterMapIter.next();
                    pw.println("\t" + initEntry.getKey() + "=\"" + encode(initEntry.getValue()) + "\"");
                }
                pw.println();
            }
        } finally {
            pw.close();
        }
    }

    String encodedArgs(Map<Object, Object> initParameters) {
        StringBuffer initArgs = new StringBuffer();
        Iterator<Map.Entry<Object, Object>> initParameterIter = initParameters.entrySet().iterator();
        for (boolean started = false; initParameterIter.hasNext();) {
            Map.Entry<Object, Object> initEntry = initParameterIter.next();
            if (!(initEntry.getKey() instanceof String)) continue;
            if (started) initArgs.append(',');
            initArgs.append(initEntry.getKey()).append('=').append(Support.encode((String)initEntry.getValue(), ",=\\#"));
            started = true;
        }
        return initArgs.toString();
    }

    final boolean isTrue(String value) {
        return value != null && value.equalsIgnoreCase("true");
    }

    final String flag(boolean state) {
        return state ? "true" : "false";
    }

    public void setServerProperties(Properties serverProperties) {
        serverProperties.setProperty("managerCredentials", managerUsername.getText() + ":" + managerPassword.getText());
        serverProperties.setProperty("smtpServerHost", smtpServerHost.getText());
        serverProperties.setProperty("ftpProxyCache", flag(ftpProxyCache.getState()));
        serverProperties.setProperty("gopherProxyCache", flag(gopherProxyCache.getState()));
        serverProperties.setProperty("httpProxyCache", flag(httpProxyCache.getState()));
        serverProperties.setProperty("ftpProxyDisabled", flag(ftpProxyDisabled.getState()));
        serverProperties.setProperty("gopherProxyDisabled", flag(gopherProxyDisabled.getState()));
        serverProperties.setProperty("httpProxyDisabled", flag(httpProxyDisabled.getState()));
        serverProperties.setProperty("imap4ProxyDisabled", flag(imap4ProxyDisabled.getState()));
        serverProperties.setProperty("msgProxyDisabled", flag(msgProxyDisabled.getState()));
        serverProperties.setProperty("nntpProxyDisabled", flag(nntpProxyDisabled.getState()));
        serverProperties.setProperty("pop3ProxyDisabled", flag(pop3ProxyDisabled.getState()));
        serverProperties.setProperty("smtpProxyDisabled", flag(smtpProxyDisabled.getState()));
        serverProperties.setProperty("waisProxyDisabled", flag(waisProxyDisabled.getState()));
        serverProperties.setProperty("sslProxyDisabled", flag(sslProxyDisabled.getState()));
        serverProperties.setProperty("ftpProxyHost", ftpProxyHost.getText());
        serverProperties.setProperty("ftpProxyPort", ftpProxyPort.getText());
        serverProperties.setProperty("gopherProxyHost", gopherProxyHost.getText());
        serverProperties.setProperty("gopherProxyPort", gopherProxyPort.getText());
        serverProperties.setProperty("httpProxyHost", httpProxyHost.getText());
        serverProperties.setProperty("httpProxyPort", httpProxyPort.getText());
        serverProperties.setProperty("imap4ProxyHost", imap4ProxyHost.getText());
        serverProperties.setProperty("imap4ProxyPort", imap4ProxyPort.getText());
        serverProperties.setProperty("msgProxyHost", msgProxyHost.getText());
        serverProperties.setProperty("msgProxyPort", msgProxyPort.getText());
        serverProperties.setProperty("nntpProxyHost", nntpProxyHost.getText());
        serverProperties.setProperty("nntpProxyPort", nntpProxyPort.getText());
        serverProperties.setProperty("pop3ProxyHost", pop3ProxyHost.getText());
        serverProperties.setProperty("pop3ProxyPort", pop3ProxyPort.getText());
        serverProperties.setProperty("smtpProxyHost", smtpProxyHost.getText());
        serverProperties.setProperty("smtpProxyPort", smtpProxyPort.getText());
        serverProperties.setProperty("waisProxyHost", waisProxyHost.getText());
        serverProperties.setProperty("waisProxyPort", waisProxyPort.getText());
        serverProperties.setProperty("sslProxyHost", sslProxyHost.getText());
        serverProperties.setProperty("sslProxyPort", sslProxyPort.getText());
        serverProperties.setProperty("jspCompiler", "jdt");
        serverProperties.setProperty("sslVerify", "none");
    }

    public void setDefault(Properties properties, String name, String value) {
        if (properties.getProperty(name) == null) {
            properties.setProperty(name, value);
        }
    }

    public void setup(String serverDirectory, boolean textOnly)
        throws IOException {
        if (textOnly) {
            reader = new BufferedReader(new InputStreamReader(System.in));
        }
        try {
            boolean standardAndSecure = false;
            String userHome = System.getProperty("user.home"),
                sslServerDirectory = null;
            File serverPropertiesFile = null, sslServerPropertiesFile = null;
            Properties serverProperties = new Properties(), sslServerProperties = new Properties();
            File serverOptionsFile = new File(userHome, "KeppiProxyOptions");
            if (serverOptionsFile.exists()) {
                java.util.List<String> args = new ArrayList<String>();
                BufferedReader bf = new BufferedReader(new FileReader(serverOptionsFile));
                String s;
                while ((s = bf.readLine()) != null) {
                    StringTokenizer st = new StringTokenizer(s, "\t ");
                    while (st.hasMoreTokens()) {
                        String arg = st.nextToken("\t ");
                        if (arg.equals("\"")) continue;
                        if (arg.startsWith("\"")) {
                            if (st.hasMoreTokens() && !arg.endsWith("\"")) {
                                arg += st.nextToken("\"");
                                arg = arg.substring(1);
                            } else arg = arg.substring(1, arg.length() - 1);
                        }
                        args.add(arg);
                    }
                }
                Iterator argItem = args.iterator();
                while (argItem.hasNext()) {
                    String arg = (String)argItem.next();
                    if (arg.charAt(0) == '-')
                        for (int i = 1; i < arg.length(); i++)
                            switch (Character.toLowerCase(arg.charAt(i))) {
                            case 'd':
                                if (serverDirectory == null)
                                    serverDirectory = (String)argItem.next();
                                continue;
                            case 'b':
                            case 'c':
                            case 'l':
                            case 'p':
                                argItem.next();
                                continue;
                            case 'z':
                                standardAndSecure = true;
                                continue;
                            default:
                                continue;
                            }
                }
            }
            if (serverDirectory == null) {
                serverDirectory = new File(userHome, "KeppiProxy").getPath();
                Properties properties = null;
                TextField textField = null;
                if (textOnly) {
                    properties = new Properties();
                    properties.put("serverDirectory", serverDirectory);
                } else {
                    textField = new TextField(serverDirectory);
                }
                if (dialog("Program files directory", "Please give the preferred directory for program files.", textField, "serverDirectory",
                           null, null, null, false, textOnly, properties)) System.exit(1);
                if (textOnly) {
                    serverDirectory = properties.getProperty("serverDirectory");
                } else {
                    serverDirectory = textField.getText();
                }
            }
            serverPropertiesFile = new File(serverDirectory, "server.properties");
            sslServerDirectory = new File(serverDirectory, "ssl").getPath();
            sslServerPropertiesFile = new File(sslServerDirectory, "server.properties");
            if (serverPropertiesFile.exists()) {
                InputStream in = new FileInputStream(serverPropertiesFile);
                try {
                    serverProperties.load(in);
                } finally {
                    in.close();
                }
            }
            if (sslServerPropertiesFile.exists()) {
                InputStream in = new FileInputStream(sslServerPropertiesFile);
                try {
                    sslServerProperties.load(in);
                } finally {
                    in.close();
                }
            }
            InetAddress localAddress = null;
            Enumeration networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                NetworkInterface networkInterface = (NetworkInterface)networkInterfaces.nextElement();
                Enumeration addresses = networkInterface.getInetAddresses();
                while (addresses.hasMoreElements()) {
                    InetAddress inetAddress = (InetAddress)addresses.nextElement();
                    if (!(inetAddress instanceof Inet4Address) || inetAddress.isLoopbackAddress()) continue;
                    localAddress = inetAddress;
                    break;
                }
                if (localAddress != null) break;
            }
            if (localAddress == null) localAddress = InetAddress.getLocalHost();
            String[] serverTypes = new String[] {"Standard", "Secure", "Standard & Secure", "Mobile"};
            String logFormat = serverProperties.getProperty("logFormat");
            if (logFormat == null) serverProperties.setProperty("logFormat", "c-ip time-taken authuser [date time] \"request\" s-status bytes-received bytes-sent cached proxy-pipelined [thread number running-requests waiting-requests queued-requests filter-threads sender-threads responder-threads cache-threads other-threads]");
            String managerCredentials = serverProperties.getProperty("managerCredentials"), username = null, password = null, passwordConfirmation = null;
            if (managerCredentials != null) {
                StringTokenizer st = new StringTokenizer(managerCredentials, ":");
                username = st.nextToken();
                password = st.hasMoreTokens() ? st.nextToken() : "";
            }
            if (username == null) {
                username = "manager";
            }
            if (password == null) {
                password = "reganam";
            }
            passwordConfirmation = password;
            String serverTypeItem = null;
            if (isTrue(serverProperties.getProperty("secure"))) serverTypeItem = "Secure";
            else if (standardAndSecure) serverTypeItem = "Standard & Secure";
            else if (isTrue(serverProperties.getProperty("mobileServer"))) serverTypeItem = "Mobile";
            else serverTypeItem = "Standard";
            Dialog dialog = null;
            keppi_setup installListener = null, cancelListener = null;
            String value = serverProperties.getProperty("serverID");
            String serverIDValue = value != null && value.length() > 0
                && (Character.isLetter(value.charAt(0)) || value.equals("127.0.0.1")) ? value : localAddress.getHostName();
            value = sslServerProperties.getProperty("serverID");
            String sslServerIDValue = value != null && value.length() > 0
                && (Character.isLetter(value.charAt(0)) || value.equals("127.0.0.1")) ? value : localAddress.getHostName();
            value = serverProperties.getProperty("serverName");
            String serverNameValue = value != null && value.length() > 0
                && Character.isLetter(value.charAt(0)) ? value : localAddress.getHostAddress();
            value = sslServerProperties.getProperty("serverName");
            String sslServerNameValue = value != null && value.length() > 0
                && Character.isLetter(value.charAt(0)) ? value : localAddress.getHostAddress();
            if (textOnly) {
                setDefault(serverProperties, "port", "8000");
                setDefault(sslServerProperties, "port", "8443");
                setDefault(serverProperties, "serverID", serverIDValue);
                setDefault(sslServerProperties, "serverID", sslServerIDValue);
                setDefault(serverProperties, "serverName", serverNameValue);
                setDefault(sslServerProperties, "serverName", sslServerNameValue);
                setDefault(sslServerProperties, "password", "changeit");
                setDefault(serverProperties, "smtpServerHost", "localhost");
                setDefault(serverProperties, "ftpProxyHost", "");
                setDefault(serverProperties, "ftpProxyPort", "8000");
                setDefault(serverProperties, "gopherProxyHost", "");
                setDefault(serverProperties, "gopherProxyPort", "8000");
                setDefault(serverProperties, "httpProxyHost", "");
                setDefault(serverProperties, "httpProxyPort", "8000");
                setDefault(serverProperties, "imap4ProxyHost", "");
                setDefault(serverProperties, "imap4ProxyPort", "8000");
                setDefault(serverProperties, "msgProxyHost", "");
                setDefault(serverProperties, "msgProxyPort", "8000");
                setDefault(serverProperties, "nntpProxyHost", "");
                setDefault(serverProperties, "nntpProxyPort", "8000");
                setDefault(serverProperties, "pop3ProxyHost", "");
                setDefault(serverProperties, "pop3ProxyPort", "8000");
                setDefault(serverProperties, "smtpProxyHost", "");
                setDefault(serverProperties, "smtpProxyPort", "8000");
                setDefault(serverProperties, "waisProxyHost", "");
                setDefault(serverProperties, "waisProxyPort", "8000");
                setDefault(serverProperties, "waisProxyHost", "");
                setDefault(serverProperties, "waisProxyPort", "8000");
                setDefault(serverProperties, "sslProxyHost", "");
                setDefault(serverProperties, "sslProxyPort", "8000");
            } else {
                dialog = new Dialog(new Frame(), "Keppi Proxy settings");
                dialog.setModal(true);
                Panel mainPanel = new Panel();
                GridBagLayout mainGridBagLayout = new GridBagLayout();
                mainPanel.setLayout(mainGridBagLayout);
                GridBagConstraints gridBagConstraints = new GridBagConstraints();
                gridBagConstraints.anchor = GridBagConstraints.WEST;
                GridBagConstraints leftGridBagConstraints = new GridBagConstraints();
                leftGridBagConstraints.anchor = GridBagConstraints.WEST;
                leftGridBagConstraints.gridx = 0;
                leftGridBagConstraints.gridwidth = GridBagConstraints.RELATIVE;
                GridBagConstraints rightGridBagConstraints = new GridBagConstraints();
                rightGridBagConstraints.anchor = GridBagConstraints.WEST;
                rightGridBagConstraints.gridx = 1;
                rightGridBagConstraints.gridwidth = GridBagConstraints.REMAINDER;
                Label label = new Label("Manager user name");
                mainGridBagLayout.setConstraints(label, leftGridBagConstraints);
                mainPanel.add(label);
                managerUsername = new TextField(username);
                managerUsername.setColumns(8);
                mainGridBagLayout.setConstraints(managerUsername, rightGridBagConstraints);
                mainPanel.add(managerUsername);
                label = new Label("Manager password");
                mainGridBagLayout.setConstraints(label, leftGridBagConstraints);
                mainPanel.add(label);
                managerPassword = new TextField(password);
                managerPassword.setColumns(8);
                managerPassword.setEchoChar('*');
                mainGridBagLayout.setConstraints(managerPassword, rightGridBagConstraints);
                mainPanel.add(managerPassword);
                label = new Label("Manager password confirmation");
                mainGridBagLayout.setConstraints(label, leftGridBagConstraints);
                mainPanel.add(label);
                managerPasswordConfirmation = new TextField(password);
                managerPasswordConfirmation.setColumns(8);
                managerPasswordConfirmation.setEchoChar('*');
                mainGridBagLayout.setConstraints(managerPasswordConfirmation, rightGridBagConstraints);
                mainPanel.add(managerPasswordConfirmation);
                label = new Label("Server type");
                mainGridBagLayout.setConstraints(label, leftGridBagConstraints);
                mainPanel.add(label);
                serverType = new Choice();
                for (final String aServerTypeItem : serverTypes) {
                    serverType.add(aServerTypeItem);
                }
                serverType.select(serverTypeItem);
                mainGridBagLayout.setConstraints(serverType, rightGridBagConstraints);
                mainPanel.add(serverType);
                label = new Label("Server port");
                mainGridBagLayout.setConstraints(label, leftGridBagConstraints);
                mainPanel.add(label);
                value = serverProperties.getProperty("port");
                port = new TextField(value != null ? value : "8000");
                port.setColumns(4);
                mainGridBagLayout.setConstraints(port, rightGridBagConstraints);
                mainPanel.add(port);
                label = new Label("Secure server port");
                mainGridBagLayout.setConstraints(label, leftGridBagConstraints);
                mainPanel.add(label);
                value = sslServerProperties.getProperty("port");
                sslPort = new TextField(value != null ? value : "8443");
                sslPort.setColumns(4);
                mainGridBagLayout.setConstraints(sslPort, rightGridBagConstraints);
                mainPanel.add(sslPort);
                label = new Label("Server ID");
                mainGridBagLayout.setConstraints(label, leftGridBagConstraints);
                mainPanel.add(label);
                serverID = new TextField(serverIDValue);
                serverID.setColumns(16);
                mainGridBagLayout.setConstraints(serverID, rightGridBagConstraints);
                mainPanel.add(serverID);
                label = new Label("Secure server ID");
                mainGridBagLayout.setConstraints(label, leftGridBagConstraints);
                mainPanel.add(label);
                sslServerID = new TextField(sslServerIDValue);
                sslServerID.setColumns(16);
                mainGridBagLayout.setConstraints(sslServerID, rightGridBagConstraints);
                mainPanel.add(sslServerID);
                label = new Label("Server name");
                mainGridBagLayout.setConstraints(label, leftGridBagConstraints);
                mainPanel.add(label);
                serverName = new TextField(serverNameValue);
                serverName.setColumns(16);
                mainGridBagLayout.setConstraints(serverName, rightGridBagConstraints);
                mainPanel.add(serverName);
                label = new Label("Secure server name");
                mainGridBagLayout.setConstraints(label, leftGridBagConstraints);
                mainPanel.add(label);
                sslServerName = new TextField(sslServerNameValue);
                sslServerName.setColumns(16);
                mainGridBagLayout.setConstraints(sslServerName, rightGridBagConstraints);
                mainPanel.add(sslServerName);
                label = new Label("Private key password");
                mainGridBagLayout.setConstraints(label, leftGridBagConstraints);
                mainPanel.add(label);
                value = sslServerProperties.getProperty("password");
                privateKeyPassword = new TextField(value != null ? value : "changeit");
                privateKeyPassword.setColumns(8);
                privateKeyPassword.setEchoChar('*');
                mainGridBagLayout.setConstraints(privateKeyPassword, rightGridBagConstraints);
                mainPanel.add(privateKeyPassword);
                label = new Label("Mail host");
                mainGridBagLayout.setConstraints(label, leftGridBagConstraints);
                mainPanel.add(label);
                value = serverProperties.getProperty("smtpServerHost");
                smtpServerHost = new TextField(value != null ? value : "localhost");
                smtpServerHost.setColumns(16);
                mainGridBagLayout.setConstraints(smtpServerHost, rightGridBagConstraints);
                mainPanel.add(smtpServerHost);
                label = new Label("Proxy caching");
                mainGridBagLayout.setConstraints(label, leftGridBagConstraints);
                mainPanel.add(label);
                Panel panel = new Panel();
                GridBagLayout gridBagLayout = new GridBagLayout();
                panel.setLayout(gridBagLayout);
                ftpProxyCache = new Checkbox("Ftp", isTrue(serverProperties.getProperty("ftpProxyCache")));
                gridBagLayout.setConstraints(ftpProxyCache, gridBagConstraints);
                panel.add(ftpProxyCache);
                gopherProxyCache = new Checkbox("Gopher", isTrue(serverProperties.getProperty("gopherProxyCache")));
                gridBagLayout.setConstraints(gopherProxyCache, gridBagConstraints);
                panel.add(gopherProxyCache);
                httpProxyCache = new Checkbox("Http", isTrue(serverProperties.getProperty("httpProxyCache")));
                gridBagLayout.setConstraints(httpProxyCache, gridBagConstraints);
                panel.add(httpProxyCache);
                mainGridBagLayout.setConstraints(panel, rightGridBagConstraints);
                mainPanel.add(panel);
                label = new Label("Proxy disabling");
                mainGridBagLayout.setConstraints(label, leftGridBagConstraints);
                mainPanel.add(label);
                panel = new Panel();
                gridBagLayout = new GridBagLayout();
                panel.setLayout(gridBagLayout);
                ftpProxyDisabled = new Checkbox("Ftp", isTrue(serverProperties.getProperty("ftpProxyDisabled")));
                gridBagLayout.setConstraints(ftpProxyDisabled, gridBagConstraints);
                panel.add(ftpProxyDisabled);
                gopherProxyDisabled = new Checkbox("Gopher", isTrue(serverProperties.getProperty("gopherProxyDisabled")));
                gridBagLayout.setConstraints(gopherProxyDisabled, gridBagConstraints);
                panel.add(gopherProxyDisabled);
                httpProxyDisabled = new Checkbox("Http", isTrue(serverProperties.getProperty("httpProxyDisabled")));
                gridBagLayout.setConstraints(httpProxyDisabled, gridBagConstraints);
                panel.add(httpProxyDisabled);
                imap4ProxyDisabled = new Checkbox("Imap4", isTrue(serverProperties.getProperty("imap4ProxyDisabled")));
                gridBagLayout.setConstraints(imap4ProxyDisabled, gridBagConstraints);
                panel.add(imap4ProxyDisabled);
                msgProxyDisabled = new Checkbox("Msg", isTrue(serverProperties.getProperty("msgProxyDisabled")));
                gridBagLayout.setConstraints(msgProxyDisabled, gridBagConstraints);
                panel.add(msgProxyDisabled);
                GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
                gridBagConstraints1.anchor = GridBagConstraints.WEST;
                gridBagConstraints1.gridy = 1;
                nntpProxyDisabled = new Checkbox("Nntp", isTrue(serverProperties.getProperty("nntpProxyDisabled")));
                gridBagLayout.setConstraints(nntpProxyDisabled, gridBagConstraints1);
                panel.add(nntpProxyDisabled);
                pop3ProxyDisabled = new Checkbox("Pop3", isTrue(serverProperties.getProperty("pop3ProxyDisabled")));
                gridBagLayout.setConstraints(pop3ProxyDisabled, gridBagConstraints1);
                panel.add(pop3ProxyDisabled);
                smtpProxyDisabled = new Checkbox("Smtp", isTrue(serverProperties.getProperty("smtpProxyDisabled")));
                gridBagLayout.setConstraints(smtpProxyDisabled, gridBagConstraints1);
                panel.add(smtpProxyDisabled);
                waisProxyDisabled = new Checkbox("Wais", isTrue(serverProperties.getProperty("waisProxyDisabled")));
                gridBagLayout.setConstraints(waisProxyDisabled, gridBagConstraints1);
                panel.add(waisProxyDisabled);
                sslProxyDisabled = new Checkbox("Ssl", isTrue(serverProperties.getProperty("sslProxyDisabled")));
                gridBagLayout.setConstraints(sslProxyDisabled, gridBagConstraints1);
                panel.add(sslProxyDisabled);
                mainGridBagLayout.setConstraints(panel, rightGridBagConstraints);
                mainPanel.add(panel);
                label = new Label("Ftp proxy host");
                mainGridBagLayout.setConstraints(label, leftGridBagConstraints);
                mainPanel.add(label);
                value = serverProperties.getProperty("ftpProxyHost");
                ftpProxyHost = new TextField(value != null ? value : "");
                ftpProxyHost.setColumns(16);
                mainGridBagLayout.setConstraints(ftpProxyHost, rightGridBagConstraints);
                mainPanel.add(ftpProxyHost);
                label = new Label("Ftp proxy port");
                mainGridBagLayout.setConstraints(label, leftGridBagConstraints);
                mainPanel.add(label);
                value = serverProperties.getProperty("ftpProxyPort");
                ftpProxyPort = new TextField(value != null ? value : "8000");
                ftpProxyPort.setColumns(4);
                mainGridBagLayout.setConstraints(ftpProxyPort, rightGridBagConstraints);
                mainPanel.add(ftpProxyPort);
                label = new Label("Gopher proxy host");
                mainGridBagLayout.setConstraints(label, leftGridBagConstraints);
                mainPanel.add(label);
                value = serverProperties.getProperty("gopherProxyHost");
                gopherProxyHost = new TextField(value != null ? value : "");
                gopherProxyHost.setColumns(16);
                mainGridBagLayout.setConstraints(gopherProxyHost, rightGridBagConstraints);
                mainPanel.add(gopherProxyHost);
                label = new Label("Gopher proxy port");
                mainGridBagLayout.setConstraints(label, leftGridBagConstraints);
                mainPanel.add(label);
                value = serverProperties.getProperty("gopherProxyPort");
                gopherProxyPort = new TextField(value != null ? value : "8000");
                gopherProxyPort.setColumns(4);
                mainGridBagLayout.setConstraints(gopherProxyPort, rightGridBagConstraints);
                mainPanel.add(gopherProxyPort);
                label = new Label("Http proxy host");
                mainGridBagLayout.setConstraints(label, leftGridBagConstraints);
                mainPanel.add(label);
                value = serverProperties.getProperty("httpProxyHost");
                httpProxyHost = new TextField(value != null ? value : "");
                httpProxyHost.setColumns(16);
                mainGridBagLayout.setConstraints(httpProxyHost, rightGridBagConstraints);
                mainPanel.add(httpProxyHost);
                label = new Label("Http proxy port");
                mainGridBagLayout.setConstraints(label, leftGridBagConstraints);
                mainPanel.add(label);
                value = serverProperties.getProperty("httpProxyPort");
                httpProxyPort = new TextField(value != null ? value : "8000");
                httpProxyPort.setColumns(4);
                mainGridBagLayout.setConstraints(httpProxyPort, rightGridBagConstraints);
                mainPanel.add(httpProxyPort);
                label = new Label("Imap4 proxy host");
                mainGridBagLayout.setConstraints(label, leftGridBagConstraints);
                mainPanel.add(label);
                value = serverProperties.getProperty("imap4ProxyHost");
                imap4ProxyHost = new TextField(value != null ? value : "");
                imap4ProxyHost.setColumns(16);
                mainGridBagLayout.setConstraints(imap4ProxyHost, rightGridBagConstraints);
                mainPanel.add(imap4ProxyHost);
                label = new Label("Imap4 proxy port");
                mainGridBagLayout.setConstraints(label, leftGridBagConstraints);
                mainPanel.add(label);
                value = serverProperties.getProperty("imap4ProxyPort");
                imap4ProxyPort = new TextField(value != null ? value : "8000");
                imap4ProxyPort.setColumns(4);
                mainGridBagLayout.setConstraints(imap4ProxyPort, rightGridBagConstraints);
                mainPanel.add(imap4ProxyPort);
                label = new Label("Msg proxy host");
                mainGridBagLayout.setConstraints(label, leftGridBagConstraints);
                mainPanel.add(label);
                value = serverProperties.getProperty("msgProxyHost");
                msgProxyHost = new TextField(value != null ? value : "");
                msgProxyHost.setColumns(16);
                mainGridBagLayout.setConstraints(msgProxyHost, rightGridBagConstraints);
                mainPanel.add(msgProxyHost);
                label = new Label("Msg proxy port");
                mainGridBagLayout.setConstraints(label, leftGridBagConstraints);
                mainPanel.add(label);
                value = serverProperties.getProperty("msgProxyPort");
                msgProxyPort = new TextField(value != null ? value : "8000");
                msgProxyPort.setColumns(4);
                mainGridBagLayout.setConstraints(msgProxyPort, rightGridBagConstraints);
                mainPanel.add(msgProxyPort);
                label = new Label("Nntp proxy host");
                mainGridBagLayout.setConstraints(label, leftGridBagConstraints);
                mainPanel.add(label);
                value = serverProperties.getProperty("nntpProxyHost");
                nntpProxyHost = new TextField(value != null ? value : "");
                nntpProxyHost.setColumns(16);
                mainGridBagLayout.setConstraints(nntpProxyHost, rightGridBagConstraints);
                mainPanel.add(nntpProxyHost);
                label = new Label("Nntp proxy port");
                mainGridBagLayout.setConstraints(label, leftGridBagConstraints);
                mainPanel.add(label);
                value = serverProperties.getProperty("nntpProxyPort");
                nntpProxyPort = new TextField(value != null ? value : "8000");
                nntpProxyPort.setColumns(4);
                mainGridBagLayout.setConstraints(nntpProxyPort, rightGridBagConstraints);
                mainPanel.add(nntpProxyPort);
                label = new Label("Pop3 proxy host");
                mainGridBagLayout.setConstraints(label, leftGridBagConstraints);
                mainPanel.add(label);
                value = serverProperties.getProperty("pop3ProxyHost");
                pop3ProxyHost = new TextField(value != null ? value : "");
                pop3ProxyHost.setColumns(16);
                mainGridBagLayout.setConstraints(pop3ProxyHost, rightGridBagConstraints);
                mainPanel.add(pop3ProxyHost);
                label = new Label("Pop3 proxy port");
                mainGridBagLayout.setConstraints(label, leftGridBagConstraints);
                mainPanel.add(label);
                value = serverProperties.getProperty("pop3ProxyPort");
                pop3ProxyPort = new TextField(value != null ? value : "8000");
                pop3ProxyPort.setColumns(4);
                mainGridBagLayout.setConstraints(pop3ProxyPort, rightGridBagConstraints);
                mainPanel.add(pop3ProxyPort);
                label = new Label("Smtp proxy host");
                mainGridBagLayout.setConstraints(label, leftGridBagConstraints);
                mainPanel.add(label);
                value = serverProperties.getProperty("smtpProxyHost");
                smtpProxyHost = new TextField(value != null ? value : "");
                smtpProxyHost.setColumns(16);
                mainGridBagLayout.setConstraints(smtpProxyHost, rightGridBagConstraints);
                mainPanel.add(smtpProxyHost);
                label = new Label("Smtp proxy port");
                mainGridBagLayout.setConstraints(label, leftGridBagConstraints);
                mainPanel.add(label);
                value = serverProperties.getProperty("smtpProxyPort");
                smtpProxyPort = new TextField(value != null ? value : "8000");
                smtpProxyPort.setColumns(4);
                mainGridBagLayout.setConstraints(smtpProxyPort, rightGridBagConstraints);
                mainPanel.add(smtpProxyPort);
                label = new Label("Wais proxy host");
                mainGridBagLayout.setConstraints(label, leftGridBagConstraints);
                mainPanel.add(label);
                value = serverProperties.getProperty("waisProxyHost");
                waisProxyHost = new TextField(value != null ? value : "");
                waisProxyHost.setColumns(16);
                mainGridBagLayout.setConstraints(waisProxyHost, rightGridBagConstraints);
                mainPanel.add(waisProxyHost);
                label = new Label("Wais proxy port");
                mainGridBagLayout.setConstraints(label, leftGridBagConstraints);
                mainPanel.add(label);
                value = serverProperties.getProperty("waisProxyPort");
                waisProxyPort = new TextField(value != null ? value : "8000");
                waisProxyPort.setColumns(4);
                mainGridBagLayout.setConstraints(waisProxyPort, rightGridBagConstraints);
                mainPanel.add(waisProxyPort);
                label = new Label("Ssl proxy host");
                mainGridBagLayout.setConstraints(label, leftGridBagConstraints);
                mainPanel.add(label);
                value = serverProperties.getProperty("sslProxyHost");
                sslProxyHost = new TextField(value != null ? value : "");
                sslProxyHost.setColumns(16);
                mainGridBagLayout.setConstraints(sslProxyHost, rightGridBagConstraints);
                mainPanel.add(sslProxyHost);
                label = new Label("Ssl proxy port");
                mainGridBagLayout.setConstraints(label, leftGridBagConstraints);
                mainPanel.add(label);
                value = serverProperties.getProperty("sslProxyPort");
                sslProxyPort = new TextField(value != null ? value : "8000");
                sslProxyPort.setColumns(4);
                mainGridBagLayout.setConstraints(sslProxyPort, rightGridBagConstraints);
                mainPanel.add(sslProxyPort);
                Button install = new Button("Install");
                mainGridBagLayout.setConstraints(install, leftGridBagConstraints);
                mainPanel.add(install);
                Button cancel = new Button("Cancel");
                mainGridBagLayout.setConstraints(cancel, rightGridBagConstraints);
                mainPanel.add(cancel);
                installListener = new keppi_setup(dialog);
                install.addActionListener(installListener);
                cancelListener = new keppi_setup(dialog);
                cancel.addActionListener(cancelListener);
                dialog.addWindowListener(new WindowAdapter() {
                        public void windowClosing(WindowEvent e) {
                            e.getWindow().setVisible(false);
                        }
                    });
                ScrollPane pane = new ScrollPane(ScrollPane.SCROLLBARS_ALWAYS);
                pane.add(mainPanel);
                dialog.add(pane);
                dialog.setSize(640, 400);
            }
            String message = "";
            do {
                if (!textOnly) {
                    dialog.setVisible(true);
                    if (cancelListener.pressed) break;
                    if (!installListener.pressed) break;
                }
                if (textOnly ? !password.equals(passwordConfirmation)
                    : !managerPassword.getText().equals(managerPasswordConfirmation.getText())) {
                    message = "Manager password confirmation mismatch";
                    continue;
                }
                message = "Retry installation?";
                if (textOnly) {
                    Properties properties = new Properties();
                    properties.setProperty("managerUserName", username);
                    properties.setProperty("managerPassword", password);
                    properties.setProperty("managerPasswordConfirmation", password);
                    properties.setProperty("serverType", serverTypeItem);
                    dialog(null, "Manager user name", null, "managerUserName", null, null, null, true, true, properties);
                    dialog(null, "Manager password", null, "managerPassword", null, null, null, true, true, properties);
                    dialog(null, "Manager password confirmation", null, "managerPasswordConfirmation", null, null, null, true, true, properties);
                    dialog(null, "Server type", null, null, "serverType", serverTypes, null, true, true, properties);
                    String serverTypeValue = properties.getProperty("serverType");
                    if (!"Secure".equals(serverTypeValue)) {
                        dialog(null, "Server port", null, "port", null, null, null, true, true, serverProperties);
                        dialog(null, "Server ID", null, "serverID", null, null, null, true, true, serverProperties);
                        dialog(null, "Server name", null, "serverName", null, null, null, true, true, serverProperties);
                    }
                    if ("Secure".equals(serverTypeValue) || "Standard & Secure".equals(serverTypeValue)) {
                        dialog(null, "Secure server port", null, "port", null, null, null, true, true, sslServerProperties);
                        dialog(null, "Secure server ID", null, "serverID", null, null, null, true, true, sslServerProperties);
                        dialog(null, "Secure server name", null, "serverName", null, null, null, true, true, sslServerProperties);
                        dialog(null, "Private key password", null, "password", null, null, null, true, true, sslServerProperties);
                    }
                    dialog(null, "Mail host", null, "smtpServerHost", null, null, null, true, true, serverProperties);
                    dialog(null, "Ftp proxy caching", null, null, null, null, "ftpProxyCache", true, true, serverProperties);
                    dialog(null, "Gopher proxy caching", null, null, null, null, "gopherProxyCache", true, true, serverProperties);
                    dialog(null, "Http proxy caching", null, null, null, null, "httpProxyCache", true, true, serverProperties);
                    dialog(null, "Ftp proxy disabled", null, null, null, null, "ftpProxyDisabled", true, true, serverProperties);
                    dialog(null, "Gopher proxy disabled", null, null, null, null, "gopherProxyDisabled", true, true, serverProperties);
                    dialog(null, "Http proxy disabled", null, null, null, null, "httpProxyDisabled", true, true, serverProperties);
                    dialog(null, "Imap4 proxy disabled", null, null, null, null, "imap4ProxyDisabled", true, true, serverProperties);
                    dialog(null, "Msg proxy disabled", null, null, null, null, "msgProxyDisabled", true, true, serverProperties);
                    dialog(null, "Nntp proxy disabled", null, null, null, null, "nntpProxyDisabled", true, true, serverProperties);
                    dialog(null, "Pop3 proxy disabled", null, null, null, null, "pop3ProxyDisabled", true, true, serverProperties);
                    dialog(null, "Smtp proxy disabled", null, null, null, null, "smtpProxyDisabled", true, true, serverProperties);
                    dialog(null, "Wais proxy disabled", null, null, null, null, "waisProxyDisabled", true, true, serverProperties);
                    dialog(null, "SSL proxy disabled", null, null, null, null, "sslProxyDisabled", true, true, serverProperties);
                    dialog(null, "Ftp proxy host", null, "ftpProxyHost", null, null, null, true, true, serverProperties);
                    dialog(null, "Ftp proxy port", null, "ftpProxyPort", null, null, null, true, true, serverProperties);
                    dialog(null, "Gopher proxy host", null, "gopherProxyHost", null, null, null, true, true, serverProperties);
                    dialog(null, "Gopher proxy port", null, "gopherProxyPort", null, null, null, true, true, serverProperties);
                    dialog(null, "Http proxy host", null, "httpProxyHost", null, null, null, true, true, serverProperties);
                    dialog(null, "Http proxy port", null, "httpProxyPort", null, null, null, true, true, serverProperties);
                    dialog(null, "Imap4 proxy host", null, "imap4ProxyHost", null, null, null, true, true, serverProperties);
                    dialog(null, "Imap4 proxy port", null, "imap4ProxyPort", null, null, null, true, true, serverProperties);
                    dialog(null, "Msg proxy host", null, "msgProxyHost", null, null, null, true, true, serverProperties);
                    dialog(null, "Msg proxy port", null, "msgProxyPort", null, null, null, true, true, serverProperties);
                    dialog(null, "Nntp proxy host", null, "nntpProxyHost", null, null, null, true, true, serverProperties);
                    dialog(null, "Nntp proxy port", null, "nntpProxyPort", null, null, null, true, true, serverProperties);
                    dialog(null, "Pop3 proxy host", null, "pop3ProxyHost", null, null, null, true, true, serverProperties);
                    dialog(null, "Pop3 proxy port", null, "pop3ProxyPort", null, null, null, true, true, serverProperties);
                    dialog(null, "Smtp proxy host", null, "smtpProxyHost", null, null, null, true, true, serverProperties);
                    dialog(null, "Smtp proxy port", null, "smtpProxyPort", null, null, null, true, true, serverProperties);
                    dialog(null, "Wais proxy host", null, "waisProxyHost", null, null, null, true, true, serverProperties);
                    dialog(null, "Wais proxy port", null, "waisProxyPort", null, null, null, true, true, serverProperties);
                    dialog(null, "SSL proxy host", null, "sslProxyHost", null, null, null, true, true, serverProperties);
                    dialog(null, "SSL proxy port", null, "sslProxyPort", null, null, null, true, true, serverProperties);
                    username = properties.getProperty("managerUserName");
                    password = properties.getProperty("managerPassword");
                    passwordConfirmation = properties.getProperty("managerPasswordConfirmation");
                    serverTypeItem = properties.getProperty("serverType");
                } else {
                    serverProperties.setProperty("serverID", serverID.getText().trim());
                    serverProperties.setProperty("serverName", serverName.getText().trim());
                    serverTypeItem = serverType.getSelectedItem();
                    serverProperties.setProperty("port", port.getText().trim());
                    setServerProperties(serverProperties);
                    sslServerProperties.setProperty("serverID", sslServerID.getText().trim());
                    sslServerProperties.setProperty("serverName", sslServerName.getText().trim());
                    sslServerProperties.setProperty("port", sslPort.getText().trim());
                    sslServerProperties.setProperty("password", privateKeyPassword.getText());
                    setServerProperties(sslServerProperties);
                }
                serverProperties.setProperty("mobileServer", flag(serverTypeItem.equals("Mobile")));
                sslServerProperties.setProperty("secure", "true");
                copyDir(new File("KeppiProxy"), new File(serverDirectory), true);
                if (File.separatorChar == '/') {
                    new File(serverDirectory, "keppi").setExecutable(true);
                    new File(serverDirectory, "manag").setExecutable(true);
                    new File(serverDirectory, "robot").setExecutable(true);
                    new File(serverDirectory, "bin/jikes").setExecutable(true);
                    new File(serverDirectory, "KeppiProxy").setExecutable(true);
                    new File(serverDirectory, "w3service").setExecutable(true);
                    new File(serverDirectory, "w3service_mac").setExecutable(true);
                }
                OutputStream fout = new FileOutputStream(serverPropertiesFile);
                try {
                    serverProperties.store(fout, "server.properties stored by keppi_setup");
                } finally {
                    fout.close();
                }
                fout = new FileOutputStream(sslServerPropertiesFile);
                try {
                    sslServerProperties.store(fout, "server.properties stored by keppi_setup");
                } finally {
                    fout.close();
                }
                FileOutputStream serverOptionsOut = new FileOutputStream(serverOptionsFile);
                if (serverTypeItem.equals("Standard & Secure"))
                    Support.writeBytes(serverOptionsOut, "-dzd \"" + serverDirectory + "\" \"" + serverDirectory + "/ssl\"", null);
                else if (serverTypeItem.equals("Secure"))
                    Support.writeBytes(serverOptionsOut, "-d \"" + sslServerDirectory + "\"", null);
                else Support.writeBytes(serverOptionsOut, "-d \"" + serverDirectory + "\"", null);
                serverOptionsOut.close();
                copyFile(serverOptionsFile, new File(serverDirectory, "KeppiProxyOptions"));
                File defaultDirectory = new File(serverDirectory, "webapps/ROOT");
                new File(defaultDirectory.getCanonicalPath()).mkdirs();
                PrintStream pout = new PrintStream(new FileOutputStream(new File(defaultDirectory, "proxy-config")));
                try {
                    pout.print("function FindProxyForURL(url,host)\n{\n\treturn \"PROXY "
                               + serverProperties.getProperty("serverName").trim() + ":" + serverProperties.getProperty("port").trim() + "\";\n}");
                } finally {
                    pout.close();
                }
                break;
            } while (!dialog(message, textOnly));
            System.exit(0);
        } catch (Throwable th) {
            th.printStackTrace();
            dialog("Error " + Support.stackTrace(th), textOnly);
            System.exit(1);
        }
    }

    public static void main(String argv[])
        throws IOException {
        keppi_setup keppi_setup = new keppi_setup();
        String serverDirectory = null;
        boolean textOnly = false;
        for (int argi = 0; argi < argv.length; argi++) {
            String arg = argv[argi];
            if (arg.startsWith("-"))
                switch (arg.charAt(1)) {
                case 'd':
                    serverDirectory = argv[++argi];
                    continue;
                case 't':
                    textOnly = true;
                    continue;
                case '?':
                    break;
                }
            System.out.println("Options:");
            System.out.println("-d <server directory> : set server directory");
            System.out.println("-t : use text only terminal");
            System.out.println("-? : this help");
        }
        keppi_setup.setup(serverDirectory, textOnly);
    }

}
