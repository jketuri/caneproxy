
Version: 2016-6-2

HTTP/1.1 web server, Java Enterprise Edition (JEE)-compatible web
application server and caching forward and reverse proxy server. Also
Web Client Robot, which can be used to read files from network and
mirror content from Internet.
Supports Servlet API version 2.4.
Does not yet support asynchronous operation.

This is not 100% pure Java program. Currently tested in Win32-, Linux-
and Mac-environments. You must have Java 2 Runtime Environment version
1.8 installed from http://www.oracle.com.

In Windows, Linux or Mac unzip keppi_common1.zip and keppi_common2.zip
to the same directory. In Windows unzip keppi_win32.zip, in Linux
unzip keppi_linux.zip, keppi_linux_glibc2_5.zip or keppi_linux64.zip, or
in Mac unzip keppi_mac.zip, then untar keppi.tgz all to the same
directory. Glibc-library version in Linux you can check with command:

ldd --version

Go to directory 'keppi'. Run setup.bat or ./setup.  When running
setup, you should not have any other Java-programs running, like older
version of this program. Give option '-t' for setup-program to use
only terminal when installing.

Setup will ask directory path for actual program. This directory will
contain initial program parameters. Default directory name is
KeppiProxy in user's home directory. File KeppiProxyOptions in user's
home directory will refer to this directory. In this directory are
batch files keppi.bat (or keppi) which you can use to invoke server in
command prompt (use option -v for verbose mode), and robot.bat (or
robot) which you can use to invoke Web Client Robot program. Give
option '-?' to view options. Batch file manag.bat (or manag) you can
use to invoke Server Management Program.
In Setup-program select all checkboxes labeled "Proxy disabling" if
server is not running behind firewall, or add to file "domains" in server
directory lines:

private=ftp:*
private=gopher:*
private=http:*
private=https:*
private=imap4:*
private=msg:*
private=nntp:*
private=pop3:*
private=smtp:*
private=wais:*

and add some user to private-domain, otherwise server will be a open
proxy.

Put web applications (.war-files) to directory webapps in program
directory. Address http://host:port/proxy-config can be used for
automatic proxy configuration in Web browser. If you are going to run
server in public internet, remember to change manager password. You
can also install server program as Windows NT/2000 service as
described in document keppi.html, which you may want to read. If
service fails to start, stack trace can be read from file
KeppiProxyError in root directory. Ask for advice from email address:
ywanderingj@yahoo.com
