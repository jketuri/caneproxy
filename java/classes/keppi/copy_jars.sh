TO=$HOME/git/caneproxy/java
mkdir -pv $TO/jms1.1/lib
cp -v $JAVAHOME/jms1.1/lib/javax.jms.jar $TO/jms1.1/lib
JAVAMAIL_TO=$TO/`basename $JAVAMAIL`
mkdir -pv $JAVAMAIL_TO
cp -v $JAVAMAIL/mail.jar $JAVAMAIL_TO
mkdir -pv $TO/comm
cp -v $JAVAHOME/comm/comm.jar $TO/comm
TOMCAT_TO=$TO/`basename $TOMCAT_HOME`
mkdir -pv $TOMCAT_TO/bin
cp -v $TOMCAT_HOME/bin/tomcat-juli.jar $TOMCAT_TO/bin
mkdir -pv $TOMCAT_TO/lib
cp -v $TOMCAT_HOME/lib/catalina-ant.jar $TOMCAT_TO/lib
cp -v $TOMCAT_HOME/lib/el-api.jar $TOMCAT_TO/lib
cp -v $TOMCAT_HOME/lib/jsp-api.jar $TOMCAT_TO/lib
cp -v $TOMCAT_HOME/lib/servlet-api.jar $TOMCAT_TO/lib
cp -v $TOMCAT_HOME/lib/jasper.jar $TOMCAT_TO/lib
cp -v $TOMCAT_HOME/lib/jasper-el.jar $TOMCAT_TO/lib
ANT_TO=$TO/`basename $ANT_HOME`
mkdir -pv $ANT_TO/lib
cp -v $ANT_HOME/lib/ant.jar $ANT_TO/lib
cp -v $ANT_HOME/lib/ant-launcher.jar $ANT_TO/lib
mkdir -pv $TO/jdbm-1.0/lib
cp -v $JAVAHOME/jdbm-1.0/lib/jdbm-1.0.jar $TO/jdbm-1.0/lib
mkdir -pv $TO/crimson-1.1.3
cp -v $JAVAHOME/crimson-1.1.3/crimson.jar $TO/crimson-1.1.3
OPENSSL_TO=$TO/`basename $OPENSSL`
mkdir -pv $OPENSSL_TO
cp -v $OPENSSL/libcrypto.a $OPENSSL_TO
cp -v $OPENSSL/libssl.a $OPENSSL_TO
