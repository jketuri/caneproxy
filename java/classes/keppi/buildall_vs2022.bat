set MAKE=nmake
mkdir "%JAVAHOME%\bin"
mkdir "%JAVAHOME%\obj"
mkdir "%JAVAHOME%\debug"
mkdir "%JAVAHOME%\bin64"
mkdir "%JAVAHOME%\obj64"
mkdir "%JAVAHOME%\debug64"
del /q "%JAVAHOME%\bin64\*.*"
del /q "%JAVAHOME%\obj64\*.*"
del /q "%JAVAHOME%\debug64\*.*"
cd "%JAVAHOME%\classes\ar\com\ktulu\yenc"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\common"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
call "%VS170COMNTOOLS%..\..\VC\Auxiliary\Build\vcvarsall.bat" amd64
if not %ERRORLEVEL% == 0 goto end
"%MAKE%" /f makefile_vs2022 M64=1
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\protocol\file"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\protocol\ftp"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\protocol\gopher"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\protocol\http"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\protocol\https"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\protocol\imap4"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\protocol\imap4s"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\protocol\ldap"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\protocol\mailto"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\protocol\msg"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\protocol\news"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\protocol\nntp"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\protocol\pop3"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\protocol\pop3s"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\protocol\resource"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\protocol\smailto"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\protocol\sms"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\protocol\wais"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\asn1"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\beans"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\cimd"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\image"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\ldapbean"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\portalbean"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\msgbean"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\calbean"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\p2p"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\sasl"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\server"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
"%MAKE%" /f makefile_vs2022 M64=1
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\cellular"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\servlet"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
"%MAKE%" /f makefile_vs2022 M64=1
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\sql"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\sqlbean"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\gps"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\FI\realitymodeler\xml"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\busi_card"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
"%MAKE%" /f makefile_vs2022 M64=1
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\pos"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
"%MAKE%" /f makefile_vs2022 M64=1
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\ttf"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
"%MAKE%" /f makefile_vs2022 M64=1
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\ttf\webpages\WEB-INF\classes"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\LiveMap"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
"%MAKE%" /f makefile_vs2022 M64=1
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes\keppi"
javac %JAVAC_OPTIONS% *.java
if not %ERRORLEVEL% == 0 goto end
"%MAKE%" /f makefile_vs2022 M64=1
if not %ERRORLEVEL% == 0 goto end
cd "%JAVAHOME%\classes"
javac %JAVAC_OPTIONS% *.java
:end
