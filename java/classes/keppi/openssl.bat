pushd %OPENSSL%
if not %ERRORLEVEL% == 0 goto end
call "%WindowsSDKDir%bin\SetEnv.cmd" /Release /x86
if not %ERRORLEVEL% == 0 goto end
perl Configure VC-WIN32
if not %ERRORLEVEL% == 0 goto end
call ms\do_nasm.bat
if not %ERRORLEVEL% == 0 goto end
nmake -f ms\ntdll.mak clean
nmake -f ms\ntdll.mak
if not %ERRORLEVEL% == 0 goto end
cd %OPENSSL64%
if not %ERRORLEVEL% == 0 goto end
call "%WindowsSDKDir%bin\SetEnv.cmd" /Release /x64
if not %ERRORLEVEL% == 0 goto end
perl Configure VC-WIN64A
if not %ERRORLEVEL% == 0 goto end
call ms\do_win64a.bat
if not %ERRORLEVEL% == 0 goto end
nmake -f ms\ntdll.mak clean
nmake -f ms\ntdll.mak
if not %ERRORLEVEL% == 0 goto end
:end
popd
