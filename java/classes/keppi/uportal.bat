setlocal
if "%1" == "" (set PROGRAM_DIR=%USERPROFILE%\KeppiProxy) else (set PROGRAM_DIR=%1)
if "%2" == "" (set UPORTAL_DIR=C:\apps\uPortal-3.2.4-quick-start\apache-tomcat-6.0.18) else (set UPORTAL_DIR=%2)
if not exist "%PROGRAM_DIR%\lib" mkdir /s %PROGRAM_DIR%\lib
@if not "%errorlevel%" == "0" goto end
move "%PROGRAM_DIR%\extra\lib\catalina.jar" "%PROGRAM_DIR%\lib\"
@if not "%errorlevel%" == "0" goto end
xcopy /y "%UPORTAL_DIR%\shared\lib\*.jar" "%PROGRAM_DIR%\lib\"
@if not "%errorlevel%" == "0" goto end
if not exist "%PROGRAM_DIR%\webapps" mkdir /s %PROGRAM_DIR%\webapps
@if not "%errorlevel%" == "0" goto end
xcopy /y "%UPORTAL_DIR%\webapps\*.*" "%PROGRAM_DIR%\webapps\"
@if not "%errorlevel%" == "0" goto end
move %PROGRAM_DIR%\webapps\uPortal\WEB-INF\lib\script-api-1.0.jar %PROGRAM_DIR%\lib\
@if not "%errorlevel%" == "0" goto end
:end
endlocal
