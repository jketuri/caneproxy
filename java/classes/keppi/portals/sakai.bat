setlocal
if "%1" == "" (set PROGRAM_DIR=%USERPROFILE%\KeppiProxy) else (set PROGRAM_DIR=%1)
if "%2" == "" (set SAKAI_DIR=C:\apps\sakai-demo-2.7.1) else (set SAKAI_DIR=%2)
if not exist "%PROGRAM_DIR%\extra\lib" mkdir "%PROGRAM_DIR%\extra\lib"
@if not "%errorlevel%" == "0" goto end
xcopy /y "%SAKAI_DIR%\server\lib\sakai-*.jar" "%PROGRAM_DIR%\extra\lib\"
@if not "%errorlevel%" == "0" goto end
xcopy /y "%SAKAI_DIR%\server\lib\commons-*.jar" "%PROGRAM_DIR%\extra\lib\"
@if not "%errorlevel%" == "0" goto end
if not exist "%PROGRAM_DIR%\lib" mkdir "%PROGRAM_DIR%\lib"
@if not "%errorlevel%" == "0" goto end
xcopy /y "%SAKAI_DIR%\common\lib\jasper-*.jar" "%PROGRAM_DIR%\lib\"
@if not "%errorlevel%" == "0" goto end
xcopy /y "%SAKAI_DIR%\common\lib\commons-*.jar" "%PROGRAM_DIR%\lib\"
@if not "%errorlevel%" == "0" goto end
xcopy /y "%SAKAI_DIR%\common\lib\sakai-*.jar" "%PROGRAM_DIR%\lib\"
@if not "%errorlevel%" == "0" goto end
xcopy /y "%SAKAI_DIR%\common\lib\log4j-*.jar" "%PROGRAM_DIR%\lib\"
@if not "%errorlevel%" == "0" goto end
xcopy /y "%SAKAI_DIR%\shared\lib\*.jar" "%PROGRAM_DIR%\lib\"
@if not "%errorlevel%" == "0" goto end
copy "%PROGRAM_DIR%\lib\mail-1.4.3.jar" "%PROGRAM_DIR%\bin\mailapi.jar"
@if not "%errorlevel%" == "0" goto end
if not exist "%PROGRAM_DIR%\components" mkdir "%PROGRAM_DIR%\components"
@if not "%errorlevel%" == "0" goto end
xcopy /s /y "%SAKAI_DIR%\components\*.*" "%PROGRAM_DIR%\components\"
@if not "%errorlevel%" == "0" goto end
if not exist "%PROGRAM_DIR%\webapps" mkdir "%PROGRAM_DIR%\webapps"
@if not "%errorlevel%" == "0" goto end
xcopy /s /y "%SAKAI_DIR%\webapps\*.war" "%PROGRAM_DIR%\webapps\"
@if not "%errorlevel%" == "0" goto end
if not exist "%PROGRAM_DIR%\sakai" mkdir "%PROGRAM_DIR%\sakai"
@if not "%errorlevel%" == "0" goto end
xcopy /s /y "%SAKAI_DIR%\sakai\*.*" "%PROGRAM_DIR%\sakai\"
@if not "%errorlevel%" == "0" goto end
:end
endlocal
