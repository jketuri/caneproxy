#!/bin/sh

check()
{
    if test "$?" != "0"
	then
	exit 1
    fi
}

if test "$1" != ""
then
PROGRAM_DIR=$1
else
PROGRAM_DIR=$HOME/KeppiProxy
fi
if test "$2" != ""
then
JETSPEED_DIR=$2
else
JETSPEED_DIR=$HOME/Jetspeed-2.2.1
fi
mkdir -p $PROGRAM_DIR/lib
check
rsync -av --exclude='catalina*.jar' --exclude='servlet*.jar' --exclude='tomcat*.jar' --exclude='jasper*.jar' --exclude='jsp-api.jar' --exclude='el-api.jar' $JETSPEED_DIR/lib/ $PROGRAM_DIR/lib/
check
mkdir -p $PROGRAM_DIR/pages
check
cp -rv $JETSPEED_DIR/pages/* $PROGRAM_DIR/pages/
check
mkdir -p $PROGRAM_DIR/webapps
check
cp -rv $JETSPEED_DIR/webapps/* $PROGRAM_DIR/webapps/
check
cp -v $JETSPEED_DIR/conf/Catalina/localhost/jetspeed.xml $PROGRAM_DIR/webapps/jetspeed/META-INF/context.xml
check
mv -v $PROGRAM_DIR/lib/derby-10.3.2.1.jar $PROGRAM_DIR/extra/lib/
check
mkdir -p $PROGRAM_DIR/log
check
