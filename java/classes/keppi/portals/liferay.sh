#!/bin/sh

check()
{
    if test "$?" != "0"
	then
	exit 1
    fi
}

if test "$1" != ""
then
PROGRAM_DIR=$1
else
PROGRAM_DIR=$HOME/KeppiProxy
fi
if test "$2" != ""
then
LIFERAY_DIR=$2
else
LIFERAY_DIR=$HOME/apps/liferay-portal-6.0.5/tomcat-6.0.26
fi
mkdir -p $PROGRAM_DIR/lib
check
cp -v $PROGRAM_DIR/extra/lib/catalina.jar $PROGRAM_DIR/lib/
check
cp -v $LIFERAY_DIR/lib/ext/*.jar $PROGRAM_DIR/lib/
check
cp -rv $LIFERAY_DIR/webapps/* $PROGRAM_DIR/webapps/
check
mkdir -p $PROGRAM_DIR/conf
check
cp -rv $LIFERAY_DIR/conf/* $PROGRAM_DIR/conf/
check
mkdir -p $PROGRAM_DIR/../data
check
cp -rv $LIFERAY_DIR/../data/* $PROGRAM_DIR/../data
check
mkdir -p $PROGRAM_DIR/log
check
exit
sed -e "s%port=.*$%port=8080%" <$PROGRAM_DIR/server.properties >$PROGRAM_DIR/server.properties.out
check
mv -v $PROGRAM_DIR/server.properties.out $PROGRAM_DIR/server.properties
check
