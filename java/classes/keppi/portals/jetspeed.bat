setlocal
if "%1" == "" (set PROGRAM_DIR=%USERPROFILE%\KeppiProxy) else (set PROGRAM_DIR=%1)
if "%2" == "" (set JETSPEED_DIR=C:\Apache\Jetspeed-2.3.1) else (set JETSPEED_DIR=%2)
if not exist "%PROGRAM_DIR%\lib" mkdir "%PROGRAM_DIR%\lib"
@if not "%errorlevel%" == "0" goto end
xcopy /y /exclude:jetspeed.exc "%JETSPEED_DIR%\lib\*.jar" "%PROGRAM_DIR%\lib\"
@if not "%errorlevel%" == "0" goto end
if not exist "%PROGRAM_DIR%\pages" mkdir "%PROGRAM_DIR%\pages"
@if not "%errorlevel%" == "0" goto end
xcopy /s /y %JETSPEED_DIR%\pages\*.* %PROGRAM_DIR%\pages\
@if not "%errorlevel%" == "0" goto end
if not exist "%PROGRAM_DIR%\webapps" mkdir "%PROGRAM_DIR%\webapps"
@if not "%errorlevel%" == "0" goto end
xcopy /s /y "%JETSPEED_DIR%\webapps\*.*" "%PROGRAM_DIR%\webapps\"
@if not "%errorlevel%" == "0" goto end
xcopy /y "%JETSPEED_DIR%\conf\Catalina\localhost\jetspeed.xml" "%PROGRAM_DIR%\webapps\jetspeed\META-INF\context.xml"
@if not "%errorlevel%" == "0" goto end
move "%PROGRAM_DIR%\lib\derby-10.3.2.1.jar" "%PROGRAM_DIR%\extra\lib\"
@if not "%errorlevel%" == "0" goto end
if not exist "%PROGRAM_DIR%\log" mkdir "%PROGRAM_DIR%\log"
@if not "%errorlevel%" == "0" goto end
:end
endlocal
