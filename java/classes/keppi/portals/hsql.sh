#!/bin/sh
export ANT_HOME="`pwd`/apache-ant-1.7.1"
export CLASSPATH=
if test "$1" = "start"
then
./apache-ant-1.7.1/bin/ant hsql-start
elif test "$1" = "stop"
then
./apache-ant-1.7.1/bin/ant hsql-stop
fi