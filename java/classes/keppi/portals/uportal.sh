#!/bin/sh

check()
{
    if test "$?" != "0"
	then
	exit 1
    fi
}

if test "$1" != ""
then
PROGRAM_DIR=$1
else
PROGRAM_DIR=$HOME/KeppiProxy
fi
if test "$2" != ""
then
UPORTAL_DIR=$2
else
UPORTAL_DIR=$HOME/apps/uPortal-3.2.4-quick-start/apache-tomcat-6.0.24
fi
mkdir -p $PROGRAM_DIR/lib
check
mv -v $PROGRAM_DIR/extra/lib/catalina.jar $PROGRAM_DIR/lib/
check
cp -v $UPORTAL_DIR/shared/lib/*.jar $PROGRAM_DIR/lib/
check
mkdir -p $PROGRAM_DIR/webapps
check
cp -rv $UPORTAL_DIR/webapps/* $PROGRAM_DIR/webapps/
check
mv -v $PROGRAM_DIR/webapps/uPortal/WEB-INF/lib/script-api-1.0.jar $PROGRAM_DIR/lib/
check
cp -v hsql.sh $UPORTAL_DIR/..
check

