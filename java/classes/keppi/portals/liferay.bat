setlocal
if "%1" == "" (set PROGRAM_DIR=%USERPROFILE%\KeppiProxy) else (set PROGRAM_DIR=%1)
if "%2" == "" (set LIFERAY_DIR=C:\apps\liferay-portal-6.0.5\tomcat-6.0.26) else (set LIFERAY_DIR=%2)
if not exist "%PROGRAM_DIR%\lib" mkdir "%PROGRAM_DIR%\lib"
@if not "%errorlevel%" == "0" goto end
copy "%PROGRAM_DIR%\extra\lib\catalina.jar" "%PROGRAM_DIR%\lib\"
@if not "%errorlevel%" == "0" goto end
xcopy /d /y "%LIFERAY_DIR%\lib\ext\*.jar" "%PROGRAM_DIR%\lib\"
@if not "%errorlevel%" == "0" goto end
xcopy /d /s /y "%LIFERAY_DIR%\webapps\*.*" "%PROGRAM_DIR%\webapps\"
@if not "%errorlevel%" == "0" goto end
if not exist "%PROGRAM_DIR%\conf" mkdir "%PROGRAM_DIR%\conf"
@if not "%errorlevel%" == "0" goto end
xcopy /d /y "%LIFERAY_DIR%\conf\*.*" "%PROGRAM_DIR%\conf\"
@if not "%errorlevel%" == "0" goto end
if not exist "%PROGRAM_DIR%\..\data" mkdir "%PROGRAM_DIR%\..\data"
@if not "%errorlevel%" == "0" goto end
xcopy /d /s /y "%LIFERAY_DIR%\..\data\*.*" "%PROGRAM_DIR%\..\data\"
@if not "%errorlevel%" == "0" goto end
if not exist "%PROGRAM_DIR%\log" mkdir "%PROGRAM_DIR%\log"
@if not "%errorlevel%" == "0" goto end
:end
endlocal
