setlocal
if "%1" == "" (set PROGRAM_DIR=%USERPROFILE%\KeppiProxy) else (set PROGRAM_DIR=%1)
if "%2" == "" (set IPOINT_DIR=%USERPROFILE%\ipoint_oe_2.4.2_war) else (set IPOINT_DIR=%2)
if not exist "%PROGRAM_DIR%\lib" mkdir "%PROGRAM_DIR%\lib"
@if not "%errorlevel%" == "0" goto end
xcopy /y "%IPOINT_DIR%\lib\*.jar" "%PROGRAM_DIR%\lib\"
@if not "%errorlevel%" == "0" goto end
if not exist "%PROGRAM_DIR%\webapps\iPoint" mkdir "%PROGRAM_DIR%\webapps\iPoint"
@if not "%errorlevel%" == "0" goto end
xcopy /s /y "%IPOINT_DIR%\iPoint\*.*" "%PROGRAM_DIR%\webapps\iPoint\"
@if not "%errorlevel%" == "0" goto end
xcopy /y ".\ipoint_oe_2.4.2_war\iPoint\META-INF\context.xml" "%PROGRAM_DIR%\webapps\iPoint\META-INF\context.xml"
@if not "%errorlevel%" == "0" goto end
:end
endlocal
