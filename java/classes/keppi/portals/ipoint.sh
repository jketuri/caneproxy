#!/bin/sh

check()
{
    if test "$?" != "0"
	then
	exit 1
    fi
}

if test "$1" != ""
then
PROGRAM_DIR=$1
else
PROGRAM_DIR=$HOME/KeppiProxy
fi
if test "$2" != ""
then
IPOINT_DIR=$2
else
IPOINT_DIR=$HOME/ipoint_oe_2.4.2_war
fi
mkdir -p $PROGRAM_DIR/lib
check
cp -v $IPOINT_DIR/lib/*.jar $PROGRAM_DIR/lib/
check
cp -rv $IPOINT_DIR/iPoint $PROGRAM_DIR/webapps/
check
cp -v ./ipoint_oe_2.4.2_war/iPoint/META-INF/context.xml $PROGRAM_DIR/webapps/iPoint/META-INF/context.xml
check
