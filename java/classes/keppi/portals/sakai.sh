#!/bin/sh

check()
{
    if test "$?" != "0"
	then
	exit 1
    fi
}

if test "$1" != ""
then
PROGRAM_DIR=$1
else
PROGRAM_DIR=$HOME/KeppiProxy
fi
if test "$2" != ""
then
SAKAI_DIR=$2
else
SAKAI_DIR=$HOME/apps/sakai-demo-2.7.1
fi
mkdir -p $PROGRAM_DIR/extra/lib
check
cp -v $SAKAI_DIR/server/lib/sakai-*.jar $PROGRAM_DIR/extra/lib/
check
cp -v $SAKAI_DIR/server/lib/commons-*.jar $PROGRAM_DIR/extra/lib/
check
mkdir -p $PROGRAM_DIR/lib
check
cp -v $SAKAI_DIR/common/lib/jasper-*.jar $PROGRAM_DIR/lib/
check
cp -v $SAKAI_DIR/common/lib/commons-*.jar $PROGRAM_DIR/lib/
check
cp -v $SAKAI_DIR/common/lib/sakai-*.jar $PROGRAM_DIR/lib/
check
cp -v $SAKAI_DIR/common/lib/log4j-*.jar $PROGRAM_DIR/lib/
check
cp -v $SAKAI_DIR/shared/lib/*.jar $PROGRAM_DIR/lib/
check
cp -v $PROGRAM_DIR/lib/mail-1.4.3.jar $PROGRAM_DIR/bin/mailapi.jar
check
mkdir -p $PROGRAM_DIR/components
check
cp -rv $SAKAI_DIR/components/* $PROGRAM_DIR/components/
check
mkdir -p $PROGRAM_DIR/webapps
check
cp -rv $SAKAI_DIR/webapps/*.war $PROGRAM_DIR/webapps/
check
mkdir -p $PROGRAM_DIR/sakai
check
cp -rv $SAKAI_DIR/sakai/* $PROGRAM_DIR/sakai/
check
