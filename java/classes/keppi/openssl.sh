pushd $OPENSSL
if [[ "${OSTYPE}" =~ darwin.* ]]
then
./Configure --openssldir=. darwin64-x86_64-cc shared
make
else
./Configure linux-elf shared
make
cd $OPENSSL64
./Configure linux-x86_64 shared
make
fi
popd
