
(defun lbraces ()
  (interactive)
  (beginning-of-buffer)
  (replace-regexp "[^]a-z0-9\)}\";=,{':>]+{" " {"))

(defun rbraces ()
  (interactive)
  (beginning-of-buffer)
  (replace-regexp "}[^]a-z0-9\)}\";=,{]*else " "} else ")
  (beginning-of-buffer)
  (replace-regexp "}[^]a-z0-9\)}\";=,{]*catch " "} catch ")
  (beginning-of-buffer)
  (replace-regexp "}[^]a-z0-9\)}\";=,{]*finally " "} finally ")
  (beginning-of-buffer)
  (replace-regexp "}[^]a-z0-9\)}\";=,{]*while " "} while "))

(defun throws ()
  (interactive)
  (beginning-of-buffer)
  (replace-regexp ") throws " ")\n        throws "))

(global-set-key "\M-p" 'lbraces)
(global-set-key "\M-o" 'rbraces)
