#!/bin/sh

check()
{
    if test "$?" != "0"
	then
	exit 1
    fi
}

if test "$1" != ""
then
PROGRAM_DIR=$1
else
PROGRAM_DIR=$HOME/KeppiProxy
fi
if test "$2" != ""
then
GRIDSPHERE_DIR=$2
else
GRIDSPHERE_DIR=$HOME/apps/GridSphere-3.1
fi
#cp -v gridsphere/build.properties $GRIDSPHERE_DIR/
#check
#cp -v gridsphere/build.xml $GRIDSPHERE_DIR/
#check
export CATALINA_HOME=$PROGRAM_DIR
check
mkdir -p $PROGRAM_DIR/conf/Catalina/localhost
check
#find $GRIDSPHERE_DIR/webapps/gridsphere/jsp -name "*.jsp" -exec ./gridsphere1.sh {} \;
#check
unset CLASSPATH
pushd $GRIDSPHERE_DIR
check
ant deploy
check
popd
#rm -fv $PROGRAM_DIR/server/lib/jasper-*.jar $PROGRAM_DIR/server/lib/jasper.jar
#check
#cp -v $HOME/apps/apache-tomcat-5.5.28/common/lib/jasper-*.jar $PROGRAM_DIR/lib/
#check
#cp -v $HOME/apps/apache-tomcat-5.5.28/common/lib/commons-el.jar $PROGRAM_DIR/lib/
#check
