;;; .xemacs/init.el

;;; uncomment this line to disable loading of "default.el" at startup
;; (setq inhibit-default-init t)

(setq-default backup-inhibited t)
(setq-default case-fold-search t)

(setq-default indent-tabs-mode nil)

(line-number-mode t)

(setq auto-mode-alist
      (append '(("\.java$" . java-mode)) auto-mode-alist))
