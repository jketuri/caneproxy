#!/bin/sh

check()
{
    if test "$?" != "0"
	then
	exit 1
    fi
}

if test "$1" != ""
then
PROGRAM_DIR=$1
else
PROGRAM_DIR=$HOME/KeppiProxy
fi
mkdir -p $PROGRAM_DIR/conf/Catalina/localhost
check
cp -v DreamFace.xml $PROGRAM_DIR/conf/Catalina/localhost/
check
cp -v mysql-connector-java-5.1.6/mysql-connector-java-5.1.6-bin.jar $PROGRAM_DIR/common/lib/
check
cp -v DreamFace.war $PROGRAM_DIR/webapps/
check
