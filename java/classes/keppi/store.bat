verify on
del %JAVAHOME%\store.zip
cd %JAVAHOME%\classes
@if not "%1" == "" goto end
zip -9r %JAVAHOME%\store * -x \*.class -x \*.bak
@if "%1" == "" goto end
pause
dir a:
@if not "%1" == "" goto end
xcopy /y %JAVAHOME%\store.zip a:\
@if not "%1" == "" goto end
unzip -t a:\store
@if not "%1" == "" goto end
dir a:
:end