setlocal
set JAVA_OPTS=-Djava.compiler=NONE -Dsun.lang.ClassLoader.allowArraySyntax=true -client -Xms512m -Xmx768m -XX:PermSize=128m -XX:MaxPermSize=196m -XX:NewSize=192m -XX:MaxNewSize=384m
set PATH=.\bin;%PATH%
java %JAVA_OPTS% -classpath .\bin\FI_realitymodeler_server.jar FI.realitymodeler.server.ClientRobot %1 %2 %3 %4 %5
endlocal
