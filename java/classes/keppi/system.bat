del %temp%\system.zip
\jdk\bin\zip -9rv -b %temp% %temp%\system \jdk\* -x \jdk\classes\*
if not "%1"=="" goto skip
\jdk\bin\zip -9rv -b %temp% %temp%\system \jdk\classes\metadata\* \jdk\classes\moments.* \jdk\classes\FI\tenet\* \jdk\classes\java\* \jdk\classes\sunsoft\* -i *.class *.html *.txt *.var \jdk\classes\moments.bat \jdk\classes\FI\tenet\services.bat \jdk\classes\FI\tenet\server\mime_types \jdk\classes\FI\tenet\server\virtual_paths
:skip
copy /b \jdk\bin\unzipsfx.exe+%temp%\system.zip %temp%\system.exe
\jdk\bin\zip -A %temp%\system.exe
del %temp%\system.zip
dir %temp%\system.exe
