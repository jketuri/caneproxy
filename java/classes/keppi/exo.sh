#!/bin/sh

check()
{
    if test "$?" != "0"
	then
	exit 1
    fi
}

if test "$1" != ""
then
PROGRAM_DIR=$1
else
PROGRAM_DIR=$HOME/KeppiProxy
fi
if test "$2" != ""
then
EXO_DIR=$2
else
EXO_DIR=$HOME/apps/exo-eXoCS-1.3-Beta2-tomcat
fi
mkdir -p $PROGRAM_DIR/lib
check
cp -v $EXO_DIR/lib/*.jar $PROGRAM_DIR/lib/
check
#rm -v $PROGRAM_DIR/lib/catalina-ant.jar $PROGRAM_DIR/lib/catalina.jar $PROGRAM_DIR/lib/el-api.jar $PROGRAM_DIR/lib/jasper-el.jar $PROGRAM_DIR/lib/jasper.jar $PROGRAM_DIR/lib/jsp-api.jar $PROGRAM_DIR/lib/mail-1.4.jar $PROGRAM_DIR/lib/servlet-api.jar $PROGRAM_DIR/lib/tomcat-coyote.jar
mkdir -p $PROGRAM_DIR/webapps
check
cp -rv $EXO_DIR/webapps/*.war $PROGRAM_DIR/webapps/
check
mkdir -p $PROGRAM_DIR/conf
check
cp -rv $EXO_DIR/conf/* $PROGRAM_DIR/conf/
check
cp -v $EXO_DIR/bin/key.txt $PROGRAM_DIR/
check
