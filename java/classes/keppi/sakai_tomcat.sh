if test "$1" != ""
then
PROGRAM_DIR=$1
else
PROGRAM_DIR=$HOME/apache-tomcat-6.0.18
fi
if test "$2" != ""
then
SAKAI_DIR=$2
else
#SAKAI_DIR=$HOME/sakai
SAKAI_DIR=$HOME/apps/sakai-demo-2.5.4
fi
cp -v $SAKAI_DIR/server/lib/sakai-*.jar $PROGRAM_DIR/lib/
#cp -v $SAKAI_DIR/common/lib/naming-*.jar $PROGRAM_DIR/lib/
cp -v $SAKAI_DIR/common/lib/sakai-*.jar $PROGRAM_DIR/lib/
cp -v $SAKAI_DIR/common/lib/commons-*.jar $PROGRAM_DIR/lib/
cp -v $SAKAI_DIR/common/lib/log4j-*.jar $PROGRAM_DIR/lib/
cp -v $SAKAI_DIR/shared/lib/*.jar $PROGRAM_DIR/lib/
mkdir -p $PROGRAM_DIR/components
cp -rv $SAKAI_DIR/components/* $PROGRAM_DIR/components/
mkdir -p $PROGRAM_DIR/webapps
cp -rv $SAKAI_DIR/webapps/*.war $PROGRAM_DIR/webapps/
mkdir -p $PROGRAM_DIR/sakai
cp -rv $SAKAI_DIR/sakai/* $PROGRAM_DIR/sakai/
