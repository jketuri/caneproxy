pushd "%JAVAHOME%\classes\keppi"
if not exist "\keppi_pack" md "\keppi_pack"
@if not "%errorlevel%" == "0" goto end
del /f \keppi_pack\portals.zip
@if not "%errorlevel%" == "0" goto end
zip -r \keppi_pack\portals portals
@if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\classes\keppi\readme.txt" \keppi_pack\
@if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\classes\keppi\portals.html" \keppi_pack\
@if not "%errorlevel%" == "0" goto end
if a%OS%==aWindows_NT goto label1
deltree /y "%JAVAHOME%\keppi"
goto skip1
:label1
rd /s /q "%JAVAHOME%\keppi"
:skip1
md "%JAVAHOME%\keppi"
@if not "%errorlevel%" == "0" goto end
md "%JAVAHOME%\keppi\KeppiProxy\bin"
@if not "%errorlevel%" == "0" goto end
md "%JAVAHOME%\keppi\KeppiProxy\lib"
@if not "%errorlevel%" == "0" goto end
md "%JAVAHOME%\keppi\KeppiProxy\ssl"
@if not "%errorlevel%" == "0" goto end
md "%JAVAHOME%\keppi\KeppiProxy\extra\lib"
@if not "%errorlevel%" == "0" goto end
md "%JAVAHOME%\keppi\classes"
@if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\classes\keppi\ssl\public\ca.crt" "%JAVAHOME%\keppi\KeppiProxy\ssl\caCert.pem"
@if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\classes\keppi\ssl\public\server.crt" "%JAVAHOME%\keppi\KeppiProxy\ssl\cert.pem"
@if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\classes\keppi\ssl\private\server.key" "%JAVAHOME%\keppi\KeppiProxy\ssl\key.pem"
@if not "%errorlevel%" == "0" goto end
xcopy "%JAVAHOME%\classes\FI\realitymodeler\*.class" "%JAVAHOME%\keppi\classes\FI\realitymodeler\"
@if not "%errorlevel%" == "0" goto end
xcopy "%JAVAHOME%\classes\FI\realitymodeler\protocol\file\*.class" "%JAVAHOME%\keppi\classes\FI\realitymodeler\protocol\file\"
@if not "%errorlevel%" == "0" goto end
xcopy "%JAVAHOME%\classes\FI\realitymodeler\protocol\ftp\*.class" "%JAVAHOME%\keppi\classes\FI\realitymodeler\protocol\ftp\"
@if not "%errorlevel%" == "0" goto end
xcopy "%JAVAHOME%\classes\FI\realitymodeler\protocol\gopher\*.class" "%JAVAHOME%\keppi\classes\FI\realitymodeler\protocol\gopher\"
@if not "%errorlevel%" == "0" goto end
xcopy "%JAVAHOME%\classes\FI\realitymodeler\protocol\http\*.class" "%JAVAHOME%\keppi\classes\FI\realitymodeler\protocol\http\"
@if not "%errorlevel%" == "0" goto end
xcopy "%JAVAHOME%\classes\FI\realitymodeler\protocol\https\*.class" "%JAVAHOME%\keppi\classes\FI\realitymodeler\protocol\https\"
@if not "%errorlevel%" == "0" goto end
xcopy "%JAVAHOME%\classes\FI\realitymodeler\protocol\imap4\*.class" "%JAVAHOME%\keppi\classes\FI\realitymodeler\protocol\imap4\"
@if not "%errorlevel%" == "0" goto end
xcopy "%JAVAHOME%\classes\FI\realitymodeler\protocol\imap4s\*.class" "%JAVAHOME%\keppi\classes\FI\realitymodeler\protocol\imap4s\"
@if not "%errorlevel%" == "0" goto end
xcopy "%JAVAHOME%\classes\FI\realitymodeler\protocol\ldap\*.class" "%JAVAHOME%\keppi\classes\FI\realitymodeler\protocol\ldap\"
@if not "%errorlevel%" == "0" goto end
xcopy "%JAVAHOME%\classes\FI\realitymodeler\protocol\mailto\*.class" "%JAVAHOME%\keppi\classes\FI\realitymodeler\protocol\mailto\"
@if not "%errorlevel%" == "0" goto end
xcopy "%JAVAHOME%\classes\FI\realitymodeler\protocol\msg\*.class" "%JAVAHOME%\keppi\classes\FI\realitymodeler\protocol\msg\"
@if not "%errorlevel%" == "0" goto end
xcopy "%JAVAHOME%\classes\FI\realitymodeler\protocol\news\*.class" "%JAVAHOME%\keppi\classes\FI\realitymodeler\protocol\news\"
@if not "%errorlevel%" == "0" goto end
xcopy "%JAVAHOME%\classes\FI\realitymodeler\protocol\nntp\*.class" "%JAVAHOME%\keppi\classes\FI\realitymodeler\protocol\nntp\"
@if not "%errorlevel%" == "0" goto end
xcopy "%JAVAHOME%\classes\FI\realitymodeler\protocol\pop3\*.class" "%JAVAHOME%\keppi\classes\FI\realitymodeler\protocol\pop3\"
@if not "%errorlevel%" == "0" goto end
xcopy "%JAVAHOME%\classes\FI\realitymodeler\protocol\resource\*.class" "%JAVAHOME%\keppi\classes\FI\realitymodeler\protocol\resource\"
@if not "%errorlevel%" == "0" goto end
xcopy "%JAVAHOME%\classes\FI\realitymodeler\protocol\smailto\*.class" "%JAVAHOME%\keppi\classes\FI\realitymodeler\protocol\smailto\"
@if not "%errorlevel%" == "0" goto end
xcopy "%JAVAHOME%\classes\FI\realitymodeler\protocol\sms\*.class" "%JAVAHOME%\keppi\classes\FI\realitymodeler\protocol\sms\"
@if not "%errorlevel%" == "0" goto end
xcopy "%JAVAHOME%\classes\FI\realitymodeler\protocol\smtp\*.class" "%JAVAHOME%\keppi\classes\FI\realitymodeler\protocol\smtp\"
@if not "%errorlevel%" == "0" goto end
xcopy "%JAVAHOME%\classes\FI\realitymodeler\protocol\wais\*.class" "%JAVAHOME%\keppi\classes\FI\realitymodeler\protocol\wais\"
@if not "%errorlevel%" == "0" goto end
xcopy "%JAVAHOME%\classes\FI\realitymodeler\resources\*.*" "%JAVAHOME%\keppi\classes\FI\realitymodeler\resources\"
@if not "%errorlevel%" == "0" goto end
cd "%JAVAHOME%\keppi\classes"
@if not "%errorlevel%" == "0" goto end
"%jdkhome%\bin\jar" cvf "%JAVAHOME%\keppi\KeppiProxy\bin\FI_realitymodeler.jar" "FI\realitymodeler"
@if not "%errorlevel%" == "0" goto end
xcopy /s /y "%JAVAHOME%\classes\FI\*.class" "%JAVAHOME%\keppi\classes\FI\"
@if not "%errorlevel%" == "0" goto end
xcopy /s /y "%JAVAHOME%\classes\ar\*.class" "%JAVAHOME%\keppi\classes\ar\"
@if not "%errorlevel%" == "0" goto end
xcopy "%JAVAHOME%\classes\FI\realitymodeler\server\resources\*.*" "%JAVAHOME%\keppi\classes\FI\realitymodeler\server\resources\"
@if not "%errorlevel%" == "0" goto end
xcopy "%JAVAHOME%\classes\FI\realitymodeler\cellular\resources\*.*" "%JAVAHOME%\keppi\classes\FI\realitymodeler\cellular\resources\"
@if not "%errorlevel%" == "0" goto end
rem xcopy "%JAVAHOME%\classes\FI\realitymodeler\servlet\resources\*.*" "%JAVAHOME%\keppi\classes\FI\realitymodeler\servlet\resources\"
rem "%jdkhome%\bin\jar" cvf "%JAVAHOME%\keppi\KeppiProxy\bin\FI_realitymodeler_asn1.jar" "FI\realitymodeler\asn1"
rem "%jdkhome%\bin\jar" cvf "%JAVAHOME%\keppi\KeppiProxy\bin\FI_realitymodeler_beans.jar" "FI\realitymodeler\beans"
"%jdkhome%\bin\jar" cvf "%JAVAHOME%\keppi\KeppiProxy\bin\FI_realitymodeler_cellular.jar" "FI\realitymodeler\cellular"
@if not "%errorlevel%" == "0" goto end
"%jdkhome%\bin\jar" cvf "%JAVAHOME%\keppi\KeppiProxy\bin\FI_realitymodeler_common.jar" "FI\realitymodeler\common" "ar"
@if not "%errorlevel%" == "0" goto end
rem "%jdkhome%\bin\jar" cvf "%JAVAHOME%\keppi\KeppiProxy\bin\FI_realitymodeler_image.jar" "FI\realitymodeler\image"
rem "%jdkhome%\bin\jar" cvf "%JAVAHOME%\keppi\KeppiProxy\bin\FI_realitymodeler_ldapbean.jar" "FI\realitymodeler\ldapbean"
"%jdkhome%\bin\jar" cvf "%JAVAHOME%\keppi\KeppiProxy\bin\FI_realitymodeler_p2p.jar" "FI\realitymodeler\p2p"
@if not "%errorlevel%" == "0" goto end
"%jdkhome%\bin\jar" cvf "%JAVAHOME%\keppi\KeppiProxy\bin\FI_realitymodeler_sasl.jar" "FI\realitymodeler\sasl"
@if not "%errorlevel%" == "0" goto end
"%jdkhome%\bin\jar" cmvf "%JAVAHOME%\classes\FI\realitymodeler\server\manifest" "%JAVAHOME%\keppi\KeppiProxy\bin\FI_realitymodeler_server.jar" "FI\realitymodeler\server"
rem "%jdkhome%\bin\jar" cvf "%JAVAHOME%\keppi\KeppiProxy\bin\FI_realitymodeler_servlet.jar" "FI\realitymodeler\servlet"
"%jdkhome%\bin\jar" cvf "%JAVAHOME%\keppi\KeppiProxy\bin\FI_realitymodeler_sql.jar" "FI\realitymodeler\sql"
@if not "%errorlevel%" == "0" goto end
rem "%jdkhome%\bin\jar" cvf "%JAVAHOME%\keppi\KeppiProxy\bin\FI_realitymodeler_sqlbean.jar" "FI\realitymodeler\sqlbean"
rem "%jdkhome%\bin\jar" cvf "%JAVAHOME%\keppi\KeppiProxy\bin\FI_realitymodeler_xml.jar" "FI\realitymodeler\xml"
cd "%JAVAHOME%\keppi"
@if not "%errorlevel%" == "0" goto end
if a%OS%==aWindows_NT goto label2
deltree /y "%JAVAHOME%\keppi\classes"
@if not "%errorlevel%" == "0" goto end
goto skip2
:label2
rd /s /q "%JAVAHOME%\keppi\classes"
@if not "%errorlevel%" == "0" goto end
:skip2
@rem copy "%JAVAHOME%\commons-pool-1.3\commons-pool-1.3.jar" "%JAVAHOME%\keppi\KeppiProxy\extra\lib"
@rem @if not "%errorlevel%" == "0" goto end
@rem copy "%JAVAHOME%\commons-dbcp-1.2.2\commons-dbcp-1.2.2.jar" "%JAVAHOME%\keppi\KeppiProxy\extra\lib"
@rem @if not "%errorlevel%" == "0" goto end
copy "%TOMCAT_HOME%\lib\catalina.jar" "%JAVAHOME%\keppi\KeppiProxy\extra\lib"
@if not "%errorlevel%" == "0" goto end
copy "%TOMCAT_HOME%\lib\tomcat-dbcp.jar" "%JAVAHOME%\keppi\KeppiProxy\extra\lib"
@if not "%errorlevel%" == "0" goto end
copy "%TOMCAT_HOME%\lib\tomcat-api.jar" "%JAVAHOME%\keppi\KeppiProxy\lib"
@if not "%errorlevel%" == "0" goto end
copy "%TOMCAT_HOME%\lib\tomcat-coyote.jar" "%JAVAHOME%\keppi\KeppiProxy\lib"
@if not "%errorlevel%" == "0" goto end
copy "%TOMCAT_HOME%\lib\tomcat-util.jar" "%JAVAHOME%\keppi\KeppiProxy\lib"
@if not "%errorlevel%" == "0" goto end
copy "%TOMCAT_HOME%\lib\tomcat-util-scan.jar" "%JAVAHOME%\keppi\KeppiProxy\lib"
@if not "%errorlevel%" == "0" goto end
copy "%TOMCAT_HOME%\lib\ecj-4.6.3.jar" "%JAVAHOME%\keppi\KeppiProxy\lib"
@if not "%errorlevel%" == "0" goto end
copy "%TOMCAT_HOME%\lib\el-api.jar" "%JAVAHOME%\keppi\KeppiProxy\lib"
@if not "%errorlevel%" == "0" goto end
copy "%TOMCAT_HOME%\lib\jsp-api.jar" "%JAVAHOME%\keppi\KeppiProxy\lib"
@if not "%errorlevel%" == "0" goto end
copy "%TOMCAT_HOME%\lib\jasper.jar" "%JAVAHOME%\keppi\KeppiProxy\lib"
@if not "%errorlevel%" == "0" goto end
copy "%TOMCAT_HOME%\lib\jasper-el.jar" "%JAVAHOME%\keppi\KeppiProxy\lib"
@if not "%errorlevel%" == "0" goto end
copy "%TOMCAT_HOME%\lib\catalina-ant.jar" "%JAVAHOME%\keppi\KeppiProxy\lib"
@if not "%errorlevel%" == "0" goto end
@rem copy "%JAVAHOME%\commapi\comm.jar" "%JAVAHOME%\keppi\KeppiProxy\bin"
@rem if not "%errorlevel%" == "0" goto end
@rem copy "%JAVAHOME%\sasl\lib\sasl.jar" "%JAVAHOME%\keppi\KeppiProxy\bin"
@rem @if not "%errorlevel%" == "0" goto end
@rem copy "%JAVAHOME%\jndi\lib\ldapbp.jar" "%JAVAHOME%\keppi\KeppiProxy\bin"
@rem @if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\jdbm-1.0\lib\jdbm-1.0.jar" "%JAVAHOME%\keppi\KeppiProxy\bin"
@if not "%errorlevel%" == "0" goto end
copy "%TOMCAT_HOME%\bin\tomcat-juli.jar" "%JAVAHOME%\keppi\KeppiProxy\bin"
@if not "%errorlevel%" == "0" goto end
copy "%TOMCAT_HOME%\lib\servlet-api.jar" "%JAVAHOME%\keppi\KeppiProxy\bin"
@if not "%errorlevel%" == "0" goto end
@rem copy "%ANT_HOME%\lib\ant.jar" "%JAVAHOME%\keppi\KeppiProxy\lib"
@rem @if not "%errorlevel%" == "0" goto end
@rem copy "%ANT_HOME%\lib\ant-launcher.jar" "%JAVAHOME%\keppi\KeppiProxy\lib"
@rem @if not "%errorlevel%" == "0" goto end
@rem copy "%JAVAHOME%\jaf-1.1\activation.jar" "%JAVAHOME%\keppi\KeppiProxy\bin"
@rem @if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\javax.mail.jar" "%JAVAHOME%\keppi\KeppiProxy\bin"
@if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\jms1.1\lib\jms.jar" "%JAVAHOME%\keppi\KeppiProxy\bin"
@if not "%errorlevel%" == "0" goto end
@rem copy "%JAVAHOME%\jyenc-0.5\jyenc-0.5.jar" "%JAVAHOME%\keppi\KeppiProxy\bin"
@rem @if not "%errorlevel%" == "0" goto end
@rem copy "%JAVAHOME%\crimson-1.1.3\crimson.jar" "%JAVAHOME%\keppi\KeppiProxy\bin"
@rem @if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\classes\FI\realitymodeler\common\mime_types" "%JAVAHOME%\keppi\KeppiProxy\"
@if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\classes\FI\realitymodeler\server\domains" "%JAVAHOME%\keppi\KeppiProxy\"
@if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\classes\FI\realitymodeler\server\servlets" "%JAVAHOME%\keppi\KeppiProxy\"
@if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\classes\FI\realitymodeler\server\server.properties" "%JAVAHOME%\keppi\KeppiProxy\"
@if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\classes\FI\realitymodeler\server\system.properties" "%JAVAHOME%\keppi\KeppiProxy\"
@if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\classes\FI\realitymodeler\server\robots" "%JAVAHOME%\keppi\KeppiProxy\"
@if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\classes\FI\realitymodeler\server\robot_forms" "%JAVAHOME%\keppi\KeppiProxy\"
@if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\classes\FI\realitymodeler\server\robot_parts" "%JAVAHOME%\keppi\KeppiProxy\"
@if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\classes\FI\realitymodeler\server\robots.txt" "%JAVAHOME%\keppi\KeppiProxy\"
@if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\classes\FI\realitymodeler\server\virtual_paths" "%JAVAHOME%\keppi\KeppiProxy\"
@if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\classes\keppi\keppi.bat" "%JAVAHOME%\keppi\KeppiProxy\"
@if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\classes\keppi\manag.bat" "%JAVAHOME%\keppi\KeppiProxy\"
@if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\classes\keppi\robot.bat" "%JAVAHOME%\keppi\KeppiProxy\"
@if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\classes\keppi\setup.bat" "%JAVAHOME%\keppi\"
@if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\classes\keppi\*.class" "%JAVAHOME%\keppi\"
@if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\classes\keppi\readme.txt" "%JAVAHOME%\keppi\"
@if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\classes\keppi\keppi.html" "%JAVAHOME%\keppi\"
@if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\classes\keppi\LICENSE-Apache" "%JAVAHOME%\keppi\"
@if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\classes\keppi\LICENSE-OpenSSL" "%JAVAHOME%\keppi\"
@if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\classes\keppi\license-jikes.html" "%JAVAHOME%\keppi\"
@if not "%errorlevel%" == "0" goto end
cd "%JAVAHOME%"
@rem copy "%JAVAHOME%\commapi\win32com.dll" "%JAVAHOME%\keppi\KeppiProxy\bin"
@rem if not "%errorlevel%" == "0" goto end
@rem copy "%JAVAHOME%\commapi\javax.comm.properties" "%JAVAHOME%\keppi\KeppiProxy\bin"
@rem if not "%errorlevel%" == "0" goto end
copy "%OPENSSL64%\out32dll\libeay32.dll" "%JAVAHOME%\keppi\KeppiProxy\bin"
@if not "%errorlevel%" == "0" goto end
copy "%OPENSSL64%\out32dll\ssleay32.dll" "%JAVAHOME%\keppi\KeppiProxy\bin"
@if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\bin64\W3Service.exe" "%JAVAHOME%\keppi\KeppiProxy\bin"
@if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\bin64\FI_realitymodeler_native.dll" "%JAVAHOME%\keppi\KeppiProxy\bin"
@if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\bin64\FI_realitymodeler.dll" "%JAVAHOME%\keppi\KeppiProxy\bin"
@if not "%errorlevel%" == "0" goto end
rem copy "%JAVAHOME%\bin64\FI_realitymodeler_servlet.dll" "%JAVAHOME%\keppi\KeppiProxy\bin"
copy "%JAVAHOME%\bin64\FI_realitymodeler_ssl.dll" "%JAVAHOME%\keppi\KeppiProxy\bin"
@if not "%errorlevel%" == "0" goto end
@rem copy "%JAVAHOME%\jikes\src\jikes.exe" "%JAVAHOME%\keppi\KeppiProxy\bin"
@rem if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\bin64\keppi_setup.exe" "%JAVAHOME%\keppi"
@if not "%errorlevel%" == "0" goto end
del /f "\keppi_pack\keppi_win64.zip"
@if not "%errorlevel%" == "0" goto end
zip -rv "\keppi_pack\keppi_win64" keppi -x keppi\KeppiProxy\extra\lib\* -x keppi\KeppiProxy\lib\* -x keppi\KeppiProxy\bin\*.jar
@if not "%errorlevel%" == "0" goto end
@rem copy "%JAVAHOME%\commapi\win32com.dll" "%JAVAHOME%\keppi\KeppiProxy\bin"
@rem if not "%errorlevel%" == "0" goto end
@rem copy "%JAVAHOME%\commapi\javax.comm.properties" "%JAVAHOME%\keppi\KeppiProxy\bin"
@rem if not "%errorlevel%" == "0" goto end
copy "%OPENSSL%\out32dll\libeay32.dll" "%JAVAHOME%\keppi\KeppiProxy\bin"
@if not "%errorlevel%" == "0" goto end
copy "%OPENSSL%\out32dll\ssleay32.dll" "%JAVAHOME%\keppi\KeppiProxy\bin"
@if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\bin\W3Service.exe" "%JAVAHOME%\keppi\KeppiProxy\bin"
@if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\bin\FI_realitymodeler_native.dll" "%JAVAHOME%\keppi\KeppiProxy\bin"
@if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\bin\FI_realitymodeler.dll" "%JAVAHOME%\keppi\KeppiProxy\bin"
@if not "%errorlevel%" == "0" goto end
rem copy "%JAVAHOME%\bin\FI_realitymodeler_servlet.dll" "%JAVAHOME%\keppi\KeppiProxy\bin"
copy "%JAVAHOME%\bin\FI_realitymodeler_ssl.dll" "%JAVAHOME%\keppi\KeppiProxy\bin"
@if not "%errorlevel%" == "0" goto end
@rem copy "%JAVAHOME%\jikes\src\jikes.exe" "%JAVAHOME%\keppi\KeppiProxy\bin"
@rem if not "%errorlevel%" == "0" goto end
copy "%JAVAHOME%\bin\keppi_setup.exe" "%JAVAHOME%\keppi"
@if not "%errorlevel%" == "0" goto end
del /f "\keppi_pack\keppi_win32.zip"
@if not "%errorlevel%" == "0" goto end
zip -rv "\keppi_pack\keppi_win32" keppi -x keppi\KeppiProxy\extra\lib\* -x keppi\KeppiProxy\lib\* -x keppi\KeppiProxy\bin\*.jar
@if not "%errorlevel%" == "0" goto end
del /f "\keppi_pack\keppi_common1.zip"
@if not "%errorlevel%" == "0" goto end
zip -r "\keppi_pack\keppi_common1" keppi\KeppiProxy\extra\lib\* keppi\KeppiProxy\lib\*
@if not "%errorlevel%" == "0" goto end
del /f "\keppi_pack\keppi_common2.zip"
@if not "%errorlevel%" == "0" goto end
zip -r "\keppi_pack\keppi_common2" keppi\KeppiProxy\bin\*.jar
@if not "%errorlevel%" == "0" goto end
dir "\keppi_pack\keppi_common1.zip" "\keppi_pack\keppi_common2.zip" "\keppi_pack\keppi_win32.zip"
:end
popd
