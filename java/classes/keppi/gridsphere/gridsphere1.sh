#!/bin/sh

check()
{
    if test "$?" != "0"
	then
	exit 1
    fi
}

echo "$1"
check
sed -e 's/"\(<%=[^"<>]*"[^"=]*"[^"<>]*%>\)"/'\''\1'\''/g' <"$1" >"$1.sed"
check
mv -v "$1.sed" "$1"
check
