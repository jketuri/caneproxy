
check() {
    STATUS=$?
    if test "$STATUS" != "0"
    then
    echo "Error status $STATUS"
    exit 1
    fi
}

DAYS=3600
NUMBITS=4096
if test ! -d private
then
mkdir private
check
fi
if test ! -d public
then
mkdir public
check
fi
if test "$1" != ""
then
echo "Make CA key"
openssl genrsa -out private/ca.key -passout pass:changeit -des3 $NUMBITS
check
echo "Make CA cert"
yes "" | openssl req -passin pass:changeit -out public/ca.crt -new -key private/ca.key -config ca.cnf -x509 -days $DAYS
check
fi
echo "Make server key"
openssl genrsa -out private/server.key -passout pass:changeit -des3 $NUMBITS
check
echo "Make server cert signing request"
yes "" | openssl req -passin pass:changeit -out private/server.csr -new -key private/server.key -config cert.cnf
check
echo "Sign server key with CA cert"
openssl x509 -in private/server.csr -out public/server.crt -days $DAYS -req -CA public/ca.crt -CAkey private/ca.key -CAcreateserial -passin pass:changeit
check
echo "Convert server cert and key to pkcs12"
openssl pkcs12 -export -inkey private/server.key -in public/server.crt -out private/server.p12 -passin pass:changeit -passout pass:changeit
check
echo "Convert server cert to DER"
openssl x509 -inform PEM -outform DER -in public/server.crt -out public/server.der
check
echo "Make client key"
openssl genrsa -out private/client.key -passout pass:changeit -des3 $NUMBITS
check
echo "Make client cert signing request"
yes "" | openssl req -passin pass:changeit -out private/client.csr -new -key private/client.key -config cert.cnf
check
echo "Sign client key with CA cert"
openssl x509 -in private/client.csr -out public/client.crt -days $DAYS -req -CA public/ca.crt -CAkey private/ca.key -CAcreateserial -passin pass:changeit
check
echo "Convert client cert and key to pkcs12"
openssl pkcs12 -export -inkey private/client.key -in public/client.crt -out private/client.p12 -passin pass:changeit -passout pass:changeit
check
echo "Convert client cert to DER"
openssl x509 -inform PEM -outform DER -in public/client.crt -out public/client.der
check
rm -f keppi.jks
check
echo "Import CA cert to keystore"
keytool -import -alias ca -file public/ca.crt -keypass changeit -noprompt -trustcacerts -keystore keppi.jks -storepass changeit -v
check
echo "Import server cert to keystore"
keytool -import -alias keppi_server -file public/server.crt -keypass changeit -noprompt -trustcacerts -keystore keppi.jks -storepass changeit -v
check
echo "Import client cert to keystore"
keytool -import -alias keppi_client -file public/client.crt -keypass changeit -noprompt -trustcacerts -keystore keppi.jks -storepass changeit -v
check
keytool -list -keystore keppi.jks -storepass changeit -v
check
cat >public/server_cert.pem public/ca.crt public/server.crt
check
cat >private/server_key.pem private/ca.key private/server.key
check
cat >public/client_cert.pem public/ca.crt public/client.crt
check
cat >private/client_key.pem private/ca.key private/client.key
check
echo "Import server key to keystore"
keytool -import -alias tomcat -file public/server_cert.pem -keypass changeit -noprompt -trustcacerts -keystore keppi.jks -storepass changeit -v
check
mkdir -p $HOME/KeppiProxy/ssl
check
cp public/ca.crt $HOME/KeppiProxy/ssl/caCert.pem
check
cp private/server.key $HOME/KeppiProxy/ssl/key.pem
check
cp public/server.crt $HOME/KeppiProxy/ssl/cert.pem
check
