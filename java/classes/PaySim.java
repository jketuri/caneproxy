
import FI.realitymodeler.*;
import FI.realitymodeler.asn1.*;
import java.io.*;
import java.net.*;
import java.util.*;
import javax.servlet.*;

class PaySimReq extends Request
{
    static final int PSPid = AsnAny.makeTagId(AsnAny.CNTX, AsnAny.CONS, 3),
        PSPinitPaymentCmd = AsnAny.makeTagId(AsnAny.CNTX, AsnAny.CONS, 10),
        PSPinitPaymentRes = AsnAny.makeTagId(AsnAny.CNTX, AsnAny.CONS, 11),
        PSPpaymentStatusCmd = AsnAny.makeTagId(AsnAny.CNTX, AsnAny.CONS, 20),
        PSPpaymentStatusRes = AsnAny.makeTagId(AsnAny.CNTX, AsnAny.CONS, 21),
        PSParchivePaymentLogCmd = AsnAny.makeTagId(AsnAny.CNTX, AsnAny.CONS, 30),
        PSParchivePaymentLogRes = AsnAny.makeTagId(AsnAny.CNTX, AsnAny.CONS, 31),
        PSPgetPaymentStatsCmd = AsnAny.makeTagId(AsnAny.CNTX, AsnAny.CONS, 40),
        PSPgetPaymentStatsRes = AsnAny.makeTagId(AsnAny.CNTX, AsnAny.CONS, 41),
        PSPinitClaimCmd = AsnAny.makeTagId(AsnAny.CNTX, AsnAny.CONS, 50),
        PSPinitClaimRes = AsnAny.makeTagId(AsnAny.CNTX, AsnAny.CONS, 51),
        PSPconnectCmd = AsnAny.makeTagId(AsnAny.CNTX, AsnAny.CONS, 90),
        PSPconnectRes = AsnAny.makeTagId(AsnAny.CNTX, AsnAny.CONS, 91),
        PSPsynchronizePaymentLogCmd = AsnAny.makeTagId(AsnAny.CNTX, AsnAny.CONS, 100),
        PSPsynchronizePaymentLogRes = AsnAny.makeTagId(AsnAny.CNTX, AsnAny.CONS, 101);

    public PaySimReq()
    {
        super(null);
    }

    public Object clone()
    {
        return new PaySimReq();
    }

    public void run()
    {
        try {
            AsnBuf buf = new AsnBuf(new byte[1024]);
            DataInputStream din = new DataInputStream(socket.getInputStream());
            DataOutputStream dout = new DataOutputStream(socket.getOutputStream());
            for (;;) {
                if (din.readInt() == 0) break;
                AsnChoice msg = new AsnChoice(), res = new AsnChoice();
                if (msg.bDecTag(din) != PSPid) throw new Exception("Wrong protocol id");
                msg.bDecLen(din);
                msg.bDec(din);
                System.out.println("Received message");
                msg.print(System.out);
                System.out.println();
                AsnSeq resSeq = new AsnSeq();
                Vector msgVec = ((AsnSeq)msg.getValue()).getValue(), resVec = new Vector();
                int tag = msg.getTag();
                if (tag == PSPinitPaymentCmd) {
                    resVec.addElement(msgVec.firstElement());
                    resVec.addElement(new AsnInt(0));
                    resSeq.set(resVec);
                    res.set(PSPinitPaymentRes, resSeq);
                }
                else if (tag == PSPpaymentStatusCmd) {
                    resVec.addElement(msgVec.firstElement());
                    resVec.addElement(new AsnInt(0));
                    resSeq.set(resVec);
                    res.set(PSPpaymentStatusRes, resSeq);
                }
                else if (tag == PSParchivePaymentLogCmd) {
                    resVec.addElement(new AsnInt(0));
                    resSeq.set(resVec);
                    res.set(PSParchivePaymentLogRes, resSeq);
                }
                else if (tag == PSPgetPaymentStatsCmd) {
                    resVec.addElement(new AsnInt(0));
                    resVec.addElement(new AsnInt(1)); // totNumTrans
                    resVec.addElement(new AsnInt(2)); // numFailedTrans
                    resVec.addElement(new AsnInt(3)); // totAmtLogged
                    resVec.addElement(new AsnInt(4)); // numOfClaims
                    resVec.addElement(new AsnInt(5)); // avgTransAmt
                    resVec.addElement(new AsnInt(6)); // maxTransAmt
                    resSeq.set(resVec);
                    res.set(PSPgetPaymentStatsRes, resSeq);
                }
                else if (tag == PSPinitClaimCmd) {
                    resVec.addElement(msgVec.firstElement());
                    resVec.addElement(new AsnInt(0));
                    resSeq.set(resVec);
                    res.set(PSPinitClaimRes, resSeq);
                }
                else if (tag == PSPconnectCmd) {
                    resVec.addElement(msgVec.firstElement());
                    resVec.addElement(new AsnInt(0));
                    resSeq.set(resVec);
                    res.set(PSPconnectRes, resSeq);
                }
                else if (tag == PSPsynchronizePaymentLogCmd) {
                    resVec.addElement(new AsnInt(0));
                    resSeq.set(resVec);
                    res.set(PSPsynchronizePaymentLogRes, resSeq);
                }
                else throw new Exception("Unknown command tag " + tag);
                System.out.println("Sending response");
                res.print(System.out);
                System.out.println();
                buf.init();
                int l = res.bEnc(buf);
                l += res.bEncConsLen(buf, l);
                l += res.bEncTag(buf, PSPid);
                dout.writeInt(l);
                dout.write(buf.data, buf.dataPos, l);
                dout.flush();
                System.out.println("Sending status and waiting for response");
                msg = new AsnChoice();
                msgVec = new Vector();
                msgVec.addElement(new AsnOcts(""));
                msgVec.addElement(new AsnInt(0));
                msgVec.addElement(new AsnOcts(""));
                msgVec.addElement(new AsnInt(0));
                AsnSeq msgSeq = new AsnSeq();
                msgSeq.set(msgVec);
                msg.set(PSPpaymentStatusCmd, msgSeq);
                buf.init();
                l = msg.bEnc(buf);
                l += msg.bEncConsLen(buf, l);
                l += msg.bEncTag(buf, PSPid);
                dout.writeInt(l);
                dout.write(buf.data, buf.dataPos, l);
                dout.flush();
                if (din.readInt() == 0) break;
                res = new AsnChoice();
                if (res.bDecTag(din) != PSPid) throw new Exception("Wrong protocol id");
                res.bDecLen(din);
                res.bDec(din);
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        finally {
            try {
                socket.close();
            }
            catch (IOException ex) {}
        }
    }

}

public class PaySim extends SocketServer implements Servlet
{
    private ServletConfig config;

    public PaySim() throws IOException
    {
        super(new ServerSocket(1929), new PaySimReq(), 60000L);
        start();
    }

    public void init(ServletConfig config) throws ServletException
    {
        this.config = config;
        try {
            new PaySim();
        }
        catch (IOException ex) {
            throw new ServletException(ex.toString());
        }
    }

    public ServletConfig getServletConfig()
    {
        return config;
    }


    public void service(ServletRequest req, ServletResponse res)
    {
    }

    public String getServletInfo()
    {
        return getClass().getName();
    }

    public void destroy()
    {
    }

    public static void main(String argv[]) throws Exception
    {
        if (argv.length > 0) {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            W3Requester requester = new W3Requester(argv[0], 1929);
            DataInputStream din = new DataInputStream(requester.input);
            DataOutputStream dout = new DataOutputStream(requester.output);
            AsnBuf buf = new AsnBuf(new byte[1024]);
            String s;
            try {
                while ((s = br.readLine()) != null)
                    {
                        StringTokenizer st = new StringTokenizer(s);
                        int id = Integer.parseInt(st.nextToken());
                        AsnChoice cmd = new AsnChoice();
                        AsnSeq cmdSeq = new AsnSeq();
                        Vector cmdVec = new Vector();
                        switch (id) {
                        case 0:
                            cmdVec.addElement(new AsnOcts(st.nextToken()));
                            cmdVec.addElement(new AsnInt(Integer.parseInt(st.nextToken())));
                            cmdVec.addElement(new AsnInt(Integer.parseInt(st.nextToken())));
                            cmdVec.addElement(new AsnOcts(AsnAny.UTCTIME_TAG_CODE, st.nextToken()));
                            cmdVec.addElement(new AsnOcts(st.nextToken()));
                            cmdVec.addElement(new AsnInt(Integer.parseInt(st.nextToken())));
                            cmdSeq.set(cmdVec);
                            cmd.set(PaySimReq.PSPinitPaymentCmd, cmdSeq);
                            break;
                        case 2:
                            cmdVec.addElement(new AsnOcts(st.nextToken()));
                            cmdVec.addElement(new AsnInt(Integer.parseInt(st.nextToken())));
                            cmdVec.addElement(new AsnOcts(st.nextToken()));
                            cmdVec.addElement(new AsnInt(Integer.parseInt(st.nextToken())));
                            cmdSeq.set(cmdVec);
                            cmd.set(PaySimReq.PSPpaymentStatusCmd, cmdSeq);
                            break;
                        case 4:
                            cmdVec.addElement(new AsnOcts(st.nextToken()));
                            cmdVec.addElement(new AsnOcts(AsnAny.UTCTIME_TAG_CODE, st.nextToken()));
                            cmdVec.addElement(new AsnOcts(AsnAny.UTCTIME_TAG_CODE, st.nextToken()));
                            cmdSeq.set(cmdVec);
                            cmd.set(PaySimReq.PSParchivePaymentLogCmd, cmdSeq);
                            break;
                        case 6:
                            cmdVec.addElement(new AsnOcts(AsnAny.UTCTIME_TAG_CODE, st.nextToken()));
                            cmdSeq.set(cmdVec);
                            cmd.set(PaySimReq.PSPgetPaymentStatsCmd, cmdSeq);
                            break;
                        case 8:
                            cmdVec.addElement(new AsnOcts(st.nextToken()));
                            cmdVec.addElement(new AsnInt(Integer.parseInt(st.nextToken())));
                            cmdVec.addElement(new AsnOcts(AsnAny.UTCTIME_TAG_CODE, st.nextToken()));
                            cmdSeq.set(cmdVec);
                            cmd.set(PaySimReq.PSPinitClaimCmd, cmdSeq);
                            break;
                        case 10:
                            cmdVec.addElement(new AsnOcts(st.nextToken()));
                            cmdSeq.set(cmdVec);
                            cmd.set(PaySimReq.PSPconnectCmd, cmdSeq);
                            break;
                        case 12:
                            cmdVec.addElement(new AsnOcts(st.nextToken()));
                            cmdVec.addElement(new AsnOcts(AsnAny.UTCTIME_TAG_CODE, st.nextToken()));
                            cmdVec.addElement(new AsnOcts(AsnAny.UTCTIME_TAG_CODE, st.nextToken()));
                            cmdSeq.set(cmdVec);
                            cmd.set(PaySimReq.PSPsynchronizePaymentLogCmd, cmdSeq);
                            break;
                        default:
                            throw new Exception("Unknown command id " + id);
                        }
                        System.out.println("Sending command");
                        cmd.print(System.out);
                        System.out.println();
                        buf.init();
                        int l = cmd.bEnc(buf);
                        l += cmd.bEncConsLen(buf, l);
                        l += cmd.bEncTag(buf, PaySimReq.PSPid);
                        dout.writeInt(l);
                        dout.write(buf.data, buf.dataPos, l);
                        dout.flush();
                        if (din.readInt() == 0) throw new Exception("Didn't get response");
                        AsnChoice res = new AsnChoice();
                        if (res.bDecTag(din) != PaySimReq.PSPid) throw new Exception("Wrong protocol id");
                        res.bDecLen(din);
                        res.bDec(din);
                        System.out.println("Received response");
                        res.print(System.out);
                        System.out.println();
                        System.out.println("Waiting for status and sending response");
                        if (din.readInt() == 0) throw new Exception("Didn't get status");
                        cmd = new AsnChoice();
                        if ((cmd.bDecTag(din)) != PaySimReq.PSPid) throw new Exception("Wrong protocol id");
                        cmd.bDecLen(din);
                        cmd.bDec(din);
                        Vector resVec = new Vector();
                        resVec.addElement(new AsnOcts(""));
                        resVec.addElement(new AsnInt(0));
                        AsnSeq resSeq = new AsnSeq();
                        resSeq.set(resVec);
                        res = new AsnChoice();
                        res.set(PaySimReq.PSPpaymentStatusRes, resSeq);
                        buf.init();
                        l = res.bEnc(buf);
                        l += res.bEncConsLen(buf, l);
                        l += res.bEncTag(buf, PaySimReq.PSPid);
                        dout.writeInt(l);
                        dout.write(buf.data, buf.dataPos, l);
                        dout.flush();
                    }
            }
            finally {
                dout.writeInt(0);
                dout.flush();
                requester.close();
            }
            return;
        }
        System.out.println("Starting payment simulator");
        new PaySim();
    }

}
