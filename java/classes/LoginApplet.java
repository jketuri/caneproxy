
import FI.realitymodeler.common.*;
import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;

public class LoginApplet extends Applet implements ActionListener
{
    TextField username, password;
    Button login, clear;

    public void init()
    {
        GridBagLayout gbl = new GridBagLayout();
        setLayout(gbl);
        GridBagConstraints gbc = new GridBagConstraints();
        Label label;
        label = new Label("Username:");
        gbl.setConstraints(label, gbc);
        add(label);
        label = new Label("Password:");
        gbc.gridy = 1;
        gbl.setConstraints(label, gbc);
        add(label);
        clear = new Button("Clear");
        gbc.gridy = 2;
        gbl.setConstraints(clear, gbc);
        add(clear);
        clear.addActionListener(this);
        login = new Button("Login");
        gbc.gridx = 1;
        gbl.setConstraints(login, gbc);
        add(login);
        login.addActionListener(this);
        gbc.weightx = 1;
        gbc.fill = gbc.HORIZONTAL;
        username = new TextField();
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbl.setConstraints(username, gbc);
        add(username);
        password = new TextField();
        password.setEchoChar('#');
        gbc.gridy = 1;
        gbl.setConstraints(password, gbc);
        add(password);
    }

    public void actionPerformed(ActionEvent e)
    {
        Object source = e.getSource();
        if (source == login) {
            try {
                String s = new BASE64Encoder().encodeStream((username.getText() + ":" + password.getText()).getBytes());
                getAppletContext().showDocument(new URL(getParameter("location") + "?" +
                                                        URLEncoder.encode(s.substring(0, s.length() - 1))));
            }
            catch (IOException ex) {
                ex.printStackTrace();
                destroy();
            }
        }
        else if (source == clear) {
            username.setText("");
            password.setText("");
        }
    }

}
