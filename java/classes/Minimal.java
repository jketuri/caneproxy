
import FI.realitymodeler.p2p.*;
import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.rmi.*;
import java.util.*;
import javax.jms.*;

public class Minimal extends Applet implements ActionListener, Runnable
{
	static final int n_minpics = 8;
	static final int n_minclips = 4;
	static final int mp_width = 64;
	static final int mp_height = 92;

	Image minpics[];
	byte status[];
	boolean turn;
	int result, yoff;
	TextField msg, opponentField;
	String opponent, player;
	Random random;
	Peer2Peer p2p = null;
	QueueConnection connection = null;
	QueueSession session = null;
	QueueSender sender = null;
	QueueReceiver receiver = null;
	Thread thread = null;

void restart()
{
	if (thread != null && thread.isAlive()) thread.interrupt();
	thread = new Thread(this);
	thread.start();
}

public void init()
{
	int i, n, t = 0;
	random = new Random(System.currentTimeMillis());
	status = new byte[2];
	minpics = new Image[2];
	for (i = 0; i < 2; i++)
	{
		n = Math.abs(random.nextInt()) % n_minpics + 1;
		if (i == 0) t = n;
		else if (n == t) n = t == n_minpics ? 1 : t + 1;
		try
		{
			minpics[i] = getImage(getDocumentBase(), "images/minpic" + n + ".gif");
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			destroy();
		}
	}
	yoff = getSize().height - (mp_height + 4) * 2;
	msg = new TextField(30);
	msg.setEditable(false);
	add(msg);
	add(opponentField = new TextField(30));
	opponentField.addActionListener(this);
	addMouseListener(new MouseAdapter()
	{
		public void mousePressed(MouseEvent e)
		{
			if (result != 0)
			{
				boolean lose = result == 2;
				newGame();
				if (opponent != null) send();
				else if (lose) think();
				repaint();
				return;
			}
			if (!turn) return;
			int row = (e.getY() - yoff) / (mp_height + 1);
			if (row < 0 || row > 1) return;
			int c = status[row] & 0xf;
			int cc = (e.getX() - 4) / (mp_width + 1);
			if (cc == c || cc < 0 || cc > 6 || cc > 6 - (status[row] >> 4)) return;
			status[row] = (byte)(cc | (status[row] & 0xf0));
			result = test();
			if (opponent != null) send();
			else if (result == 0)
			{
				think();
				result = test();
			}
			repaint();
		}
	});
	try
	{
		InetAddress addresses[] = InetAddress.getAllByName("127.0.0.1"),
		loopback = InetAddress.getByName("127.0.0.1");
		for (i = 0; i < addresses.length; i++)
		if (!addresses[i].equals(loopback))
		{
			player = addresses[i].getHostAddress();
			break;
		}
		String ref = getDocumentBase().getRef();
		if (ref != null && !ref.equals(""))
		{
			p2p = Peer2Peer.getPeer2Peer(getDocumentBase().getHost());
			connection = p2p.createQueueConnection();
			session = connection.createQueueSession(false, QueueSession.AUTO_ACKNOWLEDGE);
			receiver = session.createReceiver(p2p.getQueue(getClass().getName() + "/" + player));
			sender = session.createSender(p2p.getQueue(getClass().getName() + "/" + ref));
			opponent = ref;
			opponentField.setText(opponent);
			restart();
		}
	}
	catch (Exception ex)
	{
		ex.printStackTrace();
		destroy();
	}
	newGame();
}

public void close()
{
	try
	{
		try
		{
			if (receiver != null)
			{
				receiver.close();
				receiver = null;
			}
		}
		finally
		{
			try
			{
				if (sender != null)
				{
					sender.close();
					sender = null;
				}
			}
			finally
			{
				try
				{
					if (session != null)
					{
						session.close();
						session = null;
					}
				}
				finally
				{
					if (connection != null)
					{
						connection.close();
						connection = null;
					}
				}
			}
		}
	}
	catch (JMSException ex) {}
}

public void destroy()
{
	super.destroy();
	try
	{
		if (thread != null && thread.isAlive()) thread.interrupt();
	}
	finally
	{
		close();
	}
}

public void paint(Graphics g)
{
	g.clearRect(0, 0, getSize().width, getSize().height);
	int i, n, v;
	n = yoff + (mp_height + 1) * 2;
	for (i = 0; i < 9; i++)
	{
		v = 4 + i * (mp_width + 1);
		g.drawLine(v, yoff, v, n);
	}
	n = 4 + 8 * (mp_width + 1);
	for (i = 0; i < 3; i++)
	{
		v = yoff + i * (mp_height + 1);
		g.drawLine(4, v, n, v);
	}
	for (i = 0; i < 2; i++)
	{
		v = yoff + 1 + i * (mp_height + 1);
		g.drawImage(minpics[0], 5 + (status[i] & 0xf) * (mp_width + 1), v, this);
		g.drawImage(minpics[1], 5 + (7 - (status[i] >> 4)) * (mp_width + 1), v, this);
	}
	msg.setText(result == -1 ? "Problems with connection" : result == 1 ? "You won!" :
	result == 2 ? "You lost!" : turn ? "Your turn" : "Opponent's turn");
	if (result > 0) play(getDocumentBase(), "audio/minclip" + (result == 1 ? "A" : "B") +
	(Math.abs(random.nextInt()) % n_minclips + 1) + ".au");
}

int test()
{
	int i;
	for (i = 0; i < 2; i++)
	if ((status[i] & 0xf) < 6) break;
	if (i == 2) return 1;
	for (i = 0; i < 2; i++)
	if ((status[i] >> 4) < 6) break;
	if (i == 2) return 2;
	return 0;
}

void think()
{
	int i, j = 0, m = -1, n;
	for (i = 0; i < 2; i++)
	if ((n = (status[i] >> 4) + (status[i] & 0xf)) > m)
	{
		m = n;
		j = i;
	}
	j ^= 1;
	if ((m > 4 || random.nextFloat() > .2) &&
	(status[j] >> 4) + (n = status[j] & 0xf) < m)
	{
		status[j] = (byte)(n | ((m - n) << 4));
		return;
	}
	for (i = j = Math.abs(random.nextInt()) % 2; i == j; i ^= 1)
	if ((n = (status[i] >> 4) + (status[i] & 0xf)) < 5)
	{
		status[i] += 0x10 * (Math.abs(random.nextInt()) % (5 - n) + 1);
		return;
	}
	for (i = 0; i < 2; i++)
	if ((n = status[i] >> 4) > 0)
	{
		status[i] -= 0x10;
		return;
	}
	status[Math.abs(random.nextInt()) % 2] += 0x10;
	return;
}

void newGame()
{
	turn = true;
	status[0] = status[1] = 0;
	result = 0;
}

public void run()
{
	for (;;)
	try
	{
		ObjectMessage message = (ObjectMessage)receiver.receive();
		status = (byte[])message.getObject();
		for (int i = 0; i < 2; i++) status[i] = (byte)((status[i] << 4) | (status[i] >> 4));
		result = test();
		turn = true;
		repaint();
	}
	catch (Exception ex)
	{
		if (ex instanceof InterruptedException) break;
		ex.printStackTrace();
		msg.setText(ex.toString());
	}
}

public void send()
{
	try
	{
		sender.send(session.createObjectMessage(status));
		turn = false;
	}
	catch (Exception ex)
	{
		ex.printStackTrace();
		msg.setText(ex.toString());
	}
}

public void actionPerformed(ActionEvent e)
{
msg.setText("action performed");
	Object source = e.getSource();
	if (source == opponentField)
	{
msg.setText("oppnentField");
		newGame();
		opponent = opponentField.getText();
		if (!opponent.equals(""))
		try
		{
msg.setText("opponent="+opponent);
			close();
msg.setText("getting p2p="+p2p);
			if (p2p == null) p2p = Peer2Peer.getPeer2Peer(getDocumentBase().getHost());
msg.setText("p2p");
			connection = p2p.createQueueConnection();
msg.setText("connection");
			session = connection.createQueueSession(false, QueueSession.AUTO_ACKNOWLEDGE);
msg.setText("session");
			receiver = session.createReceiver(session.createQueue(getClass().getName() + "/" + player));
msg.setText("receiver");
			sender = session.createSender(session.createQueue(getClass().getName() + "/" + opponent));
msg.setText("sender created="+player);
			turn = true;
			restart();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			msg.setText(ex.toString());
			opponent = null;
			return;
		}
		else opponent = null;
		repaint();
	}
}

}
